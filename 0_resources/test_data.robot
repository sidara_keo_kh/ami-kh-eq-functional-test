*** Variables ***
${env}            local
#${browser}        phantomjs    # phantomjs chrome

#${system_user_rsa_public_key}   MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgHegV0ZHUmFBFjQ3CTefUxmBvj52GwbfhhufPSbUH6BfDz7Jtfp3NMaHQSCn9M/9zyUYmIDyhiJQeEfceZtkN8snpTU6uTddmeSAOKbafoI7md6fPMYYojHPGS2R4yIGMSt290AlIaGEtrZVb9KfG31wxrcXNvG2zua1noNEWg5bAgMBAAE=
#${agent_rsa_public_key}         MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCsfOnDzDh8d7ZvlMnXZaZhVZ5hrU85VhXCAGQNokTVw6NvHbv0sOJF6ttehfsDlokpAy6SMsfuOK2pXVzV6Y4PcFgl8CO5foxv0Xw0BjP8Le6NhDSFfJGzzLNxBiNDtSuE+GSRzggyF/a8YyJP2xOTBEM4Owdoyv4YmjBnl5L0xwIDAQAB=
#${customer_rsa_public_key}      MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgF5MrbmgNBVsTr7hgjPX7R540dRHNQ/2gyvoO6ZiCNF4przP31GEWW0cdG96t3FDy35aV/oxX2ZwJF9U3TBo6fSt2k1QVLj6DimwXUQsyJiRntfcl2yyFe9VmWnXQgE37T8yft21EEv4cccaE1EvgMmCycMNXo+ysdS/EB6YDTihAgMBAAE=

${setup_admin_id}    1
${setup_admin_user_type_id}    3
${setup_admin_username}    suhlaing.phyo
#
#${set_new_password1}    pass99word1
#${set_new_password2}    9pass
#
#${setup_edc_channel_id}     1
#${setup_mobile_channel_id}     2
#
#${setup_admin_password}    pass99word
#${setup_admin_password_encrypted}     RW/ABgCDl3NhVud4/QGMsJCtd9EwzON2GIoGvoLI/FubuEm13L4Xv/iUHj6L2nuBnufr9XipsXRT8MGSnNUczFJOMlYPNbGzy0KjrvIPT+QThDnNj8F5ZtaoonbGEc3RQGLVvDJTK7ddFzW9Siba06k9EtoEFjGtffxroNEeCNI=
#${setup_admin_password_encrypted_utf8}       RW%2FABgCDl3NhVud4%2FQGMsJCtd9EwzON2GIoGvoLI%2FFubuEm13L4Xv%2FiUHj6L2nuBnufr9XipsXRT8MGSnNUczFJOMlYPNbGzy0KjrvIPT%2BQThDnNj8F5ZtaoonbGEc3RQGLVvDJTK7ddFzW9Siba06k9EtoEFjGtffxroNEeCNI%3D
#
#${setup_admin_password_old}      P@ssw0rd001
#${setup_admin_password_encrypted_old}      a6Sga/qzXLSm7RNuCC0Zqk6zcMk37s7hzR1EZQjA0Ziw6h+fIH+xZHMsuyInVMUFk/bP24Nd99gXINT3xfgTTjnTYHCPDUd+gt+3VRC+GOxuncnnr/y9/tnGEwx1fN/49UzZEeYVcLAcXy0lQNswvBoLupBfy9yN2VbI6bFNLm0=
#${setup_admin_password_encrypted_utf8_old}     a6Sga%2FqzXLSm7RNuCC0Zqk6zcMk37s7hzR1EZQjA0Ziw6h%2BfIH%2BxZHMsuyInVMUFk%2FbP24Nd99gXINT3xfgTTjnTYHCPDUd%2Bgt%2B3VRC%2BGOxuncnnr%2Fy9%2FtnGEwx1fN%2F49UzZEeYVcLAcXy0lQNswvBoLupBfy9yN2VbI6bFNLm0%3D
#
${setup_password_admin_encrypted_utf8}     WGiELswl1YlsIPKewaax+d59ijF8NEhdIzIK+P9WoAxxwJ67i4qCuWChluxOBETi50IP97+UPHdRhOpTjpw2ZIN7ZKXBia6/jNN4rYtFiWus4DXrsKhrcGPYpJ8YVJ7R5w2sLqQV9DjDl/SqqL889Dn9P3Rzll91FA9xxnfC5qk=
#${set_new_password_encrypted}         TsTwTF4KRBs3iaFSdA1Og2hlEpBLYpKHVaToUR+Ah8dMEoSoKOxGSjmY9+G/V163BMKqI/Hz+1qeb9XoBoD2xt+5En0O9RrJRMYUS+gJYYGeiyhCJFipoMy0Viq9fV0XimasPxKONaBjL1gyyNTAtZzjfSB1F5N0yvSYczTT7Wk=
#${set_invalid_password_encrypted}     invalid_not_encrypted
#
#${setup_password_robot}     P@ssw0rd
#${setup_password_robot_encrypted}     OptA0POsmIiiX+X61jTJs659e02kEWHOwElEtxYzTVmkjQvk5uMe5kzc09UHP+bBhLujQPN4FtXB+Asw6oQnQzwD0F+9pnoYhzk4ah8U1I7kkK/q6CH+RcUluEQTck3AggwTRpuwpBCYaeZSzULwFV2r4LDWbF8nXeFLOXN526c=
#
#${setup_agent_password}   P@ssw0rd
#${setup_agent_password_encrypted}     gIdm+uMA5ACWJfX0FP5WWuED+tibdTaMBO4uayf65++WTP13e8SJNE+DFkv/6XITi6bwnpTzpRO0G2YVBI7W1xWU+836MdaL2qbAOIckG/FIe9k/bFA1VM/mA3fhQ25eB0RDtCxAWfHRRf+9Fry+j3wDDmWJMMn8CyrCY2eVhzU=
#${setup_agent_password_encrypted_utf8}          HYXvRTfBC1ho4MW5bLKqC6rEb3ZvCkxQvBjcb%2ByKe3vtnIXiW1yCsHrNCKGjgscnNsIHap%2BifwcsOmPhT3sqAAE55qLs0ll6QEn219PildJVbgFlLvFqiMWBukET0uRLFKqPGDjjkcvIuvF9VRKi1uAmwBO9RkAroZ7ac68JKL4%3D
#${setup_agent_password_encrypted_encoded}     kMJchf%2BGh22wC8RPNkgp%2FzKGn208QqoZQOJnLO6VP6NbeCfE0MwVo6uE%2FM2Gylm2VdhAhwqsprifu7%2Bnc8QnT8F96ymqat6u2gnnbmHJOIHqrsDTleoqAQrAnDx9uoKApXiv5flvaYba6IhBBuhdieTlj8ldSxyimR6LP3NlZOQ%3D
#
#${set_agent_pin}        090887
#${setup_agent_pin_encrypted}        dvM0pSYqkIJQ/hHxDDxbMeO+JXwemJCPgU7iW/bo/tPiONBo0x4KB4cwOITDtcMJKPcTnuziw/USywNeiqxKmE2JBv8hqTaHL71lDudLE5SS5saqlcJuUH5iO/M0iit3R1zskpCK7e/uj11QoUdB1TSWdxfoWofPirlCEJjO0kI=
#${setup_agent_pin_encrypted_utf8}           ifGe5HPTRkHymdPeqT%2F51ZJb8yE5UE%2Ff%2BObVbmxXxvkS92ksYmuisGrBgnpPpl4sHf9I8OTgOrBNxtCXrVG1EylDuimZGSXcOR%2Ft3kyze4bLwILeAXNehFiIvvkoQAbPgZXVOtlNE0X0NvXz8rHxjUxpVJcwjXzIu6MpomJSCRo%3D
#
#${setup_company_agent_account_id}   a123456789b123456789
#${setup_company_agent_password_encrypted}   gmR1qdAt49tU1J1mlheblfuukrYtVn4oNo0hvpm%2FeuQYanaGOehTpAlJuLFisNHOtlueZxwBf84C8PRM6L0MFt6aR1okM8GHVW7RgLYQK9geUwPw6PMyOt9HDrip3KMlpck4%2BSQwDlrE6BRsRdqXWbttWUadSIWSEaHmqx14dOY%3D
#
#${setup_datetime_format}         %Y-%m-%d
#
${setup_admin_client_id}                              99JMWO5NKPXWZKHB1FG2XUTPTJXUJ9DA
${setup_admin_client_secret}                          dlfrBCuKAQpsU2fCwcv4AdZ4WV350WB5prrIMNmfSLWhyQJfNas4YRGGDqT5dMPX
#${client_id_password_type}              JMETER67890123456789012345678901
#${client_secret_password_type}          A123456789012345678901234567890123456789012345678901234567890123
#${IP-ADDRESS}                                   100.1.1.2
#
#${setup_password_customer_encrypted}  ONIdnLAbo6hogTbBk5rW9eor4jL3+HQNxW3LY+kg9L2tfJ4eaaMpqGdLfQFmgsjjD/2UIzfz4CxwlgEuuSKvkpnAh/Ko0StTQgRLRx3/0tlRJPop+RSHzhkQjZV/8vzraq2FadEtYadmPfIt0K0pOLZr2d3jwCF73YvossLA5Mw=
#${setup_password_customer_encrypted_utf8}     ONIdnLAbo6hogTbBk5rW9eor4jL3%2BHQNxW3LY%2Bkg9L2tfJ4eaaMpqGdLfQFmgsjjD%2F2UIzfz4CxwlgEuuSKvkpnAh%2FKo0StTQgRLRx3%2F0tlRJPop%2BRSHzhkQjZV%2F8vzraq2FadEtYadmPfIt0K0pOLZr2d3jwCF73YvossLA5Mw%3D
#
#${setup_agent_username}     68cgsy8lmi
#${setup_agent_password_encrypted}    R5icapIYnXSVZyVaK5pdO7tEszIMzpHx9OOujUjFmclwV7LxmtABgd907f7TRFjlfph60E8KaNH7t%2BWdkjeR9Fj1kahiv6xIHY4AyFnJ6IZSOQRjUl3NgqaPeqmj%2BHs7kmk5W9QjsAsD%2FAfExAWXbD2ESpPiX6f1hMxkryg5x%2Bs%3D&client_id=JMETER67890123456789012345678901
#${setup_record_per_page}  50
#
#${setup_agent_password_rsa_encrypted}    DmquBzBmCK1U9IVyL/sLc4lO2P/fswkkUi8HnTRmKP55Q4W+hm0eEyo/UhNmdSpymqqO+cQeIl8v8Qt7kLuPvBNm+WEol5P9j8qAffJazlyMGyW/aOjGLP7gI8//bZ8+oi3nc/tZfQvxNSn0lxa8BRFdSENKpCdZh5rylgoxmhY=
#${setup_agent_pin_rsa_encrypted}     RUlBqMdRhMwZlj2WTqiwnr3aGVZS04xjTj4BP5ump1s2f5jXES9Wnw+JZTL7P9H9Demgm4UaoljUFvbug2MYvY+eiF/HsvJQMeqGdEJQ/0ywpW3aPNyiT1DAUgbqJRTWaTK3dYwJ7Gc5YEZm0yZYoyssPRN2MPdUy6i9XU53UYs=
#${setup_agent_wrong_password_rsa_encrypted}   ILiLam1LjnPmbnuix5uY9C9pEzK/YuKqLcR+oKVgQbtYdDuKimitOZf19jM7cUh1wFHeY5rDkieigWoJdCI4G+L+YCfszlcnT43xVk0VX4Hv7Xu9elNg/6kIzP0anKcVSmGVPTrgvOdk3zrxf/qjQVNhl6Tf1lKXSZvuY5TrUNk=
#${setup_agent_wrong_pin_rsa_encrypted}      a1bAsYVNRFGdyqPB1tm0GLgCfMUYoIoq/j2Uq1WtJ1Qb3RfyZozagrbIq2L8gDp1yTWVLweBeQjBpoZfwBFoe2gGSdx99AcA08fJAgkK1bbJ3ftqljGnWrIIiKc/nM/6XBpoLoDUIhemR3juzKlfVlxrV3+Ak5ZnisdlvlnR3+w=
#${setup_agent_wrong_password_encrypted}     dvM0pSYqkIJQ/hHxDDxbMeO+JXwemJCPgU7iW/bo/tPiONBo0x4KB4cwOITDtcMJKPcTnuziw/USywNeiqxKmE2JBv8hqTaHL71lDudLE5SS5saqlcJuUH5iO/M0iit3R1zskpCK7e/uj11QoUdB1TSWdxfoWofPirlCEJjO0kI=
#
#${setup_payment_pin_encrypted}      LnkEnvNFYXbc1fQeEHImcGk0qfXfH6RzLbkgXsOL9N3gbN9dfNCZNrUcWEKFVMu56BRAa6Sm/sVMTlgDm0phzavZeHAfUGwh+AWrugtEwGUb5+IW9zAIpGpmeiphjJW8AjcHYE+WtFOM+g6XgSDa50OVl93PDyOh6N7UUKiATis=
#
#${setup_device_id}     7MQYNIAVW504WEHR5L3F95J8KFSUALA3
#${setup-device_description}     Device description for jmeter testing
#
#${setup_currency_VND}     VND
#${setup_scope}     global
#${setup_company_agent_user_id}    1
#${setup_company_agent_user_type}    2
#${wallet_view_transaction_history_in_days}    90
#
#${param_not_used}    param_not_used
#${param_is_null}     param_is_null
#
##agent_type
#${setup_company_agent_type_id}      1
#${setup_company_agent_type_name}      Company
#
#${setup_channel_adapter_client_id}                              JMETER67890123456789012345678901
#${setup_channel_adapter_client_secret}                          A123456789012345678901234567890123456789012345678901234567890123
#
##SPI
#${role_legal_compliance_id}                  6
#${output_json_permissions_roles_response_path}    Robot/API/3_Prepare_data/By_JsonSchema/Permissions_Roles_Response.json
#${http_success_code}    200
#
##mock_url
#${pre_link_url}     ${mock_url}:4001/sof-card/verify-card
#${pre_sof_order_url}    ${mock_url}:4005/sof-card/pre-sof-order
#${link_url}   ${mock_url}:4002/sof-card/link-card
#${debit_url}    ${mock_url}:4011/sof-card/debit-url
#${credit_url}    ${mock_url}:4010/sof-card/credit-url
#${un_link_url}    ${mock_url}:4003/sof-card/unlink-card
#${bank_debit_url}    ${mock_url}:4444
#${bank_credit_url}    ${mock_url}:4444
#${SPI_timeout}    ${mock_url}:4546/timeout
#${SPI_get_otp_url}    ${mock_url}:4548
#${SPI_timeout}      ${mock_url}:4546/timeout
#${check_status_url}     ${mock_url}:4012
#${cancel_url}   ${mock_url}:4013
#${SPI_Timeout_for_delay_order}      ${mock_url}:4651/timeout/wait_delay_hours/valid/1
#${mock_api_gateway_get_header_url}     ${mock_url}:4650
#
#${http_status_OK}    200
#${http_status_BAD_REQUEST}    400
#${http_status_INTERNAL_SERVER_ERROR}    500
#
##inventory
#${inventory_success_code}    290000
#${inventory_success_message}    Success
