*** Settings ***
Resource          ../../3_prepare_data/by_api/imports.robot

*** Keywords ***
[Settlement_Configuration] - Verify search settlement configuration all
    [Arguments]     &{arg_dic}
    ${dic}  [200] API search settlement configurations     &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    ${datas}        get from dictionary     ${response.json()['data']}    settlement_configurations
    :FOR      ${data}       IN       @{datas}
    \  Dictionary Should Contain Key    ${data}    id
    \  Dictionary Should Contain Key    ${data}    currency
    \  Dictionary Should Contain Key    ${data}    service_group_id
    \  Dictionary Should Contain Key    ${data}    service_id
    \  Dictionary Should Contain Key    ${data}    is_deleted
    \  Dictionary Should Contain Key    ${data}    created_timestamp
    \  Dictionary Should Contain Key    ${data}    last_updated_timestamp


[Settlement_Configuration] - Verify search settlement configuration by id
    [Arguments]     &{arg_dic}
    ${dic}     [200] API search settlement configurations     &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    ${settlement_configurations}        get from dictionary     ${response.json()['data']}    settlement_configurations
    ${size}      get length       ${settlement_configurations}
    should be equal as integers     ${size}      1
    should be equal as strings  ${arg_dic.id}            ${settlement_configurations[0]['id']}
    should be equal as strings  VND            ${settlement_configurations[0]['currency']}
    should be equal as strings  ${arg_dic.service_group_id}            ${settlement_configurations[0]['service_group_id']}
    should be equal as strings  ${arg_dic.service_id}            ${settlement_configurations[0]['service_id']}


[Settlement_Configuration] - Verify search settlement configuration by is_deleted
    [Arguments]     &{arg_dic}
    ${dic}  [200] API search settlement configurations     &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    ${datas}        get from dictionary     ${response.json()['data']}    settlement_configurations
    :FOR      ${data}       IN       @{datas}
    \  should be equal as strings   False            ${data['is_deleted']}

[Settlement_Configuration] - Verify search settlement configuration by all criterial
    [Arguments]     &{arg_dic}
    ${dic}     [200] API search settlement configurations     &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings        ${response.json()['status']['code']}      success
    ${settlement_configurations}        get from dictionary     ${response.json()['data']}    settlement_configurations
    ${size}      get length       ${settlement_configurations}
    should be equal as integers     ${size}      1
    should be equal as strings  ${arg_dic.id}            ${settlement_configurations[0]['id']}
    should be equal as strings  VND            ${settlement_configurations[0]['currency']}
    should be equal as strings  ${arg_dic.service_group_id}            ${settlement_configurations[0]['service_group_id']}
    should be equal as strings  ${arg_dic.service_id}            ${settlement_configurations[0]['service_id']}
    should be equal as strings   False            ${settlement_configurations[0]['is_deleted']}

    ${page}        get from dictionary     ${response.json()['data']}    page
    should be equal as integers   1            ${page['total_pages']}
    should be equal as strings   False            ${page['has_next']}
    should be equal as strings   False            ${page['has_previous']}
    should be equal as integers   1            ${page['current_page']}
    should be equal as integers   1            ${page['total_elements']}

[Settlement_Configuration] - Create multiple settlement configuration
    [Arguments]  &{arg_dic}
    : FOR    ${INDEX}    IN RANGE    0    ${arg_dic.count}
    \   [Payment][Test][200] - System user create settlement configuration      &{arg_dic}
    ${dic}  [200] API search settlement configurations   &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    ${page}        get from dictionary     ${response.json()['data']}    page
    should be equal as strings   False            ${page['has_next']}
    should be equal as strings   False            ${page['has_previous']}
    should be equal as integers   1            ${page['current_page']}
    should be equal as integers   1            ${page['total_pages']}

[Settlement_Configuration] - Verify search settlement configuration by ID and verify is_deleted
    [Arguments]     &{arg_dic}
    ${dic}     [200] API search settlement configurations     &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    ${settlement_configurations}        get from dictionary     ${response.json()['data']}    settlement_configurations
    ${size}      get length       ${settlement_configurations}
    should be equal as integers     ${size}      1
    should be equal as strings  ${arg_dic.id}            ${settlement_configurations[0]['id']}