*** Settings ***
Resource          ../../3_prepare_data/by_api/imports.robot
Resource          ../../2_api/2_use_case-deprecated/imports.robot
Resource          ../../ami-operation-portal/keywords/common/common_web_keywords.robot

*** Keywords ***
[Payment][Suite] Save service_id and service_name for currency
    [Arguments]    &{arg_dic}
    set suite variable      ${suite_service_id_${arg_dic.currency}}            ${arg_dic.service_id}
    set suite variable      ${suite_service_name_${arg_dic.currency}}          ${arg_dic.service_name}

[Settlement][Payment][Fail] - Create pending settlement fail
    [Arguments]    &{arg_dic}
    ${random_string}    generate random string    5    [LETTERS]
    ${amount}           generate random string    3     123456789
    ${reconciliation_id}          generate random string    9     123456789
    ${current_date}               Get Current Date	    UTC       result_format=%Y-%m-%dT%H:%M:%SZ
    ${current_date_start_date}    Get Current Date	    UTC         result_format=%Y-%m-%dT00:00:00Z
    ${current_date_end_date}      Get Current Date	    UTC         result_format=%Y-%m-%dT23:59:59Z
    ${tomorrow_date}=             add time to date    ${current_date}       1 days     result_format=%Y-%m-%dT00:00:00Z

    set test variable    ${test_settlement_description}                 ${test_case_id}_description_settlement_${random_string}
    set test variable    ${test_created_by}                             LOCAL
    set test variable    ${test_settlement_amount}                      ${amount}000
    set test variable    ${test_due_date}                               ${tomorrow_date}
    set test variable    ${test_reconciliation_id}                      ${reconciliation_id}
    set test variable    ${test_reconciliation_from_date}               ${current_date_start_date}
    set test variable    ${test_reconciliation_to_date}                 ${current_date_end_date}

    ${dic}    [Fail] API create pending settlement    access_token=${suite_setup_admin_access_token}     description=${test_settlement_description}  amount=${test_settlement_amount}    payer_user_id=${arg_dic.payer_user_id}  payer_user_type=${arg_dic.payer_user_type}    payer_sof_id=${arg_dic.payer_sof_id}    payer_sof_type_id=2     payee_user_id=${arg_dic.payee_user_id}  payee_user_type=${arg_dic.payee_user_type}    payee_sof_id=${arg_dic.payee_sof_id}    payee_sof_type_id=2     created_by=${test_created_by}    currency=${arg_dic.currency}  due_date=${test_due_date}    reconciliation_id=${test_reconciliation_id}    reconciliation_from_date=${test_reconciliation_from_date}  reconciliation_to_date=${test_reconciliation_to_date}    unsettled_transactions=${arg_dic.unsettled_transactions}   status_code=${arg_dic.status_code}    status_message=${arg_dic.status_message}

[Payment][Test][Fail] - Update pending settlement fail
    [Arguments]    &{arg_dic}
    ${current_date}               Get Current Date	    UTC       result_format=%Y-%m-%dT%H:%M:%SZ
    ${after_tomorrow_date}=             add time to date    ${current_date}       2 days     result_format=%Y-%m-%dT00:00:00Z
    set test variable    ${test_updated_due_date}                               ${after_tomorrow_date}
    [Fail] API update pending settlement       access_token=${arg_dic.access_token}         settlement_id=${arg_dic.settlement_id}       due_date=${test_updated_due_date}      status_code=${arg_dic.status_code}    status_message=${arg_dic.status_message}

[Settlement][Report] - Search pending settlement by settlement_id
    [Arguments]     ${settlement_id}
    [Report][Prepare] - Search pending settlement - body
        ...     output=search_order_body
        ...     $.settlement_id=${settlement_id}
    [Report][200] - Search pending settlement
        ...     headers=${SUITE_ADMIN_HEADERS}
        ...     body=${search_order_body}
    set test variable       ${search_settlement_id}        ${settlement_id}

[Settlement][Report][Verify] - Verify pending settlement detail return correctly with full data
    #verify_settlement_data
    REST.integer    $.data.settlements[0].id    ${test_settlement_id}
    REST.string     $.data.settlements[0].description    ${test_settlement_description}
    REST.number     $.data.settlements[0].amount    ${test_settlement_amount}
    REST.string     $.data.settlements[0].currency    VND
    REST.string     $.data.settlements[0].status    CREATED
    REST.string     $.data.settlements[0].created_by    LOCAL
    REST.string     $.data.settlements[0].due_date    ${test_due_date}
    #verify_payer_data
    REST.integer    $.data.settlements[0].payer.id    ${suite_setup_agent_id}
    REST.string     $.data.settlements[0].payer.name    ${suite_setup_agent_first_name} ${suite_setup_agent_last_name}
    REST.integer    $.data.settlements[0].payer.user_type.id    2
    REST.string     $.data.settlements[0].payer.user_type.name    agent
    REST.integer    $.data.settlements[0].payer.sof.id    ${suite_setup_sof_cash_id_in_VND}
    REST.integer    $.data.settlements[0].payer.sof.type_id    2
    #verify_payee_data
    REST.integer    $.data.settlements[0].payee.id    ${suite_customer_id}
    REST.string     $.data.settlements[0].payee.name    ${suite_customer_first_name} ${suite_customer_last_name}
    REST.integer    $.data.settlements[0].payee.user_type.id    1
    REST.string     $.data.settlements[0].payee.user_type.name    customer
    REST.integer    $.data.settlements[0].payee.sof.id    ${suite_customer_sof_cash_id_in_VND}
    REST.integer    $.data.settlements[0].payee.sof.type_id    2
    #verify_created_user_data
    REST.integer    $.data.settlements[0].created_user.id    ${setup_admin_id}
    REST.string     $.data.settlements[0].created_user.name    ${setup_admin_username}
    REST.integer    $.data.settlements[0].created_user.user_type.id    ${setup_admin_user_type_id}
    REST.string     $.data.settlements[0].created_user.user_type.name    system-user
    #verify_last_updated_user_data
    REST.integer    $.data.settlements[0].last_updated_user.id    ${setup_admin_id}
    REST.string     $.data.settlements[0].last_updated_user.name    ${setup_admin_username}
    REST.integer    $.data.settlements[0].last_updated_user.user_type.id    ${setup_admin_user_type_id}
    REST.string     $.data.settlements[0].last_updated_user.user_type.name    system-user
    #verify_created_and_last_updated_timestamp
    ${created_timestamp}        rest extract     $.data.settlements[0].created_timestamp
    ${last_updated_timestamp}        rest extract     $.data.settlements[0].last_updated_timestamp
    should be equal as strings      ${created_timestamp}        ${last_updated_timestamp}
    #verify_reconciliation_data
    REST.integer    $.data.settlements[0].reconciliation_id     ${test_reconciliation_id}
    REST.string     $.data.settlements[0].reconciliation_from_date     ${test_reconciliation_from_date}
    REST.string     $.data.settlements[0].reconciliation_to_date     ${test_reconciliation_to_date}

[Settlement][Report][Verify] - Verify pending settlement detail return correctly with only required data
    #verify_settlement_data
    REST.integer    $.data.settlements[0].id    ${test_settlement_id}
    REST.string     $.data.settlements[0].description    ${test_settlement_description}
    REST.number     $.data.settlements[0].amount    ${test_settlement_amount}
    REST.string     $.data.settlements[0].currency    VND
    REST.string     $.data.settlements[0].status    CREATED
    REST.string     $.data.settlements[0].created_by    LOCAL
    REST.string     $.data.settlements[0].due_date    ${test_due_date}
    #verify_payer_data
    REST.integer    $.data.settlements[0].payer.id    ${suite_setup_agent_id}
    REST.string     $.data.settlements[0].payer.name    ${suite_setup_agent_first_name} ${suite_setup_agent_last_name}
    REST.integer    $.data.settlements[0].payer.user_type.id    2
    REST.string     $.data.settlements[0].payer.user_type.name    agent
    REST.integer    $.data.settlements[0].payer.sof.id    ${suite_setup_sof_cash_id_in_VND}
    REST.integer    $.data.settlements[0].payer.sof.type_id    2
    #verify_payee_data
    REST.integer    $.data.settlements[0].payee.id    ${suite_customer_id}
    REST.string     $.data.settlements[0].payee.name    ${suite_customer_first_name} ${suite_customer_last_name}
    REST.integer    $.data.settlements[0].payee.user_type.id    1
    REST.string     $.data.settlements[0].payee.user_type.name    customer
    REST.integer    $.data.settlements[0].payee.sof.id    ${suite_customer_sof_cash_id_in_VND}
    REST.integer    $.data.settlements[0].payee.sof.type_id    2
    #verify_created_user_data
    REST.integer    $.data.settlements[0].created_user.id    ${setup_admin_id}
    REST.string     $.data.settlements[0].created_user.name    ${setup_admin_username}
    REST.integer    $.data.settlements[0].created_user.user_type.id    ${setup_admin_user_type_id}
    REST.string     $.data.settlements[0].created_user.user_type.name    system-user
    #verify_last_updated_user_data
    REST.integer    $.data.settlements[0].last_updated_user.id    ${setup_admin_id}
    REST.string     $.data.settlements[0].last_updated_user.name    ${setup_admin_username}
    REST.integer    $.data.settlements[0].last_updated_user.user_type.id    ${setup_admin_user_type_id}
    REST.string     $.data.settlements[0].last_updated_user.user_type.name    system-user
    #verify_created_and_last_updated_timestamp
    ${created_timestamp}        rest extract     $.data.settlements[0].created_timestamp
    ${last_updated_timestamp}        rest extract     $.data.settlements[0].last_updated_timestamp
    should be equal as strings      ${created_timestamp}        ${last_updated_timestamp}
    #verify_reconciliation_data
    REST.null       $.data.settlements[0].reconciliation_id
    REST.null       $.data.settlements[0].reconciliation_from_date
    REST.null       $.data.settlements[0].reconciliation_to_date
    #verify_unsettled_transactions_data
    ${unsettled_transactions}   rest extract       $.data.settlements[0].unsettled_transactions
    should be equal as strings      ${unsettled_transactions}       []

[Settlement][Report][Verify] - Verify last updated timestamp has changed
    [Arguments]     &{arg_dic}
    [Common] - Verify datetime is changed       datetime_before=${arg_dic.datetime_before}   datetime_after=${arg_dic.datetime_after}

[Settlement][Report][Verify] - Verify last updated user
    [Arguments]     &{arg_dic}
    REST.integer    $.data.settlements[0].last_updated_user.id           ${arg_dic.last_updated_user_id}
    REST.string     $.data.settlements[0].last_updated_user.name         ${arg_dic.last_updated_user_name}
    REST.integer    $.data.settlements[0].last_updated_user.user_type.id      ${setup_admin_user_type_id}
    REST.string     $.data.settlements[0].last_updated_user.user_type.name    system-user

[Settlement][Report][Verify] - Verify due_date
    [Arguments]     &{arg_dic}
    REST.string     $.data.settlements[0].due_date           ${arg_dic.due_date}

[Settlement][Report][Verify] - Verify status of settlement
    [Arguments]    &{arg_dic}
    REST.string     $.data.settlements[0].status         ${arg_dic.status}

[Settlement][Report][Verify] - Verify empty list return
    ${data}         rest extract    $.data.settlements
    ${size}=        get length     ${data}
    should be equal as integers         ${size}     0
    REST.integer    $.data.page.total_elements
    REST.null       $.data.page.has_next
    REST.null       $.data.page.has_previous
    REST.null       $.data.page.current_page
    REST.integer    $.data.page.total_pages

[Settlement][Report][Extract] - Extract field from response
    [Arguments]     ${output}=test_value    &{arg_dic}
    ${value}        rest extract     $.data.settlements[0].${arg_dic.path}
    [Common] - Set variable       name=${output}      value=${value}

[Settlement][Report][Extract] - Extract all data from response
    [Arguments]     ${output}=test_data
    ${value}        rest extract     $.data.settlements
    [Common] - Set variable       name=${output}      value=${value}