*** Settings ***
Resource          ../../3_prepare_data/by_api/imports.robot
Resource          ../../3_prepare_data/imports.robot

*** Keywords ***
Sof service balance limitation - Create sof service balance limitation
    [Arguments]    ${output}=test_balance_limitation_id   &{arg_dic}
    ${create_limitation_dic}     create dictionary      access_token=${arg_dic.access_token}    maximum_amount=${arg_dic.maximum_amount}       minimum_amount=${arg_dic.minimum_amount}    service_id=${arg_dic.service_id}       sof_id=${arg_dic.sof_id}
    ${dic}      [200] API create sof service balance limitation     ${create_limitation_dic}
    ${response}=    get from dictionary    ${dic}   response
    ${balance_limitation_id}=  get from dictionary   ${dic}   balance_limitation_id
    [Common] - Set variable    name=${output}   value=${balance_limitation_id}

Sof service balance limitation - Verify sof service balance limitation response is correct with maximum amount is null
    [Arguments]  &{args}
    [Report] Search sof service balance limitation              access_token=${args.access_token}    service_id=${args.service_id}
     REST.integer    response status    200
     REST.string   $.status.code     success
     REST.string   $.status.message     Success
     REST.integer   $.data.sof_services_balance_limitation[0].id       ${args.id}
     REST.integer   $.data.sof_services_balance_limitation[0].service.service_id       ${args.service_id}
     REST.integer   $.data.sof_services_balance_limitation[0].sof_id       ${args.sof_id}
     REST.Null   $.data.sof_services_balance_limitation[0].maximum_amount

Sof service balance limitation - Create sof service balance limitation with maximum amount is null
    [Arguments]   ${service_id}       ${sof_id}    ${access_token}=${suite_admin_access_token}      ${maximum_amount}=${param_not_used}     ${output}=test_balance_limitation_id
    ${create_limitation_dic}     create dictionary      access_token=${access_token}    service_id=${service_id}   sof_id=${sof_id}     maximum_amount=${maximum_amount}
    ${return_dic}     [200] API create sof service balance limitation     ${create_limitation_dic}
    [Common] - Set variable     name=${output}    value=${return_dic.balance_limitation_id}

Sof service balance limitation - User create sof service balance limitation and get error
    [Arguments]     &{arg_dic}
    [Fail] User create sof service balance limitation and get error     &{arg_dic}