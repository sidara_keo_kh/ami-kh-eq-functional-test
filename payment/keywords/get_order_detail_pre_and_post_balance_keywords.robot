*** Settings ***
Resource          ../../3_prepare_data/by_api/imports.robot


*** Keywords ***
Order Detail - Get pre_balance and post palance of initiator, payer and payee by api
    ${test_initiator_pre_balance}   rest extract     $.data.order_balance_movements[0].pre_balance
    ${test_initiator_post_balance}   rest extract     $.data.order_balance_movements[1].post_balance
    ${test_payer_pre_balance}   rest extract     $.data.order_balance_movements[0].pre_balance
    ${test_payer_post_balance}   rest extract     $.data.order_balance_movements[1].post_balance
    ${test_payee_pre_balance}   rest extract     $.data.order_balance_movements[2].pre_balance
    ${test_payee_post_balance}   rest extract     $.data.order_balance_movements[2].post_balance
    set test variable   ${test_initiator_pre_balance}   ${test_initiator_pre_balance}
    set test variable   ${test_initiator_post_balance}  ${test_initiator_post_balance}
    set test variable   ${test_payer_pre_balance}   ${test_payer_pre_balance}
    set test variable   ${test_payer_post_balance}   ${test_payer_post_balance}
    set test variable   ${test_payee_pre_balance}   ${test_payee_pre_balance}
    set test variable   ${test_payee_post_balance}   ${test_payee_post_balance}

Order Detail - Verify pre_balance and post balance of '${actors}' actors should be return
    ${show_initiator_balance}=      Run Keyword And Return Status    should contain     ${actors}    initiator
    ${show_payer_balance}=      Run Keyword And Return Status    should contain     ${actors}    payer
    ${show_payee_balance}=      Run Keyword And Return Status    should contain     ${actors}    payee
    run keyword if     ${show_initiator_balance}   should not be equal as strings       ${test_initiator_pre_balance}   ${NONE}
                        ...     ELSE    should be equal as strings   ${test_initiator_pre_balance}  ${NONE}

    run keyword if     ${show_initiator_balance}       should not be equal as strings    ${test_initiator_post_balance}   ${NONE}
                        ...     ELSE    should be equal as strings     ${test_initiator_post_balance}   ${NONE}

    run keyword if     ${show_payer_balance}           should not be equal as strings    ${test_payer_pre_balance}   ${NONE}
                        ...     ELSE    should be equal as strings     ${test_payer_pre_balance}    ${NONE}

    run keyword if     ${show_payer_balance}           should not be equal as strings    ${test_payer_post_balance}   ${NONE}
                        ...     ELSE    should be equal as strings     ${test_payer_post_balance}   ${NONE}

    run keyword if     ${show_payee_balance}           should not be equal as strings    ${test_payee_pre_balance}   ${NONE}
                        ...     ELSE    should be equal as strings     ${test_payee_pre_balance}       ${NONE}

    run keyword if     ${show_payee_balance}           should not be equal as strings    ${test_payee_post_balance}   ${NONE}
                        ...     ELSE    should be equal as strings    ${test_payee_post_balance}    ${NONE}