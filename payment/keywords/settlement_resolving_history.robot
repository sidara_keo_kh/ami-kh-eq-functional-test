*** Settings ***
Resource          ../../3_prepare_data/by_api/imports.robot

*** Keywords ***
[Settlement_Resolving_History] - Verify settlement status
    [Arguments]     &{arg_dic}
    [Settlement][Report] - Search pending settlement by settlement_id       ${test_settlement_id}
    [Settlement][Report][Extract] - Extract field from response     path=status      output=status
#    ${dic}  [Settlement][Report] - Search pending settlement by settlement_id     ${arg_dic.id}
#    ${response}    get from dictionary     ${dic}    response
#    ${status}       set variable     ${response.json()['data']['settlements'][0]['status']}
    should be equal as strings      ${status}       ${arg_dic.expected_status}

[Settlement_Resolving_History] - Get last updated timestamp on settlement
    [Arguments]    ${id}    ${output}
#    ${dic}  [Settlement][Report] - Search pending settlement by settlement_id     ${id}
#    ${response}    get from dictionary     ${dic}    response
#    ${last_updated_timestamp}       set variable     ${response.json()['data']['settlements'][0]['last_updated_timestamp']}
    [Settlement][Report] - Search pending settlement by settlement_id       ${id}
    [Settlement][Report][Extract] - Extract field from response     path=last_updated_timestamp      output=${output}
#    [Common] - Set variable   name=${output}   value=${last_updated_timestamp}

[Settlement_Resolving_History][Report] - Verify search settlement status by id
    [Arguments]     &{arg_dic}
    ${dic}    [200] API search pending settlement    access_token=${suite_setup_admin_access_token}    settlement_id=${arg_dic.settlement_id}
    should be equal as strings    ${dic.status_0}    ${arg_dic.settlement_status}

[Settlement_Resolving_History][Report] - Verify search order status by id
    [Arguments]     &{arg_dic}
    set to dictionary       ${arg_dic}      access_token        ${suite_setup_admin_access_token}
    ${dic}          [200] API search payment orders with Arguments = dic        ${arg_dic}
    should be equal as strings     ${dic.response.json()['data']['orders'][0]['state']}    ${arg_dic.expected_state}
    should be equal as integers    ${dic.response.json()['data']['orders'][0]['status']}    ${arg_dic.expected_status}

[Settlement_Resolving_History][Report] - Verify search settlement resolving histories by ID
    [Arguments]     &{arg_dic}
    [Report][200] - Search settlement resolving histories    access_token=${arg_dic.access_token}    id=${arg_dic.expected_id}
    REST.integer    $.data.settlement_resolving_histories[0].id     ${arg_dic.expected_id}
    REST.integer    $.data.settlement_resolving_histories[0].settlement_id     ${arg_dic.expected_settlement_id}
    ${test_settlement_order_id}        rest extract     $.data.settlement_resolving_histories[0].order_id
    set test variable       ${test_settlement_order_id}     ${test_settlement_order_id}

[Settlement_Resolving_History] - Verify settlement last updated user id
    [Arguments]     &{arg_dic}
#    ${dic}  [Settlement][Report] - Search pending settlement by settlement_id     ${arg_dic.id}
    [Settlement][Report] - Search pending settlement by settlement_id       ${test_settlement_id}
    [Settlement][Report][Extract] - Extract field from response     path=last_updated_user.id      output=last_updated_user_id
    should be equal as strings      ${last_updated_user_id}      ${arg_dic.expected_last_updated_user_id}
#    ${dic.last_updated_user_id_0}