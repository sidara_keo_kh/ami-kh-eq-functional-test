*** Settings ***
Resource          ../../3_prepare_data/by_api/imports.robot

*** Keywords ***
[200][API] [Execute order] Verify API return success
    ${response_code}       get from dictionary     ${response}      response
    should be equal as strings        ${response_code}      <Response [200]>

[Fail][API][Execute order] Verify error message for suspend sof of order with actor type is payee/payer/initiator
    ${response}    get from dictionary     ${test_result_dic}    response
    should be equal as strings        ${response.json()['status']['message']}      Cannot perform transaction due to suspended SOF

[Fail][API][Execute order] Verify error message for suspend sof of order with actor type is specific ID
    ${response}    get from dictionary     ${test_result_dic}    response
    should be equal as strings        ${response.json()['status']['message']}      Service Config Error

[Fail][API][Execute order] Verify error message for suspend sof of order with actor type is beneficiary
    ${response}    get from dictionary     ${test_result_dic}    response
    should be equal as strings        ${response.json()['status']['message']}      Service Beneficiary Error

[200][API][Get order detail] Verify artifact order detail should return response with requestor information
    ${dic}  [200] API get order detail
    ...     ${test_agent_access_token}
    ...     ${test_order_id}
    should be equal as strings  ${test_requestor_id}            ${dic.requestor_id}
    should be equal as strings  agent                           ${dic.requestor_user_type}
    should be equal as strings  ${test_requestor_sof_id}        ${dic.requestor_sof_id}
    should be equal as strings  2                               ${dic.requestor_sof_type}

[200][API][Get order detail] Verify normal order detail should return response with requestor information
    ${dic}  [200] API get order detail
    ...     ${test_agent_access_token}
    ...     ${test_order_id}
    should be equal as strings  ${test_requestor_id}            ${dic.requestor_id}
    should be equal as strings  agent                           ${dic.requestor_user_type}
    should be equal as strings  ${test_requestor_sof_id}        ${dic.requestor_sof_id}
    should be equal as strings  2                               ${dic.requestor_sof_type}

[200][API][Get order detail] Verify trust order detail should return response with requestor information
    ${dic}  [200] API get order detail
    ...     ${test_agent_access_token}
    ...     ${test_order_id}
    should be equal as strings  ${test_trusted_id}              ${dic.requestor_id}
    should be equal as strings  agent                           ${dic.requestor_user_type}
    should be equal as strings  ${test_trusted_sof_id}          ${dic.requestor_sof_id}
    should be equal as strings  2                               ${dic.requestor_sof_type}

[200][API][Get order detail] Verify adjustment order detail should return response with requestor information
    ${dic}  [200] API get order detail
    ...     ${test_agent_access_token}
    ...     ${test_order_id}
    should be equal as strings  ${test_initiator_id}              ${dic.requestor_id}
    should be equal as strings  agent                           ${dic.requestor_user_type}
    should be equal as strings  ${test_initiator_sof_id}          ${dic.requestor_sof_id}
    should be equal as strings  2                               ${dic.requestor_sof_type}

[200][API][Get order detail] Verify cashout order detail should return response with requestor information
    ${dic}  [200] API get order detail
    ...     ${test_water_agent_access_token}
    ...     ${test_order_id}
    should be equal as strings  ${test_water_agent_id}              ${dic.requestor_id}
    should be equal as strings  agent                           ${dic.requestor_user_type}
    should be equal as strings  ${test_water_sof_cash_id}          ${dic.requestor_sof_id}
    should be equal as strings  2                               ${dic.requestor_sof_type}

[Sof_Service_Balance_Limitation][Test] Prepare sof service balance limitation
    [Arguments]     &{arg_dic}
    ${create_limitation_dic}     create dictionary      access_token=${arg_dic.access_token}    maximum_amount=${arg_dic.maximum_amount}       minimum_amount=${arg_dic.minimum_amount}    service_id=${arg_dic.service_id}       sof_id=${arg_dic.sof_id}
    ${dic}      [200] API create sof service balance limitation     ${create_limitation_dic}
    ${response}=    get from dictionary    ${dic}   response
    ${balance_limitation_id}=  get from dictionary   ${dic}   balance_limitation_id
    set test variable       ${test_balance_limitation_id}     ${balance_limitation_id}
