*** Settings ***
Library     DebugLibrary

Resource          ../../payment/keywords/get_order_detail_pre_and_post_balance_keywords.robot

Suite Setup     [Suite] Setup

*** Keywords ***
[Suite] Setup
    [System_User][200][Prepare] - Create system user
#    ...     output=SUITE_ADMIN_HEADERS
    ...     output_user_id=suite_admin_user_id
    ...     output_user_name=suite_admin_username
    ...     output_password=suite_admin_password
    [Agent][Suite][200] - log in as company agent
    [Payment][Suite][200] - add money for company agent in "USD" currency
    [Payment][Suite][200] - create normal service structure in "USD" currency
    [Agent][Suite][200] - Create "Water" agent with currency
    ...     currency=USD
    [Payment][Suite][200] - company agent fund in for "Water" agent
    [Customer][Suite][200] - Create customer with currency
    ...     currency=USD
    [Agent][200][Prepare] - Create an agent with sof_cash
    ...     currency=USD
    ...     output_agent_id=suite_agent_id_2
    ...     output_agent_type_id=suite_agent_type_id_2
    ...     output_sof_cash_id=suite_agent_sof_cash_id_2
    [Suite][200] Create bank for 'USD'
    [Suite][200] link bank for water agent in "USD" currency

*** Test Cases ***
TC_EQP_13092 - [Payment] Separate get order detail response value - get by agent or customer
    [Tags]      EQTR-17538      2019-06-TH-Optimus
    TC_EQP_13092 - Step 1. Prepare normal order with initiator = payer and debit payer 2 times in balance movement
    TC_EQP_13092 - Step 2. Payment get order detail by agent
    TC_EQP_13092 - Step 3. Payment get order detail by customer

*** Keywords ***
TC_EQP_13092 - Step 1. Prepare normal order with initiator = payer and debit payer 2 times in balance movement
    ${VAR_LIST_FEE_TIER_DATA}       set variable    "a":{"type_first":"Flat value","amount_first":10}
    ${VAR_BALANCE_DISTRIBUTIONS}       set variable         {"action_type":"debit","actor_type":"Payer","sof_type_id":2,"specific_sof":null,"amount_type":"Amount","rate":null},{"action_type":"debit","actor_type":"Specific ID","specific_actor_id":${suite_water_agent_id},"sof_type_id":2,"specific_sof":"${suite_water_sof_cash_id_USD}","amount_type":"A","rate":null},{"action_type":"credit","actor_type":"Payee","sof_type_id":2,"specific_sof":null,"amount_type":"Amount","rate":null}
    [Payment][200][Prepare] - Create service structure     USD     output_service_id=test_service_id    output_service_name=test_service_name     balance_distributions=${VAR_BALANCE_DISTRIBUTIONS}    list_fee_tier_data=${VAR_LIST_FEE_TIER_DATA}
    [Payment][Test][200] - create and execute normal order        access_token=${suite_water_agent_access_token}    product_service_id=${test_service_id}   product_service_name=${test_service_name}      amount=100

TC_EQP_13092 - Step 2. Payment get order detail by agent
    [Payment][200][Prepare] - Get order detail      access_token=${suite_water_agent_access_token}    order_id=${test_normal_order_id}
    Order Detail - Get pre_balance and post palance of initiator, payer and payee by api
    Order Detail - Verify pre_balance and post balance of 'payer,initiator' actors should be return

TC_EQP_13092 - Step 3. Payment get order detail by customer
    [Payment][200][Prepare] - Get order detail      access_token=${suite_customer_access_token}    order_id=${test_normal_order_id}
    Order Detail - Get pre_balance and post palance of initiator, payer and payee by api
    Order Detail - Verify pre_balance and post balance of 'payee' actors should be return

*** Test Cases ***
TC_EQP_13093 - [Payment] Separate get order detail response value - get by system user
    [Tags]      EQTR-17538      2019-06-TH-Optimus
    TC_EQP_13093 - Step 1. Prepare normal order with initiator = payer and debit payer 2 times in balance movement
    TC_EQP_13093 - Step 2. Payment get order detail by system user

*** Keywords ***
TC_EQP_13093 - Step 1. Prepare normal order with initiator = payer and debit payer 2 times in balance movement
    ${ext_transaction_id}=  generate random string  5   1357924680
    ${VAR_LIST_FEE_TIER_DATA}       set variable    "a":{"type_first":"Flat value","amount_first":10}
    ${VAR_BALANCE_DISTRIBUTIONS}       set variable         {"action_type":"debit","actor_type":"Payer","sof_type_id":2,"specific_sof":null,"amount_type":"Amount","rate":null},{"action_type":"debit","actor_type":"Specific ID","specific_actor_id":${suite_water_agent_id},"sof_type_id":2,"specific_sof":"${suite_water_sof_cash_id_USD}","amount_type":"A","rate":null},{"action_type":"credit","actor_type":"Payee","sof_type_id":2,"specific_sof":null,"amount_type":"Amount","rate":null}
    [Payment][200][Prepare] - Create service structure for cancellation     USD     output_service_id=test_service_id    output_service_name=test_service_name     balance_distributions=${VAR_BALANCE_DISTRIBUTIONS}    list_fee_tier_data=${VAR_LIST_FEE_TIER_DATA}
    [Payment][Test][200] - create and execute normal order        access_token=${suite_water_agent_access_token}    product_service_id=${test_service_id}   product_service_name=${test_service_name}      amount=100
    [Payment][Prepare] - Cancel normal order - body   output=cancel_request_body   access_token=${suite_admin_access_token}    ref_order_id=${test_normal_order_id}      payer_user_ref_type=access token     payer_user_ref_value=${suite_customer_access_token}    ext_transaction_id=${ext_transaction_id}
    [Payment][200] - Admin cancel normal order      ${SUITE_ADMIN_HEADERS}    ${cancel_request_body}    ${test_normal_order_id}
    ${execute_order_dic}     create dictionary      access_token=${suite_admin_access_token}    order_id=${test_cancel_order_id}
    [200] API payment execute order     ${execute_order_dic}

TC_EQP_13093 - Step 2. Payment get order detail by system user
    [Payment][200][Prepare] - Get order detail      access_token=${suite_admin_access_token}    order_id=${test_cancel_order_id}
    Order Detail - Get pre_balance and post palance of initiator, payer and payee by api
    Order Detail - Verify pre_balance and post balance of 'payee,payer,initiator' actors should be return
