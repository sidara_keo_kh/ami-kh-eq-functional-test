*** Settings ***
Resource          ../../payment/keywords/payment.robot

#Suite Setup       run keywords
#                  ...     [System_User][Suite][200] - login as setup system user
#                  ...       [Suite][200] Prepare an agent with sof_cash for currency "USD"
#                  ...       [Suite][200] API add service group
#Default Tags      robot     API     sof_cash

#robot -d results payment/testcases/mmpayment.robot
*** Test Cases ***
TC_EQP_11060 - [API][200] Create normal order with requestor is agent
    [Tags]      API    success      2018-23-VN-SAVIORS
    [System_User][Suite][200] - login as setup system user
#    [Suite][200] Prepare an agent with sof_cash for currency "USD"
#    [Suite][200] API add service group
#    [Test] Prepare payer and payee sof cash
#    [Test] Prepare requestor sof cash
#    [Test][200] Prepare service structure from service name without balance distribution
#    [Test][200] API create balance distribution with actor type of credit is payee
#    [Payment][Test][200] - create normal order with requestor
#    [200][API][Get order detail] Verify normal order detail should return response with requestor information