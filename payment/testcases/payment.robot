*** Settings ***
Resource          ../../payment/keywords/payment.robot

Suite Setup       run keywords
                  ...     [System_User][Suite][200] - login as setup system user
                  ...       [Suite][200] Prepare an agent with sof_cash for currency "USD"
                  ...       [Suite][200] API add service group
Default Tags      robot     API     sof_cash

*** Test Cases ***
TC_EQP_10306 - [Fail] Execute artifact order with suspend sof and action type is debit
    [Tags]      API    success      2018-23-VN-SAVIORS
    [Test][200] Prepare service structure from service name without balance distribution
    [Test][200] API create balance distribution with actor type of debit is payee
    [Prepare] - Create an artifact order with suspend sof
    [Test][Fail]Call Execute Order With Security
    [Fail][API][Execute order] Verify error message for suspend sof of order with actor type is payee/payer/initiator

TC_EQP_10307 - [Fail] Execute artifact order with suspend sof and action type is credit
    [Tags]      API    success      2018-23-VN-SAVIORS
    [Test][200] Prepare service structure from service name without balance distribution
    [Test][200] API create balance distribution with actor type of credit is payee
    [Prepare] - Create an artifact order with suspend sof
    [Test][Fail]Call Execute Order With Security
    [Fail][API][Execute order] Verify error message for suspend sof of order with actor type is payee/payer/initiator

TC_EQP_10308 - [200] Execute normal order with suspend sof and action type is credit
    [Tags]      API    success      2018-23-VN-SAVIORS
    [Test] Prepare payer and payee sof cash
    [200] API suspend sof cash
    [Test][200] Prepare service structure from service name without balance distribution
    [Test][200] API create balance distribution with actor type of credit is payee
    [Payment][Test][200] - create normal order for payer and payee then execute

TC_EQP_10309 - [Fail] Execute normal order with suspend sof and action type is debit
    [Tags]      API    success      2018-23-VN-SAVIORS
    [Test] Prepare payer and payee sof cash
    [200] API suspend sof cash
    [Test][200] Prepare service structure from service name without balance distribution
    [Test][200] API create balance distribution with actor type of debit is payee
    [Test][Fail] Create normal order for payer and payee then execute
    [Fail][API][Execute order] Verify error message for suspend sof of order with actor type is payee/payer/initiator

TC_EQP_10310 - [200] Execute BA order with suspend sof cash
    [Tags]      API    fail      2018-23-VN-SAVIORS
    [Test] Prepare payer and payee and initiator sof cash
    [200] API suspend sof cash
    [Test][200] Prepare service structure from service name
    [Test][200] create balance adjustment then execute

TC_EQP_10311 - [Fail] Execute artifact order with suspend sof and actor type is Specific ID
    [Tags]      API    success      2018-23-VN-SAVIORS
    [Test] Prepare specific ID sof cash
    [Test][200] Prepare service structure from service name without balance distribution
    [Test][200] API create balance distribution with actor type is specific
    [Prepare] - Create an artifact order with suspend sof of specific ID
    [Test][Fail]Call Execute Order With Security
    [Fail][API][Execute order] Verify error message for suspend sof of order with actor type is specific ID

TC_EQP_10313 - [Fail] Execute normal order with suspend sof and actor type is beneficiary
    [Tags]      API    success      2018-23-VN-SAVIORS
    [Test] Prepare beneficiary and payee sof cash
    [Test][200] Prepare service structure from service name without balance distribution
    [Test][200] API create balance distribution with action type is credit and actor type is beneficiary
    [Prepare] - Create an artifact order with suspend sof of beneficiary
    [Test][Fail]Call Execute Order With Security
    [Fail][API][Execute order] Verify error message for suspend sof of order with actor type is beneficiary

TC_EQP_11064 - [API][200] Create trust order with requestor
    [Test] Prepare truster and trusted and payee sof cash
    [Trust_Management][Test][200] generate trust token between truster and trusted agent    access_token=${suite_admin_access_token}
    [Test] Create trust order then execute
    [200][API][Get order detail] Verify trust order detail should return response with requestor information

TC_EQP_11061 - [API][200] Create BA order with requestor
    [Tags]      API    fail      2018-23-VN-SAVIORS
    [Test] Prepare payer and payee and initiator sof cash
    [Test][200] Prepare service structure from service name
    [Test][200] create balance adjustment with requestor then execute
    [200][API][Get order detail] Verify adjustment order detail should return response with requestor information

TC_EQP_11063 - [API][200] Create payroll order with requestor is agent
    [System_User][Test][200] - create account login via api without any role
    [Agent][Suite][200] - log in as company agent
    [Payment][Test][200] - add money for company agent in "VND" currency
    [Agent][Test][200] - create "Water" agent in "VND" currency
    [Customer][Test][200] - create customer in "VND" currency
    [Payment][Test][200] - create normal service structure in "VND" currency
    [Payment][Test][200] company agent fund in for prepared customer
    [Test][200] create payroll card for prepared customer
    [Report][Test][200] - system user updates payroll card number for prepared customer
    [Test][200] create cashout payment order with requestor then execute
    [200][API][Get order detail] Verify cashout order detail should return response with requestor information

TC_EQP_11060 - [API][200] Create normal order with requestor is agent
    [Tags]      API    success      2018-23-VN-SAVIORS
    [Test] Prepare payer and payee sof cash
    [Test] Prepare requestor sof cash
    [Test][200] Prepare service structure from service name without balance distribution
    [Test][200] API create balance distribution with actor type of credit is payee
    [Payment][Test][200] - create normal order with requestor
    [200][API][Get order detail] Verify normal order detail should return response with requestor information

TC_EQP_11062 - [API][200] Create artifact order with requestor is agent
    [Tags]      API    success      2018-23-VN-SAVIORS
    [Test][200] Prepare service structure from service name without balance distribution
    [Test][200] API create balance distribution with actor type of debit is payee
    [Test] Prepare payer and payee sof cash
    [Test] Prepare requestor sof cash
    [Prepare] - Create an artifact order with requestor
    [200][API][Get order detail] Verify artifact order detail should return response with requestor information