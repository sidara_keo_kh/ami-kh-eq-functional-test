*** Settings ***
Resource         ../1_raw-deprecated/imports.robot

*** Keywords ***
[200] API create fraud ticket
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}     start_active_ticket_timestamp                ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}     end_active_ticket_timestamp                 ${param_not_used}
    ${dic}   API create fraud ticket     ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    should be equal as strings      success     ${response.json()['status']['code']}
    should be equal as strings      Success     ${response.json()['status']['message']}
    set to dictionary    ${dic}     ticket_id   ${response.json()['data']['tickets'][0]['ticket_id']}
    [Return]    ${dic}

[200] API create new fraud ticket
    [Documentation]       example for @{data}
    ...     ${item1}  set variable    {"device_id":"device_id","device_description":"device_description"}
    ...     ${item2}  set variable    {"card_id":"1"}
    ...     @{data}  create list     ${item1}    ${item2}
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      access_token    ${suite_admin_access_token}

    ${dic}   API create new fraud ticket       ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    should be equal as strings      success     ${response.json()['status']['code']}
    should be equal as strings      Success     ${response.json()['status']['message']}
    set to dictionary    ${dic}     tickets     ${response.json()['data']['tickets']}
    [Return]   ${dic}

[200] API delete fraud ticket
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      access_token    ${suite_admin_access_token}

    ${dic}   API delete fraud ticket      ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    should be equal as strings      success     ${response.json()['status']['code']}
    should be equal as strings      Success     ${response.json()['status']['message']}
    [Return]   ${dic}

[200] API search all fraud tickets
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      access_token    ${suite_admin_access_token}

    ${dic}   API search all fraud tickets      ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    should be equal as strings      success     ${response.json()['status']['code']}
    should be equal as strings      Success     ${response.json()['status']['message']}
    set to dictionary    ${dic}     tickets     ${response.json()['data']['tickets']}
    [Return]    ${dic}

[Fail] API search all fraud tickets
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      access_token    ${suite_admin_access_token}

    ${dic}   API search all fraud tickets      ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    500
    should be equal as strings     ${response.json()['error']}    Internal Server Error
    should contain      ${response.json()['message']}       Unparseable date

[200] API register customer profile with device_id
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API register customer profile with device_id     ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    should be equal as strings      ${response.json()['status']['code']}      success
    should be equal as strings      ${response.json()['status']['message']}      Success
    [Return]    ${dic}

[Fail] API register customer profile with device_id
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API register customer profile with device_id     ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    400
    [Return]    ${dic}

[200] API search fraud tickets by device_id
    [Arguments]     ${arg_dic}      ${device_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API search fraud tickets by device_id     ${arg_dic}       ${device_id}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    should be equal as strings      ${response.json()['status']['code']}      success
    should be equal as strings      ${response.json()['status']['message']}      Success
    set to dictionary    ${dic}    rule_action_id   ${response.json()['data']['tickets'][0]['rule_action_id']}
    [Return]    ${dic}

[200] API get all unblock ticket reasons
    [Arguments]  ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      access_token    ${suite_admin_access_token}

    ${dic}  API get all unblock ticket reasons  ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}     200
    should be equal as strings      ${response.json()['status']['code']}        success
    should be equal as strings      ${response.json()['status']['message']}     Success
    [Return]    ${dic}

[200] API unblock ticket with reason
    [Arguments]  ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      access_token    ${suite_admin_access_token}

    ${dic}  API unblock ticket with reason  ${arg_dic}
    ${response}     get from dictionary     ${dic}  response
    should be equal as integers     ${response.status_code}     200
    should be equal as strings      ${response.json()['status']['code']}        success
    should be equal as strings      ${response.json()['status']['message']}     Success
    [Return]    ${dic}

[Fail] API unblock ticket with reason for Customer Remittance voucher - On Hold
    [Arguments]  ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      access_token    ${suite_admin_access_token}

    ${dic}  API unblock ticket with reason  ${arg_dic}
    ${response}     get from dictionary     ${dic}  response
    should be equal as integers     ${response.status_code}     400
    should be equal as strings      ${response.json()['status']['code']}        invalid_request
    should be equal as strings      ${response.json()['status']['message']}     Cannot unblock hold voucher ticket
    [Return]    ${dic}

[Fail] API unblock ticket without reason
    [Arguments]  ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      access_token    ${suite_admin_access_token}

    ${dic}  API unblock ticket with reason  ${arg_dic}
    ${response}     get from dictionary     ${dic}  response
    should be equal as integers     ${response.status_code}     400
    should be equal as strings      ${response.json()['status']['code']}        invalid_request
    should be equal as strings      ${response.json()['status']['message']}     Cannot unblock ticket without the reason
    [Return]    ${dic}

[Fail] API unblock ticket with reason but ticket does not exist
    [Arguments]  ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      access_token    ${suite_admin_access_token}

    ${dic}  API unblock ticket with reason  ${arg_dic}
    ${response}     get from dictionary     ${dic}  response
    should be equal as integers     ${response.status_code}     400
    should be equal as strings      ${response.json()['status']['code']}        not_found
    should be equal as strings      ${response.json()['status']['message']}     Ticket is not found in ticket list
    [Return]    ${dic}

[Fail] API unblock ticket with reason but ticket already deleted
    [Arguments]  ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      access_token    ${suite_admin_access_token}

    ${dic}  API unblock ticket with reason  ${arg_dic}
    ${response}     get from dictionary     ${dic}  response
    should be equal as integers     ${response.status_code}     400
    should be equal as strings      ${response.json()['status']['code']}        already_deleted
    should be equal as strings      ${response.json()['status']['message']}     Ticket is already deleted
    [Return]    ${dic}