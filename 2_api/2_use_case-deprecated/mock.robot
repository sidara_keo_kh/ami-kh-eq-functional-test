*** Settings ***
Resource         ../1_raw-deprecated/imports.robot

*** Keywords ***
[200] Mock create
    [Arguments]     ${dic}
    ${dic}=     [Common] - Set default value for keyword in dictionary     ${dic}      mb_host         ${mb_host}
    ${dic}=     [Common] - Set default value for keyword in dictionary     ${dic}      mb_port         ${mb_port}

    ${response}     create_mock_to_prepare_data      ${dic.port}     ${dic.name}     ${dic.menu}       ${dic.mb_host}   ${dic.mb_port}
    log     ${response.text}
#    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     201

[200] Mock delete
    [Arguments]
    ...     ${mock_data}
    ${dic}     Mock delete
    ...     ${mock_data}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     201




[200] Mock post prepare data
    [Arguments]          ${dic}
    ${dic}      Mock post prepare data      ${dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] Mock get prepare data
    [Arguments]          ${dic}
    ${dic}      Mock get prepare data      ${dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

#*** Test Cases ***
#1243432423
#    ${dic}=     create dictionary       port=1001       name=prepare mock for balance            menu=payment
#    [200] Mock create        ${dic}
#    ${data}         catenate        SEPARATOR=
#    ...    {
#    ...    "scope": "service.robot",
#    ...    "otp_reference_id": "service.robot",
#    ...    "user_reference_code": "99999",
#    ...    "order_id": "2123423423423",
#    ...    "abc": "123",
#    ...    "abc": "456"
#    ...    }
#    ${post_dic}     create dictionary       menu=payment       data=${data}     port=1001
#    [200] Mock post prepare data            ${post_dic}
#    ${get_dic}      create dictionary       path=payment/service.robot      port=1001
#    [200] Mock get prepare data            ${get_dic}
