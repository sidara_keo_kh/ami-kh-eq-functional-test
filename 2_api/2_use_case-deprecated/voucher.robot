*** Settings ***
Resource         ../1_raw-deprecated/imports.robot

*** Keywords ***
[200] API get list voucher groups
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}    client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}    client_secret     ${setup_admin_client_secret}
    ${dic}   API get list voucher groups        ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    ${data_contract}  ${data}     run keyword and ignore error    set variable    ${response.json()['data']}
    ${length}      get length       ${data}
    ${voucher_groups}    create list
    :for     ${index}       IN RANGE        0       ${length}
    \       ${value}        set variable        ${response.json()['data'][${index}]}
    \       append to list     ${voucher_groups}         ${value}
    set to dictionary   ${dic}      data_contract    ${data_contract}
    set to dictionary   ${dic}      data    ${data}
    set to dictionary   ${dic}      voucher_groups    ${voucher_groups}
    [Return]   ${dic}

[200] API update voucher status
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}    client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}    client_secret     ${setup_admin_client_secret}
    ${dic}   API update voucher status        ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API create multiple discount vouchers
   [Arguments]     ${arg_dic}
   [Common] - Set default value for keyword in dictionary     ${arg_dic}    client_id     ${setup_admin_client_id}
   [Common] - Set default value for keyword in dictionary     ${arg_dic}    client_secret     ${setup_admin_client_secret}
   ${dic}   API create multiple discount vouchers        ${arg_dic}
   ${response}    get from dictionary     ${dic}    response
   should be equal as integers     ${response.status_code}    200
   set to dictionary    ${dic}   ids    ${response.json()['data']['ids']}
   [Return]   ${dic}

[200] API create multiple-use vouchers
   [Arguments]     ${arg_dic}
   [Common] - Set default value for keyword in dictionary     ${arg_dic}    client_id     ${setup_admin_client_id}
   [Common] - Set default value for keyword in dictionary     ${arg_dic}    client_secret     ${setup_admin_client_secret}
   ${dic}   API create multiple-use vouchers        ${arg_dic}
   ${response}    get from dictionary     ${dic}    response
   should be equal as integers     ${response.status_code}    200
   set to dictionary    ${dic}   id    ${response.json()['data']['id']}
   [Return]   ${dic}