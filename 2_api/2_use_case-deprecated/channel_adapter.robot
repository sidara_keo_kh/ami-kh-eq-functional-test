*** Settings ***
Resource         ../1_raw-deprecated/imports.robot

*** Keywords ***
[200] API agent get his/her shop list via channel adapter
    [Arguments]
    ...     ${access_token}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}	API get list shop
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}=    get from dictionary    ${dic}   response
	should be equal as integers     ${response.status_code}     200

    log      ${response.text}
	${first_shop_id_contract}  ${first_shop_id}     run keyword and ignore error    set variable    ${response.json()['data'][0]['id']}
    set to dictionary   ${dic}      first_shop_id_contract    ${first_shop_id_contract}
    set to dictionary   ${dic}      first_shop_id    ${first_shop_id}

    ${first_agent_id_contract}  ${first_agent_id}     run keyword and ignore error    set variable    ${response.json()['data'][0]['agent_id']}
    set to dictionary   ${dic}      first_agent_id_contract    ${first_agent_id_contract}
    set to dictionary   ${dic}      first_agent_id     ${first_agent_id}

    ${first_shop_category_id_contract}  ${first_shop_category_id}     run keyword and ignore error    set variable    ${response.json()['data'][0]['shop_category_id']}
    set to dictionary   ${dic}      first_shop_category_id_contract     ${first_shop_category_id_contract}
    set to dictionary   ${dic}      first_shop_category_id     ${first_shop_category_id}


    ${first_name_contract}  ${first_name}     run keyword and ignore error    set variable    ${response.json()['data'][0]['name']}
    set to dictionary   ${dic}      first_name_contract     ${first_shop_category_id_contract}
    set to dictionary   ${dic}      first_name     ${first_shop_category_id}

    ${first_shop_type_id_contract}  ${first_shop_type_id}     run keyword and ignore error    set variable    ${response.json()['data'][0]['shop_type_id']}
    set to dictionary   ${dic}      first_shop_type_id_contract     ${first_shop_type_id_contract}
    set to dictionary   ${dic}      first_shop_type_id     ${first_shop_type_id}

    ${address_contract}  ${address}     run keyword and ignore error    set variable    ${response.json()['data'][0]['address']['address']}
    set to dictionary   ${dic}      address_contract     ${address_contract}
    set to dictionary   ${dic}      address     ${address}

    ${city_contract}  ${city}     run keyword and ignore error    set variable    ${response.json()['data'][0]['address']['city']}
    set to dictionary   ${dic}      city_contract     ${city_contract}
    set to dictionary   ${dic}      city     ${city}

    ${province_contract}  ${province}     run keyword and ignore error    set variable    ${response.json()['data'][0]['address']['province']}
    set to dictionary   ${dic}      province_contract     ${province_contract}
    set to dictionary   ${dic}      province     ${province}

    ${district_contract}  ${district}     run keyword and ignore error    set variable    ${response.json()['data'][0]['address']['district']}
    set to dictionary   ${dic}      district_contract     ${district_contract}
    set to dictionary   ${dic}      district     ${district}

    ${comune_contract}  ${comune}     run keyword and ignore error    set variable    ${response.json()['data'][0]['address']['commune']}
    set to dictionary   ${dic}      comune_contract     ${comune_contract}
    set to dictionary   ${dic}      comune     ${comune}

    ${country_contract}  ${country}     run keyword and ignore error    set variable    ${response.json()['data'][0]['address']['country']}
    set to dictionary   ${dic}      country_contract     ${country_contract}
    set to dictionary   ${dic}      country     ${country}

    ${landmark_contract}  ${landmark}     run keyword and ignore error    set variable    ${response.json()['data'][0]['address']['landmark']}
    set to dictionary   ${dic}      landmark_contract     ${landmark_contract}
    set to dictionary   ${dic}      landmark     ${landmark}

    ${latitude_contract}  ${latitude}     run keyword and ignore error    set variable    ${response.json()['data'][0]['address']['latitude']}
    set to dictionary   ${dic}      latitude_contract     ${latitude_contract}
    set to dictionary   ${dic}      latitude    ${latitude}

    ${longtitude_contract}  ${longtitude}     run keyword and ignore error    set variable    ${response.json()['data'][0]['address']['longitude']}
    set to dictionary   ${dic}      longtitude_contract     ${longtitude_contract}
    set to dictionary   ${dic}      longtitude    ${longtitude}

    ${citizen_association_contract}  ${citizen_association}     run keyword and ignore error    set variable    ${response.json()['data'][0]['address']['citizen_association']}
    set to dictionary   ${dic}      citizen_association_contract     ${citizen_association_contract}
    set to dictionary   ${dic}      citizen_association   ${citizen_association}

    ${neighbourhood_association_contract}  ${neighbourhood_association}     run keyword and ignore error    set variable    ${response.json()['data'][0]['address']['neighbourhood_association']}
    set to dictionary   ${dic}      neighbourhood_association_contract     ${neighbourhood_association_contract}
    set to dictionary   ${dic}      neighbourhood_association   ${neighbourhood_association}

    ${relationship_manager_id_contract}  ${relationship_manager_id}     run keyword and ignore error    set variable    ${response.json()['data'][0]['relationship_manager_id']}
    set to dictionary   ${dic}      relationship_manager_id_contract     ${relationship_manager_id_contract}
    set to dictionary   ${dic}      relationship_manager_id   ${relationship_manager_id}

    ${acquisition_source_contract}  ${acquisition_source}     run keyword and ignore error    set variable    ${response.json()['data'][0]['acquisition_source']}
    set to dictionary   ${dic}      acquisition_source_contract     ${acquisition_source_contract}
    set to dictionary   ${dic}      acquisition_source  ${acquisition_source}

    ${postal_code_contract}  ${postal_code}     run keyword and ignore error    set variable    ${response.json()['data'][0]['postal_code']}
    set to dictionary   ${dic}      postal_code_contract    ${postal_code_contract}
    set to dictionary   ${dic}      postal_code  ${postal_code}


    ${representative_first_name_contract}  ${representative_first_name}     run keyword and ignore error    set variable    ${response.json()['data'][0]['representative_first_name']}
    set to dictionary   ${dic}      representative_first_name_contract     ${representative_first_name_contract}
    set to dictionary   ${dic}      representative_first_name    ${representative_first_name}


    ${representative_middle_name_contract}  ${representative_middle_name}     run keyword and ignore error    set variable    ${response.json()['data'][0]['representative_middle_name']}
    set to dictionary   ${dic}      representative_middle_name_contract     ${representative_middle_name_contract}
    set to dictionary   ${dic}      representative_middle_name   ${representative_middle_name}

    ${representative_last_name_contract}  ${representative_last_name}     run keyword and ignore error    set variable    ${response.json()['data'][0]['representative_last_name']}
    set to dictionary   ${dic}      representative_last_name_contract     ${representative_last_name_contract}
    set to dictionary   ${dic}      representative_last_name   ${representative_last_name}


    ${representative_mobile_number_contract}  ${representative_mobile_number}     run keyword and ignore error    set variable    ${response.json()['data'][0]['representative_mobile_number']}
    set to dictionary   ${dic}      representative_mobile_number_contract     ${representative_mobile_number_contract}
    set to dictionary   ${dic}      representative_mobile_number   ${representative_mobile_number}


    ${representative_telephone_number_contract}  ${representative_telephone_number}     run keyword and ignore error    set variable    ${response.json()['data'][0]['representative_telephone_number']}
    set to dictionary   ${dic}      representative_telephone_number_contract     ${representative_telephone_number_contract}
    set to dictionary   ${dic}      representative_telephone_number   ${representative_telephone_number}

    ${representative_email_contract}  ${representative_email}     run keyword and ignore error    set variable    ${response.json()['data'][0]['representative_email']}
    set to dictionary   ${dic}      representative_email_contract     ${representative_email_contract}
    set to dictionary   ${dic}      representative_email   ${representative_email}

    ${shop_mobile_number_contract}  ${shop_mobile_number}     run keyword and ignore error    set variable    ${response.json()['data'][0]['shop_mobile_number']}
    set to dictionary   ${dic}      shop_mobile_number_contract     ${shop_mobile_number_contract}
    set to dictionary   ${dic}      shop_mobile_number   ${shop_mobile_number}

    ${shop_telephone_number_contract}  ${shop_telephone_number}     run keyword and ignore error    set variable    ${response.json()['data'][0]['shop_telephone_number']}
    set to dictionary   ${dic}      shop_telephone_number_contract     ${shop_telephone_number_contract}
    set to dictionary   ${dic}      shop_telephone_number   ${shop_telephone_number}

    ${shop_email_contract}  ${shop_email}     run keyword and ignore error    set variable    ${response.json()['data'][0]['shop_email']}
    set to dictionary   ${dic}      shop_email_contract     ${shop_email_contract}
    set to dictionary   ${dic}      shop_email   ${shop_email}

    ${relationship_manager_name_contract}  ${relationship_manager_name}     run keyword and ignore error    set variable    ${response.json()['data'][0]['relationship_manager_name']}
    set to dictionary   ${dic}      relationship_manager_name_contract     ${relationship_manager_name_contract}
    set to dictionary   ${dic}      relationship_manager_name   ${relationship_manager_name}

    ${relationship_manager_email_contract}  ${relationship_manager_email}     run keyword and ignore error    set variable    ${response.json()['data'][0]['relationship_manager_email']}
    set to dictionary   ${dic}      relationship_manager_email_contract     ${relationship_manager_email_contract}
    set to dictionary   ${dic}      relationship_manager_email  ${relationship_manager_email}

    ${acquiring_sales_executive_name_contract}  ${acquiring_sales_executive_name}     run keyword and ignore error    set variable    ${response.json()['data'][0]['acquiring_sales_executive_name']}
    set to dictionary   ${dic}      acquiring_sales_executive_name_contract     ${acquiring_sales_executive_name_contract}
    set to dictionary   ${dic}      acquiring_sales_executive_name  ${acquiring_sales_executive_name}

    ${sales_region_contract}  ${sales_region}     run keyword and ignore error    set variable    ${response.json()['data'][0]['sales_region']}
    set to dictionary   ${dic}      sales_region_contract     ${sales_region_contract}
    set to dictionary   ${dic}      sales_region  ${sales_region}

    ${account_manager_name_contract}  ${account_manager_name}     run keyword and ignore error    set variable    ${response.json()['data'][0]['account_manager_name']}
    set to dictionary   ${dic}      account_manager_name_contract     ${account_manager_name_contract}
    set to dictionary   ${dic}      account_manager_name  ${account_manager_name}

    ${ref1_contract}  ${ref1}     run keyword and ignore error    set variable    ${response.json()['data'][0]['ref1']}
    set to dictionary   ${dic}      ref1_contract     ${ref1_contract}
    set to dictionary   ${dic}      ref1  ${ref1}

    ${ref2_contract}  ${ref2}     run keyword and ignore error    set variable    ${response.json()['data'][0]['ref2']}
    set to dictionary   ${dic}      ref2_contract     ${ref2_contract}
    set to dictionary   ${dic}      ref2  ${ref2}

    ${is_deleted_contract}  ${is_deleted}     run keyword and ignore error    set variable    ${response.json()['data'][0]['is_deleted']}
    set to dictionary   ${dic}     is_deleted_contract     ${is_deleted_contract}
    set to dictionary   ${dic}      is_deleted  ${is_deleted}

    ${mobile_device_unique_reference_contract}  ${mobile_device_unique_reference}     run keyword and ignore error    set variable    ${response.json()['data'][0]['mobile_device_unique_reference']}
    set to dictionary   ${dic}     mobile_device_unique_reference_contract     ${mobile_device_unique_reference_contract}
    set to dictionary   ${dic}      mobile_device_unique_reference  ${mobile_device_unique_reference}


    ${edc_serial_number_contract}  ${edc_serial_number}     run keyword and ignore error    set variable    ${response.json()['data'][0]['edc_serial_number']}
    set to dictionary   ${dic}      edc_serial_number_contract     ${edc_serial_number_contract}
    set to dictionary   ${dic}      edc_serial_number  ${edc_serial_number}

    ${pos_serial_number_contract}  ${pos_serial_number}     run keyword and ignore error    set variable    ${response.json()['data'][0]['pos_serial_number']}
    set to dictionary   ${dic}      pos_serial_number_contract     ${pos_serial_number_contract}
    set to dictionary   ${dic}      pos_serial_number  ${pos_serial_number}

    ${pos_serial_number_contract}  ${pos_serial_number}     run keyword and ignore error    set variable    ${response.json()['data'][0]['pos_serial_number']}
    set to dictionary   ${dic}      pos_serial_number_contract     ${pos_serial_number_contract}
    set to dictionary   ${dic}      pos_serial_number  ${pos_serial_number}

	[Return]    ${dic}


[200]API search order configuration by service group id
        ${dic}  API search order configuration by service group id
        ${response}=    get from dictionary    ${dic}   response
	    should be equal as integers     ${response.status_code}     200
	    log      ${response.text}

        ${id_contract}  ${id}     run keyword and ignore error    set variable    ${response.json()['data']['id']}
        set to dictionary   ${dic}      id_contract    ${id_contract}
        set to dictionary   ${dic}      id    ${id}

#        ${application_type_contract}  ${application_   type}     run keyword and ignore error    set variable    ${response.json()['data']['application_type']}
#        set to dictionary   ${dic}      application_type_contract    ${application_type_contract}
#        set to dictionary   ${dic}      application_type   ${application_type}
#
#        ${application_type_id_contract}  ${application_type_id}     run keyword and ignore error    set variable    ${response.json()['data']['application_type']['id']}
#        set to dictionary   ${dic}      application_type_id_contract    ${application_type_id_contract}
#        set to dictionary   ${dic}      application_type_id   ${application_type_id}
#
#        ${application_type_app_type_name_contract}  ${application_type_app_type_name}     run keyword and ignore error    set variable    ${response.json()['data']['application_type']['app_type_name']}
#        set to dictionary   ${dic}      application_type_app_type_name_contract    ${application_type_app_type_name_contract}
#        set to dictionary   ${dic}      application_type_app_type_name   ${application_type_app_type_name}

        ${service_group_id_contract}  ${service_group_id}     run keyword and ignore error    set variable    ${response.json()['data']['service_group_id']}
        set to dictionary   ${dic}      service_group_id_contract    ${service_group_id_contract}
        set to dictionary   ${dic}      service_group_id   ${service_group_id}

        ${template_contract}  ${template}     run keyword and ignore error    set variable    ${response.json()['data']['template']}
        set to dictionary   ${dic}      template_contract    ${template_contract}
        set to dictionary   ${dic}      template   ${template}

        ${template_id_contract}  ${template_id}     run keyword and ignore error    set variable    ${response.json()['data']['template']['id']}
        set to dictionary   ${dic}      template_id_contract    ${template_id_contract}
        set to dictionary   ${dic}      template_id   ${template_id}

        ${template_name_contract}  ${template_name}     run keyword and ignore error    set variable    ${response.json()['data']['template']['name']}
        set to dictionary   ${dic}      template_name_contract    ${template_name_contract}
        set to dictionary   ${dic}      template_name  ${template_name}

        ${image_path_contract}  ${image_path}     run keyword and ignore error    set variable    ${response.json()['data']['template']['image_path']}
        set to dictionary   ${dic}      image_path_contract    ${image_path_contract}
        set to dictionary   ${dic}      image_path  ${image_path}

        ${author_contract}  ${author}     run keyword and ignore error    set variable    ${response.json()['data']['author']}
        set to dictionary   ${dic}      author_contract    ${author_contract}
        set to dictionary   ${dic}      author  ${author}

        ${status_contract}  ${status}     run keyword and ignore error    set variable    ${response.json()['data']['status']}
        set to dictionary   ${dic}      status_contract    ${status_contract}
        set to dictionary   ${dic}      status  ${status}

        ${config_services_contract}  ${config_services}     run keyword and ignore error    set variable    ${response.json()['data']['config_services']}
        set to dictionary   ${dic}      config_services_contract    ${config_services_contract}
        set to dictionary   ${dic}      config_services  ${config_services}


#        ${config_services_service_id_contract}  ${config_services_service_id}     run keyword and ignore error    set variable    ${response.json()['data']['config_services']['service_id']}
#        set to dictionary   ${dic}      config_services_service_id_contract    ${config_services_service_id_contract}
#        set to dictionary   ${dic}      config_services_service_id  ${config_services_service_id}
#
#        ${config_services_service_group_id_contract}  ${config_services_service_group_id}     run keyword and ignore error    set variable    ${response.json()['data']['config_services']['service_group_id']}
#        set to dictionary   ${dic}      config_services_service_group_id_contract    ${config_services_service_group_id_contract}
#        set to dictionary   ${dic}      config_services_service_group_id ${config_services_service_group_id}
#
#
#        ${config_services_service_name_contract}  ${config_services_service_name}     run keyword and ignore error    set variable    ${response.json()['data']['config_services']['service_name']}
#        set to dictionary   ${dic}      config_services_service_name_contract    ${config_services_service_name_contract}
#        set to dictionary   ${dic}      config_services_service_name   ${config_services_service_name}
#
#
#        ${config_services_currency_contract}  ${config_services_currency}     run keyword and ignore error    set variable    ${response.json()['data']['config_services']['currency']}
#        set to dictionary   ${dic}      config_services_currency_contract    ${config_services_currency_contract}
#        set to dictionary   ${dic}      config_services_currency   ${config_services_currency}
#
#        ${config_services_currency_contract}  ${config_services_currency}     run keyword and ignore error    set variable    ${response.json()['data']['config_services']['currency']}
#        set to dictionary   ${dic}      config_services_currency_contract    ${config_services_currency_contract}
#        set to dictionary   ${dic}      config_services_currency   ${config_services_currency}
#
#        ${config_services_status_contract}  ${config_services_status}     run keyword and ignore error    set variable    ${response.json()['data']['config_services']['status']}
#        set to dictionary   ${dic}      config_services_status_contract    ${config_services_status_contract}
#        set to dictionary   ${dic}      config_services_status   ${config_services_status}
#
#
#        ${config_services_description_contract}  ${config_services_description}     run keyword and ignore error    set variable    ${response.json()['data']['config_services']['description']}
#        set to dictionary   ${dic}      config_services_description_contract    ${config_services_description_contract}
#        set to dictionary   ${dic}      config_services_description   ${config_services_description}
#
#        ${config_services_is_deleted_contract}  ${config_services_is_deleted}     run keyword and ignore error    set variable    ${response.json()['data']['config_services']['is_deleted']}
#        set to dictionary   ${dic}      config_services_is_deleted_contract    ${config_services_is_deleted_contract}
#        set to dictionary   ${dic}      config_services_is_deleted   ${config_services_is_deleted}


        [Return]    ${dic}

[200] API agent bind his/her device via channel adpater
    [Arguments]
    ...     ${access_token}
    ...     ${shop_id}
    ...     ${device_model}
    ...     ${device_unique_reference}
    ...     ${device_name}
    ...     ${mac_address}
    ...     ${network_provider_name}
    ...     ${public_ip_address}
    ...     ${supporting_file_1}
    ...     ${supporting_file_2}
    ...     ${os}
    ...     ${os_version}
    ...     ${display_size_in_inches}
    ...     ${pixel_counts}
    ...     ${unique_number}
    ...     ${serial_number}
    ...     ${app_version}
    ...     ${otp_reference}
    ${dic}	API agent bind device
    ...     ${shop_id}
    ...     ${mac_address}
    ...     ${network_provider_name}
    ...     ${public_ip_address}
    ...     ${supporting_file_1}
    ...     ${supporting_file_2}
    ...     ${device_name}
    ...     ${device_model}
    ...     ${device_unique_reference}
    ...     ${os}
    ...     ${os_version}
    ...     ${display_size_in_inches}
    ...     ${pixel_counts}
    ...     ${unique_number}
    ...     ${serial_number}
    ...     ${app_version}
    ...     ${otp_reference}
    ...     ${access_token}
    ...     ${setup_channel_adapter_client_id}
    ...     ${setup_channel_adapter_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary   ${dic}      status_code     ${response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${response.json()['status']['message']}
    set to dictionary   ${dic}      data  ${response.json()['data']}
    [Return]   ${dic}

[Fail] API agent bind his/her device via channel adpater
    [Arguments]
    ...     ${access_token}
    ...     ${shop_id}
    ...     ${device_model}
    ...     ${device_unique_reference}
    ...     ${device_name}
    ...     ${mac_address}
    ...     ${network_provider_name}
    ...     ${public_ip_address}
    ...     ${supporting_file_1}
    ...     ${supporting_file_2}
    ...     ${os}
    ...     ${os_version}
    ...     ${display_size_in_inches}
    ...     ${pixel_counts}
    ...     ${unique_number}
    ...     ${serial_number}
    ...     ${app_version}
    ...     ${otp_reference}
    ${dic}	API agent bind device
    ...     ${shop_id}
    ...     ${mac_address}
    ...     ${network_provider_name}
    ...     ${public_ip_address}
    ...     ${supporting_file_1}
    ...     ${supporting_file_2}
    ...     ${device_name}
    ...     ${device_model}
    ...     ${device_unique_reference}
    ...     ${os}
    ...     ${os_version}
    ...     ${display_size_in_inches}
    ...     ${pixel_counts}
    ...     ${unique_number}
    ...     ${serial_number}
    ...     ${app_version}
    ...     ${otp_reference}
    ...     ${access_token}
    ...     ${setup_channel_adapter_client_id}
    ...     ${setup_channel_adapter_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     400
    set to dictionary   ${dic}      status_code     ${response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${response.json()['status']['message']}
    [Return]   ${dic}

[200] Agent get list binded devices via channel adapter
    [Arguments]
    ...     ${access_token}

    ${dic}	API agent list binded devices
    ...     ${access_token}
    ...     ${setup_channel_adapter_client_id}
    ...     ${setup_channel_adapter_client_secret}

    ${response}=    get from dictionary    ${dic}   response
    set to dictionary   ${dic}      status_code     ${response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${response.json()['status']['message']}
    set to dictionary   ${dic}      data  ${response.json()['data']}
    [Return]   ${dic}

[200] API agent unbind device via channel adpater
	[Arguments]
    ... 	${access_token}
    ...     ${bind_device_id}
    ...     ${unbind_device_id}
    ...     ${otp_reference_id}
	${dic}	API agent unbind device
    ... 	${access_token}
    ...     ${setup_channel_adapter_client_id}
    ...     ${setup_channel_adapter_client_secret}
    ...     ${bind_device_id}
    ...     ${unbind_device_id}
    ...     ${otp_reference_id}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary   ${dic}      status_code     ${response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${response.json()['status']['message']}
	[Return]    ${dic}

[Fail] API agent unbind device via channel adpater
	[Arguments]
    ... 	${access_token}
    ...     ${bind_device_id}
    ...     ${unbind_device_id}
    ...     ${otp_reference_id}
	${dic}	API agent unbind device
    ... 	${access_token}
    ...     ${setup_channel_adapter_client_id}
    ...     ${setup_channel_adapter_client_secret}
    ...     ${bind_device_id}
    ...     ${unbind_device_id}
    ...     ${otp_reference_id}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     400
    set to dictionary   ${dic}      status_code     ${response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${response.json()['status']['message']}
	[Return]    ${dic}

[200] API agent login via channel adpater
	[Arguments]
    ...     ${username}
    ...     ${password}
    ...     ${login_type}
    ...     ${grant_type}
    ...     ${device_id}
    ...     ${channel_id}
    ...     ${device_type}
	${dic}	API agent login via channel adapter
    ...     ${username}
    ...     ${password}
    ...     ${login_type}
    ...     ${grant_type}
    ...     ${device_id}
    ...     ${param_not_used}
    ...     ${setup_channel_adapter_client_id}
    ...     ${setup_channel_adapter_client_secret}
    ...     ${channel_id}
    ...     ${device_type}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary   ${dic}      status_code     ${response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${response.json()['status']['message']}
    set to dictionary   ${dic}      data  ${response.json()['data']}
	[Return]    ${dic}

[200] Revoke access token when login new device
    [Arguments]
    ...     ${test_agent_access_token}
    ${dic}	Revoke access token when login new device
    ...     ${test_agent_access_token}
    ...     ${setup_channel_adapter_client_id}
    ...     ${setup_channel_adapter_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200

[Fail] API agent login via channel adpater
	[Arguments]
    ...     ${username}
    ...     ${password}
    ...     ${login_type}
    ...     ${grant_type}
    ...     ${device_id}
    ...     ${channel_id}
    ...     ${device_type}
	${dic}	API agent login via channel adapter
    ...     ${username}
    ...     ${password}
    ...     ${login_type}
    ...     ${grant_type}
    ...     ${device_id}
    ...     ${param_not_used}
    ...     ${setup_channel_adapter_client_id}
    ...     ${setup_channel_adapter_client_secret}
    ...     ${channel_id}
    ...     ${device_type}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     400
    set to dictionary   ${dic}      status_code     ${response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${response.json()['status']['message']}
    set to dictionary   ${dic}      failed_login_remaining  ${response.json()['data']['failed_login_remaining']}
	[Return]    ${dic}

[Fail] API agent login via channel adpater without failed remaining
	[Arguments]
    ...     ${username}
    ...     ${password}
    ...     ${login_type}
    ...     ${grant_type}
    ...     ${device_id}
    ...     ${channel_id}
    ...     ${device_type}
	${dic}	API agent login via channel adapter
    ...     ${username}
    ...     ${password}
    ...     ${login_type}
    ...     ${grant_type}
    ...     ${device_id}
    ...     ${param_not_used}
    ...     ${setup_channel_adapter_client_id}
    ...     ${setup_channel_adapter_client_secret}
    ...     ${channel_id}
    ...     ${device_type}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     400
    set to dictionary   ${dic}      status_code     ${response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${response.json()['status']['message']}
	[Return]    ${dic}


[200] API get mobile configuration
	${dic}	API get mobile configuration
    ...     ${setup_channel_adapter_client_id}
    ...     ${setup_channel_adapter_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary   ${dic}      status_code     ${response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${response.json()['status']['message']}
    set to dictionary   ${dic}      data  ${response.json()['data']}
	[Return]    ${dic}

[200] API channel gateway - agent get basic profile
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_id       ${setup_channel_adapter_client_id}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_secret       ${setup_channel_adapter_client_secret}
    ${dic}      API channel gateway - agent get basic profile    ${arg_dic}
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary   ${dic}      status_code     ${dic.response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${dic.response.json()['status']['message']}
    set to dictionary   ${dic}      data  ${dic.response.json()['data']}
    [Return]    ${dic}

[200] API agent logout via channel adapter
	[Arguments]
    ... 	${access_token}
    ...     ${login_device_id}
	${dic}	API agent logout via channel adapter
    ...     ${access_token}
    ...     ${setup_channel_adapter_client_id}
    ...     ${setup_channel_adapter_client_secret}
    ...     ${login_device_id}
    ${response}=    get from dictionary    ${dic}   response
    log     ${response.text}
    should be equal as integers     ${response.status_code}     200
    set to dictionary   ${dic}      status_code     ${response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${response.json()['status']['message']}
    set to dictionary   ${dic}      data  ${response.json()['data']}
	[Return]    ${dic}

[Fail] API agent logout via channel adapter
	[Arguments]
    ... 	${access_token}
    ...     ${login_device_id}
	${dic}	API agent logout via channel adapter
    ...     ${access_token}
    ...     ${setup_channel_adapter_client_id}
    ...     ${setup_channel_adapter_client_secret}
    ...     ${login_device_id}
    ${response}=    get from dictionary    ${dic}   response
    log     ${response.text}
    should be equal as integers     ${response.status_code}     400
    set to dictionary   ${dic}      status_code     ${response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${response.json()['status']['message']}
	[Return]    ${dic}


[200] API create order configuration api
    [Arguments]
    ...     ${service_group_id}
    ...     ${template_id}
    ...     ${author}
    ...     ${status}
    ...     ${agent_access_token}

    ${response}	API create order configuration api
    ...     ${service_group_id}
    ...     ${template_id}
    ...     ${author}
    ...     ${status}
    ...     ${agent_access_token}

    ${dic} =	Create Dictionary   response=${response}
    set to dictionary   ${dic}      data  ${response.json()['data']}

    [Return]   ${dic}

[200] API update order configuration api
    [Documentation]     example for @{list_configs_data}
    ...     ${a}  set variable    {"en_label":"","local_label":"","value":"a","other_ref_key":"","priority":1,"equation":"+","config_type_id":"2", "actor_id":"1"},
    ...     ${b}  set variable    {"en_label":"","local_label":"","value":"a","other_ref_key":"","priority":1,"equation":"+","config_type_id":"2", "actor_id":"2"},
    ...     ${c}  set variable    {"en_label":"","local_label":"","value":"a","other_ref_key":"","priority":1,"equation":"+","config_type_id":"2", "actor_id":"3"}
    ...     ${list_configs_data}  create list     ${a}    ${b}    ${c}
     [Arguments]
    ...     ${order_config_id}
    ...     ${author}
    ...     ${status}
    ...     ${service_group_id}
    ...     ${agent_access_token}
    ...     ${list_configs_data}

    ${response}	API update order configuration api
    ...     ${order_config_id}
    ...     ${author}
    ...     ${status}
    ...     ${service_group_id}
    ...     ${agent_access_token}
    ...     ${list_configs_data}

    ${dic} =	Create Dictionary   response=${response}
    set to dictionary   ${dic}      data  ${response.json()['data']}

    [Return]   ${dic}

[200] API update order configuration api with pre and post wallet balance
    [Documentation]     example for @{list_configs_data}
    ...     ${a}  set variable    {"en_label":"","local_label":"","value":"pre_wallet_balance","other_ref_key":"","priority":1,"equation":"+","config_type_id":"2", "actor_id":"1"},
    ...     ${b}  set variable    {"en_label":"","local_label":"","value":"post_wallet_balance","other_ref_key":"","priority":1,"equation":"+","config_type_id":"2", "actor_id":"2"},
    ...     ${c}  set variable    {"en_label":"","local_label":"","value":"a","other_ref_key":"","priority":1,"equation":"+","config_type_id":"2", "actor_id":"3"}
    ...     ${list_configs_data}  create list     ${a}    ${b}      ${c}
     [Arguments]
    ...     ${order_config_id}
    ...     ${author}
    ...     ${status}
    ...     ${service_group_id}
    ...     ${agent_access_token}
    ...     ${list_configs_data}

    ${response}	API update order configuration api
    ...     ${order_config_id}
    ...     ${author}
    ...     ${status}
    ...     ${service_group_id}
    ...     ${agent_access_token}
    ...     ${list_configs_data}

    ${dic} =	Create Dictionary   response=${response}
    set to dictionary   ${dic}      data  ${response.json()['data']}

    [Return]   ${dic}

[200] API get version of application
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API get version of application       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[200] API get mobile application configurations
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API get mobile application configurations       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[200] API get agent balance
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API get agent balance       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[200] API get agent profile
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API get agent profile       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[200] API get list faqs
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API get list faqs       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary   ${dic}      data  ${response.json()['data']}
	[Return]    ${dic}

[Fail] API get list faqs
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API get list faqs       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     400
    set to dictionary   ${dic}      data  ${response.json()['data']}
	[Return]    ${dic}

[200] API get list contact us
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API get list contact us    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[Fail] API get list contact us
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API get list contact us    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     400
	[Return]    ${dic}

[200] API get list of agent sof bank information
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API get list of agent sof bank information    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[200] API activation Otp generation
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API activation Otp generation    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    otp_reference_id         ${response.json()['data']['otp_reference_id']}
	[Return]    ${dic}

[200] API activation Otp regeneration
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API activation Otp regeneration    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[200] API activation Otp verification
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API activation Otp verification    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[200] API get order list
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API get order list    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[Fail] API get order list
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API get order list    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
	[Return]    ${dic}

[200] API get order detail via channel adapter
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API get order detail via channel adapter    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary   ${dic}      data    ${response.json()['data']}
	[Return]    ${dic}

[Fail] API get order detail via channel adapter
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API get order detail via channel adapter    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     400
	[Return]    ${dic}

[200] API get order configuration detail by id
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API get order configuration detail by id    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary   ${dic}      data    ${response.json()['data']}
	[Return]    ${dic}

[200] API get list order configuration
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API get list order configuration    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary   ${dic}      data    ${response.json()['data']}
	[Return]    ${dic}

[200] API get all templates
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API get all templates    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary   ${dic}      data    ${response.json()['data']}
	[Return]    ${dic}

[200] API Validate agent identity
    [Arguments]     ${arg_dic}
    ${dic}      API Validate agent identity    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[Fail] API Validate agent identity
    [Arguments]     ${arg_dic}
    ${dic}      API Validate agent identity    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     400
    set to dictionary   ${dic}      status_code     ${response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${response.json()['status']['message']}
	[Return]    ${dic}

[200] Get OTP code from mock server
    ${dic}     API Get Otp From Mock   ${test_otp_reference_id}
    ${response}    get from dictionary    ${dic}   response
    log     ${response.json()}
    ${otp_code}   set variable    ${response.json()['data']['otp_code']}
    Set To Dictionary    ${dic}    otp_code  ${otp_code}
    ${user_reference_code}   set variable    ${response.json()['data']['user_reference_code']}
    Set To Dictionary    ${dic}    user_reference_code  ${user_reference_code}
    should be equal as integers     ${response.status_code}     200
    log            ${response.text}         level=INFO
    [Return]    ${dic}

[200] Verify OTP
    ${arg_dic}  create dictionary
    [Common] - Set default value for keyword in dictionary         ${arg_dic}     device_unique_reference       ${login_device_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     otp_code    ${test_otp_code}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     otp_ref    ${test_otp_ref}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     user_ref_code    ${test_user_reference_code}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     agent_id    ${test_agent_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     access_token    ${suite_admin_access_token}

    ${dic}      API Verification Otp    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[Fail] Verify OTP Expired Time
    ${arg_dic}  create dictionary
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     otp_code    ${test_otp_code}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     otp_ref    ${test_otp_ref}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     user_ref_code    ${test_user_reference_code}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     agent_id    ${test_agent_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     access_token    ${suite_admin_access_token}

    sleep     3m

    ${dic}      API Verification Otp    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    set to dictionary   ${dic}      status_code     ${response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${response.json()['status']['message']}
    should be equal as integers     ${response.status_code}     400
    should be equal as strings      ${dic.status_message}     OTP expired

	[Return]    ${dic}

[Fail] Verify OTP with wrong OTP
    ${arg_dic}  create dictionary
    [Common] - Set default value for keyword in dictionary         ${arg_dic}     device_unique_reference       ${login_device_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     otp_code    12345
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     otp_ref    ${test_otp_ref}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     user_ref_code    ${test_user_reference_code}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     agent_id    ${test_agent_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     access_token    ${suite_admin_access_token}

    ${dic}      API Verification Otp    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    set to dictionary   ${dic}      status_code     ${response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${response.json()['status']['message']}
    should be equal as integers     ${response.status_code}     400
    should be equal as strings      ${dic.status_message}     OTP Verification fail

	[Return]    ${dic}

[200] Set new password
    [Arguments]     ${arg_dic}
    ${dic}      API Set new password    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

Success] Regenerate OTP with OTP Reference ID
    ${arg_dic}  create dictionary

    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     access_token    ${suite_admin_access_token}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     otp_ref    ${test_otp_ref}

    ${dic}      API Regenerate OTP with OTP Reference ID     ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}


[Fail] Set new password with deleted identity
    [Arguments]     ${arg_dic}
    ${dic}      API Set new password    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    set to dictionary   ${dic}      status_code     ${dic.response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${dic.response.json()['status']['message']}
    should be equal as integers     ${response.status_code}     400
	[Return]    ${dic}

[Fail] Set new password with invalid credential
    [Arguments]     ${arg_dic}
    ${dic}      API Set new password    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    set to dictionary   ${dic}      status_code     ${dic.response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${dic.response.json()['status']['message']}
    should be equal as integers     ${response.status_code}     400
	[Return]    ${dic}

[200] API agent change password of identity
	[Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_id       ${setup_channel_adapter_client_id}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_secret       ${setup_channel_adapter_client_secret}
    ${dic}      API agent change password of identity  ${arg_dic}
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary   ${dic}      status_code     ${dic.response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${dic.response.json()['status']['message']}
    set to dictionary   ${dic}      data  ${dic.response.json()['data']}
    [Return]    ${dic}

[Fail] API agent change identity password with invalid credential
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_id       ${setup_channel_adapter_client_id}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_secret       ${setup_channel_adapter_client_secret}
    ${dic}      API agent change password of identity  ${arg_dic}
    should be equal as integers     ${dic.response.status_code}     400
    set to dictionary   ${dic}      status_code     ${dic.response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${dic.response.json()['status']['message']}
    set to dictionary   ${dic}      data  ${dic.response.json()['data']}
    [Return]    ${dic}

[200] API agent change pin of identity
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_id       ${setup_channel_adapter_client_id}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_secret       ${setup_channel_adapter_client_secret}
    ${dic}      API agent change pin identity  ${arg_dic}
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary   ${dic}      status_code     ${dic.response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${dic.response.json()['status']['message']}
    set to dictionary   ${dic}      data  ${dic.response.json()['data']}
    [Return]    ${dic}

[Fail] API agent change pin of identity
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_id       ${setup_channel_adapter_client_id}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_secret       ${setup_channel_adapter_client_secret}
    ${dic}      API agent change pin identity  ${arg_dic}
    should be equal as integers     ${dic.response.status_code}     400
    set to dictionary   ${dic}      status_code     ${dic.response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${dic.response.json()['status']['message']}
    set to dictionary   ${dic}      data  ${dic.response.json()['data']}
    [Return]    ${dic}

[200] Regenerate OTP with OTP Reference ID
    ${arg_dic}  create dictionary

    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     access_token    ${suite_admin_access_token}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     otp_ref    ${test_otp_ref}

    ${dic}      API Regenerate OTP with OTP Reference ID     ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[200] API Check if identiy is logged in or not
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_id       ${setup_channel_adapter_client_id}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_secret       ${setup_channel_adapter_client_secret}
    ${dic}      API Check if identity is logged in or not  ${arg_dic}
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary   ${dic}      status_code     ${dic.response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${dic.response.json()['status']['message']}
    set to dictionary   ${dic}      data  ${dic.response.json()['data']}
    [Return]    ${dic}

[200] Call generate OTP API with access token
    [Arguments]     ${arg_dic}
    set to dictionary     	${arg_dic}     client_id    ${setup_channel_adapter_client_id}
    set to dictionary     	${arg_dic}     client_secret    ${setup_channel_adapter_client_secret}
    set to dictionary     	${arg_dic}     access_token    ${test_agent_access_token}
    ${dic}      API Otp generate with access token    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    otp_reference_id         ${response.json()['data']['otp_reference_id']}
	[Return]    ${dic}

[200] API Search agent orders
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_secret       ${setup_admin_client_secret}
    ${dic}      API Search agent orders  ${arg_dic}
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary   ${dic}      status_code     ${dic.response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${dic.response.json()['status']['message']}
    set to dictionary   ${dic}      data  ${dic.response.json()['data']}
    [Return]    ${dic}

[200] API Get agent transactions summary
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_id       ${setup_channel_adapter_client_id}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_secret       ${setup_channel_adapter_client_secret}
    ${dic}      API Get agent transactions summary  ${arg_dic}
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary   ${dic}      status_code     ${dic.response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${dic.response.json()['status']['message']}
    set to dictionary   ${dic}      data  ${dic.response.json()['data']}
    [Return]    ${dic}

[Fail] API Get agent transactions summary
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_id       ${setup_channel_adapter_client_id}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_secret       ${setup_channel_adapter_client_secret}
    ${dic}      API Get agent transactions summary  ${arg_dic}
    should be equal as integers     ${dic.response.status_code}     400
    set to dictionary   ${dic}      status_code     ${dic.response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${dic.response.json()['status']['message']}
    set to dictionary   ${dic}      data  ${dic.response.json()['data']}
    [Return]    ${dic}

[200] API Search order configuration by service_group_id and template_id
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_id       ${setup_channel_adapter_client_id}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_secret       ${setup_channel_adapter_client_secret}
    ${dic}      API search order configuration by service group id and template_id  ${arg_dic}
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary   ${dic}      status_code     ${dic.response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${dic.response.json()['status']['message']}
    set to dictionary   ${dic}      data  ${dic.response.json()['data']}
    [Return]    ${dic}

[Fail] API Search order configuration by service_group_id and template_id
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_id       ${setup_channel_adapter_client_id}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_secret       ${setup_channel_adapter_client_secret}
    ${dic}      API search order configuration by service group id and template_id  ${arg_dic}
    should be equal as integers     ${dic.response.status_code}     400
    set to dictionary   ${dic}      status_code     ${dic.response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${dic.response.json()['status']['message']}
    set to dictionary   ${dic}      data  ${dic.response.json()['data']}
    [Return]    ${dic}

[200] API get order detail for printing
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API get order detail for printing    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[Fail] API get order detail for printing
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API get order detail for printing    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     400
	[Return]    ${dic}

[200] API create order configuration
    [Arguments]  ${arg_dic}
    ${dic}  API create order configuration  ${arg_dic}
    [Return]    ${dic}

[200] API Verify smartcard with edc device
    [Arguments]  ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id         ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      channel_id        ${setup_edc_channel_id}
    ${dic}    API Verify smartcard with edc device        ${arg_dic}
	${response}=    get from dictionary    ${dic}   response
	should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[200] API Get Service Group List Via Channel Adapter
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API Get Service Group List Via Channel Adapter    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[200] Agent login with smart card
    set test variable   ${grant_type}       password
    set test variable   ${channel_id}       ${setup_edc_channel_id}
    ${dic}  [200] API agent login via channel adpater
    ...     ${test_card_number}
    ...     ${setup_agent_pin_encrypted}
    ...     ${param_is_null}
    ...     ${grant_type}
    ...     ${test_edc_serial_number}
    ...     ${channel_id}
    ...     ${param_not_used}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    ${agent_access_token}       set variable    ${response.json()['data']['access_token']}
    set to dictionary  ${dic}    access_token    ${agent_access_token}
    [Return]   ${dic}

[Fail] Agent login with smart card
    [Arguments]     ${arg_dic}
    set test variable   ${grant_type}       password
    set test variable   ${login_device_id}       ${arg_dic.device_id}
    set test variable   ${channel_id}       1
    ${dic}  [Fail] API agent login via channel adpater without failed remaining
    ...     ${arg_dic.serial_number}
    ...     ${setup_agent_pin_encrypted}
    ...     ${param_is_null}
    ...     ${grant_type}
    ...     ${arg_dic.device_id}
    ...     ${channel_id}
    ...     ${param_not_used}
    ${response}    get from dictionary     ${dic}    response
    log  ${response.text}
    should be equal as integers     ${response.status_code}    400
    [Return]   ${dic}


[Fail] API Verify smartcard with Invalid Request
    [Arguments]  ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id         ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}    API Verify smartcard with edc device        ${arg_dic}
	${response}=    get from dictionary    ${dic}   response
	should be equal as integers     ${response.status_code}     400
	[Return]    ${dic}

[200] API Create agent identity via channel adapter
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     header_client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     header_client_secret    ${setup_admin_client_secret}
    ${dic}      API create agent identity    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[Fail] API Create agent identity via channel adapter
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     header_client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     header_client_secret    ${setup_admin_client_secret}
    ${dic}      API create agent identity    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     400
	[Return]    ${dic}

[200] API Generate QR code
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API Generate QR code       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    set test variable  ${qr_code_id}  ${response.json()['data']['id']}
    set test variable  ${qr_code_created_timestamp}  ${response.json()['data']['created_timestamp']}
    set to dictionary   ${dic}      data  ${dic.response.json()['data']}
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[200] API Create normal order via channel adapter
    [Arguments]   ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}  API create payment normal order via channel adapter     ${arg_dic}
    ${response}     get from dictionary         ${dic}      response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}     order_id     ${response.json()['data']['order_id']}
    [Return]    ${dic}

[Fail] API Create normal order via channel adapter
    [Arguments]   ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}  API create payment normal order via channel adapter     ${arg_dic}
    ${response}     get from dictionary         ${dic}      response
    should be equal as integers     ${response.status_code}     400
    [Return]    ${dic}

[200] API execute order via channel adapter
    [Arguments]   ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}  API execute order via channel adapter     ${arg_dic}
    ${response}     get from dictionary         ${dic}      response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API execute order via channel adapter
    [Arguments]   ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}  API execute order via channel adapter     ${arg_dic}
    ${response}     get from dictionary         ${dic}      response
    should be equal as integers     ${response.status_code}     400
    [Return]    ${dic}

[200] API Verify QR code
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API Verify QR code       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[Fail] API Verify QR code
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API Verify QR code       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     400
	[Return]    ${dic}