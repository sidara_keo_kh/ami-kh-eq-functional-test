*** Settings ***
Resource         ../1_raw-deprecated/imports.robot

*** Keywords ***
[200] API system user authentication
    [Arguments]    ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      grant_type            password
    ${dic}   API system user authentication        ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200

    ${access_token}     run keyword and ignore error    set variable    ${response.json()['accessToken']}
    set to dictionary   ${dic}      accessToken    ${access_token}

    ${correlation_id}   set variable    ${response.json()['correlation_id']}
    Set To Dictionary    ${dic}    correlationId     ${correlation_id}

    ${user_id_contract}  ${user_id}     run keyword and ignore error    set variable    ${response.json()['user_id']}
    set to dictionary   ${dic}      user_id_contract    ${user_id_contract}
    set to dictionary   ${dic}      user_id    ${user_id}
    [Return]   ${dic}

[200] API create system user
    [Arguments]    ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}   API create system user    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    ${id_contract}      ${id}   run keyword and ignore error    set variable    ${response.json()['data']['id']}
    set to dictionary       ${dic}    id_contract     ${id_contract}
    set to dictionary       ${dic}    id              ${id}
    [Return]   ${dic}

[Fail] API create system user
    [Arguments]    ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}   API create system user    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}
    set to dictionary   ${dic}     data   ${response.json()['data']}
    [Return]   ${dic}

[200] API add permissions to role
    [Arguments]    &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}   API add permissions to role        &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API add permissions to role
    [Arguments]    &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}   API add permissions to role        &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}
    set to dictionary   ${dic}     data   ${response.json()['data']}
    [Return]   ${dic}

[200] API create role
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}  API create role     &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary       ${dic}      id   ${response.json()['data']['id']}
    [Return]   ${dic}

[Fail] API create role
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}  API create role     &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}
    set to dictionary   ${dic}     data   ${response.json()['data']}
    [Return]   ${dic}

[200] API delete system-user
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}  API delete system user     &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API delete system-user
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}  API delete system user     &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}
    set to dictionary   ${dic}     data   ${response.json()['data']}
    [Return]   ${dic}

[200] API get system user by access token
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}  API get system user by access token    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200

    ${id_contract}  ${id}   run keyword and ignore error    set variable    ${response.json()['data']['id1']}
    set to dictionary       ${dic}  id_contract     ${id_contract}
    set to dictionary       ${dic}  id     ${id}
    ${username_contract}  ${username}   run keyword and ignore error    set variable    ${response.json()['data']['username']}
    set to dictionary       ${dic}  username_contract     ${username_contract}
    set to dictionary       ${dic}  username              ${username}
    [Return]   ${dic}

[200] API update system user
    [Arguments]    &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id          ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}      API update system user              &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API update system user
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id          ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}      API update system user              &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}
    set to dictionary   ${dic}     data   ${response.json()['data']}
    [Return]   ${dic}

[200] API update system-user password
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id          ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}      API update system-user password     &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API update system-user password
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id          ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}      API update system-user password     &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}
    [Return]   ${dic}

[200] API Change system-user password
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}      API Change system-user password     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get role and permission
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}      API get role and permission     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API create permission
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}      API create permission     &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API create permission
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}      API create permission      &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}
    [Return]   ${dic}

[200] API update permission
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}     API update permission     &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API update permission
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}     API update permission    &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}
    [Return]   ${dic}

[200] API delete permission
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}     API delete permission     &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API delete permission
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}     API delete permission     &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}
    [Return]   ${dic}

[200] API remove permissions from role
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}     API remove permissions from role    &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API remove permissions from role
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}     API remove permissions from role     &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}
    set to dictionary   ${dic}     data   ${response.json()['data']}
    [Return]   ${dic}

[200] API update role
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}         API update role   &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API update role
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}         API update role   &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}
    set to dictionary   ${dic}     data   ${response.json()['data']}
    [Return]   ${dic}

[200] API delete role
    [Arguments]      &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}         API delete role    &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API delete role
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}         API delete role   &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    [Return]   ${dic}

[200] API add user to role
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}         API add user to role   &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}     200
    [Return]   ${dic}

[Fail] API add user to role
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}         API add user to role   &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}
    set to dictionary   ${dic}     data   ${response.json()['data']}
    [Return]   ${dic}

[200] API unlink role from user
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}         API unlink role from user   &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}     200
    [Return]   ${dic}

[Fail] API unlink role from user
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}         API unlink role from user   &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}
    set to dictionary   ${dic}     data   ${response.json()['data']}
    [Return]   ${dic}

[200] API system user link system user top company profile
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      client_id                      ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      client_secret                  ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      header_device_id               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      header_device_description      ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      header_device_imei             ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      header_device_ip4              ${param_not_used}
    ${dic}    API system user create link system user and company profile   ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API system user create system user
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      client_id                      ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      client_secret                  ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      title               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      first_name               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      middle_name               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      last_name               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      suffix               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      date_of_birth               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      place_of_birth               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      nationality               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      occupation               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      occupation_title               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      email               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      mobile_number               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      is_external               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      unique_reference               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      source_of_funds               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      national_id_number               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      current_address_citizen_association               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      current_address_neighbourhood_association               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      current_address_address               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      current_address_commune               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      current_address_district               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      current_address_city               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      current_address_province               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      current_address_postal_code               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      current_address_country               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      current_address_landmark               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      current_address_longitude               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      current_address_latitude               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      permanent_address_citizen_association               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      permanent_address_neighbourhood_association               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      permanent_address_address               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      permanent_address_commune               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      permanent_address_district               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      permanent_address_city               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      permanent_address_province               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      permanent_address_postal_code               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      permanent_address_country               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      permanent_address_landmark               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      permanent_address_longitude               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      permanent_address_latitude               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      additional_field_1_name               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      additional_field_1_value               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      additional_field_2_name               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      additional_field_2_value               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      additional_field_3_name               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      additional_field_3_value               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      additional_field_4_name               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      additional_field_4_value               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      additional_field_5_name               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      additional_field_5_value               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      additional_supporting_file_1_url               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      additional_supporting_file_2_url               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      additional_supporting_file_3_url               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      additional_supporting_file_4_url               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      additional_supporting_file_5_url               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      header_device_id               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      header_device_description               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      header_device_imei               ${param_not_used}
    [Common] - Set default value for keyword in dictionary       ${arg_dic}      header_device_ip4               ${param_not_used}

    ${dic}  API system user create system user  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
	should be equal as integers     ${response.status_code}     200
	set to dictionary       ${dic}      id   ${response.json()['data']['id']}
	[Return]    ${dic}

[200] API active suspend system user
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}     client_secret         ${setup_admin_client_secret}
    ${dic}         API active suspend system user   &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary       ${dic}      data         ${response.json()['status']['code']}
    [Return]   ${dic}

[Fail] API active suspend system user
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}         API active suspend system user   &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}
    set to dictionary   ${dic}     data   ${response.json()['data']}
    [Return]   ${dic}