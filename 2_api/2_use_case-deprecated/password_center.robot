*** Settings ***
Resource         ../1_raw-deprecated/imports.robot

*** Keywords ***
[200] API update password rule config
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API update password rule config    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[200] API create identity type
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API create identity type    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[200] API update identity type
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API update identity type    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[200] API delete identity type
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API delete identity type    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

