*** Settings ***
Resource         ../1_raw-deprecated/imports.robot

*** Keywords ***
[200] API report - get formula by report type id
    [Arguments]
        ...     ${report_type_id}
        ...     ${access_token}
        ...     ${client_id}=${setup_admin_client_id}
        ...     ${client_secret}=${setup_admin_client_secret}
    ${dic}   API report - get formula by report type id
        ...     ${report_type_id}
        ...     ${access_token}
        ...     ${client_id}
        ...     ${client_secret}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary     ${dic}        actual_tpv                      ${response.json()['data']['tpv']}
    set to dictionary     ${dic}        actual_fee                      ${response.json()['data']['fee']}
    set to dictionary     ${dic}        actual_commission               ${response.json()['data']['commission']}
    set to dictionary     ${dic}        actual_effective_timestamp      ${response.json()['data']['effective_timestamp']}
    [Return]   ${dic}

[Report][200] Get payment report formula
    [Arguments]
        ...     ${client_id}=${setup_admin_client_id}
        ...     ${client_secret}=${setup_admin_client_secret}
        ...     &{arg_dic}
    Set To Dictionary       ${arg_dic}
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
    [Report] Get payment report formula     &{arg_dic}
    REST.integer    response status     200

[Report][200] Update payment report formula
    [Arguments]
        ...     ${client_id}=${setup_admin_client_id}
        ...     ${client_secret}=${setup_admin_client_secret}
        ...     &{arg_dic}
    Set To Dictionary       ${arg_dic}
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
    [Report] Update payment report formula     &{arg_dic}
    REST.integer    response status     200