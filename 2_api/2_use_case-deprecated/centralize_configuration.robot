*** Settings ***
Resource         ../1_raw-deprecated/imports.robot

*** Keywords ***
[200] API update configuration detail
    [Arguments]
    ...     ${access_token}
    ...     ${device_id}
    ...     ${device_description}
    ...     ${scopeName}
    ...     ${key}
    ...     ${value}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}   API update configuration detail
    ...     ${access_token}
    ...     ${device_id}
    ...     ${device_description}
    ...     ${scopeName}
    ...     ${key}
    ...     ${value}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get configuration detail
    [Arguments]
    ...     ${access_token}
    ...     ${scopeName}
    ...     ${key}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}   API get configuration detail
    ...     ${access_token}
    ...     ${scopeName}
    ...     ${key}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary   ${dic}     value    ${response.json()['data']['value']}
    [Return]   ${dic}

[200] API get all configurations
    [Arguments]
    ...     ${access_token}
    ...     ${scopeName}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API get all configurations
    ...     ${access_token}
    ...     ${scopeName}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary   ${dic}     data    ${response.json()['data']}
    [Return]   ${dic}

[200] API get all pre-load countries
    [Arguments]
    ...     ${access_token}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API get all pre-load countries
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary   ${dic}     country_code    ${response.json()['data'][0]['country_code']}
    [Return]   ${dic}

[200] API get_all_pre-load_currencies
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}  API get_all_pre-load_currencies     &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get all configuration socpe
    [Arguments]
    ...     ${access_token}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API get all configuration socpe
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}


[Fail] API get_all_pre-load_currencies
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}      API get_all_pre-load_currencies     &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}
    [Return]   ${dic}

[Centralize Configuration][200] Get all currencies
    [Arguments]    &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    [Centralize Configuration] Get all currencies      &{arg_dic}
    REST.integer    response status     200