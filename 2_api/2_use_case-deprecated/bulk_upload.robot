*** Settings ***
Resource         ../1_raw-deprecated/imports.robot

*** Keywords ***
[200] API post bulk file
    [Arguments]
    ...     ${access_token}
    ...     ${file_id}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}    API post bulk file
    ...     ${access_token}
    ...     ${file_id}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API upload bulk file
    [Arguments]    &{arg_dic}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    client_secret       ${setup_admin_client_secret}
    ${dic}  API update bulk file    &{arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}   id    ${response.json()['data']['id']}
    [Return]    ${dic}

[Fail] API post bulk file
    [Arguments]
    ...     ${access_token}
    ...     ${file_id}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}    API post bulk file
    ...     ${access_token}
    ...     ${file_id}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     400
    [Return]    ${dic}

[200] API search functions
    [Arguments]
    ...     ${access_token}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API search functions
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary   ${dic}  data    ${response.json()['data']}
    [Return]    ${dic}

[200] API download bulk file
    [Arguments]    ${arg_dic}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    client_secret       ${setup_admin_client_secret}
    ${dic}      API download bulk file   ${arg_dic}
    should be equal as integers     ${dic.response.status_code}     200