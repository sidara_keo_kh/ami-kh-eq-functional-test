*** Settings ***
Resource         ../1_raw-deprecated/imports.robot

*** Keywords ***

[200] API system user create customer classification
    [Arguments]
    ...     ${access_token}
    ...     ${classification_name}
    ...     ${classification_desc}=test_classification_desc
    ${dic}   API create customer classification
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}
    ...     ${access_token}
    ...     ${classification_name}
    ...     ${classification_desc}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary       ${dic}      id   ${response.json()['data']['id']}
    [Return]   ${dic}

[200] API update customer classification
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      description     robot_customer_classification_desciption_updated
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update customer classification    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API update customer classification
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      description     robot_customer_classification_desciption_updated
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update customer classification    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API delete customer classification
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API delete customer classification    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API delete customer classification
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API delete customer classification    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    ${arg_dic.code}
    [Return]   ${dic}

[200] API get customer profiles
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API get customer profiles    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API get customer profiles
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API get customer profiles    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API active-suspend customer
    [Arguments]
    ...     ${is_suspended}
    ...     ${customer_id}
    ...     ${client_id}=${setup_admin_client_id}
    ...     ${client_secret}=${setup_admin_client_secret}
    ...     ${access_token}=${suite_admin_access_token}
    ${dic}   API active-suspend customer
    ...     ${client_id}
    ...     ${client_secret}
    ...     ${access_token}
    ...     ${is_suspended}
    ...     ${customer_id}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get all customer identities
    [Arguments]
    ...     ${client_id}
    ...     ${client_secret}
    ...     ${access_token}
    ${dic}   API get all customer identities
    ...     ${client_id}
    ...     ${client_secret}
    ...     ${access_token}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]

[200] API add customer identities
    [Arguments]
    ...     ${identity_type_id}
    ...     ${username}
    ...     ${password}
    ...     ${access_token}
    ...     ${client_id}=${setup_admin_client_id}
    ...     ${client_secret}=${setup_admin_client_secret}
    ${dic}   API add customer identities
    ...     ${identity_type_id}
    ...     ${username}
    ...     ${password}
    ...     ${access_token}
    ...     ${client_id}
    ...     ${client_secret}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API system user create customer
    [Arguments]
    ...     ${identity_username}
    ...     ${identity_password}
    ...     ${identity_identity_type_id}
    ...     ${access_token}
    ...     ${primary_mobile_number}
    ...     ${unique_reference}
    ...     ${is_testing_account}=${param_not_used}
    ...     ${is_system_account}=${param_not_used}
    ...     ${acquisition_source}=${param_not_used}
    ...     ${customer_classification_id}=${param_not_used}
    ...     ${referrer_user_type_id}=${param_not_used}
    ...     ${referrer_user_type_name}=${param_not_used}
    ...     ${referrer_user_id}=${param_not_used}
    ...     ${beneficiary}=${param_not_used}
    ...     ${mm_card_type_id}=${param_not_used}
    ...     ${mm_card_level_id}=${param_not_used}
    ...     ${mm_factory_card_number}=${param_not_used}
    ...     ${is_require_otp}=${param_not_used}
    ...     ${tin_number}=${param_not_used}
    ...     ${tin_number_local}=${param_not_used}
    ...     ${title}=${param_not_used}
    ...     ${title_local}=${param_not_used}
    ...     ${first_name}=${param_not_used}
    ...     ${first_name_local}=${param_not_used}
    ...     ${middle_name}=${param_not_used}
    ...     ${middle_name_local}=${param_not_used}
    ...     ${last_name}=${param_not_used}
    ...     ${last_name_local}=${param_not_used}
    ...     ${suffix}=${param_not_used}
    ...     ${suffix_local}=${param_not_used}
    ...     ${date_of_birth}=${param_not_used}
    ...     ${place_of_birth}=${param_not_used}
    ...     ${place_of_birth_local}=${param_not_used}
    ...     ${gender}=${param_not_used}
    ...     ${gender_local}=${param_not_used}
    ...     ${ethnicity}=${param_not_used}
    ...     ${employer}=${param_not_used}
    ...     ${nationality}=${param_not_used}
    ...     ${occupation}=${param_not_used}
    ...     ${occupation_local}=${param_not_used}
    ...     ${occupation_title}=${param_not_used}
    ...     ${occupation_title_local}=${param_not_used}
    ...     ${township_code}=${param_not_used}
    ...     ${township_name}=${param_not_used}
    ...     ${township_name_local}=${param_not_used}
    ...     ${mother_name}=${param_not_used}
    ...     ${mother_name_local}=${param_not_used}
    ...     ${mother_maiden_name}=${param_not_used}
    ...     ${mother_maiden_name_local}=${param_not_used}
    ...     ${civil_status}=${param_not_used}
    ...     ${email}=${param_not_used}
    ...     ${secondary_mobile_number}=${param_not_used}
    ...     ${tertiary_mobile_number}=${param_not_used}
    ...     ${telephone_number}=${param_not_used}
    ...     ${current_address_citizen_association}=${param_not_used}
    ...     ${current_address_neighbourhood_association}=${param_not_used}
    ...     ${current_address_address}=${param_not_used}
    ...     ${current_address_address_local}=${param_not_used}
    ...     ${current_address_commune}=${param_not_used}
    ...     ${current_address_commune_local}=${param_not_used}
    ...     ${current_address_district}=${param_not_used}
    ...     ${current_address_district_local}=${param_not_used}
    ...     ${current_address_city}=${param_not_used}
    ...     ${current_address_city_local}=${param_not_used}
    ...     ${current_address_province}=${param_not_used}
    ...     ${current_address_province_local}=${param_not_used}
    ...     ${current_address_postal_code}=${param_not_used}
    ...     ${current_address_postal_code_local}=${param_not_used}
    ...     ${current_address_country}=${param_not_used}
    ...     ${current_address_country_local}=${param_not_used}
    ...     ${current_address_landmark}=${param_not_used}
    ...     ${current_address_longitude}=${param_not_used}
    ...     ${current_address_latitude}=${param_not_used}
    ...     ${permanent_address_citizen_association}=${param_not_used}
    ...     ${permanent_address_neighbourhood_association}=${param_not_used}
    ...     ${permanent_address_address}=${param_not_used}
    ...     ${permanent_address_address_local}=${param_not_used}
    ...     ${permanent_address_commune}=${param_not_used}
    ...     ${permanent_address_commune_local}=${param_not_used}
    ...     ${permanent_address_district}=${param_not_used}
    ...     ${permanent_address_district_local}=${param_not_used}
    ...     ${permanent_address_city}=${param_not_used}
    ...     ${permanent_address_city_local}=${param_not_used}
    ...     ${permanent_address_province}=${param_not_used}
    ...     ${permanent_address_province_local}=${param_not_used}
    ...     ${permanent_address_postal_code}=${param_not_used}
    ...     ${permanent_address_postal_code_local}=${param_not_used}
    ...     ${permanent_address_country}=${param_not_used}
    ...     ${permanent_address_country_local}=${param_not_used}
    ...     ${permanent_address_landmark}=${param_not_used}
    ...     ${permanent_address_longitude}=${param_not_used}
    ...     ${permanent_address_latitude}=${param_not_used}
    ...     ${primary_identity_type}=${param_not_used}
    ...     ${primary_identity_status}=${param_not_used}
    ...     ${primary_identity_identity_id}=${param_not_used}
    ...     ${primary_identity_identity_id_local}=${param_not_used}
    ...     ${primary_identity_place_of_issue}=${param_not_used}
    ...     ${primary_identity_issue_date}=${param_not_used}
    ...     ${primary_identity_expired_date}=${param_not_used}
    ...     ${primary_identity_front_identity_url}=${param_not_used}
    ...     ${primary_identity_back_identity_url}=${param_not_used}
    ...     ${primary_identity_signature_url}=${param_not_used}
    ...     ${secondary_identity_type}=${param_not_used}
    ...     ${secondary_identity_status}=${param_not_used}
    ...     ${secondary_identity_identity_id}=${param_not_used}
    ...     ${secondary_identity_identity_id_local}=${param_not_used}
    ...     ${secondary_identity_place_of_issue}=${param_not_used}
    ...     ${secondary_identity_issue_date}=${param_not_used}
    ...     ${secondary_identity_expired_date}=${param_not_used}
    ...     ${secondary_identity_front_identity_url}=${param_not_used}
    ...     ${secondary_identity_back_identity_url}=${param_not_used}
    ...     ${secondary_identity_signature_url}=${param_not_used}
    ...     ${tertiary_identity_type}=${param_not_used}
    ...     ${tertiary_identity_status}=${param_not_used}
    ...     ${tertiary_identity_identity_id}=${param_not_used}
    ...     ${tertiary_identity_identity_id_local}=${param_not_used}
    ...     ${tertiary_identity_place_of_issue}=${param_not_used}
    ...     ${tertiary_identity_issue_date}=${param_not_used}
    ...     ${tertiary_identity_expired_date}=${param_not_used}
    ...     ${tertiary_identity_front_identity_url}=${param_not_used}
    ...     ${tertiary_identity_back_identity_url}=${param_not_used}
    ...     ${tertiary_identity_signature_url}=${param_not_used}
    ...     ${kyc_level}=${param_not_used}
    ...     ${kyc_remark}=${param_not_used}
    ...     ${kyc_verify_by}=${param_not_used}
    ...     ${kyc_verify_date}=${param_not_used}
    ...     ${kyc_risk_level}=${param_not_used}
    ...     ${additional_profile_picture_url}=${param_not_used}
    ...     ${additional_tax_id_card_photo_url}=${param_not_used}
    ...     ${additional_field_1_name}=${param_not_used}
    ...     ${additional_field_1_value}=${param_not_used}
    ...     ${additional_field_2_name}=${param_not_used}
    ...     ${additional_field_2_value}=${param_not_used}
    ...     ${additional_field_3_name}=${param_not_used}
    ...     ${additional_field_3_value}=${param_not_used}
    ...     ${additional_field_4_name}=${param_not_used}
    ...     ${additional_field_4_value}=${param_not_used}
    ...     ${additional_field_5_name}=${param_not_used}
    ...     ${additional_field_5_value}=${param_not_used}
    ...     ${additional_supporting_file_1_url}=${param_not_used}
    ...     ${additional_supporting_file_2_url}=${param_not_used}
    ...     ${additional_supporting_file_3_url}=${param_not_used}
    ...     ${additional_supporting_file_4_url}=${param_not_used}
    ...     ${additional_supporting_file_5_url}=${param_not_used}
    ...     ${identity_auto_generate_password}=${param_not_used}
    ...     ${employee_id}=${param_not_used}
    ...     ${source_of_funds}=${param_not_used}
    ...     ${additional_region_id}=${param_not_used}
    ...     ${additional_sss_number}=${param_not_used}
    ...     ${additional_gsis_number}=${param_not_used}
    ...     ${additional_tax_income_number}=${param_not_used}
    ...     ${additional_philhealth_number}=${param_not_used}
    ...     ${additional_hdmf_number}=${param_not_used}
    ...     ${additional_additional_id_type}=${param_not_used}
    ...     ${additional_id_number}=${param_not_used}
    ...     ${payroll_card_number}=${param_not_used}
    ...     ${header_device_id}=${param_not_used}
    ...     ${header_device_description}=${param_not_used}
    ...     ${header_device_imei}=${param_not_used}
    ...     ${header_device_ip4}=${param_not_used}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}

    ${dic}  API system user create customer
    ...     ${identity_username}
    ...     ${identity_password}
    ...     ${identity_identity_type_id}
    ...     ${access_token}
    ...     ${primary_mobile_number}
    ...     ${unique_reference}
    ...     ${is_testing_account}
    ...     ${is_system_account}
    ...     ${acquisition_source}
    ...     ${customer_classification_id}
    ...     ${referrer_user_type_id}
    ...     ${referrer_user_type_name}
    ...     ${referrer_user_id}
    ...     ${beneficiary}
    ...     ${mm_card_type_id}
    ...     ${mm_card_level_id}
    ...     ${mm_factory_card_number}
    ...     ${is_require_otp}
    ...     ${tin_number}
    ...     ${tin_number_local}
    ...     ${title}
    ...     ${title_local}
    ...     ${first_name}
    ...     ${first_name_local}
    ...     ${middle_name}
    ...     ${middle_name_local}
    ...     ${last_name}
    ...     ${last_name_local}
    ...     ${suffix}
    ...     ${suffix_local}
    ...     ${date_of_birth}
    ...     ${place_of_birth}
    ...     ${place_of_birth_local}
    ...     ${gender}
    ...     ${gender_local}
    ...     ${ethnicity}
    ...     ${employer}
    ...     ${nationality}
    ...     ${occupation}
    ...     ${occupation_local}
    ...     ${occupation_title}
    ...     ${occupation_title_local}
    ...     ${township_code}
    ...     ${township_name}
    ...     ${township_name_local}
    ...     ${mother_name}
    ...     ${mother_name_local}
    ...     ${mother_maiden_name}
    ...     ${mother_maiden_name_local}
    ...     ${civil_status}
    ...     ${email}
    ...     ${secondary_mobile_number}
    ...     ${tertiary_mobile_number}
    ...     ${telephone_number}
    ...     ${current_address_citizen_association}
    ...     ${current_address_neighbourhood_association}
    ...     ${current_address_address}
    ...     ${current_address_address_local}
    ...     ${current_address_commune}
    ...     ${current_address_commune_local}
    ...     ${current_address_district}
    ...     ${current_address_district_local}
    ...     ${current_address_city}
    ...     ${current_address_city_local}
    ...     ${current_address_province}
    ...     ${current_address_province_local}
    ...     ${current_address_postal_code}
    ...     ${current_address_postal_code_local}
    ...     ${current_address_country}
    ...     ${current_address_country_local}
    ...     ${current_address_landmark}
    ...     ${current_address_longitude}
    ...     ${current_address_latitude}
    ...     ${permanent_address_citizen_association}
    ...     ${permanent_address_neighbourhood_association}
    ...     ${permanent_address_address}
    ...     ${permanent_address_address_local}
    ...     ${permanent_address_commune}
    ...     ${permanent_address_commune_local}
    ...     ${permanent_address_district}
    ...     ${permanent_address_district_local}
    ...     ${permanent_address_city}
    ...     ${permanent_address_city_local}
    ...     ${permanent_address_province}
    ...     ${permanent_address_province_local}
    ...     ${permanent_address_postal_code}
    ...     ${permanent_address_postal_code_local}
    ...     ${permanent_address_country}
    ...     ${permanent_address_country_local}
    ...     ${permanent_address_landmark}
    ...     ${permanent_address_longitude}
    ...     ${permanent_address_latitude}
    ...     ${primary_identity_type}
    ...     ${primary_identity_status}
    ...     ${primary_identity_identity_id}
    ...     ${primary_identity_identity_id_local}
    ...     ${primary_identity_place_of_issue}
    ...     ${primary_identity_issue_date}
    ...     ${primary_identity_expired_date}
    ...     ${primary_identity_front_identity_url}
    ...     ${primary_identity_back_identity_url}
    ...     ${primary_identity_signature_url}
    ...     ${secondary_identity_type}
    ...     ${secondary_identity_status}
    ...     ${secondary_identity_identity_id}
    ...     ${secondary_identity_identity_id_local}
    ...     ${secondary_identity_place_of_issue}
    ...     ${secondary_identity_issue_date}
    ...     ${secondary_identity_expired_date}
    ...     ${secondary_identity_front_identity_url}
    ...     ${secondary_identity_back_identity_url}
    ...     ${secondary_identity_signature_url}
    ...     ${tertiary_identity_type}
    ...     ${tertiary_identity_status}
    ...     ${tertiary_identity_identity_id}
    ...     ${tertiary_identity_identity_id_local}
    ...     ${tertiary_identity_place_of_issue}
    ...     ${tertiary_identity_issue_date}
    ...     ${tertiary_identity_expired_date}
    ...     ${tertiary_identity_front_identity_url}
    ...     ${tertiary_identity_back_identity_url}
    ...     ${tertiary_identity_signature_url}
    ...     ${kyc_level}
    ...     ${kyc_remark}
    ...     ${kyc_verify_by}
    ...     ${kyc_verify_date}
    ...     ${kyc_risk_level}
    ...     ${additional_profile_picture_url}
    ...     ${additional_tax_id_card_photo_url}
    ...     ${additional_field_1_name}
    ...     ${additional_field_1_value}
    ...     ${additional_field_2_name}
    ...     ${additional_field_2_value}
    ...     ${additional_field_3_name}
    ...     ${additional_field_3_value}
    ...     ${additional_field_4_name}
    ...     ${additional_field_4_value}
    ...     ${additional_field_5_name}
    ...     ${additional_field_5_value}
    ...     ${additional_supporting_file_1_url}
    ...     ${additional_supporting_file_2_url}
    ...     ${additional_supporting_file_3_url}
    ...     ${additional_supporting_file_4_url}
    ...     ${additional_supporting_file_5_url}
    ...     ${identity_auto_generate_password}
    ...     ${employee_id}
    ...     ${source_of_funds}
    ...     ${additional_region_id}
    ...     ${additional_sss_number}
    ...     ${additional_gsis_number}
    ...     ${additional_tax_income_number}
    ...     ${additional_philhealth_number}
    ...     ${additional_hdmf_number}
    ...     ${additional_additional_id_type}
    ...     ${additional_id_number}
    ...     ${payroll_card_number}
    ...     ${header_device_id}
    ...     ${header_device_description}
    ...     ${header_device_imei}
    ...     ${header_device_ip4}
    ...     ${header_client_id}
    ...     ${header_client_secret}
	${response}=    get from dictionary    ${dic}   response
	should be equal as integers     ${response.status_code}     200
	set to dictionary       ${dic}      id   ${response.json()['data']['id']}
	[Return]    ${dic}

#[200] API register customer
#    [Arguments]
#    ...     ${identity_username}
#    ...     ${identity_password}
#    ...     ${identity_identity_type_id}
#    ...     ${access_token}
#    ...     ${primary_mobile_number}
#    ...     ${unique_reference}
#    ...     ${header_device_id}=${param_not_used}
#    ...     ${header_device_description}=${param_not_used}
#    ...     ${referrer_user_type_id}=${param_not_used}
#    ...     ${referrer_user_type_name}=${param_not_used}
#    ...     ${referrer_user_id}=${param_not_used}
#    ...     ${beneficiary}=${param_not_used}
#    ...     ${mm_card_type_id}=${param_not_used}
#    ...     ${mm_card_level_id}=${param_not_used}
#    ...     ${mm_factory_card_number}=${param_not_used}
#    ...     ${is_require_otp}=${param_not_used}
#    ...     ${tin_number}=${param_not_used}
#    ...     ${title}=${param_not_used}
#    ...     ${first_name}=${param_not_used}
#    ...     ${middle_name}=${param_not_used}
#    ...     ${last_name}=${param_not_used}
#    ...     ${suffix}=${param_not_used}
#    ...     ${date_of_birth}=${param_not_used}
#    ...     ${place_of_birth}=${param_not_used}
#    ...     ${gender}=${param_not_used}
#    ...     ${ethnicity}=${param_not_used}
#    ...     ${employer}=${param_not_used}
#    ...     ${nationality}=${param_not_used}
#    ...     ${occupation}=${param_not_used}
#    ...     ${occupation_title}=${param_not_used}
#    ...     ${township_code}=${param_not_used}
#    ...     ${township_name}=${param_not_used}
#    ...     ${mother_name}=${param_not_used}
#    ...     ${mother_maiden_name}=${param_not_used}
#    ...     ${civil_status}=${param_not_used}
#    ...     ${email}=${param_not_used}
#    ...     ${secondary_mobile_number}=${param_not_used}
#    ...     ${tertiary_mobile_number}=${param_not_used}
#    ...     ${telephone_number}=${param_not_used}
#    ...     ${current_address_citizen_association}=${param_not_used}
#    ...     ${current_address_neighbourhood_association}=${param_not_used}
#    ...     ${current_address_address}=${param_not_used}
#    ...     ${current_address_commune}=${param_not_used}
#    ...     ${current_address_district}=${param_not_used}
#    ...     ${current_address_city}=${param_not_used}
#    ...     ${current_address_province}=${param_not_used}
#    ...     ${current_address_postal_code}=${param_not_used}
#    ...     ${current_address_country}=${param_not_used}
#    ...     ${current_address_landmark}=${param_not_used}
#    ...     ${current_address_longitude}=${param_not_used}
#    ...     ${current_address_latitude}=${param_not_used}
#    ...     ${permanent_address_citizen_association}=${param_not_used}
#    ...     ${permanent_address_neighbourhood_association}=${param_not_used}
#    ...     ${permanent_address_address}=${param_not_used}
#    ...     ${permanent_address_commune}=${param_not_used}
#    ...     ${permanent_address_district}=${param_not_used}
#    ...     ${permanent_address_city}=${param_not_used}
#    ...     ${permanent_address_province}=${param_not_used}
#    ...     ${permanent_address_postal_code}=${param_not_used}
#    ...     ${permanent_address_country}=${param_not_used}
#    ...     ${permanent_address_landmark}=${param_not_used}
#    ...     ${permanent_address_longitude}=${param_not_used}
#    ...     ${permanent_address_latitude}=${param_not_used}
#    ...     ${primary_identity_type}=${param_not_used}
#    ...     ${primary_identity_identity_id}=${param_not_used}
#    ...     ${primary_identity_place_of_issue}=${param_not_used}
#    ...     ${primary_identity_issue_date}=${param_not_used}
#    ...     ${primary_identity_expired_date}=${param_not_used}
#    ...     ${primary_identity_front_identity_url}=${param_not_used}
#    ...     ${primary_identity_back_identity_url}=${param_not_used}
#    ...     ${primary_identity_signature_url}=${param_not_used}
#    ...     ${secondary_identity_type}=${param_not_used}
#    ...     ${secondary_identity_identity_id}=${param_not_used}
#    ...     ${secondary_identity_place_of_issue}=${param_not_used}
#    ...     ${secondary_identity_issue_date}=${param_not_used}
#    ...     ${secondary_identity_expired_date}=${param_not_used}
#    ...     ${secondary_identity_front_identity_url}=${param_not_used}
#    ...     ${secondary_identity_back_identity_url}=${param_not_used}
#    ...     ${secondary_identity_signature_url}=${param_not_used}
#    ...     ${tertiary_identity_type}=${param_not_used}
#    ...     ${tertiary_identity_identity_id}=${param_not_used}
#    ...     ${tertiary_identity_place_of_issue}=${param_not_used}
#    ...     ${tertiary_identity_issue_date}=${param_not_used}
#    ...     ${tertiary_identity_expired_date}=${param_not_used}
#    ...     ${tertiary_identity_front_identity_url}=${param_not_used}
#    ...     ${tertiary_identity_back_identity_url}=${param_not_used}
#    ...     ${tertiary_identity_signature_url}=${param_not_used}
#    ...     ${additional_profile_picture_url}=${param_not_used}
#    ...     ${additional_tax_id_card_photo_url}=${param_not_used}
#    ...     ${identity_auto_generate_password}=${param_not_used}
#    ...     ${header_client_id}=${setup_admin_client_id}
#    ...     ${header_client_secret}=${setup_admin_client_secret}
#    ...     ${header_device_imei}=${param_not_used}
#    ...     ${header_device_ip4}=${param_not_used}
#    ${dic}  API register customer
#    ...     ${identity_username}
#    ...     ${identity_password}
#    ...     ${identity_identity_type_id}
#    ...     ${access_token}
#    ...     ${primary_mobile_number}
#    ...     ${unique_reference}
#    ...     ${header_device_id}
#    ...     ${header_device_description}
#    ...     ${referrer_user_type_id}
#    ...     ${referrer_user_type_name}
#    ...     ${referrer_user_id}
#    ...     ${beneficiary}
#    ...     ${mm_card_type_id}
#    ...     ${mm_card_level_id}
#    ...     ${mm_factory_card_number}
#    ...     ${is_require_otp}
#    ...     ${tin_number}
#    ...     ${title}
#    ...     ${first_name}
#    ...     ${middle_name}
#    ...     ${last_name}
#    ...     ${suffix}
#    ...     ${date_of_birth}
#    ...     ${place_of_birth}
#    ...     ${gender}
#    ...     ${ethnicity}
#    ...     ${employer}
#    ...     ${nationality}
#    ...     ${occupation}
#    ...     ${occupation_title}
#    ...     ${township_code}
#    ...     ${township_name}
#    ...     ${mother_name}
#    ...     ${mother_maiden_name}
#    ...     ${civil_status}
#    ...     ${email}
#    ...     ${secondary_mobile_number}
#    ...     ${tertiary_mobile_number}
#    ...     ${telephone_number}
#    ...     ${current_address_citizen_association}
#    ...     ${current_address_neighbourhood_association}
#    ...     ${current_address_address}
#    ...     ${current_address_commune}
#    ...     ${current_address_district}
#    ...     ${current_address_city}
#    ...     ${current_address_province}
#    ...     ${current_address_postal_code}
#    ...     ${current_address_country}
#    ...     ${current_address_landmark}
#    ...     ${current_address_longitude}
#    ...     ${current_address_latitude}
#    ...     ${permanent_address_citizen_association}
#    ...     ${permanent_address_neighbourhood_association}
#    ...     ${permanent_address_address}
#    ...     ${permanent_address_commune}
#    ...     ${permanent_address_district}
#    ...     ${permanent_address_city}
#    ...     ${permanent_address_province}
#    ...     ${permanent_address_postal_code}
#    ...     ${permanent_address_country}
#    ...     ${permanent_address_landmark}
#    ...     ${permanent_address_longitude}
#    ...     ${permanent_address_latitude}
#    ...     ${primary_identity_type}
#    ...     ${primary_identity_identity_id}
#    ...     ${primary_identity_place_of_issue}
#    ...     ${primary_identity_issue_date}
#    ...     ${primary_identity_expired_date}
#    ...     ${primary_identity_front_identity_url}
#    ...     ${primary_identity_back_identity_url}
#    ...     ${primary_identity_signature_url}
#    ...     ${secondary_identity_type}
#    ...     ${secondary_identity_identity_id}
#    ...     ${secondary_identity_place_of_issue}
#    ...     ${secondary_identity_issue_date}
#    ...     ${secondary_identity_expired_date}
#    ...     ${secondary_identity_front_identity_url}
#    ...     ${secondary_identity_back_identity_url}
#    ...     ${secondary_identity_signature_url}
#    ...     ${tertiary_identity_type}
#    ...     ${tertiary_identity_identity_id}
#    ...     ${tertiary_identity_place_of_issue}
#    ...     ${tertiary_identity_issue_date}
#    ...     ${tertiary_identity_expired_date}
#    ...     ${tertiary_identity_front_identity_url}
#    ...     ${tertiary_identity_back_identity_url}
#    ...     ${tertiary_identity_signature_url}
#    ...     ${additional_profile_picture_url}
#    ...     ${additional_tax_id_card_photo_url}
#    ...     ${identity_auto_generate_password}
#    ...     ${header_client_id}
#    ...     ${header_client_secret}
#    ...     ${header_device_imei}
#    ...     ${header_device_ip4}
#	${response}=    get from dictionary    ${dic}   response
#	should be equal as integers     ${response.status_code}     200
#	set to dictionary       ${dic}      id   ${response.json()['data']['id']}
#	[Return]    ${dic}

[200] API register customer
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id         ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      header_device_id         ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_id     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_name     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_id     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      beneficiary     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mm_card_type_id     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mm_card_level_id     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mm_factory_card_number     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_require_otp     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tin_number     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tin_number_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      title     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      title_local     ${param_not_used}
##    [Common] - Set default value for keyword in dictionary     ${arg_dic}      first_name     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      first_name_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      middle_name     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      middle_name_local     ${param_not_used}
##    [Common] - Set default value for keyword in dictionary     ${arg_dic}      last_name     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      last_name_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      suffix     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      suffix_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      date_of_birth     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      place_of_birth     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      place_of_birth_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      gender     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      gender_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      ethnicity     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      employer     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      nationality     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      occupation     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      occupation_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      occupation_title     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      occupation_title_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      township_code     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      township_name     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      township_name_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mother_name     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mother_name_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mother_maiden_name     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mother_maiden_name_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      civil_status     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      email     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_mobile_number     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_mobile_number     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tertiary_mobile_number     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      telephone_number     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      current_address_citizen_association     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      current_address_neighbourhood_association     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      current_address_address     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      current_address_address_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      current_address_commune     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      current_address_commune_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      current_address_district     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      current_address_district_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      current_address_city     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      current_address_city_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      current_address_province     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      current_address_province_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      current_address_postal_code     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      current_address_postal_code_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      current_address_country     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      current_address_country_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      current_address_landmark     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      current_address_longitude     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      current_address_latitude     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      permanent_address_citizen_association     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      permanent_address_neighbourhood_association     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      permanent_address_address     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      permanent_address_address_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      permanent_address_commune     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      permanent_address_commune_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      permanent_address_district     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      permanent_address_district_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      permanent_address_city     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      permanent_address_city_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      permanent_address_province     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      permanent_address_province_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      permanent_address_postal_code     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      permanent_address_postal_code_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      permanent_address_country     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      permanent_address_country_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      permanent_address_landmark     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      permanent_address_longitude     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      permanent_address_latitude     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_type     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_identity_id     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_identity_id_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_place_of_issue     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_issue_date     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_expired_date     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_front_identity_url     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_back_identity_url     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_signature_url     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_identity_type     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_identity_identity_id     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_identity_identity_id_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_identity_place_of_issue     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_identity_issue_date     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_identity_expired_date     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_identity_front_identity_url     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_identity_back_identity_url     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_identity_signature_url     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tertiary_identity_type     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tertiary_identity_identity_id     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tertiary_identity_identity_id_local     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tertiary_identity_place_of_issue     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tertiary_identity_issue_date     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tertiary_identity_expired_date     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tertiary_identity_front_identity_url     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tertiary_identity_back_identity_url     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tertiary_identity_signature_url     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      additional_profile_picture_url     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      additional_tax_id_card_photo_url     ${param_not_used}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      identity_auto_generate_password     ${param_not_used}

    ${dic}     API register customer    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

Fail] API register customer with device_id
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id         ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_id     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_name     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_id     ${param_not_used}

    ${dic}     API register customer    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     400     ${response.status_code}
    should be equal as strings      unauthorized_device     ${response.json()['status']['code']}
    should be equal as strings      Device is not allow to registration     ${response.json()['status']['message']}

    [Return]    ${dic}

[200] API system user creates customer profile
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_member      true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_testing_account      false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_system_account      false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_id     3
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_name     system-user
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_require_otp     false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_type       National ID
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tertiary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      kyc_level     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      kyc_risk_level     Low
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API system user creates customer profile    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[Fail] API system user creates customer profile
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_testing_account      false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_system_account      false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_id     3
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_name     system-user
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_require_otp     false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_type       National ID
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tertiary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      kyc_level     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      kyc_risk_level     Low
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API system user creates customer profile    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}

[200] API customer registers customer profile
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_id     3
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_name     system-user
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_require_otp     false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_type       National ID
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API customer registers customer profile    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[Fail] API customer registers customer profile
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_id     3
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_name     system-user
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_require_otp     false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_type       National ID
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API customer registers customer profile    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API system user updates customer profile
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API system user updates customer profile    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200

[Fail] API system user updates customer profile
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_testing_account      false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_system_account      false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_id     3
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_name     system-user
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_require_otp     false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_type       National ID
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tertiary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      kyc_level     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      kyc_risk_level     Low
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API system user updates customer profile    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}

[200] API system user updates customer profile with PATCH method
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_testing_account      false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_system_account      false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_id     3
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_name     system-user
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_require_otp     false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_type       National ID
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tertiary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      kyc_level     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      kyc_risk_level     Low
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API system user updates customer profile with PATCH method    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200

[Fail] API system user updates customer profile with PATCH method
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_testing_account      false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_system_account      false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_id     3
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_name     system-user
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_require_otp     false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_type       National ID
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tertiary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      kyc_level     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      kyc_risk_level     Low
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API system user updates customer profile with PATCH method    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}

[200] API customer updates customer profile
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_id     3
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_name     system-user
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_require_otp     false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_type       National ID
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API customer updates customer profile    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API customer updates customer profile
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_id     3
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_name     system-user
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_require_otp     false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_type       National ID
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API customer updates customer profile    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API customer updates customer profile with PATCH method
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_id     3
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_name     system-user
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_require_otp     false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_type       National ID
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API customer updates customer profile with PATCH method    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API customer updates customer profile with PATCH method
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_id     3
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_name     system-user
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_require_otp     false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_type       National ID
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API customer updates customer profile with PATCH method    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API delete customer
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API delete customer    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API delete customer
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API delete customer    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    ${arg_dic.code}
    [Return]   ${dic}

[200] API update customer kyc
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      kyc_level     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      kyc_risk_level     Low
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update customer kyc   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200

[Fail] API update customer kyc
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      kyc_level     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      kyc_risk_level     Low
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update customer kyc   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}

[200] API reset customer password
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API reset customer password   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API reset customer password
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API reset customer password   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API delete customer identity
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}  API delete customer identity    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API delete customer identity
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}  API delete customer identity    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    ${arg_dic.code}
    [Return]   ${dic}

[200] API update customer password
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API update customer password   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API update customer password
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API update customer password   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API update customer identity
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update customer identity   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API update customer identity
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update customer identity   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API customer creates device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API customer creates device  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[Fail] API customer creates device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API customer creates device  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API customer updates device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API customer updates device  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API customer updates device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API customer updates device  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API system user updates customer device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API system user updates customer device  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API system user updates customer device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API system user updates customer device  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API system user updates customer device status
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_active     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API system user updates customer device status  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API system user updates customer device status
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_active     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API system user updates customer device status   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API customer deletes device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API customer deletes device  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API customer deletes device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API customer deletes device  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API system user deletes customer device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API system user deletes customer device    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API system user deletes customer device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API system user deletes customer device    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    ${arg_dic.code}
    [Return]   ${dic}

[200] API customer gets all devices
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API customer gets all devices  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API customer gets all devices
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API customer gets all devices  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API system user gets customer device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API system user gets customer device   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[Fail] API system user gets customer device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API system user gets customer device  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}