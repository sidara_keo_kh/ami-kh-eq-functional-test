*** Settings ***
Resource         ../1_raw-deprecated/imports.robot

*** Keywords ***
[200] API generate OTP
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id              ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret          ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      email                  ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mobile_number          ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_generate_ref_code   false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      device_id              123456
    ${dic}     API generate OTP   ${arg_dic}
    ${response}    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    log            ${response.text}
    [Return]    ${dic}

[200] API validate OTP
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      user_reference_code   ${param_not_used}
    ${dic}      API validate OTP   ${arg_dic}
    ${response}    get from dictionary    ${dic}   response
    log            ${response.text}         level=INFO
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API search OTP
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id          ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret      ${setup_admin_client_secret}
    ${dic}      API search OTP   &{arg_dic}
    ${response}    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    log            ${response.text}
    [Return]    ${dic}

[200] API get OTP detail on mock
    [Arguments]     ${arg_dic}
    ${dic}     API get OTP detail on mock    ${arg_dic}
    ${response}    get from dictionary    ${dic}   response
    ${otp_code}   set variable    ${response.json()['data']['otp_code']}
    Set To Dictionary    ${dic}    otp_code  ${otp_code}
    ${user_reference_code}   set variable    ${response.json()['data']['user_reference_code']}
    Set To Dictionary    ${dic}    user_reference_code  ${user_reference_code}
    should be equal as integers     ${response.status_code}     200
    log            ${response.text}         level=INFO
    [Return]    ${dic}

[200] API regenerate OTP
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API regenerate OTP                      ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
   set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}
