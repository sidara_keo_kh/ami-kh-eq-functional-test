*** Settings ***
Resource         ../1_raw-deprecated/imports.robot

*** Keywords ***
[200] API create bank
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    bin       ${param_not_used}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    description       ${param_not_used}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    debit_url       ${param_not_used}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    credit_url       ${param_not_used}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    bank_account_number       ${param_not_used}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    bank_account_name       ${param_not_used}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    cancel_url       ${param_not_used}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    check_status_url       ${param_not_used}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    pre_sof_url       ${param_not_used}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    pre_sof_read_timeout       ${param_not_used}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    read_timeout       ${param_not_used}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    access_token       ${suite_admin_access_token}
    ${dic}  API create bank    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[200] API update bank
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    bin       ${param_not_used}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    description       ${param_not_used}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    debit_url       ${param_not_used}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    credit_url       ${param_not_used}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    bank_account_number       ${param_not_used}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    bank_account_name       ${param_not_used}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    cancel_url       ${param_not_used}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    check_status_url       ${param_not_used}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    pre_sof_url       ${param_not_used}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    pre_sof_read_timeout       ${param_not_used}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    read_timeout       ${param_not_used}
    ${dic}  AIP update bank    ${arg_dic}
    should be equal as integers     ${dic.response.status_code}     200
    [Return]    ${dic}

[200] API delete bank
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary   ${arg_dic}    client_secret       ${setup_admin_client_secret}
    ${dic}      API delete bank   ${arg_dic}
    should be equal as integers     ${dic.response.status_code}     200
    [Return]    ${dic}