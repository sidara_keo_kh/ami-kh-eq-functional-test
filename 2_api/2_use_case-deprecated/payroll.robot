*** Settings ***
Resource         ../1_raw-deprecated/imports.robot

*** Keywords ***
[200] API bank agent create and execute payroll order
    [Arguments]    ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_name       robot_product_name
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_ref1       robot_product_ref1
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_ref2       robot_product_ref2
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_ref3       robot_product_ref3
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_ref4       robot_product_ref4
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_ref5       robot_product_ref5
    ${dic}  API bank agent create and execute payroll order    ${arg_dic}
    ${response}     get from dictionary         ${dic}      response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}     order_id     ${response.json()['data']['order_id']}
    [Return]    ${dic}

[200] API system create and execute payroll order
    [Arguments]    ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_name       robot_product_name
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_ref1       robot_product_ref1
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_ref2       robot_product_ref2
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_ref3       robot_product_ref3
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_ref4       robot_product_ref4
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_ref5       robot_product_ref5
    ${dic}  API system create and execute payroll order    ${arg_dic}
    ${response}     get from dictionary         ${dic}      response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}     order_id     ${response.json()['data']['order_id']}
    [Return]    ${dic}