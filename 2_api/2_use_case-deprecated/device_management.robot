*** Settings ***
Resource         ../1_raw-deprecated/imports.robot

*** Keywords ***
[200] API create channel
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API create channel    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[200] API delete channel
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API delete channel    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[200] API create channel access permission
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API create channel access permission    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[200] API revoke channel access permission
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}      API revoke channel access permission    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

