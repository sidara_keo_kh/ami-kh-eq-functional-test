*** Settings ***
Resource         ../1_raw-deprecated/imports.robot

*** Keywords ***
[200] API create prepaid card
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}     client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${dic}     API create prepaid card      ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[200] API create payroll card
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}     client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${dic}     API create payroll card      ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}


[200] API create virtual prepaid card
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       firstname       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       lastname       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       card_type_id       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       card_lifetime_in_month       ${param_not_used}


    ${dic}   API create virtual prepaid card         ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    set to dictionary    ${dic}    external_identifier   ${response.json()['data']['external_identifier']}
    set to dictionary    ${dic}    pan   ${response.json()['data']['pan']}
    set to dictionary    ${dic}    is_stopped   ${response.json()['data']['is_stopped']}
    set to dictionary    ${dic}    expiry_date   ${response.json()['data']['expiry_date']}
    set to dictionary    ${dic}    created_timestamp   ${response.json()['data']['created_timestamp']}
    [Return]   ${dic}

[200] API get full detail prepaid card
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API get full detail prepaid card         ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get prepaid card list
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API get prepaid card list         ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}


[200] API stop prepaid card
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       is_stopped       ${param_not_used}

    ${dic}   API stop prepaid card         ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API stop prepaid card
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       is_stopped       ${param_not_used}

    ${dic}   API stop prepaid card         ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    ${arg_dic.code}
    [Return]   ${dic}

[200] API admin stop prepaid card
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       is_stopped       ${param_not_used}

    ${dic}   API admin stop prepaid card         ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}


[200] API update card type
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       name       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       timeout_create_card       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       timeout_get_card_detail       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       create_card_endpoint_host       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       card_detail_endpoint_host       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       timeout_update_card_status       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       update_card_status_endpoint_host      ${param_not_used}

    ${dic}   API update card type         ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}