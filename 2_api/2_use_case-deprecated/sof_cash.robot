*** Settings ***
Resource          ../../2_api/2_use_case-deprecated/imports.robot
Resource          ../../3_prepare_data/by_api/imports.robot
Resource          ../../2_api/1_raw-deprecated/sof_cash.robot

*** Keywords ***
[200] API suspend sof cash
    [Arguments]
    ...     ${sof_cash_id}=${suite_agent_sof_id_VN}
    ...     ${access_token}=${suite_admin_access_token}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ...     ${is_suspended}=true
    ${dic}  API suspend sof cash
    ...     ${sof_cash_id}
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ...     ${is_suspended}
    ${response}     get from dictionary         ${dic}      response
    should be equal as integers     ${response.status_code}     200

    [Return]    ${dic}

[Fail] API suspend sof cash with unexist sofid
    [Arguments]
    ...     ${access_token}=${suite_admin_access_token}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ...     ${is_suspended}=true
    ${sof_cash_id}    generate random string    8    123456789
    ${dic}  API suspend sof cash
    ...     ${sof_cash_id}
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ...     ${is_suspended}
    ${response}     get from dictionary         ${dic}      response
    should be equal as integers     ${response.status_code}     400
    [Return]    ${dic}

[200] API active sof cash
    [Arguments]
    ...     ${sof_cash_id}=${suite_agent_sof_id_VN}
    ...     ${access_token}=${suite_admin_access_token}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ...     ${is_suspended}=false
    ${dic}  API suspend sof cash
    ...     ${sof_cash_id}
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ...     ${is_suspended}
    ${response}     get from dictionary         ${dic}      response
    should be equal as integers     ${response.status_code}     200

    [Return]    ${dic}

[200] API get sof cash detail
    [Arguments]
    ...     ${sof_cash_id}=${suite_agent_sof_id_VN}
    ...     ${access_token}=${suite_admin_access_token}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API get sof cash detail
    ...     ${sof_cash_id}
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}     get from dictionary         ${dic}      response
    should be equal as integers     ${response.status_code}     200

    [Return]    ${dic}

[200] API update sof cash prefill amount
    [Arguments]    ${dic}
    ${dic}  API update sof cash prefill amount    ${dic}
    ${response}     get from dictionary         ${dic}      response
    log    ${response.text}
    should be equal as integers     ${response.status_code}     200

    [Return]    ${dic}

[Fail] API update sof cash prefill amount
    [Arguments]    ${dic}
    ${dic}  API update sof cash prefill amount    ${dic}
    ${response}     get from dictionary         ${dic}      response
    log    ${response.text}
    should be equal as integers     ${response.status_code}     400
    [Return]    ${dic}