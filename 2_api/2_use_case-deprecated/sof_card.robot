*** Settings ***
Resource          ../../2_api/2_use_case-deprecated/imports.robot
Resource          ../../3_prepare_data/by_api/imports.robot

*** Keywords ***
[200] API add provider
    [Arguments]
    ...     ${name}
    ...     ${access_token}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API add provider
    ...     ${name}
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}     get from dictionary         ${dic}      response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}

    [Return]    ${dic}

[200] API add card design
    [Arguments]
    ...    ${provider_id}
    ...    ${card_design_name}
	...    ${card_type_id}
	...    ${is_active}
	...    ${currency}
	...    ${pattern}
	...    ${pre_link_url}
	...    ${pre_link_read_timeout}
	...    ${link_url}
	...    ${link_read_timeout}
	...    ${un_link_url}
	...    ${un_link_read_timeout}
	...    ${debit_url}
	...    ${debit_read_timeout}
	...    ${credit_url}
	...    ${credit_read_timeout}
	...    ${check_status_url}
	...    ${check_status_read_timeout}
	...    ${cancel_url}
	...    ${cancel_read_timeout}
	...    ${pre_sof_order_url}
	...    ${pre_sof_order_read_timeout}
    ...    ${access_token}
    ...    ${header_client_id}=${setup_admin_client_id}
    ...    ${header_client_secret}=${setup_admin_client_secret}

    ${dic}  API add card design
    ...    ${provider_id}
    ...    ${card_design_name}
	...    ${card_type_id}
	...    ${is_active}
	...    ${currency}
	...    ${pattern}
	...    ${pre_link_url}
	...    ${pre_link_read_timeout}
	...    ${link_url}
	...    ${link_read_timeout}
	...    ${un_link_url}
	...    ${un_link_read_timeout}
	...    ${debit_url}
	...    ${debit_read_timeout}
	...    ${credit_url}
	...    ${credit_read_timeout}
	...    ${check_status_url}
	...    ${check_status_read_timeout}
	...    ${cancel_url}
	...    ${cancel_read_timeout}
	...    ${pre_sof_order_url}
	...    ${pre_sof_order_read_timeout}
    ...    ${access_token}
    ...    ${header_client_id}
    ...    ${header_client_secret}
    ${response}     get from dictionary         ${dic}      response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}

    [Return]    ${dic}


[200] API get list of card types
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API get list of card types        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API add new provider
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API add new provider        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}


[200] API get details of provider
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API get details of provider        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API edit provider detail
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API edit provider detail        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}


[200] API get card design detail
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API get card design detail        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}


[200] API create card design
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}   name        name
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   card_type_id      1
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   is_active        false
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   currency        USD
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   pan_pattern        11441525
    [Common] - Set default value for keyword in dictionary     ${arg_dic}    pre_sof_order_url        null
    [Common] - Set default value for keyword in dictionary    ${arg_dic}     pre_sof_order_read_timeout        null
    [Common] - Set default value for keyword in dictionary     ${arg_dic}    pre_link_url        null
    [Common] - Set default value for keyword in dictionary     ${arg_dic}    pre_link_read_timeout        null
    [Common] - Set default value for keyword in dictionary     ${arg_dic}    link_url        null
    [Common] - Set default value for keyword in dictionary     ${arg_dic}    link_read_timeout        null
    [Common] - Set default value for keyword in dictionary    ${arg_dic}     un_link_url        null
    [Common] - Set default value for keyword in dictionary   ${arg_dic}      un_link_read_timeout        null
    [Common] - Set default value for keyword in dictionary   ${arg_dic}      debit_url        null
    [Common] - Set default value for keyword in dictionary    ${arg_dic}     debit_read_timeout       null
    [Common] - Set default value for keyword in dictionary    ${arg_dic}     credit_url        null
    [Common] - Set default value for keyword in dictionary     ${arg_dic}    credit_read_timeout        null
    [Common] - Set default value for keyword in dictionary     ${arg_dic}    check_status_url        null
    [Common] - Set default value for keyword in dictionary   ${arg_dic}      check_status_read_timeout        null
    [Common] - Set default value for keyword in dictionary    ${arg_dic}     cancel_url      null
    [Common] - Set default value for keyword in dictionary   ${arg_dic}      cancel_read_timeout        null
    [Common] - Set default value for keyword in dictionary    ${arg_dic}     pre_sof_check_status_url      null
    [Common] - Set default value for keyword in dictionary   ${arg_dic}      pre_sof_check_status_read_timeout        null
    [Common] - Set default value for keyword in dictionary   ${arg_dic}      pre_sof_check_status_max_retry        null


    ${dic}   API create card design        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get list of providers
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API get list of providers       ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get card design by PAN number
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API get card design by PAN number       ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API edit card design
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}   name        name
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   card_type_id      1
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   is_active        false
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   currency        USD
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   pan_pattern        11441525
    [Common] - Set default value for keyword in dictionary     ${arg_dic}    pre_sof_order_url        null
    [Common] - Set default value for keyword in dictionary    ${arg_dic}     pre_sof_order_read_timeout        null
    [Common] - Set default value for keyword in dictionary     ${arg_dic}    pre_link_url        null
    [Common] - Set default value for keyword in dictionary     ${arg_dic}    pre_link_read_timeout        null
    [Common] - Set default value for keyword in dictionary     ${arg_dic}    link_url        null
    [Common] - Set default value for keyword in dictionary     ${arg_dic}    link_read_timeout        null
    [Common] - Set default value for keyword in dictionary    ${arg_dic}     un_link_url        null
    [Common] - Set default value for keyword in dictionary   ${arg_dic}      un_link_read_timeout        null
    [Common] - Set default value for keyword in dictionary   ${arg_dic}      debit_url        null
    [Common] - Set default value for keyword in dictionary    ${arg_dic}     debit_read_timeout       null
    [Common] - Set default value for keyword in dictionary    ${arg_dic}     credit_url        null
    [Common] - Set default value for keyword in dictionary     ${arg_dic}    credit_read_timeout        null
    [Common] - Set default value for keyword in dictionary     ${arg_dic}    check_status_url        null
    [Common] - Set default value for keyword in dictionary   ${arg_dic}      check_status_read_timeout        null
    [Common] - Set default value for keyword in dictionary    ${arg_dic}     cancel_url      null
    [Common] - Set default value for keyword in dictionary   ${arg_dic}      cancel_read_timeout        null
    [Common] - Set default value for keyword in dictionary    ${arg_dic}     pre_sof_check_status_url      null
    [Common] - Set default value for keyword in dictionary   ${arg_dic}      pre_sof_check_status_read_timeout        null
    [Common] - Set default value for keyword in dictionary   ${arg_dic}      pre_sof_check_status_max_retry        null


    ${dic}   API edit card design        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}
