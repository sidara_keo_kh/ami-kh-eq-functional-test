*** Settings ***
Resource         ../1_raw-deprecated/imports.robot

*** Keywords ***
[200] API add service group
    [Arguments]
    ...     ${access_token}
    ...     ${service_group_name}
    ...     ${description}=robot_service_group_description
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}   API add service group
    ...     ${access_token}
    ...     ${service_group_name}
    ...     ${description}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary   ${dic}   service_group_id    ${response.json()['data']['service_group_id']}
    [Return]   ${dic}

[200] API add service group with all fields
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}   API Create Service Group With All Fields    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary   ${dic}   service_group_id    ${response.json()['data']['service_group_id']}
    [Return]   ${dic}

[200] API add profile wallet limitation
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}   API Create wallet limitation   ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary   ${dic}   profile_wallet_limitation_id    ${response.json()['data']['id']}
    [Return]   ${dic}

[200] API get a service group
    [Arguments]
     ...     ${access_token}
     ...     ${serviceGroupId}
     ...     ${header_client_id}=${setup_admin_client_id}
     ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}   API get a service group
     ...     ${access_token}
     ...     ${serviceGroupId}
     ...     ${header_client_id}
     ...     ${header_client_secret}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API remove service group
    [Arguments]
    ...     ${client_id}
    ...     ${client_secret}
    ...     ${access_token}
    ...     ${service_group_id}
    ${dic}   API remove service group
    ...     ${client_id}
    ...     ${client_secret}
    ...     ${access_token}
    ...     ${service_group_id}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get all service groups
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}      API get all service groups     &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API update service group
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API update service group     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API create service
    [Arguments]
    ...     ${access_token}
    ...     ${service_group_id}
    ...     ${service_name}
    ...     ${currency}
    ...     ${description}=robot_service_description
    ...     ${image_url}=${param_not_used}
    ...     ${display_name}=${param_not_used}
    ...     ${display_name_local}=${param_not_used}
    ...     ${apply_to_all_customer_classification}=${param_not_used}
    ...     ${list_service_customer_classification}=${EMPTY}
    ...     ${is_allow_debt}=${param_not_used}
    ... 	${header_client_id}=${setup_admin_client_id}
	... 	${header_client_secret}=${setup_admin_client_secret}
    ${dic}   API create service
    ...     ${access_token}
    ...     ${service_group_id}
    ...     ${service_name}
    ...     ${currency}
    ...     ${description}
    ...     ${image_url}
    ...     ${display_name}
    ...     ${display_name_local}
    ...     ${apply_to_all_customer_classification}
    ...     ${list_service_customer_classification}
    ...     ${is_allow_debt}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary   ${dic}  service_id  ${response.json()['data']['service_id']}
    [Return]   ${dic}

[200] API get service details
    [Arguments]
    ...     ${client_id}
    ...     ${client_secret}
    ...     ${access_token}
    ...     ${serviceId}
    ${dic}   API get service details
    ...     ${client_id}
    ...     ${client_secret}
    ...     ${access_token}
    ...     ${serviceId}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200

    ${service_name_contract}  ${service_name}     run keyword and ignore error    set variable    ${response.json()['data']['service_name']}
    set to dictionary   ${dic}      service_name_contract    ${service_name_contract}
    set to dictionary   ${dic}      service_name    ${service_name}

    ${description_contract}      ${description}     run keyword and ignore error    set variable    ${response.json()['data']['description']}
    set to dictionary   ${dic}      description     ${description}
    set to dictionary   ${dic}      description_contract     ${description_contract}

    ${currency_contract}   ${currency}     run keyword and ignore error    set variable    ${response.json()['data']['currency']}
    set to dictionary   ${dic}      currency     ${currency}
    set to dictionary   ${dic}      currency_contract     ${currency_contract}
    [Return]   ${dic}

[200] API get all services
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API get all services     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API update service
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API update service     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API active-suspend service
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API active-suspend service    &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get agent type service
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API get agent type service    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API delete service command
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API delete service command    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API create fee tier from A-O
    [Documentation]     example for @{list_fee_tier_data}
    ...     ${a}  set variable    "a":{"type_first":"Flat value","from_first":"","amount_first":100,"type_second":"% rate","from_second":"Amount","amount_second":10,"operator":"+"},
    ...     ${b}  set variable    "b":{"type_first":"Flat value","from_first":"","amount_first":100,"type_second":"% rate","from_second":"Amount","amount_second":10,"operator":"+"},
    ...     ${c}  set variable    "c":{"type_first":"Flat value","from_first":"","amount_first":100,"type_second":"% rate","from_second":"Amount","amount_second":10,"operator":"+"}
    ...     @{list_fee_tier_data}  create list     ${a}    ${b}    ${c}
    [Arguments]     ${arg_dic}

    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}

    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      fee_tier_condition   unlimit
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      condition_amount      ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      settlement_type   Amount
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      amount_type    ${param_not_used}

    [Common] - Set default value for keyword in dictionary     ${arg_dic}      fee_type        ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      fee_amount      ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      bonus_type      ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      bonus_amount    ${param_not_used}


    ${dic}      API create fee tier from A-O    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API update fee tier from A-O
    [Documentation]     example for @{list_fee_tier_data}
    ...     ${a}  set variable    "a":{"type_first":"Flat value","from_first":"","amount_first":100,"type_second":"% rate","from_second":"Amount","amount_second":10,"operator":"+"},
    ...     ${b}  set variable    "b":{"type_first":"Flat value","from_first":"","amount_first":100,"type_second":"% rate","from_second":"Amount","amount_second":10,"operator":"+"},
    ...     ${c}  set variable    "c":{"type_first":"Flat value","from_first":"","amount_first":100,"type_second":"% rate","from_second":"Amount","amount_second":10,"operator":"+"}
    ...     @{list_fee_tier_data}  create list     ${a}    ${b}    ${c}
    [Arguments]     ${arg_dic}      @{list_fee_tier_data}

    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      fee_tier_condition   unlimit
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      condition_amount     ${EMPTY}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      settlement_type   Amount

    ${dic}      API update fee tier from A-O    ${arg_dic}      @{list_fee_tier_data}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API delete fee tier
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API delete fee tier    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get list of tier from
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API get list of tier from    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get list fee tier conditions
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API get list fee tier conditions    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get all commands
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API get all commands    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get all action types
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API get all action types    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get all amount types
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API get all amount types    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get all actor types
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API get all actor types    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get all sof types
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API get all sof types    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API payment search service group by name
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API payment search service group by name     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API payment search service by name
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API payment search service by name     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get all spi url types
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API get all spi url types    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get all spi url call methods
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API get all spi url call methods    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get all payment methods
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API get all payment methods    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get SPI URL by service command
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API get SPI URL by service command    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get SPI URL by URL id
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API get SPI URL by URL id   ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API update SPI URL
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API update SPI URL    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API delete SPI URL
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API delete SPI URL    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get SPI URL configuration types
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API get SPI URL configuration types    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API create SPI URL configuration
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API create SPI URL configuration    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get SPI URL configuration details from ID
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API get SPI URL configuration details from ID    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API update SPI URL configuration
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API update SPI URL configuration    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API delete SPI URL configuration
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API delete SPI URL configuration    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get all Payment security types
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API get all Payment security types    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get all bonus type
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API get all bonus type    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get all fee type
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API get all fee type    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API update tier level
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}      API update tier level      ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get all banlance distributions
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}      API get all banlance distributions    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API delete balance distribution
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}      API delete balance distribution    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API payment create normal order
    [Arguments]
    ...     ${access_token}
    ...     ${ext_transaction_id}
    ...     ${product_service_id}
    ...     ${product_service_name}
    ...     ${product_name}
    ...     ${product_ref_1}
    ...     ${product_ref_2}
    ...     ${product_ref_3}
    ...     ${product_ref_4}
    ...     ${product_ref_5}
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${payer_user_id}
    ...     ${payer_user_type}
    ...     ${payee_user_ref_type}
    ...     ${payee_user_ref_value}
    ...     ${payee_user_id}
    ...     ${payee_user_type}
    ...     ${amount}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ...     ${header_device_unique_reference}=${param_not_used}
    ...     ${header_channel_id}=${param_not_used}
    ${dic}  API payment create normal order
    ...     ${access_token}
    ...     ${ext_transaction_id}
    ...     ${product_service_id}
    ...     ${product_service_name}
    ...     ${product_name}
    ...     ${product_ref_1}
    ...     ${product_ref_2}
    ...     ${product_ref_3}
    ...     ${product_ref_4}
    ...     ${product_ref_5}
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${payer_user_id}
    ...     ${payer_user_type}
    ...     ${payee_user_ref_type}
    ...     ${payee_user_ref_value}
    ...     ${payee_user_id}
    ...     ${payee_user_type}
    ...     ${amount}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ...     ${header_device_unique_reference}
    ...     ${header_channel_id}
    ${response}     get from dictionary         ${dic}      response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}     order_id     ${response.json()['data']['order_id']}
    [Return]    ${dic}

[Fail] API payment create normal order with service under blocked service group
    [Arguments]
    ...     ${access_token}
    ...     ${ext_transaction_id}
    ...     ${product_service_id}
    ...     ${product_service_name}
    ...     ${product_name}
    ...     ${product_ref_1}
    ...     ${product_ref_2}
    ...     ${product_ref_3}
    ...     ${product_ref_4}
    ...     ${product_ref_5}
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${payer_user_id}
    ...     ${payer_user_type}
    ...     ${payee_user_ref_type}
    ...     ${payee_user_ref_value}
    ...     ${payee_user_id}
    ...     ${payee_user_type}
    ...     ${amount}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API payment create normal order
    ...     ${access_token}
    ...     ${ext_transaction_id}
    ...     ${product_service_id}
    ...     ${product_service_name}
    ...     ${product_name}
    ...     ${product_ref_1}
    ...     ${product_ref_2}
    ...     ${product_ref_3}
    ...     ${product_ref_4}
    ...     ${product_ref_5}
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${payer_user_id}
    ...     ${payer_user_type}
    ...     ${payee_user_ref_type}
    ...     ${payee_user_ref_value}
    ...     ${payee_user_id}
    ...     ${payee_user_type}
    ...     ${amount}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}     get from dictionary         ${dic}      response
    log  ${response}
    should be equal as integers     ${response.status_code}                     400
    should be equal as strings      ${response.json()['status']['code']}        invalid_request
    should be equal as strings      ${response.json()['status']['message']}     Your remittance service is blocked please contact customer service for more details, however you can continue to use other services.
    [Return]    ${dic}

[Fail] API payment create normal order with invalid KYC level
    [Arguments]
    ...     ${access_token}
    ...     ${ext_transaction_id}
    ...     ${product_service_id}
    ...     ${product_service_name}
    ...     ${product_name}
    ...     ${product_ref_1}
    ...     ${product_ref_2}
    ...     ${product_ref_3}
    ...     ${product_ref_4}
    ...     ${product_ref_5}
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${payer_user_id}
    ...     ${payer_user_type}
    ...     ${payee_user_ref_type}
    ...     ${payee_user_ref_value}
    ...     ${payee_user_id}
    ...     ${payee_user_type}
    ...     ${amount}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API payment create normal order
    ...     ${access_token}
    ...     ${ext_transaction_id}
    ...     ${product_service_id}
    ...     ${product_service_name}
    ...     ${product_name}
    ...     ${product_ref_1}
    ...     ${product_ref_2}
    ...     ${product_ref_3}
    ...     ${product_ref_4}
    ...     ${product_ref_5}
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${payer_user_id}
    ...     ${payer_user_type}
    ...     ${payee_user_ref_type}
    ...     ${payee_user_ref_value}
    ...     ${payee_user_id}
    ...     ${payee_user_type}
    ...     ${amount}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}     get from dictionary         ${dic}      response
    log  ${response}
    should be equal as integers     ${response.status_code}                     400
    should be equal as strings      ${response.json()['status']['code']}        payment_not_allow
    should be equal as strings      ${response.json()['status']['message']}     Your current KYC level does not allow you to complete this transaction. Please contact customer service for more details.
    [Return]    ${dic}

[200] API get order detail
    [Arguments]
    ...     ${access_token}
    ...     ${order_id}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}=     API get order detail
    ...     ${access_token}
    ...     ${order_id}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}     product_name     ${response.json()['data']['product']['product_name']}
    set to dictionary    ${dic}     product_ref1     ${response.json()['data']['product']['product_ref1']}
    set to dictionary    ${dic}     product_ref2     ${response.json()['data']['product']['product_ref2']}
    set to dictionary    ${dic}     product_ref3     ${response.json()['data']['product']['product_ref3']}
    set to dictionary    ${dic}     product_ref4     ${response.json()['data']['product']['product_ref4']}
    set to dictionary    ${dic}     product_ref5     ${response.json()['data']['product']['product_ref5']}
    log     ${response.json()['data']}
    set to dictionary    ${dic}     service_name                ${response.json()['data']['product_service']['name']}
    set to dictionary    ${dic}     order_id                    ${response.json()['data']['order_id']}
    set to dictionary    ${dic}     user_id                     ${response.json()['data']['initiator_user']['id']}
    set to dictionary    ${dic}     ref_order_id                ${response.json()['data']['ref_order_id']}
    set to dictionary    ${dic}     short_order_id              ${response.json()['data']['short_order_id']}
    set to dictionary    ${dic}     created_unique_device       ${response.json()['data']['created_device_unique_reference']}
    set to dictionary    ${dic}     created_client_id           ${response.json()['data']['created_client_id']}
    set to dictionary    ${dic}     executed_client_id          ${response.json()['data']['executed_client_id']}
    set to dictionary    ${dic}     service_group               ${response.json()['data']['product_service_group']['name']}
    set to dictionary    ${dic}     ext_transaction_id          ${response.json()['data']['ext_transaction_id']}
    set to dictionary    ${dic}     payer_id                    ${response.json()['data']['payer_user']['id']}
    set to dictionary    ${dic}     payee_id                    ${response.json()['data']['payee_user']['id']}
    set to dictionary    ${dic}     requestor_id                ${response.json()['data']['requestor_user']['id']}
    set to dictionary    ${dic}     requestor_user_type         ${response.json()['data']['requestor_user']['type']}
    set to dictionary    ${dic}     requestor_sof_id            ${response.json()['data']['requestor_user']['sof_id']}
    set to dictionary    ${dic}     requestor_sof_type          ${response.json()['data']['requestor_user']['sof_type_id']}


    [Return]    ${dic}

[200] API payment execute order
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${dic}=     API payment execute order       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API payment execute order
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${return_dic}=     API payment execute order     ${arg_dic}
    ${response}=    get from dictionary    ${return_dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${return_dic}

[200] API create service command
    [Arguments]
    ...     ${access_token}
    ...     ${service_id}
    ...     ${command_id}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}=     API create service command
    ...     ${access_token}
    ...     ${service_id}
    ...     ${command_id}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API get service commands by service id
    [Arguments]
    ...     ${access_token}
    ...     ${service_id}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}=     API get service commands by service id
    ...     ${access_token}
    ...     ${service_id}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary  ${dic}    payment_service_command_id      ${response.json()['data'][0]['service_command_id']}
    run keyword and ignore error   set to dictionary  ${dic}    cancel_service_command_id      ${response.json()['data'][1]['service_command_id']}
    [Return]    ${dic}

[200] API get list fee tiers by service command id
    [Arguments]
    ...     ${access_token}
    ...     ${service_command_id}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}=     API get list fee tiers by service command id
    ...     ${access_token}
    ...     ${service_command_id}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary   ${dic}      fee_tier_id     ${response.json()['data'][0]['fee_tier_id']}
    [Return]    ${dic}

[200] API get list fee tiers by service command id with 2 fee tier configuration for service command
    [Arguments]
    ...     ${access_token}
    ...     ${service_command_id}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}=     API get list fee tiers by service command id
    ...     ${access_token}
    ...     ${service_command_id}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary   ${dic}      fee_tier_id_1     ${response.json()['data'][0]['fee_tier_id']}
    set to dictionary   ${dic}      fee_tier_id_2     ${response.json()['data'][1]['fee_tier_id']}
    [Return]    ${dic}

[200] API create fee tier
    [Arguments]
    ...     ${access_token}
    ...     ${service_command_id}
    ...     ${fee_tier_condition}
    ...     ${condition_amount}
    ...     ${fee_type}
    ...     ${fee_amount}
    ...     ${bonus_type}
    ...     ${bonus_amount}
    ...     ${amount_type}
    ...     ${settlement_type}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}=     API create fee tier
    ...     ${access_token}
    ...     ${service_command_id}
    ...     ${fee_tier_condition}
    ...     ${condition_amount}
    ...     ${fee_type}
    ...     ${fee_amount}
    ...     ${bonus_type}
    ...     ${bonus_amount}
    ...     ${amount_type}
    ...     ${settlement_type}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API create user sof cash
    [Arguments]
    ...     ${currency}
    ...     ${access_token}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}

    ${dic}=     API create user sof cash
    ...     ${currency}
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary   ${dic}   sof_id    ${response.json()['data']['sof_id']}
    [Return]    ${dic}

[200] API user get all his/her sof cash
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${dic}=     API user get all his/her sof cash       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API create SPI URL
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     header_client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     header_client_secret    ${setup_admin_client_secret}

    ${dic}=     API create SPI URL     ${arg_dic}

    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API system user create a company agent cash source of fund with currency
    [Arguments]
    ...     ${currency}
    ...     ${access_token}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}     API system user create a company agent cash source of fund with currency
    ...     ${currency}
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[None] API system user create a company agent cash source of fund with currency
    [Arguments]
    ...     ${currency}
    ...     ${access_token}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}     API system user create a company agent cash source of fund with currency
    ...     ${currency}
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    [Return]    ${dic}

[200] API system user update company agent cash balance
    [Arguments]
    ...     ${currency}
    ...     ${amount}
    ...     ${access_token}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API system user update company agent cash balance
    ...     ${currency}
    ...     ${amount}
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API system user get company agent cash balance history
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     access_token       ${suite_admin_access_token}
    ${dic}=     API system user get company agent cash balance history       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API update all balance distribution
    [Documentation]     example for @{balance_distributions}
    ...     ${1}  set variable    {"action_type":"Debit","actor_type":"Payer","sof_type_id":2,"specific_sof":null,"amount_type":"Amount","rate":null}
    ...     ${2}  set variable    {"action_type":"Credit","actor_type":"Payee","sof_type_id":2,"specific_sof":null,"amount_type":"Amount","rate":null}
    ...     ${3}  set variable    {"action_type":"Debit","actor_type":"Payee","sof_type_id":2,"specific_sof":null,"amount_type":"Amount","rate":null}
    ...     @{balance_distributions}  create list     ${1}    ${2}    ${3}

    [Arguments]
    ...     ${fee_tier_id}
    ...     ${access_token}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ...     @{balance_distributions}
    ${dic}      API update all banlance distribution
    ...     ${access_token}
    ...     ${fee_tier_id}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ...     @{balance_distributions}
    ${response}     get from dictionary     ${dic}  response
    should be equal as integers     ${response.status_code}    200
    [Return]    ${dic}

[200] API get order detail properties
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary         ${arg_dic}      access_token        ${suite_admin_access_token}
    ${dic}      API get order detail properties    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API payment create artifact order
    [Arguments]
    ...     ${access_token}
    ...     ${ext_transaction_id}
    ...     ${product_service_id}
    ...     ${product_service_name}
    ...     ${product_name}
    ...     ${product_ref_1}
    ...     ${product_ref_2}
    ...     ${product_ref_3}
    ...     ${product_ref_4}
    ...     ${product_ref_5}
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${payee_user_ref_type}
    ...     ${payee_user_ref_value}
    ...     ${payee_user_id}
    ...     ${payee_user_type}
    ...     ${amount}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ...     @{additional_references}

    ${dic}  API payment create artifact order
    ...     ${access_token}
    ...     ${ext_transaction_id}
    ...     ${product_service_id}
    ...     ${product_service_name}
    ...     ${product_name}
    ...     ${product_ref_1}
    ...     ${product_ref_2}
    ...     ${product_ref_3}
    ...     ${product_ref_4}
    ...     ${product_ref_5}
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${payee_user_ref_type}
    ...     ${payee_user_ref_value}
    ...     ${payee_user_id}
    ...     ${payee_user_type}
    ...     ${amount}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ...     @{additional_references}

    ${response}     get from dictionary         ${dic}      response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}     order_id     ${response.json()['data']['order_id']}
    [Return]    ${dic}

[200] API payment create trust order
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${product}       generate random string  20  [LETTERS]
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     product_name       robot_product_name_${product}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     product_ref_1      robot_product_ref_1_${product}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     product_ref_2      robot_product_ref_2_${product}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     product_ref_3      robot_product_ref_3_${product}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     product_ref_4      robot_product_ref_4_${product}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     product_ref_5      robot_product_ref_5_${product}

    ${dic}=     API payment create trust order       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API payment cancel artifact order
    [Arguments]
    ...     ${access_token}
    ...     ${ref_order_id}
    ...     ${ext_transaction_id}
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}      API payment cancel artifact order
    ...     ${access_token}
    ...     ${ref_order_id}
    ...     ${ext_transaction_id}
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}     get from dictionary     ${dic}  response
    should be equal as integers     ${response.status_code}    200
    [Return]    ${dic}

[200] API payment cancel normal order
    [Arguments]
    ...     ${access_token}
    ...     ${ref_order_id}
    ...     ${ext_transaction_id}
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}      API payment cancel normal order
    ...     ${access_token}
    ...     ${ref_order_id}
    ...     ${ext_transaction_id}
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}     get from dictionary     ${dic}  response
    should be equal as integers     ${response.status_code}    200
    [Return]    ${dic}

[200] API payment get order
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${dic}=     API payment get order      ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API user can link bank by bank
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${dic}=     API user can link bank by bank      ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    sof_id   ${response.json()['data']['sof_id']}
    set to dictionary    ${dic}    bank_token   ${response.json()['data']['bank_token']}
    [Return]    ${dic}

[200] API user can unlink bank by bank
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     access_token       ${suite_admin_access_token}
    ${dic}=     API user can unlink bank by bank       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API user can unlink bank by themselves
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${dic}=     API user can unlink bank by themselves      ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API user get all customer bank source of fund
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${dic}=     API user get all customer bank source of fund       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API user get all his/her source of fund
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${dic}=     API user get all his/her source of fund       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API user get his/her bank source of fund
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${dic}=     API user get his/her bank source of fund       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API verify (pre-link) card information
    [Arguments]
    ...     ${access_token}
    ...     ${card_account_name}
    ...     ${card_account_number}
    ...     ${citizen_id}
    ...     ${cvv_cvc}
    ...     ${pin}
    ...     ${expiry_date}
    ...     ${issue_date}
    ...     ${mobile_number}
    ...     ${billing_address}
    ...     ${card_type}
    ...     ${currency}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API verify (pre-link) card information
    ...     ${access_token}
    ...     ${card_account_name}
    ...     ${card_account_number}
    ...     ${citizen_id}
    ...     ${cvv_cvc}
    ...     ${pin}
    ...     ${expiry_date}
    ...     ${issue_date}
    ...     ${mobile_number}
    ...     ${billing_address}
    ...     ${card_type}
    ...     ${currency}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}     get from dictionary     ${dic}  response
    should be equal as integers     ${response.status_code}    200
    set to dictionary    ${dic}     security_ref     ${response.json()['data']['security_ref']}
    [Return]    ${dic}

[200] API user link card source of fund
    [Arguments]
    ...     ${access_token}
    ...     ${card_account_name}
    ...     ${card_account_number}
    ...     ${citizen_id}
    ...     ${cvv_cvc}
    ...     ${pin}
    ...     ${expiry_date}
    ...     ${issue_date}
    ...     ${mobile_number}
    ...     ${billing_address}
    ...     ${currency}
    ...     ${security_code}
    ...     ${security_ref}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API user link card source of fund
    ...     ${access_token}
    ...     ${card_account_name}
    ...     ${card_account_number}
    ...     ${citizen_id}
    ...     ${cvv_cvc}
    ...     ${pin}
    ...     ${expiry_date}
    ...     ${issue_date}
    ...     ${mobile_number}
    ...     ${billing_address}
    ...     ${currency}
    ...     ${security_code}
    ...     ${security_ref}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}     get from dictionary     ${dic}  response
    should be equal as integers     ${response.status_code}    200
    set to dictionary    ${dic}     token    ${response.json()['data']['token']}
    [Return]    ${dic}

[200] API user get list of his/her linked card source of fund detail
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${dic}=     API user get list of his/her linked card source of fund detail       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API user get his/her linked card source of fund detail by sof ID
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${dic}=     API user get his/her linked card source of fund detail by sof ID       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API user delete (un-link) his/her linked card source of fund
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${dic}=     API user delete (un-link) his/her linked card source of fund       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API system user create user sof cash
    [Arguments]
    ...     ${user_id}
    ...     ${user_type_id}
    ...     ${currency}
    ...     ${access_token}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}      API system user create user sof cash
    ...     ${user_id}
    ...     ${user_type_id}
    ...     ${currency}
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}     get from dictionary     ${dic}  response
    should be equal as integers     ${response.status_code}    200
    set to dictionary    ${dic}     sof_id     ${response.json()['data']['sof_id']}
    [Return]    ${dic}

[200] API user get his/her sof cash by currency
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${dic}=     API user get his/her sof cash by currency       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API create payment pin
    [Arguments]
    ...     ${pin}
    ...     ${access_token}
    ...     ${client_id}=${setup_admin_client_id}
    ...     ${client_secret}=${setup_admin_client_secret}
    ${dic}=     API create payment pin
    ...     ${pin}
    ...     ${access_token}
    ...     ${client_id}
    ...     ${client_secret}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API Get all tier level config
    [Arguments]
    ...     ${access_token}
    ...     ${client_id}=${setup_admin_client_id}
    ...     ${client_secret}=${setup_admin_client_secret}
    ${dic}=     API Get all tier level config
    ...     ${access_token}
    ...     ${client_id}
    ...     ${client_secret}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    A_label   ${response.json()['data'][0]['label']}
    set to dictionary    ${dic}    B_label   ${response.json()['data'][1]['label']}
    set to dictionary    ${dic}    C_label   ${response.json()['data'][2]['label']}
    set to dictionary    ${dic}    D_label   ${response.json()['data'][3]['label']}
    set to dictionary    ${dic}    E_label   ${response.json()['data'][4]['label']}
    set to dictionary    ${dic}    F_label   ${response.json()['data'][5]['label']}
    set to dictionary    ${dic}    G_label   ${response.json()['data'][6]['label']}
    set to dictionary    ${dic}    H_label   ${response.json()['data'][7]['label']}
    set to dictionary    ${dic}    I_label   ${response.json()['data'][8]['label']}
    set to dictionary    ${dic}    J_label   ${response.json()['data'][9]['label']}
    set to dictionary    ${dic}    K_label   ${response.json()['data'][10]['label']}
    set to dictionary    ${dic}    L_label   ${response.json()['data'][11]['label']}
    set to dictionary    ${dic}    M_label   ${response.json()['data'][12]['label']}
    set to dictionary    ${dic}    N_label   ${response.json()['data'][13]['label']}
    set to dictionary    ${dic}    O_label   ${response.json()['data'][14]['label']}
    [Return]    ${dic}

[200] API add company balance
    [Arguments]
    ...     ${currency}
    ...     ${amount}
    ...     ${access_token}
    ...     ${client_id}=${setup_admin_client_id}
    ...     ${client_secret}=${setup_admin_client_secret}
    ${dic}=     API add company balance
    ...     ${currency}
    ...     ${amount}
    ...     ${client_id}
    ...     ${client_secret}
    ...     ${access_token}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200

[200] API delete service
    [Arguments]
    ...     ${access_token}
    ...     ${serviceId}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API delete service
    ...     ${access_token}
    ...     ${serviceId}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200

[200] API user can delete customer
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     access_token       ${suite_admin_access_token}
    ${dic}=     API user can unlink bank by bank       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API apply discount code
   [Arguments]     ${arg_dic}
   [Common] - Set default value for keyword in dictionary     ${arg_dic}    client_id     ${setup_admin_client_id}
   [Common] - Set default value for keyword in dictionary     ${arg_dic}    client_secret     ${setup_admin_client_secret}
   ${dic}   API apply discount code        ${arg_dic}
   ${response}    get from dictionary     ${dic}    response
   should be equal as integers     ${response.status_code}    200
   [Return]   ${dic}

[200] API call execute order with security
   [Arguments]
   ...      ${execute_order_token}
   ...      ${order_id}
   ...      ${code}
   ...      ${type_id}
   ...      ${type_name}
   ${dic}   API call execute order with security
   ...      ${execute_order_token}
   ...      ${order_id}
   ...      ${code}
   ...      ${type_id}
   ...      ${type_name}
   ${response}    get from dictionary     ${dic}    response
   should be equal as integers     ${response.status_code}    200
   [Return]   ${dic}

[Fail] API call execute order with security
   [Arguments]
   ...      ${execute_order_token}
   ...      ${order_id}
   ...      ${code}
   ...      ${type_id}
   ...      ${type_name}
   ${dic}   API call execute order with security
   ...      ${execute_order_token}
   ...      ${order_id}
   ...      ${code}
   ...      ${type_id}
   ...      ${type_name}
   ${response}    get from dictionary     ${dic}    response
   should be equal as integers     ${response.status_code}    400
   [Return]   ${dic}

[200] API create sof service balance limitation
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     access_token       ${suite_admin_access_token}
    ${dic}=     API create sof service balance limitation       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}     balance_limitation_id     ${response.json()['data']['id']}
    [Return]    ${dic}

[200] API get tier config allow zero
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     access_token       ${suite_admin_access_token}
    ${dic}=     API get tier config allow zero       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}     value     ${response.json()['data']['value']}
    [Return]    ${dic}

[200] API delete sof service balance limitation
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     access_token       ${suite_admin_access_token}
    ${dic}=     API delete sof service balance limitation       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200

[200] API create normal order
    [Arguments]  ${arg_dic}
    ${dic}  API create normal order    ${arg_dic}
    ${response}     get from dictionary         ${dic}      response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}     order_id     ${response.json()['data']['order_id']}
    [Return]    ${dic}

[200] API create artifact order
    [Arguments]  ${arg_dic}
    ${dic}  API create artifact order  ${arg_dic}
    ${response}     get from dictionary         ${dic}      response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}     order_id     ${response.json()['data']['order_id']}
    [Return]    ${dic}

[200] API apply tier level mask to an order
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${dic}    API apply tier level mask to an order       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API payment create normal order with phone number is null and valid voucher_id
    [Arguments]
    ...     ${access_token}
    ...     ${ext_transaction_id}
    ...     ${product_service_id}
    ...     ${product_service_name}
    ...     ${product_name}
    ...     ${product_ref_1}
    ...     ${product_ref_2}
    ...     ${product_ref_3}
    ...     ${product_ref_4}
    ...     ${product_ref_5}
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${payer_user_id}
    ...     ${payer_user_type}
    ...     ${payee_user_ref_type}
    ...     ${payee_user_ref_value}
    ...     ${payee_user_id}
    ...     ${payee_user_type}
    ...     ${amount}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API payment create normal order
    ...     ${access_token}
    ...     ${ext_transaction_id}
    ...     ${product_service_id}
    ...     ${product_service_name}
    ...     ${product_name}
    ...     ${product_ref_1}
    ...     ${product_ref_2}
    ...     ${product_ref_3}
    ...     ${product_ref_4}
    ...     ${product_ref_5}
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${payer_user_id}
    ...     ${payer_user_type}
    ...     ${payee_user_ref_type}
    ...     ${payee_user_ref_value}
    ...     ${payee_user_id}
    ...     ${payee_user_type}
    ...     ${amount}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}     get from dictionary         ${dic}      response
    should be equal as integers     ${response.status_code}     200
    log  ${dic}
    log  ${response.text}
    [Return]    ${dic}

[Fail] API payment execute order with phone number is null and valid voucher_id
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${return_dic}=     API payment execute order     ${arg_dic}
    ${response}=    get from dictionary    ${return_dic}   response
    log  ${response}
    should be equal as integers     ${response.status_code}     400
    should be equal as strings      ${response.json()['status']['code']}    invalid_request
    should be equal as strings      ${response.json()['status']['message']}     Voucher not found
    [Return]    ${return_dic}

[Fail] API payment execute order with ticket already created
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${return_dic}=     API payment execute order     ${arg_dic}
    ${response}=    get from dictionary    ${return_dic}   response
    log  ${response}
    should be equal as integers     ${response.status_code}     400
    should be equal as strings      ${response.json()['status']['code']}    invalid_request
    should be equal as strings      ${response.json()['status']['message']}     Your remittance service is blocked please contact customer service for more details, however you can continue to use other services.
    [Return]    ${return_dic}

[Fail] API payment execute order with valid phone number and invalid voucher_id
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${return_dic}=     API payment execute order     ${arg_dic}
    ${response}=    get from dictionary    ${return_dic}   response
    log  ${response}
    should be equal as integers     ${response.status_code}     400
    should be equal as strings      ${response.json()['status']['code']}    invalid_request
    should be equal as strings      ${response.json()['status']['message']}     Voucher not found
    [Return]    ${return_dic}

[Fail] API payment execute order with both invalid phone number and voucher_id
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${return_dic}=     API payment execute order     ${arg_dic}
    ${response}=    get from dictionary    ${return_dic}   response
    log  ${response}
    should be equal as integers     ${response.status_code}     400
    should be equal as strings      ${response.json()['status']['code']}    invalid_request
    should be equal as strings      ${response.json()['status']['message']}     Voucher not found
    [Return]    ${return_dic}

[Fail] API payment execute order with invalid phone number and valid voucher_id
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${return_dic}=     API payment execute order     ${arg_dic}
    ${response}=    get from dictionary    ${return_dic}   response
    log  ${response}
    should be equal as integers     ${response.status_code}     400
    should be equal as strings      ${response.json()['status']['code']}    invalid_request
    should be equal as strings      ${response.json()['status']['message']}     Voucher not found
    [Return]    ${return_dic}

[Fail] API payment execute order with valid phone number and blocked voucher_id
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${return_dic}=     API payment execute order     ${arg_dic}
    ${response}=    get from dictionary    ${return_dic}   response
    log  ${response}
    should be equal as integers     ${response.status_code}     400
    should be equal as strings      ${response.json()['status']['code']}    invalid_request
    should be equal as strings      ${response.json()['status']['message']}     Exceeds maximum retry and voucher code is on hold. Please contact customer service for more details.
    [Return]    ${return_dic}

[200] API create service limitation
    [Arguments]  ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     access_token       ${suite_admin_access_token}
#    [Common] - Set default value for keyword in dictionary      ${arg_dic}     equation_id       ${param_not_used}
    ${dic}      API create service limitation       ${arg_dic}
    ${response}     get from dictionary     ${dic}  response
    should be equal as integers     ${response.status_code}    200
    set to dictionary    ${dic}     service_limitation_id     ${response.json()['data']['service_limitation_id']}
    [Return]    ${dic}

[200] API delete service limitation
    [Arguments]  ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     access_token       ${suite_admin_access_token}
    ${dic}      API delete service limitation       ${arg_dic}
    ${response}     get from dictionary     ${dic}  response
    should be equal as integers     ${response.status_code}    200
    [Return]    ${dic}

[200] API create tier mask for service
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${dic}    API create tier mask for service     ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    service_tier_mask_id   ${response.json()['data']['id']}
    [Return]    ${dic}

[200] API create service fee tier mask
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    ${dic}    API create service fee tier mask     &{arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[200] API create pending settlement
    [Arguments]  &{arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     unsettled_transactions       [${EMPTY}]
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     due_date                     ${param_not_used}
     API create pending settlement     &{arg_dic}
     REST.integer    response status    200
     REST.string     $.status.code     success
     REST.string     $.status.message     Success
     REST.integer    $.data.id
     ${id}  rest extract    $.data.id
     ${data}  rest extract    $.data
     ${dic}     create dictionary   id=${id}    data=${data}
     log dictionary     ${dic}
    [Return]    ${dic}

[200] API update pending settlement with only due date
    [Arguments]  &{arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
     API update pending settlement     &{arg_dic}
     REST.integer    response status    200
     REST.string     $.status.code     success
     REST.string     $.status.message     Success
     REST.NULL       $.data

[200] API delete pending settlement
    [Arguments]  &{arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
     API delete pending settlement     &{arg_dic}
     REST.integer    response status    200
     REST.string     $.status.code     success
     REST.string     $.status.message     Success
     REST.NULL       $.data

[Fail] API create pending settlement
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     unsettled_transactions       ${param_not_used}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     due_date                     ${param_not_used}
     API create pending settlement     &{arg_dic}
     REST.integer    response status      ${http_status_BAD_REQUEST}
     REST.string     $.status.code        ${arg_dic.status_code}
     REST.string     $.status.message     ${arg_dic.status_message}
     REST.NULL       $.data

[Fail] API update pending settlement
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
     API update pending settlement     &{arg_dic}
     REST.integer    response status    ${http_status_BAD_REQUEST}
     REST.string     $.status.code      ${arg_dic.status_code}
     REST.string     $.status.message   ${arg_dic.status_message}
     REST.NULL       $.data

[Fail] API delete pending settlement
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
     API delete pending settlement     &{arg_dic}
     REST.integer    response status    ${http_status_BAD_REQUEST}
     REST.string     $.status.code      ${arg_dic.status_code}
     REST.string     $.status.message   ${arg_dic.status_message}
     REST.NULL       $.data

[200] API system user create settlement configuration
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
     API system user create settlement configuration     &{arg_dic}
     REST.integer    response status    200
     REST.string   $.status.code     success
     REST.string   $.status.message     Success
     REST.integer   $.data.id
     ${id}  rest extract    $.data.id
     ${data}  rest extract    $.data
     ${dic}     create dictionary   id=${id}    data=${data}
     log dictionary     ${dic}
    [Return]    ${dic}

[Fail] API user create settlement configuration and get error
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    API system user create settlement configuration     &{arg_dic}
     REST.integer    response status    ${arg_dic.response_code}
     REST.string   $.status.code     ${arg_dic.status_code}
     REST.string   $.status.message     ${arg_dic.status_message}
     ${data}  rest extract    $.data

[Fail] API get all service groups
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}      API get all service groups      &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}
    [Return]   ${dic}

[200] API system user resolve pending settlement
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
     API system user resolve pending settlement     &{arg_dic}
     REST.integer    response status    200
     REST.string   $.status.code     success
     REST.string   $.status.message     Success
     REST.integer   $.data.id
     ${id}  rest extract    $.data.id
     ${data}  rest extract    $.data
     ${dic}     create dictionary   id=${id}
     [Common] - Set default value for keyword in dictionary      ${arg_dic}         output_resolving_history_id       ${id}
     [Common] - Set variable        name=${arg_dic.output_resolving_history_id}     value=${id}
     log dictionary     ${dic}
     [Return]   ${dic}

[Fail] API user resolve pending settlement and get error
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    API system user resolve pending settlement   &{arg_dic}
     REST.integer    response status    ${arg_dic.response_code}
     REST.string   $.status.code     ${arg_dic.status_code}
     REST.string   $.status.message     ${arg_dic.status_message}
     ${data}  rest extract    $.data
     ${dic}     create dictionary   data=${data}
    [Return]    ${dic}

[Fail] User create sof service balance limitation and get error
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     access_token       ${suite_admin_access_token}
    ${dic}=     API create sof service balance limitation       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.http_code}
    should be equal as strings     ${response.json()['status']['code']}     ${arg_dic.status_code}
    should be equal as strings     ${response.json()['status']['message']}     ${arg_dic.status_message}