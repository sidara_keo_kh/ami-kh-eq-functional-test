*** Settings ***
Resource         ../1_raw-deprecated/imports.robot

*** Keywords ***
[200] API add service
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_secret       ${setup_admin_client_secret}
    ${dic}  API add service     ${arg_dic}
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary   ${dic}  id      ${dic.response.json()['data']['id']}
    [Return]    ${dic}

[200] API channel gateway - delete service
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_secret       ${setup_admin_client_secret}
    ${dic}      API channel gateway - delete service    ${arg_dic}
    should be equal as integers     ${dic.response.status_code}     200
    [Return]    ${dic}

[200] API channel gateway - add api
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_secret       ${setup_admin_client_secret}
    ${dic}      API channel gateway - add api    ${arg_dic}
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary   ${dic}  id     ${dic.response.json()['data']['id']}
    [Return]    ${dic}

[200] API channel gateway - agent authentication
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   param_client_id       ${setup_admin_client_id}
    ${dic}      API channel gateway - agent authentication    ${arg_dic}
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary   ${dic}  id     ${dic.response.json()['data']['id']}
    set to dictionary  ${dic}   access_token    ${dic.response.json()['data']['access_token']}
    [Return]    ${dic}

[200] API channel gateway - revoke auth token
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   access_token       ${suite_admin_access_token}
    ${dic}      API channel gateway - revoke auth token    ${arg_dic}
    should be equal as integers     ${dic.response.status_code}     200
    [Return]    ${dic}

[200] API channel gateway - get all apis
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}      API channel gateway - get all apis     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API channel gateway - update api
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    ${dic}   API channel gateway - update api         ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API channel gateway - delete api
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    ${dic}   API channel gateway - delete api       ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API channel gateway - search api
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    ${dic}   API channel gateway - search api       ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API channel gateway - get all services
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}      API channel gateway - get all services     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API channel gateway - add service
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API channel gateway - add service         ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API channel gateway - update service
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    ${dic}   API channel gateway - update service         ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API channel gateway - search service
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    ${dic}   API channel gateway - search service     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API channel gateway - add scopes to client
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       request_client_id       ${arg_dic.client_id}
    ${dic}   API channel gateway - add scopes to client     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API channel gateway - get client scopes
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    ${dic}   API channel gateway - get client scopes       ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API channel gateway - delete scope from client
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    ${dic}   API channel gateway - delete scope from client       ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API channel gateway - get all clients
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    ${dic}   API channel gateway - get all clients       ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}
