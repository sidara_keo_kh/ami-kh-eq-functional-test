*** Settings ***
Resource         ../1_raw-deprecated/imports.robot

*** Keywords ***
[200] API search card history on report
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      paging   false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      page_index   0

    ${dic}  API search card history on report        ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary       ${dic}      trans_id    ${response.json()['data']['card_histories'][0]['trans_id']}
    [Return]    ${dic}

[200] API search company representatives profile on report
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      paging   false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      page_index   0

     API search company representatives profile on report        ${arg_dic}
     REST.integer    response status    200
     REST.string   $.status.code     success
     REST.string   $.status.message     Success
     REST.Object   $.data
     REST.Array    $.data.representatives
     REST.Object   $.data.page
     rest extract   $..id
     rest extract   $..title
     rest extract   $..type
     rest extract   $..suffix
     rest extract   $..first_name
     rest extract   $..middle_name
     rest extract   $..last_name
     rest extract   $..date_of_birth
     rest extract   $..place_of_birth
     rest extract   $..occupation
     rest extract   $..occupation_title
     rest extract   $..nationality
     rest extract   $..national_id_number
     rest extract   $..email
     rest extract   $..mobile_number
     rest extract   $..business_phone_number
     rest extract   $..source_of_funds
     rest extract   $..current_address
     rest extract   $..permanent_address
     rest extract   $..created_by_user_id
     rest extract   $..created_by_user_type
     rest extract   $..updated_by_user_id
     rest extract   $..updated_by_user_type
     rest extract   $..is_deleted

     ${data}  rest extract    $.data
     log dictionary     ${data}
    [Return]    ${data}


[200] API search customer
    [Arguments]   &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id        ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret    ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      paing         true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      page_index    1
    ${dic}   API search customer    &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API search customer
    [Arguments]   &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      paing         true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      page_index    1
    ${dic}   API search customer    &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}
    [Return]   ${dic}

[200] API search system-user
    [Arguments]
    ...     ${client_id}
    ...     ${client_secret}
    ...     ${access_token}
    ...     ${username}
    ...     ${user_id}
    ...     ${is_suspended}
    ...     ${suspend_reason}
    ...     ${email}
    ...     ${role_id}
    ...     ${paging}=true
    ...     ${page_index}=1
    ${dic}   API search system-user
    ...     ${client_id}
    ...     ${client_secret}
    ...     ${access_token}
    ...     ${username}
    ...     ${user_id}
    ...     ${is_suspended}
    ...     ${suspend_reason}
    ...     ${email}
    ...     ${role_id}
    ...     ${paging}
    ...     ${page_index}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API search cash source of fund
    [Arguments]
    ...     ${client_id}
    ...     ${client_secret}
    ...     ${access_token}
    ...     ${user_id}
    ...     ${user_type}
    ...     ${currency}
    ...     ${paging}=true
    ...     ${page_index}=1
    ${dic}   API search cash source of fund
    ...     ${client_id}
    ...     ${client_secret}
    ...     ${access_token}
    ...     ${user_id}
    ...     ${user_type}
    ...     ${currency}
    ...     ${paging}
    ...     ${page_index}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API search cash transaction
    [Arguments]
    ...     ${client_id}
    ...     ${client_secret}
    ...     ${access_token}
    ...     ${order_id}
    ...     ${sof_id}
    ...     ${action_id}
    ...     ${status_id}
    ...     ${order_detail_id}
    ...     ${paging}=true
    ...     ${page_index}=1
    ${dic}   API search cash transaction
    ...     ${client_id}
    ...     ${client_secret}
    ...     ${access_token}
    ...     ${order_id}
    ...     ${sof_id}
    ...     ${action_id}
    ...     ${status_id}
    ...     ${order_detail_id}
    ...     ${paging}
    ...     ${page_index}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API search payment orders
    [Arguments]  ${client_id}   ${client_secret}    ${access_token}
        ...     ${order_id}
        ...     ${ext_transaction_id}
        ...     ${status_id_list}
        ...     ${state}
        ...     ${to_last_updated_timestamp}
        ...     ${from_last_updated_timestamp}
        ...     ${created_client_id}
        ...     ${executed_client_id}
        ...     ${created_channel_type}
        ...     ${created_device_unique_reference}
        ...     ${error_codes}
        ...     ${service_name}
        ...     ${created_channel_type}
        ...     ${service_id_list}
        ...     ${user_type_id}
        ...     ${file_type}
        ...     ${row_number}
        ...     ${from}
        ...     ${to}
        ...     ${product_ref}
        ...     ${product_name}
        ...     ${ref_order_id}
        ...     ${paging}=true
        ...     ${page_index}=1
    ${dic}   API search payment orders      ${client_id}   ${client_secret}    ${access_token}
        ...     ${order_id}
        ...     ${ext_transaction_id}
        ...     ${status_id_list}
        ...     ${state}
        ...     ${to_last_updated_timestamp}
        ...     ${from_last_updated_timestamp}
        ...     ${created_client_id}
        ...     ${executed_client_id}
        ...     ${created_channel_type}
        ...     ${created_device_unique_reference}
        ...     ${error_codes}
        ...     ${service_name}
        ...     ${created_channel_type}
        ...     ${service_id_list}
        ...     ${user_type_id}
        ...     ${file_type}
        ...     ${row_number}
        ...     ${from}
        ...     ${to}
        ...     ${product_ref}
        ...     ${product_name}
        ...     ${ref_order_id}
        ...     ${paging}
        ...     ${page_index}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API search voucher
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${return_dic}     API search voucher   ${arg_dic}
    ${response}=    get from dictionary    ${return_dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary   ${return_dic}   id    ${response.json()['data']['vouchers'][0]['id']}
    set to dictionary   ${return_dic}   voucher_id    ${response.json()['data']['vouchers'][0]['voucher_id']}
    set to dictionary   ${return_dic}   product_ref3    ${response.json()['data']['vouchers'][0]['product']['product_ref3']}
    set to dictionary   ${return_dic}   product_ref4    ${response.json()['data']['vouchers'][0]['product']['product_ref4']}
    set to dictionary   ${return_dic}   product_ref5    ${response.json()['data']['vouchers'][0]['product']['product_ref5']}
    set to dictionary   ${return_dic}   amount    ${response.json()['data']['vouchers'][0]['amount']}
    set to dictionary   ${return_dic}   fee    ${response.json()['data']['vouchers'][0]['fee']}
    set to dictionary   ${return_dic}   created_timestamp    ${response.json()['data']['vouchers'][0]['created_timestamp']}
    set to dictionary   ${return_dic}   expire_date_timestamp    ${response.json()['data']['vouchers'][0]['expire_date_timestamp']}
    set to dictionary   ${return_dic}   issuer_user_id    ${response.json()['data']['vouchers'][0]['issuer_user_id']}
    set to dictionary   ${return_dic}   is_on_hold    ${response.json()['data']['vouchers'][0]['is_on_hold']}
    [Return]    ${return_dic}

[200] API search bank source of fund
    [Arguments]  ${client_id}   ${client_secret}    ${access_token}
        ...     ${user_id}
        ...     ${user_type_id}
        ...     ${currency}
        ...     ${bank_account_number}
        ...     ${ext_bank_reference}
        ...     ${bank_token}
        ...     ${from_created_timestamp}
        ...     ${to_created_timestamp}
        ...     ${is_deleted}=false
        ...     ${paging}=true
        ...     ${page_index}=1
    ${dic}   API search bank source of fund     ${client_id}   ${client_secret}    ${access_token}
        ...     ${user_id}
        ...     ${user_type_id}
        ...     ${currency}
        ...     ${bank_account_number}
        ...     ${ext_bank_reference}
        ...     ${bank_token}
        ...     ${from_created_timestamp}
        ...     ${to_created_timestamp}
        ...     ${is_deleted}
        ...     ${paging}
        ...     ${page_index}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API search card source of fund
    [Arguments]  ${client_id}   ${client_secret}    ${access_token}
        ...     ${user_id}
        ...     ${user_type_id}
        ...     ${currency}
        ...     ${from_created_timestamp}
        ...     ${to_created_timestamp}
        ...     ${paging}=true
        ...     ${page_index}=1
    ${dic}   API search card source of fund     ${client_id}   ${client_secret}    ${access_token}
        ...     ${user_id}
        ...     ${user_type_id}
        ...     ${currency}
        ...     ${from_created_timestamp}
        ...     ${to_created_timestamp}
        ...     ${paging}
        ...     ${page_index}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API search bank
    [Arguments]  ${client_id}   ${client_secret}    ${access_token}
        ...     ${id}
        ...     ${name}
        ...     ${currency}
        ...     ${status}
    ${dic}   API search bank     ${client_id}   ${client_secret}    ${access_token}
        ...     ${id}
        ...     ${name}
        ...     ${currency}
        ...     ${status}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API report - search bank transactions
    [Arguments]  ${client_id}   ${client_secret}    ${access_token}
        ...     ${order_id}
        ...     ${bank_name}
        ...     ${bank_id}
        ...     ${currency}
        ...     ${status}
        ...     ${action_id}
        ...     ${page_index}=1
        ...     ${paging}=true
    ${dic}   API report - search bank transactions     ${client_id}   ${client_secret}    ${access_token}
        ...     ${order_id}
        ...     ${bank_name}
        ...     ${bank_id}
        ...     ${currency}
        ...     ${status}
        ...     ${action_id}
        ...     ${page_index}
        ...     ${paging}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API search voucher refunds
    [Arguments]
    ...     ${access_token}
    ...     ${id}
    ...     ${original_voucher_id}
    ...     ${requested_username}
    ...     ${status_id}
    ...     ${from_created_timestamp}
    ...     ${to_created_timestamp}
    ...     ${paging}=true
    ...     ${page_index}=1
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API search voucher refunds
    ...     ${access_token}
    ...     ${id}
    ...     ${original_voucher_id}
    ...     ${requested_username}
    ...     ${status_id}
    ...     ${from_created_timestamp}
    ...     ${to_created_timestamp}
    ...     ${paging}
    ...     ${page_index}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary   ${dic}   id    ${response.json()['data']['refund_vouchers'][0]['id']}
    set to dictionary   ${dic}   status_id    ${response.json()['data']['refund_vouchers'][0]['status_id']}
    set to dictionary   ${dic}   requested_username    ${response.json()['data']['refund_vouchers'][0]['requested_username']}
    set to dictionary   ${dic}   original_voucher_id    ${response.json()['data']['refund_vouchers'][0]['original_voucher_id']}
    set to dictionary   ${dic}   amount    ${response.json()['data']['refund_vouchers'][0]['amount']}
    set to dictionary   ${dic}   created_timestamp    ${response.json()['data']['refund_vouchers'][0]['created_timestamp']}
    set to dictionary   ${dic}   last_updated_timestamp    ${response.json()['data']['refund_vouchers'][0]['last_updated_timestamp']}
    set to dictionary   ${dic}   reason_for_refund    ${response.json()['data']['refund_vouchers'][0]['reason_for_refund']}
    set to dictionary   ${dic}   new_voucher_id    ${response.json()['data']['refund_vouchers'][0]['new_voucher_id']}
    set to dictionary   ${dic}   product_ref2    ${response.json()['data']['refund_vouchers'][0]['product_ref2']}
    set to dictionary   ${dic}   product_ref3    ${response.json()['data']['refund_vouchers'][0]['product_ref3']}
    set to dictionary   ${dic}   product_ref4    ${response.json()['data']['refund_vouchers'][0]['product_ref4']}
    set to dictionary   ${dic}   product_ref5    ${response.json()['data']['refund_vouchers'][0]['product_ref5']}
    set to dictionary   ${dic}   reason_for_approve_or_reject    ${response.json()['data']['refund_vouchers'][0]['reason_for_approve_or_reject']}
    set to dictionary   ${dic}   fail_reason    ${response.json()['data']['refund_vouchers'][0]['fail_reason']}
    [Return]   ${dic}

[200] API report - search card transactions
    [Arguments]
        ...     ${access_token}
        ...     ${sof_id}
        ...     ${user_id}
        ...     ${user_type}
        ...     ${order_id}
        ...     ${order_detail_id}
        ...     ${provider_name}
        ...     ${currency}
        ...     ${status}
        ...     ${action_id}
        ...     ${card_design_name}
        ...     ${card_design_number}
        ...     ${card_account_name}
        ...     ${card_account_number}
        ...     ${paging}=true
        ...     ${page_index}=1
        ...     ${client_id}=${setup_admin_client_id}
        ...     ${client_secret}=${setup_admin_client_secret}
    ${dic}   API report - search card transactions
        ...     ${access_token}
        ...     ${sof_id}
        ...     ${user_id}
        ...     ${user_type}
        ...     ${order_id}
        ...     ${order_detail_id}
        ...     ${provider_name}
        ...     ${currency}
        ...     ${status}
        ...     ${action_id}
        ...     ${card_design_name}
        ...     ${card_design_number}
        ...     ${card_account_name}
        ...     ${card_account_number}
        ...     ${paging}
        ...     ${page_index}
        ...     ${client_id}
        ...     ${client_secret}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary   ${dic}   first_order_detail_id    ${response.json()['data']['card_sof_transactions'][0]['order_detail_id']}
    [Return]   ${dic}

[200] API search agent device
    [Arguments]
    ...     ${agent_id}
    ...     ${shop_id}
    ...     ${action_id}
    ...     ${access_token}
    ...     ${client_id}
    ...     ${client_secret}
    ${dic}   [200] API search agent devices
    ...     ${agent_id}
    ...     ${shop_id}
    ...     ${action_id}
    ...     ${access_token}
    ...     ${client_id}
    ...     ${client_secret}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API search customer classifications
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API search customer classifications         ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API search service group
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       service_group_id       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       service_group_name       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       to_created_timestamp       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       from_created_timestamp       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       from_last_updated_timestamp       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       to_last_updated_timestamp       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       is_deleted       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       file_type       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       row_number       ${param_not_used}

    ${dic}   API search service group         ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}


[200] API search services
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       service_group_id       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       id       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       status       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       currency       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       service_name       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       is_deleted       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       file_type       ${param_not_used}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       row_number       ${param_not_used}

    ${dic}   API search services         ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get report order detail
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}      API get report order detail     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary       ${dic}      wait_delay_timestamp         ${response.json()['data']['wait_delay_timestamp']}
    [Return]   ${dic}

[200] API search vouchers distributed to user
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      paging   false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      page_index   0
    ${dic}      API search vouchers distributed to user     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}


[200] API get list of cancel voucher in formation by cancel id
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}      API get list of cancel voucher in formation by cancel id     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API search voucher adjustments
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      paging      true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      page_index      1
    ${dic}      API search voucher adjustments     ${arg_dic}
    should be equal as integers     ${dic.response.status_code}    200
    [Return]   ${dic}

[200] API search password rule configuration
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search password rule configuration        ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search identity types
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search identity types       ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search customer devices
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search customer devices       ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search customer identity
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search customer identity      ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search agent
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      paging   true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      page_index   1
    ${dic}  API search agent        &{arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    [Return]    ${dic}

[Fail] API search agent
    [Arguments]   &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      paging   true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      page_index   1
    ${dic}   API search agent    &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}
    [Return]   ${dic}

[200] API search agent company profile
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search agent company profile       ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search agent accreditation status
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search agent accreditation status        ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    set to dictionary   ${dic}     first_creditation_status_id    ${response.json()['data'][0]['id']}
    set to dictionary   ${dic}     first_creditation_status_status    ${response.json()['data'][0]['status']}
    [Return]    ${dic}

[200] API search agent mm card type
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search agent mm card type       ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API get all agent identities
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API get all agent identities       ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    set to dictionary       ${dic}      data         ${response.json()['data']}
    [Return]    ${dic}

[200] API search agent classifications
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search agent classifications      ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search agent smart card
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search agent smart card        ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search shop type
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search shop type    ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search agent shop
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search agent shop       ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search channels permission
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search channels permission        ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search edc
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search edc        ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search agent relationship
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search agent relationship        ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search categories
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search categories       ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search products
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search products        ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search channels
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search channels        ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search products agent type relation
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search products agent type relation        ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search card on report
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search card on report      ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search shop categories
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}    API search shop categories      ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API report search SOF bank list
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API report search SOF bank list       ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search reconciled partner file
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}   API search reconciled partner file       ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}
[200] API search reconciled partner reconcile result
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search reconciled partner reconcile result        ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search reconciled sof file
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search reconciled sof file       ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search reconciled sof reconcile result
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search reconciled sof reconcile result        ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}
[200] API get all roles
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API get all roles        ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API get all permissions
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API get all permissions        &{arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search card type
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search card type        ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search card sof provider
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search card sof provider       ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}
[200] API search card design
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search card design        ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search card transactions
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search card transactions           ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    set to dictionary       ${dic}      first_order_detail_id     ${response.json()['data']['card_sof_transactions'][0]['order_detail_id']}
    [Return]    ${dic}

[200] API search card sof action history
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search card sof action history        ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API get all balance adjustments
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API get all balance adjustments        ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    set to dictionary       ${dic}      code                        ${response.json()['status']['code']}
    should be equal as integers     ${dic.response.status_code}     200
    [Return]    ${dic}

[200] API search list of fraud tickets
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search list of fraud tickets        ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API add service ids to report service whitelist
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API add service ids to report service whitelist       &{arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API get all service ids from report service whitelist
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API get all service ids from report service whitelist         ${arg_dic}
    ${response}     get from dictionary        ${dic}         response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API remove service ids to report service whitelist
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API remove service ids to report service whitelist        &{arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search agent commission
    [Documentation]    example arg_dic:  create dictionary        access_token=123       mobile_numbers="12","2233"
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API search agent commission         &{arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API summary agents commission
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API summary agents commission         &{arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[Fail] API search agent commission
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}      API search agent commission      &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}
    [Return]   ${dic}

[Fail] API summary agents commission
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}      API summary agents commission      &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}
    [Return]   ${dic}

[200] API summary payment transaction agent
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API summary payment transaction agent        ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API summary transaction own agent
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API summary transaction own agent        &{arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API summary customer wallet info
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}   API summary customer wallet info         &{arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API summary agent wallet info
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API summary agent wallet info        &{arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API summary order balance movements
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API summary order balance movements        ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API get all report type
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API get all report type                 ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API get report formula
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API get report formula                  ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API update report formula
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    ${dic}  API update report formula               ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
   set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[200] API search agent belong to company
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API search agent belong to company   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    set to dictionary       ${dic}      data         ${response.json()['data']}
    [Return]    ${dic}

[200] API search company types
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API get all company types   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    set to dictionary       ${dic}      data         ${response.json()['data']}
    [Return]    ${dic}

[200] API search payment orders with Arguments = dic
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id           ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      paging              true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      page_index          1
    ${dic}   API search payment orders with arguments = dic     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    set to dictionary       ${dic}      data         ${response.json()['data']}
    [Return]    ${dic}

[200] API search user profile by mobile number
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id           ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      paging              true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      page_index          1
    ${dic}   API search user profile     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    set to dictionary       ${dic}      data         ${response.json()['data']}
    [Return]    ${dic}

[200] API search agent type
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id           ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret       ${setup_admin_client_secret}
    ${dic}      API search agent type     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    set to dictionary       ${dic}      data         ${response.json()['data']}
    [Return]    ${dic}

[200] API search sales permissions
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id           ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret       ${setup_admin_client_secret}
    ${dic}      API search sales permissions     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    set to dictionary       ${dic}      permissions         ${response.json()['data']['permissions']}
    [Return]    ${dic}

[200] Get Company Transaction History By Connected Agent
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id           ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      paging              true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      page_index          1

    ${dic}      API Search Company Transaction History By Connected Agent     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    set to dictionary       ${dic}      data         ${response.json()['data']}
    [Return]    ${dic}

[Failed] Get Company Transaction History By Connected Agent
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id           ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      paging              true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      page_index          1

    ${dic}      API Search Company Transaction History By Connected Agent     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    set to dictionary       ${dic}      data         ${response.json()['data']}
    [Return]    ${dic}

[200] Get Company Transaction History By Specific Company
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id           ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      paging              true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      page_index          1

    ${dic}      API Search Company Transaction History By Specific Company     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    set to dictionary       ${dic}      data         ${response.json()['data']}
    [Return]    ${dic}

[200] API Get transaction history of company
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id           ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret       ${setup_admin_client_secret}
    ${dic}      API Get transaction history of company     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    set to dictionary       ${dic}      data         ${response.json()['data']}
    [Return]    ${dic}

[200] API search trust token
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id           ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret       ${setup_admin_client_secret}
    ${dic}      API search trust token     &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200

[Fail] API search trust token
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id           ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret       ${setup_admin_client_secret}
    ${dic}      API search trust token     &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}

[200] API search settlement configurations
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id           ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret       ${setup_admin_client_secret}
    ${dic}      API search settlement configurations     &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    set to dictionary       ${dic}      data         ${response.json()['data']}
    [Return]    ${dic}

[Report][200] Get summary transaction own agent
    [Arguments]
        ...     ${client_id}=${setup_admin_client_id}
        ...     ${client_secret}=${setup_admin_client_secret}
        ...     &{arg_dic}
    Set To Dictionary       ${arg_dic}
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
    [Report] Get summary transaction own agent     &{arg_dic}
    REST.integer    response status     200

[Report][200] Search payment orders
    [Arguments]
        ...     ${client_id}=${setup_admin_client_id}
        ...     ${client_secret}=${setup_admin_client_secret}
        ...     &{arg_dic}
    Set To Dictionary       ${arg_dic}
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
    [Report] Search payment orders     &{arg_dic}
    REST.integer    response status     200

[Report][200] - Search settlement resolving histories
    [Arguments]
        ...     ${client_id}=${setup_admin_client_id}
        ...     ${client_secret}=${setup_admin_client_secret}
        ...     &{arg_dic}
    Set To Dictionary       ${arg_dic}
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
    [Report] Search settlement resolving histories     &{arg_dic}
    REST.integer    response status     200
    
[200] API search pending settlement
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id   ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret   ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      paging   true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      page_index   1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      settlement_id       ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      description       ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      created_by       ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      from_due_date       ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      to_due_date       ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      status       ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      payer_id       ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      payer_name       ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      payee_id       ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      payee_name       ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      internal_transaction_id       ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      external_transaction_id       ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      from_created_timestamp       ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      to_created_timestamp       ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_over_due       ${param_not_used}

#    API search pending settlement        &{arg_dic}
#    REST.integer    response status    200
#    REST.string     $.status.code     success
#    REST.string     $.status.message     Success
#
#    ${data_settlements}     rest extract    $.data.settlements
#    ${size_settlements}=    get length      ${data_settlements}
#    ${dic}      create dictionary
#        :FOR     ${index}   IN RANGE    0   ${size_settlements}
#    \   #settlement_data
#    \    ${id}     rest extract    $.data.settlements[${index}].id
#    \    ${description}     rest extract    $.data.settlements[${index}].description
#    \    ${amount}     rest extract    $.data.settlements[${index}].amount
#    \    ${currency}     rest extract    $.data.settlements[${index}].currency
#    \    ${status}     rest extract    $.data.settlements[${index}].status
#    \    ${created_by}     rest extract    $.data.settlements[${index}].created_by
#    \    ${due_date}     rest extract    $.data.settlements[${index}].due_date
#    \    ${created_timestamp}     rest extract    $.data.settlements[${index}].created_timestamp
#    \    ${last_updated_timestamp}     rest extract    $.data.settlements[${index}].last_updated_timestamp
#    \   #_set_variable_settlement_data
#    \    ${id_${index}}     set variable    ${id}
#    \    ${description_${index}}     set variable    ${description}
#    \    ${amount_${index}}     set variable    ${amount}
#    \    ${currency_${index}}     set variable    ${currency}
#    \    ${status_${index}}     set variable    ${status}
#    \    ${created_by_${index}}     set variable    ${created_by}
#    \    ${due_date_${index}}     set variable    ${due_date}
#    \    ${created_timestamp_${index}}     set variable    ${created_timestamp}
#    \    ${last_updated_timestamp_${index}}     set variable    ${last_updated_timestamp}
#    \
#    \   #payer_data
#    \    ${payer_id}     rest extract    $.data.settlements[${index}].payer.id
#    \    ${payer_name}     rest extract    $.data.settlements[${index}].payer.name
#    \    ${payer_user_type_id}     rest extract    $.data.settlements[${index}].payer.user_type.id
#    \    ${payer_user_type_name}     rest extract    $.data.settlements[${index}].payer.user_type.name
#    \    ${payer_sof_id}     rest extract    $.data.settlements[${index}].payer.sof.id
#    \    ${payer_sof_type_id}     rest extract    $.data.settlements[${index}].payer.sof.type_id
#    \   #_set_variable_payer_data
#    \    ${payer_id_${index}}     set variable    ${payer_id}
#    \    ${payer_name_${index}}     set variable    ${payer_name}
#    \    ${payer_user_type_id_${index}}     set variable    ${payer_user_type_id}
#    \    ${payer_user_type_name_${index}}     set variable    ${payer_user_type_name}
#    \    ${payer_sof_id_${index}}     set variable    ${payer_sof_id}
#    \    ${payer_sof_type_id_${index}}     set variable    ${payer_sof_type_id}
#    \
#    \   #payee_data
#    \    ${payee_id}     rest extract    $.data.settlements[${index}].payee.id
#    \    ${payee_name}     rest extract    $.data.settlements[${index}].payee.name
#    \    ${payee_user_type_id}     rest extract    $.data.settlements[${index}].payee.user_type.id
#    \    ${payee_user_type_name}     rest extract    $.data.settlements[${index}].payee.user_type.name
#    \    ${payee_sof_id}     rest extract    $.data.settlements[${index}].payee.sof.id
#    \    ${payee_sof_type_id}     rest extract    $.data.settlements[${index}].payee.sof.type_id
#    \   #_set_variable_payee_data
#    \    ${payee_id_${index}}     set variable    ${payee_id}
#    \    ${payee_name_${index}}     set variable    ${payee_name}
#    \    ${payee_user_type_id_${index}}     set variable    ${payee_user_type_id}
#    \    ${payee_user_type_name_${index}}     set variable    ${payee_user_type_name}
#    \    ${payee_sof_id_${index}}     set variable    ${payee_sof_id}
#    \    ${payee_sof_type_id_${index}}     set variable    ${payee_sof_type_id}
#    \
#    \   #created_user
#    \    ${created_user_id}     rest extract    $.data.settlements[${index}].created_user.id
#    \    ${created_user_name}     rest extract    $.data.settlements[${index}].created_user.name
#    \   #_set_variable_created_user
#    \    ${created_user_id_${index}}     set variable    ${created_user_id}
#    \    ${created_user_name_${index}}     set variable    ${created_user_name}
#    \
#    \   #last_updated_user
#    \    ${last_updated_user_id}     rest extract    $.data.settlements[${index}].last_updated_user.id
#    \    ${last_updated_user_name}     rest extract    $.data.settlements[${index}].last_updated_user.name
#    \   #_set_variable_last_updated_user
#    \    ${last_updated_user_id_${index}}     set variable    ${last_updated_user_id}
#    \    ${last_updated_user_name_${index}}     set variable    ${last_updated_user_name}
#    \
#    \   #list_unsettled_transactions
#    \    ${unsettled_transactions}     rest extract    $.data.settlements[${index}].unsettled_transactions
#    \   #_set_variable_list_unsettled_transactions
#    \    ${unsettled_transactions_${index}}     set variable    ${unsettled_transactions}
#    \
#    \   #set_settlement_data_to_dictionary
#    \   set to dictionary      ${dic}      id=${id_${index}}       description=${description_${index}}       amount=${amount_${index}}       currency=${currency_${index}}       status=${status_${index}}       created_by=${created_by_${index}}       due_date=${due_date_${index}}       created_timestamp=${created_timestamp_${index}}       last_updated_timestamp=${last_updated_timestamp_${index}}
#    \   #set_payer_data_to_dictionary
#    \   set to dictionary      ${dic}       payer_id=${payer.id_${index}}       payer_name=${payer.name_${index}}       payer_user_type_id=${payer.user_type.id_${index}}       payer_user_type_name=${payer.user_type.name_${index}}       payer_sof_id=${payer.sof.id_${index}}       payer_sof_type_id=${payer.sof.type_id_${index}}
#    \   #set_payee_data_to_dictionary
#    \   set to dictionary      ${dic}       payee_id=${payee.id_${index}}       payee_name=${payee.name_${index}}       payee_user_type_id=${payee.user_type.id_${index}}       payee_user_type_name=${payee.user_type.name_${index}}       payee_sof_id=${payee.sof.id_${index}}       payee_sof_type_id=${payee.sof.type_id_${index}}
#    \   #set_created_user_to_dictionary
#    \   set to dictionary      ${dic}       created_user_id=${created_user.id_${index}}       created_user_name=${created_user.name_${index}}
#    \   #set_last_updated_user_to_dictionary
#    \   set to dictionary      ${dic}       last_updated_user_id=${last_updated_user.id_${index}}       last_updated_user_name=${last_updated_user.name_${index}}
#    \   #set_unsettled_transactions_to_dictionary
#    \   set to dictionary      ${dic}       unsettled_transactions=${unsettled_transactions_${index}}
#    \   log dictionary         ${dic}

    ${dic}      API search pending settlement     &{arg_dic}
    ${response}    get from dictionary            ${dic}           response
    should be equal as integers     ${response.status_code}    200
    ${size_settlements}=    get length      ${response.json()['data']['settlements']}

        :FOR     ${index}   IN RANGE    0   ${size_settlements}
    \   #settlement_data
    \   set to dictionary    ${dic}     id_${index}     ${response.json()['data']['settlements'][${index}]['id']}
    \   set to dictionary    ${dic}     description_${index}     ${response.json()['data']['settlements'][${index}]['description']}
    \   set to dictionary    ${dic}     amount_${index}     ${response.json()['data']['settlements'][${index}]['amount']}
    \   set to dictionary    ${dic}     currency_${index}     ${response.json()['data']['settlements'][${index}]['currency']}
    \   set to dictionary    ${dic}     status_${index}     ${response.json()['data']['settlements'][${index}]['status']}
    \   set to dictionary    ${dic}     created_by_${index}     ${response.json()['data']['settlements'][${index}]['created_by']}
    \   set to dictionary    ${dic}     due_date_${index}     ${response.json()['data']['settlements'][${index}]['due_date']}
    \   set to dictionary    ${dic}     created_timestamp_${index}     ${response.json()['data']['settlements'][${index}]['created_timestamp']}
    \   set to dictionary    ${dic}     last_updated_timestamp_${index}     ${response.json()['data']['settlements'][${index}]['last_updated_timestamp']}
    \   #payer_data
    \   set to dictionary    ${dic}     payer_id_${index}     ${response.json()['data']['settlements'][${index}]['payer']['id']}
    \   set to dictionary    ${dic}     payer_name_${index}     ${response.json()['data']['settlements'][${index}]['payer']['name']}
    \   set to dictionary    ${dic}     payer_user_type_id_${index}     ${response.json()['data']['settlements'][${index}]['payer']['user_type']['id']}
    \   set to dictionary    ${dic}     payer_user_type_name_${index}     ${response.json()['data']['settlements'][${index}]['payer']['user_type']['name']}
    \   set to dictionary    ${dic}     payer_sof_id_${index}     ${response.json()['data']['settlements'][${index}]['payer']['sof']['id']}
    \   set to dictionary    ${dic}     payer_sof_type_id_${index}     ${response.json()['data']['settlements'][${index}]['payer']['sof']['type_id']}
    \   #payee_data
    \   set to dictionary    ${dic}     payee_id_${index}     ${response.json()['data']['settlements'][${index}]['payee']['id']}
    \   set to dictionary    ${dic}     payee_name_${index}     ${response.json()['data']['settlements'][${index}]['payee']['name']}
    \   set to dictionary    ${dic}     payee_user_type_id_${index}     ${response.json()['data']['settlements'][${index}]['payee']['user_type']['id']}
    \   set to dictionary    ${dic}     payee_user_type_name_${index}     ${response.json()['data']['settlements'][${index}]['payee']['user_type']['name']}
    \   set to dictionary    ${dic}     payee_sof_id_${index}     ${response.json()['data']['settlements'][${index}]['payee']['sof']['id']}
    \   set to dictionary    ${dic}     payee_sof_type_id_${index}     ${response.json()['data']['settlements'][${index}]['payee']['sof']['type_id']}
    \   #created_user
    \   set to dictionary    ${dic}     created_user_id_${index}     ${response.json()['data']['settlements'][${index}]['created_user']['id']}
    \   set to dictionary    ${dic}     created_user_name_${index}     ${response.json()['data']['settlements'][${index}]['created_user']['name']}
    \   set to dictionary    ${dic}     created_user_type_id_${index}     ${response.json()['data']['settlements'][${index}]['created_user']['user_type']['id']}
    \   set to dictionary    ${dic}     created_user_type_name_${index}     ${response.json()['data']['settlements'][${index}]['created_user']['user_type']['name']}
    \   #last_updated_user
    \   set to dictionary    ${dic}     last_updated_user_id_${index}     ${response.json()['data']['settlements'][${index}]['last_updated_user']['id']}
    \   set to dictionary    ${dic}     last_updated_user_name_${index}     ${response.json()['data']['settlements'][${index}]['last_updated_user']['name']}
    \   set to dictionary    ${dic}     last_updated_user_type_id_${index}     ${response.json()['data']['settlements'][${index}]['last_updated_user']['user_type']['id']}
    \   set to dictionary    ${dic}     last_updated_user_type_name_${index}     ${response.json()['data']['settlements'][${index}]['last_updated_user']['user_type']['name']}
    \   #reconciliation_data
    \   set to dictionary    ${dic}     reconciliation_id_${index}     ${response.json()['data']['settlements'][${index}]['reconciliation_id']}
    \   set to dictionary    ${dic}     reconciliation_from_date_${index}     ${response.json()['data']['settlements'][${index}]['reconciliation_from_date']}
    \   set to dictionary    ${dic}     reconciliation_to_date_${index}     ${response.json()['data']['settlements'][${index}]['reconciliation_to_date']}
    \
    \   #list_unsettled_transactions
    \   set to dictionary    ${dic}     unsettled_transactions_${index}     ${response.json()['data']['settlements'][${index}]['unsettled_transactions']}

    #pagination _object
    set to dictionary    ${dic}     total_pages        ${response.json()['data']['page']['total_pages']}
    set to dictionary    ${dic}     has_next           ${response.json()['data']['page']['has_next']}
    set to dictionary    ${dic}     has_previous       ${response.json()['data']['page']['has_previous']}
    set to dictionary    ${dic}     current_page       ${response.json()['data']['page']['current_page']}
    set to dictionary    ${dic}     total_elements     ${response.json()['data']['page']['total_elements']}

    set to dictionary    ${dic}     size_settlements     ${size_settlements}
    [Return]    ${dic}

#[200] Get list of unsettled_transactions
#    [Arguments]     ${response}
#    :FOR     ${item}   in   ${response.json()}
#    \   #unsettled_transactions_data
#    \   set to dictionary    ${dic}     unsettled_transactions_id_${index}     ${item['id']}

[Fail] API search OTP
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id             ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret         ${setup_admin_client_secret}
    ${dic}      API search OTP      &{arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}
    [Return]   ${dic}