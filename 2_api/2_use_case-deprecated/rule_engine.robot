*** Settings ***
Resource         ../1_raw-deprecated/imports.robot

*** Keywords ***

[200] API create rule
    [Arguments]
    ...     ${name}
    ...     ${description}
    ...     ${is_active}
    ...     ${start_active_timestamp}
    ...     ${end_active_timestamp}
    ...     ${access_token}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API create new campaign
    ...     ${name}
    ...     ${description}
    ...     ${is_active}
    ...     ${start_active_timestamp}
    ...     ${end_active_timestamp}
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}     get from dictionary         ${dic}      response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    rule_id   ${response.json()['data']['id']}
    [Return]    ${dic}

[200] API update rule
    [Arguments]
    ...     ${rule_id}
    ...     ${name}
    ...     ${description}
    ...     ${is_active}
    ...     ${access_token}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API update campaign
    ...     ${rule_id}
    ...     ${name}
    ...     ${description}
    ...     ${is_active}
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}     get from dictionary         ${dic}      response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API create mechanic
    [Arguments]
    ...     ${rule_id}
    ...     ${event_name}
    ...     ${start_timestamp}
    ...     ${end_timestamp}
    ...     ${access_token}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API create new mechanic
    ...     ${rule_id}
    ...     ${event_name}
    ...     ${start_timestamp}
    ...     ${end_timestamp}
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}     get from dictionary         ${dic}      response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    mechanic_id   ${response.json()['data']['id']}
    [Return]    ${dic}

[200] API create condition
    [Arguments]
    ...     ${rule_id}
    ...     ${mechanic_id}
    ...     ${filter_type}
    ...     ${sum_key_name}
    ...     ${profile_detail_actor}
    ...     ${access_token}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API create new condition
    ...     ${rule_id}
    ...     ${mechanic_id}
    ...     ${filter_type}
    ...     ${sum_key_name}
    ...     ${profile_detail_actor}
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}     get from dictionary         ${dic}      response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    condition_id   ${response.json()['data']['id']}
    [Return]    ${dic}

[200] API create comparison
    [Arguments]
    ...     ${rule_id}
    ...     ${mechanic_id}
    ...     ${condition_id}
    ...     ${key_name}
    ...     ${key_value_type}
    ...     ${key_value}
    ...     ${operator}
    ...     ${access_token}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API create new comparison
    ...     ${rule_id}
    ...     ${mechanic_id}
    ...     ${condition_id}
    ...     ${key_name}
    ...     ${key_value_type}
    ...     ${key_value}
    ...     ${operator}
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}     get from dictionary         ${dic}      response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    comparison_id   ${response.json()['data']['id']}
    [Return]    ${dic}

[200] API create new campaign
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       name       robot_rule_name
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       description       robot_description
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       start_active_timestamp       2099-08-30T10:35:38Z
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       end_active_timestamp       2099-08-31T10:35:38Z
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       is_active       false

    ${dic}   API create new rule         ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get campaign detail by id
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API get campaign detail by id         ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary    ${dic}    start_active_timestamp   ${response.json()['data']['start_active_timestamp']}
    set to dictionary    ${dic}    end_active_timestamp   ${response.json()['data']['end_active_timestamp']}
    set to dictionary    ${dic}    created_timestamp   ${response.json()['data']['created_timestamp']}
    set to dictionary    ${dic}    last_updated_timestamp   ${response.json()['data']['last_updated_timestamp']}
    [Return]   ${dic}

[200] API get campaign list
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API get campaign list         ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API update campaign
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       name       rule_name2
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       description       rule_descrption2
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       is_active       false

    ${dic}   API update rule         ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API search campaign
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       rule_id       1
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       start_active_timestamp       2018-08-30T10:35:38Z
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       end_active_timestamp       2018-08-31T10:35:38Z
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       is_active       false

    ${dic}   API search campaign         ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}


[200] API create new mechanic
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       event_name       login
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       start_timestamp       2098-08-30T10:35:38Z
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       end_timestamp       2098-08-31T10:35:38Z

    ${dic}   API create mechanic        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API update mechanic
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       rule_id       1
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       mechanic_id       1
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       event_name       login
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       start_timestamp       2098-08-30T10:35:38Z
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       end_timestamp       2098-08-31T10:35:38Z

    ${dic}   API update mechanic        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get mechanic list
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API get mechanic list         ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get mechanic detail by id
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API get mechanic detail by id         ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}


[200] API delete mechanic
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API delete mechanic        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}


[200] API create new condition
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       filter_type       event_detail
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       sum_key_name       amount


    ${dic}   API create condition        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get condition list
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API get condition list        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get condition detail by id
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API get condition detail by id        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API update condition
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       filter_type       event_detail
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       mechanic_id       1
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       sum_key_name       null


    ${dic}   API update condition        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API delete condition
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API delete condition        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get comparison list
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API get comparison list        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}


[200] API create new comparison
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       key_name       key_name
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       key_value_type       key_value_type
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       operator       operator
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       key_value       key_value

    ${dic}   API create comparison        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}


[200] API get all filter types
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API get all filter types        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}


[200] API get all data types
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API get all data types        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get all operators
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API get all operators        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get all action types
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API get all action types        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get all actions
    [Arguments]         ${arg_dic}   ${index}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API get all actions        ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    set to dictionary    ${dic}    id   ${response.json()['data'][${index}]['id']}
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get action limitation list
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API get action limitation list        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API get condition filter list
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API get condition filter list        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}


[200] API create condition filter
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       key_name       key_name
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       key_value_type       key_value_type
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       operator       operator
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       key_value       key_value
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       is_consecutive_key       null


    ${dic}   API create condition filter        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API delete condition filter
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API delete condition filter        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API create condition reset filter
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       key_name       key_name
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       key_value_type       key_value_type
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       operator       operator
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       key_value       key_value


    ${dic}   API create condition reset filter        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}


[200] API get condition reset filter list
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API get condition reset filter list        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}


[200] API delete action
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API delete action        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API delete action limit
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API delete action limit        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}



[200] API search action history
    [Arguments]         ${arg_dic}          ${index}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       action_id       1

    ${dic}   API search action history        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    ${length}=      get length      ${response.json()['data']['action_histories']}
    set test variable   ${length}      ${length}
    run keyword if    '${length}'!='0'     run keywords    set to dictionary    ${dic}    id   ${response.json()['data']['action_histories'][${index}]['id']}
        ...    AND   set to dictionary    ${dic}    campaign_id   ${response.json()['data']['action_histories'][${index}]['rule_id']}
        ...    AND   set to dictionary    ${dic}    action_id   ${response.json()['data']['action_histories'][${index}]['action_id']}
        ...    AND   set to dictionary    ${dic}    notification_id   ${response.json()['data']['action_histories'][${index}]['notification_id']}
        ...    AND   set to dictionary    ${dic}    status   ${response.json()['data']['action_histories'][${index}]['status']}
        ...    AND   set to dictionary    ${dic}    fixed_cashback_order_id   ${response.json()['data']['action_histories'][${index}]['fixed_cashback_order_id']}
        ...    AND   set to dictionary    ${dic}    user_id   ${response.json()['data']['action_histories'][${index}]['user_id']}
        ...    AND   set to dictionary    ${dic}    action_history_id   ${response.json()['data']['action_histories'][${index}]['id']}
        ...    AND   set to dictionary    ${dic}    user_type   ${response.json()['data']['action_histories'][${index}]['user_type']}
        ...    AND   set to dictionary    ${dic}    event_id   ${response.json()['data']['action_histories'][${index}]['event']['id']}
        ...    AND   set to dictionary    ${dic}    order_id   ${response.json()['data']['action_histories'][${index}]['event']['order_id']}
        ...    AND   set to dictionary    ${dic}    event_user_type   ${response.json()['data']['action_histories'][${index}]['event']['user_type']}
        ...    AND   set to dictionary    ${dic}    event_user_id   ${response.json()['data']['action_histories'][${index}]['event']['user_id']}
        ...    AND   set to dictionary    ${dic}    event_user_type   ${response.json()['data']['action_histories'][${index}]['event']['user_type']}
        ...    AND   set to dictionary    ${dic}    event_user_id   ${response.json()['data']['action_histories'][${index}]['event']['user_id']}
        ...    AND   set test variable     ${status}        ${dic.status}
        ...    AND   set test variable     ${action_history_id}        ${dic.action_history_id}
        ...    AND   set test variable     ${fixed_cashback_order_id}        ${dic.fixed_cashback_order_id}
        ...    AND   set test variable     ${user_id}        ${dic.user_id}
        ...    AND   set test variable     ${user_type}        ${dic.user_type}
        ...    AND   set test variable     ${order_id}        ${dic.order_id}
        ...    AND   set test variable     ${campaign_id}        ${dic.campaign_id}
        ...    AND   set test variable     ${action_id}        ${dic.action_id}
        ...    AND   set test variable     ${notification_id}        ${dic.notification_id}

    [Return]   ${dic}

[200] API get rule limit list
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API get rule limit list        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API delete rule limit
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}

    ${dic}   API delete rule limit      ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API update comparison
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       key_name       key_name
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       key_value_type       key_value_type
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       operator       operator
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       key_value       key_value

    ${dic}   API update comparison        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API update condition filter
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       key_name       key_name
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       key_value_type       key_value_type
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       operator       operator
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       key_value       key_value
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       is_consecutive_key       false


    ${dic}   API update condition filter        ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}


[200] API create new action
    [Documentation]       example for @{data}
    ...     ${1}  set variable    {"key_name":"service_name","operator":"=","key_value":"service","key_value_type":"text"}
    ...     ${2}  set variable    {"key_name":"service_name","operator":"=","key_value":"service","key_value_type":"text"}
    ...     @{data}  create list     ${1}    ${2}
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       user_id       ${param_not_used}
#    [Common] - Set default value for keyword in dictionary        ${arg_dic}       user_type     ${param_not_used}

    ${dic}   API create action       ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]   ${dic}

[200] API update action
    [Documentation]       example for @{data}
    ...     ${1}  set variable    {"key_name":"service_name","operator":"=","key_value":"service","key_value_type":"text"}
    ...     ${2}  set variable    {"key_name":"service_name","operator":"=","key_value":"service","key_value_type":"text"}
    ...     @{data}  create list     ${1}    ${2}
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       action_type_id       1
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       user_id       null
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       user_type       null

    ${dic}   API update action       ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}


[200] API create action limitation
    [Documentation]       example for @{data}
    ...     ${1}  set variable    {"key_name":"service_name","operator":"=","key_value":"service","key_value_type":"text"}
    ...     ${2}  set variable    {"key_name":"service_name","operator":"=","key_value":"service","key_value_type":"text"}
    ...     @{data}  create list     ${1}    ${2}
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       limit_type       count
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       value       100

    ${dic}   API create action limitation       ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API update action limitation
    [Documentation]       example for @{data}
    ...     ${1}  set variable    {"key_name":"service_name","operator":"=","key_value":"service","key_value_type":"text"}
    ...     ${2}  set variable    {"key_name":"service_name","operator":"=","key_value":"service","key_value_type":"text"}
    ...     @{data}  create list     ${1}    ${2}
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       limit_type       count
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       value       100

    ${dic}   API update action limitation       ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}


[200] API create rule limitation
    [Documentation]       example for @{data}
    ...     ${1}  set variable    {"key_name":"service_name","operator":"=","key_value":"service","key_value_type":"text"}
    ...     ${2}  set variable    {"key_name":"service_name","operator":"=","key_value":"service","key_value_type":"text"}
    ...     @{data}  create list     ${1}    ${2}
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       limit_type       amount-per-user
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       limit_value       10000000

    ${dic}   API create rule limitation       ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set test variable     ${limit_id}        ${response.json()['data']['id']}
    [Return]   ${dic}

[Fail] API create rule limitation
    [Documentation]       example for @{data}
    ...     ${1}  set variable    {"key_name":"service_name","operator":"=","key_value":"service","key_value_type":"text"}
    ...     ${2}  set variable    {"key_name":"service_name","operator":"=","key_value":"service","key_value_type":"text"}
    ...     @{data}  create list     ${1}    ${2}
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       limit_type       amount-per-user
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       limit_value       10000000

    ${dic}   API create rule limitation       ${arg_dic}

    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    400
    [Return]   ${dic}