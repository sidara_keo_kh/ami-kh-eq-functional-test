*** Settings ***
Resource         ../1_raw-deprecated/imports.robot

*** Keywords ***
[200] API create agent type
    [Arguments]
    ...     ${access_token}
    ...     ${name}
    ...     ${description}=robot_agent_type_desciption
    ...     ${is_sale}=false
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}     API create agent type
    ...     ${access_token}
    ...     ${name}
    ...     ${description}
    ...     ${is_sale}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[200] API update agent type
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      description     robot_agent_type_desciption_updated
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update agent type    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API update agent type
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      description     robot_agent_type_desciption_updated
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update agent type    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API create agent classification
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      description     robot_agent_classification_desciption
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create agent classification    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[Fail] API create agent classification
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      description     robot_agent_classification_desciption
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create agent classification    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API update agent classification
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      description     robot_agent_classification_desciption_updated
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update agent classification    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API update agent classification
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      description     robot_agent_classification_desciption_updated
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update agent classification    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API delete agent classification
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API delete agent classification    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API delete agent classification
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API delete agent classification    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    ${arg_dic.code}
    [Return]   ${dic}

[200] API update agent accreditation
    [Arguments]     ${arg_dic}
    ${gen_verify_date}=     Get Current Date            result_format=%Y-%m-%dT00:00:00Z
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      status_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      remark        robot_agent_accreditation_remark_updated
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      risk_level    High
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      verify_date   ${gen_verify_date}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update agent accreditation    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API update agent accreditation
    [Arguments]     ${arg_dic}
    ${gen_verify_date}=     Get Current Date            result_format=%Y-%m-%dT00:00:00Z
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      status_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      remark        robot_agent_accreditation_remark_updated
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      risk_level    High
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      verify_date   ${gen_verify_date}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update agent accreditation    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API Admin updates agent profiles
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_testing_account     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_system_account     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      acquisition_source     internet_updated
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_id     2
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_name     agent
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mm_card_type_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mm_card_level_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_require_otp     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      agent_classification_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      date_of_birth     1988-01-01T00:00:00Z
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      gender     Male
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_mobile_number   1234567890
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_mobile_number     2345678901
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tertiary_mobile_number      0123456789
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      bank_account_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      bank_is_verified     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      status_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      remark        robot_agent_accreditation_remark_updated
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      risk_level    High
    ${gen_verify_date}=     Get Current Date            result_format=%Y-%m-%dT00:00:00Z
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      verify_date   ${gen_verify_date}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API Admin updates agent profiles    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API Admin updates agent profiles
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_testing_account     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_system_account     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      acquisition_source     internet_updated
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_id     2
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_name     agent
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mm_card_type_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mm_card_level_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_require_otp     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      agent_classification_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      date_of_birth     1988-01-01T00:00:00Z
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      gender     Male
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_mobile_number   1234567890
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_mobile_number     2345678901
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tertiary_mobile_number      0123456789
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      bank_account_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      bank_is_verified     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      status_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      remark        robot_agent_accreditation_remark_updated
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      risk_level    High
    ${gen_verify_date}=     Get Current Date            result_format=%Y-%m-%dT00:00:00Z
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      verify_date   ${gen_verify_date}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API Admin updates agent profiles    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API Admin updates agent profiles with PATCH method
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_testing_account     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_system_account     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      acquisition_source     internet_updated
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_id     2
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_name     agent
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mm_card_type_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mm_card_level_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_require_otp     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      agent_classification_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      date_of_birth     1988-01-01T00:00:00Z
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      gender     Male
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_mobile_number   1234567890
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_mobile_number     2345678901
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tertiary_mobile_number      0123456789
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      bank_account_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      bank_is_verified     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      status_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      remark        robot_agent_accreditation_remark_updated
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      risk_level    High
    ${gen_verify_date}=     Get Current Date            result_format=%Y-%m-%dT00:00:00Z
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      verify_date   ${gen_verify_date}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API Admin updates agent profiles with PATCH method    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API Admin updates agent profiles with PATCH method
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_testing_account     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_system_account     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      acquisition_source     internet_updated
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_id     2
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_name     agent
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mm_card_type_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mm_card_level_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_require_otp     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      agent_classification_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      date_of_birth     1988-01-01T00:00:00Z
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      gender     Male
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_mobile_number   1234567890
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_mobile_number     2345678901
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tertiary_mobile_number      0123456789
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      bank_account_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      bank_is_verified     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      status_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      remark        robot_agent_accreditation_remark_updated
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      risk_level    High
    ${gen_verify_date}=     Get Current Date            result_format=%Y-%m-%dT00:00:00Z
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      verify_date   ${gen_verify_date}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API Admin updates agent profiles with PATCH method    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API Agent updates agent profiles
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_id     2
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_name     agent
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mm_card_type_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mm_card_level_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_require_otp     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      date_of_birth     1988-01-01T00:00:00Z
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      gender     Male
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_mobile_number   1234567890
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_mobile_number     2345678901
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tertiary_mobile_number      0123456789
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API agent updates agent profiles    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API Agent updates agent profiles
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_id     2
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_name     agent
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mm_card_type_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mm_card_level_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_require_otp     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      date_of_birth     1988-01-01T00:00:00Z
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      gender     Male
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_mobile_number   1234567890
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_mobile_number     2345678901
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tertiary_mobile_number      0123456789
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API agent updates agent profiles    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API Agent updates agent profiles with PATCH method
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_id     2
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_name     agent
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mm_card_type_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mm_card_level_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_require_otp     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      date_of_birth     1988-01-01T00:00:00Z
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      gender     Male
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_mobile_number   1234567890
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_mobile_number     2345678901
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tertiary_mobile_number      0123456789
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API agent updates agent profiles with PATCH method    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API Agent updates agent profiles with PATCH method
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_id     2
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_type_name     agent
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      referrer_user_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mm_card_type_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mm_card_level_id     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_require_otp     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      date_of_birth     1988-01-01T00:00:00Z
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      gender     Male
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      primary_mobile_number   1234567890
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      secondary_mobile_number     2345678901
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      tertiary_mobile_number      0123456789
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API agent updates agent profiles with PATCH method    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API get agent profiles
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API get agent profiles    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API get agent profiles
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API get agent profiles    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API reset password
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API reset password    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API reset password
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API reset password    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API agent resets password
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      user_type_id     2
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      user_type_name     agent
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API agent resets password    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API agent resets password
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      user_type_id     2
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      user_type_name     agent
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API agent resets password    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API change password
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id         ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API change password    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API change password
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API change password    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API get all relationship type
    [Arguments]
    ...     ${client_id}
    ...     ${client_secret}
    ...     ${access_token}
    ${dic}     API get all relationship type
    ...     ${client_id}
    ...     ${client_secret}
    ...     ${access_token}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    ${length}=      get length      ${response.json()['data']}
    ${data}=   set variable         ${response.json()['data']}
    ${names}=       create list
    :FOR    ${index}     IN RANGE       0      ${length}
    \       ${name}=        set variable    ${data[${index}]['name']}
    \       append to list       ${names}     ${name}
    set to dictionary   ${dic}   name    ${names}
    [Return]    ${dic}

[200] API delete agent
	[Arguments]
    ... 	${access_token}
    ... 	${agent_id}
	... 	${client_id}=${setup_admin_client_id}
	... 	${client_secret}=${setup_admin_client_secret}
	${dic}	API delete agent
    ... 	${access_token}
    ... 	${agent_id}
	... 	${client_id}
	... 	${client_secret}
	${response}=    get from dictionary    ${dic}   response
	should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[200] API create agent
    [Arguments]  ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id         ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API create agent        ${arg_dic}
	${response}=    get from dictionary    ${dic}   response
	should be equal as integers     ${response.status_code}     200
	set to dictionary       ${dic}      id   ${response.json()['data']['id']}
	[Return]    ${dic}

[200] API create agent profiles
    [Arguments]
    ...     ${access_token}
    ...     ${agent_type_id}
    ...     ${unique_reference}
    ...     ${is_testing_account}=${param_not_used}
    ...     ${is_system_account}=${param_not_used}
    ...     ${acquisition_source}=${param_not_used}
    ...     ${referrer_user_type_id}=${param_not_used}
    ...     ${referrer_user_type_name}=${param_not_used}
    ...     ${referrer_user_id}=${param_not_used}
    ...     ${mm_card_type_id}=${param_not_used}
    ...     ${mm_card_level_id}=${param_not_used}
    ...     ${mm_factory_card_number}=${param_not_used}
    ...     ${model_type}=${param_not_used}
    ...     ${is_require_otp}=${param_not_used}
    ...     ${agent_classification_id}=${param_not_used}
    ...     ${tin_number}=${param_not_used}
    ...     ${title}=${param_not_used}
    ...     ${first_name}=${param_not_used}
    ...     ${middle_name}=${param_not_used}
    ...     ${last_name}=${param_not_used}
    ...     ${suffix}=${param_not_used}
    ...     ${date_of_birth}=${param_not_used}
    ...     ${place_of_birth}=${param_not_used}
    ...     ${gender}=${param_not_used}
    ...     ${ethnicity}=${param_not_used}
    ...     ${nationality}=${param_not_used}
    ...     ${occupation}=${param_not_used}
    ...     ${occupation_title}=${param_not_used}
    ...     ${township_code}=${param_not_used}
    ...     ${township_name}=${param_not_used}
    ...     ${national_id_number}=${param_not_used}
    ...     ${mother_name}=${param_not_used}
    ...     ${email}=${param_not_used}
    ...     ${primary_mobile_number}=${param_not_used}
    ...     ${secondary_mobile_number}=${param_not_used}
    ...     ${tertiary_mobile_number}=${param_not_used}
    ...     ${current_address_citizen_association}=${param_not_used}
    ...     ${current_address_neighbourhood_association}=${param_not_used}
    ...     ${current_address_address}=${param_not_used}
    ...     ${current_address_commune}=${param_not_used}
    ...     ${current_address_district}=${param_not_used}
    ...     ${current_address_city}=${param_not_used}
    ...     ${current_address_province}=${param_not_used}
    ...     ${current_address_postal_code}=${param_not_used}
    ...     ${current_address_country}=${param_not_used}
    ...     ${current_address_landmark}=${param_not_used}
    ...     ${current_address_longitude}=${param_not_used}
    ...     ${current_address_latitude}=${param_not_used}
    ...     ${permanent_address_citizen_association}=${param_not_used}
    ...     ${permanent_address_neighbourhood_association}=${param_not_used}
    ...     ${permanent_address_address}=${param_not_used}
    ...     ${permanent_address_commune}=${param_not_used}
    ...     ${permanent_address_district}=${param_not_used}
    ...     ${permanent_address_city}=${param_not_used}
    ...     ${permanent_address_province}=${param_not_used}
    ...     ${permanent_address_postal_code}=${param_not_used}
    ...     ${permanent_address_country}=${param_not_used}
    ...     ${permanent_address_landmark}=${param_not_used}
    ...     ${permanent_address_longitude}=${param_not_used}
    ...     ${permanent_address_latitude}=${param_not_used}
    ...     ${bank_name}=${param_not_used}
    ...     ${bank_account_status}=${param_not_used}
    ...     ${bank_account_name}=${param_not_used}
    ...     ${bank_account_number}=${param_not_used}
    ...     ${bank_branch_area}=${param_not_used}
    ...     ${bank_branch_city}=${param_not_used}
    ...     ${bank_register_date}=${param_not_used}
    ...     ${bank_register_source}=${param_not_used}
    ...     ${bank_is_verified}=${param_not_used}
    ...     ${bank_end_date}=${param_not_used}
    ...     ${contract_type}=${param_not_used}
    ...     ${contract_number}=${param_not_used}
    ...     ${contract_extension_type}=${param_not_used}
    ...     ${contract_sign_date}=${param_not_used}
    ...     ${contract_issue_date}=${param_not_used}
    ...     ${contract_expired_date}=${param_not_used}
    ...     ${contract_notification_alert}=${param_not_used}
    ...     ${contract_day_of_period_reconciliation}=${param_not_used}
    ...     ${contract_release}=${param_not_used}
    ...     ${contract_file_url}=${param_not_used}
    ...     ${contract_assessment_information_url}=${param_not_used}
    ...     ${primary_identity_type}=${param_not_used}
    ...     ${primary_identity_status}=${param_not_used}
    ...     ${primary_identity_identity_id}=${param_not_used}
    ...     ${primary_identity_place_of_issue}=${param_not_used}
    ...     ${primary_identity_issue_date}=${param_not_used}
    ...     ${primary_identity_expired_date}=${param_not_used}
    ...     ${primary_identity_front_identity_url}=${param_not_used}
    ...     ${primary_identity_back_identity_url}=${param_not_used}
    ...     ${secondary_identity_type}=${param_not_used}
    ...     ${secondary_identity_status}=${param_not_used}
    ...     ${secondary_identity_identity_id}=${param_not_used}
    ...     ${secondary_identity_place_of_issue}=${param_not_used}
    ...     ${secondary_identity_issue_date}=${param_not_used}
    ...     ${secondary_identity_expired_date}=${param_not_used}
    ...     ${secondary_identity_front_identity_url}=${param_not_used}
    ...     ${secondary_identity_back_identity_url}=${param_not_used}
    ...     ${status_id}=${param_not_used}
    ...     ${remark}=${param_not_used}
    ...     ${verify_by}=${param_not_used}
    ...     ${verify_date}=${param_not_used}
    ...     ${risk_level}=${param_not_used}
    ...     ${additional_acquiring_sales_executive_id}=${param_not_used}
    ...     ${additional_acquiring_sales_executive_name}=${param_not_used}
    ...     ${additional_relationship_manager_id}=${param_not_used}
    ...     ${additional_relationship_manager_name}=${param_not_used}
    ...     ${additional_sale_region}=${param_not_used}
    ...     ${additional_commercial_account_manager}=${param_not_used}
    ...     ${additional_profile_picture_url}=${param_not_used}
    ...     ${additional_national_id_photo_url}=${param_not_used}
    ...     ${additional_tax_id_card_photo_url}=${param_not_used}
    ...     ${additional_field_1_name}=${param_not_used}
    ...     ${additional_field_1_value}=${param_not_used}
    ...     ${additional_field_2_name}=${param_not_used}
    ...     ${additional_field_2_value}=${param_not_used}
    ...     ${additional_field_3_name}=${param_not_used}
    ...     ${additional_field_3_value}=${param_not_used}
    ...     ${additional_field_4_name}=${param_not_used}
    ...     ${additional_field_4_value}=${param_not_used}
    ...     ${additional_field_5_name}=${param_not_used}
    ...     ${additional_field_5_value}=${param_not_used}
    ...     ${additional_supporting_file_1_url}=${param_not_used}
    ...     ${additional_supporting_file_2_url}=${param_not_used}
    ...     ${additional_supporting_file_3_url}=${param_not_used}
    ...     ${additional_supporting_file_4_url}=${param_not_used}
    ...     ${additional_supporting_file_5_url}=${param_not_used}
    ...     ${is_sale}=${param_not_used}
    ...     ${sale_employee_id}=${param_not_used}
    ...     ${sale_calendar_id}=${param_not_used}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ...     ${company_id}=${param_not_used}
    ${dic}  API create agent profiles
    ...     ${access_token}
    ...     ${agent_type_id}
    ...     ${unique_reference}
    ...     ${is_testing_account}
    ...     ${is_system_account}
    ...     ${acquisition_source}
    ...     ${referrer_user_type_id}
    ...     ${referrer_user_type_name}
    ...     ${referrer_user_id}
    ...     ${mm_card_type_id}
    ...     ${mm_card_level_id}
    ...     ${mm_factory_card_number}
    ...     ${model_type}
    ...     ${is_require_otp}
    ...     ${agent_classification_id}
    ...     ${tin_number}
    ...     ${title}
    ...     ${first_name}
    ...     ${middle_name}
    ...     ${last_name}
    ...     ${suffix}
    ...     ${date_of_birth}
    ...     ${place_of_birth}
    ...     ${gender}
    ...     ${ethnicity}
    ...     ${nationality}
    ...     ${occupation}
    ...     ${occupation_title}
    ...     ${township_code}
    ...     ${township_name}
    ...     ${national_id_number}
    ...     ${mother_name}
    ...     ${email}
    ...     ${primary_mobile_number}
    ...     ${secondary_mobile_number}
    ...     ${tertiary_mobile_number}
    ...     ${current_address_citizen_association}
    ...     ${current_address_neighbourhood_association}
    ...     ${current_address_address}
    ...     ${current_address_commune}
    ...     ${current_address_district}
    ...     ${current_address_city}
    ...     ${current_address_province}
    ...     ${current_address_postal_code}
    ...     ${current_address_country}
    ...     ${current_address_landmark}
    ...     ${current_address_longitude}
    ...     ${current_address_latitude}
    ...     ${permanent_address_citizen_association}
    ...     ${permanent_address_neighbourhood_association}
    ...     ${permanent_address_address}
    ...     ${permanent_address_commune}
    ...     ${permanent_address_district}
    ...     ${permanent_address_city}
    ...     ${permanent_address_province}
    ...     ${permanent_address_postal_code}
    ...     ${permanent_address_country}
    ...     ${permanent_address_landmark}
    ...     ${permanent_address_longitude}
    ...     ${permanent_address_latitude}
    ...     ${bank_name}
    ...     ${bank_account_status}
    ...     ${bank_account_name}
    ...     ${bank_account_number}
    ...     ${bank_branch_area}
    ...     ${bank_branch_city}
    ...     ${bank_register_date}
    ...     ${bank_register_source}
    ...     ${bank_is_verified}
    ...     ${bank_end_date}
    ...     ${contract_type}
    ...     ${contract_number}
    ...     ${contract_extension_type}
    ...     ${contract_sign_date}
    ...     ${contract_issue_date}
    ...     ${contract_expired_date}
    ...     ${contract_notification_alert}
    ...     ${contract_day_of_period_reconciliation}
    ...     ${contract_release}
    ...     ${contract_file_url}
    ...     ${contract_assessment_information_url}
    ...     ${primary_identity_type}
    ...     ${primary_identity_status}
    ...     ${primary_identity_identity_id}
    ...     ${primary_identity_place_of_issue}
    ...     ${primary_identity_issue_date}
    ...     ${primary_identity_expired_date}
    ...     ${primary_identity_front_identity_url}
    ...     ${primary_identity_back_identity_url}
    ...     ${secondary_identity_type}
    ...     ${secondary_identity_status}
    ...     ${secondary_identity_identity_id}
    ...     ${secondary_identity_place_of_issue}
    ...     ${secondary_identity_issue_date}
    ...     ${secondary_identity_expired_date}
    ...     ${secondary_identity_front_identity_url}
    ...     ${secondary_identity_back_identity_url}
    ...     ${status_id}
    ...     ${remark}
    ...     ${verify_by}
    ...     ${verify_date}
    ...     ${risk_level}
    ...     ${additional_acquiring_sales_executive_id}
    ...     ${additional_acquiring_sales_executive_name}
    ...     ${additional_relationship_manager_id}
    ...     ${additional_relationship_manager_name}
    ...     ${additional_sale_region}
    ...     ${additional_commercial_account_manager}
    ...     ${additional_profile_picture_url}
    ...     ${additional_national_id_photo_url}
    ...     ${additional_tax_id_card_photo_url}
    ...     ${additional_field_1_name}
    ...     ${additional_field_1_value}
    ...     ${additional_field_2_name}
    ...     ${additional_field_2_value}
    ...     ${additional_field_3_name}
    ...     ${additional_field_3_value}
    ...     ${additional_field_4_name}
    ...     ${additional_field_4_value}
    ...     ${additional_field_5_name}
    ...     ${additional_field_5_value}
    ...     ${additional_supporting_file_1_url}
    ...     ${additional_supporting_file_2_url}
    ...     ${additional_supporting_file_3_url}
    ...     ${additional_supporting_file_4_url}
    ...     ${additional_supporting_file_5_url}
    ...     ${is_sale}
    ...     ${sale_employee_id}
    ...     ${sale_calendar_id}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ...     ${company_id}
	${response}=    get from dictionary    ${dic}   response
	should be equal as integers     ${response.status_code}     200
	set to dictionary       ${dic}      id   ${response.json()['data']['id']}
	[Return]    ${dic}

[200] API add agent identity
    [Arguments]
    ...     ${agent_id}
    ...     ${identity_type_id}
    ...     ${username}
    ...     ${password}
    ...     ${access_token}
    ...     ${client_id}=${setup_admin_client_id}
    ...     ${client_secret}=${setup_admin_client_secret}
    ${dic}   API add agent identity
    ...     ${agent_id}
    ...     ${identity_type_id}
    ...     ${username}
    ...     ${password}
    ...     ${access_token}
    ...     ${client_id}
    ...     ${client_secret}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API agent adds identity
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      user_type_id     2
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      user_type_name     agent
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API agent adds identity    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API agent adds identity
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      user_type_id     2
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      user_type_name     agent
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API agent adds identity    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API update agent identity
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update agent identity    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API update agent identity
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update agent identity    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API delete agent identity
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API delete agent identity    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API delete agent identity
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API delete agent identity    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API get agent identities
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API get agent identities    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API get agent identities
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API get agent identities    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

#[200] [Old]call create agent api
#    [Arguments]
#    ...     ${agent_type_id}
#    ...     ${header_access_token}
#    ...     ${firstname}
#    ...     ${lastname}
#    ...     ${primary_mobile_number}
#    ...     ${card_id}
#    ...     ${email}
#    ...     ${address}
#    ...     ${unique_reference}
#    ...     ${client_id}=${setup_admin_client_id}
#    ...     ${client_secret}=${setup_admin_client_secret}
#    ${dic}   [Old]call create agent api
#    ...     ${agent_type_id}
#    ...     ${header_access_token}
#    ...     ${firstname}
#    ...     ${lastname}
#    ...     ${primary_mobile_number}
#    ...     ${card_id}
#    ...     ${email}
#    ...     ${address}
#    ...     ${unique_reference}
#    ...     ${client_id}
#    ...     ${client_secret}
#    ${response}    get from dictionary     ${dic}    response
#    should be equal as integers     ${response.status_code}    200
#    ${id}       set variable  ${response.json()['data']['id']}
#    set to dictionary   ${dic}   id    ${id}
#    [Return]   ${dic}

[200] API active-suspend agent
    [Arguments]
    ...     ${access_token}
    ...     ${is_suspended}
    ...     ${agent_id}
    ...     ${active_suspend_reason}=robot_active_suspend_reason
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API active-suspend agent
    ...     ${access_token}
    ...     ${is_suspended}
    ...     ${agent_id}
    ...     ${active_suspend_reason}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API create company type
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create company type    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[Fail] API create company type
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      description     robot_company_type_desciption
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create company type    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API create company representative profiles
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
     API create company represetative profiles    ${arg_dic}
     REST.integer    response status    200
     REST.string   $.status.code     success
     REST.string   $.status.message     Success
     REST.integer   $.data.id
     ${id}  rest extract    $.data.id
     ${data}  rest extract    $.data
     ${dic}     create dictionary   id=${id}    data=${data}
    log dictionary     ${dic}
    [Return]    ${dic}

[Fail] API create company representative profiles
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
     API create company represetative profiles    ${arg_dic}
     ${fail_status}     rest extract     response status
     ${code}  rest extract    $.status.code
     ${message}  rest extract    $.status.message
     ${dic}     create dictionary   fail_status=${fail_status}    fail_code=${code}      fail_message=${message}
    [Return]    ${dic}

[200] API update company representative profiles
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    API update company represetative profiles    ${arg_dic}
    REST.integer    response status    200
    REST.string   $.status.code     success
    REST.string   $.status.message     Success

[Fail] API update company representative profiles
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
     API update company represetative profiles    ${arg_dic}
     ${fail_status}     rest extract     response status
     ${code}  rest extract    $.status.code
     ${message}  rest extract    $.status.message
     ${dic}     create dictionary   fail_status=${fail_status}    fail_code=${code}      fail_message=${message}
    [Return]    ${dic}

[200] API delete company representative profile
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    API delete company representative profile    ${arg_dic}
    REST.integer    response status    200
    REST.string   $.status.code     success
    REST.string   $.status.message     Success

[Fail] API delete company representative profiles
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
     API delete company representative profile    ${arg_dic}
     ${fail_status}     rest extract     response status
     ${code}  rest extract    $.status.code
     ${message}  rest extract    $.status.message
     ${dic}     create dictionary   fail_status=${fail_status}    fail_code=${code}      fail_message=${message}
    [Return]    ${dic}

[200] API create company profiles
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_testing_account      false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      bank_account_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      bank_is_verified     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      bank_source_of_funds     USD
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      representative_primary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create company profiles    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[200] API create salary group
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}      API create salary group     ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[200] API link customer to company
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}      API link customer to company    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200

[Fail] API create company profiles
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_testing_account      false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      bank_account_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      bank_is_verified     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      bank_source_of_funds     USD
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      representative_primary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create company profiles    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API update company profiles
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_testing_account      true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      bank_account_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      bank_is_verified     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      bank_source_of_funds     VND
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      representative_primary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update company profiles    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API update company profiles
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_testing_account      true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      bank_account_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      bank_is_verified     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      bank_source_of_funds     VND
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      representative_primary_identity_status     1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update company profiles    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API delete company profiles
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API delete company profiles    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API delete company profiles
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API delete company profiles    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    ${arg_dic.code}
    [Return]   ${dic}

[200] API create smart card
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create smart card    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[200] API get smart card by serial number
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API get smart card by serial number    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    data   ${response.json()['data']}
    [Return]    ${dic}

[Fail] API get smart card by serial number
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API get smart card by serial number    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    set to dictionary    ${dic}    status   ${response.json()['status']}
    [Return]    ${dic}

[200] API update smart card
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update smart card    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API create smart card
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create smart card    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API delete smart card
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API delete smart card    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API delete smart card by id
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API delete smart card by id    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API delete smart card
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API delete smart card    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    ${arg_dic.code}
    [Return]   ${dic}

[200] API create shop type
    [Arguments]
    ...     ${access_token}
    ...     ${name}
    ...     ${description}=robot_shop_type_description
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API create shop type
    ...     ${access_token}
    ...     ${name}
    ...     ${description}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    ${id}       set variable    ${response.json()['data']['id']}
    set to dictionary   ${dic}   id    ${id}
    [Return]   ${dic}

[200] API update shop type
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      description     robot_shop_type_desciption_updated
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update shop type    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API update shop type
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      description     robot_shop_type_desciption_updated
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update shop type    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API delete shop type
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API delete shop type   ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API delete shop type
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API delete shop type    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    ${arg_dic.code}
    [Return]   ${dic}

[200] API create shop category
    [Arguments]
    ...     ${access_token}
    ...     ${name}
    ...     ${description}=robot_shop_category_description
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API create shop category
    ...     ${access_token}
    ...     ${name}
    ...     ${description}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    ${id}       set variable    ${response.json()['data']['id']}
    set to dictionary   ${dic}   id    ${id}
    [Return]   ${dic}

[200] API update shop category
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      description     robot_shop_category_desciption_updated
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update shop category    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API update shop category
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      description     robot_shop_category_desciption_updated
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update shop category    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API delete shop category
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API delete shop category   ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API delete shop category
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API delete shop category   ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    ${arg_dic.code}
    [Return]   ${dic}

[200] API create shop
    [Arguments]
    ...     ${access_token}
    ...     ${agent_id}
    ...     ${shop_category_id}
    ...     ${name}
    ...     ${name_local}=${param_not_used}
    ...     ${acquisition_source}=${param_not_used}
    ...     ${shop_mobile_number}=${param_not_used}
    ...     ${shop_telephone_number}=${param_not_used}
    ...     ${shop_email}=${param_not_used}
    ...     ${truemoney_signage}=${param_not_used}
    ...     ${requested_services}=${param_not_used}
    ...     ${network_coverage}=${param_not_used}
    ...     ${premises}=${param_not_used}
    ...     ${physical_security}=${param_not_used}
    ...     ${number_of_staff}=${param_not_used}
    ...     ${business_hour}=${param_not_used}
    ...     ${shop_type_id}=${param_not_used}
    ...     ${address_citizen_association}=${param_not_used}
    ...     ${address_neighbourhood_association}=${param_not_used}
    ...     ${address_address}=${param_not_used}
    ...     ${address_commune}=${param_not_used}
    ...     ${address_district}=${param_not_used}
    ...     ${address_city}=${param_not_used}
    ...     ${address_province}=${param_not_used}
    ...     ${address_postal_code}=${param_not_used}
    ...     ${address_country}=${param_not_used}
    ...     ${address_landmark}=${param_not_used}
    ...     ${address_longitude}=${param_not_used}
    ...     ${address_latitude}=${param_not_used}
    ...     ${address_citizen_association_local}=${param_not_used}
    ...     ${address_neighbourhood_association_local}=${param_not_used}
    ...     ${address_address_local}=${param_not_used}
    ...     ${address_commune_local}=${param_not_used}
    ...     ${address_district_local}=${param_not_used}
    ...     ${address_city_local}=${param_not_used}
    ...     ${address_province_local}=${param_not_used}
    ...     ${address_postal_code_local}=${param_not_used}
    ...     ${address_country_local}=${param_not_used}
    ...     ${address_landmark_local}=${param_not_used}
    ...     ${additional_supporting_file_1_url}=${param_not_used}
    ...     ${additional_supporting_file_2_url}=${param_not_used}
    ...     ${additional_ref_1}=${param_not_used}
    ...     ${additional_ref_2}=${param_not_used}
    ...     ${additional_ref1_local}=${param_not_used}
    ...     ${additional_ref2_local}=${param_not_used}
    ...     ${additional_acquiring_sale_executive_name}=${param_not_used}
    ...     ${additional_acquiring_sale_executive_name_local}=${param_not_used}
    ...     ${additional_relationship_manager_name}=${param_not_used}
    ...     ${additional_relationship_manager_name_local}=${param_not_used}
    ...     ${additional_relationship_manager_id}=${param_not_used}
    ...     ${additional_relationship_manager_email}=${param_not_used}
    ...     ${additional_sale_region}=${param_not_used}
    ...     ${additional_account_manager_name}=${param_not_used}
    ...     ${additional_account_manager_name_local}=${param_not_used}
    ...     ${additional_shop_photo}=${param_not_used}
    ...     ${additional_shop_map}=${param_not_used}
    ...     ${representative_first_name}=${param_not_used}
    ...     ${representative_first_name_local}=${param_not_used}
    ...     ${representative_middle_name}=${param_not_used}
    ...     ${representative_middle_name_local}=${param_not_used}
    ...     ${representative_last_name}=${param_not_used}
    ...     ${representative_last_name_local}=${param_not_used}
    ...     ${representative_mobile_number}=${param_not_used}
    ...     ${representative_telephone_number}=${param_not_used}
    ...     ${representative_email}=${param_not_used}

    ...     ${header_device_id}=${param_not_used}
    ...     ${header_device_description}=${param_not_used}
    ...     ${header_device_imei}=${param_not_used}
    ...     ${header_device_ip4}=${param_not_used}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}

    ${dic}  API create shop
    ...     ${access_token}
    ...     ${agent_id}
    ...     ${shop_category_id}
    ...     ${name}
    ...     ${name_local}
    ...     ${acquisition_source}
    ...     ${shop_mobile_number}
    ...     ${shop_telephone_number}
    ...     ${shop_email}
    ...     ${truemoney_signage}
    ...     ${requested_services}
    ...     ${network_coverage}
    ...     ${premises}
    ...     ${physical_security}
    ...     ${number_of_staff}
    ...     ${business_hour}
    ...     ${shop_type_id}
    ...     ${address_citizen_association}
    ...     ${address_neighbourhood_association}
    ...     ${address_address}
    ...     ${address_commune}
    ...     ${address_district}
    ...     ${address_city}
    ...     ${address_province}
    ...     ${address_postal_code}
    ...     ${address_country}
    ...     ${address_landmark}
    ...     ${address_longitude}
    ...     ${address_latitude}
    ...     ${address_citizen_association_local}
    ...     ${address_neighbourhood_association_local}
    ...     ${address_address_local}
    ...     ${address_commune_local}
    ...     ${address_district_local}
    ...     ${address_city_local}
    ...     ${address_province_local}
    ...     ${address_postal_code_local}
    ...     ${address_country_local}
    ...     ${address_landmark_local}
    ...     ${additional_supporting_file_1_url}
    ...     ${additional_supporting_file_2_url}
    ...     ${additional_ref_1}
    ...     ${additional_ref_2}
    ...     ${additional_ref1_local}
    ...     ${additional_ref2_local}
    ...     ${additional_acquiring_sale_executive_name}
    ...     ${additional_acquiring_sale_executive_name_local}
    ...     ${additional_relationship_manager_name}
    ...     ${additional_relationship_manager_name_local}
    ...     ${additional_relationship_manager_id}
    ...     ${additional_relationship_manager_email}
    ...     ${additional_sale_region}
    ...     ${additional_account_manager_name}
    ...     ${additional_account_manager_name_local}
    ...     ${additional_shop_photo}
    ...     ${additional_shop_map}
    ...     ${representative_first_name}
    ...     ${representative_first_name_local}
    ...     ${representative_middle_name}
    ...     ${representative_middle_name_local}
    ...     ${representative_last_name}
    ...     ${representative_last_name_local}
    ...     ${representative_mobile_number}
    ...     ${representative_telephone_number}
    ...     ${representative_email}


    ...     ${header_device_id}
    ...     ${header_device_description}
    ...     ${header_device_imei}
    ...     ${header_device_ip4}
    ...     ${header_client_id}
    ...     ${header_client_secret}
	${response}=    get from dictionary    ${dic}   response
	should be equal as integers     ${response.status_code}     200
	set to dictionary       ${dic}      id   ${response.json()['data']['id']}
	[Return]    ${dic}

[200] API update shop
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update shop    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API update shop
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update shop    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API delete shop
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API delete shop    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API delete shop
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API delete shop    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API create edc
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create edc   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[Fail] API create edc
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create edc    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API update edc
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update edc   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API update edc
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update edc    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API delete edc
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API delete edc   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API delete edc
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API delete edc    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API create relationship
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      main_user_type_id     2
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      main_user_type_name     agent
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      sub_user_type_id     2
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      sub_user_type_name     agent
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      access_token     ${suite_admin_access_token}
    ${dic}     API create relationship   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[Fail] API create relationship
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      main_user_type_id     2
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      main_user_type_name     agent
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      sub_user_type_id     2
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      sub_user_type_name     agent
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create relationship   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API update relationship
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_sharing_benefit     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update relationship   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API update relationship
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_sharing_benefit     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update relationship   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API delete relationship
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      access_token     ${suite_admin_access_token}
    ${dic}     API delete relationship   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API delete relationship
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API delete relationship   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API get relationship
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API get relationship   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API get relationship
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API get relationship   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API get sub agents
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API get sub agents   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API get sub agents
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API get sub agents   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API authenticate agent
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id            ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret        ${setup_admin_client_secret}
#    [Common] - Set default value for keyword in dictionary     ${arg_dic}      device_unique_reference      ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      channel_id                    1
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      grant_type                    password
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      param_client_id               ${setup_admin_client_id}
    ${dic}     API agent authentication   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
	should be equal as integers     ${response.status_code}     200
	${access_token}         set variable    ${response.json()['access_token']}
	set to dictionary           ${dic}          access_token        ${access_token}
	[Return]    ${dic}

[200] API agent get his/her shop list
    [Arguments]
    ...     ${access_token}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}	API agent get his/her shop list
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}=    get from dictionary    ${dic}   response
	should be equal as integers     ${response.status_code}     200
	[Return]    ${dic}

[200] agent create device
    [Arguments]
    ...     ${access_token}
    ...     ${device_unique_reference}
    ...     ${device_model}
    ...     ${shop_id}
    ...     ${channel_type_id}
    ...     ${channel_id}
    ...     ${mac_address}
    ...     ${network_provider_name}
    ...     ${public_ip_address}
    ...     ${supporting_file_1}
    ...     ${supporting_file_2}
    ...     ${device_name}
    ...     ${os}
    ...     ${os_version}
    ...     ${display_size_in_inches}
    ...     ${pixel_counts}
    ...     ${unique_number}
    ...     ${serial_number}
    ...     ${app_version}
    ...     ${edc_serial_number}
    ...     ${edc_model}
    ...     ${edc_firmware_version}
    ...     ${edc_software_version}
    ...     ${edc_sim_card_number}
    ...     ${edc_battery_serial_number}
    ...     ${edc_adapter_serial_number}
    ...     ${edc_smartcard_1_number}
    ...     ${edc_smartcard_2_number}
    ...     ${edc_smartcard_3_number}
    ...     ${edc_smartcard_4_number}
    ...     ${edc_smartcard_5_number}
    ...     ${pos_serial_number}
    ...     ${pos_model}
    ...     ${pos_firmware_version}
    ...     ${pos_software_version}
    ...     ${pos_smartcard_1_number}
    ...     ${pos_smartcard_2_number}
    ...     ${pos_smartcard_3_number}
    ...     ${pos_smartcard_4_number}
    ...     ${pos_smartcard_5_number}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API agent create device
    ...     ${access_token}
    ...     ${device_unique_reference}
    ...     ${device_model}
    ...     ${shop_id}
    ...     ${channel_type_id}
    ...     ${channel_id}
    ...     ${mac_address}
    ...     ${network_provider_name}
    ...     ${public_ip_address}
    ...     ${supporting_file_1}
    ...     ${supporting_file_2}
    ...     ${device_name}
    ...     ${os}
    ...     ${os_version}
    ...     ${display_size_in_inches}
    ...     ${pixel_counts}
    ...     ${unique_number}
    ...     ${serial_number}
    ...     ${app_version}
    ...     ${edc_serial_number}
    ...     ${edc_model}
    ...     ${edc_firmware_version}
    ...     ${edc_software_version}
    ...     ${edc_sim_card_number}
    ...     ${edc_battery_serial_number}
    ...     ${edc_adapter_serial_number}
    ...     ${edc_smartcard_1_number}
    ...     ${edc_smartcard_2_number}
    ...     ${edc_smartcard_3_number}
    ...     ${edc_smartcard_4_number}
    ...     ${edc_smartcard_5_number}
    ...     ${pos_serial_number}
    ...     ${pos_model}
    ...     ${pos_firmware_version}
    ...     ${pos_software_version}
    ...     ${pos_smartcard_1_number}
    ...     ${pos_smartcard_2_number}
    ...     ${pos_smartcard_3_number}
    ...     ${pos_smartcard_4_number}
    ...     ${pos_smartcard_5_number}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API system user creates agent device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      shop_id     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      owner_id     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      network_provider_name     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mac_address     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      public_ip_address     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      supporting_file_1     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      supporting_file_2     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      device_name     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      device_model     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      device_unique_reference     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      os     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      os_version     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      display_size_in_inches     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      pixel_counts     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      unique_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      serial_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      app_version     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_serial_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_model     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_firmware_version     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_software_version     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_sim_card_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_battery_serial_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_adapter_serial_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_smartcard_1_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_smartcard_2_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_smartcard_3_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_smartcard_4_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_smartcard_5_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      pos_serial_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      pos_model     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      pos_firmware_version     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      pos_software_version     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      pos_smartcard_1_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      pos_smartcard_2_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      pos_smartcard_3_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      pos_smartcard_4_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      pos_smartcard_5_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      ssid     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API system user creates agent device  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[Fail] API system user creates agent device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      network_provider_name     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      mac_address     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      public_ip_address     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      supporting_file_1     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      supporting_file_2     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      device_name     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      device_model     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      device_unique_reference     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      os     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      os_version     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      display_size_in_inches     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      pixel_counts     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      unique_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      serial_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      app_version     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_serial_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_model     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_firmware_version     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_software_version     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_sim_card_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_battery_serial_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_adapter_serial_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_smartcard_1_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_smartcard_2_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_smartcard_3_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_smartcard_4_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      edc_smartcard_5_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      pos_serial_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      pos_model     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      pos_firmware_version     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      pos_software_version     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      pos_smartcard_1_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      pos_smartcard_2_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      pos_smartcard_3_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      pos_smartcard_4_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      pos_smartcard_5_number     ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API system user creates agent device  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API agent updates device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API agent updates device  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API agent updates device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API agent updates device  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API system user updates agent device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API system user updates agent device  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API system user updates agent device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API system user updates agent device  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API system user updates agent device status
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_active     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API system user updates agent device status  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}


[Fail] API system user updates agent device status
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_active     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API system user updates agent device status   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API agent deletes device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API agent deletes device  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API agent deletes device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API agent deletes device  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API system user deletes agent device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API system user deletes agent device    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API system user deletes agent device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API system user deletes agent device    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    ${arg_dic.code}
    [Return]   ${dic}

[200] API agent gets all devices
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API agent gets all devices    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API agent gets all devices
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${client_id_password_type}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${client_secret_password_type}
    ${dic}     API agent gets all devices  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API system user gets agent device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API system user gets agent device   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[Fail] API system user gets agent device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API system user gets agent device  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API system user links smart card to device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API system user links smart card to device  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[200] API delete agent type
    [Arguments]
    ... 	${access_token}
    ... 	${agentTypeId}
    ... 	${header_client_id}=${setup_admin_client_id}
    ... 	${header_client_secret}=${setup_admin_client_secret}
    ${dic}  API delete agent type
    ... 	${access_token}
    ... 	${agentTypeId}
    ... 	${header_client_id}
    ... 	${header_client_secret}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API create product category
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      description     robot_product_category_desciption
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_active     false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create product category  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[Fail] API create product category
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      description     robot_product_category_desciption
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_active     false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create product category   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API update product category
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      description     robot_product_category_desciption
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_active     false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update product category  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API update product category
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      description     robot_product_category_desciption
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_active     false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update product category   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API delete product category
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API delete product category    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API delete product category
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API delete product category    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    ${arg_dic.code}
    [Return]   ${dic}

[200] API create product
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      description     robot_product_desciption
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_allow_price_range     false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_active     false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create product   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[Fail] API create product
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      description     robot_product_desciption
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_allow_price_range     false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_active     false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create product    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API update product
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      description     robot_product_desciption
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_allow_price_range     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_active     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update product   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API update product
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      description     robot_product_desciption
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_allow_price_range     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_active     true
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API update product    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API delete product
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API delete product     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API delete product
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API delete product     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    ${arg_dic.code}
    [Return]   ${dic}

[200] API create relation between product and agent
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create relation between product and agent   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[Fail] API create relation between product and agent
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create relation between product and agent   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API delete relation between product and agent
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API delete relation between product and agent     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API delete relation between product and agent
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API delete relation between product and agent    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    ${arg_dic.code}
    [Return]   ${dic}

[200] API create relation between product and agent type
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create relation between product and agent type   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[Fail] API create relation between product and agent type
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create relation between product and agent type  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API delete relation between product and agent type
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API delete relation between product and agent type    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Fail] API delete relation between product and agent type
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API delete relation between product and agent type   ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    ${arg_dic.code}
    [Return]   ${dic}

[200] API create relation between services and agent
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create relation between services and agent   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[Fail] API create relation between services and agent
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create relation between services and agent  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API get relation between services and agent
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API get relation between services and agent   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Return]    ${dic}

[Fail] API get relation between services and agent
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API get relation between services and agent  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[Fail] API create agent identity
    [Arguments]     ${arg_dic}
    ${dic}     API create agent identity   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     400
    set to dictionary   ${dic}      status_code     ${response.json()['status']['code']}
    set to dictionary   ${dic}      status_message  ${response.json()['status']['message']}
    [Return]    ${dic}

[200] API get salary group by companyId
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API get salary groups by companyId    ${arg_dic}
    ${response}=        get from dictionary     ${dic}      response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API link agent profile with company profile
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API link agent profile with company profile   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API link agent profile with company profile
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API link agent profile with company profile    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     ${arg_dic.code}
    [Return]    ${dic}

[200] API system user update company wallet user
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API system user update company wallet user    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    [Return]    ${dic}

[200] API create sale hierachy
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create sale hierachy   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary   ${dic}      hierarchy_id     ${response.json()['data']['id']}
    [Return]    ${dic}

[200] API create connection
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create connection   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API create sale permissions configurations
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API create sale permissions configurations   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API create sale
    [Arguments]  ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id         ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}    API create sale profile        ${arg_dic}
	${response}=    get from dictionary    ${dic}   response
	should be equal as integers     ${response.status_code}     200
	set to dictionary       ${dic}      id   ${response.json()['data']['id']}
	[Return]    ${dic}

[200] System user delete device via api
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API system user deletes agent device    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API Inactive created device
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_active     false
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}     API system user updates agent device status  ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API create shop with dictionary params
    [Arguments]  ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    ${dic}  API create shop with dicionary params   ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary       ${dic}      id   ${response.json()['data']['id']}
    [Return]    ${dic}
