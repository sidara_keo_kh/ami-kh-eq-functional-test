*** Settings ***
Resource         ../1_raw-deprecated/imports.robot

*** Keywords ***
[200] API create request to cancel voucher
    [Arguments]
    ...     ${access_token}
    ...     ${currency}
    ...     ${voucher_id_1}
    ...     ${voucher_amount_1}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}     API create request to cancel voucher
    ...     ${access_token}
    ...     ${currency}
    ...     ${voucher_id_1}
    ...     ${voucher_amount_1}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary   ${dic}   voucher_cancellation_id    ${response.json()['data']['voucher_cancellation_id']}
    [Return]    ${dic}

[200] API confirm cancel vouchers
    [Arguments]
    ...     ${access_token}
    ...     ${amount}
    ...     ${voucher_cancellation_id}
    ...     ${client_id}=${setup_admin_client_id}
    ...     ${client_secret}=${setup_admin_client_secret}
    ${dic}     API confirm cancel vouchers
    ...     ${access_token}
    ...     ${amount}
    ...     ${voucher_cancellation_id}
    ...     ${client_id}
    ...     ${client_secret}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API create refund voucher
      [Arguments]
      ...     ${access_token}
      ...     ${voucher_id}
      ...     ${product_ref2}
      ...     ${product_ref3}
      ...     ${product_ref4}
      ...     ${product_ref5}
      ...     ${reason_for_refund}
      ...     ${original_voucher_id}
      ...     ${amount}
      ...     ${currency}
      ...     ${header_client_id}=${setup_admin_client_id}
      ...     ${header_client_secret}=${setup_admin_client_secret}
     ${dic}      API create refund voucher
       ...     ${access_token}
       ...     ${voucher_id}
       ...     ${product_ref2}
       ...     ${product_ref3}
       ...     ${product_ref4}
       ...     ${product_ref5}
       ...     ${reason_for_refund}
       ...     ${original_voucher_id}
       ...     ${amount}
       ...     ${currency}
       ...     ${header_client_id}
       ...     ${header_client_secret}
     ${response}=    get from dictionary    ${dic}   response
     should be equal as integers     ${response.status_code}     200
     set to dictionary   ${dic}   voucher_refund_id    ${response.json()['data']['voucher_refund_id']}
     [Return]    ${dic}

[200] API approve voucher refunds
    [Arguments]
    ...     ${access_token}
    ...     ${refund_request_id_1}
    ...     ${refund_request_id_2}
    ...     ${refund_request_id_3}
    ...     ${reason}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}
    ${dic}     API approve voucher refunds
    ...     ${access_token}
    ...     ${refund_request_id_1}
    ...     ${refund_request_id_2}
    ...     ${refund_request_id_3}
    ...     ${reason}
    ...     ${header_client_id}
    ...     ${header_client_secret}
     ${response}=    get from dictionary    ${dic}   response
     should be equal as integers     ${response.status_code}     200
     [Return]    ${dic}

[200] API execute adjustment order
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   reason       robot_reason
    ${dic}  API execute adjustment order      ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API create adjustment order
    [Arguments]     ${arg_dic}
    ${access_token}    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   reference_service_group_id       null
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   reference_service_id       null
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   cancel_ref_id       null
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_name       robot_product_name
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_ref1       robot_product_ref1
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_ref2       robot_product_ref2
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_ref3       robot_product_ref3
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_ref4       robot_product_ref4
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_ref5       robot_product_ref5
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   reason             reason
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   reference_service_group_id       null
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   reference_service_id             null
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   batch_code                       null
    ${dic}  API create adjustment order       ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary   ${dic}   order_id    ${response.json()['data']['order_id']}
    set to dictionary   ${dic}   reference_id    ${response.json()['data']['reference_id']}
    [Return]    ${dic}

[200] API reject voucher refunds
    [Arguments]    ${arg_dic}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   client_secret       ${setup_admin_client_secret}
    ${dic}  API reject voucher refunds      ${arg_dic}
    should be equal as integers     ${dic.response.status_code}     200
    [Return]    ${dic}

[200] API reject adjustment order
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    ${dic}   API reject adjustment order      ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API approve multiple balance adjustments
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    ${dic}   API approve multiple balance adjustments      ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[200] API reject multiple-balance adjustment orders
    [Arguments]         ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    ${dic}   API reject multiple-balance adjustment orders      ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}