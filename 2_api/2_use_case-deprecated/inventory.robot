*** Settings ***
Resource         ../1_raw-deprecated/imports.robot

*** Keyword ***
[Inventory][Common] - Add client information
    [Arguments]    ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	
    ...    ${arg_dic}     
    ...    client_id    
    ...    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	
    ...    ${arg_dic}     
    ...    client_secret    
    ...    ${setup_admin_client_secret}
    [Return]    ${arg_dic}
    
[Inventory][Common] - Remove client information
    [Arguments]    ${arg_dic}
    remove from dictionary    ${arg_dic}    client_id
    remove from dictionary    ${arg_dic}    client_secret
    [Return]    ${arg_dic}    
    
[Inventory][200] - API create single item
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API create single item    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_OK}
    should be equal as strings   ${response.json()['status']['code']}    ${inventory_success_code} 
    should be equal as strings   ${response.json()['status']['message']}    ${inventory_success_message}
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}

[Inventory][200] - API update single item
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API update single item    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_OK}
    should be equal as strings   ${response.json()['status']['code']}    ${inventory_success_code} 
    should be equal as strings   ${response.json()['status']['message']}    ${inventory_success_message}
    set to dictionary    ${dic}    id   ${response.json()['data']['id']} 
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}
    
[Inventory][200] - API create single item serial
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API create single item serial    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_OK}
    should be equal as strings   ${response.json()['status']['code']}    ${inventory_success_code} 
    should be equal as strings   ${response.json()['status']['message']}    ${inventory_success_message}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}
    
[Inventory][200] - API update single item quantity
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API update single item quantity    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_OK}
    should be equal as strings   ${response.json()['status']['code']}    ${inventory_success_code} 
    should be equal as strings   ${response.json()['status']['message']}    ${inventory_success_message}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}

[Inventory][200] - API get single item
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API get single item    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_OK}
    should be equal as strings   ${response.json()['status']['code']}    ${inventory_success_code} 
    should be equal as strings   ${response.json()['status']['message']}    ${inventory_success_message}
    :FOR    ${key}    IN    @{response.json()['data'].keys()}
    \    set to dictionary    ${dic}    ${key}    ${response.json()['data']['${key}']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}
    
[Inventory][200] - API get single item by sku
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API get single item by sku    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_OK}
    should be equal as strings   ${response.json()['status']['code']}    ${inventory_success_code} 
    should be equal as strings   ${response.json()['status']['message']}    ${inventory_success_message}
    :FOR    ${key}    IN    @{response.json()['data'].keys()}
    \    set to dictionary    ${dic}    ${key}    ${response.json()['data']['${key}']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}    

[Inventory][200] - API get single item serial
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API get single item serial    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_OK}
    should be equal as strings   ${response.json()['status']['code']}    ${inventory_success_code} 
    should be equal as strings   ${response.json()['status']['message']}    ${inventory_success_message}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}
    
[Inventory][200] - API get single item serial filter by serial
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API get single item serial filter by serial    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_OK}
    should be equal as strings   ${response.json()['status']['code']}    ${inventory_success_code} 
    should be equal as strings   ${response.json()['status']['message']}    ${inventory_success_message}
    set to dictionary    ${dic}    data    ${response.json()['data']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}

[Inventory][200] - API create group item
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API create group item    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_OK}
    should be equal as strings   ${response.json()['status']['code']}    ${inventory_success_code}
    should be equal as strings   ${response.json()['status']['message']}    ${inventory_success_message}
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}

[Inventory][200] - API get group
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API get group    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_OK}
    should be equal as strings   ${response.json()['status']['code']}    ${inventory_success_code}
    should be equal as strings   ${response.json()['status']['message']}    ${inventory_success_message}
    :FOR    ${key}    IN    @{response.json()['data'].keys()}
    \    set to dictionary    ${dic}    ${key}    ${response.json()['data']['${key}']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}

[Inventory][200] - API update group item
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API update group    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_OK}
    should be equal as strings   ${response.json()['status']['code']}    ${inventory_success_code}
    should be equal as strings   ${response.json()['status']['message']}    ${inventory_success_message}
    :FOR    ${key}    IN    @{response.json()['data'].keys()}
    \    set to dictionary    ${dic}    ${key}    ${response.json()['data']['${key}']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}

[Inventory][Fail] - API create group
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API create group item   ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_BAD_REQUEST}
    set to dictionary    ${dic}
    ...    status_code   ${response.json()['status']['code']}
    ...    status_message   ${response.json()['status']['message']}
    ...    data    ${response.json()['data']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}
    
[Inventory][200] - API create ticket with serial and non-serial
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API create ticket with serial and non-serial    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_OK}
    should be equal as strings   ${response.json()['status']['code']}    ${inventory_success_code} 
    should be equal as strings   ${response.json()['status']['message']}    ${inventory_success_message}
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}

[Inventory][200] - API create ticket with serial
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API create ticket with serial    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_OK}
    should be equal as strings   ${response.json()['status']['code']}    ${inventory_success_code} 
    should be equal as strings   ${response.json()['status']['message']}    ${inventory_success_message}
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}
    
[Inventory][200] - API create ticket with non-serial
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API create ticket with non-serial    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_OK}
    should be equal as strings   ${response.json()['status']['code']}    ${inventory_success_code} 
    should be equal as strings   ${response.json()['status']['message']}    ${inventory_success_message}
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}    
    
[Inventory][200] - API get ticket
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API get ticket    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_OK}
    should be equal as strings   ${response.json()['status']['code']}    ${inventory_success_code} 
    should be equal as strings   ${response.json()['status']['message']}    ${inventory_success_message}
    :FOR    ${key}    IN    @{response.json()['data'].keys()}
    \    set to dictionary    ${dic}    ${key}    ${response.json()['data']['${key}']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}
    
[Inventory][200] - API update ticket status with owner
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API update ticket status with owner    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_OK}
    should be equal as strings   ${response.json()['status']['code']}    ${inventory_success_code} 
    should be equal as strings   ${response.json()['status']['message']}    ${inventory_success_message}   
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}
    
[Inventory][200] - API update ticket status
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API update ticket status    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_OK}
    should be equal as strings   ${response.json()['status']['code']}    ${inventory_success_code} 
    should be equal as strings   ${response.json()['status']['message']}    ${inventory_success_message}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}

[Inventory][Fail] - API update ticket status
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API update ticket status    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_BAD_REQUEST}
    set to dictionary    ${dic}    
    ...    status_code   ${response.json()['status']['code']}
    ...    status_message   ${response.json()['status']['message']}
    ...    data    ${response.json()['data']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}
       
[Inventory][Fail] - API update ticket status with owner
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API update ticket status with owner    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_BAD_REQUEST}
    set to dictionary    ${dic}    
    ...    status_code   ${response.json()['status']['code']}
    ...    status_message   ${response.json()['status']['message']}
    ...    data    ${response.json()['data']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}
    
[Inventory][200] - API update ticket with serial and non-serial
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API update ticket with serial and non-serial    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_OK}
    should be equal as strings   ${response.json()['status']['code']}       ${inventory_success_code}
    should be equal as strings   ${response.json()['status']['message']}    ${inventory_success_message}
    set to dictionary    ${dic}    id   ${response.json()['data']['id']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}

[Inventory][Fail] - API create ticket with serial and non-serial
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API create ticket with serial and non-serial    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_BAD_REQUEST}
    set to dictionary    ${dic}
    ...    status_code   ${response.json()['status']['code']}
    ...    status_message   ${response.json()['status']['message']}
    ...    data    ${response.json()['data']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}
    
[Inventory][Fail] - API update ticket with non-serial
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API update ticket with non-serial    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_BAD_REQUEST}
    set to dictionary    ${dic}    
    ...    status_code   ${response.json()['status']['code']}
    ...    status_message   ${response.json()['status']['message']}
    ...    data    ${response.json()['data']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}
    
[Inventory][Fail] - API update ticket with serial
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API update ticket with serial    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_BAD_REQUEST}
    set to dictionary    ${dic}    
    ...    status_code   ${response.json()['status']['code']}
    ...    status_message   ${response.json()['status']['message']}
    ...    data    ${response.json()['data']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}
    
[Inventory][Fail] - API update ticket without serial
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API update ticket without serial    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_BAD_REQUEST}
    set to dictionary    ${dic}    
    ...    status_code   ${response.json()['status']['code']}
    ...    status_message   ${response.json()['status']['message']}
    ...    data    ${response.json()['data']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}
    
[Inventory][Fail] - API update ticket without item
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API update ticket without item    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_BAD_REQUEST}
    set to dictionary    ${dic}
    ...    status_code   ${response.json()['status']['code']}
    ...    status_message   ${response.json()['status']['message']}
    ...    data    ${response.json()['data']}
    [Return]    ${dic}
    
[Inventory][Fail] - API create ticket with serial
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API create ticket with serial    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_BAD_REQUEST}
    set to dictionary    ${dic}    
    ...    status_code   ${response.json()['status']['code']}
    ...    status_message   ${response.json()['status']['message']}
    ...    data    ${response.json()['data']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}
    
[Inventory][Fail] - API create ticket with non-serial
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API create ticket with non-serial    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_BAD_REQUEST}
    set to dictionary    ${dic}    
    ...    status_code   ${response.json()['status']['code']}
    ...    status_message   ${response.json()['status']['message']}
    ...    data    ${response.json()['data']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}
    
[Inventory][Fail] - API create ticket without item
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API create ticket without item    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_BAD_REQUEST}
    set to dictionary    ${dic}    
    ...    status_code   ${response.json()['status']['code']}
    ...    status_message   ${response.json()['status']['message']}
    ...    data    ${response.json()['data']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic} 
       
[Inventory][Fail] - API get ticket
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API get ticket    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_BAD_REQUEST}
    set to dictionary    ${dic}    
    ...    status_code   ${response.json()['status']['code']}
    ...    status_message   ${response.json()['status']['message']}
    ...    data    ${response.json()['data']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}
    
[Inventory][200] - API get ticket notes
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API get ticket notes    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_OK}
    should be equal as strings   ${response.json()['status']['code']}    ${inventory_success_code} 
    should be equal as strings   ${response.json()['status']['message']}    ${inventory_success_message}
    set to dictionary    ${dic}    
    ...    notes   ${response.json()['data']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}
    
[Inventory][Fail] - API get ticket notes
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API get ticket notes    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_BAD_REQUEST}
    set to dictionary    ${dic}    
    ...    status_code   ${response.json()['status']['code']}
    ...    status_message   ${response.json()['status']['message']}
    ...    data    ${response.json()['data']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}

[Inventory][200] - API search items
    [Arguments]    ${arg_dic}
    [Inventory][Common] - Add client information    ${arg_dic}
    ${dic}    [Inventory] - API search items    ${arg_dic}
    ${response}    get from dictionary    ${dic}    response
    should be equal as integers     ${response.status_code}    ${http_status_OK}
    should be equal as strings   ${response.json()['status']['code']}    ${inventory_success_code}
    should be equal as strings   ${response.json()['status']['message']}    ${inventory_success_message}
    set to dictionary    ${dic}    data   ${response.json()['data']}
    [Inventory][Common] - Remove client information     ${arg_dic}
    [Return]    ${dic}