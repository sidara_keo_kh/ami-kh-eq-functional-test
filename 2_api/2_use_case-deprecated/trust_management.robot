*** Settings ***
Resource         ../1_raw-deprecated/imports.robot

*** Keywords ***
[200] API generate trust token
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    ${dic}     API generate trust token     ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary   ${dic}   token    ${response.json()['data'][0]['token']}
    [Return]    ${dic}

[200] API create trust order
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_name       robot_product_name
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_ref1       robot_product_ref1
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_ref2       robot_product_ref2
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_ref3       robot_product_ref3
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_ref4       robot_product_ref4
    [Common] - Set default value for keyword in dictionary    ${arg_dic}   product_ref5       robot_product_ref5
    ${dic}     API create trust order    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary    ${dic}     order_id     ${response.json()['data']['order_id']}
    [Return]    ${dic}

[200] API execute trust order
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    ${dic}     API execute trust order    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API delete trust token
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    ${dic}     API delete trust token    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API trusted agent can see their token list
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    ${dic}     API trusted agent can see their token list    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    set to dictionary   ${dic}   id    ${response.json()['data'][0]['id']}
    [Return]    ${dic}

[200] API get list trust order
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    ${dic}     API get list trust order    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API get list truster sof
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    ${dic}     API get list truster sof    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API get trust order detail
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    ${dic}     API get trust order detail    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[200] API get truster profile
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    ${dic}     API get truster profile    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as integers     ${response.status_code}     200
    [Return]    ${dic}

[Fail] API generate trust token
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    ${dic}     API generate trust token     ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}
    [Return]    ${dic}

[Fail] API delete trust token
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_id       ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary        ${arg_dic}       client_secret       ${setup_admin_client_secret}
    ${dic}     API delete trust token    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    should be equal as strings   ${arg_dic.code}            ${response.status_code}
    should be equal as strings   ${arg_dic.status_code}     ${response.json()['status']['code']}
    should be equal as strings   ${arg_dic.status_message}  ${response.json()['status']['message']}
    [Return]    ${dic}