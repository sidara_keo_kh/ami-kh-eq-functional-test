*** Settings ***
Resource        ../../1_common/keywords.robot


*** Keywords ***
API system user authentication
    [Arguments]     ${arg_dic}
    ${header}   create dictionary
    ...     content-type  application/json
    ...     client_id  ${arg_dic.client_id}
    ...     client_secret  ${arg_dic.client_secret}
    ${params}               catenate
        ...     {
        ...     "grant_type": "${arg_dic.grant_type}",
        ...     "userName": "${arg_dic.username}",
        ...     "password": "${arg_dic.password}"
        ...     }
    create session  api     ${api_gateway_host}     disable_warnings=1
#    create session  api        https://ami-channel-gateway-dev.dev.truemoney.com.mm
    ${response}             post request  api  ${system_user_authentication_path}  data=${params}  headers=${header}
#    ${response}             post request  api  /ami-channel-gateway/authentication/v1.0/systemuser/login  data=${params}  headers=${header}
    log     ${header}
    log     ${params}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create system user
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_middle_name}		[Common] - Create string param in dic   ${arg_dic}		 middle_name
    ${param_is_external}		[Common] - Create boolean param in dic  ${arg_dic}      is_external
    ${data}              catenate      SEPARATOR=
        ...    {
        ...    	"username":"${arg_dic.username}",
        ...    	"password":"${arg_dic.password}",
        ...    	"firstname":"${arg_dic.firstname}",
        ...    	"lastname":"${arg_dic.lastname}",
        ...    	"email":"${arg_dic.email}",
        ...     ${param_middle_name}
        ...     ${param_is_external}
        ...    }
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_system_user_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API add permissions to role
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${role_id}      convert to string    ${arg_dic.role_id}
    ${data}              catenate
        ...	{
        ...		"permissions":[${arg_dic.permissions}]
        ...	}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${add_permissions_to_role_path}     replace string      ${add_permissions_to_role_path}     {roleId}    ${role_id}
    ${response}             post request
        ...     api
        ...     ${add_permissions_to_role_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API create role
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${data}              catenate
        ...	{
        ...		"name":"${arg_dic.name}"
        ...	}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_role_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete system user
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${system_user_id}    Convert To String    ${arg_dic.system_user_id}
    ${delete_system_user_path}      replace string      ${delete_system_user_path}     {user_id}    ${system_user_id}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             delete request
        ...     api
        ...     ${delete_system_user_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    ${dic}	Create Dictionary   response=${response}
    log    ${response.text}
    [Return]    ${dic}

API get system user by access token
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_system_user_by_access_token_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update system user
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${user_id}      Convert To String       ${arg_dic.user_id}
    ${update_system_user_path}      replace string    ${update_system_user_path}    {id}   ${user_id}

    ${param_mobile_number}      [Common] - Create string param in dic     ${arg_dic}      mobile_number
    ${data}              catenate       SEPARATOR=
        ...    {
        ...    	"username":"${arg_dic.username}",
        ...    	"password":"${arg_dic.password}",
        ...    	"firstname":"${arg_dic.firstname}",
        ...    	"lastname":"${arg_dic.lastname}",
        ...    	"email":"${arg_dic.email}",
        ...    	${param_mobile_number}
        ...    }
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${update_system_user_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update system-user password
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${user_id}      Convert To String       ${arg_dic.user_id}
    ${update_system_user_password_path}      replace string    ${update_system_-_user_password_path}    {userId}  ${user_id}

    ${data}              catenate
        ...    {
        ...    	"password":"${arg_dic.password}"
        ...    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${update_system_user_password_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API Change system-user password
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${data}              catenate       SEPARATOR=
        ...    {
        ...    	"old_password":"${arg_dic.old_password}",
        ...    	"new_password":"${arg_dic.new_password}"
        ...    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${change_system_-_user_password_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get role and permission
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
         ...     api
         ...     ${get_role_and_permission_path}
         ...     ${header}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create permission
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_description}    [Common] - Create string param in dic     ${arg_dic}      description

    ${data}              catenate   SEPARATOR=
        ...    {
        ...    	"name":"${arg_dic.name}",
        ...    	"is_page_level":"${arg_dic.is_page_level}",
        ...     ${param_description}
        ...    }
     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_permission_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update permission
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${permission_id}    Convert To String    ${arg_dic.permission_id}
    ${update_permission_path}       replace string      ${update_permission_path}     {permissionId}    ${permission_id}

    ${param_description}    [Common] - Create string param in dic     ${arg_dic}      description
    ${data}              catenate       SEPARATOR=
        ...    {
        ...    	"name":"${arg_dic.name}",
        ...    	"is_page_level":"${arg_dic.is_page_level}",
        ...     ${param_description}
        ...    }
     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${update_permission_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete permission
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${permission_id}    Convert To String    ${arg_dic.permission_id}
    ${delete_permission_path}       replace string      ${delete_permission_path}     {permissionId}    ${permission_id}

                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             delete request
        ...     api
        ...     ${delete_permission_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API remove permissions from role
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${role_id}      Convert To String   ${arg_dic.role_id}
    ${remove_permissions_from_role_path}     replace string      ${remove_permissions_from_role_path}     {roleId}      ${role_id}

    ${data}              catenate
        ...    {
        ...    	"permissions":[${arg_dic.permissions}]
        ...    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             delete request
        ...     api
        ...     ${remove_permissions_from_role_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update role
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${role_id}    Convert To String         ${arg_dic.role_id}
    ${update_role_path}       replace string      ${update_role_path}     {roleId}    ${role_id}

    ${param_description}    [Common] - Create string param in dic     ${arg_dic}      description
    ${param_name}    [Common] - Create string param in dic     ${arg_dic}   name
    ${data}              catenate       SEPARATOR=
        ...    {
        ...    	${param_name}
        ...     ${param_description}
        ...    }
     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${update_role_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete role
    [Arguments]      &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${role_id}    Convert To String         ${arg_dic.role_id}
    ${delete_role_path}       replace string      ${delete_role_path}     {roleId}    ${role_id}

                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             delete request
        ...     api
        ...     ${delete_role_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API add user to role
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${role_id}      Convert To String   ${arg_dic.role_id}
    ${add_user_to_role_path}     replace string      ${add_user_to_role_path}     {roleId}      ${role_id}

    ${data}              catenate
        ...    {
        ...    	"user_id":"${arg_dic.user_id}"
        ...    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${add_user_to_role_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API unlink role from user
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${user_id}      Convert To String   ${arg_dic.user_id}
    ${unlink_role_from_user_path}     replace string      ${unlink_role_from_user_path}     {userId}      ${user_id}

    ${data}              catenate
        ...    {
        ...    	"role_id":"${arg_dic.role_id}"
        ...    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             delete request
        ...     api
        ...     ${unlink_role_from_user_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API system user create link system user and company profile
    [Arguments]     ${arg_dic}
    ${header}     create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    run keyword if   '${arg_dic.header_device_id}'!='${param_not_used}'             set to dictionary       ${header}       device_id               ${arg_dic.header_device_id}
    run keyword if   '${arg_dic.header_device_description}'!='${param_not_used}'    set to dictionary       ${header}       device_description      ${arg_dic.header_device_description}
    run keyword if   '${arg_dic.header_device_imei}'!='${param_not_used}'           set to dictionary       ${header}       device_imei             ${arg_dic.header_device_imei}
    run keyword if   '${arg_dic.header_device_ip4}'!='${param_not_used}'            set to dictionary       ${header}       device_ip4              ${arg_dic.header_device_ip4}

    ${param_company_profile_id}		[Common] - Create string param		company_id		${arg_dic.company_profile_id}
    ${user_id}    convert to string   ${arg_dic.user_id}
    ${link_system_user_company_profile_path}     replace string      ${link_system_user_company_profile_path}     {userId}      ${user_id}

    ${data}              catenate   SEPARATOR=
    ...          {
    ...                 ${param_company_profile_id}
    ...          }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
    create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${link_system_user_company_profile_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${header}
    log        ${data}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API system user create system user
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    run keyword if   '${arg_dic.header_device_id}'!='${param_not_used}'              set to dictionary       ${header}       device_id                ${arg_dic.header_device_id}
    run keyword if   '${arg_dic.header_device_description}'!='${param_not_used}'     set to dictionary       ${header}       device_description       ${arg_dic.header_device_description}
    run keyword if   '${arg_dic.header_device_imei}'!='${param_not_used}'            set to dictionary       ${header}       device_imei              ${arg_dic.header_device_imei}
    run keyword if   '${arg_dic.header_device_ip4}'!='${param_not_used}'             set to dictionary       ${header}       device_ip4               ${arg_dic.header_device_ip4}

    ${param_title}		        [Common] - Create string param		title		    ${arg_dic.title}
    ${param_first_name}		    [Common] - Create string param		firstname		${arg_dic.first_name}
    ${param_middle_name}		[Common] - Create string param		middle_name		${arg_dic.middle_name}
    ${param_last_name}		    [Common] - Create string param		lastname		${arg_dic.last_name}
    ${param_suffix}		        [Common] - Create string param		suffix		    ${arg_dic.suffix}
    ${param_date_of_birth}		[Common] - Create string param		date_of_birth		${arg_dic.date_of_birth}
    ${param_place_of_birth}		[Common] - Create string param		place_of_birth		${arg_dic.place_of_birth}
    ${param_nationality}		[Common] - Create string param		nationality		    ${arg_dic.nationality}
    ${param_occupation}		    [Common] - Create string param		occupation		    ${arg_dic.occupation}
    ${param_occupation_title}		[Common] - Create string param		occupation_title		${arg_dic.occupation_title}
    ${param_email}		            [Common] - Create string param		email		            ${arg_dic.email}
    ${param_mobile_number}		    [Common] - Create string param		mobile_number		    ${arg_dic.mobile_number}
    ${param_current_address_citizen_association}		    [Common] - Create string param		citizen_association		        ${arg_dic.current_address_citizen_association}
    ${param_current_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${arg_dic.current_address_neighbourhood_association}
    ${param_current_address_address}		[Common] - Create string param		address		    ${arg_dic.current_address_address}
    ${param_current_address_commune}		[Common] - Create string param		commune		    ${arg_dic.current_address_commune}
    ${param_current_address_district}		[Common] - Create string param		district        ${arg_dic.current_address_district}
    ${param_current_address_city}		    [Common] - Create string param		city		    ${arg_dic.current_address_city}
    ${param_current_address_province}		[Common] - Create string param		province    	${arg_dic.current_address_province}
    ${param_current_address_postal_code}    [Common] - Create string param		postal_code		${arg_dic.current_address_postal_code}
    ${param_current_address_country}		[Common] - Create string param		country		    ${arg_dic.current_address_country}
    ${param_current_address_landmark}		[Common] - Create string param		landmark		${arg_dic.current_address_landmark}
    ${param_current_address_longitude}		[Common] - Create string param		longitude		${arg_dic.current_address_longitude}
    ${param_current_address_latitude}		[Common] - Create string param		latitude		${arg_dic.current_address_latitude}
    ${param_permanent_address_citizen_association}		        [Common] - Create string param		citizen_association		        ${arg_dic.permanent_address_citizen_association}
    ${param_permanent_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${arg_dic.permanent_address_neighbourhood_association}
    ${param_permanent_address_address}		[Common] - Create string param		address		    ${arg_dic.permanent_address_address}
    ${param_permanent_address_commune}		[Common] - Create string param		commune		    ${arg_dic.permanent_address_commune}
    ${param_permanent_address_district}		[Common] - Create string param		district		${arg_dic.permanent_address_district}
    ${param_permanent_address_city}		    [Common] - Create string param		city		    ${arg_dic.permanent_address_city}
    ${param_permanent_address_province}		[Common] - Create string param		province		${arg_dic.permanent_address_province}
    ${param_permanent_address_postal_code}	[Common] - Create string param		postal_code		${arg_dic.permanent_address_postal_code}
    ${param_permanent_address_country}		[Common] - Create string param		country		    ${arg_dic.permanent_address_country}
    ${param_permanent_address_landmark}		[Common] - Create string param		landmark		${arg_dic.permanent_address_landmark}
    ${param_permanent_address_longitude}	[Common] - Create string param		longitude		${arg_dic.permanent_address_longitude}
    ${param_permanent_address_latitude}		[Common] - Create string param		latitude		${arg_dic.permanent_address_latitude}
    ${param_additional_field_1_name}		[Common] - Create string param		field_1_name		${arg_dic.additional_field_1_name}
    ${param_additional_field_1_value}		[Common] - Create string param		field_1_value		${arg_dic.additional_field_1_value}
    ${param_additional_field_2_name}		[Common] - Create string param		field_2_name		${arg_dic.additional_field_2_name}
    ${param_additional_field_2_value}		[Common] - Create string param		field_2_value		${arg_dic.additional_field_2_value}
    ${param_additional_field_3_name}		[Common] - Create string param		field_3_name		${arg_dic.additional_field_3_name}
    ${param_additional_field_3_value}		[Common] - Create string param		field_3_value		${arg_dic.additional_field_3_value}
    ${param_additional_field_4_name}		[Common] - Create string param		field_4_name		${arg_dic.additional_field_4_name}
    ${param_additional_field_4_value}		[Common] - Create string param		field_4_value		${arg_dic.additional_field_4_value}
    ${param_additional_field_5_name}		[Common] - Create string param		field_5_name		${arg_dic.additional_field_5_name}
    ${param_additional_field_5_value}		[Common] - Create string param		field_5_value		${arg_dic.additional_field_5_value}
    ${param_additional_supporting_file_1_url}		[Common] - Create string param		supporting_file_1_url		${arg_dic.additional_supporting_file_1_url}
    ${param_additional_supporting_file_2_url}		[Common] - Create string param		supporting_file_2_url		${arg_dic.additional_supporting_file_2_url}
    ${param_additional_supporting_file_3_url}		[Common] - Create string param		supporting_file_3_url		${arg_dic.additional_supporting_file_3_url}
    ${param_additional_supporting_file_4_url}		[Common] - Create string param		supporting_file_4_url		${arg_dic.additional_supporting_file_4_url}
    ${param_additional_supporting_file_5_url}		[Common] - Create string param		supporting_file_5_url		${arg_dic.additional_supporting_file_5_url}
    ${param_source_of_funds}	    [Common] - Create string param		source_of_funds		    ${arg_dic.source_of_funds}
    ${param_national_id_number}	    [Common] - Create string param		national_id_number		${arg_dic.national_id_number}
    ${param_unique_reference}	    [Common] - Create string param		unique_reference		${arg_dic.unique_reference}
    ${param_is_external}	        [Common] - Create boolean param		is_external		        ${arg_dic.is_external}
    ${param_identity_username}		[Common] - Create string param		username		        ${arg_dic.username}
    ${param_identity_password}		[Common] - Create string param		password		        ${arg_dic.password}

    ${data}              catenate   SEPARATOR=
    ...          {
    ...                 ${param_identity_username}
    ...                 ${param_identity_password}
    ...                 ${param_email}
    ...                 ${param_mobile_number}
    ...                 ${param_is_external}
    ...                 ${param_unique_reference}
    ...                 ${param_title}
    ...                 ${param_first_name}
    ...                 ${param_middle_name}
    ...                 ${param_last_name}
    ...                 ${param_suffix}
    ...                 ${param_date_of_birth}
    ...                 ${param_place_of_birth}
    ...                 ${param_nationality}
    ...                 ${param_occupation}
    ...                 ${param_occupation_title}
    ...                 ${param_source_of_funds}
    ...                 ${param_national_id_number}
    ...                 "address": {
    ...                     "current_address": {
    ...      					${param_current_address_citizen_association}
    ...      					${param_current_address_neighbourhood_association}
    ...      					${param_current_address_address}
    ...      					${param_current_address_commune}
    ...      					${param_current_address_district}
    ...      					${param_current_address_city}
    ...      					${param_current_address_province}
    ...      					${param_current_address_postal_code}
    ...      					${param_current_address_country}
    ...      					${param_current_address_landmark}
    ...      					${param_current_address_longitude}
    ...      					${param_current_address_latitude}
    ...                     },
    ...                     "permanent_address": {
    ...      					${param_permanent_address_citizen_association}
    ...      					${param_permanent_address_neighbourhood_association}
    ...      					${param_permanent_address_address}
    ...      					${param_permanent_address_commune}
    ...      					${param_permanent_address_district}
    ...      					${param_permanent_address_city}
    ...      					${param_permanent_address_province}
    ...      					${param_permanent_address_postal_code}
    ...      					${param_permanent_address_country}
    ...      					${param_permanent_address_landmark}
    ...      					${param_permanent_address_longitude}
    ...      					${param_permanent_address_latitude}
    ...                     }
    ...                 },
    ...                 "additional": {
    ...      				${param_additional_field_1_name}
    ...      				${param_additional_field_1_value}
    ...      				${param_additional_field_2_name}
    ...      				${param_additional_field_2_value}
    ...      				${param_additional_field_3_name}
    ...      				${param_additional_field_3_value}
    ...      				${param_additional_field_4_name}
    ...      				${param_additional_field_4_value}
    ...      				${param_additional_field_5_name}
    ...      				${param_additional_field_5_value}
    ...      				${param_additional_supporting_file_1_url}
    ...      				${param_additional_supporting_file_2_url}
    ...      				${param_additional_supporting_file_3_url}
    ...      				${param_additional_supporting_file_4_url}
    ...      				${param_additional_supporting_file_5_url}
    ...                 }
    ...          }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
    create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_system_user_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${header}
    log        ${data}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API active suspend system user
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${system_user_id}      Convert To String       ${arg_dic.system_user_id}
    ${system_user_active_suspend_system_user_path}      replace string    ${system_user_active_suspend_system_user_path}    {system_user_id}   ${system_user_id}
    ${param_suspend_reason}      [Common] - Create string param in dic     ${arg_dic}      suspend_reason
    ${data}              catenate       SEPARATOR=
        ...    {
        ...    	"is_suspended":"${arg_dic.is_suspended}"
        ...    	${param_suspend_reason}
        ...    }
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${system_user_active_suspend_system_user_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}