*** Settings ***
Resource        ../../1_common/keywords.robot


*** Keywords ***
API get list voucher groups
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_list_voucher_groups_path}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API update voucher status
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate
        ...   {
        ...     "is_on_hold":${arg_dic.is_on_hold}
        ...	  }
    ${id}       convert to string     ${arg_dic.id}
    ${update_voucher_status_path}=    replace string        ${update_voucher_status_path}       {id}         ${id}
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${update_voucher_status_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create multiple discount vouchers
    [Arguments]  ${arg_dic}
    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...       "prefix": "${arg_dic.prefix}",
        ...       "number_of_digit": ${arg_dic.number_of_digit},
        ...       "discount_type": "${arg_dic.discount_type}",
        ...       "discount_amount": ${arg_dic.discount_amount},
        ...       "currency": "${arg_dic.currency}",
        ...       "payer_user": {
        ...         "id": ${arg_dic.payer_user_id},
        ...         "type": "${arg_dic.payer_user_type}",
        ...         "sof_id": ${arg_dic.payer_user_sof_id},
        ...         "sof_type_id": ${arg_dic.payer_user_sof_type_id}
        ...       },
        ...       "applied_to": "${arg_dic.applied_to}",
        ...       "min_amount": ${arg_dic.min_amount},
        ...       "max_discount": ${arg_dic.max_discount},
        ...       "permitted_service_group_id": "${arg_dic.permitted_service_group_id}",
        ...       "permitted_service_id": "${arg_dic.permitted_service_id}",
        ...       "voucher_group": "${arg_dic.voucher_group}",
        ...       "number_of_needed_voucher": ${arg_dic.number_of_needed_voucher}
        ...     }
    ${data}  replace string      ${data}      ,}    }
                create session          api        ${api_gateway_host}    disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_multiple_discount_vouchers_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create multiple-use vouchers
    [Arguments]  ${arg_dic}
    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_expiration_date}        [Common] - Create string param in dic      ${arg_dic}       expiration_date
    ${data}              catenate       SEPARATOR=
        ...     {
        ...       "voucher_code": "${arg_dic.voucher_code}",
        ...       "discount_type": "${arg_dic.discount_type}",
        ...       "discount_amount": ${arg_dic.discount_amount},
        ...       "currency": "${arg_dic.currency}",
        ...       "payer_user": {
        ...         "id": ${arg_dic.payer_user_id},
        ...         "type": "${arg_dic.payer_user_type}",
        ...         "sof_id": ${arg_dic.payer_user_sof_id},
        ...         "sof_type_id": ${arg_dic.payer_user_sof_type_id}
        ...       },
        ...       "applied_to": "${arg_dic.applied_to}",
        ...       "min_amount": ${arg_dic.min_amount},
        ...       "max_discount": ${arg_dic.max_discount},
        ...       "permitted_service_group_id": "${arg_dic.permitted_service_group_id}",
        ...       "permitted_service_id": "${arg_dic.permitted_service_id}",
        ...       "max_per_user": ${arg_dic.max_per_user},
        ...       ${param_expiration_date}
        ...     }
    ${data}  replace string      ${data}      ,}    }
                create session          api        ${api_gateway_host}    disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_multiple-use_vouchers_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}