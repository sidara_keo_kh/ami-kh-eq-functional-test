*** Settings ***
Resource        ../../1_common/keywords.robot


*** Keywords ***
API create prepaid card
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...     	"firstname":"${arg_dic.firstname}",
        ...     	"lastname":"${arg_dic.lastname}",
        ...     	"card_type_id":${arg_dic.card_type_id},
        ...     	"card_lifetime_in_month":${arg_dic.card_lifetime_in_month}
        ...     }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_virtual_prepaid_card_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${data}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create virtual prepaid card
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
     ${param_firstname}=   [Common] - Create string param        firstname        ${arg_dic.firstname}
     ${param_lastname}=   [Common] - Create string param        lastname        ${arg_dic.lastname}
     ${param_card_type_id}=   [Common] - Create int param        card_type_id        ${arg_dic.card_type_id}
     ${param_card_lifetime_in_month}=   [Common] - Create int param        card_lifetime_in_month        ${arg_dic.card_lifetime_in_month}

     ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_firstname}
        ...         	 ${param_lastname}
        ...         	 ${param_card_type_id}
        ...         	 ${param_card_lifetime_in_month}

        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },

                            create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             post request
         ...     api
         ...    ${prepaid_card_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create payroll card
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...         "card_identifier":"${arg_dic.card_identifier}",
        ...     	"card_type_id":${arg_dic.card_type_id},
        ...     	"card_lifetime_in_month":${arg_dic.card_lifetime_in_month},
        ...         "user": {
        ...             "type":"${arg_dic.user_type}",
        ...             "id":${arg_dic.user_id}
        ...         }
        ...     }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_payroll_card_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${data}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get full detail prepaid card
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
     ${card_id}      convert to string     ${arg_dic.card_id}
     ${get_full_detail_prepaid_card_path}          replace string          ${get_full_detail_prepaid_card_path}       {card_id}       ${card_id}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_full_detail_prepaid_card_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API get prepaid card list
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${prepaid_card_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API stop prepaid card
    [Arguments]         ${arg_dic}

    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${card_id}      convert to string     ${arg_dic.card_id}
    ${stop_prepaid_card_path}          replace string          ${stop_prepaid_card_path}       {card_id}       ${card_id}
    ${param_is_stopped}=   [Common] - Create boolean param        is_stopped        ${arg_dic.is_stopped}

    ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_is_stopped}
        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${stop_prepaid_card_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log         ${data}
    log         ${response.text}
    ${return_dic} =	Create Dictionary   response=${response}
    [Return]    ${return_dic}

API admin stop prepaid card
    [Arguments]         ${arg_dic}

    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${card_id}      convert to string     ${arg_dic.card_id}
    ${admin_stop_prepaid_card_path}          replace string          ${admin_stop_prepaid_card_path}       {card_id}       ${card_id}
    ${param_is_stopped}=   [Common] - Create boolean param        is_stopped        ${arg_dic.is_stopped}

    ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_is_stopped}
        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${admin_stop_prepaid_card_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    ${return_dic} =	Create Dictionary   response=${response}
    [Return]    ${return_dic}


API update card type
    [Arguments]         ${arg_dic}

    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${card_type_id}      convert to string     ${arg_dic.card_type_id}
    ${update_card_type_path}          replace string          ${update_card_type_path}       {card_type_id}       ${card_type_id}
    ${param_name}=   [Common] - Create string param        name        ${arg_dic.name}
    ${param_timeout_create_card}=   [Common] - Create int param        timeout_create_card        ${arg_dic.timeout_create_card}
    ${param_timeout_get_card_detail}=   [Common] - Create int param        timeout_get_card_detail        ${arg_dic.timeout_get_card_detail}
    ${param_create_card_endpoint_host}=   [Common] - Create string param        create_card_endpoint_host        ${arg_dic.create_card_endpoint_host}
    ${param_card_detail_endpoint_host}=   [Common] - Create string param        card_detail_endpoint_host        ${arg_dic.card_detail_endpoint_host}
    ${param_timeout_update_card_status}=   [Common] - Create int param        timeout_update_card_status        ${arg_dic.timeout_update_card_status}
    ${param_update_card_status_endpoint_host}=   [Common] - Create string param        update_card_status_endpoint_host        ${arg_dic.update_card_status_endpoint_host}

    ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_name}
        ...         	 ${param_timeout_create_card}
        ...         	 ${param_timeout_get_card_detail}
        ...         	 ${param_create_card_endpoint_host}
        ...         	 ${param_card_detail_endpoint_host}
        ...         	 ${param_timeout_update_card_status}
        ...         	 ${param_update_card_status_endpoint_host}

        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${update_card_type_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    ${return_dic} =	Create Dictionary   response=${response}
    [Return]    ${return_dic}
