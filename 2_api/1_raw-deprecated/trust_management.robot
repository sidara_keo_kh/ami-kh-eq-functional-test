*** Settings ***
Resource        ../../1_common/keywords.robot


*** Keywords ***
API generate trust token
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...     {
        ...        	"trusts":	[{
        ...        		    "truster_user_id": ${arg_dic.truster_user_id},
        ...        		    "truster_user_type_id": ${arg_dic.truster_user_type_id},
        ...        		    "trusted_user_id": ${arg_dic.trusted_user_id},
        ...        		    "trusted_user_type_id": ${arg_dic.trusted_user_type_id},
        ...        		    "expired_unit": "${arg_dic.expired_unit}",
        ...        		    "expired_duration": ${arg_dic.expired_duration}
        ...        		}]
        ...        }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${generate_trust_token_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create trust order
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_requestor_user_id}         [Common] - Create string param of object in dic    ${arg_dic}    requestor_user        user_id
    ${param_requestor_user_type_name}  [Common] - Create string param of object in dic    ${arg_dic}    requestor_user        user_type
    ${param_requestor_sof_id}          [Common] - Create string param of object in dic    ${arg_dic}    requestor_user_sof    id
    ${param_requestor_sof_type_id}     [Common] - Create string param of object in dic    ${arg_dic}    requestor_user_sof    type_id

    ${data}              catenate           SEPARATOR=
    ...	    {
    ...	        "ext_transaction_id": "${arg_dic.ext_transaction_id}",
    ...	        "trust_token": "${arg_dic.trust_token}",
    ...	        "product_service": {
    ...	        "id": ${arg_dic.product_service_id},
    ...	        "name": "${arg_dic.product_service_name}"
    ...	    },
    ...     "product": {
    ...         "product_name": "${arg_dic.product_name}",
    ...         "product_ref1": "${arg_dic.product_ref1}",
    ...         "product_ref2": "${arg_dic.product_ref2}",
    ...         "product_ref3": "${arg_dic.product_ref3}",
    ...         "product_ref4": "${arg_dic.product_ref4}",
    ...         "product_ref5": "${arg_dic.product_ref5}"
    ...         },
    ...     "requestor_user":{
    ...         ${param_requestor_user_id}
    ...         ${param_requestor_user_type_name}
    ...         "sof":{
    ...             ${param_requestor_sof_id}
    ...             ${param_requestor_sof_type_id}
    ...         },
    ...     },
    ...	    "payer_user": {
    ...	        "user_id": ${arg_dic.payer_user_id},
    ...	        "user_type": "${arg_dic.payer_user_type_name}",
    ...	        "sof": {
    ...	        "id": ${arg_dic.payer_sof_id},
    ...	        "type_id": ${arg_dic.payer_sof_type_id}
    ...	                }
    ...	                },
    ...	    "payee_user": {
    ...	        "user_id": ${arg_dic.payee_user_id},
    ...	        "user_type": "${arg_dic.payee_user_type_name}",
    ...	        "sof": {
    ...	        "id": ${arg_dic.payee_sof_id},
    ...	        "type_id": ${arg_dic.payee_sof_type_id}
    ...	                }
    ...	                },
    ...	    "amount": ${arg_dic.amount}
    ...	    }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },

    create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_trus_order_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${return_dic} =	Create Dictionary   response=${response}
    log    ${data}
    log    ${response.text}
    [Return]    ${return_dic}

API execute trust order
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${order_id}        convert to string           ${arg_dic.order_id}
    ${execute_trust_order_path}          replace string          ${execute_trust_order_path}       {order_id}       ${order_id}

    ${data}              catenate           SEPARATOR=
        ...	    {
        ...	        "trust_token": "${arg_dic.trust_token}"
        ...	    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${execute_trust_order_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${return_dic} =	Create Dictionary   response=${response}
    log    ${response.text}
    [Return]    ${return_dic}

API delete trust token
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${id}        convert to string           ${arg_dic.id}
    ${delete_trust_token_path}          replace string          ${delete_trust_token_path}       {id}       ${id}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             delete request
        ...     api
        ...     ${delete_trust_token_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    ${return_dic} =	Create Dictionary   response=${response}
    log    ${response.text}
    [Return]    ${return_dic}

API trusted agent can see their token list
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${params}               catenate                SEPARATOR=&
        ...     role=${arg_dic.trust_role}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${trusted_agent_can_see_their_token_list_path}
        ...     ${header}
        ...     ${NONE}
        ...     ${params}
    ${return_dic} =	Create Dictionary   response=${response}
    log    ${response.text}
    [Return]    ${return_dic}

API get list trust order
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${trust_token}        convert to string           ${arg_dic.trust_token}
    ${get_list_trust_order_path}          replace string          ${get_list_trust_order_path}       {trust_token}       ${trust_token}

                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_list_trust_order_path}
        ...     ${header}
        ...     ${NONE}
        ...     ${NONE}
    ${return_dic} =	Create Dictionary   response=${response}
    log    ${response.text}
    [Return]    ${return_dic}

API get list truster sof
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${trust_token}        convert to string           ${arg_dic.trust_token}
    ${get_list_truster_sof_path}          replace string          ${get_list_truster_sof_path}       {trust_token}       ${trust_token}

                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_list_truster_sof_path}
        ...     ${header}
        ...     ${NONE}
        ...     ${NONE}
    ${return_dic} =	Create Dictionary   response=${response}
    log    ${response.text}
    [Return]    ${return_dic}

API get trust order detail
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${trust_token}        convert to string           ${arg_dic.trust_token}
    ${order_id}        convert to string           ${arg_dic.order_id}
    ${get_trust_order_detail_path}          replace string          ${get_trust_order_detail_path}       {trust_token}       ${trust_token}
    ${get_trust_order_detail_path}          replace string          ${get_trust_order_detail_path}       {order_id}       ${order_id}

                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_trust_order_detail_path}
        ...     ${header}
        ...     ${NONE}
        ...     ${NONE}
    ${return_dic} =	Create Dictionary   response=${response}
    log    ${response.text}
    [Return]    ${return_dic}

API get truster profile
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${trust_token}        convert to string           ${arg_dic.trust_token}
    ${get_truster_profile_path}          replace string          ${get_truster_profile_path}       {trust_token}       ${trust_token}

                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_truster_profile_path}
        ...     ${header}
        ...     ${NONE}
        ...     ${NONE}
    ${return_dic} =	Create Dictionary   response=${response}
    log    ${response.text}
    [Return]    ${return_dic}