*** Settings ***
Resource        ../../1_common/keywords.robot


*** Keywords ***
API get list shop
    [Arguments]
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${create_agent_shop_channel_adapter_api_path}
        ...     ${header}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API create order configuration api
    [Arguments]
    ...     ${service_group_id}
    ...     ${template_id}
    ...     ${author}
    ...     ${status}
    ...     ${agent_access_token}

    ${header}               create dictionary
        ...     client_id=${setup_admin_client_id}
        ...     client_secret=${setup_admin_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${agent_access_token}

    ${data}              catenate
        ...	{
        ...		"service_group_id": ${service_group_id},
        ...		"template_id": ${template_id},
        ...		"author": "${author}",
        ...		"status": "${status}"
        ...	}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_order_config_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
                            should be equal as integers     ${response.status_code}         200
    [Return]    ${response}


API create order configuration
    [Arguments]     ${arg_dic}
    ${service_group_id}     get from dictionary     ${arg_dic}      service_group_id
    ${template_id}      get from dictionary     ${arg_dic}      template_id
    ${author}           get from dictionary     ${arg_dic}      author
    ${status}           get from dictionary     ${arg_dic}      status
    ${access_token}     get from dictionary     ${arg_dic}      access_token

    ${header}               create dictionary
        ...     client_id=${setup_admin_client_id}
        ...     client_secret=${setup_admin_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}

    ${data}              catenate
        ...	{
        ...		"service_group_id": ${service_group_id},
        ...		"template_id": ${template_id},
        ...		"author": "${author}",
        ...		"status": "${status}"
        ...	}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_order_config_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    should be equal as integers     ${response.status_code}         200
    [Return]    ${response}

API update order configuration api
    [Arguments]
    ...     ${order_config_id}
    ...     ${author}
    ...     ${status}
    ...     ${service_group_id}
    ...     ${agent_access_token}
    ...     ${list_configs_data}
    ${order_config_id}     convert to string  ${order_config_id}
    ${path}          replace string       ${update_order_config_path}       {order_config_id}       ${order_config_id}
    ${header}               create dictionary
        ...     client_id=${setup_admin_client_id}
        ...     client_secret=${setup_admin_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${agent_access_token}

    ${data}              catenate       SEPARATOR=
        ...	{
        ...     "list_configs":     [${list_configs_data}],
        ...		"services": [${service_group_id}],
        ...		"author": "${author}",
        ...		"status": "${status}"
        ...	}
    ${data}  replace string      ${data}      ,]    ]
    ${data}  replace string      ${data}      },,    },
    ${data}  replace string      ${data}        u'{     {
    ${data}  replace string      ${data}        [[     [
    ${data}  replace string      ${data}        ]]     ]
    ${data}  replace string      ${data}        ,',     ,
    ${data}  replace string      ${data}        }']     }]
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
        log         ${data}
              log         ${response.text}
                            should be equal as integers     ${response.status_code}         200

    [Return]    ${response}


API search order configuration by service group id and template_id
        [Arguments]     ${arg_dic}
        ${correlation_id}       generate random string  20  [UPPER]
        ${access_token}     get from dictionary     ${arg_dic}      access_token
        ${header}               create dictionary
            ...     client_id=${setup_admin_client_id}
            ...     client_secret=${setup_admin_client_secret}
            ...     content-type=application/json
            ...     Authorization=Bearer ${access_token}

        #${path}=   replace string      ${search_order_config_by_service_group_id_path}   *       ${test_service_group_id}    1
        ${template_id}      get from dictionary     ${arg_dic}      template_id
        ${service_group_id}         get from dictionary     ${arg_dic}      service_group_id
        ${service_group_id}     convert to string   ${service_group_id}
        ${search_order_config_by_service_group_id_path}          replace string          ${search_order_config_by_service_group_id_path}       {service_group_id}      ${service_group_id}
        ${search_order_config_by_service_group_id_path}          replace string          ${search_order_config_by_service_group_id_path}       {template_id}      ${template_id}
                                create session          api     ${api_gateway_host}     disable_warnings=1
        ${response}             get request
                ...     api
                ...     ${search_order_config_by_service_group_id_path}
                ...     ${header}
        log         ${response.text}
        ${dic} =	Create Dictionary   response=${response}
        [Return]    ${dic}

API agent bind device
    [Arguments]
    ...     ${shop_id}
    ...     ${mac_address}
    ...     ${network_provider_name}
    ...     ${public_ip_address}
    ...     ${supporting_file_1}
    ...     ${supporting_file_2}
    ...     ${device_name}
    ...     ${device_model}
    ...     ${device_unique_reference}
    ...     ${os}
    ...     ${os_version}
    ...     ${display_size_in_inches}
    ...     ${pixel_counts}
    ...     ${unique_number}
    ...     ${serial_number}
    ...     ${app_version}
    ...     ${otp_reference}
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${param_shop_id}		[Common] - Create int param		shop_id		${shop_id}
    ${param_mac_address}		[Common] - Create string param		mac_address		${mac_address}
    ${param_network_provider_name}		[Common] - Create string param		network_provider_name		${network_provider_name}
    ${param_public_ip_address}		[Common] - Create string param		public_ip_address		${public_ip_address}
    ${param_supporting_file_1}		[Common] - Create string param		supporting_file_1		${supporting_file_1}
    ${param_supporting_file_2}		[Common] - Create string param		supporting_file_2		${supporting_file_2}
    ${param_device_name}		[Common] - Create string param		device_name		${device_name}
    ${param_device_model}		[Common] - Create string param		device_model		${device_model}
    ${param_device_unique_reference}		[Common] - Create string param		device_unique_reference		${device_unique_reference}
    ${param_os}		[Common] - Create string param		os		${os}
    ${param_os_version}		[Common] - Create string param		os_version		${os_version}
    ${param_display_size_in_inches}		[Common] - Create string param		display_size_in_inches		${display_size_in_inches}
    ${param_pixel_counts}		[Common] - Create string param		pixel_counts		${pixel_counts}
    ${param_unique_number}		[Common] - Create string param		unique_number		${unique_number}
    ${param_serial_number}		[Common] - Create string param		serial_number		${serial_number}
    ${param_app_version}		[Common] - Create string param		app_version		${app_version}
    ${param_otp_reference}		[Common] - Create string param		otp_reference		${otp_reference}
    ${data}              catenate       SEPARATOR=
    ...    {
    ...            ${param_shop_id}
    ...            ${param_mac_address}
    ...            ${param_network_provider_name}
    ...            ${param_public_ip_address}
    ...            ${param_supporting_file_1}
    ...            ${param_supporting_file_2}
    ...            ${param_device_name}
    ...            ${param_device_model}
    ...            ${param_device_unique_reference}
    ...            ${param_os}
    ...            ${param_os_version}
    ...            ${param_display_size_in_inches}
    ...            ${param_pixel_counts}
    ...            ${param_unique_number}
    ...            ${param_serial_number}
    ...            ${param_otp_reference}
    ...    }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },    }
                            create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${agent_bind_device_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${response_text}=   set variable    ${response.text}
    log     ${data}
    log     ${response_text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API agent list binded devices
     [Arguments]
        ...     ${access_token}
        ...     ${header_client_id}
        ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
                            create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_binded_devices_path}
        ...     ${header}

    log         ${response.text}
    ${response_text}=   set variable    ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API agent unbind device
    [Arguments]
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ...     ${unbind_device_id}
    ...     ${bind_device_id}
    ...     ${otp_reference_id}
    ${header}           create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
        ...     device_unique_reference=${bind_device_id}
    ${unbind_device_id}     convert to string  ${unbind_device_id}
    ${unbind_device_channel_adapter_api_path}          replace string          ${unbind_device_channel_adapter_api_path}       {deviceID}      ${unbind_device_id}
    ${unbind_device_channel_adapter_api_path}          replace string          ${unbind_device_channel_adapter_api_path}       {otpReferenceID}          ${otp_reference_id}
    create session          api                                     ${channel_gateway_host}             disable_warnings=1
    ${response}             delete request
        ...     api
        ...     ${unbind_device_channel_adapter_api_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log         ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

Revoke access token when login new device
    [Arguments]
    ...     ${test_agent_access_token}
    ...     ${setup_channel_adapter_client_id}
    ...     ${setup_channel_adapter_client_secret}
    ${header}           create dictionary
    ...     client_id=${setup_channel_adapter_client_id}
    ...     client_secret=${setup_channel_adapter_client_secret}
    ...     content-type=application/json
    ...     Authorization=Bearer ${test_agent_access_token}
    log     ${header}
    create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             get request
    ...     api
    ...     ${agent_logout}
    ...     ${header}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API agent login via channel adapter
    [Arguments]
    ...     ${username}
    ...     ${password}
    ...     ${login_type}
    ...     ${grant_type}
    ...     ${device_id}
    ...     ${refresh_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ...     ${channel_id}
    ...     ${device_type}
    log     ${device_id}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     device_unique_reference=${device_id}
        ...     channel_id=${channel_id}
        ...     device_type=${device_type}
    ${param_username}		[Common] - Create string param		username		${username}
    ${param_password}		[Common] - Create string param		password		${password}
    ${param_login_type}		[Common] - Create string param		login_type		${login_type}
    ${param_grant_type}		[Common] - Create string param		grant_type		${grant_type}
    ${param_refresh_token}		[Common] - Create string param		refresh_token		${refresh_token}
    ${data}              catenate       SEPARATOR=
        ...	{
        ...		${param_username}
        ...		${param_password}
        ...		${param_login_type}
        ...		${param_grant_type}
        ...		${param_refresh_token}
        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },    }
    log     ${data}
                            create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${agent_login}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get mobile configuration
    [Arguments]
    ...     ${header_client_id}
    ...     ${header_client_secret}

    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json


    create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_mobile_configuration}
        ...     ${header}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API channel gateway - agent get basic profile
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     Authorization=Bearer ${arg_dic.access_token}
        ...     content-type=application/json
    ${data}              catenate       SEPARATOR=
    ...    {
    ...           "username":"${arg_dic.username}"
    ...    }
                            create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${get_agent_basic_profile}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API agent logout via channel adapter
    [Arguments]
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ...     ${login_device_id}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
        ...     device_unique_reference=${login_device_id}
                            create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${agent_logout}
        ...     ${header}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get version of application
    [Arguments]    ${arg_dic}
    ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${app_id}        convert to string           ${arg_dic.app_id}
    ${os}        convert to string           ${arg_dic.os}
    ${version}        convert to string           ${arg_dic.version}
    ${check_version_path}          replace string          ${check_version_path}       {app_id}       ${app_id}
    ${check_version_path}          replace string          ${check_version_path}       {os}           ${os}
    ${check_version_path}          replace string          ${check_version_path}       {version}      ${version}
                 create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
         ...     api
         ...     ${check_version_path}
         ...     ${header}
    log          ${response.text}
    ${dic} =	 Create Dictionary   response=${response}
    [Return]     ${dic}

API get mobile application configurations
    [Arguments]    ${arg_dic}
    ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${os}        convert to string           ${arg_dic.os}
    ${get_mobile_application_configurations_path}          replace string          ${get_mobile_application_configurations_path}       {os}       ${os}
                 create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
         ...     api
         ...     ${get_mobile_application_configurations_path}
         ...     ${header}
    log          ${response.text}
    ${dic} =	 Create Dictionary   response=${response}
    [Return]     ${dic}

API get agent balance
    [Arguments]    ${arg_dic}
    ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
         ...     api
         ...     ${get_agent_balance_path}
         ...     ${header}
    log          ${response.text}
    ${dic} =	 Create Dictionary   response=${response}
    [Return]     ${dic}

API get agent profile
    [Arguments]    ${arg_dic}
    ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${lang}        convert to string           ${arg_dic.lang}
    ${get_agent_profile_path}          replace string          ${get_agent_profile_path}       {lang}       ${lang}
                 create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
         ...     api
         ...     ${get_agent_profile_path}
         ...     ${header}
    log          ${response.text}
    ${dic} =	 Create Dictionary   response=${response}
    [Return]     ${dic}

API get list faqs
    [Arguments]    ${arg_dic}
    ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${lang}        convert to string           ${arg_dic.lang}
    ${user_type}        convert to string           ${arg_dic.user_type}
    ${get_list_faqs_path}          replace string          ${get_list_faqs_path}       {lang}       ${lang}
    ${get_list_faqs_path}          replace string          ${get_list_faqs_path}       {user_type}       ${user_type}
                 create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             get request
         ...     api
         ...     ${get_list_faqs_path}
         ...     ${header}
    log          ${response.text}
    ${dic} =	 Create Dictionary   response=${response}
    [Return]     ${dic}

API get list contact us
    [Arguments]    ${arg_dic}
    ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${lang}        convert to string           ${arg_dic.lang}
    ${user_type}        convert to string           ${arg_dic.user_type}
    ${get_list_contacts_us_path}          replace string          ${get_list_contacts_us_path}       {lang}       ${lang}
    ${get_list_contacts_us_path}          replace string          ${get_list_contacts_us_path}       {user_type}       ${user_type}
                 create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             get request
         ...     api
         ...     ${get_list_contacts_us_path}
         ...     ${header}
    log          ${response.text}
    ${dic} =	 Create Dictionary   response=${response}
    [Return]     ${dic}

API get list of agent sof bank information
    [Arguments]    ${arg_dic}
    ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
         ...     api
         ...     ${get_list_of_agent_sof_bank_information}
         ...     ${header}
    log          ${response.text}
    ${dic} =	 Create Dictionary   response=${response}
    [Return]     ${dic}

API activation Otp generation
    [Arguments]  ${arg_dic}
    ${activation_field}  [Common] - Create string param in dic    ${arg_dic}     activation_field
    ${agent_id}  [Common] - Create boolean param in dic   ${arg_dic}     agent_id
    ${device_unique_reference}  [Common] - Create string param in dic   ${arg_dic}     device_unique_reference

    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	${activation_field}
        ...     	${agent_id}
        ...         ${device_unique_reference}
        ...     }
    ${data}  replace string      ${data}      ,}    }
                create session          api        ${api_gateway_host}    disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${activation_otp_generation_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API activation Otp regeneration
    [Arguments]  ${arg_dic}
    ${otp_ref_id}  [Common] - Create string param in dic    ${arg_dic}     otp_ref_id

    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	${otp_ref_id}
        ...     }
    ${data}  replace string      ${data}      ,}    }
                create session          api        ${api_gateway_host}    disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${activation_otp_regeneration_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API activation Otp verification
    [Arguments]  ${arg_dic}
    log            ${arg_dic}         level=INFO
    ${otp_code}  [Common] - Create string param in dic    ${arg_dic}     otp_code
    ${otp_ref}  [Common] - Create string param in dic    ${arg_dic}     otp_ref
    ${user_ref_code}  [Common] - Create string param in dic    ${arg_dic}     user_ref_code
    ${agent_id}  [Common] - Create string param in dic    ${arg_dic}     agent_id

    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	${otp_code}
        ...     	${otp_ref}
        ...     	${user_ref_code}
        ...     	${agent_id}
        ...     }
    ${data}  replace string      ${data}      ,}    }
                create session          api        ${api_gateway_host}    disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${activation_otp_verification_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get order list
    [Arguments]    ${arg_dic}
    ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             get request
         ...     api
         ...     ${get_order_list_path}
         ...     ${header}
    log          ${response.text}
    ${dic} =	 Create Dictionary   response=${response}
    [Return]     ${dic}

API get order detail via channel adapter
    [Arguments]    ${arg_dic}
    ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${order_id}        convert to string           ${arg_dic.order_id}
    ${get_order_detail_path}          replace string          ${get_order_detail_via_channel_adapter_path}       {order_id}       ${order_id}
                 create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             get request
         ...     api
         ...     ${get_order_detail_path}
         ...     ${header}
    log          ${response.text}
    ${dic} =	 Create Dictionary   response=${response}
    [Return]     ${dic}

API get order configuration detail by id
    [Arguments]    ${arg_dic}
    ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${order_config_id}        convert to string           ${arg_dic.order_config_id}
    ${get_order_config_by_order_config_id_path}          replace string          ${get_order_config_by_order_config_id_path}       {order_config_id}       ${order_config_id}
                 create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
         ...     api
         ...     ${get_order_config_by_order_config_id_path}
         ...     ${header}
    log          ${response.text}
    ${dic} =	 Create Dictionary   response=${response}
    [Return]     ${dic}

API get list order configuration
    [Arguments]    ${arg_dic}
    ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
         ...     api
         ...     ${get_list_order_config_path}
         ...     ${header}
    log          ${response.text}
    ${dic} =	 Create Dictionary   response=${response}
    [Return]     ${dic}

API get all templates
    [Arguments]    ${arg_dic}
    ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
         ...     api
         ...     ${get_all_templates_path}
         ...     ${header}
    log          ${response.text}
    ${dic} =	 Create Dictionary   response=${response}
    [Return]     ${dic}

API create agent identity
    [Arguments]  ${arg_dic}
    ${header}               create dictionary
         ...     client_id=${arg_dic.header_client_id}
         ...     client_secret=${arg_dic.header_client_secret}
         ...     content-type=application/json
         ...     channel_id=${arg_dic.header_channel_id}
         ...     device_unique_reference=${arg_dic.header_device_unique_reference}
    ${data}              catenate
        ...	{
        ...		"otp_reference_id": "${arg_dic.otp_reference_id}",
        ...		"username": "${arg_dic.username}",
        ...		"password": "${arg_dic.password}",
        ...		"agent_id": "${arg_dic.agent_id}",
        ...		"identity_type": "${arg_dic.identity_type}"
        ...	}
                 create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             post request
         ...     api
         ...     ${create_agent_identity}
         ...     ${data}
         ...     ${NONE}
         ...     ${header}
    log          ${response.text}
    ${dic}=	     Create Dictionary   response=${response}
    [Return]     ${dic}

API Validate Agent Identity
    [Arguments]  ${arg_dic}
    ${agent_id}  [Common] - Create string param in dic    ${arg_dic}     agent_id
    ${activation_field}  [Common] - Create string param in dic    ${arg_dic}     activation_field

    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${setup_admin_client_id}
        ...     client_secret=${setup_admin_client_secret}
        ...     content-type=application/json
    ${data}              catenate       SEPARATOR=
        ...	{
        ...		"agent_id": "${arg_dic.test_agent_id}",
        ...		"activation_field": "${arg_dic.test_national_id}"
        ...	}
    create session          api        ${channel_gateway_host}    disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${agent_validation_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API Verification Otp
    [Arguments]  ${arg_dic}
    ${otp_code}  [Common] - Create string param in dic    ${arg_dic}     otp_code
    ${otp_ref}  [Common] - Create string param in dic    ${arg_dic}     otp_ref
    ${user_ref_code}  [Common] - Create string param in dic    ${arg_dic}     user_ref_code
    ${agent_id}  [Common] - Create string param in dic    ${arg_dic}     agent_id

    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     device_unique_reference=${arg_dic.device_unique_reference}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	"otp_code":"${arg_dic.otp_code}",
        ...     	"otp_ref":"${arg_dic.otp_ref}",
        ...     	"agent_id":"${arg_dic.agent_id}",
        ...         "user_ref_code":"${arg_dic.user_ref_code}"
        ...     }
    create session          api        ${channel_gateway_host}    disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${otp_verification_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API Get Otp From Mock
    [Arguments]     ${otp_reference_id}
    ${mock_get_otp_detail_path}  replace string      ${mock_get_otp_detail_path}      {otp_reference_id}    ${otp_reference_id}

    create session          api     ${SPI_get_otp_url}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${mock_get_otp_detail_path}
    ${response_text}=      set variable  ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API Set new password
    [Arguments]  ${arg_dic}
    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${setup_admin_client_id}
        ...     client_secret=${setup_admin_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${suite_admin_access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...         "username": "${arg_dic.user_name}",
        ...     	"new_password": "${arg_dic.new_password}",
        ...     	"user_type": {
        ...     	    "id": 2,
        ...     	    "name": "agent"
        ...     	},
        ...     	"agent_id": "${arg_dic.agent_id}",
        ...     	"national_id": "${arg_dic.national_id}",
        ...     	"otp_reference_id": "${arg_dic.test_otp_ref_id}"
        ...     }
    create session          api        ${channel_gateway_host}    disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${set_new_password_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API agent change password of identity
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${setup_admin_client_id}
        ...     client_secret=${setup_admin_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${username}=  [Common] - Create string param        username        ${arg_dic.username}
    ${old_password}=  [Common] - Create string param        old_password        ${arg_dic.old_password}}
    ${new_password}=  [Common] - Create string param      new_password        ${arg_dic.new_password}
    ${data}              catenate       SEPARATOR=
    ...    {
    ...            ${username}
    ...            ${old_password}
    ...            ${new_password}
    ...    }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },    }
    create session      api     ${channel_gateway_host}
    ${response}             put request
        ...     api
        ...     ${agent_change_password}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    ${dic} =	Create Dictionary   response=${response}
    log     ${response.text}
    [Return]    ${dic}

API agent change pin identity
     [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${setup_admin_client_id}
        ...     client_secret=${setup_admin_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	"user_name":"${arg_dic.username}",
        ...     	"password":"${arg_dic.password}",
        ...     	"pin":"${arg_dic.pin}"
        ...     }
    create session      api     ${channel_gateway_host}
    ${response}             put request
        ...     api
        ...     ${change_agent_identity_pin}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    ${dic} =	Create Dictionary   response=${response}
    log     ${response.text}
    [Return]    ${dic}

API Regenerate OTP with OTP Reference ID
    [Arguments]  ${arg_dic}
    ${otp_ref}  [Common] - Create string param in dic    ${arg_dic}     otp_ref

    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	"otp_ref_id":"${arg_dic.otp_ref}"
        ...     }
    create session          api        ${channel_gateway_host}    disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${otp_regeneration_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API Check if identity is logged in or not
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${setup_admin_client_id}
        ...     client_secret=${setup_admin_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${agent_id}=  [Common] - Create int param        agent_id       ${arg_dic.agent_id}
    ${channel_id}=  [Common] - Create int param       channel_id        ${arg_dic.channel_id}
    ${username}=  [Common] - Create string param      username        ${arg_dic.username}
    ${data}              catenate       SEPARATOR=
    ...    {
    ...            ${agent_id}
    ...            ${channel_id}
    ...            ${username}
    ...    }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },    }
    log     ${data}
    create session      api     ${channel_gateway_host}
    ${response}             post request
        ...     api
        ...     ${check_if_identity_is_logged_in_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${dic} =	Create Dictionary   response=${response}
    log     ${response.text}
    [Return]    ${dic}

API Otp generate with access token
    [Arguments]  ${arg_dic}
    ${agent_id}  [Common] - Create string param in dic    ${arg_dic}     agent_id
    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
        ...
        ${data}              catenate       SEPARATOR=
        ...     {
        ...     	"agent_id":"${arg_dic.agent_id}"
        ...     }
    create session          api        ${channel_gateway_host}    disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${agents_otp_generation_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API Search agent orders
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${setup_admin_client_id}
        ...     client_secret=${setup_admin_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${page_index}=  [Common] - Create int param        page_index       ${arg_dic.page_index}
    ${paging}=  [Common] - Create boolean param       paging        ${arg_dic.paging}
    ${data}              catenate       SEPARATOR=
    ...    {
    ...            ${page_index}
    ...            ${paging}
    ...            "service_group_id_list":[${arg_dic.service_group_id_list}]
    ...    }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },    }
    log     ${data}
    create session      api     ${channel_gateway_host}
    ${response}             post request
        ...     api
        ...     ${agent_search_orders_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${dic} =	Create Dictionary   response=${response}
    log     ${response.text}
    [Return]    ${dic}

API Get agent transactions summary
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${setup_admin_client_id}
        ...     client_secret=${setup_admin_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${start_date}=  [Common] - Create string param        start_date       ${arg_dic.start_date}
    ${end_date}=  [Common] - Create string param       end_date        ${arg_dic.end_date}
    ${data}              catenate       SEPARATOR=
    ...    {
    ...            ${start_date}
    ...            ${end_date}
    ...    }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },    }
    log     ${data}
    create session      api     ${channel_gateway_host}
    ${response}             post request
        ...     api
        ...     ${agent_transactions_summary_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${dic} =	Create Dictionary   response=${response}
    log     ${response.text}
    [Return]    ${dic}

API get order detail for printing
    [Arguments]    ${arg_dic}
    ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${order_id}        convert to string           ${arg_dic.order_id}
    ${get_order_detail_path}          replace string          ${get_order_detail_printing_path}       {order_id}       ${order_id}
                 create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             get request
         ...     api
         ...     ${get_order_detail_path}
         ...     ${header}
    log          ${response.text}
    ${dic} =	 Create Dictionary   response=${response}
    [Return]     ${dic}

API Verify smartcard with edc device
    [Arguments]  ${arg_dic}

    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     channel_id=${arg_dic.channel_id}
        ...     device_unique_reference=${arg_dic.edc_serial_number}

    ${data}              catenate   SEPARATOR=
    ...         {
    ...             "card_serial_number":"${arg_dic.card_serial_number}"
    ...          }

    ${data}  replace string      ${data}      },,    },
    create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${channel_adapter_verify_smartcard_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${data}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API Get Service Group List Via Channel Adapter
    [Arguments]  ${arg_dic}
    ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    create session      api     ${channel_gateway_host}
    ${response}             get request
        ...     api
        ...     ${get_service_group_via_channel_adapter}
        ...     ${header}
    ${dic} =	Create Dictionary   response=${response}
    log     ${response.text}
    [Return]    ${dic}

API delete edc device
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${device_id}=    convert to string   ${arg_dic.device_id}
    ${agent_delete_devices_path}=    replace string      ${agent_delete_devices_path}      {device_id}        ${device_id}   1
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${agent_delete_devices_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API create payment normal order via channel adapter
    [Arguments]     ${arg_dic}
    ${payer_user_ref_type}=             get from dictionary    ${arg_dic}   payer_user_ref_type
    ${payer_user_ref_value}=            get from dictionary    ${arg_dic}   payer_user_ref_value
    ${payer_user_id}=                   get from dictionary    ${arg_dic}   payer_user_id
    ${payer_user_type}=                 get from dictionary    ${arg_dic}   payer_user_type
    ${payee_user_ref_type}=             get from dictionary    ${arg_dic}   payee_user_ref_type
    ${payee_user_ref_value}=            get from dictionary    ${arg_dic}   payee_user_ref_value
    ${payee_user_id}=                   get from dictionary    ${arg_dic}   payee_user_id
    ${payee_user_type}=                 get from dictionary    ${arg_dic}   payee_user_type
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.payer_user_ref_value}
    ${payer_user_ref_param}    create json data for user ref object        ${payer_user_ref_type}      ${payer_user_ref_value}
    ${payer_user_id_param}     create json data for user id object         ${payer_user_id}      ${payer_user_type}
    ${payee_user_ref_param}    create json data for user ref object         ${payee_user_ref_type}      ${payee_user_ref_value}
    ${payee_user_id_param}     create json data for user id object         ${payee_user_id}      ${payee_user_type}

    ${request}              catenate       SEPARATOR=
        ...    {
        ...     "payer_user": {
        ...         ${payer_user_ref_param},
        ...         ${payer_user_id_param}
        ...     },
        ...     "payee_user": {
        ...         ${payee_user_ref_param}
        ...         ${payee_user_id_param}
        ...     },
        ...     "amount": ${arg_dic.amount},
        ...     "currency": "${arg_dic.currency}",
        ...     "note": "${arg_dic.note}"
        ...     }
    ${request}  replace string      ${request}      ,}    }
    ${request}  replace string      ${request}      },,    },
                create session          api     ${channel_gateway_host}
    ${response}             post request
        ...     api
        ...     ${agent_fundin_path}
        ...     ${request}
        ...     ${NONE}
        ...     ${header}
    log    ${request}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API Generate QR code
    [Arguments]  ${arg_dic}
    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    create session      api     ${channel_gateway_host}
    ${response}             post request
        ...     api
        ...     ${generate_qr_code_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API Verify QR code
    [Arguments]  ${arg_dic}
    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${data}              catenate   SEPARATOR=
    ...         {
    ...             "id":"${arg_dic.qr_code_id}",
    ...             "created_timestamp":"${arg_dic.qr_code_created_timestamp}"
    ...         }
    create session      api     ${channel_gateway_host}
    ${response}             post request
        ...     api
        ...     ${verify_qr_code_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API execute order via channel adapter
    [Arguments]  ${arg_dic}
    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate   SEPARATOR=
    ...         {
    #...             "security":{
    #...                "code": "${arg_dic.security}",
    #...                "ref": null,
    #...                "type": {
    #...                  "id": 2,
    #...                  "name": "PIN"
    #...                }
    #...             }
    ...         }
    create session      api     ${channel_gateway_host}
    ${path}          replace string       ${channel_adapter_execute_order_path}       {order_id}       ${arg_dic.order_id}
    ${response}             post request
        ...     api
        ...     ${path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}