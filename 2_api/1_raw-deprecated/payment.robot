*** Settings ***
Resource        ../../1_common/keywords.robot


*** Keywords ***
API add service group
    [Arguments]
    ...     ${access_token}
    ...     ${service_group_name}
    ...     ${description}
    ...     ${header_client_id}
    ...     ${header_client_secret}
        ${header}               create dictionary
            ...     client_id=${header_client_id}
            ...     client_secret=${header_client_secret}
            ...     content-type=application/json
            ...     Authorization=Bearer ${access_token}

        ${data}              catenate
            ...     {
            ...         "service_group_name":"${service_group_name}",
            ...         "description":"${description}"
            ...     }
                                create session          api     ${api_gateway_host}     disable_warnings=1
        ${response}             post request
            ...     api
            ...     ${add_service_group_path}
            ...     ${data}
            ...     ${NONE}
            ...     ${header}
        log        ${data}
        log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API Create wallet limitation
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_user_type_id}		    [Common] - Create string param		user_type_id		    ${arg_dic.user_type_id}
    ${param_type_id}		        [Common] - Create string param		type_id		            ${arg_dic.type_id}
    ${param_maximum_amount}		    [Common] - Create string param		maximum_amount		    ${arg_dic.maximum_amount}
    ${param_classification_id}		[Common] - Create string param		classification_id		${arg_dic.classification_id}
    ${param_currency}		        [Common] - Create string param		currency		        ${arg_dic.currency}
    ${param_service_id}		        [Common] - Create string param		service_id		        ${arg_dic.service_id}
    ${param_effective_date_from}    [Common] - Create string param		effective_date_from		${arg_dic.effective_date_from}
    ${param_effective_date_to}		[Common] - Create string param		effective_date_to		${arg_dic.effective_date_to}
    ${param_is_sale}		        [Common] - Create string param		is_sale		${arg_dic.is_sale}


     ${request}              catenate        SEPARATOR=
        ...     {
        ...     	${param_effective_date_from}
        ...     	${param_effective_date_to}
        ...     	${param_is_sale}
        ...     	${param_service_id}
        ...         ${param_maximum_amount}
        ...         ${param_user_type_id}
        ...         ${param_type_id}
        ...         ${param_classification_id}
        ...         ${param_currency}
        ...     }

    ${request}   replace string      ${request}      ,}    }
    log        ${request}

                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             post request
        ...     api
        ...     ${add_profile_wallet_limitation}
        ...     ${request}
        ...     ${NONE}
        ...     ${header}

     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API Create Service Group With All Fields
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_service_group_name}		[Common] - Create string param		service_group_name		${arg_dic.service_group_name}
    ${param_description}		[Common] - Create string param		description		${arg_dic.description}
    ${param_image_url}		[Common] - Create string param		image_url		${arg_dic.image_url}
    ${param_display_name}		[Common] - Create string param		display_name		${arg_dic.display_name}
    ${param_display_name_local}		[Common] - Create string param		display_name_local		${arg_dic.display_name_local}

     ${request}              catenate        SEPARATOR=
        ...     {
        ...     	${param_service_group_name}
        ...     	${param_description}
        ...     	${param_image_url}
        ...     	${param_display_name}
        ...         ${param_display_name_local}
        ...     }

    ${request}   replace string      ${request}      ,}    }
    log        ${request}

                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             post request
        ...     api
        ...     ${add_service_group_path}
        ...     ${request}
        ...     ${NONE}
        ...     ${header}

     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get a service group
     [Arguments]
     ...     ${access_token}
     ...     ${serviceGroupId}
     ...     ${header_client_id}
     ...     ${header_client_secret}
     ${header}               create dictionary
         ...     client_id=${header_client_id}
         ...     client_secret=${header_client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${access_token}

     ${get_a_service_group_path}=    replace string      ${get_a_service_group_path}        {serviceGroupId}        ${serviceGroupId}    1
                 create session          api     ${api_gateway_host}     disable_warnings=1

     ${response}             get request
         ...     api
         ...     ${get_a_service_group_path}
         ...     ${header}

     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API remove service group
    [Arguments]
    ...     ${client_id}
    ...     ${client_secret}
    ...     ${access_token}
    ...     ${service_group_id}
    ${header}               create dictionary
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}

    ${service_group_id}     convert to string       ${service_group_id}
    ${path}                 replace string          ${remove_service_group_path}       {serviceGroupId}       ${service_group_id}       1
                            create session          api     ${api_gateway_host}
    ${response}             delete request
    ...     api
    ...     ${path}
    ...     ${NONE}
    ...     ${NONE}
    ...     ${header}

    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all service groups
    [Arguments]    &{arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_all_service_group_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update service group
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

     ${data}              catenate
         ...     {
         ...         "service_group_name":"${arg_dic.service_group_name}",
         ...         "description":"${arg_dic.description}"
         ...     }

     ${service_group_id}      convert to string       ${arg_dic.service_group_id}
     ${update_service_group_path}=    replace string      ${update_service_group_path}        {serviceGroupId}        ${service_group_id}    1
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             put request
         ...     api
         ...     ${update_service_group_path}
         ...     ${data}
         ...     ${NONE}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create service
    [Arguments]
    ...     ${access_token}
    ...     ${service_group_id}
    ...     ${service_name}
    ...     ${currency}
    ...     ${description}
    ...     ${image_url}
    ...     ${display_name}
    ...     ${display_name_local}
    ...     ${apply_to_all_customer_classification}
    ...     ${list_service_customer_classification}
    ...     ${is_allow_debt}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}

    ${param_service_group_id}		[Common] - Create int param		service_group_id		${service_group_id}
    ${param_service_name}		[Common] - Create string param		service_name		${service_name}
    ${param_currency}		[Common] - Create string param		currency		${currency}
    ${param_description}		[Common] - Create string param		description		${description}
    ${param_image_url}		[Common] - Create string param		image_url		${image_url}
    ${param_display_name}		[Common] - Create string param		display_name		${display_name}
    ${param_display_name_local}		[Common] - Create string param		display_name_local		${display_name_local}
    ${param_apply_to_all_customer_classification}		[Common] - Create string param		apply_to_all_customer_classification		${apply_to_all_customer_classification}
    ${param_is_allow_debt}		[Common] - Create string param		is_allow_debt		${is_allow_debt}

    ${header}               create dictionary
        ...        client_id=${header_client_id}
        ...        client_secret=${header_client_secret}
        ...        content-type=application/json
        ...        Authorization=Bearer ${access_token}
    ${request}              catenate        SEPARATOR=
        ...     {
        ...     	${param_service_group_id}
        ...     	${param_service_name}
        ...     	${param_currency}
        ...     	${param_description}
        ...     	${param_image_url}
        ...     	${param_display_name}
        ...     	${param_display_name_local}
        ...         ${param_apply_to_all_customer_classification}
        ...         ${param_is_allow_debt}
        ...         "service_customer_classification": [${list_service_customer_classification}]
        ...     }
    ${request}   replace string      ${request}      ,}    }

                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_service_path}
        ...     ${request}
        ...     ${NONE}
        ...     ${header}
    ${dic} =	Create Dictionary   response=${response}
    log         ${request}
    log         ${response.text}

    [Return]    ${dic}

API get service details
     [Arguments]
     ...     ${client_id}
     ...     ${client_secret}
     ...     ${access_token}
     ...     ${serviceId}
     ${header}               create dictionary
         ...     client_id=${client_id}
         ...     client_secret=${client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${access_token}

     ${serviceId}=    convert to string      ${serviceId}
     ${get_service_details_path}=    replace string      ${get_service_details_path}        {serviceId}        ${serviceId}    1
                 create session          api     ${api_gateway_host}     disable_warnings=1

     ${response}             get request
         ...     api
         ...     ${get_service_details_path}
         ...     ${header}

     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all services
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_all_services_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update service
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

     ${data}              catenate
         ...     {
         ...         "service_group_id":"${arg_dic.service_group_id}",
         ...         "service_name":"${arg_dic.service_name}",
         ...         "currency":"${arg_dic.currency}",
         ...         "status":"${arg_dic.status}",
         ...         "description":"${arg_dic.description}"
         ...     }

     ${service_id}      convert to string       ${arg_dic.service_id}
     ${update_service_path}=    replace string      ${update_service_path}        {serviceId}        ${service_id}    1
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             put request
         ...     api
         ...     ${update_service_path}
         ...     ${data}
         ...     ${NONE}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API active-suspend service
    [Arguments]    &{arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

     ${data}              catenate
         ...     {
         ...         "status":"${arg_dic.status}"
         ...     }

     ${service_id}      convert to string       ${arg_dic.service_id}
     ${update_service_status_path}=    replace string      ${update_service_status_path}        {service_id}        ${service_id}    1
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             put request
         ...     api
         ...     ${update_service_status_path}
         ...     ${data}
         ...     ${NONE}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete service
    [Arguments]
    ...     ${access_token}
    ...     ${serviceId}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}

    ${serviceId}     convert to string       ${serviceId}
    ${delete_service_path}                 replace string          ${delete_service_path}       {serviceId}       ${serviceId}       1
                            create session          api     ${api_gateway_host}
    ${response}             delete request
    ...     api
    ...     ${delete_service_path}
    ...     ${NONE}
    ...     ${NONE}
    ...     ${header}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get agent type service
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

     ${agent_type_id}=    convert to string      ${arg_dic.agent_type_id}
     ${get_agent_type_service_path}=    replace string      ${get_agent_type_service_path}        {agent_type_id}        ${agent_type_id}    1
                 create session          api     ${api_gateway_host}     disable_warnings=1

     ${response}             get request
         ...     api
         ...     ${get_agent_type_service_path}
         ...     ${header}

     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create service command
    [Arguments]
    ...     ${access_token}
    ...     ${service_id}
    ...     ${command_id}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...        client_id=${header_client_id}
        ...        client_secret=${header_client_secret}
        ...        content-type=application/json
        ...        Authorization=Bearer ${access_token}
    ${service_id}=    convert to string      ${service_id}
    ${command_id}=    convert to string      ${command_id}
    ${data}              catenate
            ...     {
            ...         "service_id":"${service_id}",
            ...         "command_id":"${command_id}"
            ...     }
            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_service_command_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API get service commands by service id
    [Arguments]
    ...     ${access_token}
    ...     ${service_id}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${service_id}   convert to string   ${service_id}
    ${path}=    replace string      ${get_service_commands_by_service_id_path}         {service_id}       ${service_id}    1
                create session          api     ${api_gateway_host}     disable_warnings=1

    ${response}             get request
        ...     api
        ...     ${path}
        ...     ${header}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete service command
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

     ${service_command_id}      convert to string       ${arg_dic.service_command_id}
     ${remove_service_command_path}=    replace string      ${remove_service_command_path}        {serviceCommandId}        ${service_command_id}    1
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             delete request
         ...     api
         ...     ${remove_service_command_path}
         ...     ${NONE}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create fee tier
    [Arguments]
    ...     ${access_token}
    ...     ${service_command_id}
    ...     ${fee_tier_condition}
    ...     ${condition_amount}
    ...     ${fee_type}
    ...     ${fee_amount}
    ...     ${bonus_type}
    ...     ${bonus_amount}
    ...     ${amount_type}
    ...     ${settlement_type}
    ...     ${header_client_id}
    ...     ${header_client_secret}

    ${param_fee_tier_condition}		[Common] - Create string param		fee_tier_condition		${fee_tier_condition}
    ${param_condition_amount}		[Common] - Create int param		condition_amount		${condition_amount}
    ${param_fee_type}		[Common] - Create string param		fee_type		${fee_type}
    ${param_fee_amount}		[Common] - Create int param		fee_amount		${fee_amount}
    ${param_bonus_type}		[Common] - Create string param		bonus_type		${bonus_type}
    ${param_bonus_amount}		[Common] - Create int param		bonus_amount		${bonus_amount}
    ${param_amount_type}		[Common] - Create string param		amount_type		${amount_type}
    ${param_settlement_type}		[Common] - Create string param		settlement_type		${settlement_type}

    ${header}               create dictionary
        ...        client_id=${header_client_id}
        ...        client_secret=${header_client_secret}
        ...        content-type=application/json
        ...        Authorization=Bearer ${access_token}
    ${request}              catenate        SEPARATOR=
        ...     {
        ...     	${param_fee_tier_condition}
        ...     	${param_condition_amount}
        ...     	${param_fee_type}
        ...     	${param_fee_amount}
        ...     	${param_bonus_type}
        ...     	${param_bonus_amount}
        ...     	${param_amount_type}
        ...     	${param_settlement_type}
        ...     }
    ${service_command_id}   convert to string   ${service_command_id}
    ${request}   replace string      ${request}      ,}    }
    ${path}=    replace string      ${create_fee_tier_path}         {service_command_id}       ${service_command_id}    1
            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${path}
        ...     ${request}
        ...     ${NONE}
        ...     ${header}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API create fee tier from A-O
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_fee_tier_condition}		[Common] - Create string param		fee_tier_condition		${arg_dic.fee_tier_condition}
    ${param_condition_amount}		[Common] - Create int param		condition_amount		${arg_dic.condition_amount}
    ${param_fee_type}		[Common] - Create string param		fee_type		${arg_dic.fee_type}
    ${param_fee_amount}		[Common] - Create int param		fee_amount		${arg_dic.fee_amount}
    ${param_bonus_type}		[Common] - Create string param		bonus_type		${arg_dic.bonus_type}
    ${param_bonus_amount}		[Common] - Create int param		bonus_amount		${arg_dic.bonus_amount}
    ${param_amount_type}		[Common] - Create string param		amount_type		${arg_dic.amount_type}
    ${param_settlement_type}		[Common] - Create string param		settlement_type		${arg_dic.settlement_type}

#     ${request}              catenate        SEPARATOR=
#     ...     {
#     ...     	"fee_tier_condition":"${arg_dic.fee_tier_condition}",
#     ...     	"condition_amount":"${arg_dic.condition_amount}",
#     ...        "settlement_type":"${arg_dic.settlement_type}",
#     ...        @{list_fee_tier_data}
#     ...     }

     ${request}              catenate        SEPARATOR=
        ...     {
        ...     	${param_fee_tier_condition}
        ...     	${param_condition_amount}
        ...     	${param_fee_type}
        ...     	${param_fee_amount}
        ...     	${param_bonus_type}
        ...     	${param_bonus_amount}
        ...     	${param_amount_type}
        ...     	${param_settlement_type}
        ...         ${arg_dic.list_fee_tier_data}
        ...     }

    ${request}   replace string      ${request}      ,}    }
    ${request}   replace string      ${request}      ,[    ,
    ${request}   replace string      ${request}      ]}    }
    ${request}     replace string          ${request}     u'        ${empty}
    ${request}     replace string          ${request}     }'        }

    log        ${request}

     ${service_command_id}      convert to string       ${arg_dic.service_command_id}
     ${create_fee_tier_path}=    replace string      ${create_fee_tier_path}         {service_command_id}       ${service_command_id}    1

                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             post request
        ...     api
        ...     ${create_fee_tier_path}
        ...     ${request}
        ...     ${NONE}
        ...     ${header}

     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update fee tier from A-O
    [Arguments]    ${arg_dic}       @{list_fee_tier_data}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

         ${request}              catenate        SEPARATOR=
         ...     {
         ...     	"fee_tier_condition":"${arg_dic.fee_tier_condition}",
         ...     	"condition_amount":"${arg_dic.condition_amount}",
         ...        "settlement_type":"${arg_dic.settlement_type}",
         ...        @{list_fee_tier_data}
         ...     }

        ${request}   replace string      ${request}      ,}    }
        log        ${request}

     ${fee_tier_id}      convert to string       ${arg_dic.fee_tier_id}
     ${update_fee_tier_path}=    replace string      ${update_fee_tier_path}         {fee_tier_id}       ${fee_tier_id}    1

                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             put request
        ...     api
        ...     ${update_fee_tier_path}
        ...     ${request}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}

     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get list fee tiers by service command id
    [Arguments]
    ...     ${access_token}
    ...     ${service_command_id}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${service_command_id}   convert to string   ${service_command_id}
    ${path}=    replace string      ${get_list_fee_tiers_by_service_command_id_path}        {service_command_id}       ${service_command_id}    1
                create session          api     ${api_gateway_host}     disable_warnings=1

    ${response}             get request
        ...     api
        ...     ${path}
        ...     ${header}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete fee tier
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

     ${fee_tier_id}      convert to string       ${arg_dic.fee_tier_id}
     ${remove_fee_tier_path}=    replace string      ${remove_fee_tier_path}        {fee_tier_id}        ${fee_tier_id}    1
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             delete request
         ...     api
         ...     ${remove_fee_tier_path}
         ...     ${NONE}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get list of tier from
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_tier_amount_from}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update all banlance distribution
    [Arguments]
    ...     ${access_token}
    ...     ${fee_tier_id}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ...     @{balance_distributions}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	"balance_distributions":
        ...             @{balance_distributions}
        ...     }
    ${data}     replace string          ${data}     u'        ${empty}
    ${data}     replace string          ${data}     }'        }
    ${data}     replace string          ${data}     }',        },
    ${data}     replace string      ${data}     :,[{        :[{
    ${data}     replace string      ${data}     [,        [
    ${data}     replace string          ${data}     ,]        ]
    ${data}     replace string          ${data}     \\'s        's

    ${fee_tier_id}      convert to string      ${fee_tier_id}
    ${update_all_banlance_distribution_path}=    replace string      ${update_all_banlance_distribution_path}        {fee_tier_id}        ${fee_tier_id}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${update_all_banlance_distribution_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all banlance distributions
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${fee_tier_id}   convert to string   ${arg_dic.fee_tier_id}
    ${get_all_balance_distribution_by_fee_tier_path}=    replace string      ${get_all_balance_distribution_by_fee_tier_path}        {fee_tier_id}       ${fee_tier_id}    1
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_all_balance_distribution_by_fee_tier_path}
        ...     ${header}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete balance distribution
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${balance_distribution_id}   convert to string   ${arg_dic.balance_distribution_id}
    ${delete_balance_distribution_path}=    replace string      ${delete_balance_distribution_path}        {balance_distribution_id}       ${balance_distribution_id}    1
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             delete request
        ...     api
        ...     ${delete_balance_distribution_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API get list fee tier conditions
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_fee_tier_condition_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all commands
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_all_command_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all action types
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_all_action_types_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all amount types
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_all_amount_types_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all actor types
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_all_actor_types_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all sof types
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_all_sof_types_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all bonus type
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_all_bonus_types_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all fee type
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_all_fee_types_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API payment search service group by name
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

     ${payment_search_service_group_by_name_path}=    replace string      ${payment_search_service_group_path}        {service_group_name}        ${arg_dic.service_group_name}    1
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${payment_search_service_group_by_name_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API payment search service by name
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

     ${payment_search_service_path}=    replace string      ${payment_search_service_path}        {service_name}        ${arg_dic.service_name}    1
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${payment_search_service_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all spi url types
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_all_spi_url_types_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all spi url call methods
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_all_spi_url_call_methods_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all payment methods
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_all_payment_methods_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create SPI URL
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...        client_id=${arg_dic.header_client_id}
        ...        client_secret=${arg_dic.header_client_secret}
        ...        content-type=application/json
        ...        Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate
        ...     {
        ...         "spi_url_type":"${arg_dic.spi_url_type}",
        ...         "url":"${arg_dic.url}",
        ...         "spi_url_call_method":"${arg_dic.spi_url_call_method}",
        ...         "expire_in_minute":${arg_dic.expire_in_minute},
        ...         "max_retry":${arg_dic.max_retry},
        ...         "retry_delay_millisecond":${arg_dic.retry_delay_millisecond},
        ...         "read_timeout":${arg_dic.read_timeout}
        ...     }

            create session          api     ${api_gateway_host}     disable_warnings=1
    ${service_command_id}      convert to string      ${arg_dic.service_command_id}
    ${path}=    replace string      ${create_SPI_URL_path}        {service_command_id}       ${service_command_id}    1
    ${response}             post request
        ...     api
        ...     ${path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API get SPI URL by service command
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

     ${service_command_id}      convert to string      ${arg_dic.service_command_id}
     ${get_SPI_URL_by_service_command_path}=    replace string      ${get_SPI_URL_by_service_command_path}        {service_command_id}       ${service_command_id}    1
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_SPI_URL_by_service_command_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get SPI URL by URL id
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

     ${spi_url_id}      convert to string      ${arg_dic.spi_url_id}
     ${get_SPI_URL_by_URL_id_path}=    replace string      ${get_SPI_URL_by_URL_id_path}        {spi_url_id}       ${spi_url_id}    1
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_SPI_URL_by_URL_id_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update SPI URL
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...        client_id=${arg_dic.client_id}
        ...        client_secret=${arg_dic.client_secret}
        ...        content-type=application/json
        ...        Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate
        ...     {
        ...         "service_command_id":"${arg_dic.service_command_id}",
        ...         "spi_url_type":"${arg_dic.spi_url_type}",
        ...         "url":"${arg_dic.url}",
        ...         "spi_url_call_method":"${arg_dic.spi_url_call_method}",
        ...         "expire_in_minute":${arg_dic.expire_in_minute},
        ...         "max_retry":${arg_dic.max_retry},
        ...         "retry_delay_millisecond":${arg_dic.retry_delay_millisecond},
        ...         "read_timeout":${arg_dic.read_timeout}
        ...     }

            create session          api     ${api_gateway_host}     disable_warnings=1
    ${spi_url_id}      convert to string      ${arg_dic.spi_url_id}
    ${update_SPI_URL_path}=    replace string      ${update_SPI_URL_path}        {spi_url_id}       ${spi_url_id}    1
    ${response}             put request
        ...     api
        ...     ${update_SPI_URL_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete SPI URL
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

     ${spi_url_id}      convert to string       ${arg_dic.spi_url_id}
     ${delete_SPI_URL_path}=    replace string      ${delete_SPI_URL_path}        {spi_url_id}        ${spi_url_id}    1
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             delete request
         ...     api
         ...     ${delete_SPI_URL_path}
         ...     ${NONE}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get SPI URL configuration types
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

                  create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_all_spi_url_configuration_types_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create SPI URL configuration
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate
        ...     {
        ...         "spi_url_configuration_type":"${arg_dic.spi_url_configuration_type}",
        ...         "url":"${arg_dic.url}",
        ...         "read_timeout":"${arg_dic.read_timeout}",
        ...         "max_retry":${arg_dic.max_retry},
        ...         "expire_in_minute":${arg_dic.expire_in_minute},
        ...         "retry_delay_millisecond":${arg_dic.retry_delay_millisecond}
        ...     }
     ${spi_url_id}      convert to string       ${arg_dic.spi_url_id}
     ${create_spi_url_configuration_path}=    replace string      ${create_spi_url_configuration_path}        {spi_url_id}        ${spi_url_id}    1
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             post request
         ...     api
         ...     ${create_spi_url_configuration_path}
         ...     ${data}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get SPI URL configuration details from ID
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

     ${spi_url_id}      convert to string      ${arg_dic.spi_url_id}
     ${get_spi_url_configuration_by_spi_url_path}=    replace string      ${get_spi_url_configuration_by_spi_url_path}        {spi_url_id}       ${spi_url_id}    1
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_spi_url_configuration_by_spi_url_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update SPI URL configuration
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate
        ...     {
        ...         "spi_url_configuration_type":"${arg_dic.spi_url_configuration_type}",
        ...         "url":"${arg_dic.url}",
        ...         "read_timeout":"${arg_dic.read_timeout}",
        ...         "max_retry":${arg_dic.max_retry},
        ...         "expire_in_minute":${arg_dic.expire_in_minute},
        ...         "retry_delay_millisecond":${arg_dic.retry_delay_millisecond}
        ...     }
     ${spi_url_configuration_id}      convert to string       ${arg_dic.spi_url_configuration_id}
     ${update_spi_url_configuration_path}=    replace string      ${update_spi_url_configuration_path}        {spi_url_configuration_id}        ${spi_url_configuration_id}    1
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             put request
         ...     api
         ...     ${update_spi_url_configuration_path}
         ...     ${data}
         ...     ${NONE}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete SPI URL configuration
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

     ${spi_url_configuration_id}      convert to string       ${arg_dic.spi_url_configuration_id}
     ${delete_spi_url_configuration_path}=    replace string      ${delete_spi_url_configuration_path}        {spi_url_configuration_id}        ${spi_url_configuration_id}    1
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             delete request
         ...     api
         ...     ${delete_spi_url_configuration_path}
         ...     ${NONE}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all Payment security types
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_all_payment_security_types_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create payment pin
    [Arguments]
    ...     ${pin}
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...        client_id=${header_client_id}
        ...        client_secret=${header_client_secret}
        ...        content-type=application/json
        ...        Authorization=Bearer ${access_token}
    ${data}              catenate
            ...     {
            ...         "pin":"${pin}"
            ...     }
            create session          api     ${api_gateway_host}     disable_warnings=1
    log     ${data}
    ${response}             put request
        ...     api
        ...     ${create_payment_pin_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API Get all tier level config
    [Arguments]
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...        client_id=${header_client_id}
        ...        client_secret=${header_client_secret}
        ...        content-type=application/json
        ...        Authorization=Bearer ${access_token}
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_all_tier_level_path}
        ...     ${header}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API update tier level
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate
        ...     {
        ...         	"label":"${arg_dic.label}"
        ...     }
     ${tier_level_id}      convert to string       ${arg_dic.tier_level_id}
     ${update_tier_level_label_path}=    replace string      ${update_tier_level_label_path}        {tier_level_id}        ${tier_level_id}    1
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             put request
         ...     api
         ...     ${update_tier_level_label_path}
         ...     ${data}
         ...     ${NONE}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API payment create normal order
    [Arguments]
    ...     ${access_token}
    ...     ${ext_transaction_id}
    ...     ${product_service_id}
    ...     ${product_service_name}
    ...     ${product_name}
    ...     ${product_ref_1}
    ...     ${product_ref_2}
    ...     ${product_ref_3}
    ...     ${product_ref_4}
    ...     ${product_ref_5}
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${payer_user_id}
    ...     ${payer_user_type}
    ...     ${payee_user_ref_type}
    ...     ${payee_user_ref_value}
    ...     ${payee_user_id}
    ...     ${payee_user_type}
    ...     ${amount}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ...     ${header_device_unique_reference}=${param_not_used}
    ...     ${header_channel_id}=${param_not_used}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${access_token}

    run keyword if   '${header_device_unique_reference}'!='${param_not_used}'   set to dictionary     ${header}     device_unique_reference  ${header_device_unique_reference}
    run keyword if   '${header_channel_id}'!='${param_not_used}'   set to dictionary     ${header}     channel_id  ${header_channel_id}

    ${payer_user_ref_param}    create json data for user ref object        ${payer_user_ref_type}      ${payer_user_ref_value}
    ${payer_user_id_param}     create json data for user id object         ${payer_user_id}      ${payer_user_type}
    ${payee_user_ref_param}    create json data for user ref object         ${payee_user_ref_type}      ${payee_user_ref_value}
    ${payee_user_id_param}     create json data for user id object         ${payee_user_id}      ${payee_user_type}

    ${request}              catenate       SEPARATOR=
        ...    {
        ...     "ext_transaction_id": "${ext_transaction_id}",
        ...     "product_service": {
        ...         "id": ${product_service_id},
        ...         "name": "${product_service_name}"
        ...         },
        ...     "product": {
        ...         "product_name": "${product_name}",
        ...         "product_ref1": "${product_ref_1}",
        ...         "product_ref2": "${product_ref_2}",
        ...         "product_ref3": "${product_ref_3}",
        ...         "product_ref4": "${product_ref_4}",
        ...         "product_ref5": "${product_ref_5}"
        ...         },
        ...     "payer_user": {
        ...         ${payer_user_ref_param}
        ...         ${payer_user_id_param}
        ...     },
        ...     "payee_user": {
        ...         ${payee_user_ref_param}
        ...         ${payee_user_id_param}
        ...     },
        ...     "amount": ${amount}
        ...     }
                create session          api     ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${payment_create_normal_order_path}
        ...     ${request}
        ...     ${NONE}
        ...     ${header}
    log    ${request}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API payment create artifact order
    [Arguments]
    ...     ${access_token}
    ...     ${ext_transaction_id}
    ...     ${product_service_id}
    ...     ${product_service_name}
    ...     ${product_name}
    ...     ${product_ref_1}
    ...     ${product_ref_2}
    ...     ${product_ref_3}
    ...     ${product_ref_4}
    ...     ${product_ref_5}
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${payee_user_ref_type}
    ...     ${payee_user_ref_value}
    ...     ${payee_user_id}
    ...     ${payee_user_type}
    ...     ${amount}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ...     @{additional_references}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
#    ${payee_user_ref_type}  convert to string  ${payee_user_ref_type}
#    ${payee_user_ref_value}  convert to string  ${payee_user_ref_value}
    log     ${payee_user_ref_type}
    ${payer_user_ref_param}    create json data for user ref object        ${payer_user_ref_type}      ${payer_user_ref_value}
    ${payee_user_ref_param}    create json data for user ref object         ${payee_user_ref_type}      ${payee_user_ref_value}
    ${payee_user_id_param}     create json data for user id object         ${payee_user_id}      ${payee_user_type}

    ${data_json}    set variable    ${empty}
    ${length}   get length      ${additional_references}
    :FOR     ${index}   IN RANGE   0    ${length}
    \      ${additional_reference}    get from list      ${additional_references}      ${index}
    \      ${additional_reference}    strip string         ${additional_reference}
    \      ${data_json}     catenate    SEPARATOR=,  ${data_json}   ${additional_reference}

    ${additional_references_data}              catenate       SEPARATOR=
        ...     	,"additional_references":
        ...     	[
        ...             ${data_json}
        ...     	]
    ${additional_references_data}     replace string      ${additional_references_data}     [,        [
    ${additional_references_data}     replace string          ${additional_references_data}     ,]        ]

    ${request}              catenate       SEPARATOR=
        ...    {
        ...     "ext_transaction_id": "${ext_transaction_id}",
        ...     "product_service": {
        ...         "id": ${product_service_id},
        ...         "name": "${product_service_name}"
        ...         },
        ...     "product": {
        ...         "product_name": "${product_name}",
        ...         "product_ref1": "${product_ref_1}",
        ...         "product_ref2": "${product_ref_2}",
        ...         "product_ref3": "${product_ref_3}",
        ...         "product_ref4": "${product_ref_4}",
        ...         "product_ref5": "${product_ref_5}"
        ...         },
        ...     "payer_user": {
        ...         ${payer_user_ref_param}
        ...     },
        ...     "payee_user": {
        ...         ${payee_user_ref_param}
        ...         ${payee_user_id_param}
        ...     },
        ...     "amount": ${amount}
        ...     ${additional_references_data}
        ...     }
                create session          api     ${api_gateway_host}

    log         ${request}
    ${response}             post request
        ...     api
        ...     ${payment_create_artifact_order_path}
        ...     ${request}
        ...     ${NONE}
        ...     ${header}
    log    ${request}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API payment cancel artifact order
    [Arguments]
    ...     ${access_token}
    ...     ${ref_order_id}
    ...     ${ext_transaction_id}
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${payer_user_ref_param}    create json data for user ref object        ${payer_user_ref_type}      ${payer_user_ref_value}
    ${request}              catenate       SEPARATOR=
        ...    {
        ...     "ext_transaction_id": "${ext_transaction_id}",
        ...     "payer_user": {
        ...         ${payer_user_ref_param}
        ...     }
        ...     }
                create session          api     ${api_gateway_host}
    log         ${request}
    ${path}=    replace string      ${create_cancel_artifacts_payment_path}        {ref_order_id}       ${ref_order_id}    1
    ${response}             delete request
        ...     api
        ...     ${create_cancel_artifacts_payment_path}
        ...     ${request}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API payment cancel normal order
    [Arguments]
    ...     ${access_token}
    ...     ${ref_order_id}
    ...     ${ext_transaction_id}
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${payer_user_ref_param}    create json data for user ref object        ${payer_user_ref_type}      ${payer_user_ref_value}
    ${request}              catenate       SEPARATOR=
        ...    {
        ...     "ext_transaction_id": "${ext_transaction_id}",
        ...     "payer_user": {
        ...         ${payer_user_ref_param}
        ...     }
        ...     }
                create session          api     ${api_gateway_host}
    log         ${request}
    ${path}=    replace string      ${create_cancel_normals_payment_path}        {ref_order_id}       ${ref_order_id}    1
    ${response}             delete request
        ...     api
        ...     ${create_cancel_normals_payment_path}
        ...     ${request}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API payment create trust order
    [Documentation]     example for @{list_additional_references}
    ...     ${1}  set variable    { "key": "key1", "value": "123456" },
    ...     ${2}  set variable    { "key": "key2", "value": "123456" }
    ...     @{list_additional_references}  create list     ${1}    ${2}
    ...     ${arg_dic}       create dictionary    additional_references=@{list_additional_references}
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${request}              catenate       SEPARATOR=
        ...    {
        ...     "ext_transaction_id": "${arg_dic.ext_transaction_id}",
        ...     "trust_token": "${arg_dic.trust_token}",
        ...     "product_service": {
        ...         "id": ${arg_dic.product_service_id},
        ...         "name": "${arg_dic.product_service_name}"
        ...         },
        ...     "product": {
        ...         "product_name": "${arg_dic.product_name}",
        ...         "product_ref1": "${arg_dic.product_ref_1}",
        ...         "product_ref2": "${arg_dic.product_ref_2}",
        ...         "product_ref3": "${arg_dic.product_ref_3}",
        ...         "product_ref4": "${arg_dic.product_ref_4}",
        ...         "product_ref5": "${arg_dic.product_ref_5}"
        ...         },
        ...     "payer_user": {
        ...         "user_id": ${arg_dic.payer_user_id},
        ...         "user_type": "${arg_dic.payer_user_type_name}",
        ...         "sof": {
    	...             	"id": ${arg_dic.payer_sof_id},
    	...         	    "type_id": ${arg_dic.payer_sof_type_id}
        ...                 }
        ...             },
        ...     "payee_user": {
        ...         "user_id": ${arg_dic.payee_user_id},
        ...         "user_type": "${arg_dic.payee_user_type_name}",
        ...         "sof": {
    	...             	"id": ${arg_dic.payee_sof_id},
    	...         	    "type_id": ${arg_dic.payee_sof_type_id}
        ...                 }
        ...             },
        ...     "amount": ${arg_dic.amount},
        ...     "additional_references": [ ${arg_dic.additional_references}]
        ...     }
                create session          api     ${api_gateway_host}

    ${response}             post request
        ...     api
        ...     ${payment_create_trust_order_path}
        ...     ${request}
        ...     ${NONE}
        ...     ${header}
    log to console   ${request}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API get order detail
    [Arguments]
    ...     ${access_token}
    ...     ${order_id}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${order_id_api}     convert to string       ${order_id}
    log         ${get_order_detail_path}
    ${path}=    replace string      ${get_order_detail_path}        {order_id}        ${order_id_api}    1
                create session          api     ${api_gateway_host}     disable_warnings=1

    ${response}             get request
        ...     api
        ...     ${path}
        ...     ${header}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API get order detail properties
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_order_detail_prop_path}
         ...     ${header}

     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API payment execute order
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...        client_id=${arg_dic.client_id}
        ...        client_secret=${arg_dic.client_secret}
        ...        content-type=application/json
        ...        Authorization=Bearer ${arg_dic.access_token}

    ${order_id}=    convert to string      ${arg_dic.order_id}
    ${path}=    replace string      ${payment_execute_order_path}        {orderId}        ${order_id}    1
                create session          api     ${api_gateway_host}     disable_warnings=1

    ${response}             post request
        ...     api
        ...     ${path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API payment get order
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

         create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${user_get_all_orders_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create user sof cash
    [Arguments]
    ...     ${currency}
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}

    ${header}               create dictionary
        ...        client_id=${header_client_id}
        ...        client_secret=${header_client_secret}
        ...        content-type=application/json
        ...        Authorization=Bearer ${access_token}
    ${data}              catenate
            ...     {
            ...         "currency":"${currency}"
            ...     }
            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_user_sof_cash_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API user get all his/her sof cash
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

         create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${user_get_their_sof_cash_path}
         ...     ${header}

     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API user get his/her sof cash by currency
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

         create session          api     ${api_gateway_host}     disable_warnings=1
     ${user_get_sof_cash_by_currency_path}=    replace string      ${user_get_sof_cash_by_currency_path}        {currency}       ${arg_dic.currency}    1
     ${response}             get request
         ...     api
         ...     ${user_get_sof_cash_by_currency_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API system user create user sof cash
    [Arguments]
    ...     ${user_id}
    ...     ${user_type_id}
    ...     ${currency}
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}

    ${header}               create dictionary
        ...        client_id=${header_client_id}
        ...        client_secret=${header_client_secret}
        ...        content-type=application/json
        ...        Authorization=Bearer ${access_token}
    ${data}              catenate
            ...     {
            ...         "user_id":${user_id},
            ...         "user_type_id":${user_type_id},
            ...         "currency":"${currency}"
            ...     }
            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${admin_create_user_sof_cash_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API system user create a company agent cash source of fund with currency
    [Arguments]
    ...     ${currency}
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...        client_id=${header_client_id}
        ...        client_secret=${header_client_secret}
        ...        content-type=application/json
        ...        Authorization=Bearer ${access_token}
    ${data}              catenate
            ...     {
            ...         "currency":"${currency}"
            ...     }
            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${system_user_create_a_company_agent_cash_source_of_fund_with_currency_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API system user update company agent cash balance
    [Arguments]
    ...     ${currency}
    ...     ${amount}
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...        client_id=${header_client_id}
        ...        client_secret=${header_client_secret}
        ...        content-type=application/json
        ...        Authorization=Bearer ${access_token}
    ${amount}=      convert to string    ${amount}
    ${data}              catenate
            ...     {
            ...         "amount":${amount}
            ...     }
            create session          api     ${api_gateway_host}     disable_warnings=1
    ${path}=    replace string      ${system_user_update_company_agent_cash_balance_path}        {currency}       ${currency}    1
    log     ${data}
    ${response}             put request
        ...     api
        ...     ${path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API system user get company agent cash balance history
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${system_user_get_company_agent_cash_balance_history_path}=    replace string      ${system_user_get_company_agent_cash_balance_history_path}        {currency}       ${arg_dic.currency}    1

     ${response}             get request
         ...     api
         ...     ${system_user_get_company_agent_cash_balance_history_path}
         ...     ${header}

     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API user get all his/her source of fund
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

         create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${user_get_list_sof_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API user get his/her bank source of fund
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

         create session          api     ${api_gateway_host}     disable_warnings=1
     ${bank_sof_id}     convert to string       ${arg_dic.bank_sof_id}
     ${user_get_bank_sof_by_sof_id_path}=    replace string      ${user_get_bank_sof_by_sof_id_path}        {bank_sof_id}       ${bank_sof_id}    1
     ${response}             get request
         ...     api
         ...     ${user_get_bank_sof_by_sof_id_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API user can link bank by bank
    [Arguments]    ${arg_dic}
    ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate
        ...     {
        ...         "user_id":"${arg_dic.user_id}",
        ...         "user_type_id":"${arg_dic.user_type_id}",
        ...         "bank_id":"${arg_dic.bank_id}",
        ...         "bank_account_name":"${arg_dic.bank_account_name}",
        ...         "bank_account_number":"${arg_dic.bank_account_number}",
        ...         "currency":"${arg_dic.currency}",
        ...         "ext_bank_reference":"${arg_dic.ext_bank_reference}"
        ...     }
        create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${user_can_link_bank_by_bank_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
     log        ${data}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API user can unlink bank by bank
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

     ${bank_sof_id}      convert to string       ${arg_dic.bank_sof_id}
     ${admin_unlink_bank_by_bank_path}=    replace string      ${admin_unlink_bank_by_bank_path}        {bank_sof_id}        ${bank_sof_id}    1
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             delete request
         ...     api
         ...     ${admin_unlink_bank_by_bank_path}
         ...     ${NONE}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API user can unlink bank by themselves
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

     ${bank_sof_id}      convert to string       ${arg_dic.bank_sof_id}
     ${user_unlink_bank_themself_path}=    replace string      ${user_unlink_bank_themself_path}        {bank_sof_id}        ${bank_sof_id}    1
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             delete request
         ...     api
         ...     ${user_unlink_bank_themself_path}
         ...     ${NONE}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API user get all customer bank source of fund
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

         create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${user_get_bank_sof_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API verify (pre-link) card information
    [Arguments]
    ...     ${access_token}
    ...     ${card_account_name}
    ...     ${card_account_number}
    ...     ${citizen_id}
    ...     ${cvv_cvc}
    ...     ${pin}
    ...     ${expiry_date}
    ...     ${issue_date}
    ...     ${mobile_number}
    ...     ${billing_address}
    ...     ${card_type}
    ...     ${currency}
    ...     ${header_client_id}
    ...     ${header_client_secret}

    ${header}               create dictionary
        ...        client_id=${header_client_id}
        ...        client_secret=${header_client_secret}
        ...        content-type=application/json
        ...        Authorization=Bearer ${access_token}
    ${data}              catenate
     ...    {
     ...        "card_account_name":"${card_account_name}",
     ...        "card_account_number":"${card_account_number}",
     ...        "citizen_id":"${citizen_id}",
     ...        "cvv_cvc":"${cvv_cvc}",
     ...        "pin": "${pin}",
     ...        "expiry_date":"${expiry_date}",
     ...        "issue_date":"${issue_date}",
     ...        "mobile_number":"${mobile_number}",
     ...        "billing_address":"${billing_address}",
     ...        "card_type": "${card_type}",
     ...        "currency":"${currency}"
     ...    }
        create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${verify_(pre-link)_card_information_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API user link card source of fund
    [Arguments]
    ...     ${access_token}
    ...     ${card_account_name}
    ...     ${card_account_number}
    ...     ${citizen_id}
    ...     ${cvv_cvc}
    ...     ${pin}
    ...     ${expiry_date}
    ...     ${issue_date}
    ...     ${mobile_number}
    ...     ${billing_address}
    ...     ${currency}
    ...     ${security_code}
    ...     ${security_ref}
    ...     ${header_client_id}
    ...     ${header_client_secret}

    ${header}               create dictionary
        ...        client_id=${header_client_id}
        ...        client_secret=${header_client_secret}
        ...        content-type=application/json
        ...        Authorization=Bearer ${access_token}
    ${data}              catenate
     ...    {
     ...        "card_account_name":"${card_account_name}",
     ...        "card_account_number":"${card_account_number}",
     ...        "citizen_id":"${citizen_id}",
     ...        "cvv_cvc":"${cvv_cvc}",
     ...        "pin": "${pin}",
     ...        "expiry_date":"${expiry_date}",
     ...        "issue_date":"${issue_date}",
     ...        "mobile_number":"${mobile_number}",
     ...        "billing_address":"${billing_address}",
     ...        "currency":"${currency}",
     ...    	"security": {
     ...	    "code": "${security_code}",
     ...   	    "ref":"${security_ref}"
	 ...         }
	 ...        }


        create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${user_link_card_source_of_fund_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API user get list of his/her linked card source of fund detail
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

         create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${user_get_all_sof_card_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API user get his/her linked card source of fund detail by sof ID
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
     ${card_sof_id}      convert to string       ${arg_dic.card_sof_id}
     ${user_get_sof_card_by_sof_id_path}=    replace string      ${user_get_sof_card_by_sof_id_path}        {card_sof_id}        ${card_sof_id}    1

         create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${user_get_sof_card_by_sof_id_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API user delete (un-link) his/her linked card source of fund
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
     ${card_sof_id}      convert to string       ${arg_dic.card_sof_id}
     ${user_unlink_card_by_sof_id_path}=    replace string      ${user_unlink_card_by_sof_id_path}        {card_sof_id}        ${card_sof_id}    1

         create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             delete request
         ...     api
         ...     ${user_unlink_card_by_sof_id_path}
         ...     ${NONE}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API add company balance
    [Arguments]
    ...     ${currency}
    ...     ${amount}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ...     ${access_token}
    ${add_company_balance_path_edit}=    replace string      ${system_user_update_company_agent_cash_balance_path}   {currency}   ${currency}
    ${correlation_id}       generate random string  20  [UPPER]
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${data}              catenate
        ...     {
        ...     	"amount":"${amount}"
        ...     }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${add_company_balance_path_edit}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API apply discount code
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${data}              catenate
        ...     {
        ...         "voucher_id":"${arg_dic.voucher_id}"
        ...     }
    ${apply_discount_code_path}    replace string    ${apply_discount_code_path}    {order_id}   ${arg_dic.order_id}    1
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${apply_discount_code_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${data}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API call execute order with security
    [Arguments]         ${execute_order_token}        ${order_id}       ${code}     ${type_id}      ${type_name}
    ${header}               create dictionary
        ...        client_id=${setup_admin_client_id}
        ...        client_secret=${setup_admin_client_secret}
        ...        content-type=application/json
        ...        Authorization=Bearer ${execute_order_token}

    ${path}=    replace string      ${get_order_detail_path}    {order_id}    ${order_id}

    ${request}              catenate
        ...    {
        ...     "security": {
        ...            "code":"${code}",
        ...            "ref":null,
        ...            "type":{
        ...                 "id":${type_id},
        ...                 "name":"${type_name}"
        ...            }
        ...         }
        ...     }
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${path}
        ...     ${request}
        ...     ${NONE}
        ...     ${header}

    log  ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create sof service balance limitation
    [Arguments]     ${arg_dic}
    ${param_maximum_amount}=  [Common] - Create string param in dic    ${arg_dic}     maximum_amount
    ${param_minimum_amount}=  [Common] - Create string param in dic    ${arg_dic}     minimum_amount
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${data}              catenate       SEPARATOR=
        ...     {
        ...         ${param_maximum_amount}
        ...         ${param_minimum_amount}
        ...     }
    ${data}   replace string      ${data}      ,}    }

    ${service_id_str}     convert to string       ${arg_dic.service_id}
    ${sof_id_str}         convert to string       ${arg_dic.sof_id}
    ${data}               replace string          ${data}     , }        }

    ${create_sof_service_balance_limitation_path}    replace string    ${create_sof_service_balance_limitation_path}    {service_id}   ${service_id_str}    1
    ${create_sof_service_balance_limitation_path}    replace string    ${create_sof_service_balance_limitation_path}    {sof_id}   ${sof_id_str}    1
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_sof_service_balance_limitation_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${data}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get tier config allow zero
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_tier_cofig_allow_zero}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete sof service balance limitation
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${data}              catenate
        ...     {
        ...         "sof_service_balance_limitation": ${arg_dic.list_sof_service_balance_limitation}
        ...     }

    ${data}     replace string          ${data}     u'        ${empty}
    ${data}     replace string          ${data}     }'        }
    ${data}     replace string          ${data}     }',        },
    ${data}     replace string      ${data}     :,[{        :[{
    ${data}     replace string      ${data}     [,        [
    ${data}     replace string          ${data}     ,]        ]
    ${data}     replace string          ${data}     \\'s        's
                        create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             delete request
        ...     api
        ...     ${delete_sof_service_balance_limitation_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${data}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create normal order
    [Arguments]    ${arg_dic}

    ${access_token}=                    get from dictionary    ${arg_dic}   access_token
    ${ext_transaction_id}=              get from dictionary    ${arg_dic}   ext_transaction_id
    ${product_service_id}=              get from dictionary    ${arg_dic}   product_service_id
    ${product_service_name}=            get from dictionary    ${arg_dic}   product_service_name
    ${product_name}=                    get from dictionary    ${arg_dic}   product_name
    ${product_ref_1}=                   get from dictionary    ${arg_dic}   product_ref_1
    ${product_ref_2}=                   get from dictionary    ${arg_dic}   product_ref_2
    ${product_ref_3}=                   get from dictionary    ${arg_dic}   product_ref_3
    ${product_ref_4}=                   get from dictionary    ${arg_dic}   product_ref_4
    ${product_ref_5}=                   get from dictionary    ${arg_dic}   product_ref_5
    ${payer_user_ref_type}=             get from dictionary    ${arg_dic}   payer_user_ref_type
    ${payer_user_ref_value}=            get from dictionary    ${arg_dic}   payer_user_ref_value
    ${payer_user_id}=                   get from dictionary    ${arg_dic}   payer_user_id
    ${payer_user_type}=                 get from dictionary    ${arg_dic}   payer_user_type
    ${payee_user_ref_type}=             get from dictionary    ${arg_dic}   payee_user_ref_type
    ${payee_user_ref_value}=            get from dictionary    ${arg_dic}   payee_user_ref_value
    ${payee_user_id}=                   get from dictionary    ${arg_dic}   payee_user_id
    ${payee_user_type}=                 get from dictionary    ${arg_dic}   payee_user_type
#    ${requestor_user_sof_id}=           get from dictionary    ${arg_dic}   requestor_user_sof_id
#    ${requestor_user_sof_type_id}=      get from dictionary    ${arg_dic}   requestor_user_sof_type_id
#    ${requestor_user_id}=               get from dictionary    ${arg_dic}   requestor_user_id
#    ${requestor_user_type}=             get from dictionary    ${arg_dic}   requestor_user_type
    ${amount}=                          get from dictionary    ${arg_dic}   amount
    ${header_client_id}=                get from dictionary    ${arg_dic}   setup_admin_client_id
    ${header_client_secret}=            get from dictionary    ${arg_dic}   setup_admin_client_secret
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${access_token}
    ${payer_user_ref_param}    create json data for user ref object        ${payer_user_ref_type}      ${payer_user_ref_value}
    ${payer_user_id_param}     create json data for user id object         ${payer_user_id}      ${payer_user_type}
    ${payee_user_ref_param}    create json data for user ref object         ${payee_user_ref_type}      ${payee_user_ref_value}
    ${payee_user_id_param}     create json data for user id object         ${payee_user_id}      ${payee_user_type}
    ${param_requestor_user_id}         [Common] - Create string param of object in dic    ${arg_dic}    requestor_user        user_id
    ${param_requestor_user_type_name}  [Common] - Create string param of object in dic    ${arg_dic}    requestor_user        user_type
    ${param_requestor_sof_id}          [Common] - Create string param of object in dic    ${arg_dic}    requestor_sof    id
    ${param_requestor_sof_type_id}     [Common] - Create string param of object in dic    ${arg_dic}    requestor_sof    type_id

    ${request}              catenate       SEPARATOR=
        ...    {
        ...     "ext_transaction_id": "${ext_transaction_id}",
        ...     "product_service": {
        ...         "id": ${product_service_id},
        ...         "name": "${product_service_name}"
        ...         },
        ...     "product": {
        ...         "product_name": "${product_name}",
        ...         "product_ref1": "${product_ref_1}",
        ...         "product_ref2": "${product_ref_2}",
        ...         "product_ref3": "${product_ref_3}",
        ...         "product_ref4": "${product_ref_4}",
        ...         "product_ref5": "${product_ref_5}"
        ...         },
        ...     "payer_user": {
        ...         ${payer_user_ref_param},
        ...         ${payer_user_id_param}
        ...     },
        ...     "payee_user": {
        ...         ${payee_user_ref_param}
        ...         ${payee_user_id_param}
        ...     },
        ...     "requestor_user": {
        ...         ${param_requestor_user_id}
        ...         ${param_requestor_user_type_name}
        ...         "sof": {
        ...             ${param_requestor_sof_id}
        ...             ${param_requestor_sof_type_id}
        ...         }
        ...     },
        ...     "amount": ${amount}
        ...     }

    ${request}  replace string      ${request}      ,}    }
    ${request}  replace string      ${request}      },,    },
                create session          api     ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${payment_create_normal_order_path}
        ...     ${request}
        ...     ${NONE}
        ...     ${header}
    log    ${request}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API create artifact order
    [Arguments]    ${arg_dic}

    ${access_token}=                    get from dictionary    ${arg_dic}   access_token
    ${ext_transaction_id}=              get from dictionary    ${arg_dic}   ext_transaction_id
    ${product_service_id}=              get from dictionary    ${arg_dic}   product_service_id
    ${product_service_name}=            get from dictionary    ${arg_dic}   product_service_name
    ${product_name}=                    get from dictionary    ${arg_dic}   product_name
    ${product_ref_1}=                   get from dictionary    ${arg_dic}   product_ref_1
    ${product_ref_2}=                   get from dictionary    ${arg_dic}   product_ref_2
    ${product_ref_3}=                   get from dictionary    ${arg_dic}   product_ref_3
    ${product_ref_4}=                   get from dictionary    ${arg_dic}   product_ref_4
    ${product_ref_5}=                   get from dictionary    ${arg_dic}   product_ref_5
    ${payer_user_ref_type}=             get from dictionary    ${arg_dic}   payer_user_ref_type
    ${payer_user_ref_value}=            get from dictionary    ${arg_dic}   payer_user_ref_value
#    ${payer_user_id}=                   get from dictionary    ${arg_dic}   payer_user_id
#    ${payer_user_type}=                 get from dictionary    ${arg_dic}   payer_user_type
    ${payee_user_ref_type}=             get from dictionary    ${arg_dic}   payee_user_ref_type
    ${payee_user_ref_value}=            get from dictionary    ${arg_dic}   payee_user_ref_value
    ${payee_user_id}=                   get from dictionary    ${arg_dic}   payee_user_id
    ${payee_user_type}=                 get from dictionary    ${arg_dic}   payee_user_type
    ${amount}=                          get from dictionary    ${arg_dic}   amount
    ${header_client_id}=                get from dictionary    ${arg_dic}   setup_admin_client_id
    ${header_client_secret}=            get from dictionary    ${arg_dic}   setup_admin_client_secret

    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}

    ${payer_user_ref_param}    create json data for user ref object        ${payer_user_ref_type}      ${payer_user_ref_value}
#    ${payer_user_id_param}     create json data for user id object         ${payer_user_id}      ${payer_user_type}
    ${payee_user_ref_param}    create json data for user ref object         ${payee_user_ref_type}      ${payee_user_ref_value}
    ${payee_user_id_param}     create json data for user id object         ${payee_user_id}      ${payee_user_type}
    ${param_requestor_user_id}         [Common] - Create string param of object in dic    ${arg_dic}    requestor_user        user_id
    ${param_requestor_user_type_name}  [Common] - Create string param of object in dic    ${arg_dic}    requestor_user        user_type
    ${param_requestor_sof_id}          [Common] - Create string param of object in dic    ${arg_dic}    requestor_user_sof    id
    ${param_requestor_sof_type_id}     [Common] - Create string param of object in dic    ${arg_dic}    requestor_user_sof    type_id

    ${data_json}    set variable    ${empty}
    ${request}              catenate       SEPARATOR=
        ...    {
        ...     "ext_transaction_id": "${ext_transaction_id}",
        ...     "product_service": {
        ...         "id": ${product_service_id},
        ...         "name": "${product_service_name}"
        ...         },
        ...     "product": {
        ...         "product_name": "${product_name}",
        ...         "product_ref1": "${product_ref_1}",
        ...         "product_ref2": "${product_ref_2}",
        ...         "product_ref3": "${product_ref_3}",
        ...         "product_ref4": "${product_ref_4}",
        ...         "product_ref5": "${product_ref_5}"
        ...         },
        ...     "payer_user": {
        ...         ${payer_user_ref_param}
#        ...         ${payer_user_id_param}
        ...     },
        ...     "payee_user": {
        ...         ${payee_user_id_param}
        ...     },
        ...     "requestor_user": {
        ...         ${param_requestor_user_id}
        ...         ${param_requestor_user_type_name}
        ...         "sof": {
        ...             ${param_requestor_sof_id}
        ...             ${param_requestor_sof_type_id}
        ...         }
        ...     },
        ...     "amount": ${amount}
        ...     }
    ${request}  replace string      ${request}      ,}    }
    ${request}  replace string      ${request}      },,    },
                create session          api     ${api_gateway_host}

    log         ${request}
    ${response}             post request
        ...     api
        ...     ${payment_create_artifact_order_path}
        ...     ${request}
        ...     ${NONE}
        ...     ${header}
    log    ${request}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API apply tier level mask to an order
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${data}              catenate
    ...     {
    ...     	"tier_level_masks": [
    ...     		${arg_dic.tier_level_masks}
    ...     	]
    ...     }

    ${order_id}     convert to string       ${arg_dic.order_id}
    ${apply_tier_level_mask_to_an_order_path}    replace string    ${apply_tier_level_mask_to_an_order_path}    {order_id}   ${order_id}    1
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${apply_tier_level_mask_to_an_order_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${data}
    log        ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API create service limitation
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

     ${param_equation_id}         [Common] - Create int param in dic    ${arg_dic}         equation_id

     ${data}              catenate
         ...     {
         ...         "conditions":${arg_dic.conditions},
         ...         "limitation_type":"${arg_dic.limitation_type}",
         ...         ${param_equation_id}
         ...         "limitation_value":"${arg_dic.limitation_value}",
         ...         "reset_time_block_value":${arg_dic.reset_time_block_value},
         ...         "reset_time_block_unit":"${arg_dic.reset_time_block_unit}",
         ...         "start_effective_timestamp":"${arg_dic.start_effective_timestamp}",
         ...         "end_effective_timestamp":"${arg_dic.end_effective_timestamp}"
         ...     }

     ${data}     replace string          ${data}     u'        ${empty}
     ${data}     replace string          ${data}     }'        }
     ${data}     replace string          ${data}     }',        },
     ${data}     replace string      ${data}     :,[{        :[{
     ${data}     replace string      ${data}     [,        [
     ${data}     replace string          ${data}     ,]        ]
     ${data}     replace string          ${data}     \\'s        's

     ${service_id}      convert to string       ${arg_dic.service_id}
     ${create_service_limitation_path}=    replace string      ${create_service_limitation_path}        {service_id}        ${service_id}    1
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             post request
         ...     api
         ...     ${create_service_limitation_path}
         ...     ${data}
         ...     ${NONE}
         ...     ${header}
     log        ${data}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete service limitation
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

     ${service_limitation_id}      convert to string       ${arg_dic.service_limitation_id}
     ${delete_service_limitation_path}=    replace string      ${delete_service_limitation_path}        {service_limitation_id}        ${service_limitation_id}    1
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             post request
         ...     api
         ...     ${delete_service_limitation_path}
         ...     ${NONE}
         ...     ${NONE}
         ...     ${header}

     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create tier mask for service
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_requestor_user_type}        [Common] - Create string param of object in dic    ${arg_dic}    requestor   user_type
    ${param_requestor_type_id}        [Common] - Create int param of object in dic    ${arg_dic}    requestor   type_id
    ${param_requestor_classification_id}        [Common] - Create int param of object in dic    ${arg_dic}    requestor   classification_id
    ${param_requestor_postal_code}        [Common] - Create string param of object in dic    ${arg_dic}    requestor   postal_code

    ${param_initiator_user_type}        [Common] - Create string param of object in dic    ${arg_dic}    initiator   user_type
    ${param_initiator_type_id}        [Common] - Create int param of object in dic    ${arg_dic}    initiator   type_id
    ${param_initiator_classification_id}        [Common] - Create int param of object in dic    ${arg_dic}    initiator   classification_id
    ${param_initiator_postal_code}        [Common] - Create string param of object in dic    ${arg_dic}    initiator   postal_code

    ${param_payer_user_type}        [Common] - Create string param of object in dic    ${arg_dic}    payer   user_type
    ${param_payer_type_id}        [Common] - Create int param of object in dic    ${arg_dic}    payer   type_id
    ${param_payer_classification_id}        [Common] - Create int param of object in dic    ${arg_dic}    payer   classification_id
    ${param_payer_postal_code}        [Common] - Create string param of object in dic    ${arg_dic}    payer   postal_code

    ${param_payee_user_type}        [Common] - Create string param of object in dic    ${arg_dic}    payee   user_type
    ${param_payee_type_id}        [Common] - Create int param of object in dic    ${arg_dic}    payee   type_id
    ${param_payee_classification_id}        [Common] - Create int param of object in dic    ${arg_dic}    payee   classification_id
    ${param_payee_postal_code}        [Common] - Create string param of object in dic    ${arg_dic}    payee   postal_code

    ${data}              catenate    SEPARATOR=
    ...     {
    ...    	"name":"${arg_dic.tier_mask_name}",
    ...        "requestor":  {
    ...    		${param_requestor_user_type}
    ...    		${param_requestor_type_id}
    ...    		${param_requestor_classification_id}
    ...    		${param_requestor_postal_code}
    ...    	},
    ...        "initiator":  {
    ...    		${param_initiator_user_type}
    ...    		${param_initiator_type_id}
    ...    		${param_initiator_classification_id}
    ...    		${param_initiator_postal_code}
    ...    	},
    ...        "payer":  {
    ...    		${param_payer_user_type}
    ...    		${param_payer_type_id}
    ...    		${param_payer_classification_id}
    ...    		${param_payer_postal_code}
    ...    	},
    ...        "payee":  {
    ...    		${param_payee_user_type}
    ...    		${param_payee_type_id}
    ...    		${param_payee_classification_id}
    ...    		${param_payee_postal_code}
    ...    	},
    ...    	"from_effective_timestamp":"${arg_dic.from_effective_timestamp}",
    ...     "to_effective_timestamp": "${arg_dic.to_effective_timestamp}"
    ...     }
    ${data}   replace string      ${data}      ,}    }
    ${data}   replace string      ${data}      {}    null
    ${service_id}     convert to string       ${arg_dic.service_id}
    ${create_service_tier_mask_path}    replace string    ${create_service_tier_mask_path}    {service_id}   ${service_id}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_service_tier_mask_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${data}
    log        ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API create service fee tier mask
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_condition_amount}     [Common] - Create int param in dic   ${arg_dic}      condition_amount
    ${param_amount_type}     [Common] - Create string param in dic   ${arg_dic}      amount_type
    ${data}              catenate    SEPARATOR=
    ...    {
    ...    	"fee_tier_id": ${arg_dic.fee_tier_id},
    ...    	"command_id": ${arg_dic.command_payment_id},
    ...    	"command_name": "${arg_dic.command_payment_name}",
    ...    	"fee_tier_condition":"${arg_dic.fee_tier_condition}",
    ...     ${param_amount_type}
    ...    	${param_condition_amount}
    ...     ${arg_dic.label}
    ...    }
    ${data}   replace string      ${data}      ,}    }
    ${data}   replace string      ${data}      ,,    ,
    ${service_id}     convert to string       ${arg_dic.service_id}
    ${service_tier_mask_id}     convert to string       ${arg_dic.service_tier_mask_id}
    ${payment_create_service_fee_tier_mask_path}    replace string    ${payment_create_service_fee_tier_mask_path}    {service_id}   ${service_id}
    ${payment_create_service_fee_tier_mask_path}    replace string    ${payment_create_service_fee_tier_mask_path}    {service_tier_mask_id}   ${service_tier_mask_id}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${payment_create_service_fee_tier_mask_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${data}
    log        ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API create pending settlement
    [Arguments]     &{arg_dic}
        ${header}   catenate
        ...     {
        ...         "client_id": "${arg_dic.client_id}",
        ...         "client_secret": "${arg_dic.client_secret}",
        ...         "content-type": "application/json",
        ...         "Authorization": "Bearer ${arg_dic.access_token}"
        ...     }
    ${param_description}                     [Common] - Create string param in dic   ${arg_dic}      description
    ${param_created_by}                      [Common] - Create string param in dic   ${arg_dic}      created_by
    ${param_currency}                        [Common] - Create string param in dic   ${arg_dic}      currency
    ${param_due_date}                        [Common] - Create string param in dic   ${arg_dic}      due_date
    ${param_reconciliation_from_date}        [Common] - Create string param in dic   ${arg_dic}      reconciliation_from_date
    ${param_reconciliation_to_date}          [Common] - Create string param in dic   ${arg_dic}      reconciliation_to_date

    ${param_amount}                          [Common] - Create int param in dic   ${arg_dic}      amount
    ${param_reconciliation_id}               [Common] - Create int param in dic   ${arg_dic}      reconciliation_id

    ${param_payer_user_type}                 [Common] - Create string param of object in dic   ${arg_dic}      payer       user_type
    ${param_payee_user_type}                 [Common] - Create string param of object in dic   ${arg_dic}      payee       user_type

    ${param_payer_user_id}                   [Common] - Create int param of object in dic      ${arg_dic}      payer       user_id
    ${param_payer_sof_id}                    [Common] - Create int param of object in dic      ${arg_dic}      payer_sof   id
    ${param_payer_sof_type_id}               [Common] - Create int param of object in dic      ${arg_dic}      payer_sof   type_id
    ${param_payee_user_id}                   [Common] - Create int param of object in dic      ${arg_dic}      payee       user_id
    ${param_payee_sof_id}                    [Common] - Create int param of object in dic      ${arg_dic}      payee_sof   id
    ${param_payee_sof_type_id}               [Common] - Create int param of object in dic      ${arg_dic}      payee_sof   type_id

    ${data}              catenate    SEPARATOR=
    ...    {
    ...    	${param_description}
    ...    	${param_amount}
    ...    	"payer": {
    ...            ${param_payer_user_id}
    ...            ${param_payer_user_type}
    ...            "sof": {
    ...                    ${param_payer_sof_id}
    ...                    ${param_payer_sof_type_id}
    ...                   }
    ...              },
    ...    	"payee": {
    ...            ${param_payee_user_id}
    ...            ${param_payee_user_type}
    ...            "sof": {
    ...                    ${param_payee_sof_id}
    ...                    ${param_payee_sof_type_id}
    ...                   }
    ...              },
    ...     ${param_created_by}
    ...    	${param_currency}
    ...    	${param_due_date}
    ...    	${param_reconciliation_id}
    ...    	${param_reconciliation_from_date}
    ...    	${param_reconciliation_to_date}
    ...    	"unsettled_transactions": ${arg_dic.unsettled_transactions}
    ...    }
     ${data}     replace string          ${data}     u'        ${empty}
     ${data}     replace string          ${data}     }'        }
     ${data}     replace string          ${data}     }',        },
     ${data}     replace string          ${data}     :,[{        :[{
     ${data}     replace string          ${data}     [,        [
     ${data}     replace string          ${data}     ,]        ]
     ${data}     replace string          ${data}     \\'s        's
     ${data}     replace string          ${data}     ,}        }

                            create session          api     ${api_gateway_host}     disable_warnings=1
    REST.post   ${api_gateway_host}/${create_pending_settlement_path}
        ...     body=${data}
        ...     headers=${header}
    rest extract

API update pending settlement
    [Arguments]     &{arg_dic}
        ${header}   catenate
        ...     {
        ...         "client_id": "${arg_dic.client_id}",
        ...         "client_secret": "${arg_dic.client_secret}",
        ...         "content-type": "application/json",
        ...         "Authorization": "Bearer ${arg_dic.access_token}"
        ...     }
    ${data}              catenate    SEPARATOR=
    ...    {
    ...           "due_date":"${arg_dic.due_date}"
    ...    }
    ${data}     replace string          ${data}     u'        ${empty}
    ${data}     replace string          ${data}     }'        }
    ${data}     replace string          ${data}     }',        },
    ${data}     replace string          ${data}     :,[{        :[{
    ${data}     replace string          ${data}     [,        [
    ${data}     replace string          ${data}     ,]        ]
    ${data}     replace string          ${data}     \\'s        's
    ${data}     replace string          ${data}     ,}        }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${settlement_id}     convert to string              ${arg_dic.settlement_id}
    ${update_pending_settlement_path}    replace string      ${update_pending_settlement_path}        {settlement_id}        ${settlement_id}    1
    REST.put   ${api_gateway_host}/${update_pending_settlement_path}
        ...     body=${data}
        ...     headers=${header}
    rest extract

API delete pending settlement
    [Arguments]     &{arg_dic}
        ${header}   catenate
        ...     {
        ...         "client_id": "${arg_dic.client_id}",
        ...         "client_secret": "${arg_dic.client_secret}",
        ...         "content-type": "application/json",
        ...         "Authorization": "Bearer ${arg_dic.access_token}"
        ...     }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${settlement_id}     convert to string              ${arg_dic.settlement_id}
    ${delete_pending_settlement_path}    replace string      ${delete_pending_settlement_path}        {settlement_id}        ${settlement_id}    1
    REST.delete   ${api_gateway_host}/${delete_pending_settlement_path}
        ...     headers=${header}
    rest extract


API system user create settlement configuration
    [Arguments]     &{arg_dic}
    ${param_currency}=  [Common] - Create string param in dic    ${arg_dic}     currency
    ${param_service_group_id}=  [Common] - Create int param in dic    ${arg_dic}     service_group_id
    ${param_service_id}=  [Common] - Create int param in dic    ${arg_dic}     service_id
    ${header}   catenate
        ...     {
        ...         "client_id": "${arg_dic.client_id}",
        ...         "client_secret": "${arg_dic.client_secret}",
        ...         "content-type": "application/json",
        ...         "Authorization": "Bearer ${arg_dic.access_token}"
        ...     }
    ${data}              catenate    SEPARATOR=
        ...    {
        ...    	    ${param_service_group_id}
        ...    	    ${param_service_id}
        ...    	    ${param_currency}
        ...    }
    ${data}   replace string      ${data}      ,}    }
    REST.post   ${api_gateway_host}/${payment_system_user_create_settlement_configuration_path}
        ...     body=${data}
        ...     headers=${header}
    rest extract

API system user resolve pending settlement
    [Arguments]     &{arg_dic}
        ${header}   catenate
        ...     {
        ...         "client_id": "${arg_dic.client_id}",
        ...         "client_secret": "${arg_dic.client_secret}",
        ...         "content-type": "application/json",
        ...         "Authorization": "Bearer ${arg_dic.access_token}"
        ...     }
    ${settlement_id}      convert to string       ${arg_dic.settlement_id}
    ${payment_system_user_resolve_pending_settlement_path}    replace string    ${payment_system_user_resolve_pending_settlement_path}    {settlement_id}   ${settlement_id}
                create session          api     ${api_gateway_host}     disable_warnings=1
    REST.post   ${api_gateway_host}/${payment_system_user_resolve_pending_settlement_path}
        ...     headers=${header}
    rest extract