*** Settings ***
Resource        ../../1_common/keywords.robot


*** Keywords ***
API post bulk file
    [Arguments]
    ...     ${access_token}
    ...     ${file_id}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		"file_id":${file_id}
        ...	}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${post_bulk_file_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update bulk file
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${function_id}    convert to integer      ${arg_dic.function_id}
    ${file_data}  Get Binary File  ${arg_dic.file_path}
    ${files}  Create Dictionary  file_data  ${file_data}
    ${data}  Create Dictionary  function_id  ${function_id}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${upload_bulk_file_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
        ...     ${files}
    log   ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search functions
    [Arguments]
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_functions_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log   ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API download bulk file
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		"file_id":${arg_dic.file_id},
        ...		"status_id":${arg_dic.status_id}
        ...	}
    create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${download_bulk_file_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log   ${data}
    log   ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}
