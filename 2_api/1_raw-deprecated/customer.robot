*** Settings ***
Resource        ../../1_common/keywords.robot



*** Keywords ***

API create customer classification
    [Arguments]
    ...     ${client_id}
    ...     ${client_secret}
    ...     ${access_token}
    ...     ${classification_name}
    ...     ${classification_desc}
    ${header}               create dictionary
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${data}              catenate
        ...	{
        ...     "name":"${classification_name}",
        ...     "description":"${classification_desc}"
        ...	}
    create session          api        ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${customer_classification_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${data}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update customer classification
    [Arguments]     ${arg_dic}
    ${param_name}=  [Common] - Create string param     name        ${arg_dic.name}
    ${param_description}=  [Common] - Create string param     description        ${arg_dic.description}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_name}
        ...		${param_description}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${customer_classification_id}=    convert to string   ${arg_dic.customer_classification_id}
    ${update_customer_classification_path}=    replace string      ${update_customer_classification_path}      {customer_classification_id}        ${customer_classification_id}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${update_customer_classification_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete customer classification
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${customer_classification_id}=    convert to string   ${arg_dic.customer_classification_id}
    ${delete_customer_classification_path}=    replace string      ${delete_customer_classification_path}      {customer_classification_id}        ${customer_classification_id}   1
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${delete_customer_classification_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API get customer profiles
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${header}   [Common] - add new param in header   ${arg_dic}    device_id   ${header}

                create session          api     ${api_gateway_host}     disable_warnings=1

    ${response}             get request
        ...     api
        ...     ${get_customer_path}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API active-suspend customer
    [Arguments]
    ...     ${client_id}
    ...     ${client_secret}
    ...     ${access_token}
    ...     ${is_suspended}
    ...     ${customer_id}
    ${header}               create dictionary
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${data}              catenate
        ...	{
        ...     "is_suspended":${is_suspended}
        ...	}

    ${customer_id}=    convert to string   ${customer_id}
    ${active-suspend_customer_path}=    replace string        ${active-suspend_customer_path}       {customer_id}         ${customer_id}
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${active-suspend_customer_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all customer identities
    [Arguments]
    ...     ${client_id}
    ...     ${client_secret}
    ...     ${access_token}

    ${header}               create dictionary
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_all_customer_identities_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${response_text}=      set variable  ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}


#API register customer olddd
#    [Arguments]
#        ...     ${username}
#        ...     ${password}
#        ...     ${identity_type_id}
#        ...     ${access_token}
#        ...     ${mobile_number}
#        ...     ${unique_reference}
#        ...     ${firstname}
#        ...     ${middle_name}
#        ...     ${lastname}
#        ...     ${place_of_birth}
#        ...     ${date_of_birth}
#        ...     ${citizen_card_id}
#        ...     ${passport_id}
#        ...     ${citizen_card_date_of_issue}
#        ...     ${passport_date_of_issue}
#        ...     ${gender}
#        ...     ${profile_picture_url}
#        ...     ${tax_id}
#        ...     ${social_security_id}
#        ...     ${permanent_address}
#        ...     ${permanent_city}
#        ...     ${permanent_province}
#        ...     ${permanent_district}
#        ...     ${permanent_commune}
#        ...     ${permanent_country}
#        ...     ${permanent_landmark}
#        ...     ${permanent_latitude}
#        ...     ${permanent_longitude}
#        ...     ${current_address}
#        ...     ${current_city}
#        ...     ${current_province}
#        ...     ${current_district}
#        ...     ${current_commune}
#        ...     ${current_country}
#        ...     ${current_landmark}
#        ...     ${current_latitude}
#        ...     ${current_longitude}
#        ...     ${email}
#        ...     ${occupations}
#        ...     ${nationality}
#        ...     ${beneficiary}
#        ...     ${kyc_status}
#        ...     ${kyc_remark}
#        ...     ${client_id}
#        ...     ${client_secret}
#    ${param_username}=  [Common] - Create string param    username        ${username}
#    ${param_password}=  [Common] - Create string param    password        ${password}
#    ${param_identity_type_id}=  [Common] - Create string param    identity_type_id        ${identity_type_id}
#    ${param_mobile_number}=  [Common] - Create string param    mobile_number        ${mobile_number}
#    ${param_unique_reference}=  [Common] - Create string param      unique_reference        ${unique_reference}
#    ${param_firstname}=  [Common] - Create string param     firstname        ${firstname}
#    ${param_middle_name}=  [Common] - Create string param     middle_name        ${middle_name}
#    ${param_lastname}=  [Common] - Create string param     lastname        ${lastname}
#    ${param_place_of_birth}=  [Common] - Create string param     place_of_birth        ${place_of_birth}
#    ${param_date_of_birth}=  [Common] - Create string param     date_of_birth        ${date_of_birth}
#    ${param_citizen_card_id}=  [Common] - Create string param     citizen_card_id        ${citizen_card_id}
#    ${param_passport_id}=  [Common] - Create string param     passport_id        ${passport_id}
#    ${param_citizen_card_date_of_issue}=  [Common] - Create string param     citizen_card_date_of_issue        ${citizen_card_date_of_issue}
#    ${param_passport_date_of_issue}=  [Common] - Create string param     passport_date_of_issue        ${passport_date_of_issue}
#    ${param_gender}=  [Common] - Create string param     gender        ${gender}
#    ${param_profile_picture_url}=  [Common] - Create string param     profile_picture_url        ${profile_picture_url}
#    ${param_tax_id}=  [Common] - Create string param     tax_id        ${tax_id}
#    ${param_social_security_id}=  [Common] - Create string param     social_security_id        ${social_security_id}
#
#    ${param_permanent_address}=  [Common] - Create string param     permanent_address        ${permanent_address}
#    ${param_permanent_city}=  [Common] - Create string param     permanent_city        ${permanent_city}
#    ${param_permanent_province}=  [Common] - Create string param     permanent_province        ${permanent_province}
#    ${param_permanent_district}=  [Common] - Create string param     permanent_district        ${permanent_district}
#    ${param_permanent_commune}=  [Common] - Create string param     permanent_commune        ${permanent_commune}
#    ${param_permanent_country}=  [Common] - Create string param     permanent_country        ${permanent_country}
#    ${param_permanent_landmark}=  [Common] - Create string param     permanent_landmark        ${permanent_landmark}
#    ${param_permanent_latitude}=  [Common] - Create string param     permanent_latitude        ${permanent_latitude}
#    ${param_permanent_longitude}=  [Common] - Create string param     permanent_longitude        ${permanent_longitude}
#
#    ${param_current_address}=  [Common] - Create string param     current_address        ${permanent_address}
#    ${param_current_city}=  [Common] - Create string param     current_city        ${permanent_city}
#    ${param_current_province}=  [Common] - Create string param     current_province        ${permanent_province}
#    ${param_current_district}=  [Common] - Create string param     current_district        ${permanent_district}
#    ${param_current_commune}=  [Common] - Create string param     current_commune        ${permanent_commune}
#    ${param_current_country}=  [Common] - Create string param     current_country        ${permanent_country}
#    ${param_current_landmark}=  [Common] - Create string param     current_landmark        ${permanent_landmark}
#    ${param_current_latitude}=  [Common] - Create string param     current_latitude        ${permanent_latitude}
#    ${param_current_longitude}=  [Common] - Create string param     current_longitude        ${permanent_longitude}
#
#    ${param_email}=  [Common] - Create string param     email        ${email}
#    ${param_occupations}=  [Common] - Create string param     occupations        ${occupations}
#    ${param_nationality}=  [Common] - Create string param     nationality        ${nationality}
#    ${param_beneficiary}=  [Common] - Create string param     beneficiary        ${beneficiary}
#    ${param_kyc_status}=  [Common] - Create string param     kyc_status        ${kyc_status}
#    ${param_kyc_remark}=  [Common] - Create string param     kyc_remark        ${kyc_remark}
#
#    ${header}               create dictionary       SEPARATOR=
#        ...     client_id=${client_id}
#        ...     client_secret=${client_secret}
#        ...     content-type=application/json
#        ...     Authorization=Bearer ${access_token}
#    ${data}              catenate       SEPARATOR=
#        ...     {
#        ...     "profile":{
#        ...     ${param_mobile_number}
#        ...     ${param_unique_reference}
#        ...     ${param_firstname}
#        ...     ${param_middle_name}
#        ...     ${param_lastname}
#        ...     ${param_place_of_birth}
#        ...     ${param_date_of_birth}
#        ...     ${param_citizen_card_id}
#        ...     ${param_passport_id}
#        ...     ${param_citizen_card_date_of_issue}
#        ...     ${param_passport_date_of_issue}
#        ...     ${param_gender}
#        ...     ${param_profile_picture_url}
#        ...     ${param_tax_id}
#        ...     ${param_social_security_id}
#        ...     ${param_permanent_address}
#        ...     ${param_permanent_city}
#        ...     ${param_permanent_province}
#        ...     ${param_permanent_district}
#        ...     ${param_permanent_commune}
#        ...     ${param_permanent_country}
#        ...     ${param_permanent_landmark}
#        ...     ${param_permanent_latitude}
#        ...     ${param_permanent_longitude}
#        ...     ${param_current_address}
#        ...     ${param_current_city}
#        ...     ${param_current_province}
#        ...     ${param_current_district}
#        ...     ${param_current_commune}
#        ...     ${param_current_country}
#        ...     ${param_current_landmark}
#        ...     ${param_current_latitude}
#        ...     ${param_current_longitude}
#        ...     ${param_email}
#        ...     ${param_occupations}
#        ...     ${param_nationality}
#        ...     ${param_beneficiary}
#        ...     ${param_kyc_status}
#        ...     ${param_kyc_remark}
#        ...     },
#        ...     "identity":{
#        ...             ${param_username}
#        ...             ${param_password}
#        ...             ${param_identity_type_id}
#        ...         }
#        ...     }
#    ${data}  replace string      ${data}      ,}    }
#                create session          api        ${api_gateway_host}
#    ${response}             post request
#        ...     api
#        ...     ${register_customer}
#        ...     ${data}
#        ...     ${NONE}
#        ...     ${header}
#    log         ${response.text}
#    ${dic} =	Create Dictionary   response=${response}
#    [Return]    ${dic}

API add customer identities
    [Arguments]
    ...     ${identity_type_id}
    ...     ${username}
    ...     ${password}
    ...     ${access_token}
    ...     ${client_id}
    ...     ${client_secret}
    ${header}               create dictionary
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${data}              catenate
        ...	{
        ...     "identity_type_id":${identity_type_id},
        ...     "username":"${username}",
        ...     "password":"${password}"
        ...	}
                create session          api        ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${add_customer_identities_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API system user create customer
    [Arguments]
    ...     ${username}
    ...     ${password}
    ...     ${identity_type_id}
    ...     ${access_token}
    ...     ${primary_mobile_number}
    ...     ${unique_reference}
    ...     ${is_testing_account}
    ...     ${is_system_account}
    ...     ${acquisition_source}
    ...     ${customer_classification_id}
    ...     ${referrer_user_type_id}
    ...     ${referrer_user_type_name}
    ...     ${referrer_user_id}
    ...     ${beneficiary}
    ...     ${mm_card_type_id}
    ...     ${mm_card_level_id}
    ...     ${mm_factory_card_number}
    ...     ${is_require_otp}
    ...     ${tin_number}
    ...     ${tin_number_local}
    ...     ${title}
    ...     ${title_local}
    ...     ${first_name}
    ...     ${first_name_local}
    ...     ${middle_name}
    ...     ${middle_name_local}
    ...     ${last_name}
    ...     ${last_name_local}
    ...     ${suffix}
    ...     ${suffix_local}
    ...     ${date_of_birth}
    ...     ${place_of_birth}
    ...     ${place_of_birth_local}
    ...     ${gender}
    ...     ${gender_local}
    ...     ${ethnicity}
    ...     ${employer}
    ...     ${nationality}
    ...     ${occupation}
    ...     ${occupation_local}
    ...     ${occupation_title}
    ...     ${occupation_title_local}
    ...     ${township_code}
    ...     ${township_name}
    ...     ${township_name_local}
    ...     ${mother_name}
    ...     ${mother_name_local}
    ...     ${mother_maiden_name}
    ...     ${mother_maiden_name_local}
    ...     ${civil_status}
    ...     ${email}
    ...     ${secondary_mobile_number}
    ...     ${tertiary_mobile_number}
    ...     ${telephone_number}
    ...     ${current_address_citizen_association}
    ...     ${current_address_neighbourhood_association}
    ...     ${current_address_address}
    ...     ${current_address_address_local}
    ...     ${current_address_commune}
    ...     ${current_address_commune_local}
    ...     ${current_address_district}
    ...     ${current_address_district_local}
    ...     ${current_address_city}
    ...     ${current_address_city_local}
    ...     ${current_address_province}
    ...     ${current_address_province_local}
    ...     ${current_address_postal_code}
    ...     ${current_address_postal_code_local}
    ...     ${current_address_country}
    ...     ${current_address_country_local}
    ...     ${current_address_landmark}
    ...     ${current_address_longitude}
    ...     ${current_address_latitude}
    ...     ${permanent_address_citizen_association}
    ...     ${permanent_address_neighbourhood_association}
    ...     ${permanent_address_address}
    ...     ${permanent_address_address_local}
    ...     ${permanent_address_commune}
    ...     ${permanent_address_commune_local}
    ...     ${permanent_address_district}
    ...     ${permanent_address_district_local}
    ...     ${permanent_address_city}
    ...     ${permanent_address_city_local}
    ...     ${permanent_address_province}
    ...     ${permanent_address_province_local}
    ...     ${permanent_address_postal_code}
    ...     ${permanent_address_postal_code_local}
    ...     ${permanent_address_country}
    ...     ${permanent_address_country_local}
    ...     ${permanent_address_landmark}
    ...     ${permanent_address_longitude}
    ...     ${permanent_address_latitude}
    ...     ${primary_identity_type}
    ...     ${primary_identity_status}
    ...     ${primary_identity_identity_id}
    ...     ${primary_identity_identity_id_local}
    ...     ${primary_identity_place_of_issue}
    ...     ${primary_identity_issue_date}
    ...     ${primary_identity_expired_date}
    ...     ${primary_identity_front_identity_url}
    ...     ${primary_identity_back_identity_url}
    ...     ${primary_identity_signature_url}
    ...     ${secondary_identity_type}
    ...     ${secondary_identity_status}
    ...     ${secondary_identity_identity_id}
    ...     ${secondary_identity_identity_id_local}
    ...     ${secondary_identity_place_of_issue}
    ...     ${secondary_identity_issue_date}
    ...     ${secondary_identity_expired_date}
    ...     ${secondary_identity_front_identity_url}
    ...     ${secondary_identity_back_identity_url}
    ...     ${secondary_identity_signature_url}
    ...     ${tertiary_identity_type}
    ...     ${tertiary_identity_status}
    ...     ${tertiary_identity_identity_id}
    ...     ${tertiary_identity_identity_id_local}
    ...     ${tertiary_identity_place_of_issue}
    ...     ${tertiary_identity_issue_date}
    ...     ${tertiary_identity_expired_date}
    ...     ${tertiary_identity_front_identity_url}
    ...     ${tertiary_identity_back_identity_url}
    ...     ${tertiary_identity_signature_url}
    ...     ${kyc_level}
    ...     ${kyc_remark}
    ...     ${kyc_verify_by}
    ...     ${kyc_verify_date}
    ...     ${kyc_risk_level}
    ...     ${additional_profile_picture_url}
    ...     ${additional_tax_id_card_photo_url}
    ...     ${additional_field_1_name}
    ...     ${additional_field_1_value}
    ...     ${additional_field_2_name}
    ...     ${additional_field_2_value}
    ...     ${additional_field_3_name}
    ...     ${additional_field_3_value}
    ...     ${additional_field_4_name}
    ...     ${additional_field_4_value}
    ...     ${additional_field_5_name}
    ...     ${additional_field_5_value}
    ...     ${additional_supporting_file_1_url}
    ...     ${additional_supporting_file_2_url}
    ...     ${additional_supporting_file_3_url}
    ...     ${additional_supporting_file_4_url}
    ...     ${additional_supporting_file_5_url}
    ...     ${identity_auto_generate_password}

    ...     ${employee_id}
    ...     ${source_of_funds}
    ...     ${additional_region_id}
    ...     ${additional_sss_number}
    ...     ${additional_gsis_number}
    ...     ${additional_tax_income_number}
    ...     ${additional_philhealth_number}
    ...     ${additional_hdmf_number}
    ...     ${additional_additional_id_type}
    ...     ${additional_id_number}
    ...     ${payroll_card_number}
    ...     ${header_device_id}
    ...     ${header_device_description}
    ...     ${header_device_imei}
    ...     ${header_device_ip4}
    ...     ${header_client_id}
    ...     ${header_client_secret}

    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}

    run keyword if   '${header_device_id}'!='${param_not_used}'   set to dictionary       ${header}       device_id       ${header_device_id}
    run keyword if   '${header_device_description}'!='${param_not_used}'    set to dictionary       ${header}       device_description       ${header_device_description}
    run keyword if   '${header_device_imei}'!='${param_not_used}'     set to dictionary       ${header}       device_imei     ${header_device_imei}
    run keyword if   '${header_device_ip4}'!='${param_not_used}'    set to dictionary       ${header}       device_ip4      ${header_device_ip4}

    ${param_referrer_user_type}     create json data for user type object       referrer_user_type   ${referrer_user_type_id}      ${referrer_user_type_name}
    ${param_is_testing_account}		[Common] - Create boolean param		is_testing_account		${is_testing_account}
    ${param_is_system_account}		[Common] - Create boolean param		is_system_account		${is_system_account}
    ${param_acquisition_source}		[Common] - Create string param		acquisition_source		${acquisition_source}
    ${param_customer_classification_id}		[Common] - Create int param		customer_classification_id		${customer_classification_id}
    ${param_referrer_user_id}		[Common] - Create int param		referrer_user_id		${referrer_user_id}
    ${param_unique_reference}		[Common] - Create string param		unique_reference		${unique_reference}
    ${param_beneficiary}		[Common] - Create string param		beneficiary		${beneficiary}
    ${param_mm_card_type_id}		[Common] - Create int param		mm_card_type_id		${mm_card_type_id}
    ${param_mm_card_level_id}		[Common] - Create int param		mm_card_level_id		${mm_card_level_id}
    ${param_mm_factory_card_number}		[Common] - Create string param		mm_factory_card_number		${mm_factory_card_number}
    ${param_is_require_otp}		[Common] - Create boolean param		is_require_otp		${is_require_otp}
    ${param_tin_number}		[Common] - Create string param		tin_number		${tin_number}
    ${param_tin_number_local}		[Common] - Create string param		tin_number_local		${tin_number_local}
    ${param_title}		[Common] - Create string param		title		${title}
    ${param_title_local}		[Common] - Create string param		title_local		${title_local}
    ${param_first_name}		[Common] - Create string param		first_name		${first_name}
    ${param_first_name_local}		[Common] - Create string param		first_name_local		${first_name_local}
    ${param_middle_name}		[Common] - Create string param		middle_name		${middle_name}
    ${param_middle_name_local}		[Common] - Create string param		middle_name_local		${middle_name_local}
    ${param_last_name}		[Common] - Create string param		last_name		${last_name}
    ${param_last_name_local}		[Common] - Create string param		last_name_local		${last_name_local}
    ${param_suffix}		[Common] - Create string param		suffix		${suffix}
    ${param_suffix_local}		[Common] - Create string param		suffix_local		${suffix_local}
    ${param_date_of_birth}		[Common] - Create string param		date_of_birth		${date_of_birth}
    ${param_place_of_birth}		[Common] - Create string param		place_of_birth		${place_of_birth}
    ${param_place_of_birth_local}		[Common] - Create string param		place_of_birth_local		${place_of_birth_local}
    ${param_gender}		[Common] - Create string param		gender		${gender}
    ${param_gender_local}		[Common] - Create string param		gender_local		${gender_local}
    ${param_ethnicity}		[Common] - Create string param		ethnicity		${ethnicity}
    ${param_employer}		[Common] - Create string param		employer		${employer}
    ${param_nationality}		[Common] - Create string param		nationality		${nationality}
    ${param_occupation}		[Common] - Create string param		occupation		${occupation}
    ${param_occupation_local}		[Common] - Create string param		occupation_local		${occupation_local}
    ${param_occupation_title}		[Common] - Create string param		occupation_title		${occupation_title}
    ${param_occupation_title_local}		[Common] - Create string param		occupation_title_local		${occupation_title_local}
    ${param_township_code}		[Common] - Create string param		township_code		${township_code}
    ${param_township_name}		[Common] - Create string param		township_name		${township_name}
    ${param_township_name_local}		[Common] - Create string param		township_name_local		${township_name_local}
    ${param_mother_name}		[Common] - Create string param		mother_name		${mother_name}
    ${param_mother_name_local}		[Common] - Create string param		mother_name_local		${mother_name_local}
    ${param_mother_maiden_name}		[Common] - Create string param		mother_maiden_name		${mother_maiden_name}
    ${param_mother_maiden_name_local}		[Common] - Create string param		mother_maiden_name_local		${mother_maiden_name_local}
    ${param_civil_status}		[Common] - Create string param		civil_status		${civil_status}
    ${param_email}		[Common] - Create string param		email		${email}
    ${param_primary_mobile_number}		[Common] - Create string param		primary_mobile_number		${primary_mobile_number}
    ${param_secondary_mobile_number}		[Common] - Create string param		secondary_mobile_number		${secondary_mobile_number}
    ${param_tertiary_mobile_number}		[Common] - Create string param		tertiary_mobile_number		${tertiary_mobile_number}
    ${param_telephone_number}		[Common] - Create string param		telephone_number		${telephone_number}
    ${param_current_address_citizen_association}		[Common] - Create string param		citizen_association		${current_address_citizen_association}
    ${param_current_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${current_address_neighbourhood_association}
    ${param_current_address_address}		[Common] - Create string param		address		${current_address_address}
    ${param_current_address_address_local}		[Common] - Create string param		address_local		${current_address_address_local}
    ${param_current_address_commune}		[Common] - Create string param		commune		${current_address_commune}
    ${param_current_address_commune_local}		[Common] - Create string param		commune_local		${current_address_commune_local}
    ${param_current_address_district}		[Common] - Create string param		district		${current_address_district}
    ${param_current_address_district_local}		[Common] - Create string param		district_local		${current_address_district_local}
    ${param_current_address_city}		[Common] - Create string param		city		${current_address_city}
    ${param_current_address_city_local}		[Common] - Create string param		city_local		${current_address_city_local}
    ${param_current_address_province}		[Common] - Create string param		province		${current_address_province}
    ${param_current_address_province_local}		[Common] - Create string param		province_local		${current_address_province_local}
    ${param_current_address_postal_code}		[Common] - Create string param		postal_code		${current_address_postal_code}
    ${param_current_address_postal_code_local}		[Common] - Create string param		postal_code_local		${current_address_postal_code_local}
    ${param_current_address_country}		[Common] - Create string param		country		${current_address_country}
    ${param_current_address_country_local}		[Common] - Create string param		country_local	${current_address_country_local}
    ${param_current_address_landmark}		[Common] - Create string param		landmark		${current_address_landmark}
    ${param_current_address_longitude}		[Common] - Create string param		longitude		${current_address_longitude}
    ${param_current_address_latitude}		[Common] - Create string param		latitude		${current_address_latitude}
    ${param_permanent_address_citizen_association}		[Common] - Create string param		citizen_association		${permanent_address_citizen_association}
    ${param_permanent_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${permanent_address_neighbourhood_association}
    ${param_permanent_address_address}		[Common] - Create string param		address		${permanent_address_address}
    ${param_permanent_address_address_local}		[Common] - Create string param		address_local		${permanent_address_address_local}
    ${param_permanent_address_commune}		[Common] - Create string param		commune		${permanent_address_commune}
    ${param_permanent_address_commune_local}		[Common] - Create string param		commune_local		${permanent_address_commune_local}
    ${param_permanent_address_district}		[Common] - Create string param		district		${permanent_address_district}
    ${param_permanent_address_district_local}		[Common] - Create string param		district_local		${permanent_address_district_local}
    ${param_permanent_address_city}		[Common] - Create string param		city		${permanent_address_city}
    ${param_permanent_address_city_local}		[Common] - Create string param		city_local		${permanent_address_city_local}
    ${param_permanent_address_province}		[Common] - Create string param		province		${permanent_address_province}
    ${param_permanent_address_province_local}		[Common] - Create string param		province_local		${permanent_address_province_local}
    ${param_permanent_address_postal_code}		[Common] - Create string param		postal_code		${permanent_address_postal_code}
    ${param_permanent_address_postal_code_local}		[Common] - Create string param		postal_code_local		${permanent_address_postal_code_local}
    ${param_permanent_address_country}		[Common] - Create string param		country		${permanent_address_country}
    ${param_permanent_address_country_local}		[Common] - Create string param		country_local		${permanent_address_country_local}
    ${param_permanent_address_landmark}		[Common] - Create string param		landmark		${permanent_address_landmark}
    ${param_permanent_address_longitude}		[Common] - Create string param		longitude		${permanent_address_longitude}
    ${param_permanent_address_latitude}		[Common] - Create string param		latitude		${permanent_address_latitude}
    ${param_primary_identity_type}		[Common] - Create string param		type		${primary_identity_type}
    ${param_primary_identity_status}		[Common] - Create int param		status		${primary_identity_status}
    ${param_primary_identity_identity_id}		[Common] - Create string param		identity_id		${primary_identity_identity_id}
    ${param_primary_identity_identity_id_local}		[Common] - Create string param		identity_id_local		${primary_identity_identity_id_local}
    ${param_primary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${primary_identity_place_of_issue}
    ${param_primary_identity_issue_date}		[Common] - Create string param		issue_date		${primary_identity_issue_date}
    ${param_primary_identity_expired_date}		[Common] - Create string param		expired_date		${primary_identity_expired_date}
    ${param_primary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${primary_identity_front_identity_url}
    ${param_primary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${primary_identity_back_identity_url}
    ${param_primary_identity_signature_url}		[Common] - Create string param		signature_url		${primary_identity_signature_url}
    ${param_secondary_identity_type}		[Common] - Create string param		type		${secondary_identity_type}
    ${param_secondary_identity_status}		[Common] - Create int param		status		${secondary_identity_status}
    ${param_secondary_identity_identity_id}		[Common] - Create string param		identity_id		${secondary_identity_identity_id}
    ${param_secondary_identity_identity_id_local}		[Common] - Create string param		identity_id_local		${secondary_identity_identity_id_local}
    ${param_secondary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${secondary_identity_place_of_issue}
    ${param_secondary_identity_issue_date}		[Common] - Create string param		issue_date		${secondary_identity_issue_date}
    ${param_secondary_identity_expired_date}		[Common] - Create string param		expired_date		${secondary_identity_expired_date}
    ${param_secondary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${secondary_identity_front_identity_url}
    ${param_secondary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${secondary_identity_back_identity_url}
    ${param_secondary_identity_signature_url}		[Common] - Create string param		signature_url		${secondary_identity_signature_url}
    ${param_tertiary_identity_type}		[Common] - Create string param		type		${tertiary_identity_type}
    ${param_tertiary_identity_status}		[Common] - Create int param		status		${tertiary_identity_status}
    ${param_tertiary_identity_identity_id}		[Common] - Create string param		identity_id		${tertiary_identity_identity_id}
    ${param_tertiary_identity_identity_id_local}		[Common] - Create string param		identity_id_local		${tertiary_identity_identity_id_local}
    ${param_tertiary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${tertiary_identity_place_of_issue}
    ${param_tertiary_identity_issue_date}		[Common] - Create string param		issue_date		${tertiary_identity_issue_date}
    ${param_tertiary_identity_expired_date}		[Common] - Create string param		expired_date		${tertiary_identity_expired_date}
    ${param_tertiary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${tertiary_identity_front_identity_url}
    ${param_tertiary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${tertiary_identity_back_identity_url}
    ${param_tertiary_identity_signature_url}		[Common] - Create string param		signature_url		${tertiary_identity_signature_url}
    ${param_kyc_level}		[Common] - Create int param		level		${kyc_level}
    ${param_kyc_remark}		[Common] - Create string param		remark		${kyc_remark}
    ${param_kyc_verify_by}		[Common] - Create string param		verify_by		${kyc_verify_by}
    ${param_kyc_verify_date}		[Common] - Create string param		verify_date		${kyc_verify_date}
    ${param_kyc_risk_level}		[Common] - Create string param		risk_level		${kyc_risk_level}
    ${param_additional_profile_picture_url}		[Common] - Create string param		profile_picture_url		${additional_profile_picture_url}
    ${param_additional_tax_id_card_photo_url}		[Common] - Create string param		tax_id_card_photo_url		${additional_tax_id_card_photo_url}
    ${param_additional_field_1_name}		[Common] - Create string param		field_1_name		${additional_field_1_name}
    ${param_additional_field_1_value}		[Common] - Create string param		field_1_value		${additional_field_1_value}
    ${param_additional_field_2_name}		[Common] - Create string param		field_2_name		${additional_field_2_name}
    ${param_additional_field_2_value}		[Common] - Create string param		field_2_value		${additional_field_2_value}
    ${param_additional_field_3_name}		[Common] - Create string param		field_3_name		${additional_field_3_name}
    ${param_additional_field_3_value}		[Common] - Create string param		field_3_value		${additional_field_3_value}
    ${param_additional_field_4_name}		[Common] - Create string param		field_4_name		${additional_field_4_name}
    ${param_additional_field_4_value}		[Common] - Create string param		field_4_value		${additional_field_4_value}
    ${param_additional_field_5_name}		[Common] - Create string param		field_5_name		${additional_field_5_name}
    ${param_additional_field_5_value}		[Common] - Create string param		field_5_value		${additional_field_5_value}
    ${param_additional_supporting_file_1_url}		[Common] - Create string param		supporting_file_1_url		${additional_supporting_file_1_url}
    ${param_additional_supporting_file_2_url}		[Common] - Create string param		supporting_file_2_url		${additional_supporting_file_2_url}
    ${param_additional_supporting_file_3_url}		[Common] - Create string param		supporting_file_3_url		${additional_supporting_file_3_url}
    ${param_additional_supporting_file_4_url}		[Common] - Create string param		supporting_file_4_url		${additional_supporting_file_4_url}
    ${param_additional_supporting_file_5_url}		[Common] - Create string param		supporting_file_5_url		${additional_supporting_file_5_url}
    ${param_employee_id}		[Common] - Create string param		employee_id		${employee_id}
    ${param_source_of_funds}	[Common] - Create string param		source_of_funds		${source_of_funds}
    ${param_region_id}			[Common] - Create string param		region_id		${additional_region_id}
    ${param_sss_number}	 		[Common] - Create string param		sss_number		${additional_sss_number}
    ${param_gsis_number}		[Common] - Create string param		gsis_number		${additional_gsis_number}
    ${param_tax_income_number}  [Common] - Create string param		tax_income_number		${additional_tax_income_number}
    ${param_philhealth_number}  [Common] - Create string param		philhealth_number		${additional_philhealth_number}
    ${param_hdmf_number}		[Common] - Create string param		hdmf_number		${additional_hdmf_number}
    ${param_additional_additional_id_type}	[Common] - Create string param		additional_id_type		${additional_additional_id_type}
    ${param_additional_id_number} 	[Common] - Create string param		additional_id_number		${additional_id_number}
    ${param_payroll_card_number} 	[Common] - Create string param		payroll_card_number		${payroll_card_number}

    ${param_identity_identity_type_id}		[Common] - Create int param		identity_type_id		${identity_type_id}
    ${param_identity_username}		[Common] - Create string param		username		${username}
    ${param_identity_password}		[Common] - Create string param		password		${password}
    ${param_identity_auto_generate_password}		[Common] - Create boolean param		auto_generate_password		${identity_auto_generate_password}

    ${data}              catenate   SEPARATOR=
    ...          {
    ...              "profile": {
    ...                 ${param_is_testing_account}
    ...                 ${param_is_system_account}
    ...                 ${param_acquisition_source}
    ...                 ${param_customer_classification_id}
    ...                 ${param_referrer_user_type}
    ...                 ${param_referrer_user_id}
    ...                 ${param_unique_reference}
    ...                 ${param_beneficiary}
    ...                 ${param_mm_card_type_id}
    ...                 ${param_mm_card_level_id}
    ...                 ${param_mm_factory_card_number}
    ...                 ${param_is_require_otp}
    ...                 ${param_tin_number}
    ...                 ${param_tin_number_local}
    ...                 ${param_title}
    ...                 ${param_title_local}
    ...                 ${param_first_name}
    ...                 ${param_first_name_local}
    ...                 ${param_middle_name}
    ...                 ${param_middle_name_local}
    ...                 ${param_last_name}
    ...                 ${param_last_name_local}
    ...                 ${param_suffix}
    ...                 ${param_suffix_local}
    ...                 ${param_date_of_birth}
    ...                 ${param_place_of_birth}
    ...                 ${param_place_of_birth_local}
    ...                 ${param_gender}
    ...                 ${param_gender_local}
    ...                 ${param_ethnicity}
    ...                 ${param_employer}
    ...                 ${param_nationality}
    ...                 ${param_occupation}
    ...                 ${param_occupation_local}
    ...                 ${param_occupation_title}
    ...                 ${param_occupation_title_local}
    ...                 ${param_township_code}
    ...                 ${param_township_name}
    ...                 ${param_township_name_local}
    ...                 ${param_mother_name}
    ...                 ${param_mother_name_local}
    ...                 ${param_mother_maiden_name}
    ...                 ${param_mother_maiden_name_local}
    ...                 ${param_civil_status}
    ...                 ${param_email}
    ...                 ${param_primary_mobile_number}
    ...                 ${param_secondary_mobile_number}
    ...                 ${param_tertiary_mobile_number}
    ...                 ${param_telephone_number}
    ...                 ${param_employee_id}
    ...                 ${param_source_of_funds}
    ...                 ${param_payroll_card_number}
    ...                 "address": {
    ...                     "current_address": {
    ...      					${param_current_address_citizen_association}
    ...      					${param_current_address_neighbourhood_association}
    ...      					${param_current_address_address}
    ...      					${param_current_address_commune}
    ...      					${param_current_address_district}
    ...      					${param_current_address_city}
    ...      					${param_current_address_province}
    ...      					${param_current_address_postal_code}
    ...      					${param_current_address_country}
    ...      					${param_current_address_landmark}
    ...      					${param_current_address_longitude}
    ...      					${param_current_address_latitude}
    ...      					${param_current_address_address_local}
    ...      					${param_current_address_commune_local}
    ...      					${param_current_address_district_local}
    ...      					${param_current_address_city_local}
    ...      					${param_current_address_province_local}
    ...      					${param_current_address_postal_code_local}
    ...      					${param_current_address_country_local}
    ...                     },
    ...                     "permanent_address": {
    ...      					${param_permanent_address_citizen_association}
    ...      					${param_permanent_address_neighbourhood_association}
    ...      					${param_permanent_address_address}
    ...      					${param_permanent_address_commune}
    ...      					${param_permanent_address_district}
    ...      					${param_permanent_address_city}
    ...      					${param_permanent_address_province}
    ...      					${param_permanent_address_postal_code}
    ...      					${param_permanent_address_country}
    ...      					${param_permanent_address_landmark}
    ...      					${param_permanent_address_longitude}
    ...      					${param_permanent_address_latitude}
    ...      					${param_permanent_address_address_local}
    ...      					${param_permanent_address_commune_local}
    ...      					${param_permanent_address_district_local}
    ...      					${param_permanent_address_city_local}
    ...      					${param_permanent_address_province_local}
    ...      					${param_permanent_address_postal_code_local}
    ...      					${param_permanent_address_country_local}
    ...                     }
    ...                 },
    ...                 "kyc": {
    ...                     "primary_identity": {
    ...      					${param_primary_identity_type}
    ...      					${param_primary_identity_status}
    ...      					${param_primary_identity_identity_id}
    ...      					${param_primary_identity_identity_id_local}
    ...      					${param_primary_identity_place_of_issue}
    ...      					${param_primary_identity_issue_date}
    ...      					${param_primary_identity_expired_date}
    ...      					${param_primary_identity_front_identity_url}
    ...      					${param_primary_identity_back_identity_url}
    ...      					${param_primary_identity_signature_url}
    ...                     },
    ...                     "secondary_identity": {
    ...      					${param_secondary_identity_type}
    ...      					${param_secondary_identity_status}
    ...      					${param_secondary_identity_identity_id}
    ...      					${param_secondary_identity_identity_id_local}
    ...      					${param_secondary_identity_place_of_issue}
    ...      					${param_secondary_identity_issue_date}
    ...      					${param_secondary_identity_expired_date}
    ...      					${param_secondary_identity_front_identity_url}
    ...      					${param_secondary_identity_back_identity_url}
    ...      					${param_secondary_identity_signature_url}
    ...                     },
    ...                     "tertiary_identity": {
    ...      					${param_tertiary_identity_type}
    ...      					${param_tertiary_identity_status}
    ...      					${param_tertiary_identity_identity_id}
    ...      					${param_tertiary_identity_identity_id_local}
    ...      					${param_tertiary_identity_place_of_issue}
    ...      					${param_tertiary_identity_issue_date}
    ...      					${param_tertiary_identity_expired_date}
    ...      					${param_tertiary_identity_front_identity_url}
    ...      					${param_tertiary_identity_back_identity_url}
    ...      					${param_tertiary_identity_signature_url}
    ...                     },
    ...      				${param_kyc_level}
    ...      				${param_kyc_remark}
    ...      				${param_kyc_verify_by}
    ...      				${param_kyc_verify_date}
    ...      				${param_kyc_risk_level}
    ...                 },
    ...                 "additional": {
    ...      				${param_additional_profile_picture_url}
    ...      				${param_additional_tax_id_card_photo_url}
    ...      				${param_additional_field_1_name}
    ...      				${param_additional_field_1_value}
    ...      				${param_additional_field_2_name}
    ...      				${param_additional_field_2_value}
    ...      				${param_additional_field_3_name}
    ...      				${param_additional_field_3_value}
    ...      				${param_additional_field_4_name}
    ...      				${param_additional_field_4_value}
    ...      				${param_additional_field_5_name}
    ...      				${param_additional_field_5_value}
    ...      				${param_additional_supporting_file_1_url}
    ...      				${param_additional_supporting_file_2_url}
    ...      				${param_additional_supporting_file_3_url}
    ...      				${param_additional_supporting_file_4_url}
    ...      				${param_additional_supporting_file_5_url}
    ...      				${param_region_id}
    ...      				${param_sss_number}
    ...      				${param_gsis_number}
    ...      				${param_tax_income_number}
    ...      				${param_philhealth_number}
    ...      				${param_hdmf_number}
    ...      				${param_additional_additional_id_type}
    ...      				${param_additional_id_number}
    ...                 }
    ...              },
    ...              "identity": {
    ...      			${param_identity_identity_type_id}
    ...      			${param_identity_username}
    ...      			${param_identity_password}
    ...      			${param_identity_auto_generate_password}
    ...              }
    ...          }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
    create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${system_user_create_customer_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${header}
    log        ${data}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

#API register customer
#    [Arguments]
#    ...     ${username}
#    ...     ${password}
#    ...     ${identity_type_id}
#    ...     ${access_token}
#    ...     ${primary_mobile_number}
#    ...     ${unique_reference}
#    ...     ${header_device_id}
#    ...     ${header_device_description}
#    ...     ${referrer_user_type_id}
#    ...     ${referrer_user_type_name}
#    ...     ${referrer_user_id}
#    ...     ${beneficiary}
#    ...     ${mm_card_type_id}
#    ...     ${mm_card_level_id}
#    ...     ${mm_factory_card_number}
#    ...     ${is_require_otp}
#    ...     ${tin_number}
#    ...     ${title}
#    ...     ${first_name}
#    ...     ${middle_name}
#    ...     ${last_name}
#    ...     ${suffix}
#    ...     ${date_of_birth}
#    ...     ${place_of_birth}
#    ...     ${gender}
#    ...     ${ethnicity}
#    ...     ${employer}
#    ...     ${nationality}
#    ...     ${occupation}
#    ...     ${occupation_title}
#    ...     ${township_code}
#    ...     ${township_name}
#    ...     ${mother_name}
#    ...     ${mother_maiden_name}
#    ...     ${civil_status}
#    ...     ${email}
#    ...     ${secondary_mobile_number}
#    ...     ${tertiary_mobile_number}
#    ...     ${telephone_number}
#    ...     ${current_address_citizen_association}
#    ...     ${current_address_neighbourhood_association}
#    ...     ${current_address_address}
#    ...     ${current_address_commune}
#    ...     ${current_address_district}
#    ...     ${current_address_city}
#    ...     ${current_address_province}
#    ...     ${current_address_postal_code}
#    ...     ${current_address_country}
#    ...     ${current_address_landmark}
#    ...     ${current_address_longitude}
#    ...     ${current_address_latitude}
#    ...     ${permanent_address_citizen_association}
#    ...     ${permanent_address_neighbourhood_association}
#    ...     ${permanent_address_address}
#    ...     ${permanent_address_commune}
#    ...     ${permanent_address_district}
#    ...     ${permanent_address_city}
#    ...     ${permanent_address_province}
#    ...     ${permanent_address_postal_code}
#    ...     ${permanent_address_country}
#    ...     ${permanent_address_landmark}
#    ...     ${permanent_address_longitude}
#    ...     ${permanent_address_latitude}
#    ...     ${primary_identity_type}
#    ...     ${primary_identity_identity_id}
#    ...     ${primary_identity_place_of_issue}
#    ...     ${primary_identity_issue_date}
#    ...     ${primary_identity_expired_date}
#    ...     ${primary_identity_front_identity_url}
#    ...     ${primary_identity_back_identity_url}
#    ...     ${primary_identity_signature_url}
#    ...     ${secondary_identity_type}
#    ...     ${secondary_identity_identity_id}
#    ...     ${secondary_identity_place_of_issue}
#    ...     ${secondary_identity_issue_date}
#    ...     ${secondary_identity_expired_date}
#    ...     ${secondary_identity_front_identity_url}
#    ...     ${secondary_identity_back_identity_url}
#    ...     ${secondary_identity_signature_url}
#    ...     ${tertiary_identity_type}
#    ...     ${tertiary_identity_identity_id}
#    ...     ${tertiary_identity_place_of_issue}
#    ...     ${tertiary_identity_issue_date}
#    ...     ${tertiary_identity_expired_date}
#    ...     ${tertiary_identity_front_identity_url}
#    ...     ${tertiary_identity_back_identity_url}
#    ...     ${tertiary_identity_signature_url}
#    ...     ${additional_profile_picture_url}
#    ...     ${additional_tax_id_card_photo_url}
#    ...     ${identity_auto_generate_password}
#    ...     ${header_client_id}
#    ...     ${header_client_secret}
#    ...     ${header_device_imei}
#    ...     ${header_device_ip4}
#
#    ${header}               create dictionary
#        ...     client_id=${header_client_id}
#        ...     client_secret=${header_client_secret}
#        ...     content-type=application/json
#        ...     Authorization=Bearer ${access_token}
#
#    run keyword if   '${header_device_id}'!='${param_not_used}'     set to dictionary       ${header}       device_id       ${header_device_id}
#    run keyword if   '${header_device_description}'!='${param_not_used}'     run keywords   set to dictionary       ${header}       device_description       ${header_device_description}
#    run keyword if   '${header_device_imei}'!='${param_not_used}'   run keywords    set to dictionary       ${header}       device_imei     ${header_device_imei}
#    run keyword if   '${header_device_ip4}'!='${param_not_used}'    run keywords    set to dictionary       ${header}       device_ip4      ${header_device_ip4}
#
#    ${param_referrer_user_type_id}		[Common] - Create int param		id		${referrer_user_type_id}
#    ${param_referrer_user_type_name}		[Common] - Create string param		name		${referrer_user_type_name}
#    ${param_referrer_user_id}		[Common] - Create int param		referrer_user_id		${referrer_user_id}
#    ${param_unique_reference}		[Common] - Create string param		unique_reference		${unique_reference}
#    ${param_beneficiary}		[Common] - Create string param		beneficiary		${beneficiary}
#    ${param_mm_card_type_id}		[Common] - Create int param		mm_card_type_id		${mm_card_type_id}
#    ${param_mm_card_level_id}		[Common] - Create int param		mm_card_level_id		${mm_card_level_id}
#    ${param_mm_factory_card_number}		[Common] - Create string param		mm_factory_card_number		${mm_factory_card_number}
#    ${param_is_require_otp}		[Common] - Create boolean param		is_require_otp		${is_require_otp}
#    ${param_tin_number}		[Common] - Create string param		tin_number		${tin_number}
#    ${param_title}		[Common] - Create string param		title		${title}
#    ${param_first_name}		[Common] - Create string param		first_name		${first_name}
#    ${param_middle_name}		[Common] - Create string param		middle_name		${middle_name}
#    ${param_last_name}		[Common] - Create string param		last_name		${last_name}
#    ${param_suffix}		[Common] - Create string param		suffix		${suffix}
#    ${param_date_of_birth}		[Common] - Create string param		date_of_birth		${date_of_birth}
#    ${param_place_of_birth}		[Common] - Create string param		place_of_birth		${place_of_birth}
#    ${param_gender}		[Common] - Create string param		gender		${gender}
#    ${param_ethnicity}		[Common] - Create string param		ethnicity		${ethnicity}
#    ${param_employer}		[Common] - Create string param		employer		${employer}
#    ${param_nationality}		[Common] - Create string param		nationality		${nationality}
#    ${param_occupation}		[Common] - Create string param		occupation		${occupation}
#    ${param_occupation_title}		[Common] - Create string param		occupation_title		${occupation_title}
#    ${param_township_code}		[Common] - Create string param		township_code		${township_code}
#    ${param_township_name}		[Common] - Create string param		township_name		${township_name}
#    ${param_mother_name}		[Common] - Create string param		mother_name		${mother_name}
#    ${param_mother_maiden_name}		[Common] - Create string param		mother_maiden_name		${mother_maiden_name}
#    ${param_civil_status}		[Common] - Create string param		civil_status		${civil_status}
#    ${param_email}		[Common] - Create string param		email		${email}
#    ${param_primary_mobile_number}		[Common] - Create string param		primary_mobile_number		${primary_mobile_number}
#    ${param_secondary_mobile_number}		[Common] - Create string param		secondary_mobile_number		${secondary_mobile_number}
#    ${param_tertiary_mobile_number}		[Common] - Create string param		tertiary_mobile_number		${tertiary_mobile_number}
#    ${param_telephone_number}		[Common] - Create string param		telephone_number		${telephone_number}
#    ${param_current_address_citizen_association}		[Common] - Create string param		citizen_association		${current_address_citizen_association}
#    ${param_current_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${current_address_neighbourhood_association}
#    ${param_current_address_address}		[Common] - Create string param		address		${current_address_address}
#    ${param_current_address_commune}		[Common] - Create string param		commune		${current_address_commune}
#    ${param_current_address_district}		[Common] - Create string param		district		${current_address_district}
#    ${param_current_address_city}		[Common] - Create string param		city		${current_address_city}
#    ${param_current_address_province}		[Common] - Create string param		province		${current_address_province}
#    ${param_current_address_postal_code}		[Common] - Create string param		postal_code		${current_address_postal_code}
#    ${param_current_address_country}		[Common] - Create string param		country		${current_address_country}
#    ${param_current_address_landmark}		[Common] - Create string param		landmark		${current_address_landmark}
#    ${param_current_address_longitude}		[Common] - Create string param		longitude		${current_address_longitude}
#    ${param_current_address_latitude}		[Common] - Create string param		latitude		${current_address_latitude}
#    ${param_permanent_address_citizen_association}		[Common] - Create string param		citizen_association		${permanent_address_citizen_association}
#    ${param_permanent_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${permanent_address_neighbourhood_association}
#    ${param_permanent_address_address}		[Common] - Create string param		address		${permanent_address_address}
#    ${param_permanent_address_commune}		[Common] - Create string param		commune		${permanent_address_commune}
#    ${param_permanent_address_district}		[Common] - Create string param		district		${permanent_address_district}
#    ${param_permanent_address_city}		[Common] - Create string param		city		${permanent_address_city}
#    ${param_permanent_address_province}		[Common] - Create string param		province		${permanent_address_province}
#    ${param_permanent_address_postal_code}		[Common] - Create string param		postal_code		${permanent_address_postal_code}
#    ${param_permanent_address_country}		[Common] - Create string param		country		${permanent_address_country}
#    ${param_permanent_address_landmark}		[Common] - Create string param		landmark		${permanent_address_landmark}
#    ${param_permanent_address_longitude}		[Common] - Create string param		longitude		${permanent_address_longitude}
#    ${param_permanent_address_latitude}		[Common] - Create string param		latitude		${permanent_address_latitude}
#    ${param_primary_identity_type}		[Common] - Create string param		type		${primary_identity_type}
#    ${param_primary_identity_identity_id}		[Common] - Create string param		identity_id		${primary_identity_identity_id}
#    ${param_primary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${primary_identity_place_of_issue}
#    ${param_primary_identity_issue_date}		[Common] - Create string param		issue_date		${primary_identity_issue_date}
#    ${param_primary_identity_expired_date}		[Common] - Create string param		expired_date		${primary_identity_expired_date}
#    ${param_primary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${primary_identity_front_identity_url}
#    ${param_primary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${primary_identity_back_identity_url}
#    ${param_primary_identity_signature_url}		[Common] - Create string param		signature_url		${primary_identity_signature_url}
#    ${param_secondary_identity_type}		[Common] - Create string param		type		${secondary_identity_type}
#    ${param_secondary_identity_identity_id}		[Common] - Create string param		identity_id		${secondary_identity_identity_id}
#    ${param_secondary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${secondary_identity_place_of_issue}
#    ${param_secondary_identity_issue_date}		[Common] - Create string param		issue_date		${secondary_identity_issue_date}
#    ${param_secondary_identity_expired_date}		[Common] - Create string param		expired_date		${secondary_identity_expired_date}
#    ${param_secondary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${secondary_identity_front_identity_url}
#    ${param_secondary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${secondary_identity_back_identity_url}
#    ${param_secondary_identity_signature_url}		[Common] - Create string param		signature_url		${secondary_identity_signature_url}
#    ${param_tertiary_identity_type}		[Common] - Create string param		type		${tertiary_identity_type}
#    ${param_tertiary_identity_identity_id}		[Common] - Create string param		identity_id		${tertiary_identity_identity_id}
#    ${param_tertiary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${tertiary_identity_place_of_issue}
#    ${param_tertiary_identity_issue_date}		[Common] - Create string param		issue_date		${tertiary_identity_issue_date}
#    ${param_tertiary_identity_expired_date}		[Common] - Create string param		expired_date		${tertiary_identity_expired_date}
#    ${param_tertiary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${tertiary_identity_front_identity_url}
#    ${param_tertiary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${tertiary_identity_back_identity_url}
#    ${param_tertiary_identity_signature_url}		[Common] - Create string param		signature_url		${tertiary_identity_signature_url}
#    ${param_additional_profile_picture_url}		[Common] - Create string param		profile_picture_url		${additional_profile_picture_url}
#    ${param_additional_tax_id_card_photo_url}		[Common] - Create string param		tax_id_card_photo_url		${additional_tax_id_card_photo_url}
#    ${param_identity_identity_type_id}		[Common] - Create int param		identity_type_id		${identity_type_id}
#    ${param_identity_username}		[Common] - Create string param		username		${username}
#    ${param_identity_password}		[Common] - Create string param		password		${password}
#    ${param_identity_auto_generate_password}		[Common] - Create boolean param		auto_generate_password		${identity_auto_generate_password}
#
#    ${data}              catenate   SEPARATOR=
#    ...          {
#    ...              "profile": {
#    ...                 "referrer_user_type": {
#    ...                     ${param_referrer_user_type_id}
#    ...                     ${param_referrer_user_type_name}
#    ...                 },
#    ...                 ${param_referrer_user_id}
#    ...                 ${param_unique_reference}
#    ...                 ${param_beneficiary}
#    ...                 ${param_mm_card_type_id}
#    ...                 ${param_mm_card_level_id}
#    ...                 ${param_mm_factory_card_number}
#    ...                 ${param_is_require_otp}
#    ...                 ${param_tin_number}
#    ...                 ${param_title}
#    ...                 ${param_first_name}
#    ...                 ${param_middle_name}
#    ...                 ${param_last_name}
#    ...                 ${param_suffix}
#    ...                 ${param_date_of_birth}
#    ...                 ${param_place_of_birth}
#    ...                 ${param_gender}
#    ...                 ${param_ethnicity}
#    ...                 ${param_employer}
#    ...                 ${param_nationality}
#    ...                 ${param_occupation}
#    ...                 ${param_occupation_title}
#    ...                 ${param_township_code}
#    ...                 ${param_township_name}
#    ...                 ${param_mother_name}
#    ...                 ${param_mother_maiden_name}
#    ...                 ${param_civil_status}
#    ...                 ${param_email}
#    ...                 ${param_primary_mobile_number}
#    ...                 ${param_secondary_mobile_number}
#    ...                 ${param_tertiary_mobile_number}
#    ...                 ${param_telephone_number}
#    ...                 "address": {
#    ...                     "current_address": {
#    ...      					${param_current_address_citizen_association}
#    ...      					${param_current_address_neighbourhood_association}
#    ...      					${param_current_address_address}
#    ...      					${param_current_address_commune}
#    ...      					${param_current_address_district}
#    ...      					${param_current_address_city}
#    ...      					${param_current_address_province}
#    ...      					${param_current_address_postal_code}
#    ...      					${param_current_address_country}
#    ...      					${param_current_address_landmark}
#    ...      					${param_current_address_longitude}
#    ...      					${param_current_address_latitude}
#    ...                     },
#    ...                     "permanent_address": {
#    ...      					${param_permanent_address_citizen_association}
#    ...      					${param_permanent_address_neighbourhood_association}
#    ...      					${param_permanent_address_address}
#    ...      					${param_permanent_address_commune}
#    ...      					${param_permanent_address_district}
#    ...      					${param_permanent_address_city}
#    ...      					${param_permanent_address_province}
#    ...      					${param_permanent_address_postal_code}
#    ...      					${param_permanent_address_country}
#    ...      					${param_permanent_address_landmark}
#    ...      					${param_permanent_address_longitude}
#    ...      					${param_permanent_address_latitude}
#    ...                     }
#    ...                 },
#    ...                 "kyc": {
#    ...                     "primary_identity": {
#    ...      					${param_primary_identity_type}
#    ...      					${param_primary_identity_identity_id}
#    ...      					${param_primary_identity_place_of_issue}
#    ...      					${param_primary_identity_issue_date}
#    ...      					${param_primary_identity_expired_date}
#    ...      					${param_primary_identity_front_identity_url}
#    ...      					${param_primary_identity_back_identity_url}
#    ...      					${param_primary_identity_signature_url}
#    ...                     },
#    ...                     "secondary_identity": {
#    ...      					${param_secondary_identity_type}
#    ...      					${param_secondary_identity_identity_id}
#    ...      					${param_secondary_identity_place_of_issue}
#    ...      					${param_secondary_identity_issue_date}
#    ...      					${param_secondary_identity_expired_date}
#    ...      					${param_secondary_identity_front_identity_url}
#    ...      					${param_secondary_identity_back_identity_url}
#    ...      					${param_secondary_identity_signature_url}
#    ...                     },
#    ...                     "tertiary_identity": {
#    ...      					${param_tertiary_identity_type}
#    ...      					${param_tertiary_identity_identity_id}
#    ...      					${param_tertiary_identity_place_of_issue}
#    ...      					${param_tertiary_identity_issue_date}
#    ...      					${param_tertiary_identity_expired_date}
#    ...      					${param_tertiary_identity_front_identity_url}
#    ...      					${param_tertiary_identity_back_identity_url}
#    ...      					${param_tertiary_identity_signature_url}
#    ...                     }
#    ...                 },
#    ...                 "additional": {
#    ...      				${param_additional_profile_picture_url}
#    ...      				${param_additional_tax_id_card_photo_url}
#    ...                 }
#    ...              },
#    ...              "identity": {
#    ...      			${param_identity_identity_type_id}
#    ...      			${param_identity_username}
#    ...      			${param_identity_password}
#    ...      			${param_identity_auto_generate_password}
#    ...              }
#    ...          }
#    ${data}  replace string      ${data}      ,}    }
#    ${data}  replace string      ${data}      },,    },
#    create session          api     ${api_gateway_host}     disable_warnings=1
#    ${response}             post request
#        ...     api
#        ...     ${register_customer_path}
#        ...     ${data}
#        ...     ${NONE}
#        ...     ${header}
#    log        ${data}
#    log        ${response.text}
#    ${dic} =	Create Dictionary   response=${response}
#    [Return]    ${dic}

API register customer
    [Arguments]     ${arg_dic}
    ${param_referrer_user_type}     create json data for user type object       referrer_user_type   ${arg_dic.referrer_user_type_id}      ${arg_dic.referrer_user_type_name}
    ${param_referrer_user_id}		[Common] - Create int param in dic    ${arg_dic}		referrer_user_id
    ${param_unique_reference}		[Common] - Create string param in dic    ${arg_dic}		unique_reference
    ${param_beneficiary}		[Common] - Create string param in dic    ${arg_dic}		beneficiary
    ${param_mm_card_type_id}		[Common] - Create int param in dic    ${arg_dic}		mm_card_type_id
    ${param_mm_card_level_id}		[Common] - Create int param in dic    ${arg_dic}		mm_card_level_id
    ${param_mm_factory_card_number}		[Common] - Create string param in dic    ${arg_dic}		mm_factory_card_number
    ${param_is_require_otp}		[Common] - Create boolean param in dic    ${arg_dic}		is_require_otp
    ${param_tin_number}		[Common] - Create string param in dic    ${arg_dic}		tin_number
    ${param_tin_number_local}		[Common] - Create string param in dic    ${arg_dic}		tin_number_local
    ${param_title}		[Common] - Create string param in dic    ${arg_dic}		title
    ${param_title_local}		[Common] - Create string param in dic    ${arg_dic}		title_local
    ${param_first_name}		[Common] - Create string param in dic    ${arg_dic}		first_name
    ${param_first_name_local}		[Common] - Create string param in dic    ${arg_dic}		first_name_local
    ${param_middle_name}		[Common] - Create string param in dic    ${arg_dic}		middle_name
    ${param_middle_name_local}		[Common] - Create string param in dic    ${arg_dic}		middle_name_local
    ${param_last_name}		[Common] - Create string param in dic    ${arg_dic}		last_name
    ${param_last_name_local}		[Common] - Create string param in dic    ${arg_dic}		last_name_local
    ${param_suffix}		[Common] - Create string param in dic    ${arg_dic}		suffix
    ${param_suffix_local}		[Common] - Create string param in dic    ${arg_dic}		suffix_local
    ${param_date_of_birth}		[Common] - Create string param in dic    ${arg_dic}		date_of_birth
    ${param_place_of_birth}		[Common] - Create string param in dic    ${arg_dic}		place_of_birth
    ${param_place_of_birth_local}		[Common] - Create string param in dic    ${arg_dic}		place_of_birth_local
    ${param_gender}		[Common] - Create string param in dic    ${arg_dic}		gender
    ${param_gender_local}		[Common] - Create string param in dic    ${arg_dic}		gender_local
    ${param_ethnicity}		[Common] - Create string param in dic    ${arg_dic}		ethnicity
    ${param_employer}		[Common] - Create string param in dic    ${arg_dic}		employer
    ${param_nationality}		[Common] - Create string param in dic    ${arg_dic}		nationality
    ${param_occupation}		[Common] - Create string param in dic    ${arg_dic}		occupation
    ${param_occupation_local}		[Common] - Create string param in dic    ${arg_dic}		occupation_local
    ${param_occupation_title}		[Common] - Create string param in dic    ${arg_dic}		occupation_title
    ${param_occupation_title_local}		[Common] - Create string param in dic    ${arg_dic}		occupation_title_local
    ${param_township_code}		[Common] - Create string param in dic    ${arg_dic}		township_code
    ${param_township_name}		[Common] - Create string param in dic    ${arg_dic}		township_name
    ${param_township_name_local}		[Common] - Create string param in dic    ${arg_dic}		township_name_local
    ${param_mother_name}		[Common] - Create string param in dic    ${arg_dic}		mother_name
    ${param_mother_name_local}		[Common] - Create string param in dic    ${arg_dic}		mother_name_local
    ${param_mother_maiden_name}		[Common] - Create string param in dic    ${arg_dic}		mother_maiden_name
    ${param_mother_maiden_name_local}		[Common] - Create string param in dic    ${arg_dic}		mother_maiden_name_local
    ${param_civil_status}		[Common] - Create string param in dic    ${arg_dic}		civil_status
    ${param_email}		[Common] - Create string param in dic    ${arg_dic}		email
    ${param_primary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}		primary_mobile_number
    ${param_secondary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}		secondary_mobile_number
    ${param_tertiary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}		tertiary_mobile_number
    ${param_telephone_number}		[Common] - Create string param in dic    ${arg_dic}		telephone_number
    ${param_current_address_citizen_association}		[Common] - Create string param in dic    ${arg_dic}		citizen_association
    ${param_current_address_neighbourhood_association}		[Common] - Create string param in dic    ${arg_dic}		neighbourhood_association
    ${param_current_address_address}		[Common] - Create string param in dic    ${arg_dic}		address
    ${param_current_address_address_local}		[Common] - Create string param in dic    ${arg_dic}		address_local
    ${param_current_address_commune}		[Common] - Create string param in dic    ${arg_dic}		commune
    ${param_current_address_commune_local}		[Common] - Create string param in dic    ${arg_dic}		commune_local
    ${param_current_address_district}		[Common] - Create string param in dic    ${arg_dic}		district
    ${param_current_address_district_local}		[Common] - Create string param in dic    ${arg_dic}		district_local
    ${param_current_address_city}		[Common] - Create string param in dic    ${arg_dic}		city
    ${param_current_address_city_local}		[Common] - Create string param in dic    ${arg_dic}		city_local
    ${param_current_address_province}		[Common] - Create string param in dic    ${arg_dic}		province
    ${param_current_address_province_local}		[Common] - Create string param in dic    ${arg_dic}		province_local
    ${param_current_address_postal_code}		[Common] - Create string param in dic    ${arg_dic}		postal_code
    ${param_current_address_postal_code_local}		[Common] - Create string param in dic    ${arg_dic}		postal_code_local
    ${param_current_address_country}		[Common] - Create string param in dic    ${arg_dic}		country
    ${param_current_address_country_local}		[Common] - Create string param in dic    ${arg_dic}		country_local
    ${param_current_address_landmark}		[Common] - Create string param in dic    ${arg_dic}		landmark
    ${param_current_address_longitude}		[Common] - Create string param in dic    ${arg_dic}		longitude
    ${param_current_address_latitude}		[Common] - Create string param in dic    ${arg_dic}		latitude
    ${param_permanent_address_citizen_association}		[Common] - Create string param in dic    ${arg_dic}		citizen_association
    ${param_permanent_address_neighbourhood_association}		[Common] - Create string param in dic    ${arg_dic}		neighbourhood_association
    ${param_permanent_address_address}		[Common] - Create string param in dic    ${arg_dic}		address
    ${param_permanent_address_address_local}		[Common] - Create string param in dic    ${arg_dic}		address_local
    ${param_permanent_address_commune}		[Common] - Create string param in dic    ${arg_dic}		commune
    ${param_permanent_address_commune_local}		[Common] - Create string param in dic    ${arg_dic}		commune_local
    ${param_permanent_address_district}		[Common] - Create string param in dic    ${arg_dic}		district
    ${param_permanent_address_district_local}		[Common] - Create string param in dic    ${arg_dic}		district_local
    ${param_permanent_address_city}		[Common] - Create string param in dic    ${arg_dic}		city
    ${param_permanent_address_city_local}		[Common] - Create string param in dic    ${arg_dic}		city_local
    ${param_permanent_address_province}		[Common] - Create string param in dic    ${arg_dic}		province
    ${param_permanent_address_province_local}		[Common] - Create string param in dic    ${arg_dic}		province_local
    ${param_permanent_address_postal_code}		[Common] - Create string param in dic    ${arg_dic}		postal_code
    ${param_permanent_address_postal_code_local}		[Common] - Create string param in dic    ${arg_dic}		postal_code_local
    ${param_permanent_address_country}		[Common] - Create string param in dic    ${arg_dic}		country
    ${param_permanent_address_country_local}		[Common] - Create string param in dic    ${arg_dic}		country_local
    ${param_permanent_address_landmark}		[Common] - Create string param in dic    ${arg_dic}		landmark
    ${param_permanent_address_longitude}		[Common] - Create string param in dic    ${arg_dic}		longitude
    ${param_permanent_address_latitude}		[Common] - Create string param in dic    ${arg_dic}		latitude
    ${param_primary_identity_type}		[Common] - Create string param in dic    ${arg_dic}		type
    ${param_primary_identity_identity_id}		[Common] - Create string param in dic    ${arg_dic}		identity_id
    ${param_primary_identity_identity_id_local}		[Common] - Create string param in dic    ${arg_dic}		identity_id_local
    ${param_primary_identity_place_of_issue}		[Common] - Create string param in dic    ${arg_dic}		place_of_issue
    ${param_primary_identity_issue_date}		[Common] - Create string param in dic    ${arg_dic}		issue_date
    ${param_primary_identity_expired_date}		[Common] - Create string param in dic    ${arg_dic}		expired_date
    ${param_primary_identity_front_identity_url}		[Common] - Create string param in dic    ${arg_dic}		front_identity_url
    ${param_primary_identity_back_identity_url}		[Common] - Create string param in dic    ${arg_dic}		back_identity_url
    ${param_primary_identity_signature_url}		[Common] - Create string param in dic    ${arg_dic}		signature_url
    ${param_secondary_identity_type}		[Common] - Create string param in dic    ${arg_dic}		type
    ${param_secondary_identity_identity_id}		[Common] - Create string param in dic    ${arg_dic}		identity_id
    ${param_secondary_identity_identity_id_local}		[Common] - Create string param in dic    ${arg_dic}		identity_id_local
    ${param_secondary_identity_place_of_issue}		[Common] - Create string param in dic    ${arg_dic}		place_of_issue
    ${param_secondary_identity_issue_date}		[Common] - Create string param in dic    ${arg_dic}		issue_date
    ${param_secondary_identity_expired_date}		[Common] - Create string param in dic    ${arg_dic}		expired_date
    ${param_secondary_identity_front_identity_url}		[Common] - Create string param in dic    ${arg_dic}		front_identity_url
    ${param_secondary_identity_back_identity_url}		[Common] - Create string param in dic    ${arg_dic}		back_identity_url
    ${param_secondary_identity_signature_url}		[Common] - Create string param in dic    ${arg_dic}		signature_url
    ${param_tertiary_identity_type}		[Common] - Create string param in dic    ${arg_dic}		type
    ${param_tertiary_identity_identity_id}		[Common] - Create string param in dic    ${arg_dic}		identity_id
    ${param_tertiary_identity_identity_id_local}		[Common] - Create string param in dic    ${arg_dic}		identity_id_local
    ${param_tertiary_identity_place_of_issue}		[Common] - Create string param in dic    ${arg_dic}		place_of_issue
    ${param_tertiary_identity_issue_date}		[Common] - Create string param in dic    ${arg_dic}		issue_date
    ${param_tertiary_identity_expired_date}		[Common] - Create string param in dic    ${arg_dic}		expired_date
    ${param_tertiary_identity_front_identity_url}		[Common] - Create string param in dic    ${arg_dic}		front_identity_url
    ${param_tertiary_identity_back_identity_url}		[Common] - Create string param in dic    ${arg_dic}		back_identity_url
    ${param_tertiary_identity_signature_url}		[Common] - Create string param in dic    ${arg_dic}		signature_url
    ${param_additional_profile_picture_url}		[Common] - Create string param in dic    ${arg_dic}		profile_picture_url
    ${param_additional_tax_id_card_photo_url}		[Common] - Create string param in dic    ${arg_dic}		tax_id_card_photo_url
    ${param_identity_identity_type_id}		[Common] - Create int param in dic    ${arg_dic}		identity_type_id
    ${param_identity_username}		[Common] - Create string param in dic    ${arg_dic}		username
    ${param_identity_password}		[Common] - Create string param in dic    ${arg_dic}		password
    ${param_identity_auto_generate_password}		[Common] - Create boolean param in dic    ${arg_dic}		auto_generate_password

    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}

    run keyword if   '${arg_dic.header_device_id}'!='${param_not_used}'      set to dictionary       ${header}       device_id       ${arg_dic.header_device_id}

    ${data}              catenate           SEPARATOR=
        ...          {
        ...              "profile": {
        ...                 ${param_referrer_user_type}
        ...                 ${param_referrer_user_id}
        ...                 ${param_unique_reference}
        ...                 ${param_beneficiary}
        ...                 ${param_mm_card_type_id}
        ...                 ${param_mm_card_level_id}
        ...                 ${param_mm_factory_card_number}
        ...                 ${param_is_require_otp}
        ...                 ${param_tin_number}
        ...                 ${param_tin_number_local}
        ...                 ${param_title}
        ...                 ${param_title_local}
        ...                 ${param_first_name}
        ...                 ${param_first_name_local}
        ...                 ${param_middle_name}
        ...                 ${param_middle_name_local}
        ...                 ${param_last_name}
        ...                 ${param_last_name_local}
        ...                 ${param_suffix}
        ...                 ${param_suffix_local}
        ...                 ${param_date_of_birth}
        ...                 ${param_place_of_birth}
        ...                 ${param_place_of_birth_local}
        ...                 ${param_gender}
        ...                 ${param_gender_local}
        ...                 ${param_ethnicity}
        ...                 ${param_employer}
        ...                 ${param_nationality}
        ...                 ${param_occupation}
        ...                 ${param_occupation_local}
        ...                 ${param_occupation_title}
        ...                 ${param_occupation_title_local}
        ...                 ${param_township_code}
        ...                 ${param_township_name}
        ...                 ${param_township_name_local}
        ...                 ${param_mother_name}
        ...                 ${param_mother_name_local}
        ...                 ${param_mother_maiden_name}
        ...                 ${param_mother_maiden_name_local}
        ...                 ${param_civil_status}
        ...                 ${param_email}
        ...                 ${param_primary_mobile_number}
        ...                 ${param_secondary_mobile_number}
        ...                 ${param_tertiary_mobile_number}
        ...                 ${param_telephone_number}
        ...                 "address": {
        ...                     "current_address": {
        ...      					${param_current_address_citizen_association}
        ...      					${param_current_address_neighbourhood_association}
        ...      					${param_current_address_address}
        ...      					${param_current_address_commune}
        ...      					${param_current_address_district}
        ...      					${param_current_address_city}
        ...      					${param_current_address_province}
        ...      					${param_current_address_postal_code}
        ...      					${param_current_address_country}
        ...      					${param_current_address_landmark}
        ...      					${param_current_address_longitude}
        ...      					${param_current_address_latitude}
        ...      					${param_current_address_address_local}
        ...      					${param_current_address_commune_local}
        ...      					${param_current_address_district_local}
        ...      					${param_current_address_city_local}
        ...      					${param_current_address_province_local}
        ...      					${param_current_address_postal_code_local}
        ...      					${param_current_address_country_local}
        ...                     },
        ...                     "permanent_address": {
        ...      					${param_permanent_address_citizen_association}
        ...      					${param_permanent_address_neighbourhood_association}
        ...      					${param_permanent_address_address}
        ...      					${param_permanent_address_commune}
        ...      					${param_permanent_address_district}
        ...      					${param_permanent_address_city}
        ...      					${param_permanent_address_province}
        ...      					${param_permanent_address_postal_code}
        ...      					${param_permanent_address_country}
        ...      					${param_permanent_address_landmark}
        ...      					${param_permanent_address_longitude}
        ...      					${param_permanent_address_latitude}
        ...      					${param_permanent_address_address_local}
        ...      					${param_permanent_address_commune_local}
        ...      					${param_permanent_address_district_local}
        ...      					${param_permanent_address_city_local}
        ...      					${param_permanent_address_province_local}
        ...      					${param_permanent_address_postal_code_local}
        ...      					${param_permanent_address_country_local}
        ...                     }
        ...                 },
        ...                 "kyc": {
        ...                     "primary_identity": {
        ...      					${param_primary_identity_type}
        ...      					${param_primary_identity_identity_id}
        ...      					${param_primary_identity_identity_id_local}
        ...      					${param_primary_identity_place_of_issue}
        ...      					${param_primary_identity_issue_date}
        ...      					${param_primary_identity_expired_date}
        ...      					${param_primary_identity_front_identity_url}
        ...      					${param_primary_identity_back_identity_url}
        ...      					${param_primary_identity_signature_url}
        ...                     },
        ...                     "secondary_identity": {
        ...      					${param_secondary_identity_type}
        ...      					${param_secondary_identity_identity_id}
        ...      					${param_secondary_identity_identity_id_local}
        ...      					${param_secondary_identity_place_of_issue}
        ...      					${param_secondary_identity_issue_date}
        ...      					${param_secondary_identity_expired_date}
        ...      					${param_secondary_identity_front_identity_url}
        ...      					${param_secondary_identity_back_identity_url}
        ...      					${param_secondary_identity_signature_url}
        ...                     },
        ...                     "tertiary_identity": {
        ...      					${param_tertiary_identity_type}
        ...      					${param_tertiary_identity_identity_id}
        ...      					${param_tertiary_identity_identity_id_local}
        ...      					${param_tertiary_identity_place_of_issue}
        ...      					${param_tertiary_identity_issue_date}
        ...      					${param_tertiary_identity_expired_date}
        ...      					${param_tertiary_identity_front_identity_url}
        ...      					${param_tertiary_identity_back_identity_url}
        ...      					${param_tertiary_identity_signature_url}
        ...                     }
        ...                 },
        ...                 "additional": {
        ...      				${param_additional_profile_picture_url}
        ...      				${param_additional_tax_id_card_photo_url}
        ...                 }
        ...              },
        ...              "identity": {
        ...      			${param_identity_identity_type_id}
        ...      			${param_identity_username}
        ...      			${param_identity_password}
        ...      			${param_identity_auto_generate_password}
        ...              }
        ...          }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${register_customer_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API system user creates customer profile
    [Arguments]     ${arg_dic}
    ${param_referrer_user_type}     create json data for user type object       referrer_user_type   ${arg_dic.referrer_user_type_id}      ${arg_dic.referrer_user_type_name}
    ${param_is_member}		[Common] - Create boolean param		is_member		${arg_dic.is_member}
    ${param_is_testing_account}		[Common] - Create boolean param		is_testing_account		${arg_dic.is_testing_account}
    ${param_is_system_account}		[Common] - Create boolean param		is_system_account		${arg_dic.is_system_account}
    ${param_acquisition_source}		[Common] - Create string param		acquisition_source		${arg_dic.acquisition_source}
    ${param_customer_classification_id}		[Common] - Create int param		customer_classification_id		${arg_dic.customer_classification_id}
    ${param_referrer_user_id}		[Common] - Create int param		referrer_user_id		${arg_dic.referrer_user_id}
    ${param_unique_reference}		[Common] - Create string param		unique_reference		${arg_dic.unique_reference}
    ${param_beneficiary}		[Common] - Create string param		beneficiary		${arg_dic.beneficiary}
    ${param_mm_card_type_id}		[Common] - Create int param		mm_card_type_id		${arg_dic.mm_card_type_id}
    ${param_mm_card_level_id}		[Common] - Create int param		mm_card_level_id		${arg_dic.mm_card_level_id}
    ${param_mm_factory_card_number}		[Common] - Create string param		mm_factory_card_number		${arg_dic.mm_factory_card_number}
    ${param_is_require_otp}		[Common] - Create boolean param		is_require_otp		${arg_dic.is_require_otp}
    ${param_tin_number}		[Common] - Create string param		tin_number		${arg_dic.tin_number}
    ${param_tin_number_local}		[Common] - Create string param		tin_number_local		${arg_dic.tin_number_local}
    ${param_title}		[Common] - Create string param		title		${arg_dic.title}
    ${param_title_local}		[Common] - Create string param		title_local		${arg_dic.title_local}
    ${param_first_name}		[Common] - Create string param		first_name		${arg_dic.first_name}
    ${param_first_name_local}		[Common] - Create string param		first_name_local		${arg_dic.first_name_local}
    ${param_middle_name}		[Common] - Create string param		middle_name		${arg_dic.middle_name}
    ${param_middle_name_local}		[Common] - Create string param		middle_name_local		${arg_dic.middle_name_local}
    ${param_last_name}		[Common] - Create string param		last_name		${arg_dic.last_name}
    ${param_last_name_local}		[Common] - Create string param		last_name_local		${arg_dic.last_name_local}
    ${param_suffix}		[Common] - Create string param		suffix		${arg_dic.suffix}
    ${param_suffix_local}		[Common] - Create string param		suffix_local		${arg_dic.suffix_local}
    ${param_date_of_birth}		[Common] - Create string param		date_of_birth		${arg_dic.date_of_birth}
    ${param_place_of_birth}		[Common] - Create string param		place_of_birth		${arg_dic.place_of_birth}
    ${param_place_of_birth_local}		[Common] - Create string param		place_of_birth_local		${arg_dic.place_of_birth_local}
    ${param_gender}		[Common] - Create string param		gender		${arg_dic.gender}
    ${param_gender_local}		[Common] - Create string param		gender_local		${arg_dic.gender_local}
    ${param_ethnicity}		[Common] - Create string param		ethnicity		${arg_dic.ethnicity}
    ${param_employer}		[Common] - Create string param		employer		${arg_dic.employer}
    ${param_nationality}		[Common] - Create string param		nationality		${arg_dic.nationality}
    ${param_occupation}		[Common] - Create string param		occupation		${arg_dic.occupation}
    ${param_occupation_local}		[Common] - Create string param		occupation_local		${arg_dic.occupation_local}
    ${param_occupation_title}		[Common] - Create string param		occupation_title		${arg_dic.occupation_title}
    ${param_occupation_title_local}		[Common] - Create string param		occupation_title_local		${arg_dic.occupation_title_local}
    ${param_township_code}		[Common] - Create string param		township_code		${arg_dic.township_code}
    ${param_township_name}		[Common] - Create string param		township_name		${arg_dic.township_name}
    ${param_township_name_local}		[Common] - Create string param		township_name_local		${arg_dic.township_name_local}
    ${param_mother_name}		[Common] - Create string param		mother_name		${arg_dic.mother_name}
    ${param_mother_name_local}		[Common] - Create string param		mother_name_local		${arg_dic.mother_name_local}
    ${param_mother_maiden_name}		[Common] - Create string param		mother_maiden_name		${arg_dic.mother_maiden_name}
    ${param_mother_maiden_name_local}		[Common] - Create string param		mother_maiden_name_local		${arg_dic.mother_maiden_name_local}
    ${param_civil_status}		[Common] - Create string param		civil_status		${arg_dic.civil_status}
    ${param_email}		[Common] - Create string param		email		${arg_dic.email}
    ${param_primary_mobile_number}		[Common] - Create string param		primary_mobile_number		${arg_dic.primary_mobile_number}
    ${param_secondary_mobile_number}		[Common] - Create string param		secondary_mobile_number		${arg_dic.secondary_mobile_number}
    ${param_tertiary_mobile_number}		[Common] - Create string param		tertiary_mobile_number		${arg_dic.tertiary_mobile_number}
    ${param_telephone_number}		[Common] - Create string param		telephone_number		${arg_dic.telephone_number}
    ${param_current_address_citizen_association}		[Common] - Create string param		citizen_association		${arg_dic.current_address_citizen_association}
    ${param_current_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${arg_dic.current_address_neighbourhood_association}
    ${param_current_address_address}		[Common] - Create string param		address		${arg_dic.current_address_address}
    ${param_current_address_address_local}		[Common] - Create string param		address_local		${arg_dic.current_address_address_local}
    ${param_current_address_commune}		[Common] - Create string param		commune		${arg_dic.current_address_commune}
    ${param_current_address_commune_local}		[Common] - Create string param		commune_local		${arg_dic.current_address_commune_local}
    ${param_current_address_district}		[Common] - Create string param		district		${arg_dic.current_address_district}
    ${param_current_address_district_local}		[Common] - Create string param		district_local		${arg_dic.current_address_district_local}
    ${param_current_address_city}		[Common] - Create string param		city		${arg_dic.current_address_city}
    ${param_current_address_city_local}		[Common] - Create string param		city_local		${arg_dic.current_address_city_local}
    ${param_current_address_province}		[Common] - Create string param		province		${arg_dic.current_address_province}
    ${param_current_address_province_local}		[Common] - Create string param		province_local		${arg_dic.current_address_province_local}
    ${param_current_address_postal_code}		[Common] - Create string param		postal_code		${arg_dic.current_address_postal_code}
    ${param_current_address_postal_code_local}		[Common] - Create string param		postal_code_local		${arg_dic.current_address_postal_code_local}
    ${param_current_address_country}		[Common] - Create string param		country		${arg_dic.current_address_country}
    ${param_current_address_country_local}		[Common] - Create string param		country_local	${arg_dic.current_address_country_local}
    ${param_current_address_landmark}		[Common] - Create string param		landmark		${arg_dic.current_address_landmark}
    ${param_current_address_longitude}		[Common] - Create string param		longitude		${arg_dic.current_address_longitude}
    ${param_current_address_latitude}		[Common] - Create string param		latitude		${arg_dic.current_address_latitude}
    ${param_permanent_address_citizen_association}		[Common] - Create string param		citizen_association		${arg_dic.permanent_address_citizen_association}
    ${param_permanent_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${arg_dic.permanent_address_neighbourhood_association}
    ${param_permanent_address_address}		[Common] - Create string param		address		${arg_dic.permanent_address_address}
    ${param_permanent_address_address_local}		[Common] - Create string param		address_local		${arg_dic.permanent_address_address_local}
    ${param_permanent_address_commune}		[Common] - Create string param		commune		${arg_dic.permanent_address_commune}
    ${param_permanent_address_commune_local}		[Common] - Create string param		commune_local		${arg_dic.permanent_address_commune_local}
    ${param_permanent_address_district}		[Common] - Create string param		district		${arg_dic.permanent_address_district}
    ${param_permanent_address_district_local}		[Common] - Create string param		district_local		${arg_dic.permanent_address_district_local}
    ${param_permanent_address_city}		[Common] - Create string param		city		${arg_dic.permanent_address_city}
    ${param_permanent_address_city_local}		[Common] - Create string param		city_local		${arg_dic.permanent_address_city_local}
    ${param_permanent_address_province}		[Common] - Create string param		province		${arg_dic.permanent_address_province}
    ${param_permanent_address_province_local}		[Common] - Create string param		province_local		${arg_dic.permanent_address_province_local}
    ${param_permanent_address_postal_code}		[Common] - Create string param		postal_code		${arg_dic.permanent_address_postal_code}
    ${param_permanent_address_postal_code_local}		[Common] - Create string param		postal_code_local		${arg_dic.permanent_address_postal_code_local}
    ${param_permanent_address_country}		[Common] - Create string param		country		${arg_dic.permanent_address_country}
    ${param_permanent_address_country_local}		[Common] - Create string param		country_local		${arg_dic.permanent_address_country_local}
    ${param_permanent_address_landmark}		[Common] - Create string param		landmark		${arg_dic.permanent_address_landmark}
    ${param_permanent_address_longitude}		[Common] - Create string param		longitude		${arg_dic.permanent_address_longitude}
    ${param_permanent_address_latitude}		[Common] - Create string param		latitude		${arg_dic.permanent_address_latitude}
    ${param_primary_identity_type}		[Common] - Create string param		type		${arg_dic.primary_identity_type}
    ${param_primary_identity_status}		[Common] - Create int param		status		${arg_dic.primary_identity_status}
    ${param_primary_identity_identity_id}		[Common] - Create string param		identity_id		${arg_dic.primary_identity_identity_id}
    ${param_primary_identity_identity_id_local}		[Common] - Create string param		identity_id_local		${arg_dic.primary_identity_identity_id_local}
    ${param_primary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${arg_dic.primary_identity_place_of_issue}
    ${param_primary_identity_issue_date}		[Common] - Create string param		issue_date		${arg_dic.primary_identity_issue_date}
    ${param_primary_identity_expired_date}		[Common] - Create string param		expired_date		${arg_dic.primary_identity_expired_date}
    ${param_primary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${arg_dic.primary_identity_front_identity_url}
    ${param_primary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${arg_dic.primary_identity_back_identity_url}
    ${param_primary_identity_signature_url}		[Common] - Create string param		signature_url		${arg_dic.primary_identity_signature_url}
    ${param_secondary_identity_type}		[Common] - Create string param		type		${arg_dic.secondary_identity_type}
    ${param_secondary_identity_status}		[Common] - Create int param		status		${arg_dic.secondary_identity_status}
    ${param_secondary_identity_identity_id}		[Common] - Create string param		identity_id		${arg_dic.secondary_identity_identity_id}
    ${param_secondary_identity_identity_id_local}		[Common] - Create string param		identity_id_local		${arg_dic.secondary_identity_identity_id_local}
    ${param_secondary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${arg_dic.secondary_identity_place_of_issue}
    ${param_secondary_identity_issue_date}		[Common] - Create string param		issue_date		${arg_dic.secondary_identity_issue_date}
    ${param_secondary_identity_expired_date}		[Common] - Create string param		expired_date		${arg_dic.secondary_identity_expired_date}
    ${param_secondary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${arg_dic.secondary_identity_front_identity_url}
    ${param_secondary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${arg_dic.secondary_identity_back_identity_url}
    ${param_secondary_identity_signature_url}		[Common] - Create string param		signature_url		${arg_dic.secondary_identity_signature_url}
    ${param_tertiary_identity_type}		[Common] - Create string param		type		${arg_dic.tertiary_identity_type}
    ${param_tertiary_identity_status}		[Common] - Create int param		status		${arg_dic.tertiary_identity_status}
    ${param_tertiary_identity_identity_id}		[Common] - Create string param		identity_id		${arg_dic.tertiary_identity_identity_id}
    ${param_tertiary_identity_identity_id_local}		[Common] - Create string param		identity_id_local		${arg_dic.tertiary_identity_identity_id_local}
    ${param_tertiary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${arg_dic.tertiary_identity_place_of_issue}
    ${param_tertiary_identity_issue_date}		[Common] - Create string param		issue_date		${arg_dic.tertiary_identity_issue_date}
    ${param_tertiary_identity_expired_date}		[Common] - Create string param		expired_date		${arg_dic.tertiary_identity_expired_date}
    ${param_tertiary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${arg_dic.tertiary_identity_front_identity_url}
    ${param_tertiary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${arg_dic.tertiary_identity_back_identity_url}
    ${param_tertiary_identity_signature_url}		[Common] - Create string param		signature_url		${arg_dic.tertiary_identity_signature_url}
    ${param_kyc_level}		[Common] - Create int param		level		${arg_dic.kyc_level}
    ${param_kyc_remark}		[Common] - Create string param		remark		${arg_dic.kyc_remark}
    ${param_kyc_verify_by}		[Common] - Create string param		verify_by		${arg_dic.kyc_verify_by}
    ${param_kyc_verify_date}		[Common] - Create string param		verify_date		${arg_dic.kyc_verify_date}
    ${param_kyc_risk_level}		[Common] - Create string param		risk_level		${arg_dic.kyc_risk_level}
    ${param_additional_profile_picture_url}		[Common] - Create string param		profile_picture_url		${arg_dic.additional_profile_picture_url}
    ${param_additional_tax_id_card_photo_url}		[Common] - Create string param		tax_id_card_photo_url		${arg_dic.additional_tax_id_card_photo_url}
    ${param_additional_field_1_name}		[Common] - Create string param		field_1_name		${arg_dic.additional_field_1_name}
    ${param_additional_field_1_value}		[Common] - Create string param		field_1_value		${arg_dic.additional_field_1_value}
    ${param_additional_field_2_name}		[Common] - Create string param		field_2_name		${arg_dic.additional_field_2_name}
    ${param_additional_field_2_value}		[Common] - Create string param		field_2_value		${arg_dic.additional_field_2_value}
    ${param_additional_field_3_name}		[Common] - Create string param		field_3_name		${arg_dic.additional_field_3_name}
    ${param_additional_field_3_value}		[Common] - Create string param		field_3_value		${arg_dic.additional_field_3_value}
    ${param_additional_field_4_name}		[Common] - Create string param		field_4_name		${arg_dic.additional_field_4_name}
    ${param_additional_field_4_value}		[Common] - Create string param		field_4_value		${arg_dic.additional_field_4_value}
    ${param_additional_field_5_name}		[Common] - Create string param		field_5_name		${arg_dic.additional_field_5_name}
    ${param_additional_field_5_value}		[Common] - Create string param		field_5_value		${arg_dic.additional_field_5_value}
    ${param_additional_supporting_file_1_url}		[Common] - Create string param		supporting_file_1_url		${arg_dic.additional_supporting_file_1_url}
    ${param_additional_supporting_file_2_url}		[Common] - Create string param		supporting_file_2_url		${arg_dic.additional_supporting_file_2_url}
    ${param_additional_supporting_file_3_url}		[Common] - Create string param		supporting_file_3_url		${arg_dic.additional_supporting_file_3_url}
    ${param_additional_supporting_file_4_url}		[Common] - Create string param		supporting_file_4_url		${arg_dic.additional_supporting_file_4_url}
    ${param_additional_supporting_file_5_url}		[Common] - Create string param		supporting_file_5_url		${arg_dic.additional_supporting_file_5_url}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...  {
        ...                 ${param_is_member}
        ...                 ${param_is_testing_account}
        ...                 ${param_is_system_account}
        ...                 ${param_acquisition_source}
        ...                 ${param_customer_classification_id}
        ...                 ${param_referrer_user_type}
        ...                 ${param_referrer_user_id}
        ...                 ${param_unique_reference}
        ...                 ${param_beneficiary}
        ...                 ${param_mm_card_type_id}
        ...                 ${param_mm_card_level_id}
        ...                 ${param_mm_factory_card_number}
        ...                 ${param_is_require_otp}
        ...                 ${param_tin_number}
        ...                 ${param_tin_number_local}
        ...                 ${param_title}
        ...                 ${param_title_local}
        ...                 ${param_first_name}
        ...                 ${param_first_name_local}
        ...                 ${param_middle_name}
        ...                 ${param_middle_name_local}
        ...                 ${param_last_name}
        ...                 ${param_last_name_local}
        ...                 ${param_suffix}
        ...                 ${param_suffix_local}
        ...                 ${param_date_of_birth}
        ...                 ${param_place_of_birth}
        ...                 ${param_place_of_birth_local}
        ...                 ${param_gender}
        ...                 ${param_gender_local}
        ...                 ${param_ethnicity}
        ...                 ${param_employer}
        ...                 ${param_nationality}
        ...                 ${param_occupation}
        ...                 ${param_occupation_local}
        ...                 ${param_occupation_title}
        ...                 ${param_occupation_title_local}
        ...                 ${param_township_code}
        ...                 ${param_township_name}
        ...                 ${param_township_name_local}
        ...                 ${param_mother_name}
        ...                 ${param_mother_name_local}
        ...                 ${param_mother_maiden_name}
        ...                 ${param_mother_maiden_name_local}
        ...                 ${param_civil_status}
        ...                 ${param_email}
        ...                 ${param_primary_mobile_number}
        ...                 ${param_secondary_mobile_number}
        ...                 ${param_tertiary_mobile_number}
        ...                 ${param_telephone_number}
        ...                 "address": {
        ...                     "current_address": {
        ...      					${param_current_address_citizen_association}
        ...      					${param_current_address_neighbourhood_association}
        ...      					${param_current_address_address}
        ...      					${param_current_address_commune}
        ...      					${param_current_address_district}
        ...      					${param_current_address_city}
        ...      					${param_current_address_province}
        ...      					${param_current_address_postal_code}
        ...      					${param_current_address_country}
        ...      					${param_current_address_landmark}
        ...      					${param_current_address_longitude}
        ...      					${param_current_address_latitude}
        ...      					${param_current_address_address_local}
        ...      					${param_current_address_commune_local}
        ...      					${param_current_address_district_local}
        ...      					${param_current_address_city_local}
        ...      					${param_current_address_province_local}
        ...      					${param_current_address_postal_code_local}
        ...      					${param_current_address_country_local}
        ...                     },
        ...                     "permanent_address": {
        ...      					${param_permanent_address_citizen_association}
        ...      					${param_permanent_address_neighbourhood_association}
        ...      					${param_permanent_address_address}
        ...      					${param_permanent_address_commune}
        ...      					${param_permanent_address_district}
        ...      					${param_permanent_address_city}
        ...      					${param_permanent_address_province}
        ...      					${param_permanent_address_postal_code}
        ...      					${param_permanent_address_country}
        ...      					${param_permanent_address_landmark}
        ...      					${param_permanent_address_longitude}
        ...      					${param_permanent_address_latitude}
        ...      					${param_permanent_address_address_local}
        ...      					${param_permanent_address_commune_local}
        ...      					${param_permanent_address_district_local}
        ...      					${param_permanent_address_city_local}
        ...      					${param_permanent_address_province_local}
        ...      					${param_permanent_address_postal_code_local}
        ...      					${param_permanent_address_country_local}
        ...                     }
        ...                 },
        ...                 "kyc": {
        ...                     "primary_identity": {
        ...      					${param_primary_identity_type}
        ...      					${param_primary_identity_status}
        ...      					${param_primary_identity_identity_id}
        ...      					${param_primary_identity_identity_id_local}
        ...      					${param_primary_identity_place_of_issue}
        ...      					${param_primary_identity_issue_date}
        ...      					${param_primary_identity_expired_date}
        ...      					${param_primary_identity_front_identity_url}
        ...      					${param_primary_identity_back_identity_url}
        ...      					${param_primary_identity_signature_url}
        ...                     },
        ...                     "secondary_identity": {
        ...      					${param_secondary_identity_type}
        ...      					${param_secondary_identity_status}
        ...      					${param_secondary_identity_identity_id}
        ...      					${param_secondary_identity_identity_id_local}
        ...      					${param_secondary_identity_place_of_issue}
        ...      					${param_secondary_identity_issue_date}
        ...      					${param_secondary_identity_expired_date}
        ...      					${param_secondary_identity_front_identity_url}
        ...      					${param_secondary_identity_back_identity_url}
        ...      					${param_secondary_identity_signature_url}
        ...                     },
        ...                     "tertiary_identity": {
        ...      					${param_tertiary_identity_type}
        ...      					${param_tertiary_identity_status}
        ...      					${param_tertiary_identity_identity_id}
        ...      					${param_tertiary_identity_identity_id_local}
        ...      					${param_tertiary_identity_place_of_issue}
        ...      					${param_tertiary_identity_issue_date}
        ...      					${param_tertiary_identity_expired_date}
        ...      					${param_tertiary_identity_front_identity_url}
        ...      					${param_tertiary_identity_back_identity_url}
        ...      					${param_tertiary_identity_signature_url}
        ...                     },
        ...      				${param_kyc_level}
        ...      				${param_kyc_remark}
        ...      				${param_kyc_verify_by}
        ...      				${param_kyc_verify_date}
        ...      				${param_kyc_risk_level}
        ...                 },
        ...                 "additional": {
        ...      				${param_additional_profile_picture_url}
        ...      				${param_additional_tax_id_card_photo_url}
        ...      				${param_additional_field_1_name}
        ...      				${param_additional_field_1_value}
        ...      				${param_additional_field_2_name}
        ...      				${param_additional_field_2_value}
        ...      				${param_additional_field_3_name}
        ...      				${param_additional_field_3_value}
        ...      				${param_additional_field_4_name}
        ...      				${param_additional_field_4_value}
        ...      				${param_additional_field_5_name}
        ...      				${param_additional_field_5_value}
        ...      				${param_additional_supporting_file_1_url}
        ...      				${param_additional_supporting_file_2_url}
        ...      				${param_additional_supporting_file_3_url}
        ...      				${param_additional_supporting_file_4_url}
        ...      				${param_additional_supporting_file_5_url}
        ...                 }
        ...  }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${system_user_create_customer_profile_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API customer registers customer profile
    [Arguments]     ${arg_dic}
    ${param_referrer_user_type}     create json data for user type object       referrer_user_type   ${arg_dic.referrer_user_type_id}      ${arg_dic.referrer_user_type_name}
    ${param_referrer_user_id}		[Common] - Create int param		referrer_user_id		${arg_dic.referrer_user_id}
    ${param_unique_reference}		[Common] - Create string param		unique_reference		${arg_dic.unique_reference}
    ${param_beneficiary}		[Common] - Create string param		beneficiary		${arg_dic.beneficiary}
    ${param_mm_card_type_id}		[Common] - Create int param		mm_card_type_id		${arg_dic.mm_card_type_id}
    ${param_mm_card_level_id}		[Common] - Create int param		mm_card_level_id		${arg_dic.mm_card_level_id}
    ${param_mm_factory_card_number}		[Common] - Create string param		mm_factory_card_number		${arg_dic.mm_factory_card_number}
    ${param_is_require_otp}		[Common] - Create boolean param		is_require_otp		${arg_dic.is_require_otp}
    ${param_tin_number}		[Common] - Create string param		tin_number		${arg_dic.tin_number}
    ${param_tin_number_local}		[Common] - Create string param		tin_number_local		${arg_dic.tin_number_local}
    ${param_title}		[Common] - Create string param		title		${arg_dic.title}
    ${param_title_local}		[Common] - Create string param		title_local		${arg_dic.title_local}
    ${param_first_name}		[Common] - Create string param		first_name		${arg_dic.first_name}
    ${param_first_name_local}		[Common] - Create string param		first_name_local		${arg_dic.first_name_local}
    ${param_middle_name}		[Common] - Create string param		middle_name		${arg_dic.middle_name}
    ${param_middle_name_local}		[Common] - Create string param		middle_name_local		${arg_dic.middle_name_local}
    ${param_last_name}		[Common] - Create string param		last_name		${arg_dic.last_name}
    ${param_last_name_local}		[Common] - Create string param		last_name_local		${arg_dic.last_name_local}
    ${param_suffix}		[Common] - Create string param		suffix		${arg_dic.suffix}
    ${param_suffix_local}		[Common] - Create string param		suffix_local		${arg_dic.suffix_local}
    ${param_date_of_birth}		[Common] - Create string param		date_of_birth		${arg_dic.date_of_birth}
    ${param_place_of_birth}		[Common] - Create string param		place_of_birth		${arg_dic.place_of_birth}
    ${param_place_of_birth_local}		[Common] - Create string param		place_of_birth_local		${arg_dic.place_of_birth_local}
    ${param_gender}		[Common] - Create string param		gender		${arg_dic.gender}
    ${param_gender_local}		[Common] - Create string param		gender_local		${arg_dic.gender_local}
    ${param_ethnicity}		[Common] - Create string param		ethnicity		${arg_dic.ethnicity}
    ${param_employer}		[Common] - Create string param		employer		${arg_dic.employer}
    ${param_nationality}		[Common] - Create string param		nationality		${arg_dic.nationality}
    ${param_occupation}		[Common] - Create string param		occupation		${arg_dic.occupation}
    ${param_occupation_local}		[Common] - Create string param		occupation_local		${arg_dic.occupation_local}
    ${param_occupation_title}		[Common] - Create string param		occupation_title		${arg_dic.occupation_title}
    ${param_occupation_title_local}		[Common] - Create string param		occupation_title_local		${arg_dic.occupation_title_local}
    ${param_township_code}		[Common] - Create string param		township_code		${arg_dic.township_code}
    ${param_township_name}		[Common] - Create string param		township_name		${arg_dic.township_name}
    ${param_township_name_local}		[Common] - Create string param		township_name_local		${arg_dic.township_name_local}
    ${param_mother_name}		[Common] - Create string param		mother_name		${arg_dic.mother_name}
    ${param_mother_name_local}		[Common] - Create string param		mother_name_local		${arg_dic.mother_name_local}
    ${param_mother_maiden_name}		[Common] - Create string param		mother_maiden_name		${arg_dic.mother_maiden_name}
    ${param_mother_maiden_name_local}		[Common] - Create string param		mother_maiden_name_local		${arg_dic.mother_maiden_name_local}
    ${param_civil_status}		[Common] - Create string param		civil_status		${arg_dic.civil_status}
    ${param_email}		[Common] - Create string param		email		${arg_dic.email}
    ${param_primary_mobile_number}		[Common] - Create string param		primary_mobile_number		${arg_dic.primary_mobile_number}
    ${param_secondary_mobile_number}		[Common] - Create string param		secondary_mobile_number		${arg_dic.secondary_mobile_number}
    ${param_tertiary_mobile_number}		[Common] - Create string param		tertiary_mobile_number		${arg_dic.tertiary_mobile_number}
    ${param_telephone_number}		[Common] - Create string param		telephone_number		${arg_dic.telephone_number}
    ${param_current_address_citizen_association}		[Common] - Create string param		citizen_association		${arg_dic.current_address_citizen_association}
    ${param_current_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${arg_dic.current_address_neighbourhood_association}
    ${param_current_address_address}		[Common] - Create string param		address		${arg_dic.current_address_address}
    ${param_current_address_address_local}		[Common] - Create string param		address_local		${arg_dic.current_address_address_local}
    ${param_current_address_commune}		[Common] - Create string param		commune		${arg_dic.current_address_commune}
    ${param_current_address_commune_local}		[Common] - Create string param		commune_local		${arg_dic.current_address_commune_local}
    ${param_current_address_district}		[Common] - Create string param		district		${arg_dic.current_address_district}
    ${param_current_address_district_local}		[Common] - Create string param		district_local		${arg_dic.current_address_district_local}
    ${param_current_address_city}		[Common] - Create string param		city		${arg_dic.current_address_city}
    ${param_current_address_city_local}		[Common] - Create string param		city_local		${arg_dic.current_address_city_local}
    ${param_current_address_province}		[Common] - Create string param		province		${arg_dic.current_address_province}
    ${param_current_address_province_local}		[Common] - Create string param		province_local		${arg_dic.current_address_province_local}
    ${param_current_address_postal_code}		[Common] - Create string param		postal_code		${arg_dic.current_address_postal_code}
    ${param_current_address_postal_code_local}		[Common] - Create string param		postal_code_local		${arg_dic.current_address_postal_code_local}
    ${param_current_address_country}		[Common] - Create string param		country		${arg_dic.current_address_country}
    ${param_current_address_country_local}		[Common] - Create string param		country_local	${arg_dic.current_address_country_local}
    ${param_current_address_landmark}		[Common] - Create string param		landmark		${arg_dic.current_address_landmark}
    ${param_current_address_longitude}		[Common] - Create string param		longitude		${arg_dic.current_address_longitude}
    ${param_current_address_latitude}		[Common] - Create string param		latitude		${arg_dic.current_address_latitude}
    ${param_permanent_address_citizen_association}		[Common] - Create string param		citizen_association		${arg_dic.permanent_address_citizen_association}
    ${param_permanent_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${arg_dic.permanent_address_neighbourhood_association}
    ${param_permanent_address_address}		[Common] - Create string param		address		${arg_dic.permanent_address_address}
    ${param_permanent_address_address_local}		[Common] - Create string param		address_local		${arg_dic.permanent_address_address_local}
    ${param_permanent_address_commune}		[Common] - Create string param		commune		${arg_dic.permanent_address_commune}
    ${param_permanent_address_commune_local}		[Common] - Create string param		commune_local		${arg_dic.permanent_address_commune_local}
    ${param_permanent_address_district}		[Common] - Create string param		district		${arg_dic.permanent_address_district}
    ${param_permanent_address_district_local}		[Common] - Create string param		district_local		${arg_dic.permanent_address_district_local}
    ${param_permanent_address_city}		[Common] - Create string param		city		${arg_dic.permanent_address_city}
    ${param_permanent_address_city_local}		[Common] - Create string param		city_local		${arg_dic.permanent_address_city_local}
    ${param_permanent_address_province}		[Common] - Create string param		province		${arg_dic.permanent_address_province}
    ${param_permanent_address_province_local}		[Common] - Create string param		province_local		${arg_dic.permanent_address_province_local}
    ${param_permanent_address_postal_code}		[Common] - Create string param		postal_code		${arg_dic.permanent_address_postal_code}
    ${param_permanent_address_postal_code_local}		[Common] - Create string param		postal_code_local		${arg_dic.permanent_address_postal_code_local}
    ${param_permanent_address_country}		[Common] - Create string param		country		${arg_dic.permanent_address_country}
    ${param_permanent_address_country_local}		[Common] - Create string param		country_local		${arg_dic.permanent_address_country_local}
    ${param_permanent_address_landmark}		[Common] - Create string param		landmark		${arg_dic.permanent_address_landmark}
    ${param_permanent_address_longitude}		[Common] - Create string param		longitude		${arg_dic.permanent_address_longitude}
    ${param_permanent_address_latitude}		[Common] - Create string param		latitude		${arg_dic.permanent_address_latitude}
    ${param_primary_identity_type}		[Common] - Create string param		type		${arg_dic.primary_identity_type}
    ${param_primary_identity_identity_id}		[Common] - Create string param		identity_id		${arg_dic.primary_identity_identity_id}
    ${param_primary_identity_identity_id_local}		[Common] - Create string param		identity_id_local		${arg_dic.primary_identity_identity_id_local}
    ${param_primary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${arg_dic.primary_identity_place_of_issue}
    ${param_primary_identity_issue_date}		[Common] - Create string param		issue_date		${arg_dic.primary_identity_issue_date}
    ${param_primary_identity_expired_date}		[Common] - Create string param		expired_date		${arg_dic.primary_identity_expired_date}
    ${param_primary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${arg_dic.primary_identity_front_identity_url}
    ${param_primary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${arg_dic.primary_identity_back_identity_url}
    ${param_primary_identity_signature_url}		[Common] - Create string param		signature_url		${arg_dic.primary_identity_signature_url}
    ${param_secondary_identity_type}		[Common] - Create string param		type		${arg_dic.secondary_identity_type}
    ${param_secondary_identity_identity_id}		[Common] - Create string param		identity_id		${arg_dic.secondary_identity_identity_id}
    ${param_secondary_identity_identity_id_local}		[Common] - Create string param		identity_id_local		${arg_dic.secondary_identity_identity_id_local}
    ${param_secondary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${arg_dic.secondary_identity_place_of_issue}
    ${param_secondary_identity_issue_date}		[Common] - Create string param		issue_date		${arg_dic.secondary_identity_issue_date}
    ${param_secondary_identity_expired_date}		[Common] - Create string param		expired_date		${arg_dic.secondary_identity_expired_date}
    ${param_secondary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${arg_dic.secondary_identity_front_identity_url}
    ${param_secondary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${arg_dic.secondary_identity_back_identity_url}
    ${param_secondary_identity_signature_url}		[Common] - Create string param		signature_url		${arg_dic.secondary_identity_signature_url}
    ${param_tertiary_identity_type}		[Common] - Create string param		type		${arg_dic.tertiary_identity_type}
    ${param_tertiary_identity_identity_id}		[Common] - Create string param		identity_id		${arg_dic.tertiary_identity_identity_id}
    ${param_tertiary_identity_identity_id_local}		[Common] - Create string param		identity_id_local		${arg_dic.tertiary_identity_identity_id_local}
    ${param_tertiary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${arg_dic.tertiary_identity_place_of_issue}
    ${param_tertiary_identity_issue_date}		[Common] - Create string param		issue_date		${arg_dic.tertiary_identity_issue_date}
    ${param_tertiary_identity_expired_date}		[Common] - Create string param		expired_date		${arg_dic.tertiary_identity_expired_date}
    ${param_tertiary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${arg_dic.tertiary_identity_front_identity_url}
    ${param_tertiary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${arg_dic.tertiary_identity_back_identity_url}
    ${param_tertiary_identity_signature_url}		[Common] - Create string param		signature_url		${arg_dic.tertiary_identity_signature_url}
    ${param_additional_profile_picture_url}		[Common] - Create string param		profile_picture_url		${arg_dic.additional_profile_picture_url}
    ${param_additional_tax_id_card_photo_url}		[Common] - Create string param		tax_id_card_photo_url		${arg_dic.additional_tax_id_card_photo_url}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...  {
        ...                 ${param_referrer_user_type}
        ...                 ${param_referrer_user_id}
        ...                 ${param_unique_reference}
        ...                 ${param_beneficiary}
        ...                 ${param_mm_card_type_id}
        ...                 ${param_mm_card_level_id}
        ...                 ${param_mm_factory_card_number}
        ...                 ${param_is_require_otp}
        ...                 ${param_tin_number}
        ...                 ${param_tin_number_local}
        ...                 ${param_title}
        ...                 ${param_title_local}
        ...                 ${param_first_name}
        ...                 ${param_first_name_local}
        ...                 ${param_middle_name}
        ...                 ${param_middle_name_local}
        ...                 ${param_last_name}
        ...                 ${param_last_name_local}
        ...                 ${param_suffix}
        ...                 ${param_suffix_local}
        ...                 ${param_date_of_birth}
        ...                 ${param_place_of_birth}
        ...                 ${param_place_of_birth_local}
        ...                 ${param_gender}
        ...                 ${param_gender_local}
        ...                 ${param_ethnicity}
        ...                 ${param_employer}
        ...                 ${param_nationality}
        ...                 ${param_occupation}
        ...                 ${param_occupation_local}
        ...                 ${param_occupation_title}
        ...                 ${param_occupation_title_local}
        ...                 ${param_township_code}
        ...                 ${param_township_name}
        ...                 ${param_township_name_local}
        ...                 ${param_mother_name}
        ...                 ${param_mother_name_local}
        ...                 ${param_mother_maiden_name}
        ...                 ${param_mother_maiden_name_local}
        ...                 ${param_civil_status}
        ...                 ${param_email}
        ...                 ${param_primary_mobile_number}
        ...                 ${param_secondary_mobile_number}
        ...                 ${param_tertiary_mobile_number}
        ...                 ${param_telephone_number}
        ...                 "address": {
        ...                     "current_address": {
        ...      					${param_current_address_citizen_association}
        ...      					${param_current_address_neighbourhood_association}
        ...      					${param_current_address_address}
        ...      					${param_current_address_commune}
        ...      					${param_current_address_district}
        ...      					${param_current_address_city}
        ...      					${param_current_address_province}
        ...      					${param_current_address_postal_code}
        ...      					${param_current_address_country}
        ...      					${param_current_address_landmark}
        ...      					${param_current_address_longitude}
        ...      					${param_current_address_latitude}
        ...      					${param_current_address_address_local}
        ...      					${param_current_address_commune_local}
        ...      					${param_current_address_district_local}
        ...      					${param_current_address_city_local}
        ...      					${param_current_address_province_local}
        ...      					${param_current_address_postal_code_local}
        ...      					${param_current_address_country_local}
        ...                     },
        ...                     "permanent_address": {
        ...      					${param_permanent_address_citizen_association}
        ...      					${param_permanent_address_neighbourhood_association}
        ...      					${param_permanent_address_address}
        ...      					${param_permanent_address_commune}
        ...      					${param_permanent_address_district}
        ...      					${param_permanent_address_city}
        ...      					${param_permanent_address_province}
        ...      					${param_permanent_address_postal_code}
        ...      					${param_permanent_address_country}
        ...      					${param_permanent_address_landmark}
        ...      					${param_permanent_address_longitude}
        ...      					${param_permanent_address_latitude}
        ...      					${param_permanent_address_address_local}
        ...      					${param_permanent_address_commune_local}
        ...      					${param_permanent_address_district_local}
        ...      					${param_permanent_address_city_local}
        ...      					${param_permanent_address_province_local}
        ...      					${param_permanent_address_postal_code_local}
        ...      					${param_permanent_address_country_local}
        ...                     }
        ...                 },
        ...                 "kyc": {
        ...                     "primary_identity": {
        ...      					${param_primary_identity_type}
        ...      					${param_primary_identity_identity_id}
        ...      					${param_primary_identity_identity_id_local}
        ...      					${param_primary_identity_place_of_issue}
        ...      					${param_primary_identity_issue_date}
        ...      					${param_primary_identity_expired_date}
        ...      					${param_primary_identity_front_identity_url}
        ...      					${param_primary_identity_back_identity_url}
        ...      					${param_primary_identity_signature_url}
        ...                     },
        ...                     "secondary_identity": {
        ...      					${param_secondary_identity_type}
        ...      					${param_secondary_identity_identity_id}
        ...      					${param_secondary_identity_identity_id_local}
        ...      					${param_secondary_identity_place_of_issue}
        ...      					${param_secondary_identity_issue_date}
        ...      					${param_secondary_identity_expired_date}
        ...      					${param_secondary_identity_front_identity_url}
        ...      					${param_secondary_identity_back_identity_url}
        ...      					${param_secondary_identity_signature_url}
        ...                     },
        ...                     "tertiary_identity": {
        ...      					${param_tertiary_identity_type}
        ...      					${param_tertiary_identity_identity_id}
        ...      					${param_tertiary_identity_identity_id_local}
        ...      					${param_tertiary_identity_place_of_issue}
        ...      					${param_tertiary_identity_issue_date}
        ...      					${param_tertiary_identity_expired_date}
        ...      					${param_tertiary_identity_front_identity_url}
        ...      					${param_tertiary_identity_back_identity_url}
        ...      					${param_tertiary_identity_signature_url}
        ...                     }
        ...                 },
        ...                 "additional": {
        ...      				${param_additional_profile_picture_url}
        ...      				${param_additional_tax_id_card_photo_url}
        ...                 }
        ...  }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${register_customer_profile_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API system user updates customer profile
    [Arguments]     ${arg_dic}
#    ${param_referrer_user_type}     create json data for user type object       referrer_user_type
    ${param_is_testing_account}     [Common] - Create boolean param in dic    ${arg_dic}        is_testing_account
    ${param_is_system_account}      [Common] - Create boolean param in dic    ${arg_dic}        is_system_account
    ${param_acquisition_source}     [Common] - Create string param in dic    ${arg_dic}     acquisition_source
    ${param_customer_classification_id}     [Common] - Create int param in dic    ${arg_dic}        customer_classification_id
    ${param_referrer_user_id}       [Common] - Create int param in dic    ${arg_dic}        referrer_user_id
    ${param_unique_reference}       [Common] - Create string param in dic    ${arg_dic}     unique_reference
    ${param_beneficiary}        [Common] - Create string param in dic    ${arg_dic}     beneficiary
    ${param_mm_card_type_id}        [Common] - Create int param in dic    ${arg_dic}        mm_card_type_id
    ${param_mm_card_level_id}       [Common] - Create int param in dic    ${arg_dic}        mm_card_level_id
    ${param_mm_factory_card_number}     [Common] - Create string param in dic    ${arg_dic}     mm_factory_card_number
    ${param_is_require_otp}     [Common] - Create boolean param in dic    ${arg_dic}        is_require_otp
    ${param_tin_number}     [Common] - Create string param in dic    ${arg_dic}     tin_number
    ${param_tin_number_local}       [Common] - Create string param in dic    ${arg_dic}     tin_number_local
    ${param_title}      [Common] - Create string param in dic    ${arg_dic}     title
    ${param_title_local}        [Common] - Create string param in dic    ${arg_dic}     title_local
    ${param_first_name}     [Common] - Create string param in dic    ${arg_dic}     first_name
    ${param_first_name_local}       [Common] - Create string param in dic    ${arg_dic}     first_name_local
    ${param_middle_name}        [Common] - Create string param in dic    ${arg_dic}     middle_name
    ${param_middle_name_local}      [Common] - Create string param in dic    ${arg_dic}     middle_name_local
    ${param_last_name}      [Common] - Create string param in dic    ${arg_dic}     last_name
    ${param_last_name_local}        [Common] - Create string param in dic    ${arg_dic}     last_name_local
    ${param_suffix}     [Common] - Create string param in dic    ${arg_dic}     suffix
    ${param_suffix_local}       [Common] - Create string param in dic    ${arg_dic}     suffix_local
    ${param_date_of_birth}      [Common] - Create string param in dic    ${arg_dic}     date_of_birth
    ${param_place_of_birth}     [Common] - Create string param in dic    ${arg_dic}     place_of_birth
    ${param_place_of_birth_local}       [Common] - Create string param in dic    ${arg_dic}     place_of_birth_local
    ${param_gender}     [Common] - Create string param in dic    ${arg_dic}     gender
    ${param_gender_local}       [Common] - Create string param in dic    ${arg_dic}     gender_local
    ${param_ethnicity}      [Common] - Create string param in dic    ${arg_dic}     ethnicity
    ${param_employer}       [Common] - Create string param in dic    ${arg_dic}     employer
    ${param_nationality}        [Common] - Create string param in dic    ${arg_dic}     nationality
    ${param_occupation}     [Common] - Create string param in dic    ${arg_dic}     occupation
    ${param_occupation_local}       [Common] - Create string param in dic    ${arg_dic}     occupation_local
    ${param_occupation_title}       [Common] - Create string param in dic    ${arg_dic}     occupation_title
    ${param_occupation_title_local}     [Common] - Create string param in dic    ${arg_dic}     occupation_title_local
    ${param_township_code}      [Common] - Create string param in dic    ${arg_dic}     township_code
    ${param_township_name}      [Common] - Create string param in dic    ${arg_dic}     township_name
    ${param_township_name_local}        [Common] - Create string param in dic    ${arg_dic}     township_name_local
    ${param_mother_name}        [Common] - Create string param in dic    ${arg_dic}     mother_name
    ${param_mother_name_local}      [Common] - Create string param in dic    ${arg_dic}     mother_name_local
    ${param_mother_maiden_name}     [Common] - Create string param in dic    ${arg_dic}     mother_maiden_name
    ${param_mother_maiden_name_local}       [Common] - Create string param in dic    ${arg_dic}     mother_maiden_name_local
    ${param_civil_status}       [Common] - Create string param in dic    ${arg_dic}     civil_status
    ${param_email}      [Common] - Create string param in dic    ${arg_dic}     email
    ${param_primary_mobile_number}      [Common] - Create string param in dic    ${arg_dic}     primary_mobile_number
    ${param_secondary_mobile_number}        [Common] - Create string param in dic    ${arg_dic}     secondary_mobile_number
    ${param_tertiary_mobile_number}     [Common] - Create string param in dic    ${arg_dic}     tertiary_mobile_number
    ${param_telephone_number}       [Common] - Create string param in dic    ${arg_dic}     telephone_number
    ${param_current_address_citizen_association}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       citizen_association
    ${param_current_address_neighbourhood_association}      [Common] - Create string param of object in dic    ${arg_dic}    current_address       neighbourhood_association
    ${param_current_address_address}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       address
    ${param_current_address_commune}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       commune
    ${param_current_address_district}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       district
    ${param_current_address_city}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       city
    ${param_current_address_province}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       province
    ${param_current_address_postal_code}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       postal_code
    ${param_current_address_country}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       country
    ${param_current_address_landmark}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       landmark
    ${param_current_address_longitude}      [Common] - Create string param of object in dic    ${arg_dic}    current_address       longitude
    ${param_current_address_latitude}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       latitude
    ${param_permanent_address_citizen_association}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       citizen_association
    ${param_permanent_address_neighbourhood_association}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       neighbourhood_association
    ${param_permanent_address_address}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       address
    ${param_permanent_address_commune}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       commune
    ${param_permanent_address_district}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       district
    ${param_permanent_address_city}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       city
    ${param_permanent_address_province}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       province
    ${param_permanent_address_postal_code}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       postal_code
    ${param_permanent_address_country}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       country
    ${param_permanent_address_landmark}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       landmark
    ${param_permanent_address_longitude}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       longitude
    ${param_permanent_address_latitude}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       latitude
    ${param_primary_identity_type}      [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       type
    ${param_primary_identity_status}        [Common] - Create int param of object in dic    ${arg_dic}    primary_identity      status
    ${param_primary_identity_identity_id}       [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       identity_id
    ${param_primary_identity_place_of_issue}        [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       place_of_issue
    ${param_primary_identity_issue_date}        [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       issue_date
    ${param_primary_identity_expired_date}      [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       expired_date
    ${param_primary_identity_front_identity_url}        [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       front_identity_url
    ${param_primary_identity_back_identity_url}     [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       back_identity_url
    ${param_primary_identity_identity_id_local}     [Common] - Create string param of object in dic    ${arg_dic}      primary_identity    identity_id_local
    ${param_secondary_identity_type}        [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       type
    ${param_secondary_identity_status}      [Common] - Create int param of object in dic    ${arg_dic}    secondary_identity      status
    ${param_secondary_identity_identity_id}     [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       identity_id
    ${param_secondary_identity_place_of_issue}      [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       place_of_issue
    ${param_secondary_identity_issue_date}      [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       issue_date
    ${param_secondary_identity_expired_date}        [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       expired_date
    ${param_secondary_identity_front_identity_url}      [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       front_identity_url
    ${param_secondary_identity_back_identity_url}       [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       back_identity_url
    ${param_secondary_identity_identity_id_local}       [Common] - Create string param of object in dic    ${arg_dic}      secondary_identity      identity_id_local
    ${param_tertiary_identity_type}     [Common] - Create string param in dic    ${arg_dic}     type
    ${param_tertiary_identity_status}       [Common] - Create int param in dic    ${arg_dic}        status
    ${param_tertiary_identity_identity_id}      [Common] - Create string param in dic    ${arg_dic}     identity_id
    ${param_tertiary_identity_identity_id_local}        [Common] - Create string param in dic    ${arg_dic}     identity_id_local
    ${param_tertiary_identity_place_of_issue}       [Common] - Create string param in dic    ${arg_dic}     place_of_issue
    ${param_tertiary_identity_issue_date}       [Common] - Create string param in dic    ${arg_dic}     issue_date
    ${param_tertiary_identity_expired_date}     [Common] - Create string param in dic    ${arg_dic}     expired_date
    ${param_tertiary_identity_front_identity_url}       [Common] - Create string param in dic    ${arg_dic}     front_identity_url
    ${param_tertiary_identity_back_identity_url}        [Common] - Create string param in dic    ${arg_dic}     back_identity_url
    ${param_tertiary_identity_signature_url}        [Common] - Create string param in dic    ${arg_dic}     signature_url
    ${param_kyc_level}      [Common] - Create int param in dic    ${arg_dic}        level
    ${param_kyc_remark}     [Common] - Create string param in dic    ${arg_dic}     remark
    ${param_kyc_verify_by}      [Common] - Create string param in dic    ${arg_dic}     verify_by
    ${param_kyc_verify_date}        [Common] - Create string param in dic    ${arg_dic}     verify_date
    ${param_kyc_risk_level}     [Common] - Create string param in dic    ${arg_dic}     risk_level
    ${param_additional_profile_picture_url}     [Common] - Create string param in dic    ${arg_dic}     profile_picture_url
    ${param_additional_tax_id_card_photo_url}       [Common] - Create string param in dic    ${arg_dic}     tax_id_card_photo_url
    ${param_additional_field_1_name}        [Common] - Create string param in dic    ${arg_dic}     field_1_name
    ${param_additional_field_1_value}       [Common] - Create string param in dic    ${arg_dic}     field_1_value
    ${param_additional_field_2_name}        [Common] - Create string param in dic    ${arg_dic}     field_2_name
    ${param_additional_field_2_value}       [Common] - Create string param in dic    ${arg_dic}     field_2_value
    ${param_additional_field_3_name}        [Common] - Create string param in dic    ${arg_dic}     field_3_name
    ${param_additional_field_3_value}       [Common] - Create string param in dic    ${arg_dic}     field_3_value
    ${param_additional_field_4_name}        [Common] - Create string param in dic    ${arg_dic}     field_4_name
    ${param_additional_field_4_value}       [Common] - Create string param in dic    ${arg_dic}     field_4_value
    ${param_additional_field_5_name}        [Common] - Create string param in dic    ${arg_dic}     field_5_name
    ${param_additional_field_5_value}       [Common] - Create string param in dic    ${arg_dic}     field_5_value
    ${param_additional_supporting_file_1_url}       [Common] - Create string param in dic    ${arg_dic}     supporting_file_1_url
    ${param_additional_supporting_file_2_url}       [Common] - Create string param in dic    ${arg_dic}     supporting_file_2_url
    ${param_additional_supporting_file_3_url}       [Common] - Create string param in dic    ${arg_dic}     supporting_file_3_url
    ${param_additional_supporting_file_4_url}       [Common] - Create string param in dic    ${arg_dic}     supporting_file_4_url
    ${param_additional_supporting_file_5_url}       [Common] - Create string param in dic    ${arg_dic}     supporting_file_5_url
    ${param_payroll_card_number}        [Common] - Create string param in dic    ${arg_dic}     payroll_card_number
    ${param_current_address_address_local}      [Common] - Create string param of object in dic    ${arg_dic}    current_address       address_local
    ${param_current_address_commune_local}      [Common] - Create string param of object in dic    ${arg_dic}    current_address       commune_local
    ${param_current_address_district_local}     [Common] - Create string param of object in dic    ${arg_dic}    current_address       district_local
    ${param_current_address_city_local}         [Common] - Create string param of object in dic    ${arg_dic}    current_address       city_local
    ${param_current_address_province_local}     [Common] - Create string param of object in dic    ${arg_dic}    current_address       province_local
    ${param_current_address_postal_code_local}      [Common] - Create string param of object in dic    ${arg_dic}    current_address       postal_code_local
    ${param_current_address_country_local}      [Common] - Create string param of object in dic    ${arg_dic}    current_address       country_local
    ${param_permanent_address_address_local}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address     address_local
    ${param_permanent_address_commune_local}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address     commune_local
    ${param_permanent_address_district_local}       [Common] - Create string param of object in dic    ${arg_dic}    permanent_address     district_local
    ${param_permanent_address_city_local}       [Common] - Create string param of object in dic    ${arg_dic}    permanent_address     city_local
    ${param_permanent_address_province_local}       [Common] - Create string param of object in dic    ${arg_dic}    permanent_address     province_local
    ${param_permanent_address_postal_code_local}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address     postal_code_local
    ${param_permanent_address_country_local}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address     country_local
    ${param_bank_name_local}        [Common] - Create string param in dic    ${arg_dic}        name_local
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...  {
        ...                 ${param_is_testing_account}
        ...                 ${param_is_system_account}
        ...                 ${param_acquisition_source}
        ...                 ${param_customer_classification_id}
        #…                 ${param_referrer_user_type}
        ...                 ${param_referrer_user_id}
        ...                 ${param_unique_reference}
        ...                 ${param_beneficiary}
        ...                 ${param_mm_card_type_id}
        ...                 ${param_mm_card_level_id}
        ...                 ${param_mm_factory_card_number}
        ...                 ${param_is_require_otp}
        ...                 ${param_tin_number}
        ...                 ${param_tin_number_local}
        ...                 ${param_title}
        ...                 ${param_title_local}
        ...                 ${param_first_name}
        ...                 ${param_first_name_local}
        ...                 ${param_middle_name}
        ...                 ${param_middle_name_local}
        ...                 ${param_last_name}
        ...                 ${param_last_name_local}
        ...                 ${param_suffix}
        ...                 ${param_suffix_local}
        ...                 ${param_date_of_birth}
        ...                 ${param_place_of_birth}
        ...                 ${param_place_of_birth_local}
        ...                 ${param_gender}
        ...                 ${param_gender_local}
        ...                 ${param_ethnicity}
        ...                 ${param_employer}
        ...                 ${param_nationality}
        ...                 ${param_occupation}
        ...                 ${param_occupation_local}
        ...                 ${param_occupation_title}
        ...                 ${param_occupation_title_local}
        ...                 ${param_township_code}
        ...                 ${param_township_name}
        ...                 ${param_township_name_local}
        ...                 ${param_mother_name}
        ...                 ${param_mother_name_local}
        ...                 ${param_mother_maiden_name}
        ...                 ${param_mother_maiden_name_local}
        ...                 ${param_civil_status}
        ...                 ${param_email}
        ...                 ${param_primary_mobile_number}
        ...                 ${param_secondary_mobile_number}
        ...                 ${param_tertiary_mobile_number}
        ...                 ${param_telephone_number}
        ...                 ${param_payroll_card_number}
        ...                 "address": {
        ...                     "current_address": {
        ...      					${param_current_address_citizen_association}
        ...      					${param_current_address_neighbourhood_association}
        ...      					${param_current_address_address}
        ...      					${param_current_address_commune}
        ...      					${param_current_address_district}
        ...      					${param_current_address_city}
        ...      					${param_current_address_province}
        ...      					${param_current_address_postal_code}
        ...      					${param_current_address_country}
        ...      					${param_current_address_landmark}
        ...      					${param_current_address_longitude}
        ...      					${param_current_address_latitude}
        ...                         ${param_current_address_address_local}
        ...                         ${param_current_address_commune_local}
        ...                         ${param_current_address_district_local}
        ...                         ${param_current_address_city_local}
        ...                         ${param_current_address_province_local}
        ...                         ${param_current_address_postal_code_local}
        ...                         ${param_current_address_country_local}
        ...                     },
        ...                     "permanent_address": {
        ...      					${param_permanent_address_citizen_association}
        ...      					${param_permanent_address_neighbourhood_association}
        ...      					${param_permanent_address_address}
        ...      					${param_permanent_address_commune}
        ...      					${param_permanent_address_district}
        ...      					${param_permanent_address_city}
        ...      					${param_permanent_address_province}
        ...      					${param_permanent_address_postal_code}
        ...      					${param_permanent_address_country}
        ...      					${param_permanent_address_landmark}
        ...      					${param_permanent_address_longitude}
        ...      					${param_permanent_address_latitude}
        ...                         ${param_permanent_address_address_local}
        ...                         ${param_permanent_address_commune_local}
        ...                         ${param_permanent_address_district_local}
        ...                         ${param_permanent_address_city_local}
        ...                         ${param_permanent_address_province_local}
        ...                         ${param_permanent_address_postal_code_local}
        ...                         ${param_permanent_address_country_local}
        ...                     }
        ...                 },
        ...                 "kyc": {
        ...                     "primary_identity": {
        ...      					${param_primary_identity_type}
        ...      					${param_primary_identity_status}
        ...      					${param_primary_identity_identity_id}
        ...      					${param_primary_identity_identity_id_local}
        ...      					${param_primary_identity_place_of_issue}
        ...      					${param_primary_identity_issue_date}
        ...      					${param_primary_identity_expired_date}
        ...      					${param_primary_identity_front_identity_url}
        ...      					${param_primary_identity_back_identity_url}
        ...                     },
        ...                     "secondary_identity": {
        ...      					${param_secondary_identity_type}
        ...      					${param_secondary_identity_status}
        ...      					${param_secondary_identity_identity_id}
        ...      					${param_secondary_identity_identity_id_local}
        ...      					${param_secondary_identity_place_of_issue}
        ...      					${param_secondary_identity_issue_date}
        ...      					${param_secondary_identity_expired_date}
        ...      					${param_secondary_identity_front_identity_url}
        ...      					${param_secondary_identity_back_identity_url}
        ...                     },
        ...                     "tertiary_identity": {
        ...      					${param_tertiary_identity_type}
        ...      					${param_tertiary_identity_status}
        ...      					${param_tertiary_identity_identity_id}
        ...      					${param_tertiary_identity_identity_id_local}
        ...      					${param_tertiary_identity_place_of_issue}
        ...      					${param_tertiary_identity_issue_date}
        ...      					${param_tertiary_identity_expired_date}
        ...      					${param_tertiary_identity_front_identity_url}
        ...      					${param_tertiary_identity_back_identity_url}
        ...      					${param_tertiary_identity_signature_url}
        ...                     },
        ...      				${param_kyc_level}
        ...      				${param_kyc_remark}
        ...      				${param_kyc_verify_by}
        ...      				${param_kyc_verify_date}
        ...      				${param_kyc_risk_level}
        ...                 },
        ...                 "additional": {
        ...      				${param_additional_profile_picture_url}
        ...      				${param_additional_tax_id_card_photo_url}
        ...      				${param_additional_field_1_name}
        ...      				${param_additional_field_1_value}
        ...      				${param_additional_field_2_name}
        ...      				${param_additional_field_2_value}
        ...      				${param_additional_field_3_name}
        ...      				${param_additional_field_3_value}
        ...      				${param_additional_field_4_name}
        ...      				${param_additional_field_4_value}
        ...      				${param_additional_field_5_name}
        ...      				${param_additional_field_5_value}
        ...      				${param_additional_supporting_file_1_url}
        ...      				${param_additional_supporting_file_2_url}
        ...      				${param_additional_supporting_file_3_url}
        ...      				${param_additional_supporting_file_4_url}
        ...      				${param_additional_supporting_file_5_url}
        ...                 }
        ...  }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${customer_id}=    convert to string   ${arg_dic.customer_id}
    ${system_user_update_customer_path}=    replace string      ${system_user_update_customer_path}      {customer_id}        ${customer_id}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${system_user_update_customer_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${data}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API system user updates customer profile with PATCH method
    [Arguments]     ${arg_dic}
    ${param_referrer_user_type}     create json data for user type object       referrer_user_type   ${arg_dic.referrer_user_type_id}      ${arg_dic.referrer_user_type_name}
    ${param_is_testing_account}		[Common] - Create boolean param		is_testing_account		${arg_dic.is_testing_account}
    ${param_is_system_account}		[Common] - Create boolean param		is_system_account		${arg_dic.is_system_account}
    ${param_acquisition_source}		[Common] - Create string param		acquisition_source		${arg_dic.acquisition_source}
    ${param_customer_classification_id}		[Common] - Create int param		customer_classification_id		${arg_dic.customer_classification_id}
    ${param_referrer_user_id}		[Common] - Create int param		referrer_user_id		${arg_dic.referrer_user_id}
    ${param_beneficiary}		[Common] - Create string param		beneficiary		${arg_dic.beneficiary}
    ${param_mm_card_type_id}		[Common] - Create int param		mm_card_type_id		${arg_dic.mm_card_type_id}
    ${param_mm_card_level_id}		[Common] - Create int param		mm_card_level_id		${arg_dic.mm_card_level_id}
    ${param_mm_factory_card_number}		[Common] - Create string param		mm_factory_card_number		${arg_dic.mm_factory_card_number}
    ${param_is_require_otp}		[Common] - Create boolean param		is_require_otp		${arg_dic.is_require_otp}
    ${param_tin_number}		[Common] - Create string param		tin_number		${arg_dic.tin_number}
    ${param_tin_number_local}		[Common] - Create string param		tin_number_local		${arg_dic.tin_number_local}
    ${param_title}		[Common] - Create string param		title		${arg_dic.title}
    ${param_title_local}		[Common] - Create string param		title_local		${arg_dic.title_local}
    ${param_first_name}		[Common] - Create string param		first_name		${arg_dic.first_name}
    ${param_first_name_local}		[Common] - Create string param		first_name_local		${arg_dic.first_name_local}
    ${param_middle_name}		[Common] - Create string param		middle_name		${arg_dic.middle_name}
    ${param_middle_name_local}		[Common] - Create string param		middle_name_local		${arg_dic.middle_name_local}
    ${param_last_name}		[Common] - Create string param		last_name		${arg_dic.last_name}
    ${param_last_name_local}		[Common] - Create string param		last_name_local		${arg_dic.last_name_local}
    ${param_suffix}		[Common] - Create string param		suffix		${arg_dic.suffix}
    ${param_suffix_local}		[Common] - Create string param		suffix_local		${arg_dic.suffix_local}
    ${param_date_of_birth}		[Common] - Create string param		date_of_birth		${arg_dic.date_of_birth}
    ${param_place_of_birth}		[Common] - Create string param		place_of_birth		${arg_dic.place_of_birth}
    ${param_place_of_birth_local}		[Common] - Create string param		place_of_birth_local		${arg_dic.place_of_birth_local}
    ${param_gender}		[Common] - Create string param		gender		${arg_dic.gender}
    ${param_gender_local}		[Common] - Create string param		gender_local		${arg_dic.gender_local}
    ${param_ethnicity}		[Common] - Create string param		ethnicity		${arg_dic.ethnicity}
    ${param_employer}		[Common] - Create string param		employer		${arg_dic.employer}
    ${param_nationality}		[Common] - Create string param		nationality		${arg_dic.nationality}
    ${param_occupation}		[Common] - Create string param		occupation		${arg_dic.occupation}
    ${param_occupation_local}		[Common] - Create string param		occupation_local		${arg_dic.occupation_local}
    ${param_occupation_title}		[Common] - Create string param		occupation_title		${arg_dic.occupation_title}
    ${param_occupation_title_local}		[Common] - Create string param		occupation_title_local		${arg_dic.occupation_title_local}
    ${param_township_code}		[Common] - Create string param		township_code		${arg_dic.township_code}
    ${param_township_name}		[Common] - Create string param		township_name		${arg_dic.township_name}
    ${param_township_name_local}		[Common] - Create string param		township_name_local		${arg_dic.township_name_local}
    ${param_mother_name}		[Common] - Create string param		mother_name		${arg_dic.mother_name}
    ${param_mother_name_local}		[Common] - Create string param		mother_name_local		${arg_dic.mother_name_local}
    ${param_mother_maiden_name}		[Common] - Create string param		mother_maiden_name		${arg_dic.mother_maiden_name}
    ${param_mother_maiden_name_local}		[Common] - Create string param		mother_maiden_name_local		${arg_dic.mother_maiden_name_local}
    ${param_civil_status}		[Common] - Create string param		civil_status		${arg_dic.civil_status}
    ${param_email}		[Common] - Create string param		email		${arg_dic.email}
    ${param_primary_mobile_number}		[Common] - Create string param		primary_mobile_number		${arg_dic.primary_mobile_number}
    ${param_secondary_mobile_number}		[Common] - Create string param		secondary_mobile_number		${arg_dic.secondary_mobile_number}
    ${param_tertiary_mobile_number}		[Common] - Create string param		tertiary_mobile_number		${arg_dic.tertiary_mobile_number}
    ${param_telephone_number}		[Common] - Create string param		telephone_number		${arg_dic.telephone_number}
    ${param_current_address_citizen_association}		[Common] - Create string param		citizen_association		${arg_dic.current_address_citizen_association}
    ${param_current_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${arg_dic.current_address_neighbourhood_association}
    ${param_current_address_address}		[Common] - Create string param		address		${arg_dic.current_address_address}
    ${param_current_address_address_local}		[Common] - Create string param		address_local		${arg_dic.current_address_address_local}
    ${param_current_address_commune}		[Common] - Create string param		commune		${arg_dic.current_address_commune}
    ${param_current_address_commune_local}		[Common] - Create string param		commune_local		${arg_dic.current_address_commune_local}
    ${param_current_address_district}		[Common] - Create string param		district		${arg_dic.current_address_district}
    ${param_current_address_district_local}		[Common] - Create string param		district_local		${arg_dic.current_address_district_local}
    ${param_current_address_city}		[Common] - Create string param		city		${arg_dic.current_address_city}
    ${param_current_address_city_local}		[Common] - Create string param		city_local		${arg_dic.current_address_city_local}
    ${param_current_address_province}		[Common] - Create string param		province		${arg_dic.current_address_province}
    ${param_current_address_province_local}		[Common] - Create string param		province_local		${arg_dic.current_address_province_local}
    ${param_current_address_postal_code}		[Common] - Create string param		postal_code		${arg_dic.current_address_postal_code}
    ${param_current_address_postal_code_local}		[Common] - Create string param		postal_code_local		${arg_dic.current_address_postal_code_local}
    ${param_current_address_country}		[Common] - Create string param		country		${arg_dic.current_address_country}
    ${param_current_address_country_local}		[Common] - Create string param		country_local	${arg_dic.current_address_country_local}
    ${param_current_address_landmark}		[Common] - Create string param		landmark		${arg_dic.current_address_landmark}
    ${param_current_address_longitude}		[Common] - Create string param		longitude		${arg_dic.current_address_longitude}
    ${param_current_address_latitude}		[Common] - Create string param		latitude		${arg_dic.current_address_latitude}
    ${param_permanent_address_citizen_association}		[Common] - Create string param		citizen_association		${arg_dic.permanent_address_citizen_association}
    ${param_permanent_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${arg_dic.permanent_address_neighbourhood_association}
    ${param_permanent_address_address}		[Common] - Create string param		address		${arg_dic.permanent_address_address}
    ${param_permanent_address_address_local}		[Common] - Create string param		address_local		${arg_dic.permanent_address_address_local}
    ${param_permanent_address_commune}		[Common] - Create string param		commune		${arg_dic.permanent_address_commune}
    ${param_permanent_address_commune_local}		[Common] - Create string param		commune_local		${arg_dic.permanent_address_commune_local}
    ${param_permanent_address_district}		[Common] - Create string param		district		${arg_dic.permanent_address_district}
    ${param_permanent_address_district_local}		[Common] - Create string param		district_local		${arg_dic.permanent_address_district_local}
    ${param_permanent_address_city}		[Common] - Create string param		city		${arg_dic.permanent_address_city}
    ${param_permanent_address_city_local}		[Common] - Create string param		city_local		${arg_dic.permanent_address_city_local}
    ${param_permanent_address_province}		[Common] - Create string param		province		${arg_dic.permanent_address_province}
    ${param_permanent_address_province_local}		[Common] - Create string param		province_local		${arg_dic.permanent_address_province_local}
    ${param_permanent_address_postal_code}		[Common] - Create string param		postal_code		${arg_dic.permanent_address_postal_code}
    ${param_permanent_address_postal_code_local}		[Common] - Create string param		postal_code_local		${arg_dic.permanent_address_postal_code_local}
    ${param_permanent_address_country}		[Common] - Create string param		country		${arg_dic.permanent_address_country}
    ${param_permanent_address_country_local}		[Common] - Create string param		country_local		${arg_dic.permanent_address_country_local}
    ${param_permanent_address_landmark}		[Common] - Create string param		landmark		${arg_dic.permanent_address_landmark}
    ${param_permanent_address_longitude}		[Common] - Create string param		longitude		${arg_dic.permanent_address_longitude}
    ${param_permanent_address_latitude}		[Common] - Create string param		latitude		${arg_dic.permanent_address_latitude}
    ${param_primary_identity_type}		[Common] - Create string param		type		${arg_dic.primary_identity_type}
    ${param_primary_identity_status}		[Common] - Create int param		status		${arg_dic.primary_identity_status}
    ${param_primary_identity_identity_id}		[Common] - Create string param		identity_id		${arg_dic.primary_identity_identity_id}
    ${param_primary_identity_identity_id_local}		[Common] - Create string param		identity_id_local		${arg_dic.primary_identity_identity_id_local}
    ${param_primary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${arg_dic.primary_identity_place_of_issue}
    ${param_primary_identity_issue_date}		[Common] - Create string param		issue_date		${arg_dic.primary_identity_issue_date}
    ${param_primary_identity_expired_date}		[Common] - Create string param		expired_date		${arg_dic.primary_identity_expired_date}
    ${param_primary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${arg_dic.primary_identity_front_identity_url}
    ${param_primary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${arg_dic.primary_identity_back_identity_url}
    ${param_primary_identity_signature_url}		[Common] - Create string param		signature_url		${arg_dic.primary_identity_signature_url}
    ${param_secondary_identity_type}		[Common] - Create string param		type		${arg_dic.secondary_identity_type}
    ${param_secondary_identity_status}		[Common] - Create int param		status		${arg_dic.secondary_identity_status}
    ${param_secondary_identity_identity_id}		[Common] - Create string param		identity_id		${arg_dic.secondary_identity_identity_id}
    ${param_secondary_identity_identity_id_local}		[Common] - Create string param		identity_id_local		${arg_dic.secondary_identity_identity_id_local}
    ${param_secondary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${arg_dic.secondary_identity_place_of_issue}
    ${param_secondary_identity_issue_date}		[Common] - Create string param		issue_date		${arg_dic.secondary_identity_issue_date}
    ${param_secondary_identity_expired_date}		[Common] - Create string param		expired_date		${arg_dic.secondary_identity_expired_date}
    ${param_secondary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${arg_dic.secondary_identity_front_identity_url}
    ${param_secondary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${arg_dic.secondary_identity_back_identity_url}
    ${param_secondary_identity_signature_url}		[Common] - Create string param		signature_url		${arg_dic.secondary_identity_signature_url}
    ${param_tertiary_identity_type}		[Common] - Create string param		type		${arg_dic.tertiary_identity_type}
    ${param_tertiary_identity_status}		[Common] - Create int param		status		${arg_dic.tertiary_identity_status}
    ${param_tertiary_identity_identity_id}		[Common] - Create string param		identity_id		${arg_dic.tertiary_identity_identity_id}
    ${param_tertiary_identity_identity_id_local}		[Common] - Create string param		identity_id_local		${arg_dic.tertiary_identity_identity_id_local}
    ${param_tertiary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${arg_dic.tertiary_identity_place_of_issue}
    ${param_tertiary_identity_issue_date}		[Common] - Create string param		issue_date		${arg_dic.tertiary_identity_issue_date}
    ${param_tertiary_identity_expired_date}		[Common] - Create string param		expired_date		${arg_dic.tertiary_identity_expired_date}
    ${param_tertiary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${arg_dic.tertiary_identity_front_identity_url}
    ${param_tertiary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${arg_dic.tertiary_identity_back_identity_url}
    ${param_tertiary_identity_signature_url}		[Common] - Create string param		signature_url		${arg_dic.tertiary_identity_signature_url}
    ${param_kyc_level}		[Common] - Create int param		level		${arg_dic.kyc_level}
    ${param_kyc_remark}		[Common] - Create string param		remark		${arg_dic.kyc_remark}
    ${param_kyc_verify_by}		[Common] - Create string param		verify_by		${arg_dic.kyc_verify_by}
    ${param_kyc_verify_date}		[Common] - Create string param		verify_date		${arg_dic.kyc_verify_date}
    ${param_kyc_risk_level}		[Common] - Create string param		risk_level		${arg_dic.kyc_risk_level}
    ${param_additional_profile_picture_url}		[Common] - Create string param		profile_picture_url		${arg_dic.additional_profile_picture_url}
    ${param_additional_tax_id_card_photo_url}		[Common] - Create string param		tax_id_card_photo_url		${arg_dic.additional_tax_id_card_photo_url}
    ${param_additional_field_1_name}		[Common] - Create string param		field_1_name		${arg_dic.additional_field_1_name}
    ${param_additional_field_1_value}		[Common] - Create string param		field_1_value		${arg_dic.additional_field_1_value}
    ${param_additional_field_2_name}		[Common] - Create string param		field_2_name		${arg_dic.additional_field_2_name}
    ${param_additional_field_2_value}		[Common] - Create string param		field_2_value		${arg_dic.additional_field_2_value}
    ${param_additional_field_3_name}		[Common] - Create string param		field_3_name		${arg_dic.additional_field_3_name}
    ${param_additional_field_3_value}		[Common] - Create string param		field_3_value		${arg_dic.additional_field_3_value}
    ${param_additional_field_4_name}		[Common] - Create string param		field_4_name		${arg_dic.additional_field_4_name}
    ${param_additional_field_4_value}		[Common] - Create string param		field_4_value		${arg_dic.additional_field_4_value}
    ${param_additional_field_5_name}		[Common] - Create string param		field_5_name		${arg_dic.additional_field_5_name}
    ${param_additional_field_5_value}		[Common] - Create string param		field_5_value		${arg_dic.additional_field_5_value}
    ${param_additional_supporting_file_1_url}		[Common] - Create string param		supporting_file_1_url		${arg_dic.additional_supporting_file_1_url}
    ${param_additional_supporting_file_2_url}		[Common] - Create string param		supporting_file_2_url		${arg_dic.additional_supporting_file_2_url}
    ${param_additional_supporting_file_3_url}		[Common] - Create string param		supporting_file_3_url		${arg_dic.additional_supporting_file_3_url}
    ${param_additional_supporting_file_4_url}		[Common] - Create string param		supporting_file_4_url		${arg_dic.additional_supporting_file_4_url}
    ${param_additional_supporting_file_5_url}		[Common] - Create string param		supporting_file_5_url		${arg_dic.additional_supporting_file_5_url}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...  {
        ...                 ${param_is_testing_account}
        ...                 ${param_is_system_account}
        ...                 ${param_acquisition_source}
        ...                 ${param_customer_classification_id}
        ...                 ${param_referrer_user_type}
        ...                 ${param_referrer_user_id}
        ...                 ${param_beneficiary}
        ...                 ${param_mm_card_type_id}
        ...                 ${param_mm_card_level_id}
        ...                 ${param_mm_factory_card_number}
        ...                 ${param_is_require_otp}
        ...                 ${param_tin_number}
        ...                 ${param_tin_number_local}
        ...                 ${param_title}
        ...                 ${param_title_local}
        ...                 ${param_first_name}
        ...                 ${param_first_name_local}
        ...                 ${param_middle_name}
        ...                 ${param_middle_name_local}
        ...                 ${param_last_name}
        ...                 ${param_last_name_local}
        ...                 ${param_suffix}
        ...                 ${param_suffix_local}
        ...                 ${param_date_of_birth}
        ...                 ${param_place_of_birth}
        ...                 ${param_place_of_birth_local}
        ...                 ${param_gender}
        ...                 ${param_gender_local}
        ...                 ${param_ethnicity}
        ...                 ${param_employer}
        ...                 ${param_nationality}
        ...                 ${param_occupation}
        ...                 ${param_occupation_local}
        ...                 ${param_occupation_title}
        ...                 ${param_occupation_title_local}
        ...                 ${param_township_code}
        ...                 ${param_township_name}
        ...                 ${param_township_name_local}
        ...                 ${param_mother_name}
        ...                 ${param_mother_name_local}
        ...                 ${param_mother_maiden_name}
        ...                 ${param_mother_maiden_name_local}
        ...                 ${param_civil_status}
        ...                 ${param_email}
        ...                 ${param_primary_mobile_number}
        ...                 ${param_secondary_mobile_number}
        ...                 ${param_tertiary_mobile_number}
        ...                 ${param_telephone_number}
        ...                 "address": {
        ...                     "current_address": {
        ...      					${param_current_address_citizen_association}
        ...      					${param_current_address_neighbourhood_association}
        ...      					${param_current_address_address}
        ...      					${param_current_address_commune}
        ...      					${param_current_address_district}
        ...      					${param_current_address_city}
        ...      					${param_current_address_province}
        ...      					${param_current_address_postal_code}
        ...      					${param_current_address_country}
        ...      					${param_current_address_landmark}
        ...      					${param_current_address_longitude}
        ...      					${param_current_address_latitude}
        ...      					${param_current_address_address_local}
        ...      					${param_current_address_commune_local}
        ...      					${param_current_address_district_local}
        ...      					${param_current_address_city_local}
        ...      					${param_current_address_province_local}
        ...      					${param_current_address_postal_code_local}
        ...      					${param_current_address_country_local}
        ...                     },
        ...                     "permanent_address": {
        ...      					${param_permanent_address_citizen_association}
        ...      					${param_permanent_address_neighbourhood_association}
        ...      					${param_permanent_address_address}
        ...      					${param_permanent_address_commune}
        ...      					${param_permanent_address_district}
        ...      					${param_permanent_address_city}
        ...      					${param_permanent_address_province}
        ...      					${param_permanent_address_postal_code}
        ...      					${param_permanent_address_country}
        ...      					${param_permanent_address_landmark}
        ...      					${param_permanent_address_longitude}
        ...      					${param_permanent_address_latitude}
        ...      					${param_permanent_address_address_local}
        ...      					${param_permanent_address_commune_local}
        ...      					${param_permanent_address_district_local}
        ...      					${param_permanent_address_city_local}
        ...      					${param_permanent_address_province_local}
        ...      					${param_permanent_address_postal_code_local}
        ...      					${param_permanent_address_country_local}
        ...                     }
        ...                 },
        ...                 "kyc": {
        ...                     "primary_identity": {
        ...      					${param_primary_identity_type}
        ...      					${param_primary_identity_status}
        ...      					${param_primary_identity_identity_id}
        ...      					${param_primary_identity_identity_id_local}
        ...      					${param_primary_identity_place_of_issue}
        ...      					${param_primary_identity_issue_date}
        ...      					${param_primary_identity_expired_date}
        ...      					${param_primary_identity_front_identity_url}
        ...      					${param_primary_identity_back_identity_url}
        ...      					${param_primary_identity_signature_url}
        ...                     },
        ...                     "secondary_identity": {
        ...      					${param_secondary_identity_type}
        ...      					${param_secondary_identity_status}
        ...      					${param_secondary_identity_identity_id}
        ...      					${param_secondary_identity_identity_id_local}
        ...      					${param_secondary_identity_place_of_issue}
        ...      					${param_secondary_identity_issue_date}
        ...      					${param_secondary_identity_expired_date}
        ...      					${param_secondary_identity_front_identity_url}
        ...      					${param_secondary_identity_back_identity_url}
        ...      					${param_secondary_identity_signature_url}
        ...                     },
        ...                     "tertiary_identity": {
        ...      					${param_tertiary_identity_type}
        ...      					${param_tertiary_identity_status}
        ...      					${param_tertiary_identity_identity_id}
        ...      					${param_tertiary_identity_identity_id_local}
        ...      					${param_tertiary_identity_place_of_issue}
        ...      					${param_tertiary_identity_issue_date}
        ...      					${param_tertiary_identity_expired_date}
        ...      					${param_tertiary_identity_front_identity_url}
        ...      					${param_tertiary_identity_back_identity_url}
        ...      					${param_tertiary_identity_signature_url}
        ...                     },
        ...      				${param_kyc_level}
        ...      				${param_kyc_remark}
        ...      				${param_kyc_verify_by}
        ...      				${param_kyc_verify_date}
        ...      				${param_kyc_risk_level}
        ...                 },
        ...                 "additional": {
        ...      				${param_additional_profile_picture_url}
        ...      				${param_additional_tax_id_card_photo_url}
        ...      				${param_additional_field_1_name}
        ...      				${param_additional_field_1_value}
        ...      				${param_additional_field_2_name}
        ...      				${param_additional_field_2_value}
        ...      				${param_additional_field_3_name}
        ...      				${param_additional_field_3_value}
        ...      				${param_additional_field_4_name}
        ...      				${param_additional_field_4_value}
        ...      				${param_additional_field_5_name}
        ...      				${param_additional_field_5_value}
        ...      				${param_additional_supporting_file_1_url}
        ...      				${param_additional_supporting_file_2_url}
        ...      				${param_additional_supporting_file_3_url}
        ...      				${param_additional_supporting_file_4_url}
        ...      				${param_additional_supporting_file_5_url}
        ...                 }
        ...  }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${customer_id}=    convert to string   ${arg_dic.customer_id}
    ${system_user_update_customer_path}=    replace string      ${system_user_update_customer_path}      {customer_id}        ${customer_id}   1
                create session          api        ${api_gateway_host}
    ${response}             patch request
        ...     api
        ...     ${system_user_update_customer_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
        ...     ${NONE}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API customer updates customer profile
    [Arguments]     ${arg_dic}
    ${param_referrer_user_type}     create json data for user type object       referrer_user_type   ${arg_dic.referrer_user_type_id}      ${arg_dic.referrer_user_type_name}
    ${param_referrer_user_id}		[Common] - Create int param		referrer_user_id		${arg_dic.referrer_user_id}
    ${param_unique_reference}		[Common] - Create string param		unique_reference		${arg_dic.unique_reference}
    ${param_beneficiary}		[Common] - Create string param		beneficiary		${arg_dic.beneficiary}
    ${param_mm_card_type_id}		[Common] - Create int param		mm_card_type_id		${arg_dic.mm_card_type_id}
    ${param_mm_card_level_id}		[Common] - Create int param		mm_card_level_id		${arg_dic.mm_card_level_id}
    ${param_mm_factory_card_number}		[Common] - Create string param		mm_factory_card_number		${arg_dic.mm_factory_card_number}
    ${param_is_require_otp}		[Common] - Create boolean param		is_require_otp		${arg_dic.is_require_otp}
    ${param_tin_number}		[Common] - Create string param		tin_number		${arg_dic.tin_number}
    ${param_tin_number_local}		[Common] - Create string param		tin_number_local		${arg_dic.tin_number_local}
    ${param_title}		[Common] - Create string param		title		${arg_dic.title}
    ${param_title_local}		[Common] - Create string param		title_local		${arg_dic.title_local}
    ${param_first_name}		[Common] - Create string param		first_name		${arg_dic.first_name}
    ${param_first_name_local}		[Common] - Create string param		first_name_local		${arg_dic.first_name_local}
    ${param_middle_name}		[Common] - Create string param		middle_name		${arg_dic.middle_name}
    ${param_middle_name_local}		[Common] - Create string param		middle_name_local		${arg_dic.middle_name_local}
    ${param_last_name}		[Common] - Create string param		last_name		${arg_dic.last_name}
    ${param_last_name_local}		[Common] - Create string param		last_name_local		${arg_dic.last_name_local}
    ${param_suffix}		[Common] - Create string param		suffix		${arg_dic.suffix}
    ${param_suffix_local}		[Common] - Create string param		suffix_local		${arg_dic.suffix_local}
    ${param_date_of_birth}		[Common] - Create string param		date_of_birth		${arg_dic.date_of_birth}
    ${param_place_of_birth}		[Common] - Create string param		place_of_birth		${arg_dic.place_of_birth}
    ${param_place_of_birth_local}		[Common] - Create string param		place_of_birth_local		${arg_dic.place_of_birth_local}
    ${param_gender}		[Common] - Create string param		gender		${arg_dic.gender}
    ${param_gender_local}		[Common] - Create string param		gender_local		${arg_dic.gender_local}
    ${param_ethnicity}		[Common] - Create string param		ethnicity		${arg_dic.ethnicity}
    ${param_employer}		[Common] - Create string param		employer		${arg_dic.employer}
    ${param_nationality}		[Common] - Create string param		nationality		${arg_dic.nationality}
    ${param_occupation}		[Common] - Create string param		occupation		${arg_dic.occupation}
    ${param_occupation_local}		[Common] - Create string param		occupation_local		${arg_dic.occupation_local}
    ${param_occupation_title}		[Common] - Create string param		occupation_title		${arg_dic.occupation_title}
    ${param_occupation_title_local}		[Common] - Create string param		occupation_title_local		${arg_dic.occupation_title_local}
    ${param_township_code}		[Common] - Create string param		township_code		${arg_dic.township_code}
    ${param_township_name}		[Common] - Create string param		township_name		${arg_dic.township_name}
    ${param_township_name_local}		[Common] - Create string param		township_name_local		${arg_dic.township_name_local}
    ${param_mother_name}		[Common] - Create string param		mother_name		${arg_dic.mother_name}
    ${param_mother_name_local}		[Common] - Create string param		mother_name_local		${arg_dic.mother_name_local}
    ${param_mother_maiden_name}		[Common] - Create string param		mother_maiden_name		${arg_dic.mother_maiden_name}
    ${param_mother_maiden_name_local}		[Common] - Create string param		mother_maiden_name_local		${arg_dic.mother_maiden_name_local}
    ${param_civil_status}		[Common] - Create string param		civil_status		${arg_dic.civil_status}
    ${param_email}		[Common] - Create string param		email		${arg_dic.email}
    ${param_primary_mobile_number}		[Common] - Create string param		primary_mobile_number		${arg_dic.primary_mobile_number}
    ${param_secondary_mobile_number}		[Common] - Create string param		secondary_mobile_number		${arg_dic.secondary_mobile_number}
    ${param_tertiary_mobile_number}		[Common] - Create string param		tertiary_mobile_number		${arg_dic.tertiary_mobile_number}
    ${param_telephone_number}		[Common] - Create string param		telephone_number		${arg_dic.telephone_number}
    ${param_current_address_citizen_association}		[Common] - Create string param		citizen_association		${arg_dic.current_address_citizen_association}
    ${param_current_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${arg_dic.current_address_neighbourhood_association}
    ${param_current_address_address}		[Common] - Create string param		address		${arg_dic.current_address_address}
    ${param_current_address_address_local}		[Common] - Create string param		address_local		${arg_dic.current_address_address_local}
    ${param_current_address_commune}		[Common] - Create string param		commune		${arg_dic.current_address_commune}
    ${param_current_address_commune_local}		[Common] - Create string param		commune_local		${arg_dic.current_address_commune_local}
    ${param_current_address_district}		[Common] - Create string param		district		${arg_dic.current_address_district}
    ${param_current_address_district_local}		[Common] - Create string param		district_local		${arg_dic.current_address_district_local}
    ${param_current_address_city}		[Common] - Create string param		city		${arg_dic.current_address_city}
    ${param_current_address_city_local}		[Common] - Create string param		city_local		${arg_dic.current_address_city_local}
    ${param_current_address_province}		[Common] - Create string param		province		${arg_dic.current_address_province}
    ${param_current_address_province_local}		[Common] - Create string param		province_local		${arg_dic.current_address_province_local}
    ${param_current_address_postal_code}		[Common] - Create string param		postal_code		${arg_dic.current_address_postal_code}
    ${param_current_address_postal_code_local}		[Common] - Create string param		postal_code_local		${arg_dic.current_address_postal_code_local}
    ${param_current_address_country}		[Common] - Create string param		country		${arg_dic.current_address_country}
    ${param_current_address_country_local}		[Common] - Create string param		country_local	${arg_dic.current_address_country_local}
    ${param_current_address_landmark}		[Common] - Create string param		landmark		${arg_dic.current_address_landmark}
    ${param_current_address_longitude}		[Common] - Create string param		longitude		${arg_dic.current_address_longitude}
    ${param_current_address_latitude}		[Common] - Create string param		latitude		${arg_dic.current_address_latitude}
    ${param_permanent_address_citizen_association}		[Common] - Create string param		citizen_association		${arg_dic.permanent_address_citizen_association}
    ${param_permanent_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${arg_dic.permanent_address_neighbourhood_association}
    ${param_permanent_address_address}		[Common] - Create string param		address		${arg_dic.permanent_address_address}
    ${param_permanent_address_address_local}		[Common] - Create string param		address_local		${arg_dic.permanent_address_address_local}
    ${param_permanent_address_commune}		[Common] - Create string param		commune		${arg_dic.permanent_address_commune}
    ${param_permanent_address_commune_local}		[Common] - Create string param		commune_local		${arg_dic.permanent_address_commune_local}
    ${param_permanent_address_district}		[Common] - Create string param		district		${arg_dic.permanent_address_district}
    ${param_permanent_address_district_local}		[Common] - Create string param		district_local		${arg_dic.permanent_address_district_local}
    ${param_permanent_address_city}		[Common] - Create string param		city		${arg_dic.permanent_address_city}
    ${param_permanent_address_city_local}		[Common] - Create string param		city_local		${arg_dic.permanent_address_city_local}
    ${param_permanent_address_province}		[Common] - Create string param		province		${arg_dic.permanent_address_province}
    ${param_permanent_address_province_local}		[Common] - Create string param		province_local		${arg_dic.permanent_address_province_local}
    ${param_permanent_address_postal_code}		[Common] - Create string param		postal_code		${arg_dic.permanent_address_postal_code}
    ${param_permanent_address_postal_code_local}		[Common] - Create string param		postal_code_local		${arg_dic.permanent_address_postal_code_local}
    ${param_permanent_address_country}		[Common] - Create string param		country		${arg_dic.permanent_address_country}
    ${param_permanent_address_country_local}		[Common] - Create string param		country_local		${arg_dic.permanent_address_country_local}
    ${param_permanent_address_landmark}		[Common] - Create string param		landmark		${arg_dic.permanent_address_landmark}
    ${param_permanent_address_longitude}		[Common] - Create string param		longitude		${arg_dic.permanent_address_longitude}
    ${param_permanent_address_latitude}		[Common] - Create string param		latitude		${arg_dic.permanent_address_latitude}
    ${param_primary_identity_type}		[Common] - Create string param		type		${arg_dic.primary_identity_type}
    ${param_primary_identity_identity_id}		[Common] - Create string param		identity_id		${arg_dic.primary_identity_identity_id}
    ${param_primary_identity_identity_id_local}		[Common] - Create string param		identity_id_local		${arg_dic.primary_identity_identity_id_local}
    ${param_primary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${arg_dic.primary_identity_place_of_issue}
    ${param_primary_identity_issue_date}		[Common] - Create string param		issue_date		${arg_dic.primary_identity_issue_date}
    ${param_primary_identity_expired_date}		[Common] - Create string param		expired_date		${arg_dic.primary_identity_expired_date}
    ${param_primary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${arg_dic.primary_identity_front_identity_url}
    ${param_primary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${arg_dic.primary_identity_back_identity_url}
    ${param_primary_identity_signature_url}		[Common] - Create string param		signature_url		${arg_dic.primary_identity_signature_url}
    ${param_secondary_identity_type}		[Common] - Create string param		type		${arg_dic.secondary_identity_type}
    ${param_secondary_identity_identity_id}		[Common] - Create string param		identity_id		${arg_dic.secondary_identity_identity_id}
    ${param_secondary_identity_identity_id_local}		[Common] - Create string param		identity_id_local		${arg_dic.secondary_identity_identity_id_local}
    ${param_secondary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${arg_dic.secondary_identity_place_of_issue}
    ${param_secondary_identity_issue_date}		[Common] - Create string param		issue_date		${arg_dic.secondary_identity_issue_date}
    ${param_secondary_identity_expired_date}		[Common] - Create string param		expired_date		${arg_dic.secondary_identity_expired_date}
    ${param_secondary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${arg_dic.secondary_identity_front_identity_url}
    ${param_secondary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${arg_dic.secondary_identity_back_identity_url}
    ${param_secondary_identity_signature_url}		[Common] - Create string param		signature_url		${arg_dic.secondary_identity_signature_url}
    ${param_tertiary_identity_type}		[Common] - Create string param		type		${arg_dic.tertiary_identity_type}
    ${param_tertiary_identity_identity_id}		[Common] - Create string param		identity_id		${arg_dic.tertiary_identity_identity_id}
    ${param_tertiary_identity_identity_id_local}		[Common] - Create string param		identity_id_local		${arg_dic.tertiary_identity_identity_id_local}
    ${param_tertiary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${arg_dic.tertiary_identity_place_of_issue}
    ${param_tertiary_identity_issue_date}		[Common] - Create string param		issue_date		${arg_dic.tertiary_identity_issue_date}
    ${param_tertiary_identity_expired_date}		[Common] - Create string param		expired_date		${arg_dic.tertiary_identity_expired_date}
    ${param_tertiary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${arg_dic.tertiary_identity_front_identity_url}
    ${param_tertiary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${arg_dic.tertiary_identity_back_identity_url}
    ${param_tertiary_identity_signature_url}		[Common] - Create string param		signature_url		${arg_dic.tertiary_identity_signature_url}
    ${param_additional_profile_picture_url}		[Common] - Create string param		profile_picture_url		${arg_dic.additional_profile_picture_url}
    ${param_additional_tax_id_card_photo_url}		[Common] - Create string param		tax_id_card_photo_url		${arg_dic.additional_tax_id_card_photo_url}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...  {
        ...                 ${param_referrer_user_type}
        ...                 ${param_referrer_user_id}
        ...                 ${param_unique_reference}
        ...                 ${param_beneficiary}
        ...                 ${param_mm_card_type_id}
        ...                 ${param_mm_card_level_id}
        ...                 ${param_mm_factory_card_number}
        ...                 ${param_is_require_otp}
        ...                 ${param_tin_number}
        ...                 ${param_tin_number_local}
        ...                 ${param_title}
        ...                 ${param_title_local}
        ...                 ${param_first_name}
        ...                 ${param_first_name_local}
        ...                 ${param_middle_name}
        ...                 ${param_middle_name_local}
        ...                 ${param_last_name}
        ...                 ${param_last_name_local}
        ...                 ${param_suffix}
        ...                 ${param_suffix_local}
        ...                 ${param_date_of_birth}
        ...                 ${param_place_of_birth}
        ...                 ${param_place_of_birth_local}
        ...                 ${param_gender}
        ...                 ${param_gender_local}
        ...                 ${param_ethnicity}
        ...                 ${param_employer}
        ...                 ${param_nationality}
        ...                 ${param_occupation}
        ...                 ${param_occupation_local}
        ...                 ${param_occupation_title}
        ...                 ${param_occupation_title_local}
        ...                 ${param_township_code}
        ...                 ${param_township_name}
        ...                 ${param_township_name_local}
        ...                 ${param_mother_name}
        ...                 ${param_mother_name_local}
        ...                 ${param_mother_maiden_name}
        ...                 ${param_mother_maiden_name_local}
        ...                 ${param_civil_status}
        ...                 ${param_email}
        ...                 ${param_primary_mobile_number}
        ...                 ${param_secondary_mobile_number}
        ...                 ${param_tertiary_mobile_number}
        ...                 ${param_telephone_number}
        ...                 "address": {
        ...                     "current_address": {
        ...      					${param_current_address_citizen_association}
        ...      					${param_current_address_neighbourhood_association}
        ...      					${param_current_address_address}
        ...      					${param_current_address_commune}
        ...      					${param_current_address_district}
        ...      					${param_current_address_city}
        ...      					${param_current_address_province}
        ...      					${param_current_address_postal_code}
        ...      					${param_current_address_country}
        ...      					${param_current_address_landmark}
        ...      					${param_current_address_longitude}
        ...      					${param_current_address_latitude}
        ...      					${param_current_address_address_local}
        ...      					${param_current_address_commune_local}
        ...      					${param_current_address_district_local}
        ...      					${param_current_address_city_local}
        ...      					${param_current_address_province_local}
        ...      					${param_current_address_postal_code_local}
        ...      					${param_current_address_country_local}
        ...                     },
        ...                     "permanent_address": {
        ...      					${param_permanent_address_citizen_association}
        ...      					${param_permanent_address_neighbourhood_association}
        ...      					${param_permanent_address_address}
        ...      					${param_permanent_address_commune}
        ...      					${param_permanent_address_district}
        ...      					${param_permanent_address_city}
        ...      					${param_permanent_address_province}
        ...      					${param_permanent_address_postal_code}
        ...      					${param_permanent_address_country}
        ...      					${param_permanent_address_landmark}
        ...      					${param_permanent_address_longitude}
        ...      					${param_permanent_address_latitude}
        ...      					${param_permanent_address_address_local}
        ...      					${param_permanent_address_commune_local}
        ...      					${param_permanent_address_district_local}
        ...      					${param_permanent_address_city_local}
        ...      					${param_permanent_address_province_local}
        ...      					${param_permanent_address_postal_code_local}
        ...      					${param_permanent_address_country_local}
        ...                     }
        ...                 },
        ...                 "kyc": {
        ...                     "primary_identity": {
        ...      					${param_primary_identity_type}
        ...      					${param_primary_identity_identity_id}
        ...      					${param_primary_identity_identity_id_local}
        ...      					${param_primary_identity_place_of_issue}
        ...      					${param_primary_identity_issue_date}
        ...      					${param_primary_identity_expired_date}
        ...      					${param_primary_identity_front_identity_url}
        ...      					${param_primary_identity_back_identity_url}
        ...      					${param_primary_identity_signature_url}
        ...                     },
        ...                     "secondary_identity": {
        ...      					${param_secondary_identity_type}
        ...      					${param_secondary_identity_identity_id}
        ...      					${param_secondary_identity_identity_id_local}
        ...      					${param_secondary_identity_place_of_issue}
        ...      					${param_secondary_identity_issue_date}
        ...      					${param_secondary_identity_expired_date}
        ...      					${param_secondary_identity_front_identity_url}
        ...      					${param_secondary_identity_back_identity_url}
        ...      					${param_secondary_identity_signature_url}
        ...                     },
        ...                     "tertiary_identity": {
        ...      					${param_tertiary_identity_type}
        ...      					${param_tertiary_identity_identity_id}
        ...      					${param_tertiary_identity_identity_id_local}
        ...      					${param_tertiary_identity_place_of_issue}
        ...      					${param_tertiary_identity_issue_date}
        ...      					${param_tertiary_identity_expired_date}
        ...      					${param_tertiary_identity_front_identity_url}
        ...      					${param_tertiary_identity_back_identity_url}
        ...      					${param_tertiary_identity_signature_url}
        ...                     }
        ...                 },
        ...                 "additional": {
        ...      				${param_additional_profile_picture_url}
        ...      				${param_additional_tax_id_card_photo_url}
        ...                 }
        ...  }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${customer_update_profile_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API customer updates customer profile with PATCH method
    [Arguments]     ${arg_dic}
    ${param_referrer_user_type}     create json data for user type object       referrer_user_type   ${arg_dic.referrer_user_type_id}      ${arg_dic.referrer_user_type_name}
    ${param_referrer_user_id}		[Common] - Create int param		referrer_user_id		${arg_dic.referrer_user_id}
    ${param_beneficiary}		[Common] - Create string param		beneficiary		${arg_dic.beneficiary}
    ${param_mm_card_type_id}		[Common] - Create int param		mm_card_type_id		${arg_dic.mm_card_type_id}
    ${param_mm_card_level_id}		[Common] - Create int param		mm_card_level_id		${arg_dic.mm_card_level_id}
    ${param_mm_factory_card_number}		[Common] - Create string param		mm_factory_card_number		${arg_dic.mm_factory_card_number}
    ${param_is_require_otp}		[Common] - Create boolean param		is_require_otp		${arg_dic.is_require_otp}
    ${param_tin_number}		[Common] - Create string param		tin_number		${arg_dic.tin_number}
    ${param_tin_number_local}		[Common] - Create string param		tin_number_local		${arg_dic.tin_number_local}
    ${param_title}		[Common] - Create string param		title		${arg_dic.title}
    ${param_title_local}		[Common] - Create string param		title_local		${arg_dic.title_local}
    ${param_first_name}		[Common] - Create string param		first_name		${arg_dic.first_name}
    ${param_first_name_local}		[Common] - Create string param		first_name_local		${arg_dic.first_name_local}
    ${param_middle_name}		[Common] - Create string param		middle_name		${arg_dic.middle_name}
    ${param_middle_name_local}		[Common] - Create string param		middle_name_local		${arg_dic.middle_name_local}
    ${param_last_name}		[Common] - Create string param		last_name		${arg_dic.last_name}
    ${param_last_name_local}		[Common] - Create string param		last_name_local		${arg_dic.last_name_local}
    ${param_suffix}		[Common] - Create string param		suffix		${arg_dic.suffix}
    ${param_suffix_local}		[Common] - Create string param		suffix_local		${arg_dic.suffix_local}
    ${param_date_of_birth}		[Common] - Create string param		date_of_birth		${arg_dic.date_of_birth}
    ${param_place_of_birth}		[Common] - Create string param		place_of_birth		${arg_dic.place_of_birth}
    ${param_place_of_birth_local}		[Common] - Create string param		place_of_birth_local		${arg_dic.place_of_birth_local}
    ${param_gender}		[Common] - Create string param		gender		${arg_dic.gender}
    ${param_gender_local}		[Common] - Create string param		gender_local		${arg_dic.gender_local}
    ${param_ethnicity}		[Common] - Create string param		ethnicity		${arg_dic.ethnicity}
    ${param_employer}		[Common] - Create string param		employer		${arg_dic.employer}
    ${param_nationality}		[Common] - Create string param		nationality		${arg_dic.nationality}
    ${param_occupation}		[Common] - Create string param		occupation		${arg_dic.occupation}
    ${param_occupation_local}		[Common] - Create string param		occupation_local		${arg_dic.occupation_local}
    ${param_occupation_title}		[Common] - Create string param		occupation_title		${arg_dic.occupation_title}
    ${param_occupation_title_local}		[Common] - Create string param		occupation_title_local		${arg_dic.occupation_title_local}
    ${param_township_code}		[Common] - Create string param		township_code		${arg_dic.township_code}
    ${param_township_name}		[Common] - Create string param		township_name		${arg_dic.township_name}
    ${param_township_name_local}		[Common] - Create string param		township_name_local		${arg_dic.township_name_local}
    ${param_mother_name}		[Common] - Create string param		mother_name		${arg_dic.mother_name}
    ${param_mother_name_local}		[Common] - Create string param		mother_name_local		${arg_dic.mother_name_local}
    ${param_mother_maiden_name}		[Common] - Create string param		mother_maiden_name		${arg_dic.mother_maiden_name}
    ${param_mother_maiden_name_local}		[Common] - Create string param		mother_maiden_name_local		${arg_dic.mother_maiden_name_local}
    ${param_civil_status}		[Common] - Create string param		civil_status		${arg_dic.civil_status}
    ${param_email}		[Common] - Create string param		email		${arg_dic.email}
    ${param_primary_mobile_number}		[Common] - Create string param		primary_mobile_number		${arg_dic.primary_mobile_number}
    ${param_secondary_mobile_number}		[Common] - Create string param		secondary_mobile_number		${arg_dic.secondary_mobile_number}
    ${param_tertiary_mobile_number}		[Common] - Create string param		tertiary_mobile_number		${arg_dic.tertiary_mobile_number}
    ${param_telephone_number}		[Common] - Create string param		telephone_number		${arg_dic.telephone_number}
    ${param_current_address_citizen_association}		[Common] - Create string param		citizen_association		${arg_dic.current_address_citizen_association}
    ${param_current_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${arg_dic.current_address_neighbourhood_association}
    ${param_current_address_address}		[Common] - Create string param		address		${arg_dic.current_address_address}
    ${param_current_address_address_local}		[Common] - Create string param		address_local		${arg_dic.current_address_address_local}
    ${param_current_address_commune}		[Common] - Create string param		commune		${arg_dic.current_address_commune}
    ${param_current_address_commune_local}		[Common] - Create string param		commune_local		${arg_dic.current_address_commune_local}
    ${param_current_address_district}		[Common] - Create string param		district		${arg_dic.current_address_district}
    ${param_current_address_district_local}		[Common] - Create string param		district_local		${arg_dic.current_address_district_local}
    ${param_current_address_city}		[Common] - Create string param		city		${arg_dic.current_address_city}
    ${param_current_address_city_local}		[Common] - Create string param		city_local		${arg_dic.current_address_city_local}
    ${param_current_address_province}		[Common] - Create string param		province		${arg_dic.current_address_province}
    ${param_current_address_province_local}		[Common] - Create string param		province_local		${arg_dic.current_address_province_local}
    ${param_current_address_postal_code}		[Common] - Create string param		postal_code		${arg_dic.current_address_postal_code}
    ${param_current_address_postal_code_local}		[Common] - Create string param		postal_code_local		${arg_dic.current_address_postal_code_local}
    ${param_current_address_country}		[Common] - Create string param		country		${arg_dic.current_address_country}
    ${param_current_address_country_local}		[Common] - Create string param		country_local	${arg_dic.current_address_country_local}
    ${param_current_address_landmark}		[Common] - Create string param		landmark		${arg_dic.current_address_landmark}
    ${param_current_address_longitude}		[Common] - Create string param		longitude		${arg_dic.current_address_longitude}
    ${param_current_address_latitude}		[Common] - Create string param		latitude		${arg_dic.current_address_latitude}
    ${param_permanent_address_citizen_association}		[Common] - Create string param		citizen_association		${arg_dic.permanent_address_citizen_association}
    ${param_permanent_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${arg_dic.permanent_address_neighbourhood_association}
    ${param_permanent_address_address}		[Common] - Create string param		address		${arg_dic.permanent_address_address}
    ${param_permanent_address_address_local}		[Common] - Create string param		address_local		${arg_dic.permanent_address_address_local}
    ${param_permanent_address_commune}		[Common] - Create string param		commune		${arg_dic.permanent_address_commune}
    ${param_permanent_address_commune_local}		[Common] - Create string param		commune_local		${arg_dic.permanent_address_commune_local}
    ${param_permanent_address_district}		[Common] - Create string param		district		${arg_dic.permanent_address_district}
    ${param_permanent_address_district_local}		[Common] - Create string param		district_local		${arg_dic.permanent_address_district_local}
    ${param_permanent_address_city}		[Common] - Create string param		city		${arg_dic.permanent_address_city}
    ${param_permanent_address_city_local}		[Common] - Create string param		city_local		${arg_dic.permanent_address_city_local}
    ${param_permanent_address_province}		[Common] - Create string param		province		${arg_dic.permanent_address_province}
    ${param_permanent_address_province_local}		[Common] - Create string param		province_local		${arg_dic.permanent_address_province_local}
    ${param_permanent_address_postal_code}		[Common] - Create string param		postal_code		${arg_dic.permanent_address_postal_code}
    ${param_permanent_address_postal_code_local}		[Common] - Create string param		postal_code_local		${arg_dic.permanent_address_postal_code_local}
    ${param_permanent_address_country}		[Common] - Create string param		country		${arg_dic.permanent_address_country}
    ${param_permanent_address_country_local}		[Common] - Create string param		country_local		${arg_dic.permanent_address_country_local}
    ${param_permanent_address_landmark}		[Common] - Create string param		landmark		${arg_dic.permanent_address_landmark}
    ${param_permanent_address_longitude}		[Common] - Create string param		longitude		${arg_dic.permanent_address_longitude}
    ${param_permanent_address_latitude}		[Common] - Create string param		latitude		${arg_dic.permanent_address_latitude}
    ${param_primary_identity_type}		[Common] - Create string param		type		${arg_dic.primary_identity_type}
    ${param_primary_identity_identity_id}		[Common] - Create string param		identity_id		${arg_dic.primary_identity_identity_id}
    ${param_primary_identity_identity_id_local}		[Common] - Create string param		identity_id_local		${arg_dic.primary_identity_identity_id_local}
    ${param_primary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${arg_dic.primary_identity_place_of_issue}
    ${param_primary_identity_issue_date}		[Common] - Create string param		issue_date		${arg_dic.primary_identity_issue_date}
    ${param_primary_identity_expired_date}		[Common] - Create string param		expired_date		${arg_dic.primary_identity_expired_date}
    ${param_primary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${arg_dic.primary_identity_front_identity_url}
    ${param_primary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${arg_dic.primary_identity_back_identity_url}
    ${param_primary_identity_signature_url}		[Common] - Create string param		signature_url		${arg_dic.primary_identity_signature_url}
    ${param_secondary_identity_type}		[Common] - Create string param		type		${arg_dic.secondary_identity_type}
    ${param_secondary_identity_identity_id}		[Common] - Create string param		identity_id		${arg_dic.secondary_identity_identity_id}
    ${param_secondary_identity_identity_id_local}		[Common] - Create string param		identity_id_local		${arg_dic.secondary_identity_identity_id_local}
    ${param_secondary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${arg_dic.secondary_identity_place_of_issue}
    ${param_secondary_identity_issue_date}		[Common] - Create string param		issue_date		${arg_dic.secondary_identity_issue_date}
    ${param_secondary_identity_expired_date}		[Common] - Create string param		expired_date		${arg_dic.secondary_identity_expired_date}
    ${param_secondary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${arg_dic.secondary_identity_front_identity_url}
    ${param_secondary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${arg_dic.secondary_identity_back_identity_url}
    ${param_secondary_identity_signature_url}		[Common] - Create string param		signature_url		${arg_dic.secondary_identity_signature_url}
    ${param_tertiary_identity_type}		[Common] - Create string param		type		${arg_dic.tertiary_identity_type}
    ${param_tertiary_identity_identity_id}		[Common] - Create string param		identity_id		${arg_dic.tertiary_identity_identity_id}
    ${param_tertiary_identity_identity_id_local}		[Common] - Create string param		identity_id_local		${arg_dic.tertiary_identity_identity_id_local}
    ${param_tertiary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${arg_dic.tertiary_identity_place_of_issue}
    ${param_tertiary_identity_issue_date}		[Common] - Create string param		issue_date		${arg_dic.tertiary_identity_issue_date}
    ${param_tertiary_identity_expired_date}		[Common] - Create string param		expired_date		${arg_dic.tertiary_identity_expired_date}
    ${param_tertiary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${arg_dic.tertiary_identity_front_identity_url}
    ${param_tertiary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${arg_dic.tertiary_identity_back_identity_url}
    ${param_tertiary_identity_signature_url}		[Common] - Create string param		signature_url		${arg_dic.tertiary_identity_signature_url}
    ${param_additional_profile_picture_url}		[Common] - Create string param		profile_picture_url		${arg_dic.additional_profile_picture_url}
    ${param_additional_tax_id_card_photo_url}		[Common] - Create string param		tax_id_card_photo_url		${arg_dic.additional_tax_id_card_photo_url}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...  {
        ...                 ${param_referrer_user_type}
        ...                 ${param_referrer_user_id}
        ...                 ${param_beneficiary}
        ...                 ${param_mm_card_type_id}
        ...                 ${param_mm_card_level_id}
        ...                 ${param_mm_factory_card_number}
        ...                 ${param_is_require_otp}
        ...                 ${param_tin_number}
        ...                 ${param_tin_number_local}
        ...                 ${param_title}
        ...                 ${param_title_local}
        ...                 ${param_first_name}
        ...                 ${param_first_name_local}
        ...                 ${param_middle_name}
        ...                 ${param_middle_name_local}
        ...                 ${param_last_name}
        ...                 ${param_last_name_local}
        ...                 ${param_suffix}
        ...                 ${param_suffix_local}
        ...                 ${param_date_of_birth}
        ...                 ${param_place_of_birth}
        ...                 ${param_place_of_birth_local}
        ...                 ${param_gender}
        ...                 ${param_gender_local}
        ...                 ${param_ethnicity}
        ...                 ${param_employer}
        ...                 ${param_nationality}
        ...                 ${param_occupation}
        ...                 ${param_occupation_local}
        ...                 ${param_occupation_title}
        ...                 ${param_occupation_title_local}
        ...                 ${param_township_code}
        ...                 ${param_township_name}
        ...                 ${param_township_name_local}
        ...                 ${param_mother_name}
        ...                 ${param_mother_name_local}
        ...                 ${param_mother_maiden_name}
        ...                 ${param_mother_maiden_name_local}
        ...                 ${param_civil_status}
        ...                 ${param_email}
        ...                 ${param_primary_mobile_number}
        ...                 ${param_secondary_mobile_number}
        ...                 ${param_tertiary_mobile_number}
        ...                 ${param_telephone_number}
        ...                 "address": {
        ...                     "current_address": {
        ...      					${param_current_address_citizen_association}
        ...      					${param_current_address_neighbourhood_association}
        ...      					${param_current_address_address}
        ...      					${param_current_address_commune}
        ...      					${param_current_address_district}
        ...      					${param_current_address_city}
        ...      					${param_current_address_province}
        ...      					${param_current_address_postal_code}
        ...      					${param_current_address_country}
        ...      					${param_current_address_landmark}
        ...      					${param_current_address_longitude}
        ...      					${param_current_address_latitude}
        ...      					${param_current_address_address_local}
        ...      					${param_current_address_commune_local}
        ...      					${param_current_address_district_local}
        ...      					${param_current_address_city_local}
        ...      					${param_current_address_province_local}
        ...      					${param_current_address_postal_code_local}
        ...      					${param_current_address_country_local}
        ...                     },
        ...                     "permanent_address": {
        ...      					${param_permanent_address_citizen_association}
        ...      					${param_permanent_address_neighbourhood_association}
        ...      					${param_permanent_address_address}
        ...      					${param_permanent_address_commune}
        ...      					${param_permanent_address_district}
        ...      					${param_permanent_address_city}
        ...      					${param_permanent_address_province}
        ...      					${param_permanent_address_postal_code}
        ...      					${param_permanent_address_country}
        ...      					${param_permanent_address_landmark}
        ...      					${param_permanent_address_longitude}
        ...      					${param_permanent_address_latitude}
        ...      					${param_permanent_address_address_local}
        ...      					${param_permanent_address_commune_local}
        ...      					${param_permanent_address_district_local}
        ...      					${param_permanent_address_city_local}
        ...      					${param_permanent_address_province_local}
        ...      					${param_permanent_address_postal_code_local}
        ...      					${param_permanent_address_country_local}
        ...                     }
        ...                 },
        ...                 "kyc": {
        ...                     "primary_identity": {
        ...      					${param_primary_identity_type}
        ...      					${param_primary_identity_identity_id}
        ...      					${param_primary_identity_identity_id_local}
        ...      					${param_primary_identity_place_of_issue}
        ...      					${param_primary_identity_issue_date}
        ...      					${param_primary_identity_expired_date}
        ...      					${param_primary_identity_front_identity_url}
        ...      					${param_primary_identity_back_identity_url}
        ...      					${param_primary_identity_signature_url}
        ...                     },
        ...                     "secondary_identity": {
        ...      					${param_secondary_identity_type}
        ...      					${param_secondary_identity_identity_id}
        ...      					${param_secondary_identity_identity_id_local}
        ...      					${param_secondary_identity_place_of_issue}
        ...      					${param_secondary_identity_issue_date}
        ...      					${param_secondary_identity_expired_date}
        ...      					${param_secondary_identity_front_identity_url}
        ...      					${param_secondary_identity_back_identity_url}
        ...      					${param_secondary_identity_signature_url}
        ...                     },
        ...                     "tertiary_identity": {
        ...      					${param_tertiary_identity_type}
        ...      					${param_tertiary_identity_identity_id}
        ...      					${param_tertiary_identity_identity_id_local}
        ...      					${param_tertiary_identity_place_of_issue}
        ...      					${param_tertiary_identity_issue_date}
        ...      					${param_tertiary_identity_expired_date}
        ...      					${param_tertiary_identity_front_identity_url}
        ...      					${param_tertiary_identity_back_identity_url}
        ...      					${param_tertiary_identity_signature_url}
        ...                     }
        ...                 },
        ...                 "additional": {
        ...      				${param_additional_profile_picture_url}
        ...      				${param_additional_tax_id_card_photo_url}
        ...                 }
        ...  }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             patch request
        ...     api
        ...     ${customer_update_profile_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
        ...     ${NONE}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete customer
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${customer_id}=    convert to string   ${arg_dic.customer_id}
    ${delete_customer_path}=    replace string      ${delete_customer_path}      {customer_id}        ${customer_id}   1
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${delete_customer_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API update customer kyc
    [Arguments]     ${arg_dic}
    ${param_kyc_level}		[Common] - Create int param		level		${arg_dic.kyc_level}
    ${param_kyc_remark}		[Common] - Create string param		remark		${arg_dic.kyc_remark}
    ${param_kyc_verify_by}		[Common] - Create string param		verify_by		${arg_dic.kyc_verify_by}
    ${param_kyc_verify_date}		[Common] - Create string param		verify_date		${arg_dic.kyc_verify_date}
    ${param_kyc_risk_level}		[Common] - Create string param		risk_level		${arg_dic.kyc_risk_level}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		                ${param_kyc_level}
        ...      				${param_kyc_remark}
        ...      				${param_kyc_verify_by}
        ...      				${param_kyc_verify_date}
        ...      				${param_kyc_risk_level}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${customer_id}=    convert to string   ${arg_dic.customer_id}
    ${update_customer_kyc_path}=    replace string      ${update_customer_kyc_path}      {customer_id}        ${customer_id}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${update_customer_kyc_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API reset customer password
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${customer_id}=    convert to string   ${arg_dic.customer_id}
    ${identity_id}=    convert to string   ${arg_dic.identity_id}
    ${reset_customer_password_path}=    replace string      ${reset_customer_password_path}      {customer_id}        ${customer_id}   1
    ${reset_customer_password_path}=    replace string      ${reset_customer_password_path}      {identity_id}        ${identity_id}   1
                create session          api        ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${reset_customer_password_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}


API delete customer identity
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${identity_id}=    convert to string   ${arg_dic.identity_id}
    ${delete_customer_identity_path}=    replace string      ${delete_customer_identity_path}      {identity_id}        ${identity_id}   1
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${delete_customer_identity_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API update customer password
    [Arguments]     ${arg_dic}
    ${param_old_password}=  [Common] - Create string param     old_password        ${arg_dic.old_password}
    ${param_new_password}=  [Common] - Create string param     new_password        ${arg_dic.new_password}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_old_password}
        ...		${param_new_password}
        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${identity_id}=    convert to string   ${arg_dic.identity_id}
    ${update_customer_password_path}=    replace string      ${update_customer_password_path}      {identity_id}        ${identity_id}   1
    create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${update_customer_password_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API update customer identity
    [Arguments]     ${arg_dic}
    ${param_username}=  [Common] - Create string param     username        ${arg_dic.username}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_username}
        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${identity_id}=    convert to string   ${arg_dic.identity_id}
    ${update_customer_identity_path}=    replace string      ${update_customer_identity_path}      {identity_id}        ${identity_id}   1
    create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${update_customer_identity_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API customer creates device
    [Arguments]     ${arg_dic}
    ${param_channel_type_id}		[Common] - Create int param		channel_type_id		${arg_dic.channel_type_id}
    ${param_channel_id}		[Common] - Create int param		channel_id		${arg_dic.channel_id}
    ${param_shop_id}		[Common] - Create int param		shop_id		${arg_dic.shop_id}
    ${param_mac_address}		[Common] - Create string param		mac_address		${arg_dic.mac_address}
    ${param_network_provider_name}		[Common] - Create string param		network_provider_name		${arg_dic.network_provider_name}
    ${param_public_ip_address}		[Common] - Create string param		public_ip_address		${arg_dic.public_ip_address}
    ${param_supporting_file_1}		[Common] - Create string param		supporting_file_1		${arg_dic.supporting_file_1}
    ${param_supporting_file_2}		[Common] - Create string param		supporting_file_2		${arg_dic.supporting_file_2}
    ${param_device_name}		[Common] - Create string param		device_name		${arg_dic.device_name}
    ${param_device_model}		[Common] - Create string param		device_model		${arg_dic.device_model}
    ${param_device_unique_reference}		[Common] - Create string param		device_unique_reference		${arg_dic.device_unique_reference}
    ${param_os}		[Common] - Create string param		os		${arg_dic.os}
    ${param_os_version}		[Common] - Create string param		os_version		${arg_dic.os_version}
    ${param_display_size_in_inches}		[Common] - Create string param		display_size_in_inches		${arg_dic.display_size_in_inches}
    ${param_pixel_counts}		[Common] - Create string param		pixel_counts		${arg_dic.pixel_counts}
    ${param_unique_number}		[Common] - Create string param		unique_number		${arg_dic.unique_number}
    ${param_serial_number}		[Common] - Create string param		serial_number		${arg_dic.serial_number}
    ${param_app_version}		[Common] - Create string param		app_version		${arg_dic.app_version}
    ${param_edc_serial_number}		[Common] - Create string param		edc_serial_number		${arg_dic.edc_serial_number}
    ${param_edc_model}		[Common] - Create string param		edc_model		${arg_dic.edc_model}
    ${param_edc_firmware_version}		[Common] - Create string param		edc_firmware_version		${arg_dic.edc_firmware_version}
    ${param_edc_software_version}		[Common] - Create string param		edc_software_version		${arg_dic.edc_software_version}
    ${param_edc_sim_card_number}		[Common] - Create string param		edc_sim_card_number		${arg_dic.edc_sim_card_number}
    ${param_edc_battery_serial_number}		[Common] - Create string param		edc_battery_serial_number		${arg_dic.edc_battery_serial_number}
    ${param_edc_adapter_serial_number}		[Common] - Create string param		edc_adapter_serial_number		${arg_dic.edc_adapter_serial_number}
    ${param_edc_smartcard_1_number}		[Common] - Create string param		edc_smartcard_1_number		${arg_dic.edc_smartcard_1_number}
    ${param_edc_smartcard_2_number}		[Common] - Create string param		edc_smartcard_2_number		${arg_dic.edc_smartcard_2_number}
    ${param_edc_smartcard_3_number}		[Common] - Create string param		edc_smartcard_3_number		${arg_dic.edc_smartcard_3_number}
    ${param_edc_smartcard_4_number}		[Common] - Create string param		edc_smartcard_4_number		${arg_dic.edc_smartcard_4_number}
    ${param_edc_smartcard_5_number}		[Common] - Create string param		edc_smartcard_5_number		${arg_dic.edc_smartcard_5_number}
    ${param_pos_serial_number}		[Common] - Create string param		pos_serial_number		${arg_dic.pos_serial_number}
    ${param_pos_model}		[Common] - Create string param		pos_model		${arg_dic.pos_model}
    ${param_pos_firmware_version}		[Common] - Create string param		pos_firmware_version		${arg_dic.pos_firmware_version}
    ${param_pos_software_version}		[Common] - Create string param		pos_software_version		${arg_dic.pos_software_version}
    ${param_pos_smartcard_1_number}		[Common] - Create string param		pos_smartcard_1_number		${arg_dic.pos_smartcard_1_number}
    ${param_pos_smartcard_2_number}		[Common] - Create string param		pos_smartcard_2_number		${arg_dic.pos_smartcard_2_number}
    ${param_pos_smartcard_3_number}		[Common] - Create string param		pos_smartcard_3_number		${arg_dic.pos_smartcard_3_number}
    ${param_pos_smartcard_4_number}		[Common] - Create string param		pos_smartcard_4_number		${arg_dic.pos_smartcard_4_number}
    ${param_pos_smartcard_5_number}		[Common] - Create string param		pos_smartcard_5_number		${arg_dic.pos_smartcard_5_number}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...            ${param_channel_type_id}
        ...            ${param_channel_id}
        ...            ${param_shop_id}
        ...            ${param_mac_address}
        ...            ${param_network_provider_name}
        ...            ${param_public_ip_address}
        ...            ${param_supporting_file_1}
        ...            ${param_supporting_file_2}
        ...            ${param_device_name}
        ...            ${param_device_model}
        ...            ${param_device_unique_reference}
        ...            ${param_os}
        ...            ${param_os_version}
        ...            ${param_display_size_in_inches}
        ...            ${param_pixel_counts}
        ...            ${param_unique_number}
        ...            ${param_serial_number}
        ...            ${param_edc_serial_number}
        ...            ${param_edc_model}
        ...            ${param_edc_firmware_version}
        ...            ${param_edc_software_version}
        ...            ${param_edc_sim_card_number}
        ...            ${param_edc_battery_serial_number}
        ...            ${param_edc_adapter_serial_number}
        ...            ${param_edc_smartcard_1_number}
        ...            ${param_edc_smartcard_2_number}
        ...            ${param_edc_smartcard_3_number}
        ...            ${param_edc_smartcard_4_number}
        ...            ${param_edc_smartcard_5_number}
        ...            ${param_pos_serial_number}
        ...            ${param_pos_model}
        ...            ${param_pos_firmware_version}
        ...            ${param_pos_software_version}
        ...            ${param_pos_smartcard_1_number}
        ...            ${param_pos_smartcard_2_number}
        ...            ${param_pos_smartcard_3_number}
        ...            ${param_pos_smartcard_4_number}
        ...            ${param_pos_smartcard_5_number}

        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${customer_create_devices_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API customer updates device
    [Arguments]     ${arg_dic}
    ${param_channel_type_id}		[Common] - Create int param		channel_type_id		${arg_dic.channel_type_id}
    ${param_channel_id}		[Common] - Create int param		channel_id		${arg_dic.channel_id}
    ${param_shop_id}		[Common] - Create int param		shop_id		${arg_dic.shop_id}
    ${param_mac_address}		[Common] - Create string param		mac_address		${arg_dic.mac_address}
    ${param_network_provider_name}		[Common] - Create string param		network_provider_name		${arg_dic.network_provider_name}
    ${param_public_ip_address}		[Common] - Create string param		public_ip_address		${arg_dic.public_ip_address}
    ${param_supporting_file_1}		[Common] - Create string param		supporting_file_1		${arg_dic.supporting_file_1}
    ${param_supporting_file_2}		[Common] - Create string param		supporting_file_2		${arg_dic.supporting_file_2}
    ${param_device_name}		[Common] - Create string param		device_name		${arg_dic.device_name}
    ${param_device_model}		[Common] - Create string param		device_model		${arg_dic.device_model}
    ${param_device_unique_reference}		[Common] - Create string param		device_unique_reference		${arg_dic.device_unique_reference}
    ${param_os}		[Common] - Create string param		os		${arg_dic.os}
    ${param_os_version}		[Common] - Create string param		os_version		${arg_dic.os_version}
    ${param_display_size_in_inches}		[Common] - Create string param		display_size_in_inches		${arg_dic.display_size_in_inches}
    ${param_pixel_counts}		[Common] - Create string param		pixel_counts		${arg_dic.pixel_counts}
    ${param_unique_number}		[Common] - Create string param		unique_number		${arg_dic.unique_number}
    ${param_serial_number}		[Common] - Create string param		serial_number		${arg_dic.serial_number}
    ${param_app_version}		[Common] - Create string param		app_version		${arg_dic.app_version}
    ${param_edc_serial_number}		[Common] - Create string param		edc_serial_number		${arg_dic.edc_serial_number}
    ${param_edc_model}		[Common] - Create string param		edc_model		${arg_dic.edc_model}
    ${param_edc_firmware_version}		[Common] - Create string param		edc_firmware_version		${arg_dic.edc_firmware_version}
    ${param_edc_software_version}		[Common] - Create string param		edc_software_version		${arg_dic.edc_software_version}
    ${param_edc_sim_card_number}		[Common] - Create string param		edc_sim_card_number		${arg_dic.edc_sim_card_number}
    ${param_edc_battery_serial_number}		[Common] - Create string param		edc_battery_serial_number		${arg_dic.edc_battery_serial_number}
    ${param_edc_adapter_serial_number}		[Common] - Create string param		edc_adapter_serial_number		${arg_dic.edc_adapter_serial_number}
    ${param_edc_smartcard_1_number}		[Common] - Create string param		edc_smartcard_1_number		${arg_dic.edc_smartcard_1_number}
    ${param_edc_smartcard_2_number}		[Common] - Create string param		edc_smartcard_2_number		${arg_dic.edc_smartcard_2_number}
    ${param_edc_smartcard_3_number}		[Common] - Create string param		edc_smartcard_3_number		${arg_dic.edc_smartcard_3_number}
    ${param_edc_smartcard_4_number}		[Common] - Create string param		edc_smartcard_4_number		${arg_dic.edc_smartcard_4_number}
    ${param_edc_smartcard_5_number}		[Common] - Create string param		edc_smartcard_5_number		${arg_dic.edc_smartcard_5_number}
    ${param_pos_serial_number}		[Common] - Create string param		pos_serial_number		${arg_dic.pos_serial_number}
    ${param_pos_model}		[Common] - Create string param		pos_model		${arg_dic.pos_model}
    ${param_pos_firmware_version}		[Common] - Create string param		pos_firmware_version		${arg_dic.pos_firmware_version}
    ${param_pos_software_version}		[Common] - Create string param		pos_software_version		${arg_dic.pos_software_version}
    ${param_pos_smartcard_1_number}		[Common] - Create string param		pos_smartcard_1_number		${arg_dic.pos_smartcard_1_number}
    ${param_pos_smartcard_2_number}		[Common] - Create string param		pos_smartcard_2_number		${arg_dic.pos_smartcard_2_number}
    ${param_pos_smartcard_3_number}		[Common] - Create string param		pos_smartcard_3_number		${arg_dic.pos_smartcard_3_number}
    ${param_pos_smartcard_4_number}		[Common] - Create string param		pos_smartcard_4_number		${arg_dic.pos_smartcard_4_number}
    ${param_pos_smartcard_5_number}		[Common] - Create string param		pos_smartcard_5_number		${arg_dic.pos_smartcard_5_number}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...            ${param_channel_type_id}
        ...            ${param_channel_id}
        ...            ${param_shop_id}
        ...            ${param_mac_address}
        ...            ${param_network_provider_name}
        ...            ${param_public_ip_address}
        ...            ${param_supporting_file_1}
        ...            ${param_supporting_file_2}
        ...            ${param_device_name}
        ...            ${param_device_model}
        ...            ${param_device_unique_reference}
        ...            ${param_os}
        ...            ${param_os_version}
        ...            ${param_display_size_in_inches}
        ...            ${param_pixel_counts}
        ...            ${param_unique_number}
        ...            ${param_serial_number}
        ...            ${param_edc_serial_number}
        ...            ${param_edc_model}
        ...            ${param_edc_firmware_version}
        ...            ${param_edc_software_version}
        ...            ${param_edc_sim_card_number}
        ...            ${param_edc_battery_serial_number}
        ...            ${param_edc_adapter_serial_number}
        ...            ${param_edc_smartcard_1_number}
        ...            ${param_edc_smartcard_2_number}
        ...            ${param_edc_smartcard_3_number}
        ...            ${param_edc_smartcard_4_number}
        ...            ${param_edc_smartcard_5_number}
        ...            ${param_pos_serial_number}
        ...            ${param_pos_model}
        ...            ${param_pos_firmware_version}
        ...            ${param_pos_software_version}
        ...            ${param_pos_smartcard_1_number}
        ...            ${param_pos_smartcard_2_number}
        ...            ${param_pos_smartcard_3_number}
        ...            ${param_pos_smartcard_4_number}
        ...            ${param_pos_smartcard_5_number}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${device_id}=    convert to string   ${arg_dic.device_id}
    ${customer_update_devices_path}=    replace string      ${customer_update_devices_path}      {device_id}        ${device_id}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${customer_update_devices_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API system user updates customer device
    [Arguments]     ${arg_dic}
    ${param_channel_type_id}		[Common] - Create int param		channel_type_id		${arg_dic.channel_type_id}
    ${param_channel_id}		[Common] - Create int param		channel_id		${arg_dic.channel_id}
    ${param_shop_id}		[Common] - Create int param		shop_id		${arg_dic.shop_id}
    ${param_mac_address}		[Common] - Create string param		mac_address		${arg_dic.mac_address}
    ${param_network_provider_name}		[Common] - Create string param		network_provider_name		${arg_dic.network_provider_name}
    ${param_public_ip_address}		[Common] - Create string param		public_ip_address		${arg_dic.public_ip_address}
    ${param_supporting_file_1}		[Common] - Create string param		supporting_file_1		${arg_dic.supporting_file_1}
    ${param_supporting_file_2}		[Common] - Create string param		supporting_file_2		${arg_dic.supporting_file_2}
    ${param_device_name}		[Common] - Create string param		device_name		${arg_dic.device_name}
    ${param_device_model}		[Common] - Create string param		device_model		${arg_dic.device_model}
    ${param_device_unique_reference}		[Common] - Create string param		device_unique_reference		${arg_dic.device_unique_reference}
    ${param_os}		[Common] - Create string param		os		${arg_dic.os}
    ${param_os_version}		[Common] - Create string param		os_version		${arg_dic.os_version}
    ${param_display_size_in_inches}		[Common] - Create string param		display_size_in_inches		${arg_dic.display_size_in_inches}
    ${param_pixel_counts}		[Common] - Create string param		pixel_counts		${arg_dic.pixel_counts}
    ${param_unique_number}		[Common] - Create string param		unique_number		${arg_dic.unique_number}
    ${param_serial_number}		[Common] - Create string param		serial_number		${arg_dic.serial_number}
    ${param_app_version}		[Common] - Create string param		app_version		${arg_dic.app_version}
    ${param_edc_serial_number}		[Common] - Create string param		edc_serial_number		${arg_dic.edc_serial_number}
    ${param_edc_model}		[Common] - Create string param		edc_model		${arg_dic.edc_model}
    ${param_edc_firmware_version}		[Common] - Create string param		edc_firmware_version		${arg_dic.edc_firmware_version}
    ${param_edc_software_version}		[Common] - Create string param		edc_software_version		${arg_dic.edc_software_version}
    ${param_edc_sim_card_number}		[Common] - Create string param		edc_sim_card_number		${arg_dic.edc_sim_card_number}
    ${param_edc_battery_serial_number}		[Common] - Create string param		edc_battery_serial_number		${arg_dic.edc_battery_serial_number}
    ${param_edc_adapter_serial_number}		[Common] - Create string param		edc_adapter_serial_number		${arg_dic.edc_adapter_serial_number}
    ${param_edc_smartcard_1_number}		[Common] - Create string param		edc_smartcard_1_number		${arg_dic.edc_smartcard_1_number}
    ${param_edc_smartcard_2_number}		[Common] - Create string param		edc_smartcard_2_number		${arg_dic.edc_smartcard_2_number}
    ${param_edc_smartcard_3_number}		[Common] - Create string param		edc_smartcard_3_number		${arg_dic.edc_smartcard_3_number}
    ${param_edc_smartcard_4_number}		[Common] - Create string param		edc_smartcard_4_number		${arg_dic.edc_smartcard_4_number}
    ${param_edc_smartcard_5_number}		[Common] - Create string param		edc_smartcard_5_number		${arg_dic.edc_smartcard_5_number}
    ${param_pos_serial_number}		[Common] - Create string param		pos_serial_number		${arg_dic.pos_serial_number}
    ${param_pos_model}		[Common] - Create string param		pos_model		${arg_dic.pos_model}
    ${param_pos_firmware_version}		[Common] - Create string param		pos_firmware_version		${arg_dic.pos_firmware_version}
    ${param_pos_software_version}		[Common] - Create string param		pos_software_version		${arg_dic.pos_software_version}
    ${param_pos_smartcard_1_number}		[Common] - Create string param		pos_smartcard_1_number		${arg_dic.pos_smartcard_1_number}
    ${param_pos_smartcard_2_number}		[Common] - Create string param		pos_smartcard_2_number		${arg_dic.pos_smartcard_2_number}
    ${param_pos_smartcard_3_number}		[Common] - Create string param		pos_smartcard_3_number		${arg_dic.pos_smartcard_3_number}
    ${param_pos_smartcard_4_number}		[Common] - Create string param		pos_smartcard_4_number		${arg_dic.pos_smartcard_4_number}
    ${param_pos_smartcard_5_number}		[Common] - Create string param		pos_smartcard_5_number		${arg_dic.pos_smartcard_5_number}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...            ${param_channel_type_id}
        ...            ${param_channel_id}
        ...            ${param_shop_id}
        ...            ${param_mac_address}
        ...            ${param_network_provider_name}
        ...            ${param_public_ip_address}
        ...            ${param_supporting_file_1}
        ...            ${param_supporting_file_2}
        ...            ${param_device_name}
        ...            ${param_device_model}
        ...            ${param_device_unique_reference}
        ...            ${param_os}
        ...            ${param_os_version}
        ...            ${param_display_size_in_inches}
        ...            ${param_pixel_counts}
        ...            ${param_unique_number}
        ...            ${param_serial_number}
        ...            ${param_edc_serial_number}
        ...            ${param_edc_model}
        ...            ${param_edc_firmware_version}
        ...            ${param_edc_software_version}
        ...            ${param_edc_sim_card_number}
        ...            ${param_edc_battery_serial_number}
        ...            ${param_edc_adapter_serial_number}
        ...            ${param_edc_smartcard_1_number}
        ...            ${param_edc_smartcard_2_number}
        ...            ${param_edc_smartcard_3_number}
        ...            ${param_edc_smartcard_4_number}
        ...            ${param_edc_smartcard_5_number}
        ...            ${param_pos_serial_number}
        ...            ${param_pos_model}
        ...            ${param_pos_firmware_version}
        ...            ${param_pos_software_version}
        ...            ${param_pos_smartcard_1_number}
        ...            ${param_pos_smartcard_2_number}
        ...            ${param_pos_smartcard_3_number}
        ...            ${param_pos_smartcard_4_number}
        ...            ${param_pos_smartcard_5_number}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${device_id}=    convert to string   ${arg_dic.device_id}
    ${system_user_update_customer_devices_path}=    replace string      ${system_user_update_customer_devices_path}      {device_id}        ${device_id}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${system_user_update_customer_devices_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API system user updates customer device status
    [Arguments]     ${arg_dic}
    ${param_is_active}		[Common] - Create int param		is_active		${arg_dic.is_active}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...            ${param_is_active}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${device_id}=    convert to string   ${arg_dic.device_id}
    ${system_user_update_customer_device_status_path}=    replace string      ${system_user_update_customer_device_status_path}      {device_id}        ${device_id}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${system_user_update_customer_device_status_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API customer deletes device
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${device_id}=    convert to string   ${arg_dic.device_id}
    ${customer_delete_devices_path}=    replace string      ${customer_delete_devices_path}      {device_id}        ${device_id}   1
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${customer_delete_devices_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API system user deletes customer device
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${device_id}=    convert to string   ${arg_dic.device_id}
    ${system_user_delete_customer_devices_path}=    replace string      ${system_user_delete_customer_devices_path}      {device_id}        ${device_id}   1
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${system_user_delete_customer_devices_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API customer gets all devices
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${channel_id}=    convert to string   ${arg_dic.channel_id}
    ${customer_get_all_devices_path}=    replace string      ${customer_get_all_devices_path}      {channel_id}        ${channel_id}   1
                create session          api        ${api_gateway_host}
    ${response}             get request
        ...     api
        ...     ${customer_get_all_devices_path}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API system user gets customer device
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${device_id}=    convert to string   ${arg_dic.device_id}
    ${system_user_get_customer_devices_path}=    replace string      ${system_user_get_customer_devices_path}      {device_id}        ${device_id}  1
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${system_user_get_customer_devices_path}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}