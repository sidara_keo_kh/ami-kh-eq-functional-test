*** Settings ***
Resource        ../../1_common/keywords.robot


*** Keywords ***
API update password rule config
    [Arguments]  ${arg_dic}
    ${min_length}  [Common] - Create string param in dic    ${arg_dic}     min_length
    ${max_length}  [Common] - Create string param in dic    ${arg_dic}     max_length
    ${expire_after}  [Common] - Create string param in dic    ${arg_dic}     expire_after
    ${disallow_last_password}  [Common] - Create string param in dic    ${arg_dic}     disallow_last_password
    ${max_repeated_numeric_characters}  [Common] - Create string param in dic    ${arg_dic}     max_repeated_numeric_characters

    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	${min_length}
        ...     	${max_length}
        ...     	${expire_after}
        ...     	${disallow_last_password}
        ...     	${max_repeated_numeric_characters}
        ...     }
    ${data}  replace string      ${data}      ,}    }
    ${identity_type_id}        convert to string           ${arg_dic.identity_type_id}
    ${update_password_rule_config_path}          replace string          ${update_password_rule_config_path}       {identity_type_id}       ${identity_type_id}
                create session          api        ${api_gateway_host}    disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${update_password_rule_config_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create identity type
    [Arguments]  ${arg_dic}
    ${name}  [Common] - Create string param in dic    ${arg_dic}     name
    ${password_type_id}  [Common] - Create string param in dic    ${arg_dic}     password_type_id

    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	${name}
        ...     	${password_type_id}
        ...     }
    ${data}  replace string      ${data}      ,}    }
                create session          api        ${api_gateway_host}    disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_identity_type}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update identity type
    [Arguments]  ${arg_dic}
    ${name}  [Common] - Create string param in dic    ${arg_dic}     name
    ${password_type_id}  [Common] - Create string param in dic    ${arg_dic}     password_type_id

    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	${name}
        ...     	${password_type_id}
        ...     }

    ${data}  replace string      ${data}      ,}    }
    ${identity_type_id}        convert to string           ${arg_dic.identity_type_id}
    ${update_identity_type}          replace string          ${update_identity_type}       {identity_type_id}       ${identity_type_id}
                create session          api        ${api_gateway_host}    disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${update_identity_type}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete identity type
    [Arguments]  ${arg_dic}
    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${identity_type_id}        convert to string           ${arg_dic.identity_type_id}
    ${delete_identity_type}          replace string          ${delete_identity_type}       {identity_type_id}       ${identity_type_id}
                create session          api        ${api_gateway_host}    disable_warnings=1
    ${response}             delete request
        ...     api
        ...     ${delete_identity_type}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}