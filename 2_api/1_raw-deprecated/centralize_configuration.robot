*** Settings ***
Resource        ../../1_common/keywords.robot


*** Keywords ***
API update configuration detail
    [Arguments]
    ...     ${access_token}
    ...     ${device_id}
    ...     ${device_description}
    ...     ${scopeName}
    ...     ${key}
    ...     ${value}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
        ...     device_id=${device_id}
        ...     device_description=${device_description}
    ${data}              catenate
        ...	{
        ...     "value":"${value}"
        ...	}
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${update_configuration_detail_path}    replace string  ${update_configuration_detail_path}    {scopeName}     ${scopeName}
    ${update_configuration_detail_path}    replace string  ${update_configuration_detail_path}    {key}     ${key}
    ${response}             put request
        ...     api
        ...     ${update_configuration_detail_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get configuration detail
    [Arguments]
    ...     ${access_token}
    ...     ${scopeName}
    ...     ${key}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${get_configuration_detail_by_key_path}    replace string  ${get_configuration_detail_by_key_path}    {scopeName}     ${scopeName}
    ${get_configuration_detail_by_key_path}    replace string  ${get_configuration_detail_by_key_path}    {key}     ${key}
    ${response}             get request
        ...     api
        ...     ${get_configuration_detail_by_key_path}
        ...     ${header}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all configurations
    [Arguments]
    ...     ${access_token}
    ...     ${scopeName}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${get_all_configurations_path}    replace string  ${get_all_configurations_path}    {scopeName}     ${scopeName}
    ${response}             get request
        ...     api
        ...     ${get_all_configurations_path}
        ...     ${header}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all pre-load countries
    [Arguments]
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_all_pre-load_countries_path}
        ...     ${header}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get_all_pre-load_currencies
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_all_pre-load_currencies_path}
        ...     ${header}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all configuration socpe
    [Arguments]
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_all_configuration_socpe_path}
        ...     ${header}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

[Centralize Configuration] Get all currencies
    [Arguments]     &{arg_dic}
    [Common][Prepare] - Authorization headers
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     access_token=${arg_dic.access_token}
    REST.get   ${api_gateway_host}/${get_all_pre-load_currencies_path}
        ...     headers=${headers}
    rest extract