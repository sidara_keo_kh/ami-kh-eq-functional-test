*** Settings ***
Resource        ../../1_common/keywords.robot


*** Keywords ***
API agent authentication
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/x-www-form-urlencoded
        ...     channel_id=${arg_dic.channel_id}

    ${header}   [Common] - add new param in header      ${arg_dic}       device_unique_reference     ${header}
    ${params}              catenate          SEPARATOR=&
        ...     grant_type=${arg_dic.grant_type}
        ...     username=${arg_dic.username}
        ...     password=${arg_dic.password}
        ...     client_id=${arg_dic.param_client_id}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${agent_authentication_path}
        ...     ${NONE}
        ...     ${params}
        ...     ${header}
    log     ${header}
    log     ${params}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API customer authentication
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/x-www-form-urlencoded
    ${params}               catenate                SEPARATOR=&
        ...     grant_type=${arg_dic.grant_type}
        ...     username=${arg_dic.username}
        ...     password=${arg_dic.password}
        ...     client_id=${arg_dic.param_client_id}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${customer_authentication_path}
        ...     ${NONE}
        ...     ${params}
        ...     ${header}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all service
    [Arguments]      ${arg_dic}
    ${param_service_group_id}=  [Common] - Create string param     service_group_id    ${arg_dic.service_group_id}
    ${param_service_name}=      [Common] - Create string param     service_name        ${arg_dic.service_name}
    ${param_currency}=          [Common] - Create string param     currency            ${arg_dic.currency}
    ${param_description}=       [Common] - Create string param     description         ${arg_dic.description}

    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${data}              catenate
        ...     {
        ...         ${param_service_group_id}
        ...         ${param_service_name}
        ...         ${param_currency}
        ...         ${param_description}
        ...     }
    ${data}  replace string      ${data}      , }    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_all_service_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${response_text}=      set variable  ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}


API get service detail
    [Arguments]      ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    create session          api     ${api_gateway_host}     disable_warnings=1
    ${get_service_detail_path}                 replace string          ${get_service_detail_path}       {serviceId}       ${arg_dic.service_id}     1
    ${response}             get request
        ...     api
        ...     ${get_service_detail_path}
        ...     ${header}
    log      ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API create oauth client
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate
        ...	{
        ...     "client_id":"${arg_dic.data_client_id}",
        ...     "client_secret":"${arg_dic.data_client_secret}",
        ...     "client_name":"${arg_dic.client_name}",
        ...     "authorized_grant_types":"${arg_dic.authorized_grant_types}",
        ...     "web_server_redirect_uri":"${arg_dic.web_server_redirect_uri}",
        ...     "access_token_validity":"${arg_dic.access_token_validity}",
        ...     "refresh_token_validity":"${arg_dic.refresh_token_validity}",
        ...     "authorization_code_validity":"${arg_dic.authorization_code_validity}"
        ...	}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_oauth_client_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API get payload agent
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_payload_agent_path}
        ...     ${header}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get customer payload
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_customer_payload_path}
        ...     ${header}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all api
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_all_api_path}
        ...     ${header}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API add api
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
    ...     client_id=${arg_dic.client_id}
    ...     client_secret=${arg_dic.client_secret}
    ...     content-type=application/json
    ...     Authorization=Bearer ${arg_dic.access_token}

    ${data}              catenate
    ...     {
    ...       "name": "${arg_dic.name}",
    ...       "http_method": "${arg_dic.http_method}",
    ...       "pattern": "${arg_dic.pattern}",
    ...       "is_internal": ${arg_dic.is_internal},
    ...       "is_required_access_token": ${arg_dic.is_required_access_token},
    ...       "service_id": ${arg_dic.service_id},
    ...       "prefix": "${arg_dic.prefix}"
    ...     }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${add_api_path}
        ...     ${data}
        ...     ${none}
        ...     ${header}
    ${dic} =	Create Dictionary   response=${response}
    log        ${response.text}
    [Return]    ${dic}

API delete api
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${delete_api_path}       replace string  ${delete_api_path}     {apiId}      ${arg_dic.api_id}   1
    create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             delete request
        ...     api
        ...     ${delete_api_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get api detail
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${api_id}     convert to string       ${arg_dic.api_id}
    ${get_api_detail_path}                 replace string          ${get_api_detail_path}       {apiId}       ${api_id}     1
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_api_detail_path}
        ...     ${header}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get client detail
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${get_client_detail_path}                 replace string          ${get_client_detail_path}      {clientId}       ${arg_dic.client_id}     1
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_client_detail_path}
        ...     ${header}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all oauth client
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
     ${get_all_oauth_client_path}          replace string          ${get_all_oauth_client_path}       {clientId}       ${clientId}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_all_oauth_client_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get client scope detail
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
     ${get_client_scope_detail_path}          replace string          ${get_client_scope_detail_path}       {clientId}       ${arg_dic.client_id}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_client_scope_detail_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all grant types
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_all_grant_types_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get api detail by name
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
     ${apiName}        convert to string           ${arg_dic.apiName}
     ${get_api_detail_by_name_path}          replace string          ${get_api_detail_by_name_path}       {apiName}       ${apiName}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_api_detail_by_name_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get authorization code
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_authorization_code_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create authorization code
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${create_authorization_code_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}
    
API revoke token by user
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate
    ...     {
    ...       "user_ids": ${arg_dic.user_ids},
    ...       "user_type": "${arg_dic.user_type}"
    ...     }
                    create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             delete request
        ...     api
        ...     ${revoke_token_by_user_id_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}

API token verification
    [Arguments]     ${arg_dic}
    ${header}       create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}     catenate
        ...     {
        ...       "client_id": "${arg_dic.client_id}",
        ...       "token": "${arg_dic.access_token}"
        ...     }
    ${response}     post request
        ...     api
        ...     ${token_verification_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${response.text}
    [Return]  ${response}

API create api-gateway service
    [Arguments]    ${arg_dic}
    ${header}   create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${data}     catenate
        ...     {
        ...       "location": "${arg_dic.location}",
        ...       "name": "${arg_dic.name}",
        ...       "timeout": ${arg_dic.timeout},
        ...       "max_total_connection": ${arg_dic.max_total_connection},
        ...       "max_per_route": ${arg_dic.max_per_route}
        ...     }
    ${response}     post request
             ...     api
             ...     ${api_gateway_create_service_path}
             ...     ${data}
             ...     ${NONE}
             ...     ${header}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create api
    [Arguments]    ${arg_dic}
    ${header}   create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${data}     catenate
        ...     {
        ...       "name": "${arg_dic.name}",
        ...       "http_method": "${arg_dic.http_method}",
        ...       "pattern": "${arg_dic.pattern}",
        ...       "is_internal": ${arg_dic.is_internal},
        ...       "is_required_access_token": ${arg_dic.is_required_access_token},
        ...       "service_id": ${arg_dic.service_id},
        ...       "permission_required": "${arg_dic.permission_required}"
        ...     }
    ${response}     post request
        ...     api
        ...     ${add_api_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API add client scope
    [Arguments]    ${arg_dic}
    ${header}  create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${url}     replace string          ${api_gateway_add_scope_path}       {clientId}       ${arg_dic.request_client_id}
               create session          api     ${api_gateway_host}     disable_warnings=1
    ${data}     catenate
        ...     {
        ...       "scopes": ${arg_dic.scopes}
        ...     }
    ${response}     post request
        ...     api
        ...     ${url}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}