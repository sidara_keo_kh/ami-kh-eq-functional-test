*** Settings ***
Documentation    Suite description
Resource        ../../1_common/keywords.robot


*** Keywords ***

API report - get formula by report type id
    [Arguments]
        ...     ${report_type_id}
        ...     ${access_token}
        ...     ${client_id}
        ...     ${client_secret}
    ${header}               create dictionary
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}

    ${report_get_report_formula_by_report_type_id}                 replace string          ${report_get_report_formula_by_report_type_id}      {report_type_id}       ${report_type_id}
                create session          api        ${api_gateway_host}
    ${response}             get request
        ...     api
        ...     ${report_get_report_formula_by_report_type_id}
        ...     ${header}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

[Report] Get payment report formula
    [Arguments]     &{arg_dic}
    [Common][Prepare] - Authorization headers
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     access_token=${arg_dic.access_token}
    ${path}     replace string          ${report_get_report_formula_by_report_type_id}      {report_type_id}       ${arg_dic.report_type_id}
    REST.get    ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract

[Report] Update payment report formula
    [Arguments]     &{arg_dic}
    [Common][Prepare] - Authorization headers
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     access_token=${arg_dic.access_token}
    ${path}     replace string          ${report_update_report_formula_by_report_type_id}      {report_type_id}       ${arg_dic.report_type_id}
    ${body}     catenate
        ...     {
        ...         "effective_timestamp":"${arg_dic.effective_timestamp}",
        ...         "tpv": "${arg_dic.tpv}",
        ...         "fee": "${arg_dic.fee}",
        ...         "commission": "${arg_dic.commission}"
        ...     }
    REST.put    ${api_gateway_host}/${path}
        ...     body=${body}
        ...     headers=${headers}
    rest extract