*** Settings ***
Resource        ../../1_common/keywords.robot


*** Keywords ***
API generate OTP
    [Arguments]     ${arg_dic}
    ${param_user_type}=              [Common] - Create string param in dic      ${arg_dic}    user_type
    ${param_user_id}=                [Common] - Create string param in dic      ${arg_dic}    user_id
    ${param_delivery_channel}=       [Common] - Create string param in dic      ${arg_dic}    delivery_channel
    ${param_email}=                  [Common] - Create string param in dic      ${arg_dic}    email
    ${param_mobile_number}=          [Common] - Create string param in dic      ${arg_dic}    mobile_number
    ${param_is_generate_ref_code}=   [Common] - Create string param in dic      ${arg_dic}    is_generate_ref_code

    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     device_id=${arg_dic.device_id}
    ${request}              catenate        SEPARATOR=
        ...     {
        ...     	${param_user_type}
        ...     	${param_user_id}
        ...     	${param_delivery_channel}
        ...     	${param_email}
        ...     	${param_mobile_number}
        ...     	${param_is_generate_ref_code}
        ...     }
    ${request}  replace string      ${request}      ,}    }
                                create session          api     ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${generate_otp_path}
        ...     ${request}
        ...     ${NONE}
        ...     ${header}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API validate OTP
    [Arguments]     ${arg_dic}
    ${param_otp_reference_id}=      [Common] - Create string param in dic      ${arg_dic}       otp_reference_id
    ${param_otp_code}=              [Common] - Create string param in dic      ${arg_dic}       otp_code
    ${param_user_reference_code}=   [Common] - Create string param in dic      ${arg_dic}       user_reference_code
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
    ${request}              catenate        SEPARATOR=
        ...     {
        ...     	${param_otp_reference_id}
        ...     	${param_otp_code}
        ...     	${param_user_reference_code}
        ...     }
    ${request}  replace string      ${request}      ,}    }
                                create session          api     ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${validate_otp_path}
        ...     ${request}
        ...     ${NONE}
        ...     ${header}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get OTP detail on mock
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     content-type=application/json
    ${mock_get_otp_detail_path}  replace string      ${mock_get_otp_detail_path}      {otp_reference_id}    ${arg_dic.otp_reference_id}
    create session          api     ${SPI_get_otp_url}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${mock_get_otp_detail_path}
        ...     ${header}
    ${response_text}=      set variable  ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API regenerate OTP
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${data}              catenate
        ...    {
        ...    	"otp_reference_id":"${arg_dic.otp_reference_id}"
        ...    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${regenerate_otp_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}