*** Settings ***
Resource        ../../1_common/keywords.robot


*** Keywords ***
API create request to cancel voucher
    [Arguments]
    ...     ${access_token}
    ...     ${currency}
    ...     ${voucher_id_1}
    ...     ${voucher_amount_1}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${access_token}
    ${voucher_id_1}     convert to integer   ${voucher_id_1}
    ${voucher_amount_1}      convert to integer     ${voucher_amount_1}
    ${request}              catenate
        ...     {
        ...     	"currency": "${currency}",
        ...     	"vouchers": [
        ...     		{"id": ${voucher_id_1}, "amount": ${voucher_amount_1}}
        ...     	]
        ...     }
                create session          api     ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${create_request_to_cancel_voucher_path}
        ...     ${request}
        ...     ${NONE}
        ...     ${header}
    log    ${request}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API create refund voucher
    [Arguments]
    ...     ${access_token}
    ...     ${voucher_id}
    ...     ${product_ref2}
    ...     ${product_ref3}
    ...     ${product_ref4}
    ...     ${product_ref5}
    ...     ${reason_for_refund}
    ...     ${original_voucher_id}
    ...     ${amount}
    ...     ${currency}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${access_token}
    ${data}              catenate       SEPARATOR=
        ...      {
        ...        "voucher_id": ${voucher_id},
        ...        "product_ref2": "${product_ref2}",
        ...        "product_ref3": "${product_ref3}",
        ...        "product_ref4": "${product_ref4}",
        ...        "product_ref5": "${product_ref5}",
        ...        "reason_for_refund": "${reason_for_refund}",
        ...        "original_voucher_id": "${original_voucher_id}",
        ...        "amount": ${amount},
        ...        "currency": "${currency}"
        ...      }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_refund_voucher_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log  ${data}
    log  ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API approve voucher refunds
    [Arguments]
    ...     ${access_token}
    ...     ${refund_request_id_1}
    ...     ${refund_request_id_2}
    ...     ${refund_request_id_3}
    ...     ${reason}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${access_token}
    ${param_refund_request_id_1}      [Common] - Create Json data for integer value in array         ${refund_request_id_1}
    ${param_refund_request_id_2}      [Common] - Create Json data for integer value in array         ${refund_request_id_2}
    ${param_refund_request_id_3}      [Common] - Create Json data for integer value in array         ${refund_request_id_3}
    ${data}              catenate       SEPARATOR=
        ...      {
        ...        "refund_request_ids": [
        ...          ${param_refund_request_id_1}
        ...          ${param_refund_request_id_2}
        ...          ${param_refund_request_id_3}
        ...        ],
        ...        "reason": "${reason}"
        ...      }
    ${data}     replace string      ${data}      ,]    ]
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${approve_voucher_refunds_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log  ${data}
    log  ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API confirm cancel vouchers
    [Arguments]
    ...     ${access_token}
    ...     ${amount}
    ...     ${voucher_cancellation_id}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${access_token}
    ${data}              catenate
        ...     {
        ...     	"amount":"${amount}"
        ...     }
    ${voucher_cancellation_id}      convert to string      ${voucher_cancellation_id}
    ${confirm_cancel_vouchers_path}=    replace string      ${confirm_cancel_vouchers_path}        {id}        ${voucher_cancellation_id}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${confirm_cancel_vouchers_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    ${response_text}=   set variable  ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API execute adjustment order
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${reference_id}      convert to string      ${arg_dic.reference_id}
    ${execute_adjustment_order_path}=    replace string      ${execute_adjustment_order_path}        {referenceId}        ${reference_id}
    ${data}              catenate       SEPARATOR=
        ...      {
        ...        "reason": "${arg_dic.reason}"
        ...      }
    ${request}              catenate
                create session          api     ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${execute_adjustment_order_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log    ${data}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API create adjustment order
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_requestor_user_id}         [Common] - Create int param of object in dic    ${arg_dic}    requestor_user        user_id
    ${param_requestor_user_type_id}    [Common] - Create int param of object in dic    ${arg_dic}    requestor_user_user_type        id
    ${param_requestor_user_type_name}  [Common] - Create string param of object in dic    ${arg_dic}    requestor_user_user_type        name
    ${param_requestor_sof_id}          [Common] - Create int param of object in dic    ${arg_dic}    requestor_user_sof    id
    ${param_requestor_sof_type_id}     [Common] - Create int param of object in dic    ${arg_dic}    requestor_user_sof    type_id

    ${data}              catenate       SEPARATOR=
    ...    {
    ...        "product_service_id": ${arg_dic.product_service_id},
    ...        "reference_order_id": "${arg_dic.reference_order_id}",
    ...        "product": {
    ...        		"product_name": "${arg_dic.product_name}",
    ...        		"product_ref1": "${arg_dic.product_ref1}",
    ...        		"product_ref2": "${arg_dic.product_ref2}",
    ...        		"product_ref3": "${arg_dic.product_ref3}",
    ...        		"product_ref4": "${arg_dic.product_ref4}",
    ...        		"product_ref5": "${arg_dic.product_ref5}"
    ...      	},
    ...        "reason":"${arg_dic.reason}",
    ...     "requestor_user":{
    ...         ${param_requestor_user_id}
    ...         "user_type":{
    ...             ${param_requestor_user_type_id}
    ...             ${param_requestor_user_type_name}
    ...         },
    ...         "sof":{
    ...             ${param_requestor_sof_id}
    ...             ${param_requestor_sof_type_id}
    ...         }
    ...     },
    ...        "initiator":
    ...       {
    ...        "user_id": ${arg_dic.initiator_user_id},
    ...        "user_type":
    ...    	    {
    ...    	      "id": ${arg_dic.initiator_user_type_id}
    ...    	    },
    ...        "sof":
    ...    	    {
    ...    	      "id": ${arg_dic.initiator_user_sof_id},
    ...    	      "type_id": ${arg_dic.initiator_sof_type_id}
    ...    	    }
    ...      },
    ...        "payer_user":
    ...        {
    ...          "user_id": ${arg_dic.payer_user_id},
    ...          "user_type":
    ...    	     {
    ...    	      "id": ${arg_dic.payer_user_type_id}
    ...    	     },
    ...          "sof":
    ...    	     {
    ...    	      "id": ${arg_dic.payer_user_sof_id},
    ...    	      "type_id": ${arg_dic.payer_user_sof_type_id}
    ...    	     }
    ...        },
    ...        "payee_user":
    ...        {
    ...          "user_id": ${arg_dic.payee_user_id},
    ...          "user_type":
    ...    	     {
    ...    	      "id": ${arg_dic.payee_user_type_id}
    ...    	     },
    ...          "sof":
    ...    	     {
    ...    	      "id": ${arg_dic.payee_user_sof_id},
    ...    	      "type_id": ${arg_dic.payee_user_sof_type_id}
    ...    	     }
    ...        },
    ...        "amount": ${arg_dic.amount},
    ...        "batch_code": "${arg_dic.batch_code}",
    ...        "cancel_ref_id": ${arg_dic.cancel_ref_id},
    ...        "reference_service_group_id":${arg_dic.reference_service_group_id},
    ...        "reference_service_id":${arg_dic.reference_service_id}
    ...      }
    ...    }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_adjustment_order_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log  ${data}
    log  ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API reject voucher refunds
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}     catenate       SEPARATOR=
    ...    {
    ...        "refund_request_ids":[${arg_dic.refund_request_ids}],
    ...        "reason": "${arg_dic.reason}"
    ...    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             delete request
        ...     api
        ...     ${reject_voucher_refunds_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API reject adjustment order
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${referenceId}        convert to string           ${arg_dic.referenceId}
    ${reject_adjustment_order_path}          replace string          ${reject_adjustment_order_path}       {referenceId}       ${referenceId}
                        create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             delete request
        ...     api
        ...     ${reject_adjustment_order_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    ${return_dic} =	Create Dictionary   response=${response}
    log    ${response.text}
    [Return]    ${return_dic}

API approve multiple balance adjustments
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${data}              catenate           SEPARATOR=
        ...    {
        ...        "reference_ids": [${arg_dic.reference_id}],
        ...        "reason": "${arg_dic.reason}"
        ...    }
    ${data}  replace string      ${data}      ,"}    }
    ${data}  replace string      ${data}      ,],    ],
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${approve_multiple_balance_adjustments}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    ${return_dic} =	Create Dictionary   response=${response}
    log     ${response.text}
    [Return]    ${return_dic}

API reject multiple-balance adjustment orders
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${data}              catenate           SEPARATOR=
        ...    {
        ...        "reference_ids": [${arg_dic.reference_id}],
        ...        "reason": "${arg_dic.reason}"
        ...    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             delete request
        ...     api
        ...     ${reject_multiple-balance_adjustment_orders_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${return_dic} =	Create Dictionary   response=${response}
    log    ${response.text}
    [Return]    ${return_dic}