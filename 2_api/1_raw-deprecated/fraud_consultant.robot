*** Settings ***
Resource          ../../1_common/keywords.robot

*** Keywords ***
API create fraud ticket
    [Arguments]     ${arg_dic}
    ${correlation_id}       generate random string      20    [UPPER]
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${start_active_ticket_timestamp}=   [Common] - Create string param in dic    ${arg_dic}     start_active_ticket_timestamp
    ${end_active_ticket_timestamp}=     [Common] - Create string param in dic    ${arg_dic}     end_active_ticket_timestamp
#    ${end_active_timestamp_value}=      set variable if    "${end_active_ticket_timestamp}"=="${NULL}"      null        "${end_active_ticket_timestamp}"
    ${data}              catenate           SEPARATOR=
        ...	{
        ...     "action": "unstop card",
        ...     "description": "Prepare data from robot framework",
        ...      ${start_active_ticket_timestamp}
        ...      ${end_active_ticket_timestamp}
        ...     "data": [{
        ...            "card_id":${arg_dic.card_id}
        ...        }]
        ...	}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_fraud_ticket_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create new fraud ticket
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_action}=   [Common] - Create string param     action        ${arg_dic.action}
    ${start_active_ticket_timestamp}=   [Common] - Create string param     start_active_ticket_timestamp       ${arg_dic.start_active_ticket_timestamp}
    ${end_active_ticket_timestamp}=   [Common] - Create string param     end_active_ticket_timestamp       ${arg_dic.end_active_ticket_timestamp}
    ${description}=   [Common] - Create string param     description       ${arg_dic.description}


    ${data_json}    set variable    ${empty}
    ${length}   get length      ${arg_dic.data}
    :FOR     ${index}   IN RANGE   0    ${length}
    \      ${ticket_data_item}    get from list      ${arg_dic.data}      ${index}
    \      ${ticket_data_item}    strip string         ${ticket_data_item}
    \      ${data_json}     catenate    SEPARATOR=,  ${data_json}   ${ticket_data_item}
    log     ${data_json}
    ${ticket_data_json}              catenate       SEPARATOR=
        ...     	,"data":
        ...     	[
        ...             ${data_json}
        ...     	]
    ${ticket_data_json}     replace string      ${ticket_data_json}     [,        [
    ${ticket_data_json}     replace string          ${ticket_data_json}     ,]        ]

    ${data}              catenate       SEPARATOR=
        ...     {
        ...         ${param_action}
        ...         ${start_active_ticket_timestamp}
        ...         ${end_active_ticket_timestamp}
        ...         ${description}
        ...         ${ticket_data_json}
        ...     }
    ${data}  replace string        ${data}      ,,      ,
    ${data}  replace string        ${data}      ,}      }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_fraud_ticket_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${data}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete fraud ticket
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

    ${ticket_id}      convert to string     ${arg_dic.ticket_id}
    ${delete_fraud_ticket_path}       replace string          ${delete_fraud_ticket_path}     {ticket_id}       ${ticket_id}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             delete request
         ...     api
         ...     ${delete_fraud_ticket_path}
         ...     ${NONE}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search all fraud tickets
    [Arguments]    ${arg_dic}
    ${header}   create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${body_key_dict}    create dictionary   ticket_id=str   event_name=str  description=str     created_timestamp=str   last_updated_timestamp=str  is_deleted=bool     start_active_ticket_timestamp=str   end_active_ticket_timestamp=str
        ...     action_id=num   action=str  device_id=str   device_description=str  card_id=num     from_created_date=str   to_created_date=str     active_date_from=str    active_date_to=str
        ...     device_imei=str     ip_address=str  card_id_list=str    from_start_active_ticket_timestamp=str  to_start_active_ticket_timestamp=str    from_end_active_ticket_timestamp=str    to_end_active_ticket_timestamp=str
        ...     agent_id=num    voucher_id=num  service_id=num
    ${body_dict}    [Common] - build body request dict from list   ${body_key_dict}     ${arg_dic}
    ${data}     [Common] - create json string from dict     ${body_dict}    ${body_key_dict}
    ${response}             post request
        ...     api
        ...     ${get_list_fraud_ticket_path}
        ...     data=${data}
        ...     headers=${header}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search all fraud tickets by voucher id
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${data}              catenate           SEPARATOR=
         ...	{"voucher_id": ${arg_dic.voucher_id}}
     ${response}             post request
         ...     api
         ...     ${get_list_fraud_ticket_path}
         ...     ${data}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API register customer profile with device_id
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     device_id=${arg_dic.device_id}
         ...     device_description=${arg_dic.device_description}

     ${new_unique_ref}=     generate random string       20   prepare_[LETTERS]
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${data}              catenate           SEPARATOR=
         ...	{
         ...        "unique_reference":"${new_unique_ref}"
         ...	}
     ${response}             post request
         ...     api
         ...     ${register_customer_profile_path}
         ...     ${data}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API search fraud tickets by device_id
    [Arguments]    ${arg_dic}    ${device_id}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
      ${data}              catenate           SEPARATOR=
         ...	{
         ...        "device_id":"${device_id}"
         ...	}
     ${response}             post request
         ...     api
         ...     ${get_list_fraud_ticket_path}
         ...     ${data}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search all fraud tickets by agent_id
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${data}              catenate           SEPARATOR=
         ...	{
         ...        "agent_id":${arg_dic.agent_id}
         ...	}
     ${response}             post request
         ...     api
         ...     ${get_list_fraud_ticket_path}
         ...     ${data}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all unblock ticket reasons
    [Arguments]  ${arg_dic}
    ${header}   create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
        create session  api     ${api_gateway_host}     ${header}   disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_all_unblock_reasons_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${response.text}
    ${dic}=     Create Dictionary   response=${response}
    [Return]    ${dic}

API unblock ticket with reason
    [Arguments]  ${arg_dic}
    ${header}   create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
        create session  api     ${api_gateway_host}     ${header}   disable_warnings=1
    ${ticket_id}      convert to string     ${arg_dic.ticket_id}
    ${unblock_fraud_ticket_path}    replace string  ${delete_ticket_with_reason_path}   {ticket_id}     ${ticket_id}
    ${body_key_dict}    create dictionary   delete_reason=str
    ${body_dict}    [Common] - build body request dict from list    ${body_key_dict}    ${arg_dic}
    ${data}     [Common] - create json string from dict    ${body_dict}     ${body_key_dict}
    ${response}             put request
        ...     api
        ...     ${unblock_fraud_ticket_path}
        ...     data=${data}
        ...     headers=${header}
    log     ${response.text}
    ${dic}=     Create Dictionary   response=${response}
    [Return]    ${dic}

API search all fraud tickets by action_id
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${data}              catenate           SEPARATOR=
         ...	{
         ...        "event_name":"${arg_dic.action_id}"
         ...	}
     ${response}             post request
         ...     api
         ...     ${get_list_fraud_ticket_path}
         ...     ${data}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search all fraud tickets by active_date_from
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${data}              catenate           SEPARATOR=
         ...	{
         ...        "from_start_active_ticket_timestamp":"${arg_dic.active_date_from}"
         ...	}
     ${response}             post request
         ...     api
         ...     ${get_list_fraud_ticket_path}
         ...     ${data}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search all fraud tickets by active_date_to
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${data}              catenate           SEPARATOR=
         ...	{
         ...        "to_start_active_ticket_timestamp":"${arg_dic.active_date_to}"
         ...	}
     ${response}             post request
         ...     api
         ...     ${get_list_fraud_ticket_path}
         ...     ${data}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search all fraud tickets by active_date_from and active_date_to
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${data}              catenate           SEPARATOR=
         ...	{
         ...        "from_start_active_ticket_timestamp":"${arg_dic.active_date_from}",
         ...        "to_start_active_ticket_timestamp":"${arg_dic.active_date_to}"
         ...	}
     ${response}             post request
         ...     api
         ...     ${get_list_fraud_ticket_path}
         ...     ${data}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search all fraud tickets by agent_id and active_date_to
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${data}              catenate           SEPARATOR=
         ...	{
         ...        "agent_id":"${arg_dic.agent_id}",
         ...        "from_start_active_ticket_timestamp":"${arg_dic.active_date_to}"
         ...	}
     ${response}             post request
         ...     api
         ...     ${get_list_fraud_ticket_path}
         ...     ${data}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API search all fraud tickets by agent_id, action, active_date_from and active_date_to
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${data}              catenate           SEPARATOR=
         ...	{
         ...        "event_name":"${arg_dic.action_id}",
         ...        "agent_id":"${arg_dic.agent_id}",
         ...        "from_start_active_ticket_timestamp":"${arg_dic.active_date_from}",
         ...        "to_start_active_ticket_timestamp":"${arg_dic.active_date_to}"
         ...	}
     ${response}             post request
         ...     api
         ...     ${get_list_fraud_ticket_path}
         ...     ${data}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}
