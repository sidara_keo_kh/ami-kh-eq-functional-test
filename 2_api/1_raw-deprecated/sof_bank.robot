*** Settings ***
Resource        ../../1_common/keywords.robot


*** Keywords ***
API create bank
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_name}   [Common] - Create string param in dic    ${arg_dic}  name
    ${param_bin}   [Common] - Create string param in dic    ${arg_dic}  bin
    ${param_description}   [Common] - Create string param in dic    ${arg_dic}  description
    ${param_debit_url}   [Common] - Create string param in dic    ${arg_dic}  debit_url
    ${param_credit_url}   [Common] - Create string param in dic    ${arg_dic}  credit_url
    ${param_bank_account_number}   [Common] - Create string param in dic    ${arg_dic}  bank_account_number
    ${param_bank_account_name}   [Common] - Create string param in dic    ${arg_dic}  bank_account_name
    ${param_currency}   [Common] - Create string param in dic    ${arg_dic}  currency
    ${param_cancel_url}   [Common] - Create string param in dic    ${arg_dic}  cancel_url
    ${param_check_status_url}   [Common] - Create string param in dic    ${arg_dic}  check_status_url
    ${param_pre_sof_url}   [Common] - Create string param in dic    ${arg_dic}  pre_sof_url
    ${param_pre_sof_read_timeout}   [Common] - Create int param in dic    ${arg_dic}  pre_sof_read_timeout
    ${param_read_timeout}   [Common] - Create int param in dic    ${arg_dic}  read_timeout

    ${data}              catenate       SEPARATOR=
        ...     {
        ...         ${param_name}
        ...         ${param_bin}
        ...         ${param_description}
        ...         ${param_debit_url}
        ...         ${param_credit_url}
        ...         ${param_bank_account_number}
        ...         ${param_bank_account_name}
        ...         ${param_currency}
        ...         ${param_cancel_url}
        ...         ${param_check_status_url}
        ...         ${param_pre_sof_url}
        ...         ${param_pre_sof_read_timeout}
        ...         ${param_read_timeout}
        ...     }
    ${data}     replace string      ${data}     ,}     }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_bank_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
        log        ${data}
        log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

AIP update bank
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${bank_id}      Convert To String       ${arg_dic.bank_id}
    ${update_bank_path}      replace string    ${update_bank_path}    {bankId}   ${bank_id}

    ${param_name}   [Common] - Create string param in dic    ${arg_dic}  name
    ${param_bin}   [Common] - Create string param in dic    ${arg_dic}  bin
    ${param_description}   [Common] - Create string param in dic    ${arg_dic}  description
    ${param_debit_url}   [Common] - Create string param in dic    ${arg_dic}  debit_url
    ${param_credit_url}   [Common] - Create string param in dic    ${arg_dic}  credit_url
    ${param_bank_account_number}   [Common] - Create string param in dic    ${arg_dic}  bank_account_number
    ${param_bank_account_name}   [Common] - Create string param in dic    ${arg_dic}  bank_account_name
    ${param_currency}   [Common] - Create string param in dic    ${arg_dic}  currency
    ${param_cancel_url}   [Common] - Create string param in dic    ${arg_dic}  cancel_url
    ${param_check_status_url}   [Common] - Create string param in dic    ${arg_dic}  check_status_url
    ${param_pre_sof_url}   [Common] - Create string param in dic    ${arg_dic}  pre_sof_url
    ${param_pre_sof_read_timeout}   [Common] - Create int param in dic    ${arg_dic}  pre_sof_read_timeout
    ${param_read_timeout}   [Common] - Create int param in dic    ${arg_dic}  read_timeout
    ${param_is_active}   [Common] - Create boolean param in dic    ${arg_dic}  is_active

    ${data}              catenate       SEPARATOR=
        ...    {
        ...         ${param_name}
        ...         ${param_bin}
        ...         ${param_description}
        ...         ${param_debit_url}
        ...         ${param_credit_url}
        ...         ${param_bank_account_number}
        ...         ${param_bank_account_name}
        ...         ${param_currency}
        ...         ${param_cancel_url}
        ...         ${param_check_status_url}
        ...         ${param_pre_sof_url}
        ...         ${param_pre_sof_read_timeout}
        ...         ${param_read_timeout}
        ...         ${param_is_active}
        ...    }
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${update_bank_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete bank
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${bank_id}    Convert To String    ${arg_dic.bank_id}
    ${delete_bank_path}      replace string      ${delete_bank_path}     {bankId}    ${bank_id}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             delete request
        ...     api
        ...     ${delete_bank_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log        ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}