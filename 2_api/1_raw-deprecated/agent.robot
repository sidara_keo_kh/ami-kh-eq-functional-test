*** Settings ***
Resource        ../../1_common/keywords.robot

*** Keywords ***
API create agent type
    [Arguments]
    ...     ${access_token}
    ...     ${name}
    ...     ${description}
    ...     ${is_sale}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${correlation_id}       generate random string  20  [UPPER]
    ${param_name}=  [Common] - Create string param     name        ${name}
    ${param_description}=  [Common] - Create string param     description        ${description}
    ${param_sale}=  [Common] - Create string param     is_sale        ${is_sale}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_name}
        ...		${param_description}
        ...     ${param_sale}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_agent_type_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update agent type
    [Arguments]     ${arg_dic}
    ${param_name}=  [Common] - Create string param     name        ${arg_dic.name}
    ${param_description}=  [Common] - Create string param     description        ${arg_dic.description}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_name}
        ...		${param_description}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${agentTypeId}=    convert to string   ${arg_dic.agentTypeId}
    ${update_agent_type_path}=    replace string      ${update_agent_type_path}      {agentTypeId}        ${agentTypeId}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${update_agent_type_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete agent type
    [Arguments]
    ... 	${access_token}
    ... 	${agentTypeId}
	... 	${header_client_id}
	... 	${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${access_token}
    ${agentTypeId}=    convert to string   ${agentTypeId}
    ${delete_agent_type_path}=    replace string      ${delete_agent_type_path}      {agentTypeId}        ${agentTypeId}   1
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${delete_agent_type_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all agent type
    [Arguments]
    ...     ${client_id}
    ...     ${client_secret}
    ...     ${access_token}
    ...     ${id}
    ${header}               create dictionary
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${data}              catenate
        ...	{
        ...		"id":"${id}"
        ...	}
    create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_card_history_on_report_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create agent classification
    [Arguments]     ${arg_dic}
    ${param_name}=  [Common] - Create string param     name        ${arg_dic.name}
    ${param_description}=  [Common] - Create string param     description        ${arg_dic.description}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_name}
        ...		${param_description}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_agent_classification_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update agent classification
    [Arguments]     ${arg_dic}
    ${correlation_id}       generate random string  20  [UPPER]
    ${param_name}=  [Common] - Create string param     name        ${arg_dic.name}
    ${param_description}=  [Common] - Create string param     description        ${arg_dic.description}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_name}
        ...		${param_description}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${agentClassificationId}=    convert to string   ${arg_dic.agentClassificationId}
    ${update_agent_classification_path}=    replace string      ${update_agent_classification_path}      {agentClassificationId}        ${agentClassificationId}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${update_agent_classification_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete agent classification
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${agentClassificationId}=    convert to string   ${arg_dic.agentClassificationId}
    ${delete_agent_classification_path}=    replace string      ${delete_agent_classification_path}      {agentClassificationId}        ${agentClassificationId}   1
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${delete_agent_classification_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API update agent accreditation
    [Arguments]     ${arg_dic}
    ${correlation_id}       generate random string  20  [UPPER]
    ${param_status_id}=  [Common] - Create int param     status_id        ${arg_dic.status_id}
    ${param_remark}=  [Common] - Create string param     remark        ${arg_dic.remark}
    ${param_risk_level}=  [Common] - Create string param     risk_level        ${arg_dic.risk_level}
    ${param_verify_by}=  [Common] - Create string param     verify_by        ${arg_dic.verify_by}
    ${param_verify_date}=  [Common] - Create string param     verify_date        ${arg_dic.verify_date}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_status_id}
        ...		${param_remark}
        ...		${param_risk_level}
        ...		${param_verify_by}
        ...		${param_verify_date}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${agent_id}=    convert to string   ${arg_dic.agent_id}
    ${update_agent_accreditation_path}=    replace string      ${update_agent_accreditation_path}      {agent_id}        ${agent_id}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${update_agent_accreditation_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API admin updates agent profiles
    [Arguments]     ${arg_dic}
    ${correlation_id}       generate random string  20  [UPPER]
    ${param_is_testing_account}		[Common] - Create boolean param in dic    ${arg_dic}		is_testing_account
    ${param_is_system_account}		[Common] - Create boolean param in dic    ${arg_dic}		is_system_account
    ${param_acquisition_source}		[Common] - Create string param in dic    ${arg_dic}		acquisition_source
    ${param_referrer_user_type_id}		[Common] - Create int param in dic    ${arg_dic}		referrer_user_type_id
    ${param_referrer_user_type_name}		[Common] - Create string param in dic    ${arg_dic}		referrer_user_type_name
    ${param_referrer_user_id}		[Common] - Create int param in dic    ${arg_dic}		referrer_user_id
    ${param_agent_type_id}		[Common] - Create int param in dic    ${arg_dic}		agent_type_id
    ${param_unique_reference}		[Common] - Create string param in dic    ${arg_dic}		unique_reference
    ${param_company_id}             [Common] - Create int param in dic    ${arg_dic}		company_id
    ${param_mm_card_type_id}		[Common] - Create int param in dic    ${arg_dic}		mm_card_type_id
    ${param_mm_card_level_id}		[Common] - Create int param in dic    ${arg_dic}		mm_card_level_id
    ${param_mm_factory_card_number}		[Common] - Create string param in dic    ${arg_dic}		mm_factory_card_number
    ${param_model_type}		[Common] - Create string param in dic    ${arg_dic}		model_type
    ${param_is_require_otp}		[Common] - Create boolean param in dic    ${arg_dic}		is_require_otp
    ${param_agent_classification_id}		[Common] - Create int param in dic    ${arg_dic}		agent_classification_id
    ${param_tin_number}		[Common] - Create string param in dic    ${arg_dic}		tin_number
    ${param_title}		[Common] - Create string param in dic    ${arg_dic}		title
    ${param_first_name}		[Common] - Create string param in dic    ${arg_dic}		first_name
    ${param_middle_name}		[Common] - Create string param in dic    ${arg_dic}		middle_name
    ${param_last_name}		[Common] - Create string param in dic    ${arg_dic}		last_name
    ${param_suffix}		[Common] - Create string param in dic    ${arg_dic}		suffix
    ${param_date_of_birth}		[Common] - Create string param in dic    ${arg_dic}		date_of_birth
    ${param_place_of_birth}		[Common] - Create string param in dic    ${arg_dic}		place_of_birth
    ${param_gender}		[Common] - Create string param in dic    ${arg_dic}		gender
    ${param_ethnicity}		[Common] - Create string param in dic    ${arg_dic}		ethnicity
    ${param_nationality}		[Common] - Create string param in dic    ${arg_dic}		nationality
    ${param_occupation}		[Common] - Create string param in dic    ${arg_dic}		occupation
    ${param_occupation_title}		[Common] - Create string param in dic    ${arg_dic}		occupation_title
    ${param_township_code}		[Common] - Create string param in dic    ${arg_dic}		township_code
    ${param_township_name}		[Common] - Create string param in dic    ${arg_dic}		township_name
    ${param_national_id_number}		[Common] - Create string param in dic    ${arg_dic}		national_id_number
    ${param_mother_name}		[Common] - Create string param in dic    ${arg_dic}		mother_name
    ${param_email}		[Common] - Create string param in dic    ${arg_dic}		email
    ${param_primary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}		primary_mobile_number
    ${param_secondary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}		secondary_mobile_number
    ${param_tertiary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}		tertiary_mobile_number
    ${param_current_address_citizen_association}		[Common] - Create string param in dic    ${arg_dic}		citizen_association
    ${param_current_address_neighbourhood_association}		[Common] - Create string param in dic    ${arg_dic}		neighbourhood_association
    ${param_current_address_address}		[Common] - Create string param in dic    ${arg_dic}		address
    ${param_current_address_commune}		[Common] - Create string param in dic    ${arg_dic}		commune
    ${param_current_address_district}		[Common] - Create string param in dic    ${arg_dic}		district
    ${param_current_address_city}		[Common] - Create string param in dic    ${arg_dic}		city
    ${param_current_address_province}		[Common] - Create string param in dic    ${arg_dic}		province
    ${param_current_address_postal_code}		[Common] - Create string param in dic    ${arg_dic}		postal_code
    ${param_current_address_country}		[Common] - Create string param in dic    ${arg_dic}		country
    ${param_current_address_landmark}		[Common] - Create string param in dic    ${arg_dic}		landmark
    ${param_current_address_longitude}		[Common] - Create string param in dic    ${arg_dic}		longitude
    ${param_current_address_latitude}		[Common] - Create string param in dic    ${arg_dic}		latitude
    ${param_permanent_address_citizen_association}		[Common] - Create string param in dic    ${arg_dic}		citizen_association
    ${param_permanent_address_neighbourhood_association}		[Common] - Create string param in dic    ${arg_dic}		neighbourhood_association
    ${param_permanent_address_address}		[Common] - Create string param in dic    ${arg_dic}		address
    ${param_permanent_address_commune}		[Common] - Create string param in dic    ${arg_dic}		commune
    ${param_permanent_address_district}		[Common] - Create string param in dic    ${arg_dic}		district
    ${param_permanent_address_city}		[Common] - Create string param in dic    ${arg_dic}		city
    ${param_permanent_address_province}		[Common] - Create string param in dic    ${arg_dic}		province
    ${param_permanent_address_postal_code}		[Common] - Create string param in dic    ${arg_dic}		postal_code
    ${param_permanent_address_country}		[Common] - Create string param in dic    ${arg_dic}		country
    ${param_permanent_address_landmark}		[Common] - Create string param in dic    ${arg_dic}		landmark
    ${param_permanent_address_longitude}		[Common] - Create string param in dic    ${arg_dic}		longitude
    ${param_permanent_address_latitude}		[Common] - Create string param in dic    ${arg_dic}		latitude
    ${param_bank_name}		[Common] - Create string param in dic    ${arg_dic}		name
    ${param_bank_account_status}		[Common] - Create int param in dic    ${arg_dic}		account_status
    ${param_bank_account_name}		[Common] - Create string param in dic    ${arg_dic}		account_name
    ${param_bank_account_number}		[Common] - Create string param in dic    ${arg_dic}		account_number
    ${param_bank_branch_area}		[Common] - Create string param in dic    ${arg_dic}		branch_area
    ${param_bank_branch_city}		[Common] - Create string param in dic    ${arg_dic}		branch_city
    ${param_bank_register_date}		[Common] - Create string param in dic    ${arg_dic}		register_date
    ${param_bank_register_source}		[Common] - Create string param in dic    ${arg_dic}		register_source
    ${param_bank_is_verified}		[Common] - Create boolean param in dic    ${arg_dic}		is_verified
    ${param_bank_end_date}		[Common] - Create string param in dic    ${arg_dic}		end_date
    ${param_contract_type}		[Common] - Create string param in dic    ${arg_dic}		type
    ${param_contract_number}		[Common] - Create string param in dic    ${arg_dic}		number
    ${param_contract_extension_type}		[Common] - Create string param in dic    ${arg_dic}		extension_type
    ${param_contract_sign_date}		[Common] - Create string param in dic    ${arg_dic}		sign_date
    ${param_contract_issue_date}		[Common] - Create string param in dic    ${arg_dic}		issue_date
    ${param_contract_expired_date}		[Common] - Create string param in dic    ${arg_dic}		expired_date
    ${param_contract_notification_alert}		[Common] - Create string param in dic    ${arg_dic}		notification_alert
    ${param_contract_day_of_period_reconciliation}		[Common] - Create string param in dic    ${arg_dic}		day_of_period_reconciliation
    ${param_contract_release}		[Common] - Create string param in dic    ${arg_dic}		release
    ${param_contract_file_url}		[Common] - Create string param in dic    ${arg_dic}		file_url
    ${param_contract_assessment_information_url}		[Common] - Create string param in dic    ${arg_dic}		assessment_information_url
    ${param_primary_identity_type}		[Common] - Create string param of object in dic    ${arg_dic}     primary_identity		type
    ${param_primary_identity_status}		[Common] - Create int param of object in dic    ${arg_dic}		primary_identity    status
    ${param_primary_identity_identity_id}		[Common] - Create string param of object in dic    ${arg_dic}     primary_identity		identity_id
    ${param_primary_identity_place_of_issue}		[Common] - Create string param of object in dic    ${arg_dic}     primary_identity		place_of_issue
    ${param_primary_identity_issue_date}		[Common] - Create string param of object in dic    ${arg_dic}     primary_identity		issue_date
    ${param_primary_identity_expired_date}		[Common] - Create string param of object in dic    ${arg_dic}     primary_identity		expired_date
    ${param_primary_identity_front_identity_url}		[Common] - Create string param of object in dic    ${arg_dic}     primary_identity		front_identity_url
    ${param_primary_identity_back_identity_url}		[Common] - Create string param of object in dic    ${arg_dic}     primary_identity		back_identity_url
    ${param_secondary_identity_type}		[Common] - Create string param of object in dic    ${arg_dic}     secondary_identity		type
    ${param_secondary_identity_status}		[Common] - Create int param of object in dic    ${arg_dic}		secondary_identity      status
    ${param_secondary_identity_identity_id}		[Common] - Create string param of object in dic    ${arg_dic}     secondary_identity		identity_id
    ${param_secondary_identity_place_of_issue}		[Common] - Create string param of object in dic    ${arg_dic}     secondary_identity		place_of_issue
    ${param_secondary_identity_issue_date}		[Common] - Create string param of object in dic    ${arg_dic}     secondary_identity		issue_date
    ${param_secondary_identity_expired_date}		[Common] - Create string param of object in dic    ${arg_dic}     secondary_identity		expired_date
    ${param_secondary_identity_front_identity_url}		[Common] - Create string param of object in dic    ${arg_dic}     secondary_identity		front_identity_url
    ${param_secondary_identity_back_identity_url}		[Common] - Create string param of object in dic    ${arg_dic}     secondary_identity		back_identity_url
    ${param_status_id}		[Common] - Create int param in dic    ${arg_dic}		status_id
    ${param_remark}		[Common] - Create string param in dic    ${arg_dic}		remark
    ${param_verify_by}		[Common] - Create string param in dic    ${arg_dic}		verify_by
    ${param_verify_date}		[Common] - Create string param in dic    ${arg_dic}		verify_date
    ${param_risk_level}		[Common] - Create string param in dic    ${arg_dic}		risk_level
    ${param_additional_acquiring_sales_executive_id}		[Common] - Create int param in dic    ${arg_dic}		acquiring_sales_executive_id
    ${param_additional_acquiring_sales_executive_name}		[Common] - Create string param in dic    ${arg_dic}		acquiring_sales_executive_name
    ${param_additional_relationship_manager_id}		[Common] - Create int param in dic    ${arg_dic}		relationship_manager_id
    ${param_additional_relationship_manager_name}		[Common] - Create string param in dic    ${arg_dic}		relationship_manager_name
    ${param_additional_sale_region}		[Common] - Create string param in dic    ${arg_dic}		sale_region
    ${param_additional_commercial_account_manager}		[Common] - Create string param in dic    ${arg_dic}		commercial_account_manager
    ${param_additional_profile_picture_url}		[Common] - Create string param in dic    ${arg_dic}		profile_picture_url
    ${param_additional_national_id_photo_url}		[Common] - Create string param in dic    ${arg_dic}		national_id_photo_url
    ${param_additional_tax_id_card_photo_url}		[Common] - Create string param in dic    ${arg_dic}		tax_id_card_photo_url
    ${param_additional_field_1_name}		[Common] - Create string param in dic    ${arg_dic}		field_1_name
    ${param_additional_field_1_value}		[Common] - Create string param in dic    ${arg_dic}		field_1_value
    ${param_additional_field_2_name}		[Common] - Create string param in dic    ${arg_dic}		field_2_name
    ${param_additional_field_2_value}		[Common] - Create string param in dic    ${arg_dic}		field_2_value
    ${param_additional_field_3_name}		[Common] - Create string param in dic    ${arg_dic}		field_3_name
    ${param_additional_field_3_value}		[Common] - Create string param in dic    ${arg_dic}		field_3_value
    ${param_additional_field_4_name}		[Common] - Create string param in dic    ${arg_dic}		field_4_name
    ${param_additional_field_4_value}		[Common] - Create string param in dic    ${arg_dic}		field_4_value
    ${param_additional_field_5_name}		[Common] - Create string param in dic    ${arg_dic}		field_5_name
    ${param_additional_field_5_value}		[Common] - Create string param in dic    ${arg_dic}		field_5_value
    ${param_additional_supporting_file_1_url}		[Common] - Create string param in dic    ${arg_dic}		supporting_file_1_url
    ${param_additional_supporting_file_2_url}		[Common] - Create string param in dic    ${arg_dic}		supporting_file_2_url
    ${param_additional_supporting_file_3_url}		[Common] - Create string param in dic    ${arg_dic}		supporting_file_3_url
    ${param_additional_supporting_file_4_url}		[Common] - Create string param in dic    ${arg_dic}		supporting_file_4_url
    ${param_additional_supporting_file_5_url}		[Common] - Create string param in dic    ${arg_dic}		supporting_file_5_url
    ${param_tin_number_local}		[Common] - Create string param in dic    ${arg_dic}		tin_number_local
    ${param_title_local}            [Common] - Create string param in dic    ${arg_dic}		title_local
    ${param_first_name_local}       [Common] - Create string param in dic    ${arg_dic}		first_name_local
    ${param_middle_name_local}      [Common] - Create string param in dic    ${arg_dic}		middle_name_local
    ${param_last_name_local}        [Common] - Create string param in dic    ${arg_dic}		last_name_local
    ${param_suffix_local}           [Common] - Create string param in dic    ${arg_dic}		suffix_local
    ${param_place_of_birth_local}   [Common] - Create string param in dic    ${arg_dic}		place_of_birth_local
    ${param_gender_local}           [Common] - Create string param in dic    ${arg_dic}		gender_local
    ${param_occupation_local}       [Common] - Create string param in dic    ${arg_dic}		occupation_local
    ${param_occupation_title_local}     [Common] - Create string param in dic    ${arg_dic}		occupation_title_local
    ${param_township_name_local}        [Common] - Create string param in dic    ${arg_dic}		township_name_local
    ${param_national_id_number_local}   [Common] - Create string param in dic    ${arg_dic}		national_id_number_local
    ${param_mother_name_local}          [Common] - Create string param in dic    ${arg_dic}		mother_name_local
    ${param_current_address_address_local}      [Common] - Create string param of object in dic    ${arg_dic}   current_address     address_local
    ${param_current_address_commune_local}      [Common] - Create string param of object in dic    ${arg_dic}     current_address		commune_local
    ${param_current_address_district_local}     [Common] - Create string param of object in dic    ${arg_dic}     current_address		district_local
    ${param_current_address_city_local}         [Common] - Create string param of object in dic    ${arg_dic}     current_address		city_local
    ${param_current_address_province_local}     [Common] - Create string param of object in dic    ${arg_dic}     current_address		province_local
    ${param_current_address_postal_code_local}      [Common] - Create string param of object in dic    ${arg_dic}     current_address		postal_code_local
    ${param_current_address_country_local}      [Common] - Create string param of object in dic    ${arg_dic}     current_address		country_local
    ${param_permanent_address_address_local}        [Common] - Create string param of object in dic    ${arg_dic}     permanent_address		address_local
    ${param_permanent_address_commune_local}        [Common] - Create string param of object in dic    ${arg_dic}     permanent_address		commune_local
    ${param_permanent_address_district_local}       [Common] - Create string param of object in dic    ${arg_dic}     permanent_address		district_local
    ${param_permanent_address_city_local}       [Common] - Create string param of object in dic    ${arg_dic}     permanent_address		city_local
    ${param_permanent_address_province_local}       [Common] - Create string param of object in dic    ${arg_dic}     permanent_address		province_local
    ${param_permanent_address_postal_code_local}        [Common] - Create string param of object in dic    ${arg_dic}     permanent_address		postal_code_local
    ${param_permanent_address_country_local}        [Common] - Create string param of object in dic    ${arg_dic}     permanent_address		country_local
    ${param_bank_name_local}        [Common] - Create string param in dic    ${arg_dic}		name_local
    ${param_bank_branch_area_local}     [Common] - Create string param in dic    ${arg_dic}		branch_area_local
    ${param_bank_branch_city_local}     [Common] - Create string param in dic    ${arg_dic}		branch_city_local
    ${param_primary_identity_identity_id_local}     [Common] - Create string param in dic    ${arg_dic}		identity_id_local
    ${param_secondary_identity_identity_id_local}       [Common] - Create string param in dic    ${arg_dic}		identity_id_local
    ${param_acquiring_sale_executive_name_local}        [Common] - Create string param in dic    ${arg_dic}		acquiring_sale_executive_name_local
    ${param_relationship_manager_name_local}        [Common] - Create string param in dic    ${arg_dic}		relationship_manager_name_local
    ${param_sale_region_local}      [Common] - Create string param in dic    ${arg_dic}		sale_region_local
    ${param_commercial_account_manager_local}       [Common] - Create string param in dic    ${arg_dic}		commercial_account_manager_local
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...          {
        ...                 ${param_is_testing_account}
        ...                 ${param_is_system_account}
        ...                 ${param_acquisition_source}
        ...                 "referrer_user_type": {
        ...                     ${param_referrer_user_type_id}
        ...                     ${param_referrer_user_type_name}
        ...                 },
        ...                 ${param_referrer_user_id}
        ...                 ${param_agent_type_id}
        ...                 ${param_unique_reference}
        ...                 ${param_mm_card_type_id}
        ...                 ${param_mm_card_level_id}
        ...                 ${param_mm_factory_card_number}
        ...                 ${param_model_type}
        ...                 ${param_is_require_otp}
        ...                 ${param_agent_classification_id}
        ...                 ${param_tin_number}
        ...                 ${param_title}
        ...                 ${param_first_name}
        ...                 ${param_middle_name}
        ...                 ${param_last_name}
        ...                 ${param_suffix}
        ...                 ${param_date_of_birth}
        ...                 ${param_place_of_birth}
        ...                 ${param_gender}
        ...                 ${param_ethnicity}
        ...                 ${param_nationality}
        ...                 ${param_occupation}
        ...                 ${param_occupation_title}
        ...                 ${param_township_code}
        ...                 ${param_township_name}
        ...                 ${param_national_id_number}
        ...                 ${param_mother_name}
        ...                 ${param_email}
        ...                 ${param_primary_mobile_number}
        ...                 ${param_secondary_mobile_number}
        ...                 ${param_tertiary_mobile_number}
        ...                 ${param_tin_number_local}
        ...                 ${param_title_local}
        ...                 ${param_first_name_local}
        ...                 ${param_middle_name_local}
        ...                 ${param_last_name_local}
        ...                 ${param_suffix_local}
        ...                 ${param_place_of_birth_local}
        ...                 ${param_gender_local}
        ...                 ${param_occupation_local}
        ...                 ${param_occupation_title_local}
        ...                 ${param_township_name_local}
        ...                 ${param_national_id_number_local}
        ...                 ${param_mother_name_local}
        ...                 "address": {
        ...                     "current_address": {
        ...      					${param_current_address_citizen_association}
        ...      					${param_current_address_neighbourhood_association}
        ...      					${param_current_address_address}
        ...      					${param_current_address_commune}
        ...      					${param_current_address_district}
        ...      					${param_current_address_city}
        ...      					${param_current_address_province}
        ...      					${param_current_address_postal_code}
        ...      					${param_current_address_country}
        ...      					${param_current_address_landmark}
        ...      					${param_current_address_longitude}
        ...      					${param_current_address_latitude}
        ...                         ${param_current_address_address_local}
        ...                         ${param_current_address_commune_local}
        ...                         ${param_current_address_district_local}
        ...                         ${param_current_address_city_local}
        ...                         ${param_current_address_province_local}
        ...                         ${param_current_address_postal_code_local}
        ...                         ${param_current_address_country_local}
        ...                     },
        ...                     "permanent_address": {
        ...      					${param_permanent_address_citizen_association}
        ...      					${param_permanent_address_neighbourhood_association}
        ...      					${param_permanent_address_address}
        ...      					${param_permanent_address_commune}
        ...      					${param_permanent_address_district}
        ...      					${param_permanent_address_city}
        ...      					${param_permanent_address_province}
        ...      					${param_permanent_address_postal_code}
        ...      					${param_permanent_address_country}
        ...      					${param_permanent_address_landmark}
        ...      					${param_permanent_address_longitude}
        ...      					${param_permanent_address_latitude}
        ...                         ${param_permanent_address_address_local}
        ...                         ${param_permanent_address_commune_local}
        ...                         ${param_permanent_address_district_local}
        ...                         ${param_permanent_address_city_local}
        ...                         ${param_permanent_address_province_local}
        ...                         ${param_permanent_address_postal_code_local}
        ...                         ${param_permanent_address_country_local}
        ...                     }
        ...                 },
        ...                 "bank":{
        ...      				${param_bank_name}
        ...      				${param_bank_account_status}
        ...      				${param_bank_account_name}
        ...      				${param_bank_account_number}
        ...      				${param_bank_branch_area}
        ...      				${param_bank_branch_city}
        ...      				${param_bank_register_date}
        ...      				${param_bank_register_source}
        ...      				${param_bank_is_verified}
        ...      				${param_bank_end_date}
        ...                     ${param_bank_name_local}
        ...                     ${param_bank_branch_area_local}
        ...                     ${param_bank_branch_city_local}
        ...                 },
        ...                 "contract": {
        ...      				${param_contract_type}
        ...      				${param_contract_number}
        ...      				${param_contract_extension_type}
        ...      				${param_contract_sign_date}
        ...      				${param_contract_issue_date}
        ...      				${param_contract_expired_date}
        ...      				${param_contract_notification_alert}
        ...      				${param_contract_day_of_period_reconciliation}
        ...      				${param_contract_release}
        ...      				${param_contract_file_url}
        ...      				${param_contract_assessment_information_url}
        ...                  },
        ...                 "accreditation": {
        ...                     "primary_identity": {
        ...      					${param_primary_identity_type}
        ...      					${param_primary_identity_status}
        ...      					${param_primary_identity_identity_id}
        ...      					${param_primary_identity_place_of_issue}
        ...      					${param_primary_identity_issue_date}
        ...      					${param_primary_identity_expired_date}
        ...      					${param_primary_identity_front_identity_url}
        ...      					${param_primary_identity_back_identity_url}
        ...                         ${param_primary_identity_identity_id_local}
        ...                     },
        ...                     "secondary_identity": {
        ...      					${param_secondary_identity_type}
        ...      					${param_secondary_identity_status}
        ...      					${param_secondary_identity_identity_id}
        ...      					${param_secondary_identity_place_of_issue}
        ...      					${param_secondary_identity_issue_date}
        ...      					${param_secondary_identity_expired_date}
        ...      					${param_secondary_identity_front_identity_url}
        ...      					${param_secondary_identity_back_identity_url}
        ...                         ${param_secondary_identity_identity_id_local}
        ...                     },
        ...      				${param_status_id}
        ...      				${param_remark}
        ...      				${param_verify_by}
        ...      				${param_verify_date}
        ...      				${param_risk_level}
        ...                 },
        ...                 "additional": {
        ...      				${param_additional_acquiring_sales_executive_id}
        ...      				${param_additional_acquiring_sales_executive_name}
        ...      				${param_additional_relationship_manager_id}
        ...      				${param_additional_relationship_manager_name}
        ...      				${param_additional_sale_region}
        ...      				${param_additional_commercial_account_manager}
        ...      				${param_additional_profile_picture_url}
        ...      				${param_additional_national_id_photo_url}
        ...      				${param_additional_tax_id_card_photo_url}
        ...      				${param_additional_field_1_name}
        ...      				${param_additional_field_1_value}
        ...      				${param_additional_field_2_name}
        ...      				${param_additional_field_2_value}
        ...      				${param_additional_field_3_name}
        ...      				${param_additional_field_3_value}
        ...      				${param_additional_field_4_name}
        ...      				${param_additional_field_4_value}
        ...      				${param_additional_field_5_name}
        ...      				${param_additional_field_5_value}
        ...      				${param_additional_supporting_file_1_url}
        ...      				${param_additional_supporting_file_2_url}
        ...      				${param_additional_supporting_file_3_url}
        ...      				${param_additional_supporting_file_4_url}
        ...      				${param_additional_supporting_file_5_url}
        ...                     ${param_acquiring_sale_executive_name_local}
        ...                     ${param_relationship_manager_name_local}
        ...                     ${param_sale_region_local}
        ...                     ${param_commercial_account_manager_local}
        ...                 }
        ...          }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${agent_id}=    convert to string   ${arg_dic.agent_id}
    ${admin_update_agent_profiles_path}=    replace string      ${admin_update_agent_profiles_path}      {agent_id}        ${agent_id}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${admin_update_agent_profiles_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API admin updates agent profiles with PATCH method
    [Arguments]     ${arg_dic}
    ${correlation_id}       generate random string  20  [UPPER]
    ${param_is_testing_account}		[Common] - Create boolean param		is_testing_account		${arg_dic.is_testing_account}
    ${param_is_system_account}		[Common] - Create boolean param		is_system_account		${arg_dic.is_system_account}
    ${param_acquisition_source}		[Common] - Create string param		acquisition_source		${arg_dic.acquisition_source}
    ${param_referrer_user_type_id}		[Common] - Create int param		id		${arg_dic.referrer_user_type_id}
    ${param_referrer_user_type_name}		[Common] - Create string param		name		${arg_dic.referrer_user_type_name}
    ${param_referrer_user_id}		[Common] - Create int param		referrer_user_id		${arg_dic.referrer_user_id}
    ${param_agent_type_id}		[Common] - Create int param		agent_type_id		${arg_dic.agent_type_id}
    ${param_unique_reference}		[Common] - Create string param		unique_reference		${arg_dic.unique_reference}
    ${param_company_id}             [Common] - Create int param		company_id		${arg_dic.company_id}
    ${param_mm_card_type_id}		[Common] - Create int param		mm_card_type_id		${arg_dic.mm_card_type_id}
    ${param_mm_card_level_id}		[Common] - Create int param		mm_card_level_id		${arg_dic.mm_card_level_id}
    ${param_mm_factory_card_number}		[Common] - Create string param		mm_factory_card_number		${arg_dic.mm_factory_card_number}
    ${param_model_type}		[Common] - Create string param		model_type		${arg_dic.model_type}
    ${param_is_require_otp}		[Common] - Create boolean param		is_require_otp		${arg_dic.is_require_otp}
    ${param_agent_classification_id}		[Common] - Create int param		agent_classification_id		${arg_dic.agent_classification_id}
    ${param_tin_number}		[Common] - Create string param		tin_number		${arg_dic.tin_number}
    ${param_title}		[Common] - Create string param		title		${arg_dic.title}
    ${param_first_name}		[Common] - Create string param		first_name		${arg_dic.first_name}
    ${param_middle_name}		[Common] - Create string param		middle_name		${arg_dic.middle_name}
    ${param_last_name}		[Common] - Create string param		last_name		${arg_dic.last_name}
    ${param_suffix}		[Common] - Create string param		suffix		${arg_dic.suffix}
    ${param_date_of_birth}		[Common] - Create string param		date_of_birth		${arg_dic.date_of_birth}
    ${param_place_of_birth}		[Common] - Create string param		place_of_birth		${arg_dic.place_of_birth}
    ${param_gender}		[Common] - Create string param		gender		${arg_dic.gender}
    ${param_ethnicity}		[Common] - Create string param		ethnicity		${arg_dic.ethnicity}
    ${param_nationality}		[Common] - Create string param		nationality		${arg_dic.nationality}
    ${param_occupation}		[Common] - Create string param		occupation		${arg_dic.occupation}
    ${param_occupation_title}		[Common] - Create string param		occupation_title		${arg_dic.occupation_title}
    ${param_township_code}		[Common] - Create string param		township_code		${arg_dic.township_code}
    ${param_township_name}		[Common] - Create string param		township_name		${arg_dic.township_name}
    ${param_national_id_number}		[Common] - Create string param		national_id_number		${arg_dic.national_id_number}
    ${param_mother_name}		[Common] - Create string param		mother_name		${arg_dic.mother_name}
    ${param_email}		[Common] - Create string param		email		${arg_dic.email}
    ${param_primary_mobile_number}		[Common] - Create string param		primary_mobile_number		${arg_dic.primary_mobile_number}
    ${param_secondary_mobile_number}		[Common] - Create string param		secondary_mobile_number		${arg_dic.secondary_mobile_number}
    ${param_tertiary_mobile_number}		[Common] - Create string param		tertiary_mobile_number		${arg_dic.tertiary_mobile_number}
    ${param_current_address_citizen_association}		[Common] - Create string param		citizen_association		${arg_dic.current_address_citizen_association}
    ${param_current_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${arg_dic.current_address_neighbourhood_association}
    ${param_current_address_address}		[Common] - Create string param		address		${arg_dic.current_address_address}
    ${param_current_address_commune}		[Common] - Create string param		commune		${arg_dic.current_address_commune}
    ${param_current_address_district}		[Common] - Create string param		district		${arg_dic.current_address_district}
    ${param_current_address_city}		[Common] - Create string param		city		${arg_dic.current_address_city}
    ${param_current_address_province}		[Common] - Create string param		province		${arg_dic.current_address_province}
    ${param_current_address_postal_code}		[Common] - Create string param		postal_code		${arg_dic.current_address_postal_code}
    ${param_current_address_country}		[Common] - Create string param		country		${arg_dic.current_address_country}
    ${param_current_address_landmark}		[Common] - Create string param		landmark		${arg_dic.current_address_landmark}
    ${param_current_address_longitude}		[Common] - Create string param		longitude		${arg_dic.current_address_longitude}
    ${param_current_address_latitude}		[Common] - Create string param		latitude		${arg_dic.current_address_latitude}
    ${param_permanent_address_citizen_association}		[Common] - Create string param		citizen_association		${arg_dic.permanent_address_citizen_association}
    ${param_permanent_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${arg_dic.permanent_address_neighbourhood_association}
    ${param_permanent_address_address}		[Common] - Create string param		address		${arg_dic.permanent_address_address}
    ${param_permanent_address_commune}		[Common] - Create string param		commune		${arg_dic.permanent_address_commune}
    ${param_permanent_address_district}		[Common] - Create string param		district		${arg_dic.permanent_address_district}
    ${param_permanent_address_city}		[Common] - Create string param		city		${arg_dic.permanent_address_city}
    ${param_permanent_address_province}		[Common] - Create string param		province		${arg_dic.permanent_address_province}
    ${param_permanent_address_postal_code}		[Common] - Create string param		postal_code		${arg_dic.permanent_address_postal_code}
    ${param_permanent_address_country}		[Common] - Create string param		country		${arg_dic.permanent_address_country}
    ${param_permanent_address_landmark}		[Common] - Create string param		landmark		${arg_dic.permanent_address_landmark}
    ${param_permanent_address_longitude}		[Common] - Create string param		longitude		${arg_dic.permanent_address_longitude}
    ${param_permanent_address_latitude}		[Common] - Create string param		latitude		${arg_dic.permanent_address_latitude}
    ${param_bank_name}		[Common] - Create string param		name		${arg_dic.bank_name}
    ${param_bank_account_status}		[Common] - Create int param		account_status		${arg_dic.bank_account_status}
    ${param_bank_account_name}		[Common] - Create string param		account_name		${arg_dic.bank_account_name}
    ${param_bank_account_number}		[Common] - Create string param		account_number		${arg_dic.bank_account_number}
    ${param_bank_branch_area}		[Common] - Create string param		branch_area		${arg_dic.bank_branch_area}
    ${param_bank_branch_city}		[Common] - Create string param		branch_city		${arg_dic.bank_branch_city}
    ${param_bank_register_date}		[Common] - Create string param		register_date		${arg_dic.bank_register_date}
    ${param_bank_register_source}		[Common] - Create string param		register_source		${arg_dic.bank_register_source}
    ${param_bank_is_verified}		[Common] - Create boolean param		is_verified		${arg_dic.bank_is_verified}
    ${param_bank_end_date}		[Common] - Create string param		end_date		${arg_dic.bank_end_date}
    ${param_contract_type}		[Common] - Create string param		type		${arg_dic.contract_type}
    ${param_contract_number}		[Common] - Create string param		number		${arg_dic.contract_number}
    ${param_contract_extension_type}		[Common] - Create string param		extension_type		${arg_dic.contract_extension_type}
    ${param_contract_sign_date}		[Common] - Create string param		sign_date		${arg_dic.contract_sign_date}
    ${param_contract_issue_date}		[Common] - Create string param		issue_date		${arg_dic.contract_issue_date}
    ${param_contract_expired_date}		[Common] - Create string param		expired_date		${arg_dic.contract_expired_date}
    ${param_contract_notification_alert}		[Common] - Create string param		notification_alert		${arg_dic.contract_notification_alert}
    ${param_contract_day_of_period_reconciliation}		[Common] - Create string param		day_of_period_reconciliation		${arg_dic.contract_day_of_period_reconciliation}
    ${param_contract_release}		[Common] - Create string param		release		${arg_dic.contract_release}
    ${param_contract_file_url}		[Common] - Create string param		file_url		${arg_dic.contract_file_url}
    ${param_contract_assessment_information_url}		[Common] - Create string param		assessment_information_url		${arg_dic.contract_assessment_information_url}
    ${param_primary_identity_type}		[Common] - Create string param		type		${arg_dic.primary_identity_type}
    ${param_primary_identity_status}		[Common] - Create int param		status		${arg_dic.primary_identity_status}
    ${param_primary_identity_identity_id}		[Common] - Create string param		identity_id		${arg_dic.primary_identity_identity_id}
    ${param_primary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${arg_dic.primary_identity_place_of_issue}
    ${param_primary_identity_issue_date}		[Common] - Create string param		issue_date		${arg_dic.primary_identity_issue_date}
    ${param_primary_identity_expired_date}		[Common] - Create string param		expired_date		${arg_dic.primary_identity_expired_date}
    ${param_primary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${arg_dic.primary_identity_front_identity_url}
    ${param_primary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${arg_dic.primary_identity_back_identity_url}
    ${param_secondary_identity_type}		[Common] - Create string param		type		${arg_dic.secondary_identity_type}
    ${param_secondary_identity_status}		[Common] - Create int param		status		${arg_dic.secondary_identity_status}
    ${param_secondary_identity_identity_id}		[Common] - Create string param		identity_id		${arg_dic.secondary_identity_identity_id}
    ${param_secondary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${arg_dic.secondary_identity_place_of_issue}
    ${param_secondary_identity_issue_date}		[Common] - Create string param		issue_date		${arg_dic.secondary_identity_issue_date}
    ${param_secondary_identity_expired_date}		[Common] - Create string param		expired_date		${arg_dic.secondary_identity_expired_date}
    ${param_secondary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${arg_dic.secondary_identity_front_identity_url}
    ${param_secondary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${arg_dic.secondary_identity_back_identity_url}
    ${param_status_id}		[Common] - Create int param		status_id		${arg_dic.status_id}
    ${param_remark}		[Common] - Create string param		remark		${arg_dic.remark}
    ${param_verify_by}		[Common] - Create string param		verify_by		${arg_dic.verify_by}
    ${param_verify_date}		[Common] - Create string param		verify_date		${arg_dic.verify_date}
    ${param_risk_level}		[Common] - Create string param		risk_level		${arg_dic.risk_level}
    ${param_additional_acquiring_sales_executive_id}		[Common] - Create int param		acquiring_sales_executive_id		${arg_dic.additional_acquiring_sales_executive_id}
    ${param_additional_acquiring_sales_executive_name}		[Common] - Create string param		acquiring_sales_executive_name		${arg_dic.additional_acquiring_sales_executive_name}
    ${param_additional_relationship_manager_id}		[Common] - Create int param		relationship_manager_id		${arg_dic.additional_relationship_manager_id}
    ${param_additional_relationship_manager_name}		[Common] - Create string param		relationship_manager_name		${arg_dic.additional_relationship_manager_name}
    ${param_additional_sale_region}		[Common] - Create string param		sale_region		${arg_dic.additional_sale_region}
    ${param_additional_commercial_account_manager}		[Common] - Create string param		commercial_account_manager		${arg_dic.additional_commercial_account_manager}
    ${param_additional_profile_picture_url}		[Common] - Create string param		profile_picture_url		${arg_dic.additional_profile_picture_url}
    ${param_additional_national_id_photo_url}		[Common] - Create string param		national_id_photo_url		${arg_dic.additional_national_id_photo_url}
    ${param_additional_tax_id_card_photo_url}		[Common] - Create string param		tax_id_card_photo_url		${arg_dic.additional_tax_id_card_photo_url}
    ${param_additional_field_1_name}		[Common] - Create string param		field_1_name		${arg_dic.additional_field_1_name}
    ${param_additional_field_1_value}		[Common] - Create string param		field_1_value		${arg_dic.additional_field_1_value}
    ${param_additional_field_2_name}		[Common] - Create string param		field_2_name		${arg_dic.additional_field_2_name}
    ${param_additional_field_2_value}		[Common] - Create string param		field_2_value		${arg_dic.additional_field_2_value}
    ${param_additional_field_3_name}		[Common] - Create string param		field_3_name		${arg_dic.additional_field_3_name}
    ${param_additional_field_3_value}		[Common] - Create string param		field_3_value		${arg_dic.additional_field_3_value}
    ${param_additional_field_4_name}		[Common] - Create string param		field_4_name		${arg_dic.additional_field_4_name}
    ${param_additional_field_4_value}		[Common] - Create string param		field_4_value		${arg_dic.additional_field_4_value}
    ${param_additional_field_5_name}		[Common] - Create string param		field_5_name		${arg_dic.additional_field_5_name}
    ${param_additional_field_5_value}		[Common] - Create string param		field_5_value		${arg_dic.additional_field_5_value}
    ${param_additional_supporting_file_1_url}		[Common] - Create string param		supporting_file_1_url		${arg_dic.additional_supporting_file_1_url}
    ${param_additional_supporting_file_2_url}		[Common] - Create string param		supporting_file_2_url		${arg_dic.additional_supporting_file_2_url}
    ${param_additional_supporting_file_3_url}		[Common] - Create string param		supporting_file_3_url		${arg_dic.additional_supporting_file_3_url}
    ${param_additional_supporting_file_4_url}		[Common] - Create string param		supporting_file_4_url		${arg_dic.additional_supporting_file_4_url}
    ${param_additional_supporting_file_5_url}		[Common] - Create string param		supporting_file_5_url		${arg_dic.additional_supporting_file_5_url}
    ${param_tin_number_local}		[Common] - Create string param		tin_number_local		${arg_dic.tin_number_local}
    ${param_title_local}            [Common] - Create string param		title_local		${arg_dic.title_local}
    ${param_first_name_local}       [Common] - Create string param		first_name_local		${arg_dic.first_name_local}
    ${param_middle_name_local}      [Common] - Create string param		middle_name_local		${arg_dic.middle_name_local}
    ${param_last_name_local}        [Common] - Create string param		last_name_local		${arg_dic.last_name_local}
    ${param_suffix_local}           [Common] - Create string param		suffix_local		${arg_dic.suffix_local}
    ${param_place_of_birth_local}   [Common] - Create string param		place_of_birth_local		${arg_dic.place_of_birth_local}
    ${param_gender_local}           [Common] - Create string param		gender_local		${arg_dic.gender_local}
    ${param_occupation_local}       [Common] - Create string param		occupation_local		${arg_dic.occupation_local}
    ${param_occupation_title_local}     [Common] - Create string param		occupation_title_local		${arg_dic.occupation_title_local}
    ${param_township_name_local}        [Common] - Create string param		township_name_local		${arg_dic.township_name_local}
    ${param_national_id_number_local}   [Common] - Create string param		national_id_number_local		${arg_dic.national_id_number_local}
    ${param_mother_name_local}          [Common] - Create string param		mother_name_local		${arg_dic.mother_name_local}
    ${param_current_address_address_local}      [Common] - Create string param		address_local		${arg_dic.current_address_address_local}
    ${param_current_address_commune_local}      [Common] - Create string param		commune_local		${arg_dic.current_address_commune_local}
    ${param_current_address_district_local}     [Common] - Create string param		district_local		${arg_dic.current_address_district_local}
    ${param_current_address_city_local}         [Common] - Create string param		city_local		${arg_dic.current_address_city_local}
    ${param_current_address_province_local}     [Common] - Create string param		province_local		${arg_dic.current_address_province_local}
    ${param_current_address_postal_code_local}      [Common] - Create string param		postal_code_local		${arg_dic.current_address_postal_code_local}
    ${param_current_address_country_local}      [Common] - Create string param		country_local		${arg_dic.current_address_country_local}
    ${param_permanent_address_address_local}        [Common] - Create string param		address_local		${arg_dic.permanent_address_address_local}
    ${param_permanent_address_commune_local}        [Common] - Create string param		commune_local		${arg_dic.permanent_address_commune_local}
    ${param_permanent_address_district_local}       [Common] - Create string param		district_local		${arg_dic.permanent_address_district_local}
    ${param_permanent_address_city_local}       [Common] - Create string param		city_local		${arg_dic.permanent_address_city_local}
    ${param_permanent_address_province_local}       [Common] - Create string param		province_local		${arg_dic.permanent_address_province_local}
    ${param_permanent_address_postal_code_local}        [Common] - Create string param		postal_code_local		${arg_dic.permanent_address_postal_code_local}
    ${param_permanent_address_country_local}        [Common] - Create string param		country_local		${arg_dic.permanent_address_country_local}
    ${param_bank_name_local}        [Common] - Create string param		name_local		${arg_dic.bank_name_local}
    ${param_bank_branch_area_local}     [Common] - Create string param		branch_area_local		${arg_dic.bank_branch_area_local}
    ${param_bank_branch_city_local}     [Common] - Create string param		branch_city_local		${arg_dic.bank_branch_city_local}
    ${param_primary_identity_identity_id_local}     [Common] - Create string param		identity_id_local		${arg_dic.primary_identity_identity_id_local}
    ${param_secondary_identity_identity_id_local}       [Common] - Create string param		identity_id_local		${arg_dic.secondary_identity_identity_id_local}
    ${param_acquiring_sale_executive_name_local}        [Common] - Create string param		acquiring_sale_executive_name_local		${arg_dic.acquiring_sale_executive_name_local}
    ${param_relationship_manager_name_local}        [Common] - Create string param		relationship_manager_name_local		${arg_dic.relationship_manager_name_local}
    ${param_sale_region_local}      [Common] - Create string param		sale_region_local		${arg_dic.sale_region_local}
    ${param_commercial_account_manager_local}       [Common] - Create string param		commercial_account_manager_local		${arg_dic.commercial_account_manager_local}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...          {
        ...                 ${param_is_testing_account}
        ...                 ${param_is_system_account}
        ...                 ${param_acquisition_source}
        ...                 "referrer_user_type": {
        ...                     ${param_referrer_user_type_id}
        ...                     ${param_referrer_user_type_name}
        ...                 },
        ...                 ${param_referrer_user_id}
        ...                 ${param_agent_type_id}
        ...                 ${param_unique_reference}
        ...                 ${param_mm_card_type_id}
        ...                 ${param_mm_card_level_id}
        ...                 ${param_mm_factory_card_number}
        ...                 ${param_model_type}
        ...                 ${param_is_require_otp}
        ...                 ${param_agent_classification_id}
        ...                 ${param_tin_number}
        ...                 ${param_title}
        ...                 ${param_first_name}
        ...                 ${param_middle_name}
        ...                 ${param_last_name}
        ...                 ${param_suffix}
        ...                 ${param_date_of_birth}
        ...                 ${param_place_of_birth}
        ...                 ${param_gender}
        ...                 ${param_ethnicity}
        ...                 ${param_nationality}
        ...                 ${param_occupation}
        ...                 ${param_occupation_title}
        ...                 ${param_township_code}
        ...                 ${param_township_name}
        ...                 ${param_national_id_number}
        ...                 ${param_mother_name}
        ...                 ${param_email}
        ...                 ${param_primary_mobile_number}
        ...                 ${param_secondary_mobile_number}
        ...                 ${param_tertiary_mobile_number}
        ...                 ${param_tin_number_local}
        ...                 ${param_title_local}
        ...                 ${param_first_name_local}
        ...                 ${param_middle_name_local}
        ...                 ${param_last_name_local}
        ...                 ${param_suffix_local}
        ...                 ${param_place_of_birth_local}
        ...                 ${param_gender_local}
        ...                 ${param_occupation_local}
        ...                 ${param_occupation_title_local}
        ...                 ${param_township_name_local}
        ...                 ${param_national_id_number_local}
        ...                 ${param_mother_name_local}
        ...                 "address": {
        ...                     "current_address": {
        ...      					${param_current_address_citizen_association}
        ...      					${param_current_address_neighbourhood_association}
        ...      					${param_current_address_address}
        ...      					${param_current_address_commune}
        ...      					${param_current_address_district}
        ...      					${param_current_address_city}
        ...      					${param_current_address_province}
        ...      					${param_current_address_postal_code}
        ...      					${param_current_address_country}
        ...      					${param_current_address_landmark}
        ...      					${param_current_address_longitude}
        ...      					${param_current_address_latitude}
        ...                         ${param_current_address_address_local}
        ...                         ${param_current_address_commune_local}
        ...                         ${param_current_address_district_local}
        ...                         ${param_current_address_city_local}
        ...                         ${param_current_address_province_local}
        ...                         ${param_current_address_postal_code_local}
        ...                         ${param_current_address_country_local}
        ...                     },
        ...                     "permanent_address": {
        ...      					${param_permanent_address_citizen_association}
        ...      					${param_permanent_address_neighbourhood_association}
        ...      					${param_permanent_address_address}
        ...      					${param_permanent_address_commune}
        ...      					${param_permanent_address_district}
        ...      					${param_permanent_address_city}
        ...      					${param_permanent_address_province}
        ...      					${param_permanent_address_postal_code}
        ...      					${param_permanent_address_country}
        ...      					${param_permanent_address_landmark}
        ...      					${param_permanent_address_longitude}
        ...      					${param_permanent_address_latitude}
        ...                         ${param_permanent_address_address_local}
        ...                         ${param_permanent_address_commune_local}
        ...                         ${param_permanent_address_district_local}
        ...                         ${param_permanent_address_city_local}
        ...                         ${param_permanent_address_province_local}
        ...                         ${param_permanent_address_postal_code_local}
        ...                         ${param_permanent_address_country_local}
        ...                     }
        ...                 },
        ...                 "bank":{
        ...      				${param_bank_name}
        ...      				${param_bank_account_status}
        ...      				${param_bank_account_name}
        ...      				${param_bank_account_number}
        ...      				${param_bank_branch_area}
        ...      				${param_bank_branch_city}
        ...      				${param_bank_register_date}
        ...      				${param_bank_register_source}
        ...      				${param_bank_is_verified}
        ...      				${param_bank_end_date}
        ...                     ${param_bank_name_local}
        ...                     ${param_bank_branch_area_local}
        ...                     ${param_bank_branch_city_local}
        ...                 },
        ...                 "contract": {
        ...      				${param_contract_type}
        ...      				${param_contract_number}
        ...      				${param_contract_extension_type}
        ...      				${param_contract_sign_date}
        ...      				${param_contract_issue_date}
        ...      				${param_contract_expired_date}
        ...      				${param_contract_notification_alert}
        ...      				${param_contract_day_of_period_reconciliation}
        ...      				${param_contract_release}
        ...      				${param_contract_file_url}
        ...      				${param_contract_assessment_information_url}
        ...                  },
        ...                 "accreditation": {
        ...                     "primary_identity": {
        ...      					${param_primary_identity_type}
        ...      					${param_primary_identity_status}
        ...      					${param_primary_identity_identity_id}
        ...      					${param_primary_identity_place_of_issue}
        ...      					${param_primary_identity_issue_date}
        ...      					${param_primary_identity_expired_date}
        ...      					${param_primary_identity_front_identity_url}
        ...      					${param_primary_identity_back_identity_url}
        ...                         ${param_primary_identity_identity_id_local}
        ...                     },
        ...                     "secondary_identity": {
        ...      					${param_secondary_identity_type}
        ...      					${param_secondary_identity_status}
        ...      					${param_secondary_identity_identity_id}
        ...      					${param_secondary_identity_place_of_issue}
        ...      					${param_secondary_identity_issue_date}
        ...      					${param_secondary_identity_expired_date}
        ...      					${param_secondary_identity_front_identity_url}
        ...      					${param_secondary_identity_back_identity_url}
        ...                         ${param_secondary_identity_identity_id_local}
        ...                     },
        ...      				${param_status_id}
        ...      				${param_remark}
        ...      				${param_verify_by}
        ...      				${param_verify_date}
        ...      				${param_risk_level}
        ...                 },
        ...                 "additional": {
        ...      				${param_additional_acquiring_sales_executive_id}
        ...      				${param_additional_acquiring_sales_executive_name}
        ...      				${param_additional_relationship_manager_id}
        ...      				${param_additional_relationship_manager_name}
        ...      				${param_additional_sale_region}
        ...      				${param_additional_commercial_account_manager}
        ...      				${param_additional_profile_picture_url}
        ...      				${param_additional_national_id_photo_url}
        ...      				${param_additional_tax_id_card_photo_url}
        ...      				${param_additional_field_1_name}
        ...      				${param_additional_field_1_value}
        ...      				${param_additional_field_2_name}
        ...      				${param_additional_field_2_value}
        ...      				${param_additional_field_3_name}
        ...      				${param_additional_field_3_value}
        ...      				${param_additional_field_4_name}
        ...      				${param_additional_field_4_value}
        ...      				${param_additional_field_5_name}
        ...      				${param_additional_field_5_value}
        ...      				${param_additional_supporting_file_1_url}
        ...      				${param_additional_supporting_file_2_url}
        ...      				${param_additional_supporting_file_3_url}
        ...      				${param_additional_supporting_file_4_url}
        ...      				${param_additional_supporting_file_5_url}
        ...                     ${param_acquiring_sale_executive_name_local}
        ...                     ${param_relationship_manager_name_local}
        ...                     ${param_sale_region_local}
        ...                     ${param_commercial_account_manager_local}
        ...                 }
        ...          }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${agent_id}=    convert to string   ${arg_dic.agent_id}
    ${admin_update_agent_profiles_path}=    replace string      ${admin_update_agent_profiles_path}      {agent_id}        ${agent_id}   1
                create session          api        ${api_gateway_host}
    ${response}             patch request
        ...     api
        ...     ${admin_update_agent_profiles_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
        ...     ${NONE}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API agent updates agent profiles
    [Arguments]     ${arg_dic}
    ${correlation_id}       generate random string  20  [UPPER]
    ${param_referrer_user_type_id}		[Common] - Create int param		id		${arg_dic.referrer_user_type_id}
    ${param_referrer_user_type_name}		[Common] - Create string param		name		${arg_dic.referrer_user_type_name}
    ${param_referrer_user_id}		[Common] - Create int param		referrer_user_id		${arg_dic.referrer_user_id}
    ${param_unique_reference}		[Common] - Create string param		unique_reference		${arg_dic.unique_reference}
    ${param_company_id}             [Common] - Create int param		company_id		${arg_dic.company_id}
    ${param_mm_card_type_id}		[Common] - Create int param		mm_card_type_id		${arg_dic.mm_card_type_id}
    ${param_mm_card_level_id}		[Common] - Create int param		mm_card_level_id		${arg_dic.mm_card_level_id}
    ${param_mm_factory_card_number}		[Common] - Create string param		mm_factory_card_number		${arg_dic.mm_factory_card_number}
    ${param_is_require_otp}		[Common] - Create boolean param		is_require_otp		${arg_dic.is_require_otp}
    ${param_tin_number}		[Common] - Create string param		tin_number		${arg_dic.tin_number}
    ${param_title}		[Common] - Create string param		title		${arg_dic.title}
    ${param_first_name}		[Common] - Create string param		first_name		${arg_dic.first_name}
    ${param_middle_name}		[Common] - Create string param		middle_name		${arg_dic.middle_name}
    ${param_last_name}		[Common] - Create string param		last_name		${arg_dic.last_name}
    ${param_suffix}		[Common] - Create string param		suffix		${arg_dic.suffix}
    ${param_date_of_birth}		[Common] - Create string param		date_of_birth		${arg_dic.date_of_birth}
    ${param_place_of_birth}		[Common] - Create string param		place_of_birth		${arg_dic.place_of_birth}
    ${param_gender}		[Common] - Create string param		gender		${arg_dic.gender}
    ${param_ethnicity}		[Common] - Create string param		ethnicity		${arg_dic.ethnicity}
    ${param_nationality}		[Common] - Create string param		nationality		${arg_dic.nationality}
    ${param_occupation}		[Common] - Create string param		occupation		${arg_dic.occupation}
    ${param_occupation_title}		[Common] - Create string param		occupation_title		${arg_dic.occupation_title}
    ${param_township_code}		[Common] - Create string param		township_code		${arg_dic.township_code}
    ${param_township_name}		[Common] - Create string param		township_name		${arg_dic.township_name}
    ${param_national_id_number}		[Common] - Create string param		national_id_number		${arg_dic.national_id_number}
    ${param_mother_name}		[Common] - Create string param		mother_name		${arg_dic.mother_name}
    ${param_email}		[Common] - Create string param		email		${arg_dic.email}
    ${param_primary_mobile_number}		[Common] - Create string param		primary_mobile_number		${arg_dic.primary_mobile_number}
    ${param_secondary_mobile_number}		[Common] - Create string param		secondary_mobile_number		${arg_dic.secondary_mobile_number}
    ${param_tertiary_mobile_number}		[Common] - Create string param		tertiary_mobile_number		${arg_dic.tertiary_mobile_number}
    ${param_current_address_citizen_association}		[Common] - Create string param		citizen_association		${arg_dic.current_address_citizen_association}
    ${param_current_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${arg_dic.current_address_neighbourhood_association}
    ${param_current_address_address}		[Common] - Create string param		address		${arg_dic.current_address_address}
    ${param_current_address_commune}		[Common] - Create string param		commune		${arg_dic.current_address_commune}
    ${param_current_address_district}		[Common] - Create string param		district		${arg_dic.current_address_district}
    ${param_current_address_city}		[Common] - Create string param		city		${arg_dic.current_address_city}
    ${param_current_address_province}		[Common] - Create string param		province		${arg_dic.current_address_province}
    ${param_current_address_postal_code}		[Common] - Create string param		postal_code		${arg_dic.current_address_postal_code}
    ${param_current_address_country}		[Common] - Create string param		country		${arg_dic.current_address_country}
    ${param_current_address_landmark}		[Common] - Create string param		landmark		${arg_dic.current_address_landmark}
    ${param_current_address_longitude}		[Common] - Create string param		longitude		${arg_dic.current_address_longitude}
    ${param_current_address_latitude}		[Common] - Create string param		latitude		${arg_dic.current_address_latitude}
    ${param_permanent_address_citizen_association}		[Common] - Create string param		citizen_association		${arg_dic.permanent_address_citizen_association}
    ${param_permanent_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${arg_dic.permanent_address_neighbourhood_association}
    ${param_permanent_address_address}		[Common] - Create string param		address		${arg_dic.permanent_address_address}
    ${param_permanent_address_commune}		[Common] - Create string param		commune		${arg_dic.permanent_address_commune}
    ${param_permanent_address_district}		[Common] - Create string param		district		${arg_dic.permanent_address_district}
    ${param_permanent_address_city}		[Common] - Create string param		city		${arg_dic.permanent_address_city}
    ${param_permanent_address_province}		[Common] - Create string param		province		${arg_dic.permanent_address_province}
    ${param_permanent_address_postal_code}		[Common] - Create string param		postal_code		${arg_dic.permanent_address_postal_code}
    ${param_permanent_address_country}		[Common] - Create string param		country		${arg_dic.permanent_address_country}
    ${param_permanent_address_landmark}		[Common] - Create string param		landmark		${arg_dic.permanent_address_landmark}
    ${param_permanent_address_longitude}		[Common] - Create string param		longitude		${arg_dic.permanent_address_longitude}
    ${param_permanent_address_latitude}		[Common] - Create string param		latitude		${arg_dic.permanent_address_latitude}
    ${param_contract_type}		[Common] - Create string param		type		${arg_dic.contract_type}
    ${param_contract_number}		[Common] - Create string param		number		${arg_dic.contract_number}
    ${param_contract_extension_type}		[Common] - Create string param		extension_type		${arg_dic.contract_extension_type}
    ${param_contract_sign_date}		[Common] - Create string param		sign_date		${arg_dic.contract_sign_date}
    ${param_contract_issue_date}		[Common] - Create string param		issue_date		${arg_dic.contract_issue_date}
    ${param_contract_expired_date}		[Common] - Create string param		expired_date		${arg_dic.contract_expired_date}
    ${param_contract_notification_alert}		[Common] - Create string param		notification_alert		${arg_dic.contract_notification_alert}
    ${param_contract_day_of_period_reconciliation}		[Common] - Create string param		day_of_period_reconciliation		${arg_dic.contract_day_of_period_reconciliation}
    ${param_contract_release}		[Common] - Create string param		release		${arg_dic.contract_release}
    ${param_primary_identity_type}		[Common] - Create string param		type		${arg_dic.primary_identity_type}
    ${param_primary_identity_identity_id}		[Common] - Create string param		identity_id		${arg_dic.primary_identity_identity_id}
    ${param_primary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${arg_dic.primary_identity_place_of_issue}
    ${param_primary_identity_issue_date}		[Common] - Create string param		issue_date		${arg_dic.primary_identity_issue_date}
    ${param_primary_identity_expired_date}		[Common] - Create string param		expired_date		${arg_dic.primary_identity_expired_date}
    ${param_primary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${arg_dic.primary_identity_front_identity_url}
    ${param_primary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${arg_dic.primary_identity_back_identity_url}
    ${param_secondary_identity_type}		[Common] - Create string param		type		${arg_dic.secondary_identity_type}
    ${param_secondary_identity_identity_id}		[Common] - Create string param		identity_id		${arg_dic.secondary_identity_identity_id}
    ${param_secondary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${arg_dic.secondary_identity_place_of_issue}
    ${param_secondary_identity_issue_date}		[Common] - Create string param		issue_date		${arg_dic.secondary_identity_issue_date}
    ${param_secondary_identity_expired_date}		[Common] - Create string param		expired_date		${arg_dic.secondary_identity_expired_date}
    ${param_secondary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${arg_dic.secondary_identity_front_identity_url}
    ${param_secondary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${arg_dic.secondary_identity_back_identity_url}
    ${param_additional_profile_picture_url}		[Common] - Create string param		profile_picture_url		${arg_dic.additional_profile_picture_url}
    ${param_additional_national_id_photo_url}		[Common] - Create string param		national_id_photo_url		${arg_dic.additional_national_id_photo_url}
    ${param_additional_tax_id_card_photo_url}		[Common] - Create string param		tax_id_card_photo_url		${arg_dic.additional_tax_id_card_photo_url}
    ${param_tin_number_local}		[Common] - Create string param		tin_number_local		${arg_dic.tin_number_local}
    ${param_title_local}            [Common] - Create string param		title_local		${arg_dic.title_local}
    ${param_first_name_local}       [Common] - Create string param		first_name_local		${arg_dic.first_name_local}
    ${param_middle_name_local}      [Common] - Create string param		middle_name_local		${arg_dic.middle_name_local}
    ${param_last_name_local}        [Common] - Create string param		last_name_local		${arg_dic.last_name_local}
    ${param_suffix_local}           [Common] - Create string param		suffix_local		${arg_dic.suffix_local}
    ${param_place_of_birth_local}   [Common] - Create string param		place_of_birth_local		${arg_dic.place_of_birth_local}
    ${param_gender_local}           [Common] - Create string param		gender_local		${arg_dic.gender_local}
    ${param_occupation_local}       [Common] - Create string param		occupation_local		${arg_dic.occupation_local}
    ${param_occupation_title_local}     [Common] - Create string param		occupation_title_local		${arg_dic.occupation_title_local}
    ${param_township_name_local}        [Common] - Create string param		township_name_local		${arg_dic.township_name_local}
    ${param_national_id_number_local}   [Common] - Create string param		national_id_number_local		${arg_dic.national_id_number_local}
    ${param_mother_name_local}          [Common] - Create string param		mother_name_local		${arg_dic.mother_name_local}
    ${param_current_address_address_local}      [Common] - Create string param		address_local		${arg_dic.current_address_address_local}
    ${param_current_address_commune_local}      [Common] - Create string param		commune_local		${arg_dic.current_address_commune_local}
    ${param_current_address_district_local}     [Common] - Create string param		district_local		${arg_dic.current_address_district_local}
    ${param_current_address_city_local}         [Common] - Create string param		city_local		${arg_dic.current_address_city_local}
    ${param_current_address_province_local}     [Common] - Create string param		province_local		${arg_dic.current_address_province_local}
    ${param_current_address_postal_code_local}      [Common] - Create string param		postal_code_local		${arg_dic.current_address_postal_code_local}
    ${param_current_address_country_local}      [Common] - Create string param		country_local		${arg_dic.current_address_country_local}
    ${param_permanent_address_address_local}        [Common] - Create string param		address_local		${arg_dic.permanent_address_address_local}
    ${param_permanent_address_commune_local}        [Common] - Create string param		commune_local		${arg_dic.permanent_address_commune_local}
    ${param_permanent_address_district_local}       [Common] - Create string param		district_local		${arg_dic.permanent_address_district_local}
    ${param_permanent_address_city_local}       [Common] - Create string param		city_local		${arg_dic.permanent_address_city_local}
    ${param_permanent_address_province_local}       [Common] - Create string param		province_local		${arg_dic.permanent_address_province_local}
    ${param_permanent_address_postal_code_local}        [Common] - Create string param		postal_code_local		${arg_dic.permanent_address_postal_code_local}
    ${param_permanent_address_country_local}        [Common] - Create string param		country_local		${arg_dic.permanent_address_country_local}
    ${param_primary_identity_identity_id_local}     [Common] - Create string param		identity_id_local		${arg_dic.primary_identity_identity_id_local}
    ${param_secondary_identity_identity_id_local}       [Common] - Create string param		identity_id_local		${arg_dic.secondary_identity_identity_id_local}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...          {
        ...                 "referrer_user_type": {
        ...                     ${param_referrer_user_type_id}
        ...                     ${param_referrer_user_type_name}
        ...                 },
        ...                 ${param_referrer_user_id}
        ...                 ${param_unique_reference}
        ...                 ${param_mm_card_type_id}
        ...                 ${param_mm_card_level_id}
        ...                 ${param_mm_factory_card_number}
        ...                 ${param_is_require_otp}
        ...                 ${param_tin_number}
        ...                 ${param_title}
        ...                 ${param_first_name}
        ...                 ${param_middle_name}
        ...                 ${param_last_name}
        ...                 ${param_suffix}
        ...                 ${param_date_of_birth}
        ...                 ${param_place_of_birth}
        ...                 ${param_gender}
        ...                 ${param_ethnicity}
        ...                 ${param_nationality}
        ...                 ${param_occupation}
        ...                 ${param_occupation_title}
        ...                 ${param_township_code}
        ...                 ${param_township_name}
        ...                 ${param_national_id_number}
        ...                 ${param_mother_name}
        ...                 ${param_email}
        ...                 ${param_primary_mobile_number}
        ...                 ${param_secondary_mobile_number}
        ...                 ${param_tertiary_mobile_number}
        ...                 ${param_tin_number_local}
        ...                 ${param_title_local}
        ...                 ${param_first_name_local}
        ...                 ${param_middle_name_local}
        ...                 ${param_last_name_local}
        ...                 ${param_suffix_local}
        ...                 ${param_place_of_birth_local}
        ...                 ${param_gender_local}
        ...                 ${param_occupation_local}
        ...                 ${param_occupation_title_local}
        ...                 ${param_township_name_local}
        ...                 ${param_national_id_number_local}
        ...                 ${param_mother_name_local}
        ...                 "address": {
        ...                     "current_address": {
        ...      					${param_current_address_citizen_association}
        ...      					${param_current_address_neighbourhood_association}
        ...      					${param_current_address_address}
        ...      					${param_current_address_commune}
        ...      					${param_current_address_district}
        ...      					${param_current_address_city}
        ...      					${param_current_address_province}
        ...      					${param_current_address_postal_code}
        ...      					${param_current_address_country}
        ...      					${param_current_address_landmark}
        ...      					${param_current_address_longitude}
        ...      					${param_current_address_latitude}
        ...                         ${param_current_address_address_local}
        ...                         ${param_current_address_commune_local}
        ...                         ${param_current_address_district_local}
        ...                         ${param_current_address_city_local}
        ...                         ${param_current_address_province_local}
        ...                         ${param_current_address_postal_code_local}
        ...                         ${param_current_address_country_local}
        ...                     },
        ...                     "permanent_address": {
        ...      					${param_permanent_address_citizen_association}
        ...      					${param_permanent_address_neighbourhood_association}
        ...      					${param_permanent_address_address}
        ...      					${param_permanent_address_commune}
        ...      					${param_permanent_address_district}
        ...      					${param_permanent_address_city}
        ...      					${param_permanent_address_province}
        ...      					${param_permanent_address_postal_code}
        ...      					${param_permanent_address_country}
        ...      					${param_permanent_address_landmark}
        ...      					${param_permanent_address_longitude}
        ...      					${param_permanent_address_latitude}
        ...                         ${param_permanent_address_address_local}
        ...                         ${param_permanent_address_commune_local}
        ...                         ${param_permanent_address_district_local}
        ...                         ${param_permanent_address_city_local}
        ...                         ${param_permanent_address_province_local}
        ...                         ${param_permanent_address_postal_code_local}
        ...                         ${param_permanent_address_country_local}
        ...                     }
        ...                 },
        ...                 "contract": {
        ...      				${param_contract_type}
        ...      				${param_contract_number}
        ...      				${param_contract_extension_type}
        ...      				${param_contract_sign_date}
        ...      				${param_contract_issue_date}
        ...      				${param_contract_expired_date}
        ...      				${param_contract_notification_alert}
        ...      				${param_contract_day_of_period_reconciliation}
        ...      				${param_contract_release}
        ...                  },
        ...                 "accreditation": {
        ...                     "primary_identity": {
        ...      					${param_primary_identity_type}
        ...      					${param_primary_identity_identity_id}
        ...      					${param_primary_identity_place_of_issue}
        ...      					${param_primary_identity_issue_date}
        ...      					${param_primary_identity_expired_date}
        ...      					${param_primary_identity_front_identity_url}
        ...      					${param_primary_identity_back_identity_url}
        ...                         ${param_primary_identity_identity_id_local}
        ...                     },
        ...                     "secondary_identity": {
        ...      					${param_secondary_identity_type}
        ...      					${param_secondary_identity_identity_id}
        ...      					${param_secondary_identity_place_of_issue}
        ...      					${param_secondary_identity_issue_date}
        ...      					${param_secondary_identity_expired_date}
        ...      					${param_secondary_identity_front_identity_url}
        ...      					${param_secondary_identity_back_identity_url}
        ...                         ${param_secondary_identity_identity_id_local}
        ...                     }
        ...                 },
        ...                 "additional": {
        ...      				${param_additional_profile_picture_url}
        ...      				${param_additional_national_id_photo_url}
        ...      				${param_additional_tax_id_card_photo_url}
        ...                 }
        ...          }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${agent_update_agent_profiles_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API agent updates agent profiles with PATCH method
    [Arguments]     ${arg_dic}
    ${correlation_id}       generate random string  20  [UPPER]
    ${param_referrer_user_type_id}		[Common] - Create int param		id		${arg_dic.referrer_user_type_id}
    ${param_referrer_user_type_name}		[Common] - Create string param		name		${arg_dic.referrer_user_type_name}
    ${param_referrer_user_id}		[Common] - Create int param		referrer_user_id		${arg_dic.referrer_user_id}
    ${param_unique_reference}		[Common] - Create string param		unique_reference		${arg_dic.unique_reference}
    ${param_company_id}             [Common] - Create int param		company_id		${arg_dic.company_id}
    ${param_mm_card_type_id}		[Common] - Create int param		mm_card_type_id		${arg_dic.mm_card_type_id}
    ${param_mm_card_level_id}		[Common] - Create int param		mm_card_level_id		${arg_dic.mm_card_level_id}
    ${param_mm_factory_card_number}		[Common] - Create string param		mm_factory_card_number		${arg_dic.mm_factory_card_number}
    ${param_is_require_otp}		[Common] - Create boolean param		is_require_otp		${arg_dic.is_require_otp}
    ${param_tin_number}		[Common] - Create string param		tin_number		${arg_dic.tin_number}
    ${param_title}		[Common] - Create string param		title		${arg_dic.title}
    ${param_first_name}		[Common] - Create string param		first_name		${arg_dic.first_name}
    ${param_middle_name}		[Common] - Create string param		middle_name		${arg_dic.middle_name}
    ${param_last_name}		[Common] - Create string param		last_name		${arg_dic.last_name}
    ${param_suffix}		[Common] - Create string param		suffix		${arg_dic.suffix}
    ${param_date_of_birth}		[Common] - Create string param		date_of_birth		${arg_dic.date_of_birth}
    ${param_place_of_birth}		[Common] - Create string param		place_of_birth		${arg_dic.place_of_birth}
    ${param_gender}		[Common] - Create string param		gender		${arg_dic.gender}
    ${param_ethnicity}		[Common] - Create string param		ethnicity		${arg_dic.ethnicity}
    ${param_nationality}		[Common] - Create string param		nationality		${arg_dic.nationality}
    ${param_occupation}		[Common] - Create string param		occupation		${arg_dic.occupation}
    ${param_occupation_title}		[Common] - Create string param		occupation_title		${arg_dic.occupation_title}
    ${param_township_code}		[Common] - Create string param		township_code		${arg_dic.township_code}
    ${param_township_name}		[Common] - Create string param		township_name		${arg_dic.township_name}
    ${param_national_id_number}		[Common] - Create string param		national_id_number		${arg_dic.national_id_number}
    ${param_mother_name}		[Common] - Create string param		mother_name		${arg_dic.mother_name}
    ${param_email}		[Common] - Create string param		email		${arg_dic.email}
    ${param_primary_mobile_number}		[Common] - Create string param		primary_mobile_number		${arg_dic.primary_mobile_number}
    ${param_secondary_mobile_number}		[Common] - Create string param		secondary_mobile_number		${arg_dic.secondary_mobile_number}
    ${param_tertiary_mobile_number}		[Common] - Create string param		tertiary_mobile_number		${arg_dic.tertiary_mobile_number}
    ${param_current_address_citizen_association}		[Common] - Create string param		citizen_association		${arg_dic.current_address_citizen_association}
    ${param_current_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${arg_dic.current_address_neighbourhood_association}
    ${param_current_address_address}		[Common] - Create string param		address		${arg_dic.current_address_address}
    ${param_current_address_commune}		[Common] - Create string param		commune		${arg_dic.current_address_commune}
    ${param_current_address_district}		[Common] - Create string param		district		${arg_dic.current_address_district}
    ${param_current_address_city}		[Common] - Create string param		city		${arg_dic.current_address_city}
    ${param_current_address_province}		[Common] - Create string param		province		${arg_dic.current_address_province}
    ${param_current_address_postal_code}		[Common] - Create string param		postal_code		${arg_dic.current_address_postal_code}
    ${param_current_address_country}		[Common] - Create string param		country		${arg_dic.current_address_country}
    ${param_current_address_landmark}		[Common] - Create string param		landmark		${arg_dic.current_address_landmark}
    ${param_current_address_longitude}		[Common] - Create string param		longitude		${arg_dic.current_address_longitude}
    ${param_current_address_latitude}		[Common] - Create string param		latitude		${arg_dic.current_address_latitude}
    ${param_permanent_address_citizen_association}		[Common] - Create string param		citizen_association		${arg_dic.permanent_address_citizen_association}
    ${param_permanent_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${arg_dic.permanent_address_neighbourhood_association}
    ${param_permanent_address_address}		[Common] - Create string param		address		${arg_dic.permanent_address_address}
    ${param_permanent_address_commune}		[Common] - Create string param		commune		${arg_dic.permanent_address_commune}
    ${param_permanent_address_district}		[Common] - Create string param		district		${arg_dic.permanent_address_district}
    ${param_permanent_address_city}		[Common] - Create string param		city		${arg_dic.permanent_address_city}
    ${param_permanent_address_province}		[Common] - Create string param		province		${arg_dic.permanent_address_province}
    ${param_permanent_address_postal_code}		[Common] - Create string param		postal_code		${arg_dic.permanent_address_postal_code}
    ${param_permanent_address_country}		[Common] - Create string param		country		${arg_dic.permanent_address_country}
    ${param_permanent_address_landmark}		[Common] - Create string param		landmark		${arg_dic.permanent_address_landmark}
    ${param_permanent_address_longitude}		[Common] - Create string param		longitude		${arg_dic.permanent_address_longitude}
    ${param_permanent_address_latitude}		[Common] - Create string param		latitude		${arg_dic.permanent_address_latitude}
    ${param_contract_type}		[Common] - Create string param		type		${arg_dic.contract_type}
    ${param_contract_number}		[Common] - Create string param		number		${arg_dic.contract_number}
    ${param_contract_extension_type}		[Common] - Create string param		extension_type		${arg_dic.contract_extension_type}
    ${param_contract_sign_date}		[Common] - Create string param		sign_date		${arg_dic.contract_sign_date}
    ${param_contract_issue_date}		[Common] - Create string param		issue_date		${arg_dic.contract_issue_date}
    ${param_contract_expired_date}		[Common] - Create string param		expired_date		${arg_dic.contract_expired_date}
    ${param_contract_notification_alert}		[Common] - Create string param		notification_alert		${arg_dic.contract_notification_alert}
    ${param_contract_day_of_period_reconciliation}		[Common] - Create string param		day_of_period_reconciliation		${arg_dic.contract_day_of_period_reconciliation}
    ${param_contract_release}		[Common] - Create string param		release		${arg_dic.contract_release}
    ${param_primary_identity_type}		[Common] - Create string param		type		${arg_dic.primary_identity_type}
    ${param_primary_identity_identity_id}		[Common] - Create string param		identity_id		${arg_dic.primary_identity_identity_id}
    ${param_primary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${arg_dic.primary_identity_place_of_issue}
    ${param_primary_identity_issue_date}		[Common] - Create string param		issue_date		${arg_dic.primary_identity_issue_date}
    ${param_primary_identity_expired_date}		[Common] - Create string param		expired_date		${arg_dic.primary_identity_expired_date}
    ${param_primary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${arg_dic.primary_identity_front_identity_url}
    ${param_primary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${arg_dic.primary_identity_back_identity_url}
    ${param_secondary_identity_type}		[Common] - Create string param		type		${arg_dic.secondary_identity_type}
    ${param_secondary_identity_identity_id}		[Common] - Create string param		identity_id		${arg_dic.secondary_identity_identity_id}
    ${param_secondary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${arg_dic.secondary_identity_place_of_issue}
    ${param_secondary_identity_issue_date}		[Common] - Create string param		issue_date		${arg_dic.secondary_identity_issue_date}
    ${param_secondary_identity_expired_date}		[Common] - Create string param		expired_date		${arg_dic.secondary_identity_expired_date}
    ${param_secondary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${arg_dic.secondary_identity_front_identity_url}
    ${param_secondary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${arg_dic.secondary_identity_back_identity_url}
    ${param_additional_profile_picture_url}		[Common] - Create string param		profile_picture_url		${arg_dic.additional_profile_picture_url}
    ${param_additional_national_id_photo_url}		[Common] - Create string param		national_id_photo_url		${arg_dic.additional_national_id_photo_url}
    ${param_additional_tax_id_card_photo_url}		[Common] - Create string param		tax_id_card_photo_url		${arg_dic.additional_tax_id_card_photo_url}
    ${param_tin_number_local}		[Common] - Create string param		tin_number_local		${arg_dic.tin_number_local}
    ${param_title_local}            [Common] - Create string param		title_local		${arg_dic.title_local}
    ${param_first_name_local}       [Common] - Create string param		first_name_local		${arg_dic.first_name_local}
    ${param_middle_name_local}      [Common] - Create string param		middle_name_local		${arg_dic.middle_name_local}
    ${param_last_name_local}        [Common] - Create string param		last_name_local		${arg_dic.last_name_local}
    ${param_suffix_local}           [Common] - Create string param		suffix_local		${arg_dic.suffix_local}
    ${param_place_of_birth_local}   [Common] - Create string param		place_of_birth_local		${arg_dic.place_of_birth_local}
    ${param_gender_local}           [Common] - Create string param		gender_local		${arg_dic.gender_local}
    ${param_occupation_local}       [Common] - Create string param		occupation_local		${arg_dic.occupation_local}
    ${param_occupation_title_local}     [Common] - Create string param		occupation_title_local		${arg_dic.occupation_title_local}
    ${param_township_name_local}        [Common] - Create string param		township_name_local		${arg_dic.township_name_local}
    ${param_national_id_number_local}   [Common] - Create string param		national_id_number_local		${arg_dic.national_id_number_local}
    ${param_mother_name_local}          [Common] - Create string param		mother_name_local		${arg_dic.mother_name_local}
    ${param_current_address_address_local}      [Common] - Create string param		address_local		${arg_dic.current_address_address_local}
    ${param_current_address_commune_local}      [Common] - Create string param		commune_local		${arg_dic.current_address_commune_local}
    ${param_current_address_district_local}     [Common] - Create string param		district_local		${arg_dic.current_address_district_local}
    ${param_current_address_city_local}         [Common] - Create string param		city_local		${arg_dic.current_address_city_local}
    ${param_current_address_province_local}     [Common] - Create string param		province_local		${arg_dic.current_address_province_local}
    ${param_current_address_postal_code_local}      [Common] - Create string param		postal_code_local		${arg_dic.current_address_postal_code_local}
    ${param_current_address_country_local}      [Common] - Create string param		country_local		${arg_dic.current_address_country_local}
    ${param_permanent_address_address_local}        [Common] - Create string param		address_local		${arg_dic.permanent_address_address_local}
    ${param_permanent_address_commune_local}        [Common] - Create string param		commune_local		${arg_dic.permanent_address_commune_local}
    ${param_permanent_address_district_local}       [Common] - Create string param		district_local		${arg_dic.permanent_address_district_local}
    ${param_permanent_address_city_local}       [Common] - Create string param		city_local		${arg_dic.permanent_address_city_local}
    ${param_permanent_address_province_local}       [Common] - Create string param		province_local		${arg_dic.permanent_address_province_local}
    ${param_permanent_address_postal_code_local}        [Common] - Create string param		postal_code_local		${arg_dic.permanent_address_postal_code_local}
    ${param_permanent_address_country_local}        [Common] - Create string param		country_local		${arg_dic.permanent_address_country_local}
    ${param_primary_identity_identity_id_local}     [Common] - Create string param		identity_id_local		${arg_dic.primary_identity_identity_id_local}
    ${param_secondary_identity_identity_id_local}       [Common] - Create string param		identity_id_local		${arg_dic.secondary_identity_identity_id_local}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...          {
        ...                 "referrer_user_type": {
        ...                     ${param_referrer_user_type_id}
        ...                     ${param_referrer_user_type_name}
        ...                 },
        ...                 ${param_referrer_user_id}
        ...                 ${param_unique_reference}
        ...                 ${param_mm_card_type_id}
        ...                 ${param_mm_card_level_id}
        ...                 ${param_mm_factory_card_number}
        ...                 ${param_is_require_otp}
        ...                 ${param_tin_number}
        ...                 ${param_title}
        ...                 ${param_first_name}
        ...                 ${param_middle_name}
        ...                 ${param_last_name}
        ...                 ${param_suffix}
        ...                 ${param_date_of_birth}
        ...                 ${param_place_of_birth}
        ...                 ${param_gender}
        ...                 ${param_ethnicity}
        ...                 ${param_nationality}
        ...                 ${param_occupation}
        ...                 ${param_occupation_title}
        ...                 ${param_township_code}
        ...                 ${param_township_name}
        ...                 ${param_national_id_number}
        ...                 ${param_mother_name}
        ...                 ${param_email}
        ...                 ${param_primary_mobile_number}
        ...                 ${param_secondary_mobile_number}
        ...                 ${param_tertiary_mobile_number}
        ...                 ${param_tin_number_local}
        ...                 ${param_title_local}
        ...                 ${param_first_name_local}
        ...                 ${param_middle_name_local}
        ...                 ${param_last_name_local}
        ...                 ${param_suffix_local}
        ...                 ${param_place_of_birth_local}
        ...                 ${param_gender_local}
        ...                 ${param_occupation_local}
        ...                 ${param_occupation_title_local}
        ...                 ${param_township_name_local}
        ...                 ${param_national_id_number_local}
        ...                 ${param_mother_name_local}
        ...                 "address": {
        ...                     "current_address": {
        ...      					${param_current_address_citizen_association}
        ...      					${param_current_address_neighbourhood_association}
        ...      					${param_current_address_address}
        ...      					${param_current_address_commune}
        ...      					${param_current_address_district}
        ...      					${param_current_address_city}
        ...      					${param_current_address_province}
        ...      					${param_current_address_postal_code}
        ...      					${param_current_address_country}
        ...      					${param_current_address_landmark}
        ...      					${param_current_address_longitude}
        ...      					${param_current_address_latitude}
        ...                         ${param_current_address_address_local}
        ...                         ${param_current_address_commune_local}
        ...                         ${param_current_address_district_local}
        ...                         ${param_current_address_city_local}
        ...                         ${param_current_address_province_local}
        ...                         ${param_current_address_postal_code_local}
        ...                         ${param_current_address_country_local}
        ...                     },
        ...                     "permanent_address": {
        ...      					${param_permanent_address_citizen_association}
        ...      					${param_permanent_address_neighbourhood_association}
        ...      					${param_permanent_address_address}
        ...      					${param_permanent_address_commune}
        ...      					${param_permanent_address_district}
        ...      					${param_permanent_address_city}
        ...      					${param_permanent_address_province}
        ...      					${param_permanent_address_postal_code}
        ...      					${param_permanent_address_country}
        ...      					${param_permanent_address_landmark}
        ...      					${param_permanent_address_longitude}
        ...      					${param_permanent_address_latitude}
        ...                         ${param_permanent_address_address_local}
        ...                         ${param_permanent_address_commune_local}
        ...                         ${param_permanent_address_district_local}
        ...                         ${param_permanent_address_city_local}
        ...                         ${param_permanent_address_province_local}
        ...                         ${param_permanent_address_postal_code_local}
        ...                         ${param_permanent_address_country_local}
        ...                     }
        ...                 },
        ...                 "contract": {
        ...      				${param_contract_type}
        ...      				${param_contract_number}
        ...      				${param_contract_extension_type}
        ...      				${param_contract_sign_date}
        ...      				${param_contract_issue_date}
        ...      				${param_contract_expired_date}
        ...      				${param_contract_notification_alert}
        ...      				${param_contract_day_of_period_reconciliation}
        ...      				${param_contract_release}
        ...                  },
        ...                 "accreditation": {
        ...                     "primary_identity": {
        ...      					${param_primary_identity_type}
        ...      					${param_primary_identity_identity_id}
        ...      					${param_primary_identity_place_of_issue}
        ...      					${param_primary_identity_issue_date}
        ...      					${param_primary_identity_expired_date}
        ...      					${param_primary_identity_front_identity_url}
        ...      					${param_primary_identity_back_identity_url}
        ...                         ${param_primary_identity_identity_id_local}
        ...                     },
        ...                     "secondary_identity": {
        ...      					${param_secondary_identity_type}
        ...      					${param_secondary_identity_identity_id}
        ...      					${param_secondary_identity_place_of_issue}
        ...      					${param_secondary_identity_issue_date}
        ...      					${param_secondary_identity_expired_date}
        ...      					${param_secondary_identity_front_identity_url}
        ...      					${param_secondary_identity_back_identity_url}
        ...                         ${param_secondary_identity_identity_id_local}
        ...                     }
        ...                 },
        ...                 "additional": {
        ...      				${param_additional_profile_picture_url}
        ...      				${param_additional_national_id_photo_url}
        ...      				${param_additional_tax_id_card_photo_url}
        ...                 }
        ...          }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
                create session          api        ${api_gateway_host}
    ${response}             patch request
        ...     api
        ...     ${agent_update_agent_profiles_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
        ...     ${NONE}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API get agent profiles
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_agent_profiles_path}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API reset password
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${agent_id}=    convert to string   ${arg_dic.agent_id}
    ${identity_id}=    convert to string   ${arg_dic.identity_id}
    ${reset_password_path}=    replace string      ${reset_password_path}      {agent_id}        ${agent_id}
    ${reset_password_path}=    replace string      ${reset_password_path}      {identity_id}        ${identity_id}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${reset_password_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API agent resets password
    [Arguments]     ${arg_dic}
    ${param_username}=  [Common] - Create string param     username        ${arg_dic.username}
    ${param_new_password}=  [Common] - Create string param     new_password        ${arg_dic.new_password}
    ${param_user_id}=  [Common] - Create int param     user_id        ${arg_dic.user_id}
    ${param_user_type_id}=  [Common] - Create string param     id        ${arg_dic.user_type_id}
    ${param_user_type_name}=  [Common] - Create string param     name        ${arg_dic.user_type_name}
    ${param_otp_reference_id}=  [Common] - Create string param     otp_reference_id        ${arg_dic.otp_reference_id}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_username}
        ...		${param_new_password}
        ...		${param_user_id}
        ...     "user_type":{
        ...         ${param_user_type_id}
        ...         ${param_user_type_name}
        ...     },
        ...		${param_otp_reference_id}
        ...	}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${agent_reset_password_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API change password
    [Arguments]     ${arg_dic}
    ${correlation_id}       generate random string  20  [UPPER]
    ${param_username}		[Common] - Create int param		username		${arg_dic.username}
    ${param_password}		[Common] - Create string param		password		${arg_dic.password}
    ${param_old_password}		[Common] - Create string param		old_password		${arg_dic.old_password}
    ${param_new_password}		[Common] - Create string param		new_password		${arg_dic.new_password}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...          {
        ...                 "credential_identity": {
		...                         ${param_username}
		...                         ${param_password}
	    ...                  },
	    ...                  ${param_old_password}
	    ...                  ${param_new_password}
        ...          }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
    ${identity_id}=    convert to string   ${arg_dic.identity_id}
    ${update_password_path}=    replace string      ${update_password_path}      {identity_id}        ${identity_id}
                            create session          api     ${api_gateway_host}     disable_warnings=1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${update_password_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all relationship type
    [Arguments]
    ...     ${client_id}
    ...     ${client_secret}
    ...     ${access_token}
    ${header}               create dictionary
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_all_relationship_type_path}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API create relationship
    [Arguments]     ${arg_dic}
    ${param_relationship_type_id}=  [Common] - Create int param     relationship_type_id        ${arg_dic.relationship_type_id}
    ${param_main_user_id}=  [Common] - Create int param     user_id        ${arg_dic.main_user_id}
    ${param_main_user_type_id}=  [Common] - Create int param     id        ${arg_dic.main_user_type_id}
    ${param_main_user_type_name}=  [Common] - Create string param     name        ${arg_dic.main_user_type_name}
    ${param_sub_user_id}=  [Common] - Create int param     user_id        ${arg_dic.sub_user_id}
    ${param_sub_user_type_id}=  [Common] - Create int param     id        ${arg_dic.sub_user_type_id}
    ${param_sub_user_type_name}=  [Common] - Create string param     name        ${arg_dic.sub_user_type_name}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_relationship_type_id}
        ...		"main_user":{
        ...         ${param_main_user_id}
        ...         "user_type":{
        ...             ${param_main_user_type_id}
        ...             ${param_main_user_type_name}
        ...         }
        ...     },
        ...		"sub_user":{
        ...         ${param_sub_user_id}
        ...         "user_type":{
        ...             ${param_sub_user_type_id}
        ...             ${param_sub_user_type_name}
        ...         }
        ...     }
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_agent_relationship_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete relationship
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${relationship_id}=    convert to string   ${arg_dic.relationship_id}
    ${delete_agent_relationship_path}=    replace string      ${delete_agent_relationship_path}      {relationship_id}        ${relationship_id}   1
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${delete_agent_relationship_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete agent
    [Arguments]
    ... 	${access_token}
    ... 	${agent_id}
	... 	${client_id}
	... 	${client_secret}
    ${header}               create dictionary
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${access_token}
    ${agent_id}=    convert to string   ${agent_id}
    ${path}=    replace string      ${delete_agent_path}      {agent_id}        ${agent_id}   1
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API create agent
    [Arguments]  ${arg_dic}

    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_is_testing_account}		[Common] - Create boolean param in dic    ${arg_dic}		is_testing_account
    ${param_is_system_account}		[Common] - Create boolean param in dic    ${arg_dic}		is_system_account
    ${param_acquisition_source}		[Common] - Create string param in dic    ${arg_dic}		acquisition_source
    ${param_referrer_user_type_id}		[Common] - Create int param in dic    ${arg_dic}		id
    ${param_referrer_user_type_name}		[Common] - Create string param in dic    ${arg_dic}		name
    ${param_referrer_user_id}		[Common] - Create int param in dic    ${arg_dic}		referrer_user_id
    ${param_agent_type_id}		[Common] - Create int param in dic    ${arg_dic}		agent_type_id
    ${param_unique_reference}		[Common] - Create string param in dic    ${arg_dic}		unique_reference
    ${param_mm_card_type_id}		[Common] - Create int param in dic    ${arg_dic}		mm_card_type_id
    ${param_mm_card_level_id}		[Common] - Create int param in dic    ${arg_dic}		mm_card_level_id
    ${param_mm_factory_card_number}		[Common] - Create string param in dic    ${arg_dic}		mm_factory_card_number
    ${param_model_type}		[Common] - Create string param in dic    ${arg_dic}		model_type
    ${param_is_require_otp}		[Common] - Create boolean param in dic    ${arg_dic}		is_require_otp
    ${param_agent_classification_id}		[Common] - Create int param in dic    ${arg_dic}		agent_classification_id
    ${param_tin_number}		[Common] - Create string param in dic    ${arg_dic}		tin_number
    ${param_title}		[Common] - Create string param in dic    ${arg_dic}		title
    ${param_first_name}		[Common] - Create string param in dic    ${arg_dic}		first_name
    ${param_middle_name}		[Common] - Create string param in dic    ${arg_dic}		middle_name
    ${param_last_name}		[Common] - Create string param in dic    ${arg_dic}		last_name
    ${param_suffix}		[Common] - Create string param in dic    ${arg_dic}		suffix
    ${param_date_of_birth}		[Common] - Create string param in dic    ${arg_dic}		date_of_birth
    ${param_place_of_birth}		[Common] - Create string param in dic    ${arg_dic}		place_of_birth
    ${param_gender}		[Common] - Create string param in dic    ${arg_dic}		gender
    ${param_ethnicity}		[Common] - Create string param in dic    ${arg_dic}		ethnicity
    ${param_nationality}		[Common] - Create string param in dic    ${arg_dic}		nationality
    ${param_occupation}		[Common] - Create string param in dic    ${arg_dic}		occupation
    ${param_occupation_title}		[Common] - Create string param in dic    ${arg_dic}		occupation_title
    ${param_township_code}		[Common] - Create string param in dic    ${arg_dic}		township_code
    ${param_township_name}		[Common] - Create string param in dic    ${arg_dic}		township_name
    ${param_national_id_number}		[Common] - Create string param in dic    ${arg_dic}		national_id_number
    ${param_mother_name}		[Common] - Create string param in dic    ${arg_dic}		mother_name
    ${param_email}		[Common] - Create string param in dic    ${arg_dic}		email
    ${param_primary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}		primary_mobile_number
    ${param_secondary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}		secondary_mobile_number
    ${param_tertiary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}		tertiary_mobile_number
    ${param_current_address_citizen_association}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       citizen_association
    ${param_current_address_neighbourhood_association}      [Common] - Create string param of object in dic    ${arg_dic}    current_address       neighbourhood_association
    ${param_current_address_address}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       address
    ${param_current_address_commune}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       commune
    ${param_current_address_district}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       district
    ${param_current_address_city}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       city
    ${param_current_address_province}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       province
    ${param_current_address_postal_code}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       postal_code
    ${param_current_address_country}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       country
    ${param_current_address_landmark}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       landmark
    ${param_current_address_longitude}      [Common] - Create string param of object in dic    ${arg_dic}    current_address       longitude
    ${param_current_address_latitude}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       latitude
    ${param_permanent_address_citizen_association}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       citizen_association
    ${param_permanent_address_neighbourhood_association}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       neighbourhood_association
    ${param_permanent_address_address}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       address
    ${param_permanent_address_commune}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       commune
    ${param_permanent_address_district}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       district
    ${param_permanent_address_city}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       city
    ${param_permanent_address_province}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       province
    ${param_permanent_address_postal_code}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       postal_code
    ${param_permanent_address_country}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       country
    ${param_permanent_address_landmark}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       landmark
    ${param_permanent_address_longitude}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       longitude
    ${param_permanent_address_latitude}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       latitude
    ${param_bank_name}		[Common] - Create string param in dic    ${arg_dic}		name
    ${param_bank_account_status}		[Common] - Create int param in dic    ${arg_dic}		account_status
    ${param_bank_account_name}		[Common] - Create string param in dic    ${arg_dic}		account_name
    ${param_bank_account_number}		[Common] - Create string param in dic    ${arg_dic}		account_number
    ${param_bank_branch_area}		[Common] - Create string param in dic    ${arg_dic}		branch_area
    ${param_bank_branch_city}		[Common] - Create string param in dic    ${arg_dic}		branch_city
    ${param_bank_register_date}		[Common] - Create string param in dic    ${arg_dic}		register_date
    ${param_bank_register_source}		[Common] - Create string param in dic    ${arg_dic}		register_source
    ${param_bank_is_verified}		[Common] - Create boolean param in dic    ${arg_dic}		is_verified
    ${param_bank_end_date}		[Common] - Create string param in dic    ${arg_dic}		end_date
    ${param_contract_type}		[Common] - Create string param in dic    ${arg_dic}		type
    ${param_contract_number}		[Common] - Create string param in dic    ${arg_dic}		number
    ${param_contract_extension_type}		[Common] - Create string param in dic    ${arg_dic}		extension_type
    ${param_contract_sign_date}		[Common] - Create string param in dic    ${arg_dic}		sign_date
    ${param_contract_issue_date}		[Common] - Create string param in dic    ${arg_dic}		issue_date
    ${param_contract_expired_date}		[Common] - Create string param in dic    ${arg_dic}		expired_date
    ${param_contract_notification_alert}		[Common] - Create string param in dic    ${arg_dic}		notification_alert
    ${param_contract_day_of_period_reconciliation}		[Common] - Create string param in dic    ${arg_dic}		day_of_period_reconciliation
    ${param_contract_release}		[Common] - Create string param in dic    ${arg_dic}		release
    ${param_contract_file_url}		[Common] - Create string param in dic    ${arg_dic}		file_url
    ${param_contract_assessment_information_url}		[Common] - Create string param in dic    ${arg_dic}		assessment_information_url
    ${param_primary_identity_type}      [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       type
    ${param_primary_identity_status}        [Common] - Create int param of object in dic    ${arg_dic}    primary_identity      status
    ${param_primary_identity_identity_id}       [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       identity_id
    ${param_primary_identity_place_of_issue}        [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       place_of_issue
    ${param_primary_identity_issue_date}        [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       issue_date
    ${param_primary_identity_expired_date}      [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       expired_date
    ${param_primary_identity_front_identity_url}        [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       front_identity_url
    ${param_primary_identity_back_identity_url}     [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       back_identity_url
    ${param_primary_identity_identity_id_local}     [Common] - Create string param of object in dic    ${arg_dic}      primary_identity    identity_id_local
    ${param_secondary_identity_type}        [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       type
    ${param_secondary_identity_status}      [Common] - Create int param of object in dic    ${arg_dic}    secondary_identity      status
    ${param_secondary_identity_identity_id}     [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       identity_id
    ${param_secondary_identity_place_of_issue}      [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       place_of_issue
    ${param_secondary_identity_issue_date}      [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       issue_date
    ${param_secondary_identity_expired_date}        [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       expired_date
    ${param_secondary_identity_front_identity_url}      [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       front_identity_url
    ${param_secondary_identity_back_identity_url}       [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       back_identity_url
    ${param_secondary_identity_identity_id_local}       [Common] - Create string param of object in dic    ${arg_dic}      secondary_identity		identity_id_local
    ${param_status_id}		[Common] - Create int param in dic    ${arg_dic}		status_id
    ${param_remark}		[Common] - Create string param in dic    ${arg_dic}		remark
    ${param_verify_by}		[Common] - Create string param in dic    ${arg_dic}		verify_by
    ${param_verify_date}		[Common] - Create string param in dic    ${arg_dic}		verify_date
    ${param_risk_level}		[Common] - Create string param in dic    ${arg_dic}		risk_level
    ${param_additional_acquiring_sales_executive_id}		[Common] - Create string param in dic    ${arg_dic}		acquiring_sale_executive_id
    ${param_additional_acquiring_sales_executive_name}		[Common] - Create string param in dic    ${arg_dic}		acquiring_sale_executive_name
    ${param_additional_relationship_manager_id}		[Common] - Create string param in dic    ${arg_dic}		relationship_manager_id
    ${param_additional_relationship_manager_name}		[Common] - Create string param in dic    ${arg_dic}		relationship_manager_name
    ${param_additional_sale_region}		[Common] - Create string param in dic    ${arg_dic}		sale_region
    ${param_additional_commercial_account_manager}		[Common] - Create string param in dic    ${arg_dic}		commercial_account_manager
    ${param_additional_profile_picture_url}		[Common] - Create string param in dic    ${arg_dic}		profile_picture_url
    ${param_additional_national_id_photo_url}		[Common] - Create string param in dic    ${arg_dic}		national_id_photo_url
    ${param_additional_tax_id_card_photo_url}		[Common] - Create string param in dic    ${arg_dic}		tax_id_card_photo_url
    ${param_additional_field_1_name}		[Common] - Create string param in dic    ${arg_dic}		field_1_name
    ${param_additional_field_1_value}		[Common] - Create string param in dic    ${arg_dic}		field_1_value
    ${param_additional_field_2_name}		[Common] - Create string param in dic    ${arg_dic}		field_2_name
    ${param_additional_field_2_value}		[Common] - Create string param in dic    ${arg_dic}		field_2_value
    ${param_additional_field_3_name}		[Common] - Create string param in dic    ${arg_dic}		field_3_name
    ${param_additional_field_3_value}		[Common] - Create string param in dic    ${arg_dic}		field_3_value
    ${param_additional_field_4_name}		[Common] - Create string param in dic    ${arg_dic}		field_4_name
    ${param_additional_field_4_value}		[Common] - Create string param in dic    ${arg_dic}		field_4_value
    ${param_additional_field_5_name}		[Common] - Create string param in dic    ${arg_dic}		field_5_name
    ${param_additional_field_5_value}		[Common] - Create string param in dic    ${arg_dic}		field_5_value
    ${param_additional_supporting_file_1_url}		[Common] - Create string param in dic    ${arg_dic}		supporting_file_1_url
    ${param_additional_supporting_file_2_url}		[Common] - Create string param in dic    ${arg_dic}		supporting_file_2_url
    ${param_additional_supporting_file_3_url}		[Common] - Create string param in dic    ${arg_dic}		supporting_file_3_url
    ${param_additional_supporting_file_4_url}		[Common] - Create string param in dic    ${arg_dic}		supporting_file_4_url
    ${param_additional_supporting_file_5_url}		[Common] - Create string param in dic    ${arg_dic}		supporting_file_5_url
    ${param_identity_identity_type_id}		[Common] - Create int param in dic    ${arg_dic}		identity_type_id
    ${param_identity_username}		[Common] - Create string param in dic    ${arg_dic}		username
    ${param_identity_password}		[Common] - Create string param in dic    ${arg_dic}		password
    ${param_identity_auto_generate_password}		[Common] - Create boolean param in dic    ${arg_dic}		auto_generate_password
    ${param_tin_number_local}		[Common] - Create string param in dic    ${arg_dic}		tin_number_local
    ${param_title_local}            [Common] - Create string param in dic    ${arg_dic}		title_local
    ${param_first_name_local}       [Common] - Create string param in dic    ${arg_dic}		first_name_local
    ${param_middle_name_local}      [Common] - Create string param in dic    ${arg_dic}		middle_name_local
    ${param_last_name_local}        [Common] - Create string param in dic    ${arg_dic}		last_name_local
    ${param_suffix_local}           [Common] - Create string param in dic    ${arg_dic}		suffix_local
    ${param_place_of_birth_local}   [Common] - Create string param in dic    ${arg_dic}		place_of_birth_local
    ${param_gender_local}           [Common] - Create string param in dic    ${arg_dic}		gender_local
    ${param_occupation_local}       [Common] - Create string param in dic    ${arg_dic}		occupation_local
    ${param_occupation_title_local}     [Common] - Create string param in dic    ${arg_dic}		occupation_title_local
    ${param_township_name_local}        [Common] - Create string param in dic    ${arg_dic}		township_name_local
    ${param_national_id_number_local}   [Common] - Create string param in dic    ${arg_dic}		national_id_number_local
    ${param_mother_name_local}          [Common] - Create string param in dic    ${arg_dic}		mother_name_local
    ${param_current_address_address_local}      [Common] - Create string param of object in dic    ${arg_dic}    current_address		address_local
    ${param_current_address_commune_local}      [Common] - Create string param of object in dic    ${arg_dic}    current_address		commune_local
    ${param_current_address_district_local}     [Common] - Create string param of object in dic    ${arg_dic}    current_address		district_local
    ${param_current_address_city_local}         [Common] - Create string param of object in dic    ${arg_dic}    current_address		city_local
    ${param_current_address_province_local}     [Common] - Create string param of object in dic    ${arg_dic}    current_address		province_local
    ${param_current_address_postal_code_local}      [Common] - Create string param of object in dic    ${arg_dic}    current_address		postal_code_local
    ${param_current_address_country_local}      [Common] - Create string param of object in dic    ${arg_dic}    current_address		country_local
    ${param_permanent_address_address_local}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		address_local
    ${param_permanent_address_commune_local}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		commune_local
    ${param_permanent_address_district_local}       [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		district_local
    ${param_permanent_address_city_local}       [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		city_local
    ${param_permanent_address_province_local}       [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		province_local
    ${param_permanent_address_postal_code_local}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		postal_code_local
    ${param_permanent_address_country_local}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		country_local
    ${param_bank_name_local}        [Common] - Create string param in dic    ${arg_dic}		name_local
    ${param_bank_branch_area_local}     [Common] - Create string param in dic    ${arg_dic}		branch_area_local
    ${param_bank_branch_city_local}     [Common] - Create string param in dic    ${arg_dic}		branch_city_local
    ${param_acquiring_sale_executive_name_local}        [Common] - Create string param in dic    ${arg_dic}		acquiring_sale_executive_name_local
    ${param_relationship_manager_name_local}        [Common] - Create string param in dic    ${arg_dic}		relationship_manager_name_local
    ${param_sale_region_local}      [Common] - Create string param in dic    ${arg_dic}		sale_region_local
    ${param_commercial_account_manager_local}       [Common] - Create string param in dic    ${arg_dic}		commercial_account_manager_local
    ${param_star_rating}      [Common] - Create string param in dic    ${arg_dic}		star_rating
    ${param_warning_count}       [Common] - Create string param in dic    ${arg_dic}		warning_count
    ${param_is_sale}		[Common] - Create boolean param in dic    ${arg_dic}		is_sale
    ${param_sale_employee_id}		[Common] - Create string param in dic    ${arg_dic}		employee_id
    ${param_sale_calendar_id}		[Common] - Create string param in dic    ${arg_dic}		calendar_id
    ${param_company_id}		[Common] - Create int param in dic    ${arg_dic}	 company_id
    ${data}              catenate   SEPARATOR=
    ...          {
    ...              "profile": {
    ...                 ${param_is_testing_account}
    ...                 ${param_is_system_account}
    ...                 ${param_acquisition_source}
    ...                 ${param_star_rating}
    ...                 ${param_warning_count}
    ...                 "referrer_user_type": {
    ...                     ${param_referrer_user_type_id}
    ...                     ${param_referrer_user_type_name}
    ...                 },
    ...                 ${param_referrer_user_id}
    ...                 ${param_agent_type_id}
    ...                 ${param_unique_reference}
    ...                 ${param_mm_card_type_id}
    ...                 ${param_mm_card_level_id}
    ...                 ${param_mm_factory_card_number}
    ...                 ${param_model_type}
    ...                 ${param_is_require_otp}
    ...                 ${param_agent_classification_id}
    ...                 ${param_tin_number}
    ...                 ${param_title}
    ...                 ${param_first_name}
    ...                 ${param_middle_name}
    ...                 ${param_last_name}
    ...                 ${param_suffix}
    ...                 ${param_date_of_birth}
    ...                 ${param_place_of_birth}
    ...                 ${param_gender}
    ...                 ${param_ethnicity}
    ...                 ${param_nationality}
    ...                 ${param_occupation}
    ...                 ${param_occupation_title}
    ...                 ${param_township_code}
    ...                 ${param_township_name}
    ...                 ${param_national_id_number}
    ...                 ${param_mother_name}
    ...                 ${param_email}
    ...                 ${param_primary_mobile_number}
    ...                 ${param_secondary_mobile_number}
    ...                 ${param_tertiary_mobile_number}
    ...                 ${param_tin_number_local}
    ...                 ${param_title_local}
    ...                 ${param_first_name_local}
    ...                 ${param_middle_name_local}
    ...                 ${param_last_name_local}
    ...                 ${param_suffix_local}
    ...                 ${param_place_of_birth_local}
    ...                 ${param_gender_local}
    ...                 ${param_occupation_local}
    ...                 ${param_occupation_title_local}
    ...                 ${param_township_name_local}
    ...                 ${param_national_id_number_local}
    ...                 ${param_mother_name_local}
    ...                 ${param_company_id}
    ...                 "address": {
    ...                     "current_address": {
    ...      					${param_current_address_citizen_association}
    ...      					${param_current_address_neighbourhood_association}
    ...      					${param_current_address_address}
    ...      					${param_current_address_commune}
    ...      					${param_current_address_district}
    ...      					${param_current_address_city}
    ...      					${param_current_address_province}
    ...      					${param_current_address_postal_code}
    ...      					${param_current_address_country}
    ...      					${param_current_address_landmark}
    ...      					${param_current_address_longitude}
    ...      					${param_current_address_latitude}
    ...                         ${param_current_address_address_local}
    ...                         ${param_current_address_commune_local}
    ...                         ${param_current_address_district_local}
    ...                         ${param_current_address_city_local}
    ...                         ${param_current_address_province_local}
    ...                         ${param_current_address_postal_code_local}
    ...                         ${param_current_address_country_local}
    ...                     },
    ...                     "permanent_address": {
    ...      					${param_permanent_address_citizen_association}
    ...      					${param_permanent_address_neighbourhood_association}
    ...      					${param_permanent_address_address}
    ...      					${param_permanent_address_commune}
    ...      					${param_permanent_address_district}
    ...      					${param_permanent_address_city}
    ...      					${param_permanent_address_province}
    ...      					${param_permanent_address_postal_code}
    ...      					${param_permanent_address_country}
    ...      					${param_permanent_address_landmark}
    ...      					${param_permanent_address_longitude}
    ...      					${param_permanent_address_latitude}
    ...                         ${param_permanent_address_address_local}
    ...                         ${param_permanent_address_commune_local}
    ...                         ${param_permanent_address_district_local}
    ...                         ${param_permanent_address_city_local}
    ...                         ${param_permanent_address_province_local}
    ...                         ${param_permanent_address_postal_code_local}
    ...                         ${param_permanent_address_country_local}
    ...                     }
    ...                 },
    ...                 "bank":{
    ...      				${param_bank_name}
    ...      				${param_bank_account_status}
    ...      				${param_bank_account_name}
    ...      				${param_bank_account_number}
    ...      				${param_bank_branch_area}
    ...      				${param_bank_branch_city}
    ...      				${param_bank_register_date}
    ...      				${param_bank_register_source}
    ...      				${param_bank_is_verified}
    ...      				${param_bank_end_date}
    ...                     ${param_bank_name_local}
    ...                     ${param_bank_branch_area_local}
    ...                     ${param_bank_branch_city_local}
    ...                 },
    ...                 "contract": {
    ...      				${param_contract_type}
    ...      				${param_contract_number}
    ...      				${param_contract_extension_type}
    ...      				${param_contract_sign_date}
    ...      				${param_contract_issue_date}
    ...      				${param_contract_expired_date}
    ...      				${param_contract_notification_alert}
    ...      				${param_contract_day_of_period_reconciliation}
    ...      				${param_contract_release}
    ...      				${param_contract_file_url}
    ...      				${param_contract_assessment_information_url}
    ...                  },
    ...                 "accreditation": {
    ...                     "primary_identity": {
    ...      					${param_primary_identity_type}
    ...      					${param_primary_identity_status}
    ...      					${param_primary_identity_identity_id}
    ...      					${param_primary_identity_place_of_issue}
    ...      					${param_primary_identity_issue_date}
    ...      					${param_primary_identity_expired_date}
    ...      					${param_primary_identity_front_identity_url}
    ...      					${param_primary_identity_back_identity_url}
    ...                         ${param_primary_identity_identity_id_local}
    ...                     },
    ...                     "secondary_identity": {
    ...      					${param_secondary_identity_type}
    ...      					${param_secondary_identity_status}
    ...      					${param_secondary_identity_identity_id}
    ...      					${param_secondary_identity_place_of_issue}
    ...      					${param_secondary_identity_issue_date}
    ...      					${param_secondary_identity_expired_date}
    ...      					${param_secondary_identity_front_identity_url}
    ...      					${param_secondary_identity_back_identity_url}
    ...                         ${param_secondary_identity_identity_id_local}
    ...                     },
    ...      				${param_status_id}
    ...      				${param_remark}
    ...      				${param_verify_by}
    ...      				${param_verify_date}
    ...      				${param_risk_level}
    ...                 },
    ...                 "additional": {
    ...      				${param_additional_acquiring_sales_executive_id}
    ...      				${param_additional_acquiring_sales_executive_name}
    ...      				${param_additional_relationship_manager_id}
    ...      				${param_additional_relationship_manager_name}
    ...      				${param_additional_sale_region}
    ...      				${param_additional_commercial_account_manager}
    ...      				${param_additional_profile_picture_url}
    ...      				${param_additional_national_id_photo_url}
    ...      				${param_additional_tax_id_card_photo_url}
    ...      				${param_additional_field_1_name}
    ...      				${param_additional_field_1_value}
    ...      				${param_additional_field_2_name}
    ...      				${param_additional_field_2_value}
    ...      				${param_additional_field_3_name}
    ...      				${param_additional_field_3_value}
    ...      				${param_additional_field_4_name}
    ...      				${param_additional_field_4_value}
    ...      				${param_additional_field_5_name}
    ...      				${param_additional_field_5_value}
    ...      				${param_additional_supporting_file_1_url}
    ...      				${param_additional_supporting_file_2_url}
    ...      				${param_additional_supporting_file_3_url}
    ...      				${param_additional_supporting_file_4_url}
    ...      				${param_additional_supporting_file_5_url}
    ...                     ${param_acquiring_sale_executive_name_local}
    ...                     ${param_relationship_manager_name_local}
    ...                     ${param_sale_region_local}
    ...                     ${param_commercial_account_manager_local}
    ...                 },
    ...                 ${param_is_sale}
    ...                 "sale": {
    ...                     ${param_sale_employee_id}
    ...                     ${param_sale_calendar_id}
    ...                 }
    ...              },
    ...              "identity": {
    ...      			${param_identity_identity_type_id}
    ...      			${param_identity_username}
    ...      			${param_identity_password}
    ...      			${param_identity_auto_generate_password}
    ...              }
    ...          }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
    create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_agent_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${data}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create agent profiles
    [Arguments]
    ...     ${access_token}
    ...     ${agent_type_id}
    ...     ${unique_reference}
    ...     ${is_testing_account}
    ...     ${is_system_account}
    ...     ${acquisition_source}
    ...     ${referrer_user_type_id}
    ...     ${referrer_user_type_name}
    ...     ${referrer_user_id}
    ...     ${mm_card_type_id}
    ...     ${mm_card_level_id}
    ...     ${mm_factory_card_number}
    ...     ${model_type}
    ...     ${is_require_otp}
    ...     ${agent_classification_id}
    ...     ${tin_number}
    ...     ${title}
    ...     ${first_name}
    ...     ${middle_name}
    ...     ${last_name}
    ...     ${suffix}
    ...     ${date_of_birth}
    ...     ${place_of_birth}
    ...     ${gender}
    ...     ${ethnicity}
    ...     ${nationality}
    ...     ${occupation}
    ...     ${occupation_title}
    ...     ${township_code}
    ...     ${township_name}
    ...     ${national_id_number}
    ...     ${mother_name}
    ...     ${email}
    ...     ${primary_mobile_number}
    ...     ${secondary_mobile_number}
    ...     ${tertiary_mobile_number}
    ...     ${current_address_citizen_association}
    ...     ${current_address_neighbourhood_association}
    ...     ${current_address_address}
    ...     ${current_address_commune}
    ...     ${current_address_district}
    ...     ${current_address_city}
    ...     ${current_address_province}
    ...     ${current_address_postal_code}
    ...     ${current_address_country}
    ...     ${current_address_landmark}
    ...     ${current_address_longitude}
    ...     ${current_address_latitude}
    ...     ${permanent_address_citizen_association}
    ...     ${permanent_address_neighbourhood_association}
    ...     ${permanent_address_address}
    ...     ${permanent_address_commune}
    ...     ${permanent_address_district}
    ...     ${permanent_address_city}
    ...     ${permanent_address_province}
    ...     ${permanent_address_postal_code}
    ...     ${permanent_address_country}
    ...     ${permanent_address_landmark}
    ...     ${permanent_address_longitude}
    ...     ${permanent_address_latitude}
    ...     ${bank_name}
    ...     ${bank_account_status}
    ...     ${bank_account_name}
    ...     ${bank_account_number}
    ...     ${bank_branch_area}
    ...     ${bank_branch_city}
    ...     ${bank_register_date}
    ...     ${bank_register_source}
    ...     ${bank_is_verified}
    ...     ${bank_end_date}
    ...     ${contract_type}
    ...     ${contract_number}
    ...     ${contract_extension_type}
    ...     ${contract_sign_date}
    ...     ${contract_issue_date}
    ...     ${contract_expired_date}
    ...     ${contract_notification_alert}
    ...     ${contract_day_of_period_reconciliation}
    ...     ${contract_release}
    ...     ${contract_file_url}
    ...     ${contract_assessment_information_url}
    ...     ${primary_identity_type}
    ...     ${primary_identity_status}
    ...     ${primary_identity_identity_id}
    ...     ${primary_identity_place_of_issue}
    ...     ${primary_identity_issue_date}
    ...     ${primary_identity_expired_date}
    ...     ${primary_identity_front_identity_url}
    ...     ${primary_identity_back_identity_url}
    ...     ${secondary_identity_type}
    ...     ${secondary_identity_status}
    ...     ${secondary_identity_identity_id}
    ...     ${secondary_identity_place_of_issue}
    ...     ${secondary_identity_issue_date}
    ...     ${secondary_identity_expired_date}
    ...     ${secondary_identity_front_identity_url}
    ...     ${secondary_identity_back_identity_url}
    ...     ${status_id}
    ...     ${remark}
    ...     ${verify_by}
    ...     ${verify_date}
    ...     ${risk_level}
    ...     ${additional_acquiring_sales_executive_id}
    ...     ${additional_acquiring_sales_executive_name}
    ...     ${additional_relationship_manager_id}
    ...     ${additional_relationship_manager_name}
    ...     ${additional_sale_region}
    ...     ${additional_commercial_account_manager}
    ...     ${additional_profile_picture_url}
    ...     ${additional_national_id_photo_url}
    ...     ${additional_tax_id_card_photo_url}
    ...     ${additional_field_1_name}
    ...     ${additional_field_1_value}
    ...     ${additional_field_2_name}
    ...     ${additional_field_2_value}
    ...     ${additional_field_3_name}
    ...     ${additional_field_3_value}
    ...     ${additional_field_4_name}
    ...     ${additional_field_4_value}
    ...     ${additional_field_5_name}
    ...     ${additional_field_5_value}
    ...     ${additional_supporting_file_1_url}
    ...     ${additional_supporting_file_2_url}
    ...     ${additional_supporting_file_3_url}
    ...     ${additional_supporting_file_4_url}
    ...     ${additional_supporting_file_5_url}
    ...     ${is_sale}
    ...     ${sale_employee_id}
    ...     ${sale_calendar_id}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ...     ${company_id}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${param_agent_type_id}		[Common] - Create int param		agent_type_id		${agent_type_id}
    ${param_company_id}		[Common] - Create int param		company_id		${company_id}
    ${param_unique_reference}		[Common] - Create string param		unique_reference		${unique_reference}
    ${param_is_testing_account}		[Common] - Create boolean param		is_testing_account		${is_testing_account}
    ${param_is_system_account}		[Common] - Create boolean param		is_system_account		${is_system_account}
    ${param_acquisition_source}		[Common] - Create string param		acquisition_source		${acquisition_source}
    ${param_referrer_user_type_id}		[Common] - Create int param		id		${referrer_user_type_id}
    ${param_referrer_user_type_name}		[Common] - Create string param		name		${referrer_user_type_name}
    ${param_referrer_user_id}		[Common] - Create int param		referrer_user_id		${referrer_user_id}
    ${param_mm_card_type_id}		[Common] - Create int param		mm_card_type_id		${mm_card_type_id}
    ${param_mm_card_level_id}		[Common] - Create int param		mm_card_level_id		${mm_card_level_id}
    ${param_mm_factory_card_number}		[Common] - Create string param		mm_factory_card_number		${mm_factory_card_number}
    ${param_model_type}		[Common] - Create string param		model_type		${model_type}
    ${param_is_require_otp}		[Common] - Create boolean param		is_require_otp		${is_require_otp}
    ${param_agent_classification_id}		[Common] - Create int param		agent_classification_id		${agent_classification_id}
    ${param_tin_number}		[Common] - Create string param		tin_number		${tin_number}
    ${param_title}		[Common] - Create string param		title		${title}
    ${param_first_name}		[Common] - Create string param		first_name		${first_name}
    ${param_middle_name}		[Common] - Create string param		middle_name		${middle_name}
    ${param_last_name}		[Common] - Create string param		last_name		${last_name}
    ${param_suffix}		[Common] - Create string param		suffix		${suffix}
    ${param_date_of_birth}		[Common] - Create string param		date_of_birth		${date_of_birth}
    ${param_place_of_birth}		[Common] - Create string param		place_of_birth		${place_of_birth}
    ${param_gender}		[Common] - Create string param		gender		${gender}
    ${param_ethnicity}		[Common] - Create string param		ethnicity		${ethnicity}
    ${param_nationality}		[Common] - Create string param		nationality		${nationality}
    ${param_occupation}		[Common] - Create string param		occupation		${occupation}
    ${param_occupation_title}		[Common] - Create string param		occupation_title		${occupation_title}
    ${param_township_code}		[Common] - Create string param		township_code		${township_code}
    ${param_township_name}		[Common] - Create string param		township_name		${township_name}
    ${param_national_id_number}		[Common] - Create string param		national_id_number		${national_id_number}
    ${param_mother_name}		[Common] - Create string param		mother_name		${mother_name}
    ${param_email}		[Common] - Create string param		email		${email}
    ${param_primary_mobile_number}		[Common] - Create string param		primary_mobile_number		${primary_mobile_number}
    ${param_secondary_mobile_number}		[Common] - Create string param		secondary_mobile_number		${secondary_mobile_number}
    ${param_tertiary_mobile_number}		[Common] - Create string param		tertiary_mobile_number		${tertiary_mobile_number}
    ${param_current_address_citizen_association}		[Common] - Create string param		citizen_association		${current_address_citizen_association}
    ${param_current_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${current_address_neighbourhood_association}
    ${param_current_address_address}		[Common] - Create string param		address		${current_address_address}
    ${param_current_address_commune}		[Common] - Create string param		commune		${current_address_commune}
    ${param_current_address_district}		[Common] - Create string param		district		${current_address_district}
    ${param_current_address_city}		[Common] - Create string param		city		${current_address_city}
    ${param_current_address_province}		[Common] - Create string param		province		${current_address_province}
    ${param_current_address_postal_code}		[Common] - Create string param		postal_code		${current_address_postal_code}
    ${param_current_address_country}		[Common] - Create string param		country		${current_address_country}
    ${param_current_address_landmark}		[Common] - Create string param		landmark		${current_address_landmark}
    ${param_current_address_longitude}		[Common] - Create string param		longitude		${current_address_longitude}
    ${param_current_address_latitude}		[Common] - Create string param		latitude		${current_address_latitude}
    ${param_permanent_address_citizen_association}		[Common] - Create string param		citizen_association		${permanent_address_citizen_association}
    ${param_permanent_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${permanent_address_neighbourhood_association}
    ${param_permanent_address_address}		[Common] - Create string param		address		${permanent_address_address}
    ${param_permanent_address_commune}		[Common] - Create string param		commune		${permanent_address_commune}
    ${param_permanent_address_district}		[Common] - Create string param		district		${permanent_address_district}
    ${param_permanent_address_city}		[Common] - Create string param		city		${permanent_address_city}
    ${param_permanent_address_province}		[Common] - Create string param		province		${permanent_address_province}
    ${param_permanent_address_postal_code}		[Common] - Create string param		postal_code		${permanent_address_postal_code}
    ${param_permanent_address_country}		[Common] - Create string param		country		${permanent_address_country}
    ${param_permanent_address_landmark}		[Common] - Create string param		landmark		${permanent_address_landmark}
    ${param_permanent_address_longitude}		[Common] - Create string param		longitude		${permanent_address_longitude}
    ${param_permanent_address_latitude}		[Common] - Create string param		latitude		${permanent_address_latitude}
    ${param_bank_name}		[Common] - Create string param		name		${bank_name}
    ${param_bank_account_status}		[Common] - Create int param		account_status		${bank_account_status}
    ${param_bank_account_name}		[Common] - Create string param		account_name		${bank_account_name}
    ${param_bank_account_number}		[Common] - Create string param		account_number		${bank_account_number}
    ${param_bank_branch_area}		[Common] - Create string param		branch_area		${bank_branch_area}
    ${param_bank_branch_city}		[Common] - Create string param		branch_city		${bank_branch_city}
    ${param_bank_register_date}		[Common] - Create string param		register_date		${bank_register_date}
    ${param_bank_register_source}		[Common] - Create string param		register_source		${bank_register_source}
    ${param_bank_is_verified}		[Common] - Create boolean param		is_verified		${bank_is_verified}
    ${param_bank_end_date}		[Common] - Create string param		end_date		${bank_end_date}
    ${param_contract_type}		[Common] - Create string param		type		${contract_type}
    ${param_contract_number}		[Common] - Create string param		number		${contract_number}
    ${param_contract_extension_type}		[Common] - Create string param		extension_type		${contract_extension_type}
    ${param_contract_sign_date}		[Common] - Create string param		sign_date		${contract_sign_date}
    ${param_contract_issue_date}		[Common] - Create string param		issue_date		${contract_issue_date}
    ${param_contract_expired_date}		[Common] - Create string param		expired_date		${contract_expired_date}
    ${param_contract_notification_alert}		[Common] - Create string param		notification_alert		${contract_notification_alert}
    ${param_contract_day_of_period_reconciliation}		[Common] - Create string param		day_of_period_reconciliation		${contract_day_of_period_reconciliation}
    ${param_contract_release}		[Common] - Create string param		release		${contract_release}
    ${param_contract_file_url}		[Common] - Create string param		file_url		${contract_file_url}
    ${param_contract_assessment_information_url}		[Common] - Create string param		assessment_information_url		${contract_assessment_information_url}
    ${param_primary_identity_type}		[Common] - Create string param		type		${primary_identity_type}
    ${param_primary_identity_status}		[Common] - Create int param		status		${primary_identity_status}
    ${param_primary_identity_identity_id}		[Common] - Create string param		identity_id		${primary_identity_identity_id}
    ${param_primary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${primary_identity_place_of_issue}
    ${param_primary_identity_issue_date}		[Common] - Create string param		issue_date		${primary_identity_issue_date}
    ${param_primary_identity_expired_date}		[Common] - Create string param		expired_date		${primary_identity_expired_date}
    ${param_primary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${primary_identity_front_identity_url}
    ${param_primary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${primary_identity_back_identity_url}
    ${param_secondary_identity_type}		[Common] - Create string param		type		${secondary_identity_type}
    ${param_secondary_identity_status}		[Common] - Create int param		status		${secondary_identity_status}
    ${param_secondary_identity_identity_id}		[Common] - Create string param		identity_id		${secondary_identity_identity_id}
    ${param_secondary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${secondary_identity_place_of_issue}
    ${param_secondary_identity_issue_date}		[Common] - Create string param		issue_date		${secondary_identity_issue_date}
    ${param_secondary_identity_expired_date}		[Common] - Create string param		expired_date		${secondary_identity_expired_date}
    ${param_secondary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${secondary_identity_front_identity_url}
    ${param_secondary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${secondary_identity_back_identity_url}
    ${param_status_id}		[Common] - Create int param		status_id		${status_id}
    ${param_remark}		[Common] - Create string param		remark		${remark}
    ${param_verify_by}		[Common] - Create string param		verify_by		${verify_by}
    ${param_verify_date}		[Common] - Create string param		verify_date		${verify_date}
    ${param_risk_level}		[Common] - Create string param		risk_level		${risk_level}
    ${param_additional_acquiring_sales_executive_id}		[Common] - Create int param		acquiring_sales_executive_id		${additional_acquiring_sales_executive_id}
    ${param_additional_acquiring_sales_executive_name}		[Common] - Create string param		acquiring_sales_executive_name		${additional_acquiring_sales_executive_name}
    ${param_additional_relationship_manager_id}		[Common] - Create int param		relationship_manager_id		${additional_relationship_manager_id}
    ${param_additional_relationship_manager_name}		[Common] - Create string param		relationship_manager_name		${additional_relationship_manager_name}
    ${param_additional_sale_region}		[Common] - Create string param		sale_region		${additional_sale_region}
    ${param_additional_commercial_account_manager}		[Common] - Create string param		commercial_account_manager		${additional_commercial_account_manager}
    ${param_additional_profile_picture_url}		[Common] - Create string param		profile_picture_url		${additional_profile_picture_url}
    ${param_additional_national_id_photo_url}		[Common] - Create string param		national_id_photo_url		${additional_national_id_photo_url}
    ${param_additional_tax_id_card_photo_url}		[Common] - Create string param		tax_id_card_photo_url		${additional_tax_id_card_photo_url}
    ${param_additional_field_1_name}		[Common] - Create string param		field_1_name		${additional_field_1_name}
    ${param_additional_field_1_value}		[Common] - Create string param		field_1_value		${additional_field_1_value}
    ${param_additional_field_2_name}		[Common] - Create string param		field_2_name		${additional_field_2_name}
    ${param_additional_field_2_value}		[Common] - Create string param		field_2_value		${additional_field_2_value}
    ${param_additional_field_3_name}		[Common] - Create string param		field_3_name		${additional_field_3_name}
    ${param_additional_field_3_value}		[Common] - Create string param		field_3_value		${additional_field_3_value}
    ${param_additional_field_4_name}		[Common] - Create string param		field_4_name		${additional_field_4_name}
    ${param_additional_field_4_value}		[Common] - Create string param		field_4_value		${additional_field_4_value}
    ${param_additional_field_5_name}		[Common] - Create string param		field_5_name		${additional_field_5_name}
    ${param_additional_field_5_value}		[Common] - Create string param		field_5_value		${additional_field_5_value}
    ${param_additional_supporting_file_1_url}		[Common] - Create string param		supporting_file_1_url		${additional_supporting_file_1_url}
    ${param_additional_supporting_file_2_url}		[Common] - Create string param		supporting_file_2_url		${additional_supporting_file_2_url}
    ${param_additional_supporting_file_3_url}		[Common] - Create string param		supporting_file_3_url		${additional_supporting_file_3_url}
    ${param_additional_supporting_file_4_url}		[Common] - Create string param		supporting_file_4_url		${additional_supporting_file_4_url}
    ${param_additional_supporting_file_5_url}		[Common] - Create string param		supporting_file_5_url		${additional_supporting_file_5_url}
    ${param_is_sale}		[Common] - Create boolean param		is_sale		${is_sale}
    ${param_sale_employee_id}		[Common] - Create string param		employee_id		${sale_employee_id}
    ${param_sale_calendar_id}		[Common] - Create string param		calendar_id		${sale_calendar_id}

    ${data}              catenate   SEPARATOR=
    ...          {
    ...                 ${param_is_testing_account}
    ...                 ${param_is_system_account}
    ...                 ${param_acquisition_source}
    ...                 "referrer_user_type": {
    ...                     ${param_referrer_user_type_id}
    ...                     ${param_referrer_user_type_name}
    ...                 },
    ...                 ${param_referrer_user_id}
    ...                 ${param_agent_type_id}
    ...                 ${param_company_id}
    ...                 ${param_unique_reference}
    ...                 ${param_mm_card_type_id}
    ...                 ${param_mm_card_level_id}
    ...                 ${param_mm_factory_card_number}
    ...                 ${param_model_type}
    ...                 ${param_is_require_otp}
    ...                 ${param_agent_classification_id}
    ...                 ${param_tin_number}
    ...                 ${param_title}
    ...                 ${param_first_name}
    ...                 ${param_middle_name}
    ...                 ${param_last_name}
    ...                 ${param_suffix}
    ...                 ${param_date_of_birth}
    ...                 ${param_place_of_birth}
    ...                 ${param_gender}
    ...                 ${param_ethnicity}
    ...                 ${param_nationality}
    ...                 ${param_occupation}
    ...                 ${param_occupation_title}
    ...                 ${param_township_code}
    ...                 ${param_township_name}
    ...                 ${param_national_id_number}
    ...                 ${param_mother_name}
    ...                 ${param_email}
    ...                 ${param_primary_mobile_number}
    ...                 ${param_secondary_mobile_number}
    ...                 ${param_tertiary_mobile_number}
    ...                 "address": {
    ...                     "current_address": {
    ...      					${param_current_address_citizen_association}
    ...      					${param_current_address_neighbourhood_association}
    ...      					${param_current_address_address}
    ...      					${param_current_address_commune}
    ...      					${param_current_address_district}
    ...      					${param_current_address_city}
    ...      					${param_current_address_province}
    ...      					${param_current_address_postal_code}
    ...      					${param_current_address_country}
    ...      					${param_current_address_landmark}
    ...      					${param_current_address_longitude}
    ...      					${param_current_address_latitude}
    ...                     },
    ...                     "permanent_address": {
    ...      					${param_permanent_address_citizen_association}
    ...      					${param_permanent_address_neighbourhood_association}
    ...      					${param_permanent_address_address}
    ...      					${param_permanent_address_commune}
    ...      					${param_permanent_address_district}
    ...      					${param_permanent_address_city}
    ...      					${param_permanent_address_province}
    ...      					${param_permanent_address_postal_code}
    ...      					${param_permanent_address_country}
    ...      					${param_permanent_address_landmark}
    ...      					${param_permanent_address_longitude}
    ...      					${param_permanent_address_latitude}
    ...                     }
    ...                 },
    ...                 "bank":{
    ...      				${param_bank_name}
    ...      				${param_bank_account_status}
    ...      				${param_bank_account_name}
    ...      				${param_bank_account_number}
    ...      				${param_bank_branch_area}
    ...      				${param_bank_branch_city}
    ...      				${param_bank_register_date}
    ...      				${param_bank_register_source}
    ...      				${param_bank_is_verified}
    ...      				${param_bank_end_date}
    ...                 },
    ...                 "contract": {
    ...      				${param_contract_type}
    ...      				${param_contract_number}
    ...      				${param_contract_extension_type}
    ...      				${param_contract_sign_date}
    ...      				${param_contract_issue_date}
    ...      				${param_contract_expired_date}
    ...      				${param_contract_notification_alert}
    ...      				${param_contract_day_of_period_reconciliation}
    ...      				${param_contract_release}
    ...      				${param_contract_file_url}
    ...      				${param_contract_assessment_information_url}
    ...                  },
    ...                 "accreditation": {
    ...                     "primary_identity": {
    ...      					${param_primary_identity_type}
    ...      					${param_primary_identity_status}
    ...      					${param_primary_identity_identity_id}
    ...      					${param_primary_identity_place_of_issue}
    ...      					${param_primary_identity_issue_date}
    ...      					${param_primary_identity_expired_date}
    ...      					${param_primary_identity_front_identity_url}
    ...      					${param_primary_identity_back_identity_url}
    ...                     },
    ...                     "secondary_identity": {
    ...      					${param_secondary_identity_type}
    ...      					${param_secondary_identity_status}
    ...      					${param_secondary_identity_identity_id}
    ...      					${param_secondary_identity_place_of_issue}
    ...      					${param_secondary_identity_issue_date}
    ...      					${param_secondary_identity_expired_date}
    ...      					${param_secondary_identity_front_identity_url}
    ...      					${param_secondary_identity_back_identity_url}
    ...                     },
    ...      				${param_status_id}
    ...      				${param_remark}
    ...      				${param_verify_by}
    ...      				${param_verify_date}
    ...      				${param_risk_level}
    ...                 },
    ...                 "additional": {
    ...      				${param_additional_acquiring_sales_executive_id}
    ...      				${param_additional_acquiring_sales_executive_name}
    ...      				${param_additional_relationship_manager_id}
    ...      				${param_additional_relationship_manager_name}
    ...      				${param_additional_sale_region}
    ...      				${param_additional_commercial_account_manager}
    ...      				${param_additional_profile_picture_url}
    ...      				${param_additional_national_id_photo_url}
    ...      				${param_additional_tax_id_card_photo_url}
    ...      				${param_additional_field_1_name}
    ...      				${param_additional_field_1_value}
    ...      				${param_additional_field_2_name}
    ...      				${param_additional_field_2_value}
    ...      				${param_additional_field_3_name}
    ...      				${param_additional_field_3_value}
    ...      				${param_additional_field_4_name}
    ...      				${param_additional_field_4_value}
    ...      				${param_additional_field_5_name}
    ...      				${param_additional_field_5_value}
    ...      				${param_additional_supporting_file_1_url}
    ...      				${param_additional_supporting_file_2_url}
    ...      				${param_additional_supporting_file_3_url}
    ...      				${param_additional_supporting_file_4_url}
    ...      				${param_additional_supporting_file_5_url}
    ...                 },
    ...                 ${param_is_sale}
    ...                 "sale": {
    ...                     ${param_sale_employee_id}
    ...                     ${param_sale_calendar_id}
    ...                 }
    ...              }
    ${data}  replace string      ${data}      ,}    }
    create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_agent_profiles_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API add agent identity
    [Arguments]
    ...     ${agent_id}
    ...     ${identity_type_id}
    ...     ${username}
    ...     ${password}
    ...     ${access_token}
    ...     ${client_id}
    ...     ${client_secret}
    ${header}               create dictionary
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${data}              catenate
        ...	{
        ...     "username":"${username}",
        ...     "password":"${password}",
        ...     "identity_type_id":${identity_type_id}
        ...	}
                create session          api        ${api_gateway_host}

    ${agent_id}=        convert to string   ${agent_id}
    ${path}=    replace string      ${add_agent_identity_path}        {agent_id}       ${agent_id}    1
    ${response}             post request
        ...     api
        ...     ${path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API agent adds idenity
    [Arguments]     ${arg_dic}
    ${param_identity_type_id}=  [Common] - Create int param     identity_type_id        ${arg_dic.identity_type_id}
    ${param_username}=  [Common] - Create string param     username        ${arg_dic.username}
    ${param_password}=  [Common] - Create string param     password        ${arg_dic.password}
    ${param_user_id}=  [Common] - Create int param     user_id        ${arg_dic.user_id}
    ${param_user_type_id}=  [Common] - Create string param     id        ${arg_dic.user_type_id}
    ${param_user_type_name}=  [Common] - Create string param     name        ${arg_dic.user_type_name}
    ${param_otp_reference_id}=  [Common] - Create string param     otp_reference_id        ${arg_dic.otp_reference_id}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_identity_type_id}
        ...		${param_username}
        ...		${param_password}
        ...		${param_user_id}
        ...     "user_type":{
        ...         ${param_user_type_id}
        ...         ${param_user_type_name}
        ...     },
        ...		${param_otp_reference_id}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${agent_add_agent_identity_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update agent identity
    [Arguments]     ${arg_dic}
    ${param_username}=  [Common] - Create string param     username        ${arg_dic.username}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_username}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${identity_id}=    convert to string   ${arg_dic.identity_id}
    ${update_agent_identity_path}=    replace string      ${update_agent_identity_path}      {identity_id}        ${identity_id}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${update_agent_identity_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete agent identity
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${identity_id}=    convert to string   ${arg_dic.identity_id}
    ${delete_agent_identity_path}=    replace string      ${delete_agent_identity_path}      {identity_id}        ${identity_id}   1
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${delete_agent_identity_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API get agent identities
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_agent_identity_path}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

#[Old]call create agent api
#    [Arguments]
#    ...     ${agent_type_id}
#    ...     ${header_access_token}
#    ...     ${firstname}
#    ...     ${lastname}
#    ...     ${primary_mobile_number}
#    ...     ${card_id}
#    ...     ${email}
#    ...     ${address}
#    ...     ${unique_reference}
#    ...     ${kyc_status}
#    ...     ${header_client_id}
#    ...     ${header_client_secret}
#
#    ${header}               create dictionary
#        ...     client_id=${header_client_id}
#        ...     client_secret=${header_client_secret}
#        ...     content-type=application/json
#        ...     Authorization=Bearer ${header_access_token}
#
#    ${data}              catenate
#        ...	{
#        ...
#        ...		"agent_type_id":"${agent_type_id}",
#        ...		"firstname":"${firstname}",
#        ...		"lastname":"${lastname}",
#        ...		"primary_mobile_number":"${primary_mobile_number}",
#        ...		"card_id":"${card_id}",
#        ...		"email":"${email}",
#        ...		"address":"${address}",
#        ...		"unique_reference":"${unique_reference}",
#        ...		"kyc_status":${kyc_status}
#        ...	}
#                            create session          api     ${api_gateway_host}     disable_warnings=1
#    ${response}             post request
#        ...     api
#        ...     ${create_agent_profiles_path}
#        ...     ${data}
#        ...     ${NONE}
#        ...     ${header}
#    log     ${data}
#    log         ${response.text}
#    ${dic} =	Create Dictionary   response=${response}
#    [Return]    ${dic}

API active-suspend agent
    [Arguments]
    ...     ${access_token}
    ...     ${is_suspended}
    ...     ${agent_id}
    ...     ${active_suspend_reason}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${access_token}
    ${data}              catenate
        ...	{
        ...     "is_suspended":${is_suspended},
        ...     "active_suspend_reason": "${active_suspend_reason}"
        ...	}
    ${agent_id}=   convert to string       ${agent_id}
    ${active-suspend_agent_path}=    replace string      ${active-suspend_agent_path}       {agent_id}        ${agent_id}
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${active-suspend_agent_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create company type
    [Arguments]     ${arg_dic}
    ${param_name}=  [Common] - Create string param in dic     ${arg_dic}     name
    ${param_description}=  [Common] - Create string param in dic    ${arg_dic}     description
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_name}
        ...		${param_description}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_company_type_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create company represetative profiles
    [Arguments]     ${arg_dic}
    ${param_representative_type_id}=                                        [Common] - Create string param in dic    ${arg_dic}             type_id
    ${param_representative_title}		                                    [Common] - Create string param in dic    ${arg_dic}     		title
    ${param_representative_first_name}		                                [Common] - Create string param in dic    ${arg_dic}     		first_name
    ${param_representative_middle_name}		                                [Common] - Create string param in dic    ${arg_dic}     		middle_name
    ${param_representative_last_name}		                                [Common] - Create string param in dic    ${arg_dic}     		last_name
    ${param_representative_suffix}		                                    [Common] - Create string param in dic    ${arg_dic}     		suffix
    ${param_representative_date_of_birth}		                            [Common] - Create string param in dic    ${arg_dic}     		date_of_birth
    ${param_representative_place_of_birth}		                            [Common] - Create string param in dic    ${arg_dic}     		place_of_birth
    ${param_representative_nationality}		                                [Common] - Create string param in dic    ${arg_dic}     		nationality
    ${param_representative_occupation}		                                [Common] - Create string param in dic    ${arg_dic}     		occupation
    ${param_representative_occupation_title}		                        [Common] - Create string param in dic    ${arg_dic}     		occupation_title
    ${param_representative_national_id_number}		                        [Common] - Create string param in dic    ${arg_dic}     		national_id_number
    ${param_representative_source_of_funds}		                            [Common] - Create string param in dic    ${arg_dic}     		source_of_funds
    ${param_representative_email}		                                    [Common] - Create string param in dic    ${arg_dic}     		email
    ${param_representative_mobile_number}		                            [Common] - Create string param in dic    ${arg_dic}     		mobile_number
    ${param_representative_business_phone_number}		                    [Common] - Create string param in dic    ${arg_dic}     		business_phone_number
    ${param_representative_current_address_citizen_association}		        [Common] - Create string param in dic    ${arg_dic}     		citizen_association
    ${param_representative_current_address_neighbourhood_association}		[Common] - Create string param in dic    ${arg_dic}     		neighbourhood_association
    ${param_representative_current_address_address}		                    [Common] - Create string param in dic    ${arg_dic}     		address
    ${param_representative_current_address_commune}		                    [Common] - Create string param in dic    ${arg_dic}     		commune
    ${param_representative_current_address_district}		                [Common] - Create string param in dic    ${arg_dic}     		district
    ${param_representative_current_address_city}		                    [Common] - Create string param in dic    ${arg_dic}     		city
    ${param_representative_current_address_province}		                [Common] - Create string param in dic    ${arg_dic}     		province
    ${param_representative_current_address_postal_code}		                [Common] - Create string param in dic    ${arg_dic}     		postal_code
    ${param_representative_current_address_country}		                    [Common] - Create string param in dic    ${arg_dic}     		country
    ${param_representative_current_address_landmark}		                [Common] - Create string param in dic    ${arg_dic}     		landmark
    ${param_representative_current_address_longitude}		                [Common] - Create string param in dic    ${arg_dic}     		longitude
    ${param_representative_current_address_latitude}		                [Common] - Create string param in dic    ${arg_dic}     		latitude
    ${param_representative_permanent_address_citizen_association}		    [Common] - Create string param in dic    ${arg_dic}     		citizen_association
    ${param_representative_permanent_address_neighbourhood_association}		[Common] - Create string param in dic    ${arg_dic}     		neighbourhood_association
    ${param_representative_permanent_address_address}		                [Common] - Create string param in dic    ${arg_dic}     		address
    ${param_representative_permanent_address_commune}		                [Common] - Create string param in dic    ${arg_dic}     		commune
    ${param_representative_permanent_address_district}		                [Common] - Create string param in dic    ${arg_dic}     		district
    ${param_representative_permanent_address_city}		                    [Common] - Create string param in dic    ${arg_dic}     		city
    ${param_representative_permanent_address_province}		                [Common] - Create string param in dic    ${arg_dic}     		province
    ${param_representative_permanent_address_postal_code}		            [Common] - Create string param in dic    ${arg_dic}     		postal_code
    ${param_representative_permanent_address_country}		                [Common] - Create string param in dic    ${arg_dic}     		country
    ${param_representative_permanent_address_landmark}		                [Common] - Create string param in dic    ${arg_dic}     		landmark
    ${param_representative_permanent_address_longitude}		                [Common] - Create string param in dic    ${arg_dic}     		longitude
    ${param_representative_permanent_address_latitude}		                [Common] - Create string param in dic    ${arg_dic}     		latitude


    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...          {
        ...                     ${param_representative_type_id}
        ...                     ${param_representative_title}
        ...                     ${param_representative_first_name}
        ...                     ${param_representative_middle_name}
        ...                     ${param_representative_last_name}
        ...                     ${param_representative_suffix}
        ...                     ${param_representative_date_of_birth}
        ...                     ${param_representative_place_of_birth}
        ...                     ${param_representative_nationality}
        ...                     ${param_representative_occupation}
        ...                     ${param_representative_occupation_title}
        ...                     ${param_representative_national_id_number}
        ...                     ${param_representative_source_of_funds}
        ...                     ${param_representative_email}
        ...                     ${param_representative_mobile_number}
        ...                     ${param_representative_business_phone_number}
        ...                     "current_address": {
        ...      					${param_representative_current_address_citizen_association}
        ...      					${param_representative_current_address_neighbourhood_association}
        ...      					${param_representative_current_address_address}
        ...      					${param_representative_current_address_commune}
        ...      					${param_representative_current_address_district}
        ...      					${param_representative_current_address_city}
        ...      					${param_representative_current_address_province}
        ...      					${param_representative_current_address_postal_code}
        ...      					${param_representative_current_address_country}
        ...      					${param_representative_current_address_landmark}
        ...      					${param_representative_current_address_longitude}
        ...      					${param_representative_current_address_latitude}
        ...                     },
        ...                     "permanent_address": {
        ...      					${param_representative_permanent_address_citizen_association}
        ...      					${param_representative_permanent_address_neighbourhood_association}
        ...      					${param_representative_permanent_address_address}
        ...      					${param_representative_permanent_address_commune}
        ...      					${param_representative_permanent_address_district}
        ...      					${param_representative_permanent_address_city}
        ...      					${param_representative_permanent_address_province}
        ...      					${param_representative_permanent_address_postal_code}
        ...      					${param_representative_permanent_address_country}
        ...      					${param_representative_permanent_address_landmark}
        ...      				    ${param_representative_permanent_address_longitude}
        ...      					${param_representative_permanent_address_latitude}
        ...                     },
        ...            }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
    ${companyId}=    convert to string   ${arg_dic.company_id}
    ${create_representative_path}=    replace string      ${create_company_representatives_profile_path}      {company_id}        ${companyId}   1
    REST.post   ${api_gateway_host}/${create_representative_path}
        ...     body=${data}
        ...     headers=${header}
    rest extract

API update company represetative profiles
    [Arguments]     ${arg_dic}
    ${param_representative_type_id}=                                        [Common] - Create string param in dic    ${arg_dic}             type_id
    ${param_representative_title}		                                    [Common] - Create string param in dic    ${arg_dic}     		title
    ${param_representative_first_name}		                                [Common] - Create string param in dic    ${arg_dic}     		first_name
    ${param_representative_middle_name}		                                [Common] - Create string param in dic    ${arg_dic}     		middle_name
    ${param_representative_last_name}		                                [Common] - Create string param in dic    ${arg_dic}     		last_name
    ${param_representative_suffix}		                                    [Common] - Create string param in dic    ${arg_dic}     		suffix
    ${param_representative_date_of_birth}		                            [Common] - Create string param in dic    ${arg_dic}     		date_of_birth
    ${param_representative_place_of_birth}		                            [Common] - Create string param in dic    ${arg_dic}     		place_of_birth
    ${param_representative_nationality}		                                [Common] - Create string param in dic    ${arg_dic}     		nationality
    ${param_representative_occupation}		                                [Common] - Create string param in dic    ${arg_dic}     		occupation
    ${param_representative_occupation_title}		                        [Common] - Create string param in dic    ${arg_dic}     		occupation_title
    ${param_representative_national_id_number}		                        [Common] - Create string param in dic    ${arg_dic}     		national_id_number
    ${param_representative_source_of_funds}		                            [Common] - Create string param in dic    ${arg_dic}     		source_of_funds
    ${param_representative_email}		                                    [Common] - Create string param in dic    ${arg_dic}     		email
    ${param_representative_mobile_number}		                            [Common] - Create string param in dic    ${arg_dic}     		mobile_number
    ${param_representative_business_phone_number}		                    [Common] - Create string param in dic    ${arg_dic}     		business_phone_number
    ${param_representative_current_address_citizen_association}		        [Common] - Create string param in dic    ${arg_dic}     		citizen_association
    ${param_representative_current_address_neighbourhood_association}		[Common] - Create string param in dic    ${arg_dic}     		neighbourhood_association
    ${param_representative_current_address_address}		                    [Common] - Create string param in dic    ${arg_dic}     		address
    ${param_representative_current_address_commune}		                    [Common] - Create string param in dic    ${arg_dic}     		commune
    ${param_representative_current_address_district}		                [Common] - Create string param in dic    ${arg_dic}     		district
    ${param_representative_current_address_city}		                    [Common] - Create string param in dic    ${arg_dic}     		city
    ${param_representative_current_address_province}		                [Common] - Create string param in dic    ${arg_dic}     		province
    ${param_representative_current_address_postal_code}		                [Common] - Create string param in dic    ${arg_dic}     		postal_code
    ${param_representative_current_address_country}		                    [Common] - Create string param in dic    ${arg_dic}     		country
    ${param_representative_current_address_landmark}		                [Common] - Create string param in dic    ${arg_dic}     		landmark
    ${param_representative_current_address_longitude}		                [Common] - Create string param in dic    ${arg_dic}     		longitude
    ${param_representative_current_address_latitude}		                [Common] - Create string param in dic    ${arg_dic}     		latitude
    ${param_representative_permanent_address_citizen_association}		    [Common] - Create string param in dic    ${arg_dic}     		citizen_association
    ${param_representative_permanent_address_neighbourhood_association}		[Common] - Create string param in dic    ${arg_dic}     		neighbourhood_association
    ${param_representative_permanent_address_address}		                [Common] - Create string param in dic    ${arg_dic}     		address
    ${param_representative_permanent_address_commune}		                [Common] - Create string param in dic    ${arg_dic}     		commune
    ${param_representative_permanent_address_district}		                [Common] - Create string param in dic    ${arg_dic}     		district
    ${param_representative_permanent_address_city}		                    [Common] - Create string param in dic    ${arg_dic}     		city
    ${param_representative_permanent_address_province}		                [Common] - Create string param in dic    ${arg_dic}     		province
    ${param_representative_permanent_address_postal_code}		            [Common] - Create string param in dic    ${arg_dic}     		postal_code
    ${param_representative_permanent_address_country}		                [Common] - Create string param in dic    ${arg_dic}     		country
    ${param_representative_permanent_address_landmark}		                [Common] - Create string param in dic    ${arg_dic}     		landmark
    ${param_representative_permanent_address_longitude}		                [Common] - Create string param in dic    ${arg_dic}     		longitude
    ${param_representative_permanent_address_latitude}		                [Common] - Create string param in dic    ${arg_dic}     		latitude


    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...          {
        ...                     ${param_representative_type_id}
        ...                     ${param_representative_title}
        ...                     ${param_representative_first_name}
        ...                     ${param_representative_middle_name}
        ...                     ${param_representative_last_name}
        ...                     ${param_representative_suffix}
        ...                     ${param_representative_date_of_birth}
        ...                     ${param_representative_place_of_birth}
        ...                     ${param_representative_nationality}
        ...                     ${param_representative_occupation}
        ...                     ${param_representative_occupation_title}
        ...                     ${param_representative_national_id_number}
        ...                     ${param_representative_source_of_funds}
        ...                     ${param_representative_email}
        ...                     ${param_representative_mobile_number}
        ...                     ${param_representative_business_phone_number}
        ...                     "current_address": {
        ...      					${param_representative_current_address_citizen_association}
        ...      					${param_representative_current_address_neighbourhood_association}
        ...      					${param_representative_current_address_address}
        ...      					${param_representative_current_address_commune}
        ...      					${param_representative_current_address_district}
        ...      					${param_representative_current_address_city}
        ...      					${param_representative_current_address_province}
        ...      					${param_representative_current_address_postal_code}
        ...      					${param_representative_current_address_country}
        ...      					${param_representative_current_address_landmark}
        ...      					${param_representative_current_address_longitude}
        ...      					${param_representative_current_address_latitude}
        ...                     },
        ...                     "permanent_address": {
        ...      					${param_representative_permanent_address_citizen_association}
        ...      					${param_representative_permanent_address_neighbourhood_association}
        ...      					${param_representative_permanent_address_address}
        ...      					${param_representative_permanent_address_commune}
        ...      					${param_representative_permanent_address_district}
        ...      					${param_representative_permanent_address_city}
        ...      					${param_representative_permanent_address_province}
        ...      					${param_representative_permanent_address_postal_code}
        ...      					${param_representative_permanent_address_country}
        ...      					${param_representative_permanent_address_landmark}
        ...      				    ${param_representative_permanent_address_longitude}
        ...      					${param_representative_permanent_address_latitude}
        ...                     },
        ...            }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
    ${representative_id}=    convert to string   ${arg_dic.representative_id}
    ${update_representative_path}=    replace string      ${update_company_representatives_profile_path}      {representative_id}        ${representative_id}   1
    REST.put   ${api_gateway_host}/${update_representative_path}
        ...     body=${data}
        ...     headers=${header}
    rest extract

API delete company representative profile
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${representative_id}=    convert to string   ${arg_dic.representative_id}
    ${delete_company_representative_path}=    replace string      ${delete_company_representatives_profile_path}      {representative_id}        ${representative_id}   1
                create session          api        ${api_gateway_host}
    REST.delete   ${api_gateway_host}/${delete_company_representative_path}
        ...     headers=${header}
    rest extract

API create company profiles
    [Arguments]     ${arg_dic}
    ${param_company_type_id}=  [Common] - Create string param in dic    ${arg_dic}          company_type_id
    ${param_business_type}=  [Common] - Create string param in dic    ${arg_dic}          business_type
    ${param_business_field}=  [Common] - Create string param in dic    ${arg_dic}          business_field
    ${param_name}=  [Common] - Create string param in dic    ${arg_dic}          name
    ${param_abbreviation}=  [Common] - Create string param in dic    ${arg_dic}          abbreviation
    ${param_tin_number}		[Common] - Create string param in dic    ${arg_dic}     		tin_number
    ${param_risk_level}		[Common] - Create string param in dic    ${arg_dic}     		risk_level
    ${param_is_testing_account}		[Common] - Create boolean param in dic    ${arg_dic}     		is_testing_account
    ${param_address_citizen_association}		[Common] - Create string param in dic    ${arg_dic}     		citizen_association
    ${param_address_neighbourhood_association}		[Common] - Create string param in dic    ${arg_dic}     		neighbourhood_association
    ${param_address_address}		[Common] - Create string param in dic    ${arg_dic}     		address
    ${param_address_commune}		[Common] - Create string param in dic    ${arg_dic}     		commune
    ${param_address_district}		[Common] - Create string param in dic    ${arg_dic}     		district
    ${param_address_city}		[Common] - Create string param in dic    ${arg_dic}     		city
    ${param_address_province}		[Common] - Create string param in dic    ${arg_dic}     		province
    ${param_address_postal_code}		[Common] - Create string param in dic    ${arg_dic}     		postal_code
    ${param_address_country}		[Common] - Create string param in dic    ${arg_dic}     		country
    ${param_address_landmark}		[Common] - Create string param in dic    ${arg_dic}     		landmark
    ${param_address_longitude}		[Common] - Create string param in dic    ${arg_dic}     		longitude
    ${param_address_latitude}		[Common] - Create string param in dic    ${arg_dic}     		latitude
    ${param_mobile_number}		[Common] - Create string param in dic    ${arg_dic}     		mobile_number
    ${param_telephone_number}		[Common] - Create string param in dic    ${arg_dic}     		telephone_number
    ${param_email}=  [Common] - Create string param in dic    ${arg_dic}          email
    ${param_fax_number}=  [Common] - Create string param in dic    ${arg_dic}          fax_number
    ${param_bank_name}		[Common] - Create string param in dic    ${arg_dic}     		bank_name
    ${param_bank_account_status}		[Common] - Create int param in dic    ${arg_dic}     		bank_account_status
    ${param_bank_account_name}		[Common] - Create string param in dic    ${arg_dic}     		bank_account_name
    ${param_bank_account_number}		[Common] - Create string param in dic    ${arg_dic}     		bank_account_number
    ${param_bank_branch_area}		[Common] - Create string param in dic    ${arg_dic}     		bank_branch_area
    ${param_bank_branch_city}		[Common] - Create string param in dic    ${arg_dic}     		bank_branch_city
    ${param_bank_register_date}		[Common] - Create string param in dic    ${arg_dic}     		register_date
    ${param_bank_register_source}		[Common] - Create string param in dic    ${arg_dic}     		register_source
    ${param_bank_is_verified}		[Common] - Create boolean param in dic    ${arg_dic}     		is_verified
    ${param_bank_end_date}		[Common] - Create string param in dic    ${arg_dic}     		end_date
    ${param_bank_source_of_funds}=  [Common] - Create string param in dic    ${arg_dic}          source_of_funds
    ${param_contract_type}		[Common] - Create string param in dic    ${arg_dic}     		type
    ${param_contract_number}		[Common] - Create string param in dic    ${arg_dic}     		number
    ${param_contract_extension_type}		[Common] - Create string param in dic    ${arg_dic}     		extension_type
    ${param_contract_sign_date}		[Common] - Create string param in dic    ${arg_dic}     		sign_date
    ${param_contract_issue_date}		[Common] - Create string param in dic    ${arg_dic}     		issue_date
    ${param_contract_expired_date}		[Common] - Create string param in dic    ${arg_dic}     		expired_date
    ${param_contract_notification_alert}		[Common] - Create string param in dic    ${arg_dic}     		notification_alert
    ${param_contract_day_of_period_reconciliation}		[Common] - Create string param in dic    ${arg_dic}     		day_of_period_reconciliation
    ${param_contract_file_url}		[Common] - Create string param in dic    ${arg_dic}     		file_url
    ${param_contract_assessment_information_url}		[Common] - Create string param in dic    ${arg_dic}     		assessment_information_url
    ${param_owner_tin_number}		[Common] - Create string param in dic    ${arg_dic}     		tin_number
    ${param_owner_title}		[Common] - Create string param in dic    ${arg_dic}     		title
    ${param_owner_first_name}		[Common] - Create string param in dic    ${arg_dic}     		first_name
    ${param_owner_middle_name}		[Common] - Create string param in dic    ${arg_dic}     		middle_name
    ${param_owner_last_name}		[Common] - Create string param in dic    ${arg_dic}     		last_name
    ${param_owner_suffix}		[Common] - Create string param in dic    ${arg_dic}     		suffix
    ${param_owner_date_of_birth}		[Common] - Create string param in dic    ${arg_dic}     		date_of_birth
    ${param_owner_place_of_birth}		[Common] - Create string param in dic    ${arg_dic}     		place_of_birth
    ${param_owner_gender}		[Common] - Create string param in dic    ${arg_dic}     		gender
    ${param_owner_ethnicity}		[Common] - Create string param in dic    ${arg_dic}     		ethnicity
    ${param_owner_nationality}		[Common] - Create string param in dic    ${arg_dic}     		nationality
    ${param_owner_occupation}		[Common] - Create string param in dic    ${arg_dic}     		occupation
    ${param_owner_occupation_title}		[Common] - Create string param in dic    ${arg_dic}     		occupation_title
    ${param_owner_township_code}		[Common] - Create string param in dic    ${arg_dic}     		township_code
    ${param_owner_township_name}		[Common] - Create string param in dic    ${arg_dic}     		township_name
    ${param_owner_national_id_number}		[Common] - Create string param in dic    ${arg_dic}     		national_id_number
    ${param_owner_mother_name}		[Common] - Create string param in dic    ${arg_dic}     		mother_name
    ${param_owner_email}		[Common] - Create string param in dic    ${arg_dic}     		email
    ${param_owner_primary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}     		primary_mobile_number
    ${param_owner_secondary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}     		secondary_mobile_number
    ${param_owner_tertiary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}     		tertiary_mobile_number
    ${param_owner_current_address_citizen_association}		[Common] - Create string param in dic    ${arg_dic}     		citizen_association
    ${param_owner_current_address_neighbourhood_association}		[Common] - Create string param in dic    ${arg_dic}     		neighbourhood_association
    ${param_owner_current_address_address}		[Common] - Create string param in dic    ${arg_dic}     		address
    ${param_owner_current_address_commune}		[Common] - Create string param in dic    ${arg_dic}     		commune
    ${param_owner_current_address_district}		[Common] - Create string param in dic    ${arg_dic}     		district
    ${param_owner_current_address_city}		[Common] - Create string param in dic    ${arg_dic}     		city
    ${param_owner_current_address_province}		[Common] - Create string param in dic    ${arg_dic}     		province
    ${param_owner_current_address_postal_code}		[Common] - Create string param in dic    ${arg_dic}     		postal_code
    ${param_owner_current_address_country}		[Common] - Create string param in dic    ${arg_dic}     		country
    ${param_owner_current_address_landmark}		[Common] - Create string param in dic    ${arg_dic}     		landmark
    ${param_owner_current_address_longitude}		[Common] - Create string param in dic    ${arg_dic}     		longitude
    ${param_owner_current_address_latitude}		[Common] - Create string param in dic    ${arg_dic}     		latitude
    ${param_owner_permanent_address_citizen_association}		[Common] - Create string param in dic    ${arg_dic}     		citizen_association
    ${param_owner_permanent_address_neighbourhood_association}		[Common] - Create string param in dic    ${arg_dic}     		neighbourhood_association
    ${param_owner_permanent_address_address}		[Common] - Create string param in dic    ${arg_dic}     		address
    ${param_owner_permanent_address_commune}		[Common] - Create string param in dic    ${arg_dic}     		commune
    ${param_owner_permanent_address_district}		[Common] - Create string param in dic    ${arg_dic}     		district
    ${param_owner_permanent_address_city}		[Common] - Create string param in dic    ${arg_dic}     		city
    ${param_owner_permanent_address_province}		[Common] - Create string param in dic    ${arg_dic}     		province
    ${param_owner_permanent_address_postal_code}		[Common] - Create string param in dic    ${arg_dic}     		postal_code
    ${param_owner_permanent_address_country}		[Common] - Create string param in dic    ${arg_dic}     		country
    ${param_owner_permanent_address_landmark}		[Common] - Create string param in dic    ${arg_dic}     		landmark
    ${param_owner_permanent_address_longitude}		[Common] - Create string param in dic    ${arg_dic}     		longitude
    ${param_owner_permanent_address_latitude}		[Common] - Create string param in dic    ${arg_dic}     		latitude
    ${param_representative_tin_number}		[Common] - Create string param in dic    ${arg_dic}     		tin_number
    ${param_representative_title}		[Common] - Create string param in dic    ${arg_dic}     		title
    ${param_representative_first_name}		[Common] - Create string param in dic    ${arg_dic}     		first_name
    ${param_representative_middle_name}		[Common] - Create string param in dic    ${arg_dic}     		middle_name
    ${param_representative_last_name}		[Common] - Create string param in dic    ${arg_dic}     		last_name
    ${param_representative_suffix}		[Common] - Create string param in dic    ${arg_dic}     		suffix
    ${param_representative_date_of_birth}		[Common] - Create string param in dic    ${arg_dic}     		date_of_birth
    ${param_representative_place_of_birth}		[Common] - Create string param in dic    ${arg_dic}     		place_of_birth
    ${param_representative_gender}		[Common] - Create string param in dic    ${arg_dic}     		gender
    ${param_representative_ethnicity}		[Common] - Create string param in dic    ${arg_dic}     		ethnicity
    ${param_representative_nationality}		[Common] - Create string param in dic    ${arg_dic}     		nationality
    ${param_representative_occupation}		[Common] - Create string param in dic    ${arg_dic}     		occupation
    ${param_representative_occupation_title}		[Common] - Create string param in dic    ${arg_dic}     		occupation_title
    ${param_representative_township_code}		[Common] - Create string param in dic    ${arg_dic}     		township_code
    ${param_representative_township_name}		[Common] - Create string param in dic    ${arg_dic}     		township_name
    ${param_representative_national_id_number}		[Common] - Create string param in dic    ${arg_dic}     		national_id_number
    ${param_representative_mother_name}		[Common] - Create string param in dic    ${arg_dic}     		mother_name
    ${param_representative_email}		[Common] - Create string param in dic    ${arg_dic}     		email
    ${param_representative_primary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}     		primary_mobile_number
    ${param_representative_secondary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}     		secondary_mobile_number
    ${param_representative_tertiary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}     		tertiary_mobile_number
    ${param_representative_current_address_citizen_association}		[Common] - Create string param in dic    ${arg_dic}     		citizen_association
    ${param_representative_current_address_neighbourhood_association}		[Common] - Create string param in dic    ${arg_dic}     		neighbourhood_association
    ${param_representative_current_address_address}		[Common] - Create string param in dic    ${arg_dic}     		address
    ${param_representative_current_address_commune}		[Common] - Create string param in dic    ${arg_dic}     		commune
    ${param_representative_current_address_district}		[Common] - Create string param in dic    ${arg_dic}     		district
    ${param_representative_current_address_city}		[Common] - Create string param in dic    ${arg_dic}     		city
    ${param_representative_current_address_province}		[Common] - Create string param in dic    ${arg_dic}     		province
    ${param_representative_current_address_postal_code}		[Common] - Create string param in dic    ${arg_dic}     		postal_code
    ${param_representative_current_address_country}		[Common] - Create string param in dic    ${arg_dic}     		country
    ${param_representative_current_address_landmark}		[Common] - Create string param in dic    ${arg_dic}     		landmark
    ${param_representative_current_address_longitude}		[Common] - Create string param in dic    ${arg_dic}     		longitude
    ${param_representative_current_address_latitude}		[Common] - Create string param in dic    ${arg_dic}     		latitude
    ${param_representative_permanent_address_citizen_association}		[Common] - Create string param in dic    ${arg_dic}     		citizen_association
    ${param_representative_permanent_address_neighbourhood_association}		[Common] - Create string param in dic    ${arg_dic}     		neighbourhood_association
    ${param_representative_permanent_address_address}		[Common] - Create string param in dic    ${arg_dic}     		address
    ${param_representative_permanent_address_commune}		[Common] - Create string param in dic    ${arg_dic}     		commune
    ${param_representative_permanent_address_district}		[Common] - Create string param in dic    ${arg_dic}     		district
    ${param_representative_permanent_address_city}		[Common] - Create string param in dic    ${arg_dic}     		city
    ${param_representative_permanent_address_province}		[Common] - Create string param in dic    ${arg_dic}     		province
    ${param_representative_permanent_address_postal_code}		[Common] - Create string param in dic    ${arg_dic}     		postal_code
    ${param_representative_permanent_address_country}		[Common] - Create string param in dic    ${arg_dic}     		country
    ${param_representative_permanent_address_landmark}		[Common] - Create string param in dic    ${arg_dic}     		landmark
    ${param_representative_permanent_address_longitude}		[Common] - Create string param in dic    ${arg_dic}     		longitude
    ${param_representative_permanent_address_latitude}		[Common] - Create string param in dic    ${arg_dic}     		latitude
    ${param_representative_primary_identity_type}		[Common] - Create string param in dic    ${arg_dic}     		type
    ${param_representative_primary_identity_status}		[Common] - Create int param in dic    ${arg_dic}     		status
    ${param_representative_primary_identity_identity_id}		[Common] - Create string param in dic    ${arg_dic}     		identity_id
    ${param_representative_primary_identity_place_of_issue}		[Common] - Create string param in dic    ${arg_dic}     		place_of_issue
    ${param_representative_primary_identity_issue_date}		[Common] - Create string param in dic    ${arg_dic}     		issue_date
    ${param_representative_primary_identity_expired_date}		[Common] - Create string param in dic    ${arg_dic}     		expired_date
    ${param_representative_primary_identity_front_identity_url}		[Common] - Create string param in dic    ${arg_dic}     		front_identity_url
    ${param_representative_primary_identity_back_identity_url}		[Common] - Create string param in dic    ${arg_dic}     		back_identity_url
    ${param_authorized_title}		[Common] - Create string param in dic    ${arg_dic}     		title
    ${param_authorized_first_name}		[Common] - Create string param in dic    ${arg_dic}     		first_name
    ${param_authorized_middle_name}		[Common] - Create string param in dic    ${arg_dic}     		middle_name
    ${param_authorized_last_name}		[Common] - Create string param in dic    ${arg_dic}     		last_name
    ${param_authorized_suffix}		[Common] - Create string param in dic    ${arg_dic}     		suffix
    ${param_authorized_date_of_birth}		[Common] - Create string param in dic    ${arg_dic}     		date_of_birth
    ${param_authorized_place_of_birth}		[Common] - Create string param in dic    ${arg_dic}     		place_of_birth
    ${param_authorized_nationality}		[Common] - Create string param in dic    ${arg_dic}     		nationality
    ${param_authorized_occupation}		[Common] - Create string param in dic    ${arg_dic}     		occupation
    ${param_authorized_occupation_title}		[Common] - Create string param in dic    ${arg_dic}     		occupation_title
    ${param_authorized_source_of_fund}		[Common] - Create string param in dic    ${arg_dic}     		source_of_fund
    ${param_authorized_national_id_number}		[Common] - Create string param in dic    ${arg_dic}     		national_id_number
    ${param_authorized_email}		[Common] - Create string param in dic    ${arg_dic}     		email
    ${param_authorized_mobile_number}		[Common] - Create string param in dic    ${arg_dic}     		mobile_number
    ${param_authorized_telephone_number}		[Common] - Create string param in dic    ${arg_dic}     		telephone_number
    ${param_authorized_current_address_citizen_association}		[Common] - Create string param in dic    ${arg_dic}     		citizen_association
    ${param_authorized_current_address_neighbourhood_association}		[Common] - Create string param in dic    ${arg_dic}     		neighbourhood_association
    ${param_authorized_current_address_address}		[Common] - Create string param in dic    ${arg_dic}     		address
    ${param_authorized_current_address_commune}		[Common] - Create string param in dic    ${arg_dic}     		commune
    ${param_authorized_current_address_district}		[Common] - Create string param in dic    ${arg_dic}     		district
    ${param_authorized_current_address_city}		[Common] - Create string param in dic    ${arg_dic}     		city
    ${param_authorized_current_address_province}		[Common] - Create string param in dic    ${arg_dic}     		province
    ${param_authorized_current_address_postal_code}		[Common] - Create string param in dic    ${arg_dic}     		postal_code
    ${param_authorized_current_address_country}		[Common] - Create string param in dic    ${arg_dic}     		country
    ${param_authorized_current_address_landmark}		[Common] - Create string param in dic    ${arg_dic}     		landmark
    ${param_authorized_current_address_longitude}		[Common] - Create string param in dic    ${arg_dic}     		longitude
    ${param_authorized_current_address_latitude}		[Common] - Create string param in dic    ${arg_dic}     		latitude
    ${param_authorized_permanent_address_citizen_association}		[Common] - Create string param in dic    ${arg_dic}     		citizen_association
    ${param_authorized_permanent_address_neighbourhood_association}		[Common] - Create string param in dic    ${arg_dic}     		neighbourhood_association
    ${param_authorized_permanent_address_address}		[Common] - Create string param in dic    ${arg_dic}     		address
    ${param_authorized_permanent_address_commune}		[Common] - Create string param in dic    ${arg_dic}     		commune
    ${param_authorized_permanent_address_district}		[Common] - Create string param in dic    ${arg_dic}     		district
    ${param_authorized_permanent_address_city}		[Common] - Create string param in dic    ${arg_dic}     		city
    ${param_authorized_permanent_address_province}		[Common] - Create string param in dic    ${arg_dic}     		province
    ${param_authorized_permanent_address_postal_code}		[Common] - Create string param in dic    ${arg_dic}     		postal_code
    ${param_authorized_permanent_address_country}		[Common] - Create string param in dic    ${arg_dic}     		country
    ${param_authorized_permanent_address_landmark}		[Common] - Create string param in dic    ${arg_dic}     		landmark
    ${param_authorized_permanent_address_longitude}		[Common] - Create string param in dic    ${arg_dic}     		longitude
    ${param_authorized_permanent_address_latitude}		[Common] - Create string param in dic    ${arg_dic}     		latitude
    ${param_billing_title}		[Common] - Create string param in dic    ${arg_dic}     		title
    ${param_billing_first_name}		[Common] - Create string param in dic    ${arg_dic}     		first_name
    ${param_billing_middle_name}		[Common] - Create string param in dic    ${arg_dic}     		middle_name
    ${param_billing_last_name}		[Common] - Create string param in dic    ${arg_dic}     		last_name
    ${param_billing_suffix}		[Common] - Create string param in dic    ${arg_dic}     		suffix
    ${param_billing_date_of_birth}		[Common] - Create string param in dic    ${arg_dic}     		date_of_birth
    ${param_billing_place_of_birth}		[Common] - Create string param in dic    ${arg_dic}     		place_of_birth
    ${param_billing_nationality}		[Common] - Create string param in dic    ${arg_dic}     		nationality
    ${param_billing_occupation}		[Common] - Create string param in dic    ${arg_dic}     		occupation
    ${param_billing_occupation_title}		[Common] - Create string param in dic    ${arg_dic}     		occupation_title
    ${param_billing_source_of_fund}		[Common] - Create string param in dic    ${arg_dic}     		source_of_fund
    ${param_billing_national_id_number}		[Common] - Create string param in dic    ${arg_dic}     		national_id_number
    ${param_billing_email}		[Common] - Create string param in dic    ${arg_dic}     		email
    ${param_billing_mobile_number}		[Common] - Create string param in dic    ${arg_dic}     		mobile_number
    ${param_billing_telephone_number}		[Common] - Create string param in dic    ${arg_dic}     		telephone_number
    ${param_billing_current_address_citizen_association}		[Common] - Create string param in dic    ${arg_dic}     		citizen_association
    ${param_billing_current_address_neighbourhood_association}		[Common] - Create string param in dic    ${arg_dic}     		neighbourhood_association
    ${param_billing_current_address_address}		[Common] - Create string param in dic    ${arg_dic}     		address
    ${param_billing_current_address_commune}		[Common] - Create string param in dic    ${arg_dic}     		commune
    ${param_billing_current_address_district}		[Common] - Create string param in dic    ${arg_dic}     		district
    ${param_billing_current_address_city}		[Common] - Create string param in dic    ${arg_dic}     		city
    ${param_billing_current_address_province}		[Common] - Create string param in dic    ${arg_dic}     		province
    ${param_billing_current_address_postal_code}		[Common] - Create string param in dic    ${arg_dic}     		postal_code
    ${param_billing_current_address_country}		[Common] - Create string param in dic    ${arg_dic}     		country
    ${param_billing_current_address_landmark}		[Common] - Create string param in dic    ${arg_dic}     		landmark
    ${param_billing_current_address_longitude}		[Common] - Create string param in dic    ${arg_dic}     		longitude
    ${param_billing_current_address_latitude}		[Common] - Create string param in dic    ${arg_dic}     		latitude
    ${param_billing_permanent_address_citizen_association}		[Common] - Create string param in dic    ${arg_dic}     		citizen_association
    ${param_billing_permanent_address_neighbourhood_association}		[Common] - Create string param in dic    ${arg_dic}     		neighbourhood_association
    ${param_billing_permanent_address_address}		[Common] - Create string param in dic    ${arg_dic}     		address
    ${param_billing_permanent_address_commune}		[Common] - Create string param in dic    ${arg_dic}     		commune
    ${param_billing_permanent_address_district}		[Common] - Create string param in dic    ${arg_dic}     		district
    ${param_billing_permanent_address_city}		[Common] - Create string param in dic    ${arg_dic}     		city
    ${param_billing_permanent_address_province}		[Common] - Create string param in dic    ${arg_dic}     		province
    ${param_billing_permanent_address_postal_code}		[Common] - Create string param in dic    ${arg_dic}     		postal_code
    ${param_billing_permanent_address_country}		[Common] - Create string param in dic    ${arg_dic}     		country
    ${param_billing_permanent_address_landmark}		[Common] - Create string param in dic    ${arg_dic}     		landmark
    ${param_billing_permanent_address_longitude}		[Common] - Create string param in dic    ${arg_dic}     		longitude
    ${param_billing_permanent_address_latitude}		[Common] - Create string param in dic    ${arg_dic}     		latitude
    ${param_field_1_name}		[Common] - Create string param in dic    ${arg_dic}     		field_1_name
    ${param_field_1_value}		[Common] - Create string param in dic    ${arg_dic}     		field_1_value
    ${param_field_2_name}		[Common] - Create string param in dic    ${arg_dic}     		field_2_name
    ${param_field_2_value}		[Common] - Create string param in dic    ${arg_dic}     		field_2_value
    ${param_field_3_name}		[Common] - Create string param in dic    ${arg_dic}     		field_3_name
    ${param_field_3_value}		[Common] - Create string param in dic    ${arg_dic}     		field_3_value
    ${param_field_4_name}		[Common] - Create string param in dic    ${arg_dic}     		field_4_name
    ${param_field_4_value}		[Common] - Create string param in dic    ${arg_dic}     		field_4_value
    ${param_field_5_name}		[Common] - Create string param in dic    ${arg_dic}     		field_5_name
    ${param_field_5_value}		[Common] - Create string param in dic    ${arg_dic}     		field_5_value

    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...          {
        ...                ${param_company_type_id}
        ...                ${param_business_type}
        ...                ${param_business_field}
        ...                ${param_name}
        ...                ${param_abbreviation}
        ...                ${param_tin_number}
        ...                ${param_risk_level}
        ...                ${param_is_testing_account}
        ...                "address": {
        ...      					${param_address_citizen_association}
        ...      					${param_address_neighbourhood_association}
        ...      					${param_address_address}
        ...      					${param_address_commune}
        ...      					${param_address_district}
        ...      					${param_address_city}
        ...      					${param_address_province}
        ...      					${param_address_postal_code}
        ...      					${param_address_country}
        ...      					${param_address_landmark}
        ...      					${param_address_longitude}
        ...      					${param_address_latitude}
        ...                 },
        ...                 ${param_mobile_number}
        ...                 ${param_telephone_number}
        ...                 ${param_email}
        ...                 ${param_fax_number}
        ...                 "financial":{
        ...      				${param_bank_name}
        ...      				${param_bank_account_status}
        ...      				${param_bank_account_name}
        ...      				${param_bank_account_number}
        ...      				${param_bank_branch_area}
        ...      				${param_bank_branch_city}
        ...      				${param_bank_register_date}
        ...      				${param_bank_register_source}
        ...      				${param_bank_is_verified}
        ...      				${param_bank_end_date}
        ...                     ${param_bank_source_of_funds}
        ...                 },
        ...                 "contract": {
        ...      				${param_contract_type}
        ...      				${param_contract_number}
        ...      				${param_contract_extension_type}
        ...      				${param_contract_sign_date}
        ...      				${param_contract_issue_date}
        ...      				${param_contract_expired_date}
        ...      				${param_contract_notification_alert}
        ...      				${param_contract_day_of_period_reconciliation}
        ...      				${param_contract_file_url}
        ...      				${param_contract_assessment_information_url}
        ...                  },
        ...                 "owner": {
        ...                     ${param_owner_tin_number}
        ...                     ${param_owner_title}
        ...                     ${param_owner_first_name}
        ...                     ${param_owner_middle_name}
        ...                     ${param_owner_last_name}
        ...                     ${param_owner_suffix}
        ...                     ${param_owner_date_of_birth}
        ...                     ${param_owner_place_of_birth}
        ...                     ${param_owner_gender}
        ...                     ${param_owner_ethnicity}
        ...                     ${param_owner_nationality}
        ...                     ${param_owner_occupation}
        ...                     ${param_owner_occupation_title}
        ...                     ${param_owner_township_code}
        ...                     ${param_owner_township_name}
        ...                     ${param_owner_national_id_number}
        ...                     ${param_owner_mother_name}
        ...                     ${param_owner_email}
        ...                     ${param_owner_primary_mobile_number}
        ...                     ${param_owner_secondary_mobile_number}
        ...                     ${param_owner_tertiary_mobile_number}
        ...                     "current_address": {
        ...      			        ${param_owner_current_address_citizen_association}
        ...      					${param_owner_current_address_neighbourhood_association}
        ...      					${param_owner_current_address_address}
        ...      					${param_owner_current_address_commune}
        ...      					${param_owner_current_address_district}
        ...      					${param_owner_current_address_city}
        ...      					${param_owner_current_address_province}
        ...      					${param_owner_current_address_postal_code}
        ...      					${param_owner_current_address_country}
        ...      					${param_owner_current_address_landmark}
        ...      					${param_owner_current_address_longitude}
        ...      					${param_owner_current_address_latitude}
        ...                      },
        ...                      "permanent_address": {
        ...      					${param_owner_permanent_address_citizen_association}
        ...      					${param_owner_permanent_address_neighbourhood_association}
        ...      					${param_owner_permanent_address_address}
        ...      					${param_owner_permanent_address_commune}
        ...      					${param_owner_permanent_address_district}
        ...      					${param_owner_permanent_address_city}
        ...      					${param_owner_permanent_address_province}
        ...      					${param_owner_permanent_address_postal_code}
        ...      					${param_owner_permanent_address_country}
        ...      					${param_owner_permanent_address_landmark}
        ...      					${param_owner_permanent_address_longitude}
        ...      					${param_owner_permanent_address_latitude}
        ...                     }
        ...                 },
        ...                 "representative": {
        ...                     ${param_representative_tin_number}
        ...                     ${param_representative_title}
        ...                     ${param_representative_first_name}
        ...                     ${param_representative_middle_name}
        ...                     ${param_representative_last_name}
        ...                     ${param_representative_suffix}
        ...                     ${param_representative_date_of_birth}
        ...                     ${param_representative_place_of_birth}
        ...                     ${param_representative_gender}
        ...                     ${param_representative_ethnicity}
        ...                     ${param_representative_nationality}
        ...                     ${param_representative_occupation}
        ...                     ${param_representative_occupation_title}
        ...                     ${param_representative_township_code}
        ...                     ${param_representative_township_name}
        ...                     ${param_representative_national_id_number}
        ...                     ${param_representative_mother_name}
        ...                     ${param_representative_email}
        ...                     ${param_representative_primary_mobile_number}
        ...                     ${param_representative_secondary_mobile_number}
        ...                     ${param_representative_tertiary_mobile_number}
        ...                     "current_address": {
        ...      					${param_representative_current_address_citizen_association}
        ...      					${param_representative_current_address_neighbourhood_association}
        ...      					${param_representative_current_address_address}
        ...      					${param_representative_current_address_commune}
        ...      					${param_representative_current_address_district}
        ...      					${param_representative_current_address_city}
        ...      					${param_representative_current_address_province}
        ...      					${param_representative_current_address_postal_code}
        ...      					${param_representative_current_address_country}
        ...      					${param_representative_current_address_landmark}
        ...      					${param_representative_current_address_longitude}
        ...      					${param_representative_current_address_latitude}
        ...                     },
        ...                     "permanent_address": {
        ...      					${param_representative_permanent_address_citizen_association}
        ...      					${param_representative_permanent_address_neighbourhood_association}
        ...      					${param_representative_permanent_address_address}
        ...      					${param_representative_permanent_address_commune}
        ...      					${param_representative_permanent_address_district}
        ...      					${param_representative_permanent_address_city}
        ...      					${param_representative_permanent_address_province}
        ...      					${param_representative_permanent_address_postal_code}
        ...      					${param_representative_permanent_address_country}
        ...      					${param_representative_permanent_address_landmark}
        ...      				    ${param_representative_permanent_address_longitude}
        ...      					${param_representative_permanent_address_latitude}
        ...                     },
        ...                     "primary_identity": {
        ...                         ${param_representative_primary_identity_type}
        ...                         ${param_representative_primary_identity_status}
        ...                         ${param_representative_primary_identity_identity_id}
        ...                         ${param_representative_primary_identity_place_of_issue}
        ...                         ${param_representative_primary_identity_issue_date}
        ...                         ${param_representative_primary_identity_expired_date}
        ...                         ${param_representative_primary_identity_front_identity_url}
        ...                         ${param_representative_primary_identity_back_identity_url}
        ...                     }
        ...                 },
        ...                 "authorized_signatory": {
        ...                     ${param_authorized_title}
        ...                     ${param_authorized_first_name}
        ...                     ${param_authorized_middle_name}
        ...                     ${param_authorized_last_name}
        ...                     ${param_authorized_suffix}
        ...                     ${param_authorized_date_of_birth}
        ...                     ${param_authorized_place_of_birth}
        ...                     ${param_authorized_nationality}
        ...                     ${param_authorized_occupation}
        ...                     ${param_authorized_occupation_title}
        ...                     ${param_authorized_source_of_fund}
        ...                     ${param_authorized_national_id_number}
        ...                     ${param_authorized_email}
        ...                     ${param_authorized_mobile_number}
        ...                     ${param_authorized_telephone_number}
        ...                     "current_address": {
        ...      			        ${param_authorized_current_address_citizen_association}
        ...      					${param_authorized_current_address_neighbourhood_association}
        ...      					${param_authorized_current_address_address}
        ...      					${param_authorized_current_address_commune}
        ...      					${param_authorized_current_address_district}
        ...      					${param_authorized_current_address_city}
        ...      					${param_authorized_current_address_province}
        ...      					${param_authorized_current_address_postal_code}
        ...      					${param_authorized_current_address_country}
        ...      					${param_authorized_current_address_landmark}
        ...      					${param_authorized_current_address_longitude}
        ...      					${param_authorized_current_address_latitude}
        ...                      },
        ...                      "permanent_address": {
        ...      					${param_authorized_permanent_address_citizen_association}
        ...      					${param_authorized_permanent_address_neighbourhood_association}
        ...      					${param_authorized_permanent_address_address}
        ...      					${param_authorized_permanent_address_commune}
        ...      					${param_authorized_permanent_address_district}
        ...      					${param_authorized_permanent_address_city}
        ...      					${param_authorized_permanent_address_province}
        ...      					${param_authorized_permanent_address_postal_code}
        ...      					${param_authorized_permanent_address_country}
        ...      					${param_authorized_permanent_address_landmark}
        ...      					${param_authorized_permanent_address_longitude}
        ...      					${param_authorized_permanent_address_latitude}
        ...                     }
        ...                 },
        ...                 "billing_contact": {
        ...                     ${param_billing_title}
        ...                     ${param_billing_first_name}
        ...                     ${param_billing_middle_name}
        ...                     ${param_billing_last_name}
        ...                     ${param_billing_suffix}
        ...                     ${param_billing_date_of_birth}
        ...                     ${param_billing_place_of_birth}
        ...                     ${param_billing_nationality}
        ...                     ${param_billing_occupation}
        ...                     ${param_billing_occupation_title}
        ...                     ${param_billing_source_of_fund}
        ...                     ${param_billing_national_id_number}
        ...                     ${param_billing_email}
        ...                     ${param_billing_mobile_number}
        ...                     ${param_billing_telephone_number}
        ...                     "current_address": {
        ...      			        ${param_billing_current_address_citizen_association}
        ...      					${param_billing_current_address_neighbourhood_association}
        ...      					${param_billing_current_address_address}
        ...      					${param_billing_current_address_commune}
        ...      					${param_billing_current_address_district}
        ...      					${param_billing_current_address_city}
        ...      					${param_billing_current_address_province}
        ...      					${param_billing_current_address_postal_code}
        ...      					${param_billing_current_address_country}
        ...      					${param_billing_current_address_landmark}
        ...      					${param_billing_current_address_longitude}
        ...      					${param_billing_current_address_latitude}
        ...                      },
        ...                      "permanent_address": {
        ...      					${param_billing_permanent_address_citizen_association}
        ...      					${param_billing_permanent_address_neighbourhood_association}
        ...      					${param_billing_permanent_address_address}
        ...      					${param_billing_permanent_address_commune}
        ...      					${param_billing_permanent_address_district}
        ...      					${param_billing_permanent_address_city}
        ...      					${param_billing_permanent_address_province}
        ...      					${param_billing_permanent_address_postal_code}
        ...      					${param_billing_permanent_address_country}
        ...      					${param_billing_permanent_address_landmark}
        ...      					${param_billing_permanent_address_longitude}
        ...      					${param_billing_permanent_address_latitude}
        ...                     }
        ...                 },
        ...      		    ${param_field_1_name}
        ...      		    ${param_field_1_value}
        ...      			${param_field_2_name}
        ...      			${param_field_2_value}
        ...      			${param_field_3_name}
        ...      			${param_field_3_value}
        ...      			${param_field_4_name}
        ...      			${param_field_4_value}
        ...      			${param_field_5_name}
        ...      			${param_field_5_value}
        ...            }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_company_profiles_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create salary group
    [Arguments]     ${arg_dic}
    ${param_salary_group_name}=  [Common] - Create string param      name        ${arg_dic.salary_group_name}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}          catenate           SEPARATOR=
        ...          {
        ...             ${param_salary_group_name}
        ...          }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${company_id}=    convert to string   ${arg_dic.company_id}
    ${create_salary_group_path}=    replace string      ${create_salary_group_path}      {company_id}        ${company_id}   1
    ${response}             post request
        ...     api
        ...     ${create_salary_group_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API link customer to company
    [Arguments]     ${arg_dic}
    ${param_company_id}=  [Common] - Create string param in dic     ${arg_dic}    company_id
    ${param_salary_group_id}=  [Common] - Create string param in dic    ${arg_dic}    salary_group_id
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}          catenate           SEPARATOR=
        ...          {
        ...             ${param_company_id}
        ...             ${param_salary_group_id}
        ...          }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${customer_id}=    convert to string   ${arg_dic.customer_id}
    ${link_customer_to_company_path}=    replace string      ${link_customer_to_company_path}      {customer_id}        ${customer_id}   1
    ${response}             post request
        ...     api
        ...     ${link_customer_to_company_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update company profiles
    [Arguments]     ${arg_dic}
    ${param_business_type}=  [Common] - Create string param     business_type        ${arg_dic.business_type}
    ${param_business_field}=  [Common] - Create string param     business_field        ${arg_dic.business_field}
    ${param_name}=  [Common] - Create string param     name        ${arg_dic.name}
    ${param_abbreviation}=  [Common] - Create string param     abbreviation        ${arg_dic.abbreviation}
    ${param_tin_number}		[Common] - Create string param		tin_number		${arg_dic.tin_number}
    ${param_risk_level}		[Common] - Create string param		risk_level		${arg_dic.risk_level}
    ${param_authorized_signatory}=  [Common] - Create string param     authorized_signatory        ${arg_dic.authorized_signatory}
    ${param_is_testing_account}		[Common] - Create boolean param		is_testing_account		${arg_dic.is_testing_account}
    ${param_address_citizen_association}		[Common] - Create string param		citizen_association		${arg_dic.address_citizen_association}
    ${param_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${arg_dic.address_neighbourhood_association}
    ${param_address_address}		[Common] - Create string param		address		${arg_dic.address_address}
    ${param_address_commune}		[Common] - Create string param		commune		${arg_dic.address_commune}
    ${param_address_district}		[Common] - Create string param		district		${arg_dic.address_district}
    ${param_address_city}		[Common] - Create string param		city		${arg_dic.address_city}
    ${param_address_province}		[Common] - Create string param		province		${arg_dic.address_province}
    ${param_address_postal_code}		[Common] - Create string param		postal_code		${arg_dic.address_postal_code}
    ${param_address_country}		[Common] - Create string param		country		${arg_dic.address_country}
    ${param_address_landmark}		[Common] - Create string param		landmark		${arg_dic.address_landmark}
    ${param_address_longitude}		[Common] - Create string param		longitude		${arg_dic.address_longitude}
    ${param_address_latitude}		[Common] - Create string param		latitude		${arg_dic.address_latitude}
    ${param_mobile_number}		[Common] - Create string param		mobile_number		${arg_dic.mobile_number}
    ${param_telephone_number}		[Common] - Create string param		telephone_number		${arg_dic.telephone_number}
    ${param_email}=  [Common] - Create string param     email        ${arg_dic.email}
    ${param_fax_number}=  [Common] - Create string param     fax_number        ${arg_dic.fax_number}
    ${param_bank_name}		[Common] - Create string param		bank_name		${arg_dic.bank_name}
    ${param_bank_account_status}		[Common] - Create int param		bank_account_status		${arg_dic.bank_account_status}
    ${param_bank_account_name}		[Common] - Create string param		bank_account_name		${arg_dic.bank_account_name}
    ${param_bank_account_number}		[Common] - Create string param		bank_account_number		${arg_dic.bank_account_number}
    ${param_bank_branch_area}		[Common] - Create string param		bank_branch_area		${arg_dic.bank_branch_area}
    ${param_bank_branch_city}		[Common] - Create string param		bank_branch_city		${arg_dic.bank_branch_city}
    ${param_bank_register_date}		[Common] - Create string param		register_date		${arg_dic.bank_register_date}
    ${param_bank_register_source}		[Common] - Create string param		register_source		${arg_dic.bank_register_source}
    ${param_bank_is_verified}		[Common] - Create boolean param		is_verified		${arg_dic.bank_is_verified}
    ${param_bank_end_date}		[Common] - Create string param		end_date		${arg_dic.bank_end_date}
    ${param_bank_source_of_funds}=  [Common] - Create string param     source_of_funds        ${arg_dic.bank_source_of_funds}
    ${param_contract_type}		[Common] - Create string param		type		${arg_dic.contract_type}
    ${param_contract_number}		[Common] - Create string param		number		${arg_dic.contract_number}
    ${param_contract_extension_type}		[Common] - Create string param		extension_type		${arg_dic.contract_extension_type}
    ${param_contract_sign_date}		[Common] - Create string param		sign_date		${arg_dic.contract_sign_date}
    ${param_contract_issue_date}		[Common] - Create string param		issue_date		${arg_dic.contract_issue_date}
    ${param_contract_expired_date}		[Common] - Create string param		expired_date		${arg_dic.contract_expired_date}
    ${param_contract_notification_alert}		[Common] - Create string param		notification_alert		${arg_dic.contract_notification_alert}
    ${param_contract_day_of_period_reconciliation}		[Common] - Create string param		day_of_period_reconciliation		${arg_dic.contract_day_of_period_reconciliation}
    ${param_contract_file_url}		[Common] - Create string param		file_url		${arg_dic.contract_file_url}
    ${param_contract_assessment_information_url}		[Common] - Create string param		assessment_information_url		${arg_dic.contract_assessment_information_url}
    ${param_owner_tin_number}		[Common] - Create string param		tin_number		${arg_dic.owner_tin_number}
    ${param_owner_title}		[Common] - Create string param		title		${arg_dic.owner_title}
    ${param_owner_first_name}		[Common] - Create string param		first_name		${arg_dic.owner_first_name}
    ${param_owner_middle_name}		[Common] - Create string param		middle_name		${arg_dic.owner_middle_name}
    ${param_owner_last_name}		[Common] - Create string param		last_name		${arg_dic.owner_last_name}
    ${param_owner_suffix}		[Common] - Create string param		suffix		${arg_dic.owner_suffix}
    ${param_owner_date_of_birth}		[Common] - Create string param		date_of_birth		${arg_dic.owner_date_of_birth}
    ${param_owner_place_of_birth}		[Common] - Create string param		place_of_birth		${arg_dic.owner_place_of_birth}
    ${param_owner_gender}		[Common] - Create string param		gender		${arg_dic.owner_gender}
    ${param_owner_ethnicity}		[Common] - Create string param		ethnicity		${arg_dic.owner_ethnicity}
    ${param_owner_nationality}		[Common] - Create string param		nationality		${arg_dic.owner_nationality}
    ${param_owner_occupation}		[Common] - Create string param		occupation		${arg_dic.owner_occupation}
    ${param_owner_occupation_title}		[Common] - Create string param		occupation_title		${arg_dic.owner_occupation_title}
    ${param_owner_township_code}		[Common] - Create string param		township_code		${arg_dic.owner_township_code}
    ${param_owner_township_name}		[Common] - Create string param		township_name		${arg_dic.owner_township_name}
    ${param_owner_national_id_number}		[Common] - Create string param		national_id_number		${arg_dic.owner_national_id_number}
    ${param_owner_mother_name}		[Common] - Create string param		mother_name		${arg_dic.owner_mother_name}
    ${param_owner_email}		[Common] - Create string param		email		${arg_dic.owner_email}
    ${param_owner_primary_mobile_number}		[Common] - Create string param		primary_mobile_number		${arg_dic.owner_primary_mobile_number}
    ${param_owner_econdary_mobile_number}		[Common] - Create string param		secondary_mobile_number		${arg_dic.owner_secondary_mobile_number}
    ${param_owner_tertiary_mobile_number}		[Common] - Create string param		tertiary_mobile_number		${arg_dic.owner_tertiary_mobile_number}
    ${param_owner_current_address_citizen_association}		[Common] - Create string param		citizen_association		${arg_dic.owner_current_address_citizen_association}
    ${param_owner_current_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${arg_dic.owner_current_address_neighbourhood_association}
    ${param_owner_current_address_address}		[Common] - Create string param		address		${arg_dic.owner_current_address_address}
    ${param_owner_current_address_commune}		[Common] - Create string param		commune		${arg_dic.owner_current_address_commune}
    ${param_owner_current_address_district}		[Common] - Create string param		district		${arg_dic.owner_current_address_district}
    ${param_owner_current_address_city}		[Common] - Create string param		city		${arg_dic.owner_current_address_city}
    ${param_owner_current_address_province}		[Common] - Create string param		province		${arg_dic.owner_current_address_province}
    ${param_owner_current_address_postal_code}		[Common] - Create string param		postal_code		${arg_dic.owner_current_address_postal_code}
    ${param_owner_current_address_country}		[Common] - Create string param		country		${arg_dic.owner_current_address_country}
    ${param_owner_current_address_landmark}		[Common] - Create string param		landmark		${arg_dic.owner_current_address_landmark}
    ${param_owner_current_address_longitude}		[Common] - Create string param		longitude		${arg_dic.owner_current_address_longitude}
    ${param_owner_current_address_latitude}		[Common] - Create string param		latitude		${arg_dic.owner_current_address_latitude}
    ${param_owner_permanent_address_citizen_association}		[Common] - Create string param		citizen_association		${arg_dic.owner_permanent_address_citizen_association}
    ${param_owner_permanent_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${arg_dic.owner_permanent_address_neighbourhood_association}
    ${param_owner_permanent_address_address}		[Common] - Create string param		address		${arg_dic.owner_permanent_address_address}
    ${param_owner_permanent_address_commune}		[Common] - Create string param		commune		${arg_dic.owner_permanent_address_commune}
    ${param_owner_permanent_address_district}		[Common] - Create string param		district		${arg_dic.owner_permanent_address_district}
    ${param_owner_permanent_address_city}		[Common] - Create string param		city		${arg_dic.owner_permanent_address_city}
    ${param_owner_permanent_address_province}		[Common] - Create string param		province		${arg_dic.owner_permanent_address_province}
    ${param_owner_permanent_address_postal_code}		[Common] - Create string param		postal_code		${arg_dic.owner_permanent_address_postal_code}
    ${param_owner_permanent_address_country}		[Common] - Create string param		country		${arg_dic.owner_permanent_address_country}
    ${param_owner_permanent_address_landmark}		[Common] - Create string param		landmark		${arg_dic.owner_permanent_address_landmark}
    ${param_owner_permanent_address_longitude}		[Common] - Create string param		longitude		${arg_dic.owner_permanent_address_longitude}
    ${param_owner_permanent_address_latitude}		[Common] - Create string param		latitude		${arg_dic.owner_permanent_address_latitude}
    ${param_representative_tin_number}		[Common] - Create string param		tin_number		${arg_dic.representative_tin_number}
    ${param_representative_title}		[Common] - Create string param		title		${arg_dic.representative_title}
    ${param_representative_first_name}		[Common] - Create string param		first_name		${arg_dic.representative_first_name}
    ${param_representative_middle_name}		[Common] - Create string param		middle_name		${arg_dic.representative_middle_name}
    ${param_representative_last_name}		[Common] - Create string param		last_name		${arg_dic.representative_last_name}
    ${param_representative_suffix}		[Common] - Create string param		suffix		${arg_dic.representative_suffix}
    ${param_representative_date_of_birth}		[Common] - Create string param		date_of_birth		${arg_dic.representative_date_of_birth}
    ${param_representative_place_of_birth}		[Common] - Create string param		place_of_birth		${arg_dic.representative_place_of_birth}
    ${param_representative_gender}		[Common] - Create string param		gender		${arg_dic.representative_gender}
    ${param_representative_ethnicity}		[Common] - Create string param		ethnicity		${arg_dic.representative_ethnicity}
    ${param_representative_nationality}		[Common] - Create string param		nationality		${arg_dic.representative_nationality}
    ${param_representative_occupation}		[Common] - Create string param		occupation		${arg_dic.representative_occupation}
    ${param_representative_occupation_title}		[Common] - Create string param		occupation_title		${arg_dic.representative_occupation_title}
    ${param_representative_township_code}		[Common] - Create string param		township_code		${arg_dic.representative_township_code}
    ${param_representative_township_name}		[Common] - Create string param		township_name		${arg_dic.representative_township_name}
    ${param_representative_national_id_number}		[Common] - Create string param		national_id_number		${arg_dic.representative_national_id_number}
    ${param_representative_mother_name}		[Common] - Create string param		mother_name		${arg_dic.representative_mother_name}
    ${param_representative_email}		[Common] - Create string param		email		${arg_dic.representative_email}
    ${param_representative_primary_mobile_number}		[Common] - Create string param		primary_mobile_number		${arg_dic.representative_primary_mobile_number}
    ${param_representative_secondary_mobile_number}		[Common] - Create string param		secondary_mobile_number		${arg_dic.representative_secondary_mobile_number}
    ${param_representative_tertiary_mobile_number}		[Common] - Create string param		tertiary_mobile_number		${arg_dic.representative_tertiary_mobile_number}
    ${param_representative_current_address_citizen_association}		[Common] - Create string param		citizen_association		${arg_dic.representative_current_address_citizen_association}
    ${param_representative_current_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${arg_dic.representative_current_address_neighbourhood_association}
    ${param_representative_current_address_address}		[Common] - Create string param		address		${arg_dic.representative_current_address_address}
    ${param_representative_current_address_commune}		[Common] - Create string param		commune		${arg_dic.representative_current_address_commune}
    ${param_representative_current_address_district}		[Common] - Create string param		district		${arg_dic.representative_current_address_district}
    ${param_representative_current_address_city}		[Common] - Create string param		city		${arg_dic.representative_current_address_city}
    ${param_representative_current_address_province}		[Common] - Create string param		province		${arg_dic.representative_current_address_province}
    ${param_representative_current_address_postal_code}		[Common] - Create string param		postal_code		${arg_dic.representative_current_address_postal_code}
    ${param_representative_current_address_country}		[Common] - Create string param		country		${arg_dic.representative_current_address_country}
    ${param_representative_current_address_landmark}		[Common] - Create string param		landmark		${arg_dic.representative_current_address_landmark}
    ${param_representative_current_address_longitude}		[Common] - Create string param		longitude		${arg_dic.representative_current_address_longitude}
    ${param_representative_current_address_latitude}		[Common] - Create string param		latitude		${arg_dic.representative_current_address_latitude}
    ${param_representative_permanent_address_citizen_association}		[Common] - Create string param		citizen_association		${arg_dic.representative_permanent_address_citizen_association}
    ${param_representative_permanent_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${arg_dic.representative_permanent_address_neighbourhood_association}
    ${param_representative_permanent_address_address}		[Common] - Create string param		address		${arg_dic.representative_permanent_address_address}
    ${param_representative_permanent_address_commune}		[Common] - Create string param		commune		${arg_dic.representative_permanent_address_commune}
    ${param_representative_permanent_address_district}		[Common] - Create string param		district		${arg_dic.representative_permanent_address_district}
    ${param_representative_permanent_address_city}		[Common] - Create string param		city		${arg_dic.representative_permanent_address_city}
    ${param_representative_permanent_address_province}		[Common] - Create string param		province		${arg_dic.representative_permanent_address_province}
    ${param_representative_permanent_address_postal_code}		[Common] - Create string param		postal_code		${arg_dic.representative_permanent_address_postal_code}
    ${param_representative_permanent_address_country}		[Common] - Create string param		country		${arg_dic.representative_permanent_address_country}
    ${param_representative_permanent_address_landmark}		[Common] - Create string param		landmark		${arg_dic.representative_permanent_address_landmark}
    ${param_representative_permanent_address_longitude}		[Common] - Create string param		longitude		${arg_dic.representative_permanent_address_longitude}
    ${param_representative_permanent_address_latitude}		[Common] - Create string param		latitude		${arg_dic.representative_permanent_address_latitude}
    ${param_representative_primary_identity_type}		[Common] - Create string param		type		${arg_dic.representative_primary_identity_type}
    ${param_representative_primary_identity_status}		[Common] - Create int param		status		${arg_dic.representative_primary_identity_status}
    ${param_representative_primary_identity_identity_id}		[Common] - Create string param		identity_id		${arg_dic.representative_primary_identity_identity_id}
    ${param_representative_primary_identity_place_of_issue}		[Common] - Create string param		place_of_issue		${arg_dic.representative_primary_identity_place_of_issue}
    ${param_representative_primary_identity_issue_date}		[Common] - Create string param		issue_date		${arg_dic.representative_primary_identity_issue_date}
    ${param_representative_primary_identity_expired_date}		[Common] - Create string param		expired_date		${arg_dic.representative_primary_identity_expired_date}
    ${param_representative_primary_identity_front_identity_url}		[Common] - Create string param		front_identity_url		${arg_dic.representative_primary_identity_front_identity_url}
    ${param_representative_primary_identity_back_identity_url}		[Common] - Create string param		back_identity_url		${arg_dic.representative_primary_identity_back_identity_url}
    ${param_field_1_name}		[Common] - Create string param		field_1_name		${arg_dic.field_1_name}
    ${param_field_1_value}		[Common] - Create string param		field_1_value		${arg_dic.field_1_value}
    ${param_field_2_name}		[Common] - Create string param		field_2_name		${arg_dic.field_2_name}
    ${param_field_2_value}		[Common] - Create string param		field_2_value		${arg_dic.field_2_value}
    ${param_field_3_name}		[Common] - Create string param		field_3_name		${arg_dic.field_3_name}
    ${param_field_3_value}		[Common] - Create string param		field_3_value		${arg_dic.field_3_value}
    ${param_field_4_name}		[Common] - Create string param		field_4_name		${arg_dic.field_4_name}
    ${param_field_4_value}		[Common] - Create string param		field_4_value		${arg_dic.field_4_value}
    ${param_field_5_name}		[Common] - Create string param		field_5_name		${arg_dic.field_5_name}
    ${param_field_5_value}		[Common] - Create string param		field_5_value		${arg_dic.field_5_value}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...          {
        ...                ${param_business_type}
        ...                ${param_business_field}
        ...                ${param_name}
        ...                ${param_abbreviation}
        ...                ${param_tin_number}
        ...                ${param_risk_level}
        ...                ${param_authorized_signatory}
        ...                ${param_is_testing_account}
        ...                "address": {
        ...      					${param_address_citizen_association}
        ...      					${param_address_neighbourhood_association}
        ...      					${param_address_address}
        ...      					${param_address_commune}
        ...      					${param_address_district}
        ...      					${param_address_city}
        ...      					${param_address_province}
        ...      					${param_address_postal_code}
        ...      					${param_address_country}
        ...      					${param_address_landmark}
        ...      					${param_address_longitude}
        ...      					${param_address_latitude}
        ...                 },
        ...                 ${param_mobile_number}
        ...                 ${param_telephone_number}
        ...                 ${param_email}
        ...                 ${param_fax_number}
        ...                 "financial":{
        ...      				${param_bank_name}
        ...      				${param_bank_account_status}
        ...      				${param_bank_account_name}
        ...      				${param_bank_account_number}
        ...      				${param_bank_branch_area}
        ...      				${param_bank_branch_city}
        ...      				${param_bank_register_date}
        ...      				${param_bank_register_source}
        ...      				${param_bank_is_verified}
        ...      				${param_bank_end_date}
        ...                     ${param_bank_source_of_funds}
        ...                 },
        ...                 "contract": {
        ...      				${param_contract_type}
        ...      				${param_contract_number}
        ...      				${param_contract_extension_type}
        ...      				${param_contract_sign_date}
        ...      				${param_contract_issue_date}
        ...      				${param_contract_expired_date}
        ...      				${param_contract_notification_alert}
        ...      				${param_contract_day_of_period_reconciliation}
        ...      				${param_contract_file_url}
        ...      				${param_contract_assessment_information_url}
        ...                  },
        ...                 "owner": {
        ...                     ${param_owner_tin_number}
        ...                     ${param_owner_title}
        ...                     ${param_owner_first_name}
        ...                     ${param_owner_middle_name}
        ...                     ${param_owner_last_name}
        ...                     ${param_owner_suffix}
        ...                     ${param_owner_date_of_birth}
        ...                     ${param_owner_place_of_birth}
        ...                     ${param_owner_gender}
        ...                     ${param_owner_ethnicity}
        ...                     ${param_owner_nationality}
        ...                     ${param_owner_occupation}
        ...                     ${param_owner_occupation_title}
        ...                     ${param_owner_township_code}
        ...                     ${param_owner_township_name}
        ...                     ${param_owner_national_id_number}
        ...                     ${param_owner_mother_name}
        ...                     ${param_owner_email}
        ...                     ${param_owner_primary_mobile_number}
        ...                     ${param_owner_econdary_mobile_number}
        ...                     ${param_owner_tertiary_mobile_number}
        ...                     "address": {
        ...                         "current_address": {
        ...      					    ${param_owner_current_address_citizen_association}
        ...      					    ${param_owner_current_address_neighbourhood_association}
        ...      					    ${param_owner_current_address_address}
        ...      					    ${param_owner_current_address_commune}
        ...      					    ${param_owner_current_address_district}
        ...      					    ${param_owner_current_address_city}
        ...      					    ${param_owner_current_address_province}
        ...      					    ${param_owner_current_address_postal_code}
        ...      					    ${param_owner_current_address_country}
        ...      					    ${param_owner_current_address_landmark}
        ...      					    ${param_owner_current_address_longitude}
        ...      					    ${param_owner_current_address_latitude}
        ...                         },
        ...                         "permanent_address": {
        ...      					    ${param_owner_permanent_address_citizen_association}
        ...      					    ${param_owner_permanent_address_neighbourhood_association}
        ...      					    ${param_owner_permanent_address_address}
        ...      					    ${param_owner_permanent_address_commune}
        ...      					    ${param_owner_permanent_address_district}
        ...      					    ${param_owner_permanent_address_city}
        ...      					    ${param_owner_permanent_address_province}
        ...      					    ${param_owner_permanent_address_postal_code}
        ...      					    ${param_owner_permanent_address_country}
        ...      					    ${param_owner_permanent_address_landmark}
        ...      					    ${param_owner_permanent_address_longitude}
        ...      					    ${param_owner_permanent_address_latitude}
        ...                         }
        ...                     }
        ...                 },
        ...                 "representative": {
        ...                     ${param_representative_tin_number}
        ...                     ${param_representative_title}
        ...                     ${param_representative_first_name}
        ...                     ${param_representative_middle_name}
        ...                     ${param_representative_last_name}
        ...                     ${param_representative_suffix}
        ...                     ${param_representative_date_of_birth}
        ...                     ${param_representative_place_of_birth}
        ...                     ${param_representative_gender}
        ...                     ${param_representative_ethnicity}
        ...                     ${param_representative_nationality}
        ...                     ${param_representative_occupation}
        ...                     ${param_representative_occupation_title}
        ...                     ${param_representative_township_code}
        ...                     ${param_representative_township_name}
        ...                     ${param_representative_national_id_number}
        ...                     ${param_representative_mother_name}
        ...                     ${param_representative_email}
        ...                     ${param_representative_primary_mobile_number}
        ...                     ${param_representative_secondary_mobile_number}
        ...                     ${param_representative_tertiary_mobile_number}
        ...                     "address": {
        ...                         "current_address": {
        ...      					    ${param_representative_current_address_citizen_association}
        ...      					    ${param_representative_current_address_neighbourhood_association}
        ...      					    ${param_representative_current_address_address}
        ...      					    ${param_representative_current_address_commune}
        ...      					    ${param_representative_current_address_district}
        ...      					    ${param_representative_current_address_city}
        ...      					    ${param_representative_current_address_province}
        ...      					    ${param_representative_current_address_postal_code}
        ...      					    ${param_representative_current_address_country}
        ...      					    ${param_representative_current_address_landmark}
        ...      					    ${param_representative_current_address_longitude}
        ...      					    ${param_representative_current_address_latitude}
        ...                         },
        ...                         "permanent_address": {
        ...      					    ${param_representative_permanent_address_citizen_association}
        ...      					    ${param_representative_permanent_address_neighbourhood_association}
        ...      					    ${param_representative_permanent_address_address}
        ...      					    ${param_representative_permanent_address_commune}
        ...      					    ${param_representative_permanent_address_district}
        ...      					    ${param_representative_permanent_address_city}
        ...      					    ${param_representative_permanent_address_province}
        ...      					    ${param_representative_permanent_address_postal_code}
        ...      					    ${param_representative_permanent_address_country}
        ...      					    ${param_representative_permanent_address_landmark}
        ...      					    ${param_representative_permanent_address_longitude}
        ...      					    ${param_representative_permanent_address_latitude}
        ...                         }
        ...                     },
        ...                     "primary_identity": {
        ...                         ${param_representative_primary_identity_type}
        ...                         ${param_representative_primary_identity_status}
        ...                         ${param_representative_primary_identity_identity_id}
        ...                         ${param_representative_primary_identity_place_of_issue}
        ...                         ${param_representative_primary_identity_issue_date}
        ...                         ${param_representative_primary_identity_expired_date}
        ...                         ${param_representative_primary_identity_front_identity_url}
        ...                         ${param_representative_primary_identity_back_identity_url}
        ...                     }
        ...                 },
        ...      		    ${param_field_1_name}
        ...      		    ${param_field_1_value}
        ...      			${param_field_2_name}
        ...      			${param_field_2_value}
        ...      			${param_field_3_name}
        ...      			${param_field_3_value}
        ...      			${param_field_4_name}
        ...      			${param_field_4_value}
        ...      			${param_field_5_name}
        ...      			${param_field_5_value}
        ...                 }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${company_id}=    convert to string   ${arg_dic.company_id}
    ${update_company_profiles_path}=    replace string      ${update_company_profiles_path}      {company_id}        ${company_id}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${update_company_profiles_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete company profiles
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${company_id}=    convert to string   ${arg_dic.company_id}
    ${delete_company_profiles_path}=    replace string      ${delete_company_profiles_path}      {company_id}        ${company_id}   1
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${delete_company_profiles_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API create smart card
    [Arguments]     ${arg_dic}
    ${param_serial_number}=  [Common] - Create string param     serial_number        ${arg_dic.card_number}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_serial_number}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${agent_id}=    convert to string   ${arg_dic.agent_id}
    ${create_smart_card_path}=    replace string      ${create_smart_card_path}      {agent_id}        ${agent_id}   1
                create session          api        ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${create_smart_card_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get smart card by serial number
    [Arguments]     ${arg_dic}
    ${param_serial_number}=  get from dictionary    ${arg_dic}    serial_number
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${get_smart_card_by_serial_number_path}=    replace string      ${get_smart_card_by_serial_number_path}      {serial_number}        ${param_serial_number}   1
                create session          api        ${api_gateway_host}
    ${response}             get request
        ...     api
        ...     ${get_smart_card_by_serial_number_path}
        ...     ${header}
    log      ${response.text}

    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update smart card
    [Arguments]     ${arg_dic}
    ${param_contain_password}=  [Common] - Create string param     contain_password        ${arg_dic.contain_password}
    ${param_identity_id}=  [Common] - Create string param     identity_id             ${arg_dic.identity_id}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_contain_password}
        ...		${param_identity_id}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${smart_card_id}=    convert to string   ${arg_dic.smart_card_id}
    ${update_smart_card_path}=    replace string      ${update_smart_card_path}      {smart_card_id}        ${smart_card_id}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${update_smart_card_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete smart card
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${smart_card_id}=    convert to string   ${arg_dic.smart_card_id}
    ${delete_smart_card_path}=    replace string      ${delete_smart_card_path}      {smart_card_id}        ${smart_card_id}   1
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${delete_smart_card_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete smart card by id
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${smart_card_id}=    convert to string   ${arg_dic.smart_card_id}
    ${delete_smart_card_by_id_path}=    replace string      ${delete_smart_card_by_id_path}      {smart_card_id}        ${smart_card_id}   1
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${delete_smart_card_by_id_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API create shop type
    [Arguments]
    ...     ${access_token}
    ...     ${name}
    ...     ${description}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${data}              catenate
        ...    {
        ...            "name":"${name}",
        ...            "description":"${description}"
        ...    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_shop_type_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update shop type
    [Arguments]     ${arg_dic}
    ${param_name}=  [Common] - Create string param     name        ${arg_dic.name}
    ${param_description}=  [Common] - Create string param     description        ${arg_dic.description}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_name}
        ...		${param_description}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${shop_type_id}=    convert to string   ${arg_dic.shop_type_id}
    ${update_shop_type_path}=    replace string      ${update_shop_type_path}      {shop_type_id}        ${shop_type_id}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${update_shop_type_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete shop type
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${shop_type_id}=    convert to string   ${arg_dic.shop_type_id}
    ${delete_shop_type_path}=    replace string      ${delete_shop_type_path}      {shop_type_id}        ${shop_type_id}   1
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${delete_shop_type_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API create shop category
    [Arguments]
    ...     ${access_token}
    ...     ${name}
    ...     ${description}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}

    ${data}              catenate
        ...	{
        ...		"name":"${name}",
        ...		"description":"${description}"
        ...	}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_shop_category_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update shop category
    [Arguments]     ${arg_dic}
    ${param_name}=  [Common] - Create string param     name        ${arg_dic.name}
    ${param_description}=  [Common] - Create string param     description        ${arg_dic.description}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_name}
        ...		${param_description}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${shop_category_id}=    convert to string   ${arg_dic.shop_category_id}
    ${update_shop_category_path}=    replace string      ${update_shop_category_path}      {shop_category_id}        ${shop_category_id}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${update_shop_category_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete shop category
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${shop_category_id}=    convert to string   ${arg_dic.shop_category_id}
    ${delete_shop_category_path}=    replace string      ${delete_shop_category_path}      {shop_category_id}        ${shop_category_id}   1
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${delete_shop_category_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API create shop
    [Arguments]
    ...     ${access_token}
    ...     ${agent_id}
    ...     ${shop_category_id}
    ...     ${name}
    ...     ${name_local}
    ...     ${acquisition_source}
    ...     ${shop_mobile_number}
    ...     ${shop_telephone_number}
    ...     ${shop_email}
    ...     ${truemoney_signage}
    ...     ${requested_services}
    ...     ${network_coverage}
    ...     ${premises}
    ...     ${physical_security}
    ...     ${number_of_staff}
    ...     ${business_hour}
    ...     ${shop_type_id}
    ...     ${address_citizen_association}
    ...     ${address_neighbourhood_association}
    ...     ${address_address}
    ...     ${address_commune}
    ...     ${address_district}
    ...     ${address_city}
    ...     ${address_province}
    ...     ${address_postal_code}
    ...     ${address_country}
    ...     ${address_landmark}
    ...     ${address_longitude}
    ...     ${address_latitude}
    ...     ${address_citizen_association_local}
    ...     ${address_neighbourhood_association_local}
    ...     ${address_address_local}
    ...     ${address_commune_local}
    ...     ${address_district_local}
    ...     ${address_city_local}
    ...     ${address_province_local}
    ...     ${address_postal_code_local}
    ...     ${address_country_local}
    ...     ${address_landmark_local}
    ...     ${additional_supporting_file_1_url}
    ...     ${additional_supporting_file_2_url}
    ...     ${additional_ref_1}
    ...     ${additional_ref_2}
    ...     ${additional_ref1_local}
    ...     ${additional_ref2_local}
    ...     ${additional_acquiring_sale_executive_name}
    ...     ${additional_acquiring_sale_executive_name_local}
    ...     ${additional_relationship_manager_name}
    ...     ${additional_relationship_manager_name_local}
    ...     ${additional_relationship_manager_id}
    ...     ${additional_relationship_manager_email}
    ...     ${additional_sale_region}
    ...     ${additional_account_manager_name}
    ...     ${additional_account_manager_name_local}
    ...     ${additional_shop_photo}
    ...     ${additional_shop_map}
    ...     ${representative_first_name}
    ...     ${representative_first_name_local}
    ...     ${representative_middle_name}
    ...     ${representative_middle_name_local}
    ...     ${representative_last_name}
    ...     ${representative_last_name_local}
    ...     ${representative_mobile_number}
    ...     ${representative_telephone_number}
    ...     ${representative_email}

    ...     ${header_device_id}=${param_not_used}
    ...     ${header_device_description}=${param_not_used}
    ...     ${header_device_imei}=${param_not_used}
    ...     ${header_device_ip4}=${param_not_used}
    ...     ${header_client_id}=${setup_admin_client_id}
    ...     ${header_client_secret}=${setup_admin_client_secret}

    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}

     ${param_agent_id}		[Common] - Create string param		agent_id		${agent_id}
    ${param_shop_category_id}		[Common] - Create string param		shop_category_id		${shop_category_id}
    ${param_name}		[Common] - Create string param		name		${name}
    ${param_name_local}		[Common] - Create string param		name_local		${name_local}
    ${param_acquisition_source}		[Common] - Create string param		acquisition_source		${acquisition_source}
    ${param_shop_mobile_number}		[Common] - Create string param		shop_mobile_number		${shop_mobile_number}
    ${param_shop_telephone_number}		[Common] - Create string param		shop_telephone_number		${shop_telephone_number}
    ${param_shop_email}		[Common] - Create string param		shop_email		${shop_email}
    ${param_truemoney_signage}		[Common] - Create string param		truemoney_signage		${truemoney_signage}
    ${param_requested_services}		[Common] - Create string param		requested_services		${requested_services}
    ${param_network_coverage}		[Common] - Create string param		network_coverage		${network_coverage}
    ${param_premises}		[Common] - Create string param		premises		${premises}
    ${param_physical_security}		[Common] - Create string param		physical_security		${physical_security}
    ${param_number_of_staff}		[Common] - Create string param		number_of_staff		${number_of_staff}
    ${param_business_hour}		[Common] - Create string param		business_hour		${business_hour}
    ${param_shop_type_id}		[Common] - Create string param		shop_type_id		${shop_type_id}
    ${param_address_citizen_association}		[Common] - Create string param		citizen_association		${address_citizen_association}
    ${param_address_neighbourhood_association}		[Common] - Create string param		neighbourhood_association		${address_neighbourhood_association}
    ${param_address_address}		[Common] - Create string param		address		${address_address}
    ${param_address_commune}		[Common] - Create string param		commune		${address_commune}
    ${param_address_district}		[Common] - Create string param		district		${address_district}
    ${param_address_city}		[Common] - Create string param		city		${address_city}
    ${param_address_province}		[Common] - Create string param		province		${address_province}
    ${param_address_postal_code}		[Common] - Create string param		postal_code		${address_postal_code}
    ${param_address_country}		[Common] - Create string param		country		${address_country}
    ${param_address_landmark}		[Common] - Create string param		landmark		${address_landmark}
    ${param_address_longitude}		[Common] - Create string param		longitude		${address_longitude}
    ${param_address_latitude}		[Common] - Create string param		latitude		${address_latitude}
    ${param_address_citizen_association_local}		[Common] - Create string param		citizen_association_local		${address_citizen_association_local}
    ${param_address_neighbourhood_association_local}		[Common] - Create string param		neighbourhood_association_local		${address_neighbourhood_association_local}
    ${param_address_address_local}		            [Common] - Create string param		address_local		${address_address_local}
    ${param_address_commune_local}		            [Common] - Create string param		commune_local		${address_commune_local}
    ${param_address_district_local}		            [Common] - Create string param		district_local		${address_district_local}
    ${param_address_city_local}		                [Common] - Create string param		city_local		${address_city_local}
    ${param_address_province_local}		            [Common] - Create string param		province_local		${address_province_local}
    ${param_address_postal_code_local}		        [Common] - Create string param		postal_code_local		${address_postal_code_local}
    ${param_address_country_local}		    [Common] - Create string param		country_local		${address_country_local}
    ${param_address_landmark_local}		            [Common] - Create string param		landmark_local		${address_landmark_local}

    ${param_additional_supporting_file_1_url}		[Common] - Create string param		supporting_file_1_url		${additional_supporting_file_1_url}
    ${param_additional_supporting_file_2_url}		[Common] - Create string param		supporting_file_2_url		${additional_supporting_file_2_url}
    ${param_ref_1}		                            [Common] - Create string param		ref1		${additional_ref_1}
    ${param_ref_2}		                            [Common] - Create string param		ref2		${additional_ref_2}
    ${param_ref_1_local}		                    [Common] - Create string param		ref1_local		${additional_ref_1_local}
    ${param_ref_2_local}		                    [Common] - Create string param		ref2_local		${additional_ref_2_local}
    ${param_additional_acquiring_sale_executive_name}		                            [Common] - Create string param		acquiring_sale_executive_name		${additional_acquiring_sale_executive_name}
    ${param_additional_acquiring_sale_executive_name_local}		                            [Common] - Create string param		acquiring_sale_executive_name_local		${additional_acquiring_sale_executive_name_local}
    ${param_additional_relationship_manager_name}		                            [Common] - Create string param		relationship_manager_name		${additional_relationship_manager_name}
    ${param_additional_relationship_manager_name_local}		                            [Common] - Create string param		relationship_manager_name_local		${additional_relationship_manager_name_local}
    ${param_additional_relationship_manager_id}		                            [Common] - Create string param		relationship_manager_id		${additional_relationship_manager_id}
    ${param_additional_relationship_manager_email}		                            [Common] - Create string param		relationship_manager_email		${additional_relationship_manager_email}
    ${param_additional_sale_region}		                            [Common] - Create string param		sale_region		${additional_sale_region}
    ${param_additional_account_manager_name}		                            [Common] - Create string param		account_manager_name		${additional_account_manager_name}
    ${param_additional_account_manager_name_local}		                            [Common] - Create string param	account_manager_name_local		${additional_account_manager_name_local}
    ${param_additional_shop_photo}		                            [Common] - Create string param	shop_photo		${additional_shop_photo}
    ${param_additional_shop_map}		                            [Common] - Create string param	shop_map		${additional_shop_map}
    ${param_representative_first_name}		                            [Common] - Create string param	first_name		${representative_first_name}
    ${param_representative_first_name_local}		                            [Common] - Create string param	first_name_local		${representative_first_name_local}
    ${param_representative_middle_name}		                            [Common] - Create string param	middle_name		${representative_middle_name}
    ${param_representative_middle_name_local}		                            [Common] - Create string param	middle_name_local		${representative_middle_name_local}
    ${param_representative_last_name}		                            [Common] - Create string param	last_name		${representative_last_name}
    ${param_representative_last_name_local}		                            [Common] - Create string param	last_name_local		${representative_last_name_local}
    ${param_representative_mobile_number}		                            [Common] - Create string param	mobile_number		${representative_mobile_number}
    ${param_representative_telephone_number}		                            [Common] - Create string param	telephone_number		${representative_telephone_number}
    ${param_representative_email}		                            [Common] - Create string param	email		${representative_email}


    ${data}              catenate   SEPARATOR=
    ...          {
    ...                 ${param_agent_id}
    ...                 ${param_shop_type_id}
    ...                 ${param_name}
    ...                 ${param_name_local}
    ...                 ${param_shop_category_id}
    ...                 ${param_acquisition_source}
    ...                 ${param_truemoney_signage}
    ...                 ${param_requested_services}
    ...                 ${param_network_coverage}
    ...                 ${param_physical_security}
    ...                 ${param_premises}
    ...                 ${param_number_of_staff}
    ...                 ${param_business_hour}
    ...                 ${param_shop_mobile_number}
    ...                 ${param_shop_telephone_number}
    ...                 ${param_shop_email}
    ...                 "address": {
    ...      					${param_address_citizen_association}
    ...      					${param_address_neighbourhood_association}
    ...      					${param_address_address}
    ...      					${param_address_commune}
    ...      					${param_address_district}
    ...      					${param_address_city}
    ...      					${param_address_province}
    ...      					${param_address_postal_code}
    ...      					${param_address_country}
    ...      					${param_address_landmark}
    ...      					${param_address_longitude}
    ...      					${param_address_latitude}
     ...      					${param_address_citizen_association_local}
    ...      					${param_address_neighbourhood_association_local}
    ...      					${param_address_address_local}
    ...      					${param_address_commune_local}
    ...      					${param_address_district_local}
    ...      					${param_address_city_local}
    ...      					${param_address_province_local}
    ...      					${param_address_postal_code_local}
    ...      					${param_address_country_local}
    ...      					${param_address_landmark_local}
    ...                 },
    ...                 "additional": {
    ...      				${param_additional_supporting_file_1_url}
    ...      				${param_additional_supporting_file_2_url}
    ...      				${param_ref_1}
    ...      				${param_ref_2}
    ...      				${param_ref_1_local}
    ...      				${param_ref_2_local}
    ...      				${param_additional_relationship_manager_id}
    ...      				${param_additional_relationship_manager_name}
    ...      				${param_additional_relationship_manager_name_local}
    ...      				${param_additional_relationship_manager_email}
    ...      				${param_additional_acquiring_sale_executive_name}
    ...      				${param_additional_acquiring_sale_executive_name_local}
    ...      				${param_additional_sale_region}
    ...      				${param_additional_account_manager_name}
    ...      				${param_additional_account_manager_name_local}
    ...      				${param_additional_shop_photo}
    ...      				${param_additional_shop_map}
    ...                 },
    ...                 "representative": {
    ...      				${param_representative_first_name}
    ...      				${param_representative_first_name_local}
    ...      				${param_representative_last_name}
    ...      				${param_representative_last_name_local}
    ...      				${param_representative_middle_name}
    ...      				${param_representative_middle_name_local}
    ...      				${param_representative_mobile_number}
    ...      				${param_representative_telephone_number}
    ...      				${param_representative_email}
    ...                 }
    ...          }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
    create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_agent_shop_api_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${header}
    log        ${data}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update shop
    [Arguments]     ${arg_dic}
    ${param_name}=  [Common] - Create string param     name        ${arg_dic.name}
    ${param_agent_id}		[Common] - Create int param		agent_id		${arg_dic.agent_id}
    ${param_shop_type_id}		[Common] - Create int param		shop_type_id		${arg_dic.shop_type_id}
    ${param_shop_category_id}		[Common] - Create int param		shop_category_id		${arg_dic.shop_category_id}
    ${param_address}		[Common] - Create string param		address		${arg_dic.address}
    ${param_city}		[Common] - Create string param		city		${arg_dic.city}
    ${param_province}		[Common] - Create string param		province		${arg_dic.province}
    ${param_district}		[Common] - Create string param		district		${arg_dic.district}
    ${param_commune}		[Common] - Create string param		commune		${arg_dic.commune}
    ${param_country}		[Common] - Create string param		country		${arg_dic.country}
    ${param_landmark}		[Common] - Create string param		landmark		${arg_dic.landmark}
    ${param_latitude}		[Common] - Create string param		latitude		${arg_dic.latitude}
    ${param_longitude}		[Common] - Create string param		longitude		${arg_dic.longitude}
    ${param_relationship_manager_id}		[Common] - Create string param		relationship_manager_id		${arg_dic.relationship_manager_id}
    ${param_acquisition_source}		[Common] - Create string param		acquisition_source		${arg_dic.acquisition_source}
    ${param_postal_code}		[Common] - Create string param		postal_code		${arg_dic.postal_code}
    ${param_representative_first_name}		[Common] - Create string param		representative_first_name		${arg_dic.representative_first_name}
    ${param_representative_middle_name}		[Common] - Create string param		representative_middle_name		${arg_dic.representative_middle_name}
    ${param_representative_last_name}		[Common] - Create string param		representative_last_name		${arg_dic.representative_last_name}
    ${param_representative_mobile_number}		[Common] - Create string param		representative_mobile_number		${arg_dic.representative_mobile_number}
    ${param_representative_telephone_number}		[Common] - Create string param		representative_telephone_number		${arg_dic.representative_telephone_number}
    ${param_representative_email}		[Common] - Create string param		representative_email		${arg_dic.representative_email}
    ${param_shop_mobile_number}		[Common] - Create string param		shop_mobile_number		${arg_dic.shop_mobile_number}
    ${param_shop_telephone_number}		[Common] - Create string param		shop_telephone_number		${arg_dic.shop_telephone_number}
    ${param_shop_email}		[Common] - Create string param		shop_email		${arg_dic.shop_email}
    ${param_relationship_manager_name}		[Common] - Create string param		relationship_manager_name		${arg_dic.relationship_manager_name}
    ${param_relationship_manager_email}		[Common] - Create string param		relationship_manager_email		${arg_dic.relationship_manager_email}
    ${param_acquiring_sales_executive_name}		[Common] - Create string param		acquiring_sales_executive_name		${arg_dic.acquiring_sales_executive_name}
    ${param_sales_region}		[Common] - Create string param		sales_region		${arg_dic.sales_region}
    ${param_account_manager_name}		[Common] - Create string param		account_manager_name		${arg_dic.account_manager_name}
    ${param_ref1}		[Common] - Create string param		ref1		${arg_dic.ref1}
    ${param_ref2}		[Common] - Create string param		ref2		${arg_dic.ref2}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
    ...    {
    ...            ${param_agent_id}
    ...            ${param_shop_type_id}
    ...            ${param_name}
    ...            ${param_shop_category_id}
    ...            "address": {
    ...                ${param_address}
    ...                ${param_city}
    ...                ${param_province}
    ...                ${param_district}
    ...                ${param_commune}
    ...                ${param_country}
    ...                ${param_landmark}
    ...                ${param_latitude}
    ...                ${param_longitude}
    ...            },
    ...            ${param_relationship_manager_id}
    ...            ${param_acquisition_source}
    ...            ${param_postal_code}
    ...            ${param_representative_first_name}
    ...            ${param_representative_middle_name}
    ...            ${param_representative_last_name}
    ...            ${param_representative_mobile_number}
    ...            ${param_representative_telephone_number}
    ...            ${param_representative_email}
    ...            ${param_shop_mobile_number}
    ...            ${param_shop_telephone_number}
    ...            ${param_shop_email}
    ...            ${param_relationship_manager_name}
    ...            ${param_relationship_manager_email}
    ...            ${param_acquiring_sales_executive_name}
    ...            ${param_sales_region}
    ...            ${param_account_manager_name}
    ...            ${param_ref1}
    ...            ${param_ref2}
    ...    }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${shop_id}=    convert to string   ${arg_dic.shop_id}
    ${update_agent_shop_path}=    replace string      ${update_agent_shop_path}      {shop_id}        ${shop_id}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${update_agent_shop_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete shop
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${shop_id}=    convert to string   ${arg_dic.shop_id}
    ${delete_agent_shop_path}=    replace string      ${delete_agent_shop_path}      {shop_id}        ${shop_id}   1
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${delete_agent_shop_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API agent get his/her shop list
    [Arguments]
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${response}             get request
        ...     api
        ...     ${create_agent_shop_api_path}
        ...     ${header}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create edc
    [Arguments]     ${arg_dic}
    ${param_shop_id}=  [Common] - Create string param     shop_id        ${arg_dic.shop_id}
    ${param_terminal_id}=  [Common] - Create string param     terminal_id        ${arg_dic.terminal_id}
    ${param_serial_number}=  [Common] - Create string param     serial_number        ${arg_dic.serial_number}
    ${param_sim_number}=  [Common] - Create string param     sim_number        ${arg_dic.sim_number}
    ${param_battery_number}=  [Common] - Create string param     battery_number        ${arg_dic.battery_number}
    ${param_adapter_number}=  [Common] - Create string param     adapter_number        ${arg_dic.adapter_number}
    ${param_version}=  [Common] - Create string param     version        ${arg_dic.version}
    ${param_location}=  [Common] - Create string param     location        ${arg_dic.location}
    ${param_need_repair}=  [Common] - Create boolean param     need_repair        ${arg_dic.need_repair}
    ${param_need_return}=  [Common] - Create boolean param     need_return        ${arg_dic.need_return}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_shop_id}
        ...		${param_terminal_id}
        ...		${param_serial_number}
        ...		${param_sim_number}
        ...		${param_battery_number}
        ...		${param_adapter_number}
        ...		${param_version}
        ...		${param_location}
        ...		${param_need_repair}
        ...		${param_need_return}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_edc_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update edc
    [Arguments]     ${arg_dic}
    ${param_shop_id}=  [Common] - Create string param     shop_id        ${arg_dic.shop_id}
    ${param_terminal_id}=  [Common] - Create string param     terminal_id        ${arg_dic.terminal_id}
    ${param_serial_number}=  [Common] - Create string param     serial_number        ${arg_dic.serial_number}
    ${param_sim_number}=  [Common] - Create string param     sim_number        ${arg_dic.sim_number}
    ${param_battery_number}=  [Common] - Create string param     battery_number        ${arg_dic.battery_number}
    ${param_adapter_number}=  [Common] - Create string param     adapter_number        ${arg_dic.adapter_number}
    ${param_version}=  [Common] - Create string param     version        ${arg_dic.version}
    ${param_location}=  [Common] - Create string param     location        ${arg_dic.location}
    ${param_need_repair}=  [Common] - Create string param     need_repair        ${arg_dic.need_repair}
    ${param_need_return}=  [Common] - Create string param     need_return        ${arg_dic.need_return}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...    {
        ...     ${param_shop_id}
        ...		${param_terminal_id}
        ...		${param_serial_number}
        ...		${param_sim_number}
        ...		${param_battery_number}
        ...		${param_adapter_number}
        ...		${param_version}
        ...		${param_location}
        ...		${param_need_repair}
        ...		${param_need_return}
        ...    }
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${edc_id}=    convert to string   ${arg_dic.edc_id}
    ${update_edc_path}=    replace string      ${update_edc_path}      {edc_id}        ${edc_id}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${update_edc_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete edc
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${edc_id}=    convert to string   ${arg_dic.edc_id}
    ${delete_edc_path}=    replace string      ${delete_edc_path}      {edc_id}        ${edc_id}   1
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${delete_edc_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API update relationship
    [Arguments]     ${arg_dic}
    ${param_is_sharing_benefit}=  [Common] - Create boolean param     is_sharing_benefit        ${arg_dic.is_sharing_benefit}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...  {
        ...		${param_is_sharing_benefit}
        ...  }
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${relationship_id}=    convert to string   ${arg_dic.relationship_id}
    ${update_agent_relationship_path}=    replace string      ${update_agent_relationship_path}      {relationship_id}        ${relationship_id}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${update_agent_relationship_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API get relationship
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${params}                create dictionary
        ...     relationship_type_id=${arg_dic.relationship_type_id}
        ...     role=${arg_dic.role}
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_agent_relationship_path}
        ...     ${params}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API get sub agents
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${agent_id}=    convert to string   ${arg_dic.agent_id}
    ${get_sub_agent_path}=    replace string      ${get_sub_agent_path}      {agent_id}        ${agent_id}  1
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${get_sub_agent_path}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API agent create device
    [Arguments]
    ...     ${access_token}
    ...     ${device_unique_reference}
    ...     ${device_model}
    ...     ${shop_id}
    ...     ${channel_type_id}
    ...     ${channel_id}
    ...     ${mac_address}
    ...     ${network_provider_name}
    ...     ${public_ip_address}
    ...     ${supporting_file_1}
    ...     ${supporting_file_2}
    ...     ${device_name}
    ...     ${os}
    ...     ${os_version}
    ...     ${display_size_in_inches}
    ...     ${pixel_counts}
    ...     ${unique_number}
    ...     ${serial_number}
    ...     ${app_version}
    ...     ${edc_serial_number}
    ...     ${edc_model}
    ...     ${edc_firmware_version}
    ...     ${edc_software_version}
    ...     ${edc_sim_card_number}
    ...     ${edc_battery_serial_number}
    ...     ${edc_adapter_serial_number}
    ...     ${edc_smartcard_1_number}
    ...     ${edc_smartcard_2_number}
    ...     ${edc_smartcard_3_number}
    ...     ${edc_smartcard_4_number}
    ...     ${edc_smartcard_5_number}
    ...     ${pos_serial_number}
    ...     ${pos_model}
    ...     ${pos_firmware_version}
    ...     ${pos_software_version}
    ...     ${pos_smartcard_1_number}
    ...     ${pos_smartcard_2_number}
    ...     ${pos_smartcard_3_number}
    ...     ${pos_smartcard_4_number}
    ...     ${pos_smartcard_5_number}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${param_channel_type_id}		[Common] - Create int param		channel_type_id		${channel_type_id}
    ${param_channel_id}		[Common] - Create int param		channel_id		${channel_id}
    ${param_shop_id}		[Common] - Create int param		shop_id		${shop_id}
    ${param_mac_address}		[Common] - Create string param		mac_address		${mac_address}
    ${param_network_provider_name}		[Common] - Create string param		network_provider_name		${network_provider_name}
    ${param_public_ip_address}		[Common] - Create string param		public_ip_address		${public_ip_address}
    ${param_supporting_file_1}		[Common] - Create string param		supporting_file_1		${supporting_file_1}
    ${param_supporting_file_2}		[Common] - Create string param		supporting_file_2		${supporting_file_2}
    ${param_device_name}		[Common] - Create string param		device_name		${device_name}
    ${param_device_model}		[Common] - Create string param		device_model		${device_model}
    ${param_device_unique_reference}		[Common] - Create string param		device_unique_reference		${device_unique_reference}
    ${param_os}		[Common] - Create string param		os		${os}
    ${param_os_version}		[Common] - Create string param		os_version		${os_version}
    ${param_display_size_in_inches}		[Common] - Create string param		display_size_in_inches		${display_size_in_inches}
    ${param_pixel_counts}		[Common] - Create string param		pixel_counts		${pixel_counts}
    ${param_unique_number}		[Common] - Create string param		unique_number		${unique_number}
    ${param_serial_number}		[Common] - Create string param		serial_number		${serial_number}
    ${param_app_version}		[Common] - Create string param		app_version		${app_version}
    ${param_edc_serial_number}		[Common] - Create string param		edc_serial_number		${edc_serial_number}
    ${param_edc_model}		[Common] - Create string param		edc_model		${edc_model}
    ${param_edc_firmware_version}		[Common] - Create string param		edc_firmware_version		${edc_firmware_version}
    ${param_edc_software_version}		[Common] - Create string param		edc_software_version		${edc_software_version}
    ${param_edc_sim_card_number}		[Common] - Create string param		edc_sim_card_number		${edc_sim_card_number}
    ${param_edc_battery_serial_number}		[Common] - Create string param		edc_battery_serial_number		${edc_battery_serial_number}
    ${param_edc_adapter_serial_number}		[Common] - Create string param		edc_adapter_serial_number		${edc_adapter_serial_number}
    ${param_edc_smartcard_1_number}		[Common] - Create string param		edc_smartcard_1_number		${edc_smartcard_1_number}
    ${param_edc_smartcard_2_number}		[Common] - Create string param		edc_smartcard_2_number		${edc_smartcard_2_number}
    ${param_edc_smartcard_3_number}		[Common] - Create string param		edc_smartcard_3_number		${edc_smartcard_3_number}
    ${param_edc_smartcard_4_number}		[Common] - Create string param		edc_smartcard_4_number		${edc_smartcard_4_number}
    ${param_edc_smartcard_5_number}		[Common] - Create string param		edc_smartcard_5_number		${edc_smartcard_5_number}
    ${param_pos_serial_number}		[Common] - Create string param		pos_serial_number		${pos_serial_number}
    ${param_pos_model}		[Common] - Create string param		pos_model		${pos_model}
    ${param_pos_firmware_version}		[Common] - Create string param		pos_firmware_version		${pos_firmware_version}
    ${param_pos_software_version}		[Common] - Create string param		pos_software_version		${pos_software_version}
    ${param_pos_smartcard_1_number}		[Common] - Create string param		pos_smartcard_1_number		${pos_smartcard_1_number}
    ${param_pos_smartcard_2_number}		[Common] - Create string param		pos_smartcard_2_number		${pos_smartcard_2_number}
    ${param_pos_smartcard_3_number}		[Common] - Create string param		pos_smartcard_3_number		${pos_smartcard_3_number}
    ${param_pos_smartcard_4_number}		[Common] - Create string param		pos_smartcard_4_number		${pos_smartcard_4_number}
    ${param_pos_smartcard_5_number}		[Common] - Create string param		pos_smartcard_5_number		${pos_smartcard_5_number}
    ${data}              catenate       SEPARATOR=
    ...    {
    ...            ${param_channel_type_id}
    ...            ${param_channel_id}
    ...            ${param_shop_id}
    ...            ${param_mac_address}
    ...            ${param_network_provider_name}
    ...            ${param_public_ip_address}
    ...            ${param_supporting_file_1}
    ...            ${param_supporting_file_2}
    ...            ${param_device_name}
    ...            ${param_device_model}
    ...            ${param_device_unique_reference}
    ...            ${param_os}
    ...            ${param_os_version}
    ...            ${param_display_size_in_inches}
    ...            ${param_pixel_counts}
    ...            ${param_unique_number}
    ...            ${param_serial_number}
    ...            ${param_edc_serial_number}
    ...            ${param_edc_model}
    ...            ${param_edc_firmware_version}
    ...            ${param_edc_software_version}
    ...            ${param_edc_sim_card_number}
    ...            ${param_edc_battery_serial_number}
    ...            ${param_edc_adapter_serial_number}
    ...            ${param_edc_smartcard_1_number}
    ...            ${param_edc_smartcard_2_number}
    ...            ${param_edc_smartcard_3_number}
    ...            ${param_edc_smartcard_4_number}
    ...            ${param_edc_smartcard_5_number}
    ...            ${param_pos_serial_number}
    ...            ${param_pos_model}
    ...            ${param_pos_firmware_version}
    ...            ${param_pos_software_version}
    ...            ${param_pos_smartcard_1_number}
    ...            ${param_pos_smartcard_2_number}
    ...            ${param_pos_smartcard_3_number}
    ...            ${param_pos_smartcard_4_number}
    ...            ${param_pos_smartcard_5_number}
    ...    }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${agent_create_devices_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${response_text}=   set variable    ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API system user creates agent device
    [Arguments]     ${arg_dic}
    ${param_channel_type_id}		[Common] - Create int param		channel_type_id		${arg_dic.channel_type_id}
    ${param_channel_id}		[Common] - Create int param		channel_id		${arg_dic.channel_id}
    ${param_shop_id}		[Common] - Create int param		shop_id		${arg_dic.shop_id}
    ${param_owner_id}		[Common] - Create int param		owner_id		${arg_dic.owner_id}
    ${param_mac_address}		[Common] - Create string param		mac_address		${arg_dic.mac_address}
    ${param_network_provider_name}		[Common] - Create string param		network_provider_name		${arg_dic.network_provider_name}
    ${param_public_ip_address}		[Common] - Create string param		public_ip_address		${arg_dic.public_ip_address}
    ${param_supporting_file_1}		[Common] - Create string param		supporting_file_1		${arg_dic.supporting_file_1}
    ${param_supporting_file_2}		[Common] - Create string param		supporting_file_2		${arg_dic.supporting_file_2}
    ${param_device_name}		[Common] - Create string param		device_name		${arg_dic.device_name}
    ${param_device_model}		[Common] - Create string param		device_model		${arg_dic.device_model}
    ${param_device_unique_reference}		[Common] - Create string param		device_unique_reference		${arg_dic.device_unique_reference}
    ${param_os}		[Common] - Create string param		os		${arg_dic.os}
    ${param_os_version}		[Common] - Create string param		os_version		${arg_dic.os_version}
    ${param_display_size_in_inches}		[Common] - Create string param		display_size_in_inches		${arg_dic.display_size_in_inches}
    ${param_pixel_counts}		[Common] - Create string param		pixel_counts		${arg_dic.pixel_counts}
    ${param_unique_number}		[Common] - Create string param		unique_number		${arg_dic.unique_number}
    ${param_serial_number}		[Common] - Create string param		serial_number		${arg_dic.serial_number}
    ${param_app_version}		[Common] - Create string param		app_version		${arg_dic.app_version}
    ${param_ssid}		[Common] - Create string param		ssid		${arg_dic.ssid}
    ${param_edc_serial_number}		[Common] - Create string param		edc_serial_number		${arg_dic.edc_serial_number}
    ${param_edc_model}		[Common] - Create string param		edc_model		${arg_dic.edc_model}
    ${param_edc_firmware_version}		[Common] - Create string param		edc_firmware_version		${arg_dic.edc_firmware_version}
    ${param_edc_software_version}		[Common] - Create string param		edc_software_version		${arg_dic.edc_software_version}
    ${param_edc_sim_card_number}		[Common] - Create string param		edc_sim_card_number		${arg_dic.edc_sim_card_number}
    ${param_edc_battery_serial_number}		[Common] - Create string param		edc_battery_serial_number		${arg_dic.edc_battery_serial_number}
    ${param_edc_adapter_serial_number}		[Common] - Create string param		edc_adapter_serial_number		${arg_dic.edc_adapter_serial_number}
    ${param_edc_smartcard_1_number}		[Common] - Create string param		edc_smartcard_1_number		${arg_dic.edc_smartcard_1_number}
    ${param_edc_smartcard_2_number}		[Common] - Create string param		edc_smartcard_2_number		${arg_dic.edc_smartcard_2_number}
    ${param_edc_smartcard_3_number}		[Common] - Create string param		edc_smartcard_3_number		${arg_dic.edc_smartcard_3_number}
    ${param_edc_smartcard_4_number}		[Common] - Create string param		edc_smartcard_4_number		${arg_dic.edc_smartcard_4_number}
    ${param_edc_smartcard_5_number}		[Common] - Create string param		edc_smartcard_5_number		${arg_dic.edc_smartcard_5_number}
    ${param_pos_serial_number}		[Common] - Create string param		pos_serial_number		${arg_dic.pos_serial_number}
    ${param_pos_model}		[Common] - Create string param		pos_model		${arg_dic.pos_model}
    ${param_pos_firmware_version}		[Common] - Create string param		pos_firmware_version		${arg_dic.pos_firmware_version}
    ${param_pos_software_version}		[Common] - Create string param		pos_software_version		${arg_dic.pos_software_version}
    ${param_pos_smartcard_1_number}		[Common] - Create string param		pos_smartcard_1_number		${arg_dic.pos_smartcard_1_number}
    ${param_pos_smartcard_2_number}		[Common] - Create string param		pos_smartcard_2_number		${arg_dic.pos_smartcard_2_number}
    ${param_pos_smartcard_3_number}		[Common] - Create string param		pos_smartcard_3_number		${arg_dic.pos_smartcard_3_number}
    ${param_pos_smartcard_4_number}		[Common] - Create string param		pos_smartcard_4_number		${arg_dic.pos_smartcard_4_number}
    ${param_pos_smartcard_5_number}		[Common] - Create string param		pos_smartcard_5_number		${arg_dic.pos_smartcard_5_number}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...            ${param_channel_type_id}
        ...            ${param_channel_id}
        ...            ${param_shop_id}
        ...            ${param_owner_id}
        ...            ${param_mac_address}
        ...            ${param_network_provider_name}
        ...            ${param_public_ip_address}
        ...            ${param_supporting_file_1}
        ...            ${param_supporting_file_2}
        ...            ${param_device_name}
        ...            ${param_device_model}
        ...            ${param_device_unique_reference}
        ...            ${param_os}
        ...            ${param_os_version}
        ...            ${param_display_size_in_inches}
        ...            ${param_pixel_counts}
        ...            ${param_unique_number}
        ...            ${param_serial_number}
        ...            ${param_app_version}
        ...            ${param_ssid}
        ...            ${param_edc_serial_number}
        ...            ${param_edc_model}
        ...            ${param_edc_firmware_version}
        ...            ${param_edc_software_version}
        ...            ${param_edc_sim_card_number}
        ...            ${param_edc_battery_serial_number}
        ...            ${param_edc_adapter_serial_number}
        ...            ${param_edc_smartcard_1_number}
        ...            ${param_edc_smartcard_2_number}
        ...            ${param_edc_smartcard_3_number}
        ...            ${param_edc_smartcard_4_number}
        ...            ${param_edc_smartcard_5_number}
        ...            ${param_pos_serial_number}
        ...            ${param_pos_model}
        ...            ${param_pos_firmware_version}
        ...            ${param_pos_software_version}
        ...            ${param_pos_smartcard_1_number}
        ...            ${param_pos_smartcard_2_number}
        ...            ${param_pos_smartcard_3_number}
        ...            ${param_pos_smartcard_4_number}
        ...            ${param_pos_smartcard_5_number}

        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${system_user_create_agent_devices_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API agent updates device
    [Arguments]     ${arg_dic}
    ${param_channel_type_id}		[Common] - Create int param		channel_type_id		${arg_dic.channel_type_id}
    ${param_channel_id}		[Common] - Create int param		channel_id		${arg_dic.channel_id}
    ${param_shop_id}		[Common] - Create int param		shop_id		${arg_dic.shop_id}
    ${param_mac_address}		[Common] - Create string param		mac_address		${arg_dic.mac_address}
    ${param_network_provider_name}		[Common] - Create string param		network_provider_name		${arg_dic.network_provider_name}
    ${param_public_ip_address}		[Common] - Create string param		public_ip_address		${arg_dic.public_ip_address}
    ${param_supporting_file_1}		[Common] - Create string param		supporting_file_1		${arg_dic.supporting_file_1}
    ${param_supporting_file_2}		[Common] - Create string param		supporting_file_2		${arg_dic.supporting_file_2}
    ${param_device_name}		[Common] - Create string param		device_name		${arg_dic.device_name}
    ${param_device_model}		[Common] - Create string param		device_model		${arg_dic.device_model}
    ${param_device_unique_reference}		[Common] - Create string param		device_unique_reference		${arg_dic.device_unique_reference}
    ${param_os}		[Common] - Create string param		os		${arg_dic.os}
    ${param_os_version}		[Common] - Create string param		os_version		${arg_dic.os_version}
    ${param_display_size_in_inches}		[Common] - Create string param		display_size_in_inches		${arg_dic.display_size_in_inches}
    ${param_pixel_counts}		[Common] - Create string param		pixel_counts		${arg_dic.pixel_counts}
    ${param_unique_number}		[Common] - Create string param		unique_number		${arg_dic.unique_number}
    ${param_serial_number}		[Common] - Create string param		serial_number		${arg_dic.serial_number}
    ${param_app_version}		[Common] - Create string param		app_version		${arg_dic.app_version}
    ${param_edc_serial_number}		[Common] - Create string param		edc_serial_number		${arg_dic.edc_serial_number}
    ${param_edc_model}		[Common] - Create string param		edc_model		${arg_dic.edc_model}
    ${param_edc_firmware_version}		[Common] - Create string param		edc_firmware_version		${arg_dic.edc_firmware_version}
    ${param_edc_software_version}		[Common] - Create string param		edc_software_version		${arg_dic.edc_software_version}
    ${param_edc_sim_card_number}		[Common] - Create string param		edc_sim_card_number		${arg_dic.edc_sim_card_number}
    ${param_edc_battery_serial_number}		[Common] - Create string param		edc_battery_serial_number		${arg_dic.edc_battery_serial_number}
    ${param_edc_adapter_serial_number}		[Common] - Create string param		edc_adapter_serial_number		${arg_dic.edc_adapter_serial_number}
    ${param_edc_smartcard_1_number}		[Common] - Create string param		edc_smartcard_1_number		${arg_dic.edc_smartcard_1_number}
    ${param_edc_smartcard_2_number}		[Common] - Create string param		edc_smartcard_2_number		${arg_dic.edc_smartcard_2_number}
    ${param_edc_smartcard_3_number}		[Common] - Create string param		edc_smartcard_3_number		${arg_dic.edc_smartcard_3_number}
    ${param_edc_smartcard_4_number}		[Common] - Create string param		edc_smartcard_4_number		${arg_dic.edc_smartcard_4_number}
    ${param_edc_smartcard_5_number}		[Common] - Create string param		edc_smartcard_5_number		${arg_dic.edc_smartcard_5_number}
    ${param_pos_serial_number}		[Common] - Create string param		pos_serial_number		${arg_dic.pos_serial_number}
    ${param_pos_model}		[Common] - Create string param		pos_model		${arg_dic.pos_model}
    ${param_pos_firmware_version}		[Common] - Create string param		pos_firmware_version		${arg_dic.pos_firmware_version}
    ${param_pos_software_version}		[Common] - Create string param		pos_software_version		${arg_dic.pos_software_version}
    ${param_pos_smartcard_1_number}		[Common] - Create string param		pos_smartcard_1_number		${arg_dic.pos_smartcard_1_number}
    ${param_pos_smartcard_2_number}		[Common] - Create string param		pos_smartcard_2_number		${arg_dic.pos_smartcard_2_number}
    ${param_pos_smartcard_3_number}		[Common] - Create string param		pos_smartcard_3_number		${arg_dic.pos_smartcard_3_number}
    ${param_pos_smartcard_4_number}		[Common] - Create string param		pos_smartcard_4_number		${arg_dic.pos_smartcard_4_number}
    ${param_pos_smartcard_5_number}		[Common] - Create string param		pos_smartcard_5_number		${arg_dic.pos_smartcard_5_number}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...            ${param_channel_type_id}
        ...            ${param_channel_id}
        ...            ${param_shop_id}
        ...            ${param_mac_address}
        ...            ${param_network_provider_name}
        ...            ${param_public_ip_address}
        ...            ${param_supporting_file_1}
        ...            ${param_supporting_file_2}
        ...            ${param_device_name}
        ...            ${param_device_model}
        ...            ${param_device_unique_reference}
        ...            ${param_os}
        ...            ${param_os_version}
        ...            ${param_display_size_in_inches}
        ...            ${param_pixel_counts}
        ...            ${param_unique_number}
        ...            ${param_serial_number}
        ...            ${param_edc_serial_number}
        ...            ${param_edc_model}
        ...            ${param_edc_firmware_version}
        ...            ${param_edc_software_version}
        ...            ${param_edc_sim_card_number}
        ...            ${param_edc_battery_serial_number}
        ...            ${param_edc_adapter_serial_number}
        ...            ${param_edc_smartcard_1_number}
        ...            ${param_edc_smartcard_2_number}
        ...            ${param_edc_smartcard_3_number}
        ...            ${param_edc_smartcard_4_number}
        ...            ${param_edc_smartcard_5_number}
        ...            ${param_pos_serial_number}
        ...            ${param_pos_model}
        ...            ${param_pos_firmware_version}
        ...            ${param_pos_software_version}
        ...            ${param_pos_smartcard_1_number}
        ...            ${param_pos_smartcard_2_number}
        ...            ${param_pos_smartcard_3_number}
        ...            ${param_pos_smartcard_4_number}
        ...            ${param_pos_smartcard_5_number}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${device_id}=    convert to string   ${arg_dic.device_id}
    ${agent_update_devices_path}=    replace string      ${agent_update_devices_path}      {device_id}        ${device_id}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${agent_update_devices_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API system user updates agent device
    [Arguments]     ${arg_dic}
    ${param_channel_type_id}		[Common] - Create int param		channel_type_id		${arg_dic.channel_type_id}
    ${param_channel_id}		[Common] - Create int param		channel_id		${arg_dic.channel_id}
    ${param_shop_id}		[Common] - Create int param		shop_id		${arg_dic.shop_id}
    ${param_mac_address}		[Common] - Create string param		mac_address		${arg_dic.mac_address}
    ${param_network_provider_name}		[Common] - Create string param		network_provider_name		${arg_dic.network_provider_name}
    ${param_public_ip_address}		[Common] - Create string param		public_ip_address		${arg_dic.public_ip_address}
    ${param_supporting_file_1}		[Common] - Create string param		supporting_file_1		${arg_dic.supporting_file_1}
    ${param_supporting_file_2}		[Common] - Create string param		supporting_file_2		${arg_dic.supporting_file_2}
    ${param_device_name}		[Common] - Create string param		device_name		${arg_dic.device_name}
    ${param_device_model}		[Common] - Create string param		device_model		${arg_dic.device_model}
    ${param_device_unique_reference}		[Common] - Create string param		device_unique_reference		${arg_dic.device_unique_reference}
    ${param_os}		[Common] - Create string param		os		${arg_dic.os}
    ${param_os_version}		[Common] - Create string param		os_version		${arg_dic.os_version}
    ${param_display_size_in_inches}		[Common] - Create string param		display_size_in_inches		${arg_dic.display_size_in_inches}
    ${param_pixel_counts}		[Common] - Create string param		pixel_counts		${arg_dic.pixel_counts}
    ${param_unique_number}		[Common] - Create string param		unique_number		${arg_dic.unique_number}
    ${param_serial_number}		[Common] - Create string param		serial_number		${arg_dic.serial_number}
    ${param_app_version}		[Common] - Create string param		app_version		${arg_dic.app_version}
    ${param_edc_serial_number}		[Common] - Create string param		edc_serial_number		${arg_dic.edc_serial_number}
    ${param_edc_model}		[Common] - Create string param		edc_model		${arg_dic.edc_model}
    ${param_edc_firmware_version}		[Common] - Create string param		edc_firmware_version		${arg_dic.edc_firmware_version}
    ${param_edc_software_version}		[Common] - Create string param		edc_software_version		${arg_dic.edc_software_version}
    ${param_edc_sim_card_number}		[Common] - Create string param		edc_sim_card_number		${arg_dic.edc_sim_card_number}
    ${param_edc_battery_serial_number}		[Common] - Create string param		edc_battery_serial_number		${arg_dic.edc_battery_serial_number}
    ${param_edc_adapter_serial_number}		[Common] - Create string param		edc_adapter_serial_number		${arg_dic.edc_adapter_serial_number}
    ${param_edc_smartcard_1_number}		[Common] - Create string param		edc_smartcard_1_number		${arg_dic.edc_smartcard_1_number}
    ${param_edc_smartcard_2_number}		[Common] - Create string param		edc_smartcard_2_number		${arg_dic.edc_smartcard_2_number}
    ${param_edc_smartcard_3_number}		[Common] - Create string param		edc_smartcard_3_number		${arg_dic.edc_smartcard_3_number}
    ${param_edc_smartcard_4_number}		[Common] - Create string param		edc_smartcard_4_number		${arg_dic.edc_smartcard_4_number}
    ${param_edc_smartcard_5_number}		[Common] - Create string param		edc_smartcard_5_number		${arg_dic.edc_smartcard_5_number}
    ${param_pos_serial_number}		[Common] - Create string param		pos_serial_number		${arg_dic.pos_serial_number}
    ${param_pos_model}		[Common] - Create string param		pos_model		${arg_dic.pos_model}
    ${param_pos_firmware_version}		[Common] - Create string param		pos_firmware_version		${arg_dic.pos_firmware_version}
    ${param_pos_software_version}		[Common] - Create string param		pos_software_version		${arg_dic.pos_software_version}
    ${param_pos_smartcard_1_number}		[Common] - Create string param		pos_smartcard_1_number		${arg_dic.pos_smartcard_1_number}
    ${param_pos_smartcard_2_number}		[Common] - Create string param		pos_smartcard_2_number		${arg_dic.pos_smartcard_2_number}
    ${param_pos_smartcard_3_number}		[Common] - Create string param		pos_smartcard_3_number		${arg_dic.pos_smartcard_3_number}
    ${param_pos_smartcard_4_number}		[Common] - Create string param		pos_smartcard_4_number		${arg_dic.pos_smartcard_4_number}
    ${param_pos_smartcard_5_number}		[Common] - Create string param		pos_smartcard_5_number		${arg_dic.pos_smartcard_5_number}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...            ${param_channel_type_id}
        ...            ${param_channel_id}
        ...            ${param_shop_id}
        ...            ${param_mac_address}
        ...            ${param_network_provider_name}
        ...            ${param_public_ip_address}
        ...            ${param_supporting_file_1}
        ...            ${param_supporting_file_2}
        ...            ${param_device_name}
        ...            ${param_device_model}
        ...            ${param_device_unique_reference}
        ...            ${param_os}
        ...            ${param_os_version}
        ...            ${param_display_size_in_inches}
        ...            ${param_pixel_counts}
        ...            ${param_unique_number}
        ...            ${param_serial_number}
        ...            ${param_edc_serial_number}
        ...            ${param_edc_model}
        ...            ${param_edc_firmware_version}
        ...            ${param_edc_software_version}
        ...            ${param_edc_sim_card_number}
        ...            ${param_edc_battery_serial_number}
        ...            ${param_edc_adapter_serial_number}
        ...            ${param_edc_smartcard_1_number}
        ...            ${param_edc_smartcard_2_number}
        ...            ${param_edc_smartcard_3_number}
        ...            ${param_edc_smartcard_4_number}
        ...            ${param_edc_smartcard_5_number}
        ...            ${param_pos_serial_number}
        ...            ${param_pos_model}
        ...            ${param_pos_firmware_version}
        ...            ${param_pos_software_version}
        ...            ${param_pos_smartcard_1_number}
        ...            ${param_pos_smartcard_2_number}
        ...            ${param_pos_smartcard_3_number}
        ...            ${param_pos_smartcard_4_number}
        ...            ${param_pos_smartcard_5_number}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${device_id}=    convert to string   ${arg_dic.device_id}
    ${system_user_update_agent_devices_path}=    replace string      ${system_user_update_agent_devices_path}      {device_id}        ${device_id}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${system_user_update_agent_devices_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API system user updates agent device status
    [Arguments]     ${arg_dic}
    ${param_is_active}		[Common] - Create int param		is_active		${arg_dic.is_active}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...            ${param_is_active}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${device_id}=    convert to string   ${arg_dic.device_id}
    ${system_user_update_agent_device_status_path}=    replace string      ${system_user_update_agent_device_status_path}      {device_id}        ${device_id}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${system_user_update_agent_device_status_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API agent deletes device
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${device_id}=    convert to string   ${arg_dic.device_id}
    ${agent_delete_devices_path}=    replace string      ${agent_delete_devices_path}      {device_id}        ${device_id}   1
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${agent_delete_devices_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API system user deletes agent device
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${device_id}=    convert to string   ${arg_dic.device_id}
    ${system_user_delete_agent_devices_path}=    replace string      ${system_user_delete_agent_devices_path}      {device_id}        ${device_id}
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${system_user_delete_agent_devices_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API agent gets all devices
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${channel_id}=    convert to string   ${arg_dic.channel_id}
    ${agent_get_all_devices_path}=    replace string      ${agent_get_all_devices_path}      {channel_id}        ${channel_id}   1
                create session          api        ${api_gateway_host}
    ${response}             get request
        ...     api
        ...     ${agent_get_all_devices_path}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API system user gets agent device
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${device_id}=    convert to string   ${arg_dic.device_id}
    ${system_user_get_agent_devices_path}=    replace string      ${system_user_get_agent_devices_path}      {device_id}        ${device_id}  1
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${system_user_get_agent_devices_path}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API system user links smart card to device
    [Arguments]     ${arg_dic}
    ${param_device_id}=  [Common] - Create string param     device_id        ${arg_dic.device_id}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_device_id}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${device_id}=    convert to string   ${arg_dic.device_id}
    ${smart_card_id}=  convert to string   ${arg_dic.smart_card_id}
    ${system_user_link_smart_card_device_path}=    replace string      ${system_user_link_smart_card_device_path}      {smart_card_id}        ${smart_card_id}   1
                create session          api        ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${system_user_link_smart_card_device_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create product category
    [Arguments]     ${arg_dic}
    ${param_name}=  [Common] - Create string param     name        ${arg_dic.name}
    ${param_description}=  [Common] - Create string param     description        ${arg_dic.description}
    ${param_image_url}=  [Common] - Create string param     image_url        ${arg_dic.image_url}
    ${param_is_active}=  [Common] - Create boolean param     is_active        ${arg_dic.is_active}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_name}
        ...		${param_description}
        ...     ${param_image_url}
        ...     ${param_is_active}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_product_category_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update product category
    [Arguments]     ${arg_dic}
    ${param_name}=  [Common] - Create string param     name        ${arg_dic.name}
    ${param_description}=  [Common] - Create string param     description        ${arg_dic.description}
    ${param_image_url}=  [Common] - Create string param     image_url        ${arg_dic.image_url}
    ${param_is_active}=  [Common] - Create boolean param     is_active        ${arg_dic.is_active}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_name}
        ...		${param_description}
        ...     ${param_image_url}
        ...     ${param_is_active}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${product_category_id}=    convert to string   ${arg_dic.product_category_id}
    ${update_product_category_path}=    replace string      ${update_product_category_path}      {product_category_id}        ${product_category_id}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${update_product_category_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete product category
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${product_category_id}=    convert to string   ${arg_dic.product_category_id}
    ${delete_product_category_path}=    replace string      ${delete_product_category_path}      {product_category_id}        ${product_category_id}   1
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${delete_product_category_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API create product
    [Arguments]     ${arg_dic}
    ${param_name}=  [Common] - Create string param     name        ${arg_dic.name}
    ${param_description}=  [Common] - Create string param     description        ${arg_dic.description}
    ${param_image_url}=  [Common] - Create string param     image_url        ${arg_dic.image_url}
    ${param_denomination}=  [Common] - Create Json data for list param with quote     denomination        ${arg_dic.denomination}
    ${param_min_price}=  [Common] - Create int param     min_price        ${arg_dic.min_price}
    ${param_max_price}=  [Common] - Create int param     max_price        ${arg_dic.max_price}
    ${param_is_allow_price_range}=  [Common] - Create boolean param     is_allow_price_range        ${arg_dic.is_allow_price_range}
    ${param_product_category_id}=  [Common] - Create int param     product_category_id        ${arg_dic.product_category_id}
    ${param_payment_service_id}=  [Common] - Create int param     payment_service_id        ${arg_dic.payment_service_id}
    ${param_is_active}=  [Common] - Create boolean param     is_active        ${arg_dic.is_active}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_name}
        ...		${param_description}
        ...     ${param_image_url}
        ...     ${param_denomination}
        ...     ${param_min_price}
        ...     ${param_max_price}
        ...     ${param_is_allow_price_range}
        ...     ${param_product_category_id}
        ...     ${param_payment_service_id}
        ...     ${param_is_active}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_product_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update product
    [Arguments]     ${arg_dic}
    ${param_name}=  [Common] - Create string param     name        ${arg_dic.name}
    ${param_description}=  [Common] - Create string param     description        ${arg_dic.description}
    ${param_image_url}=  [Common] - Create string param     image_url        ${arg_dic.image_url}
    ${param_denomination}=  [Common] - Create Json data for list param with quote     denomination        ${arg_dic.denomination}
    ${param_min_price}=  [Common] - Create int param     min_price        ${arg_dic.min_price}
    ${param_max_price}=  [Common] - Create int param     max_price        ${arg_dic.max_price}
    ${param_is_allow_price_range}=  [Common] - Create boolean param     is_allow_price_range        ${arg_dic.is_allow_price_range}
    ${param_product_category_id}=  [Common] - Create int param     product_category_id        ${arg_dic.product_category_id}
    ${param_payment_service_id}=  [Common] - Create int param     payment_service_id        ${arg_dic.payment_service_id}
    ${param_is_active}=  [Common] - Create boolean param     is_active        ${arg_dic.is_active}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_name}
        ...		${param_description}
        ...     ${param_image_url}
        ...     ${param_denomination}
        ...     ${param_min_price}
        ...     ${param_max_price}
        ...     ${param_is_allow_price_range}
        ...     ${param_product_category_id}
        ...     ${param_payment_service_id}
        ...     ${param_is_active}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${product_id}=    convert to string   ${arg_dic.product_id}
    ${update_product_path}=    replace string      ${update_product_path}      {product_id}        ${product_id}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${update_product_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete product
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${product_id}=    convert to string   ${arg_dic.product_id}
    ${delete_product_path}=    replace string      ${delete_product_path}      {product_id}        ${product_id}   1
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${delete_product_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API create relation between product and agent
    [Arguments]     ${arg_dic}
    ${param_agent_id}=  [Common] - Create string param     agent_id        ${arg_dic.agent_id}
    ${param_product_id}=  [Common] - Create string param     product_id        ${arg_dic.product_id}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_agent_id}
        ...		${param_product_id}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_relation_product_agent_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete relation between product and agent
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${product_agent_relation_id}=    convert to string   ${arg_dic.product_agent_relation_id}
    ${delete_relation_product_agent_path}=    replace string      ${delete_relation_product_agent_path}      {product_agent_relation_id}        ${product_agent_relation_id}   1
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${delete_relation_product_agent_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API create relation between product and agent type
    [Arguments]     ${arg_dic}
    ${param_agent_type_id}=  [Common] - Create string param     agent_type_id        ${arg_dic.agent_type_id}
    ${param_product_id}=  [Common] - Create string param     product_id        ${arg_dic.product_id}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		${param_agent_type_id}
        ...		${param_product_id}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_relation_product_agent-type_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete relation between product and agent type
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${product_agent_type_relation_id}=    convert to string   ${arg_dic.product_agent_type_relation_id}
    ${delete_relation_product_agent-type_path}=    replace string      ${delete_relation_product_agent-type_path}      {product_agent_type_relation_id}        ${product_agent_type_relation_id}   1
                create session          api        ${api_gateway_host}
    ${response}             delete request
        ...     api
        ...     ${delete_relation_product_agent-type_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API create relation between services and agent
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...		"agent_service": [
		...         {
		...         	"service_id":"${arg_dic.service1_id}"
		...         },
		... 		{
		...         	"service_id":"${arg_dic.service2_id}"
		...         }
		...     ]
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${agent_id}=    convert to string   ${arg_dic.agent_id}
    ${create_relation_service_agent_path}=    replace string      ${create_relation_service_agent_path}      {agent_id}        ${agent_id}   1
                create session          api        ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${create_relation_service_agent_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get relation between services and agent
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${agent_id}=    convert to string   ${arg_dic.agent_id}
    ${get_relation_service_agent_path}=    replace string      ${get_relation_service_agent_path}      {agent_id}        ${agent_id}   1
                create session          api        ${api_gateway_host}
    ${response}             get request
        ...     api
        ...     ${get_relation_service_agent_path}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API get salary groups by companyId
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${company_id}=    convert to string   ${arg_dic.company_id}
    ${get_salary_groups_path}=    replace string      ${get_salary_groups_path}      {company_id}        ${company_id}    1
                create session          api        ${api_gateway_host}
    ${response}             get request
        ...     api
        ...     ${get_salary_groups_path}
        ...     ${header}
    log         ${response.text}
    ${dic} =	  Create Dictionary   response=${response}
    [Return]    ${dic}

API link agent profile with company profile
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...	  "company_id": ${arg_dic.company_id}
        ...	}
    ${data}  replace string      ${data}      ,}    }
             create session          api     ${api_gateway_host}     disable_warnings=1
    ${agent_id}=    convert to string   ${arg_dic.agent_id}
    ${link_agent_profile_with_company_profile_path}=    replace string      ${link_agent_profile_with_company_profile_path}      {agent_id}        ${agent_id}   1
    ${response}             post request
        ...     api
        ...     ${link_agent_profile_with_company_profile_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API system user update company wallet user
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
    ...		{
    ...		    "wallet_user_id": ${arg_dic.wallet_user_id},
    ...		    "wallet_user_type": {
    ...		        "id": ${arg_dic.wallet_user_type_id},
    ...		        "name": "${arg_dic.wallet_user_type_name}"
    ...		    }
    ...		}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${company_id}=    convert to string   ${arg_dic.company_id}
    ${system_user_update_company_wallet_user_path}=    replace string      ${system_user_update_company_wallet_user_path}      {company_id}        ${company_id}   1
                create session          api        ${api_gateway_host}
    ${response}             put request
        ...     api
        ...     ${system_user_update_company_wallet_user_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API create sale hierachy
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...	  "root_id": ${arg_dic.root_id},
        ...	  "name": "${arg_dic.name}",
        ...   "description": "${arg_dic.description}"
        ...	}
    ${data}  replace string      ${data}      ,}    }
             create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_sale_hierachy_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create connection
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...	  "parent_id": ${arg_dic.parent_id},
        ...	  "child_id": "${arg_dic.child_id}"
        ...	}
    ${data}  replace string      ${data}      ,}    }
             create session          api     ${api_gateway_host}     disable_warnings=1
    ${sale_hierarchy_id}=    convert to string   ${arg_dic.sale_hierarchy_id}
    ${create_connection_path}=    replace string      ${create_connection_path}      {sale_hierarchy_id}        ${sale_hierarchy_id}   1

    ${response}             post request
        ...     api
        ...     ${create_connection_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create sale permissions configurations
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...	  "hierarchy_id": ${arg_dic.hierarchy_id},
        ...	  "permission_id": "${arg_dic.permission_id}",
        ...	  "actor_type_id": "${arg_dic.actor_type_id}",
        ...	  "target_type_id": "${arg_dic.target_type_id}"
        ...	}
    ${data}  replace string      ${data}      ,}    }
             create session          api     ${api_gateway_host}     disable_warnings=1

    ${response}             post request
        ...     api
        ...     ${create_sales_permissions_configurations_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create sale profile
    [Arguments]  ${arg_dic}

    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    # Identity
    ${param_identity_type_id}		[Common] - Create int param in dic    ${arg_dic}		identity_type_id
    ${param_identity_username}		[Common] - Create string param in dic    ${arg_dic}		username
    ${param_identity_password}		[Common] - Create string param in dic    ${arg_dic}		password
    ${param_identity_auto_generate_password}		[Common] - Create boolean param in dic    ${arg_dic}		auto_generate_password
    # Profile
    ${param_is_testing_account}		[Common] - Create boolean param in dic    ${arg_dic}		is_testing_account
    ${param_is_system_account}		[Common] - Create boolean param in dic    ${arg_dic}		is_system_account
    ${param_acquisition_source}		[Common] - Create string param in dic    ${arg_dic}		acquisition_source
    ${param_referrer_user_id}		[Common] - Create int param in dic    ${arg_dic}		referrer_user_id
    ${param_referrer_user_type_id}		[Common] - Create string param of object in dic    ${arg_dic}    referrer_user_type		id
    ${param_referrer_user_type_name}		[Common] - Create string param of object in dic    ${arg_dic}    referrer_user_type		name
    ${param_agent_type_id}		[Common] - Create int param in dic    ${arg_dic}		agent_type_id
    ${param_currency}    [Common] - Create string param in dic    ${arg_dic}		currency
    ${param_unique_reference}		[Common] - Create string param in dic    ${arg_dic}		unique_reference
    ${param_mm_card_type_id}		[Common] - Create int param in dic    ${arg_dic}		mm_card_type_id
    ${param_mm_card_level_id}		[Common] - Create int param in dic    ${arg_dic}		mm_card_level_id
    ${param_mm_factory_card_number}		[Common] - Create string param in dic    ${arg_dic}		mm_factory_card_number
    ${param_model_type}		[Common] - Create string param in dic    ${arg_dic}		model_type
    ${param_is_require_otp}		[Common] - Create boolean param in dic    ${arg_dic}		is_require_otp
    ${param_agent_classification_id}		[Common] - Create int param in dic    ${arg_dic}		agent_classification_id
    ${param_tin_number}		[Common] - Create string param in dic    ${arg_dic}		tin_number
    ${param_tin_number_local}		[Common] - Create string param in dic    ${arg_dic}		tin_number_local
    ${param_title}		[Common] - Create string param in dic    ${arg_dic}		title
    ${param_title_local}            [Common] - Create string param in dic    ${arg_dic}		title_local
    ${param_first_name}		[Common] - Create string param in dic    ${arg_dic}		first_name
    ${param_first_name_local}       [Common] - Create string param in dic    ${arg_dic}		first_name_local
    ${param_middle_name}		[Common] - Create string param in dic    ${arg_dic}		middle_name
    ${param_middle_name_local}      [Common] - Create string param in dic    ${arg_dic}		middle_name_local
    ${param_last_name}		[Common] - Create string param in dic    ${arg_dic}		last_name
    ${param_last_name_local}        [Common] - Create string param in dic    ${arg_dic}		last_name_local
    ${param_suffix}		[Common] - Create string param in dic    ${arg_dic}		suffix
    ${param_suffix_local}           [Common] - Create string param in dic    ${arg_dic}		suffix_local
    ${param_date_of_birth}		[Common] - Create string param in dic    ${arg_dic}		date_of_birth
    ${param_place_of_birth}		[Common] - Create string param in dic    ${arg_dic}		place_of_birth
    ${param_place_of_birth_local}   [Common] - Create string param in dic    ${arg_dic}		place_of_birth_local
    ${param_gender}		[Common] - Create string param in dic    ${arg_dic}		gender
    ${param_gender_local}           [Common] - Create string param in dic    ${arg_dic}		gender_local
    ${param_ethnicity}		[Common] - Create string param in dic    ${arg_dic}		ethnicity
    ${param_nationality}		[Common] - Create string param in dic    ${arg_dic}		nationality
    ${param_occupation}		[Common] - Create string param in dic    ${arg_dic}		occupation
    ${param_occupation_local}       [Common] - Create string param in dic    ${arg_dic}		occupation_local
    ${param_occupation_title}		[Common] - Create string param in dic    ${arg_dic}		occupation_title
    ${param_occupation_title_local}     [Common] - Create string param in dic    ${arg_dic}		occupation_title_local
    ${param_township_code}		[Common] - Create string param in dic    ${arg_dic}		township_code
    ${param_township_name}		[Common] - Create string param in dic    ${arg_dic}		township_name
    ${param_township_name_local}        [Common] - Create string param in dic    ${arg_dic}		township_name_local
    ${param_national_id_number}		[Common] - Create string param in dic    ${arg_dic}		national_id_number
    ${param_national_id_number_local}   [Common] - Create string param in dic    ${arg_dic}		national_id_number_local
    ${param_mother_name}		[Common] - Create string param in dic    ${arg_dic}		mother_name
    ${param_mother_name_local}          [Common] - Create string param in dic    ${arg_dic}		mother_name_local
    ${param_email}		[Common] - Create string param in dic    ${arg_dic}		email
    ${param_primary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}		primary_mobile_number
    ${param_secondary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}		secondary_mobile_number
    ${param_tertiary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}		tertiary_mobile_number
    ${param_star_rating}      [Common] - Create string param in dic    ${arg_dic}		star_rating
    ${param_warning_count}       [Common] - Create string param in dic    ${arg_dic}		warning_count
    ${param_is_sale}		[Common] - Create boolean param in dic    ${arg_dic}		is_sale
    # Address
    ${param_current_address_citizen_association}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       citizen_association
    ${param_current_address_neighbourhood_association}      [Common] - Create string param of object in dic    ${arg_dic}    current_address       neighbourhood_association
    ${param_current_address_address}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       address
    ${param_current_address_address_local}      [Common] - Create string param of object in dic    ${arg_dic}    current_address		address_local
    ${param_current_address_commune}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       commune
    ${param_current_address_commune_local}      [Common] - Create string param of object in dic    ${arg_dic}    current_address		commune_local
    ${param_current_address_district}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       district
    ${param_current_address_district_local}     [Common] - Create string param of object in dic    ${arg_dic}    current_address		district_local
    ${param_current_address_city}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       city
    ${param_current_address_city_local}         [Common] - Create string param of object in dic    ${arg_dic}    current_address		city_local
    ${param_current_address_province}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       province
    ${param_current_address_province_local}     [Common] - Create string param of object in dic    ${arg_dic}    current_address		province_local
    ${param_current_address_postal_code}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       postal_code
    ${param_current_address_postal_code_local}      [Common] - Create string param of object in dic    ${arg_dic}    current_address		postal_code_local
    ${param_current_address_country}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       country
    ${param_current_address_country_local}      [Common] - Create string param of object in dic    ${arg_dic}    current_address		country_local
    ${param_current_address_landmark}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       landmark
    ${param_current_address_longitude}      [Common] - Create string param of object in dic    ${arg_dic}    current_address       longitude
    ${param_current_address_latitude}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       latitude
    ${param_permanent_address_citizen_association}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       citizen_association
    ${param_permanent_address_neighbourhood_association}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       neighbourhood_association
    ${param_permanent_address_address}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       address
    ${param_permanent_address_address_local}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		address_local
    ${param_permanent_address_commune}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       commune
    ${param_permanent_address_commune_local}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		commune_local
    ${param_permanent_address_district}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       district
    ${param_permanent_address_district_local}       [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		district_local
    ${param_permanent_address_city}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       city
    ${param_permanent_address_city_local}       [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		city_local
    ${param_permanent_address_province}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       province
    ${param_permanent_address_province_local}       [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		province_local
    ${param_permanent_address_postal_code}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       postal_code
    ${param_permanent_address_postal_code_local}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		postal_code_local
    ${param_permanent_address_country}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       country
    ${param_permanent_address_country_local}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       country_local
    ${param_permanent_address_landmark}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       landmark
    ${param_permanent_address_longitude}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       longitude
    ${param_permanent_address_latitude}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       latitude
    # Company
    ${param_company_id}		[Common] - Create int param in dic    ${arg_dic}	 company_id
    # Bank
    ${param_bank_name}		[Common] - Create string param of object in dic    ${arg_dic}		bank    name
    ${param_bank_name_local}        [Common] - Create string param of object in dic    ${arg_dic}		bank    name_local
    ${param_bank_account_status}		[Common] - Create int param of object in dic    ${arg_dic}		bank    account_status
    ${param_bank_account_name}		[Common] - Create string param of object in dic    ${arg_dic}		bank    account_name
    ${param_bank_account_name_local}		[Common] - Create string param of object in dic    ${arg_dic}		bank    account_name_local
    ${param_bank_account_number}		[Common] - Create string param of object in dic    ${arg_dic}		bank    account_number
    ${param_bank_account_number_local}		[Common] - Create string param of object in dic    ${arg_dic}		bank    account_number_local
    ${param_bank_branch_area}		[Common] - Create string param of object in dic    ${arg_dic}		bank    branch_area
    ${param_bank_branch_area_local}     [Common] - Create string param of object in dic    ${arg_dic}		bank    branch_area_local
    ${param_bank_branch_city}		[Common] - Create string param of object in dic    ${arg_dic}		bank    branch_city
    ${param_bank_branch_city_local}     [Common] - Create string param of object in dic    ${arg_dic}		bank    branch_city_local
    ${param_bank_register_date}		[Common] - Create string param of object in dic    ${arg_dic}		bank    register_date
    ${param_bank_register_source}		[Common] - Create string param of object in dic    ${arg_dic}		bank    register_source
    ${param_bank_is_verified}		[Common] - Create boolean param in dic    ${arg_dic}		is_verified
    ${param_bank_end_date}		[Common] - Create string param of object in dic    ${arg_dic}		bank    end_date
    # Contract
    ${param_contract_release}		[Common] - Create string param of object in dic    ${arg_dic}		contract    release
    ${param_contract_type}		[Common] - Create string param of object in dic    ${arg_dic}		contract    type
    ${param_contract_number}		[Common] - Create string param of object in dic    ${arg_dic}		contract    number
    ${param_contract_extension_type}		[Common] - Create string param of object in dic    ${arg_dic}		contract    extension_type
    ${param_contract_sign_date}		[Common] - Create string param of object in dic    ${arg_dic}		contract    sign_date
    ${param_contract_issue_date}		[Common] - Create string param of object in dic    ${arg_dic}		contract    issue_date
    ${param_contract_expired_date}		[Common] - Create string param of object in dic    ${arg_dic}		contract    expired_date
    ${param_contract_notification_alert}		[Common] - Create string param of object in dic    ${arg_dic}		contract    notification_alert
    ${param_contract_day_of_period_reconciliation}		[Common] - Create string param of object in dic    ${arg_dic}		contract    day_of_period_reconciliation
    ${param_contract_file_url}		[Common] - Create string param of object in dic    ${arg_dic}		contract    file_url
    ${param_contract_assessment_information_url}		[Common] - Create string param of object in dic    ${arg_dic}		contract    assessment_information_url
    # Accreditation
    ${param_primary_identity_type}      [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       type
    ${param_primary_identity_status}        [Common] - Create int param of object in dic    ${arg_dic}    primary_identity      status
    ${param_primary_identity_identity_id}       [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       identity_id
    ${param_primary_identity_identity_id_local}       [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       identity_id_local
    ${param_primary_identity_place_of_issue}        [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       place_of_issue
    ${param_primary_identity_issue_date}        [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       issue_date
    ${param_primary_identity_expired_date}      [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       expired_date
    ${param_primary_identity_front_identity_url}        [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       front_identity_url
    ${param_primary_identity_back_identity_url}     [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       back_identity_url
    ${param_secondary_identity_type}        [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       type
    ${param_secondary_identity_status}      [Common] - Create int param of object in dic    ${arg_dic}    secondary_identity      status
    ${param_secondary_identity_identity_id}     [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       identity_id
    ${param_secondary_identity_identity_id_local}     [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       identity_id_local
    ${param_secondary_identity_place_of_issue}      [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       place_of_issue
    ${param_secondary_identity_issue_date}      [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       issue_date
    ${param_secondary_identity_expired_date}        [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       expired_date
    ${param_secondary_identity_front_identity_url}      [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       front_identity_url
    ${param_secondary_identity_back_identity_url}       [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       back_identity_url
    ${param_status_id}		[Common] - Create int param in dic    ${arg_dic}		status_id
    ${param_verify_by}		[Common] - Create string param in dic    ${arg_dic}		verify_by
    ${param_remark}		[Common] - Create string param in dic    ${arg_dic}		remark
    ${param_risk_level}		[Common] - Create string param in dic    ${arg_dic}		risk_level
    # Sale
    ${param_sale_employee_id}		[Common] - Create string param in dic    ${arg_dic}		employee_id
    ${param_sale_calendar_id}		[Common] - Create string param in dic    ${arg_dic}		calendar_id
    # Additional
    ${param_additional_acquiring_sales_executive_name}		[Common] - Create string param in dic    ${arg_dic}		acquiring_sale_executive_name
    ${param_additional_acquiring_sales_executive_name_local}        [Common] - Create string param in dic    ${arg_dic}		acquiring_sale_executive_name_local
    ${param_additional_acquiring_sales_executive_id}		[Common] - Create string param in dic    ${arg_dic}		acquiring_sale_executive_id
    ${param_additional_relationship_manager_name}		[Common] - Create string param in dic    ${arg_dic}		relationship_manager_name
    ${param_additional_relationship_manager_name_local}        [Common] - Create string param in dic    ${arg_dic}		relationship_manager_name_local
    ${param_additional_relationship_manager_id}		[Common] - Create string param in dic    ${arg_dic}		relationship_manager_id
    ${param_additional_sale_region}		[Common] - Create string param in dic    ${arg_dic}		sale_region
    ${param_additional_sale_region_local}      [Common] - Create string param in dic    ${arg_dic}		sale_region_local
    ${param_additional_commercial_account_manager}		[Common] - Create string param in dic    ${arg_dic}		commercial_account_manager
    ${param_additional_commercial_account_manager_local}       [Common] - Create string param in dic    ${arg_dic}		commercial_account_manager_local
    ${param_additional_profile_picture_url}		[Common] - Create string param in dic    ${arg_dic}		profile_picture_url
    ${param_additional_national_id_photo_url}		[Common] - Create string param in dic    ${arg_dic}		national_id_photo_url
    ${param_additional_tax_id_card_photo_url}		[Common] - Create string param in dic    ${arg_dic}		tax_id_card_photo_url
    ${param_additional_field_1_name}		[Common] - Create string param in dic    ${arg_dic}		field_1_name
    ${param_additional_field_1_value}		[Common] - Create string param in dic    ${arg_dic}		field_1_value
    ${param_additional_field_2_name}		[Common] - Create string param in dic    ${arg_dic}		field_2_name
    ${param_additional_field_2_value}		[Common] - Create string param in dic    ${arg_dic}		field_2_value
    ${param_additional_field_3_name}		[Common] - Create string param in dic    ${arg_dic}		field_3_name
    ${param_additional_field_3_value}		[Common] - Create string param in dic    ${arg_dic}		field_3_value
    ${param_additional_field_4_name}		[Common] - Create string param in dic    ${arg_dic}		field_4_name
    ${param_additional_field_4_value}		[Common] - Create string param in dic    ${arg_dic}		field_4_value
    ${param_additional_field_5_name}		[Common] - Create string param in dic    ${arg_dic}		field_5_name
    ${param_additional_field_5_value}		[Common] - Create string param in dic    ${arg_dic}		field_5_value
    ${param_additional_supporting_file_1_url}		[Common] - Create string param in dic    ${arg_dic}		supporting_file_1_url
    ${param_additional_supporting_file_2_url}		[Common] - Create string param in dic    ${arg_dic}		supporting_file_2_url
    ${param_additional_supporting_file_3_url}		[Common] - Create string param in dic    ${arg_dic}		supporting_file_3_url
    ${param_additional_supporting_file_4_url}		[Common] - Create string param in dic    ${arg_dic}		supporting_file_4_url
    ${param_additional_supporting_file_5_url}		[Common] - Create string param in dic    ${arg_dic}		supporting_file_5_url

    ${data}              catenate   SEPARATOR=
    ...          {
    ...              "profile": {
    ...                 ${param_is_testing_account}
    ...                 ${param_is_system_account}
    ...                 ${param_acquisition_source}
    ...                 ${param_referrer_user_id}
    ...                 "referrer_user_type": {
    ...                     ${param_referrer_user_type_id}
    ...                     ${param_referrer_user_type_name}
    ...                 },
    ...                 ${param_agent_type_id}
    ...                 ${param_currency}
    ...                 ${param_unique_reference}
    ...                 ${param_mm_card_type_id}
    ...                 ${param_mm_card_level_id}
    ...                 ${param_mm_factory_card_number}
    ...                 ${param_model_type}
    ...                 ${param_is_require_otp}
    ...                 ${param_agent_classification_id}
    ...                 ${param_tin_number}
    ...                 ${param_tin_number_local}
    ...                 ${param_title}
    ...                 ${param_title_local}
    ...                 ${param_first_name}
    ...                 ${param_first_name_local}
    ...                 ${param_middle_name}
    ...                 ${param_middle_name_local}
    ...                 ${param_last_name}
    ...                 ${param_last_name_local}
    ...                 ${param_suffix}
    ...                 ${param_suffix_local}
    ...                 ${param_date_of_birth}
    ...                 ${param_place_of_birth}
    ...                 ${param_place_of_birth_local}
    ...                 ${param_gender}
    ...                 ${param_gender_local}
    ...                 ${param_ethnicity}
    ...                 ${param_nationality}
    ...                 ${param_occupation}
    ...                 ${param_occupation_local}
    ...                 ${param_occupation_title}
    ...                 ${param_occupation_title_local}
    ...                 ${param_township_code}
    ...                 ${param_township_name}
    ...                 ${param_township_name_local}
    ...                 ${param_national_id_number}
    ...                 ${param_national_id_number_local}
    ...                 ${param_mother_name}
    ...                 ${param_mother_name_local}
    ...                 ${param_email}
    ...                 ${param_primary_mobile_number}
    ...                 ${param_secondary_mobile_number}
    ...                 ${param_tertiary_mobile_number}
    ...                 ${param_star_rating}
    ...                 ${param_warning_count}
    ...                 ${param_is_sale}
    ...                 ${param_company_id}
    ...                 "address": {
    ...                     "current_address": {
    ...      					${param_current_address_citizen_association}
    ...      					${param_current_address_neighbourhood_association}
    ...      					${param_current_address_address}
    ...                         ${param_current_address_address_local}
    ...      					${param_current_address_commune}
    ...                         ${param_current_address_commune_local}
    ...      					${param_current_address_district}
    ...                         ${param_current_address_district_local}
    ...      					${param_current_address_city}
    ...                         ${param_current_address_city_local}
    ...      					${param_current_address_province}
    ...                         ${param_current_address_province_local}
    ...      					${param_current_address_postal_code}
    ...                         ${param_current_address_postal_code_local}
    ...      					${param_current_address_country}
    ...                         ${param_current_address_country_local}
    ...      					${param_current_address_landmark}
    ...      					${param_current_address_longitude}
    ...      					${param_current_address_latitude}
    ...                     },
    ...                     "permanent_address": {
    ...      					${param_permanent_address_citizen_association}
    ...      					${param_permanent_address_neighbourhood_association}
    ...      					${param_permanent_address_address}
    ...                         ${param_permanent_address_address_local}
    ...      					${param_permanent_address_commune}
    ...                         ${param_permanent_address_commune_local}
    ...      					${param_permanent_address_district}
    ...                         ${param_permanent_address_district_local}
    ...      					${param_permanent_address_city}
    ...                         ${param_permanent_address_city_local}
    ...      					${param_permanent_address_province}
    ...                         ${param_permanent_address_province_local}
    ...      					${param_permanent_address_postal_code}
    ...                         ${param_permanent_address_postal_code_local}
    ...      					${param_permanent_address_country}
    ...                         ${param_permanent_address_country_local}
    ...      					${param_permanent_address_landmark}
    ...      					${param_permanent_address_longitude}
    ...      					${param_permanent_address_latitude}
    ...                     }
    ...                 },
    ...                 "bank":{
    ...      				${param_bank_name}
    ...                     ${param_bank_name_local}
    ...      				${param_bank_account_status}
    ...      				${param_bank_account_name}
    ...      				${param_bank_account_name_local}
    ...      				${param_bank_account_number}
    ...      				${param_bank_account_number_local}
    ...      				${param_bank_branch_area}
    ...                     ${param_bank_branch_area_local}
    ...      				${param_bank_branch_city}
    ...                     ${param_bank_branch_city_local}
    ...      				${param_bank_register_date}
    ...      				${param_bank_register_source}
    ...      				${param_bank_is_verified}
    ...      				${param_bank_end_date}
    ...                 },
    ...                 "contract": {
    ...      				${param_contract_release}
    ...      				${param_contract_type}
    ...      				${param_contract_number}
    ...      				${param_contract_extension_type}
    ...      				${param_contract_sign_date}
    ...      				${param_contract_issue_date}
    ...      				${param_contract_expired_date}
    ...      				${param_contract_notification_alert}
    ...      				${param_contract_day_of_period_reconciliation}
    ...      				${param_contract_file_url}
    ...      				${param_contract_assessment_information_url}
    ...                  },
    ...                 "accreditation": {
    ...                     "primary_identity": {
    ...      					${param_primary_identity_type}
    ...      					${param_primary_identity_status}
    ...      					${param_primary_identity_identity_id}
    ...                         ${param_primary_identity_identity_id_local}
    ...      					${param_primary_identity_place_of_issue}
    ...      					${param_primary_identity_issue_date}
    ...      					${param_primary_identity_expired_date}
    ...      					${param_primary_identity_front_identity_url}
    ...      					${param_primary_identity_back_identity_url}
    ...                     },
    ...                     "secondary_identity": {
    ...      					${param_secondary_identity_type}
    ...      					${param_secondary_identity_status}
    ...      					${param_secondary_identity_identity_id}
    ...                         ${param_secondary_identity_identity_id_local}
    ...      					${param_secondary_identity_place_of_issue}
    ...      					${param_secondary_identity_issue_date}
    ...      					${param_secondary_identity_expired_date}
    ...      					${param_secondary_identity_front_identity_url}
    ...      					${param_secondary_identity_back_identity_url}
    ...                     },
    ...      				${param_status_id}
    ...      				${param_verify_by}
    ...      				${param_remark}
    ...      				${param_risk_level}
    ...                 },
    ...                 "additional": {
    ...      				${param_additional_acquiring_sales_executive_name}
    ...      				${param_additional_acquiring_sales_executive_name_local}
    ...      				${param_additional_acquiring_sales_executive_id}
    ...      				${param_additional_relationship_manager_name}
    ...      				${param_additional_relationship_manager_name_local}
    ...      				${param_additional_relationship_manager_id}
    ...      				${param_additional_sale_region}
    ...      				${param_additional_sale_region_local}
    ...      				${param_additional_commercial_account_manager}
    ...      				${param_additional_commercial_account_manager_local}
    ...      				${param_additional_profile_picture_url}
    ...      				${param_additional_national_id_photo_url}
    ...      				${param_additional_tax_id_card_photo_url}
    ...      				${param_additional_field_1_name}
    ...      				${param_additional_field_1_value}
    ...      				${param_additional_field_2_name}
    ...      				${param_additional_field_2_value}
    ...      				${param_additional_field_3_name}
    ...      				${param_additional_field_3_value}
    ...      				${param_additional_field_4_name}
    ...      				${param_additional_field_4_value}
    ...      				${param_additional_field_5_name}
    ...      				${param_additional_field_5_value}
    ...      				${param_additional_supporting_file_1_url}
    ...      				${param_additional_supporting_file_2_url}
    ...      				${param_additional_supporting_file_3_url}
    ...      				${param_additional_supporting_file_4_url}
    ...      				${param_additional_supporting_file_5_url}
    ...                 },
    ...                 "sale": {
    ...                     ${param_sale_employee_id}
    ...                     ${param_sale_calendar_id}
    ...                 }
    ...              },
    ...              "identity": {
    ...      			${param_identity_type_id}
    ...      			${param_identity_username}
    ...      			${param_identity_password}
    ...      			${param_identity_auto_generate_password}
    ...              }
    ...          }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
    create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_agent_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${data}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create shop with dicionary params
    [Arguments]     ${arg_dic}

    ${header}               create dictionary
        ...     client_id=${setup_admin_client_id}
        ...     client_secret=${setup_admin_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_agent_id}		[Common] - Create int param in dic     ${arg_dic}      agent_id
    ${param_shop_category_id}		[Common] - Create int param in dic    ${arg_dic} 	shop_category_id
    ${param_name}		[Common] - Create string param in dic    ${arg_dic} 	name
    ${param_name_local}		[Common] - Create string param in dic    ${arg_dic} 	name_local
    ${param_acquisition_source}		[Common] - Create string param in dic    ${arg_dic} 	acquisition_source
    ${param_shop_mobile_number}		[Common] - Create string param in dic    ${arg_dic} 	shop_mobile_number
    ${param_shop_telephone_number}		[Common] - Create string param in dic    ${arg_dic} 	shop_telephone_number
    ${param_shop_email}		[Common] - Create string param in dic    ${arg_dic} 	shop_email
    ${param_truemoney_signage}		[Common] - Create string param in dic    ${arg_dic} 	truemoney_signage
    ${param_requested_services}		[Common] - Create string param in dic    ${arg_dic} 	requested_services
    ${param_network_coverage}		[Common] - Create string param in dic    ${arg_dic} 	network_coverage
    ${param_premises}		[Common] - Create string param in dic    ${arg_dic} 	premises
    ${param_physical_security}		[Common] - Create string param in dic    ${arg_dic} 	physical_security
    ${param_number_of_staff}		[Common] - Create string param in dic    ${arg_dic} 	number_of_staff
    ${param_business_hour}		[Common] - Create string param in dic    ${arg_dic} 	business_hour
    ${param_shop_type_id}		[Common] - Create string param in dic    ${arg_dic} 	shop_type_id
    ${param_address_citizen_association}		[Common] - Create string param in dic    ${arg_dic} 	citizen_association
    ${param_address_neighbourhood_association}		[Common] - Create string param in dic    ${arg_dic} 	neighbourhood_association
    ${param_address_address}		[Common] - Create string param in dic    ${arg_dic} 	address
    ${param_address_commune}		[Common] - Create string param in dic    ${arg_dic} 	commune
    ${param_address_district}		[Common] - Create string param in dic    ${arg_dic} 	district
    ${param_address_city}		[Common] - Create string param in dic    ${arg_dic} 	city
    ${param_address_province}		[Common] - Create string param in dic    ${arg_dic} 	province
    ${param_address_postal_code}		[Common] - Create string param in dic    ${arg_dic} 	postal_code
    ${param_address_country}		[Common] - Create string param in dic    ${arg_dic} 	country
    ${param_address_landmark}		[Common] - Create string param in dic    ${arg_dic} 	landmark
    ${param_address_longitude}		[Common] - Create string param in dic    ${arg_dic} 		longitude
    ${param_address_latitude}		[Common] - Create string param in dic    ${arg_dic} 	latitude
    ${param_address_citizen_association_local}		[Common] - Create string param in dic    ${arg_dic} 	citizen_association_local
    ${param_address_neighbourhood_association_local}		[Common] - Create string param in dic    ${arg_dic} 	neighbourhood_association_local
    ${param_address_address_local}		            [Common] - Create string param in dic    ${arg_dic} 	address_local
    ${param_address_commune_local}		            [Common] - Create string param in dic    ${arg_dic} 	commune_local
    ${param_address_district_local}		            [Common] - Create string param in dic    ${arg_dic} 	district_local
    ${param_address_city_local}		                [Common] - Create string param in dic    ${arg_dic} 	city_local
    ${param_address_province_local}		            [Common] - Create string param in dic    ${arg_dic} 	province_local
    ${param_address_postal_code_local}		        [Common] - Create string param in dic    ${arg_dic} 	postal_code_local
    ${param_address_country_local}		    [Common] - Create string param in dic    ${arg_dic} 	country_local
    ${param_address_landmark_local}		            [Common] - Create string param in dic    ${arg_dic} 	landmark_local

    ${param_additional_supporting_file_1_url}		[Common] - Create string param in dic    ${arg_dic} 	supporting_file_1_url
    ${param_additional_supporting_file_2_url}		[Common] - Create string param in dic    ${arg_dic} 	supporting_file_2_url
    ${param_ref_1}		                            [Common] - Create string param in dic    ${arg_dic} 	ref1
    ${param_ref_2}		                            [Common] - Create string param in dic    ${arg_dic}    	ref2
    ${param_ref_1_local}		                    [Common] - Create string param in dic    ${arg_dic} 	ref1_local
    ${param_ref_2_local}		                    [Common] - Create string param in dic    ${arg_dic} 	ref2_local
    ${param_additional_acquiring_sale_executive_name}		                            [Common] - Create string param in dic    ${arg_dic} 	acquiring_sale_executive_name
    ${param_additional_acquiring_sale_executive_name_local}		                            [Common] - Create string param in dic    ${arg_dic} 	acquiring_sale_executive_name_local
    ${param_additional_relationship_manager_name}		                            [Common] - Create string param in dic    ${arg_dic} 	relationship_manager_name
    ${param_additional_relationship_manager_name_local}		                            [Common] - Create string param in dic    ${arg_dic} 	relationship_manager_name_local
    ${param_additional_relationship_manager_id}		                            [Common] - Create string param in dic    ${arg_dic} 	relationship_manager_id
    ${param_additional_relationship_manager_email}		                            [Common] - Create string param in dic    ${arg_dic} 	relationship_manager_email
    ${param_additional_sale_region}		                            [Common] - Create string param in dic    ${arg_dic} 	sale_region
    ${param_additional_account_manager_name}		                            [Common] - Create string param in dic    ${arg_dic} 	account_manager_name
    ${param_additional_account_manager_name_local}		                            [Common] - Create string param in dic    ${arg_dic}     account_manager_name_local
    ${param_additional_shop_photo}		                            [Common] - Create string param in dic    ${arg_dic}     shop_photo
    ${param_additional_shop_map}		                            [Common] - Create string param in dic    ${arg_dic}     shop_map
    ${param_representative_first_name}		                            [Common] - Create string param in dic    ${arg_dic}     first_name
    ${param_representative_first_name_local}		                            [Common] - Create string param in dic    ${arg_dic}     first_name_local
    ${param_representative_middle_name}		                            [Common] - Create string param in dic    ${arg_dic}     middle_name
    ${param_representative_middle_name_local}		                            [Common] - Create string param in dic    ${arg_dic}     middle_name_local
    ${param_representative_last_name}		                            [Common] - Create string param in dic    ${arg_dic}     last_name
    ${param_representative_last_name_local}		                            [Common] - Create string param in dic    ${arg_dic}     last_name_local
    ${param_representative_mobile_number}		                            [Common] - Create string param in dic    ${arg_dic}     mobile_number
    ${param_representative_telephone_number}		                            [Common] - Create string param in dic    ${arg_dic}     telephone_number
    ${param_representative_email}		                            [Common] - Create string param in dic    ${arg_dic}     email


    ${data}              catenate   SEPARATOR=
    ...          {
    ...                 ${param_agent_id}
    ...                 ${param_shop_type_id}
    ...                 ${param_name}
    ...                 ${param_name_local}
    ...                 ${param_shop_category_id}
    ...                 ${param_acquisition_source}
    ...                 ${param_truemoney_signage}
    ...                 ${param_requested_services}
    ...                 ${param_network_coverage}
    ...                 ${param_physical_security}
    ...                 ${param_premises}
    ...                 ${param_number_of_staff}
    ...                 ${param_business_hour}
    ...                 ${param_shop_mobile_number}
    ...                 ${param_shop_telephone_number}
    ...                 ${param_shop_email}
    ...                 "address": {
    ...      					${param_address_citizen_association}
    ...      					${param_address_neighbourhood_association}
    ...      					${param_address_address}
    ...      					${param_address_commune}
    ...      					${param_address_district}
    ...      					${param_address_city}
    ...      					${param_address_province}
    ...      					${param_address_postal_code}
    ...      					${param_address_country}
    ...      					${param_address_landmark}
    ...      					${param_address_longitude}
    ...      					${param_address_latitude}
     ...      					${param_address_citizen_association_local}
    ...      					${param_address_neighbourhood_association_local}
    ...      					${param_address_address_local}
    ...      					${param_address_commune_local}
    ...      					${param_address_district_local}
    ...      					${param_address_city_local}
    ...      					${param_address_province_local}
    ...      					${param_address_postal_code_local}
    ...      					${param_address_country_local}
    ...      					${param_address_landmark_local}
    ...                 },
    ...                 "additional": {
    ...      				${param_additional_supporting_file_1_url}
    ...      				${param_additional_supporting_file_2_url}
    ...      				${param_ref_1}
    ...      				${param_ref_2}
    ...      				${param_ref_1_local}
    ...      				${param_ref_2_local}
    ...      				${param_additional_relationship_manager_id}
    ...      				${param_additional_relationship_manager_name}
    ...      				${param_additional_relationship_manager_name_local}
    ...      				${param_additional_relationship_manager_email}
    ...      				${param_additional_acquiring_sale_executive_name}
    ...      				${param_additional_acquiring_sale_executive_name_local}
    ...      				${param_additional_sale_region}
    ...      				${param_additional_account_manager_name}
    ...      				${param_additional_account_manager_name_local}
    ...      				${param_additional_shop_photo}
    ...      				${param_additional_shop_map}
    ...                 },
    ...                 "representative": {
    ...      				${param_representative_first_name}
    ...      				${param_representative_first_name_local}
    ...      				${param_representative_last_name}
    ...      				${param_representative_last_name_local}
    ...      				${param_representative_middle_name}
    ...      				${param_representative_middle_name_local}
    ...      				${param_representative_mobile_number}
    ...      				${param_representative_telephone_number}
    ...      				${param_representative_email}
    ...                 }
    ...          }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
    create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_agent_shop_api_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${header}
    log        ${data}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}
