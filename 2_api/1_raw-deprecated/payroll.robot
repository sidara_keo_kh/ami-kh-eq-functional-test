*** Settings ***
Resource        ../../1_common/keywords.robot


*** Keywords ***
API bank agent create and execute payroll order
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_payer_user_ref}    create json data for user ref object        ${arg_dic.payer_user_ref_type}      ${arg_dic.payer_user_ref_value}
    ${param_payer_user_id}     create json data for user id object         ${arg_dic.payer_user_id}      ${arg_dic.payer_user_type}
    ${param_payee_user_ref}    create json data for user ref object         ${arg_dic.payee_user_ref_type}      ${arg_dic.payee_user_ref_value}
    ${param_payee_user_id}     create json data for user id object         ${arg_dic.payee_user_id}      ${arg_dic.payee_user_type}
    ${param_requestor_user_id}         [Common] - Create string param of object in dic    ${arg_dic}    requestor_user        user_id
    ${param_requestor_user_type_name}  [Common] - Create string param of object in dic    ${arg_dic}    requestor_user        user_type
    ${param_requestor_sof_id}          [Common] - Create string param of object in dic    ${arg_dic}    requestor_user_sof    id
    ${param_requestor_sof_type_id}     [Common] - Create string param of object in dic    ${arg_dic}    requestor_user_sof    type_id

    ${request}              catenate       SEPARATOR=
        ...    {
        ...     "ext_transaction_id":"${arg_dic.ext_transaction_id}",
        ...     "product_service": {
        ...         "id": ${arg_dic.product_service_id},
        ...         "name":"${arg_dic.product_service_name}"
        ...         },
        ...     "product": {
        ...         "product_name":"${arg_dic.product_name}",
        ...         "product_ref1":"${arg_dic.product_ref1}",
        ...         "product_ref2":"${arg_dic.product_ref2}",
        ...         "product_ref3":"${arg_dic.product_ref3}",
        ...         "product_ref4":"${arg_dic.product_ref4}",
        ...         "product_ref5":"${arg_dic.product_ref5}"
        ...         },
        ...     "requestor_user":{
        ...         ${param_requestor_user_id}
        ...         ${param_requestor_user_type_name}
        ...         "sof":{
        ...             ${param_requestor_sof_id}
        ...             ${param_requestor_sof_type_id}
        ...         },
        ...     },
        ...     "payer_user": {
        ...         ${param_payer_user_ref}
        ...         ${param_payer_user_id}
        ...     },
        ...     "payee_user": {
        ...         ${param_payee_user_ref}
        ...         ${param_payee_user_id}
        ...     },
        ...     "amount": ${arg_dic.amount}
        ...     }
    ${request}  replace string      ${request}      ,}    }
    ${request}  replace string      ${request}      },,    },
                create session          api     ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${bank_agent_create_and_execute_payroll_order_path}
        ...     ${request}
        ...     ${NONE}
        ...     ${header}
    log    ${request}
    log    ${response.text}
    ${dic}  Create Dictionary   response=${response}
    [Return]    ${dic}

API system create and execute payroll order
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     X-IP-ADDRESS=${IP-ADDRESS}
        ...     Authorization=Bearer ${arg_dic.access_token}

#    [Common] - Create string param in dic
#    ${status}   run keyword and return status       Dictionary Should Contain Key     ${arg_dic}          payee_user_ref_type
#    ${payee_user_ref_param}     run keyword if      '${status}'=='True'   create json data for user ref object    ${arg_dic.payee_user_ref_type}      ${arg_dic.payee_user_ref_value}
#
#    ${status}   run keyword and return status       Dictionary Should Contain Key     ${arg_dic}          payee_user_id
#    ${payee_user_id_param}     run keyword if      '${status}'=='True'   create json data for user id object         ${arg_dic.payee_user_id}      ${arg_dic.payee_user_type}

    ${param_payee_user_ref}     [Common] - create json data for user ref object in dic      ${arg_dic}      payee_user_ref_type
    ${param_payee_user_id}      [Common] - create json data for user id object in dic      ${arg_dic}      payee_user_id

    ${request}              catenate       SEPARATOR=
        ...    {
        ...     "ext_transaction_id":"${arg_dic.ext_transaction_id}",
        ...     "product_service": {
        ...         "id": ${arg_dic.product_service_id},
        ...         "name":"${arg_dic.product_service_name}"
        ...         },
        ...     "product": {
        ...         "product_name":"${arg_dic.product_name}",
        ...         "product_ref1":"${arg_dic.product_ref1}",
        ...         "product_ref2":"${arg_dic.product_ref2}",
        ...         "product_ref3":"${arg_dic.product_ref3}",
        ...         "product_ref4":"${arg_dic.product_ref4}",
        ...         "product_ref5":"${arg_dic.product_ref5}"
        ...       },
        ...       "payer_user": {
        ...         "user_id": "${arg_dic.payer_user_id}",
        ...         "user_type": "${arg_dic.payer_user_type}"
        ...       },
        ...       "payee_user": {
        ...         ${param_payee_user_ref}
        ...         ${param_payee_user_id}
        ...       },
        ...     "amount": ${arg_dic.amount}
        ...     }
                create session          api     ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${system_create_and_execute_payroll_order_path}
        ...     ${request}
        ...     ${NONE}
        ...     ${header}
    log    ${request}
    log    ${response.text}
    ${dic}  Create Dictionary   response=${response}
    [Return]    ${dic}
