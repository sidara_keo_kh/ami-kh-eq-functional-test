*** Settings ***
Resource        ../../1_common/keywords.robot

*** Keywords ***
[Inventory] - API create single item
    [Arguments]    ${arg_dic}
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${key_type_dic}    create dictionary
    ...    name    str
    ...    type    str
    ...    description    str
    ...    image_url    str
    ...    local_sku    str
    ...    upc    str
    ...    has_serial_number    bool
    ${key_value_dic}    [Common] - build body request dict from list    ${key_type_dic}    ${arg_dic}
    ${data}    [Common] - create json string from dict    ${key_value_dic}   ${key_type_dic}
    create session    api    ${api_gateway_host}    disable_warnings=1
    ${response}    post request
    ...     api
    ...     ${inventory_create_single_sku_item_path}
    ...     ${data}
    ...     ${NONE}
    ...     ${headers}
    log    ${data}
    log    ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}

[Inventory] - API update single item
    [Arguments]    ${arg_dic}
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${key_type_dic}    create dictionary
    ...    name    str
    ...    description    str
    ...    image_url    str
    ...    local_sku    str
    ...    upc    str
    ${key_value_dic}    [Common] - build body request dict from list    ${key_type_dic}    ${arg_dic}
    ${data}    [Common] - create json string from dict    ${key_value_dic}   ${key_type_dic} 
    ${item_id}    convert to string    ${arg_dic.item_id}
    ${inventory_update_single_sku_item_by_id_path}=    replace string    
    ...    ${inventory_update_single_sku_item_by_id_path}    
    ...    {itemId}    
    ...    ${item_id}    
    ...    1
    create session    api    ${api_gateway_host}    disable_warnings=1
    ${response}    put request    
    ...    api    
    ...    ${inventory_update_single_sku_item_by_id_path}    
    ...    ${data}    
    ...    ${NONE}    
    ...    ${NONE}    
    ...    ${headers}
    log    ${data}
    log    ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}

[Inventory] - API create single item serial
    [Arguments]    ${arg_dic}
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${data}    catenate
    ...    {
    ...    "serial_number":"${arg_dic.serial_number}"
    ...    }
    ${item_id}    convert to string    ${arg_dic.item_id}
    ${inventory_create_single_sku_item_serial_path}=    replace string    
    ...    ${inventory_create_single_sku_item_serial_path}    
    ...    {itemId}    
    ...    ${item_id}    
    ...    1
    create session    api    ${api_gateway_host}    disable_warnings=1
    ${response}    post request
    ...     api
    ...     ${inventory_create_single_sku_item_serial_path}
    ...     ${data}
    ...     ${NONE}
    ...     ${headers}
    log    ${data}
    log    ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}
    
[Inventory] - API update single item quantity
    [Arguments]    ${arg_dic}
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${data}    catenate
    ...    {
    ...    "available_quantity":"${arg_dic.available_quantity}"
    ...    }
    ${item_id}    convert to string    ${arg_dic.item_id}
    ${inventory_update_single_sku_item_quantity_path}=    replace string    
    ...    ${inventory_update_single_sku_item_quantity_path}    
    ...    {itemId}    
    ...    ${item_id}    
    ...    1
    create session    api    ${api_gateway_host}    disable_warnings=1
    ${response}    put request    
    ...    api    
    ...    ${inventory_update_single_sku_item_quantity_path}    
    ...    ${data}    
    ...    ${NONE}    
    ...    ${NONE}    
    ...    ${headers}
    log    ${data}
    log    ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}

[Inventory] - API get single item
    [Arguments]    ${arg_dic}
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${item_id}    convert to string    ${arg_dic.item_id}
    ${inventory_get_single_sku_item_by_id_path}=    replace string    
    ...    ${inventory_get_single_sku_item_by_id_path}    
    ...    {itemId}    
    ...    ${item_id}    
    ...    1
    create session          api     ${api_gateway_host}   disable_warnings=1
    ${response}    get request    
    ...    api    
    ...    ${inventory_get_single_sku_item_by_id_path}    
    ...    ${headers}
    log    ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}
    
[Inventory] - API get single item by sku
    [Arguments]    ${arg_dic}
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${sku}    convert to string    ${arg_dic.sku}
    ${inventory_get_single_sku_item_by_sku_path}=    replace string    
    ...    ${inventory_get_single_sku_item_by_sku_path}    
    ...    {sku}    
    ...    ${sku}    
    ...    1
    create session          api     ${api_gateway_host}   disable_warnings=1
    ${response}    get request    
    ...    api    
    ...    ${inventory_get_single_sku_item_by_sku_path}    
    ...    ${headers}
    log    ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}
    
[Inventory] - API get single item serial
    [Arguments]    ${arg_dic}
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${item_id}    convert to string    ${arg_dic.item_id}
    ${inventory_get_single_sku_item_serial_path}=    replace string    
    ...    ${inventory_get_single_sku_item_serial_path}    
    ...    {itemId}    
    ...    ${item_id}    
    ...    1
    create session          api     ${api_gateway_host}   disable_warnings=1
    ${response}    get request    
    ...    api    
    ...    ${inventory_get_single_sku_item_serial_path}    
    ...    ${headers}
    log    ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}
    
[Inventory] - API get single item serial filter by serial
    [Arguments]    ${arg_dic}
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${item_id}    convert to string    ${arg_dic.item_id}
    ${inventory_get_single_sku_item_serial_path}=    replace string    
    ...    ${inventory_get_single_sku_item_serial_path}    
    ...    {itemId}    
    ...    ${item_id}    
    ...    1
    ${params}    create dictionary
    ...    serial=${arg_dic.item_serial_number}
    create session          api     ${api_gateway_host}   disable_warnings=1
    ${response}    get request    
    ...    api    
    ...    ${inventory_get_single_sku_item_serial_path}    
    ...    headers=${headers}
    ...    params=${params}
    log    ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}

[Inventory] - API get group
    [Arguments]    ${arg_dic}
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${group_id}    convert to string    ${arg_dic.group_id}
    ${inventory_get_group_by_id_path}=    replace string
    ...    ${inventory_get_group_by_id_path}
    ...    {groupId}
    ...    ${group_id}
    ...    1
    create session          api     ${api_gateway_host}   disable_warnings=1
    ${response}    get request
    ...    api
    ...    ${inventory_get_group_by_id_path}
    ...    ${headers}
    log    ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}

[Inventory] - API create group item
    [Arguments]    ${arg_dic}
    ${data}=    evaluate    json.dumps(${arg_dic})    json
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    create session    api    ${api_gateway_host}    disable_warnings=1
    ${response}    post request
    ...    api
    ...    ${inventory_create_group_item_path}
    ...    ${data}
    ...    ${NONE}
    ...    ${headers}
    log    ${data}
    log    ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}

[Inventory] - API update group
    [Arguments]    ${arg_dic}
    ${data}=    evaluate    json.dumps(${arg_dic})    json
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${group_id}    convert to string    ${arg_dic.group_id}
    ${inventory_update_group_item_by_id_path}=    replace string
    ...    ${inventory_update_group_item_by_id_path}
    ...    {groupId}
    ...    ${group_id}
    ...    1
    create session    api    ${api_gateway_host}    disable_warnings=1
    ${response}    put request
    ...    api
    ...    ${inventory_update_group_item_by_id_path}
    ...    ${data}
    ...    ${NONE}
    ...    ${NONE}
    ...    ${headers}
    log    ${data}
    log    ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}

[Inventory] - API create ticket with serial and non-serial
    [Arguments]    ${arg_dic}
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${is_sell_ticket}    run keyword and return status    should be equal as strings    Sell    ${arg_dic.ticket_type}
    ${sell_price_1}    run keyword if    ${is_sell_ticket}    catenate
    ...    "unit_price": ${arg_dic.item_unit_price_1},
    ...    "additional_price": ${arg_dic.item_additional_price_1},
    ...    ELSE    catenate    ${EMPTY}
    ${sell_price_2}    run keyword if    ${is_sell_ticket}    catenate
    ...    "unit_price": ${arg_dic.item_unit_price_2},
    ...    "additional_price": ${arg_dic.item_additional_price_2},
    ...    ELSE    catenate    ${EMPTY}
    ${data}    catenate    
    ...    {
    ...        "ticket_type":"${arg_dic.ticket_type}",
    ...        "receiver_id": ${arg_dic.receiver_id},
    ...        "receiver_type": "${arg_dic.receiver_type}",
    ...        "receiver_name": "${arg_dic.receiver_name}",
    ...        "creator_id": ${arg_dic.creator_id},
    ...        "creator_type": "${arg_dic.creator_type}",
    ...        "creator_name": "${arg_dic.creator_name}",
    ...        "creator_note": "${arg_dic.creator_note}",
    ...        "ticket_items": [
    ...        {
    ...            ${sell_price_1}
    ...            "id": ${arg_dic.item_id_1},
    ...            "item_serials": [{"serial_number": "${arg_dic.item_serial_number_1}"}]
    ...        },
    ...        {
    ...            ${sell_price_2}
    ...            "id": ${arg_dic.item_id_2},
    ...            "quantity": ${arg_dic.item_quantity_2}
    ...        }
    ...        ]
    ...    }
    create session    api    ${api_gateway_host}    disable_warnings=1
    ${response}    post request
    ...     api
    ...     ${inventory_create_ticket_path}
    ...     ${data}
    ...     ${NONE}
    ...     ${headers}
    log    ${data}
    log    ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}

[Inventory] - API create ticket with serial
    [Arguments]    ${arg_dic}
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${is_sell_ticket}    run keyword and return status    should be equal as strings    Sell    ${arg_dic.ticket_type}
    ${sell_price}        run keyword if    ${is_sell_ticket}    catenate
    ...    "unit_price": ${arg_dic.item_unit_price},
    ...    "additional_price": ${arg_dic.item_additional_price},
    ...    ELSE    catenate    ${EMPTY}
    ${data}    catenate    
    ...    {
    ...        "ticket_type":"${arg_dic.ticket_type}",
    ...        "receiver_id": ${arg_dic.receiver_id},
    ...        "receiver_type": "${arg_dic.receiver_type}",
    ...        "receiver_name": "${arg_dic.receiver_name}",
    ...        "creator_id": ${arg_dic.creator_id},
    ...        "creator_type": "${arg_dic.creator_type}",
    ...        "creator_name": "${arg_dic.creator_name}",
    ...        "creator_note": "${arg_dic.creator_note}",
    ...        "ticket_items": [
    ...        {
    ...            ${sell_price}
    ...            "id": ${arg_dic.item_id},
    ...            "item_serials": [{"serial_number": "${arg_dic.item_serial_number}"}]
    ...        }
    ...        ]
    ...    }
    create session    api    ${api_gateway_host}    disable_warnings=1
    ${response}    post request
    ...     api
    ...     ${inventory_create_ticket_path}
    ...     ${data}
    ...     ${NONE}
    ...     ${headers}
    log     ${data}
    log     ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}

[Inventory] - API create ticket with non-serial
    [Arguments]    ${arg_dic}
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${is_sell_ticket}    run keyword and return status    should be equal as strings    Sell    ${arg_dic.ticket_type}
    ${sell_price}    run keyword if    ${is_sell_ticket}    catenate
    ...    "unit_price": ${arg_dic.item_unit_price},
    ...    "additional_price": ${arg_dic.item_additional_price},
    ...    ELSE    catenate    ${EMPTY}
    ${data}    catenate    
    ...    {
    ...        "ticket_type":"${arg_dic.ticket_type}",
    ...        "receiver_id": ${arg_dic.receiver_id},
    ...        "receiver_type": "${arg_dic.receiver_type}",
    ...        "receiver_name": "${arg_dic.receiver_name}",
    ...        "creator_id": ${arg_dic.creator_id},
    ...        "creator_type": "${arg_dic.creator_type}",
    ...        "creator_name": "${arg_dic.creator_name}",
    ...        "creator_note": "${arg_dic.creator_note}",
    ...        "ticket_items": [
    ...        {
    ...            ${sell_price}
    ...            "id": ${arg_dic.item_id},
    ...            "quantity": ${arg_dic.item_quantity}
    ...        }
    ...        ]
    ...    }
    create session    api    ${api_gateway_host}    disable_warnings=1
    ${response}    post request
    ...     api
    ...     ${inventory_create_ticket_path}
    ...     ${data}
    ...     ${NONE}
    ...     ${headers}
    log     ${data}
    log     ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}

[Inventory] - API get ticket
    [Arguments]    ${arg_dic}
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${ticket_id}    convert to string    ${arg_dic.ticket_id}
    ${inventory_get_ticket_by_id_path}=    replace string    
    ...    ${inventory_get_ticket_by_id_path}    
    ...    {ticketId}    
    ...    ${ticket_id}    
    ...    1
    create session    api    ${api_gateway_host}    disable_warnings=1
    ${response}    get request    
    ...    api    
    ...    ${inventory_get_ticket_by_id_path}    
    ...    ${headers}
    log    ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}
                
[Inventory] - API update ticket status with owner
    [Arguments]    ${arg_dic}
    sleep    1s
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${data}    catenate
    ...    {
    ...    "status":"${arg_dic.status}",
    ...    "updater_id": ${arg_dic.updater_id},
    ...    "updater_type": "${arg_dic.updater_type}",
    ...    "updater_name": "${arg_dic.updater_name}",
    ...    "updater_note": "${arg_dic.updater_note}",
    ...    "owner_id": ${arg_dic.owner_id},
    ...    "owner_type": "${arg_dic.owner_type}",
    ...    "owner_name": "${arg_dic.owner_name}",
    ...    "owner_timestamp": "${arg_dic.owner_timestamp}"
    ...    }
    ${ticket_id}    convert to string    ${arg_dic.ticket_id}
    ${inventory_update_ticket_status_path}=    replace string    
    ...    ${inventory_update_ticket_status_path}    
    ...    {ticketId}    
    ...    ${ticket_id}    
    ...    1
    create session    api    ${api_gateway_host}    disable_warnings=1
    ${response}    put request    
    ...    api    
    ...    ${inventory_update_ticket_status_path}    
    ...    ${data}    
    ...    ${NONE}    
    ...    ${NONE}    
    ...    ${headers}
    log    ${data}
    log    ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}
    
[Inventory] - API update ticket status
    [Arguments]    ${arg_dic}
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${data}    catenate
    ...    {
    ...    "status":"${arg_dic.status}",
    ...    "updater_id": ${arg_dic.updater_id},
    ...    "updater_type": "${arg_dic.updater_type}",
    ...    "updater_name": "${arg_dic.updater_name}",
    ...    "updater_note": "${arg_dic.updater_note}"
    ...    }
    ${ticket_id}    convert to string    ${arg_dic.ticket_id}
    ${inventory_update_ticket_status_path}=    replace string    
    ...    ${inventory_update_ticket_status_path}    
    ...    {ticketId}    
    ...    ${ticket_id}    
    ...    1
    create session    api    ${api_gateway_host}    disable_warnings=1
    ${response}    put request    
    ...    api    
    ...    ${inventory_update_ticket_status_path}    
    ...    ${data}    
    ...    ${NONE}    
    ...    ${NONE}    
    ...    ${headers}
    log    ${data}
    log    ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}
    
[Inventory] - API update ticket with serial and non-serial
    [Arguments]    ${arg_dic}
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${is_sell_ticket}    run keyword and return status    should be equal as strings    Sell    ${arg_dic.ticket_type}
    ${sell_price_1}    run keyword if    ${is_sell_ticket}    catenate
    ...    "unit_price": ${arg_dic.item_unit_price_1},
    ...    "additional_price": ${arg_dic.item_additional_price_1},
    ...    ELSE    catenate    ${EMPTY}
    ${sell_price_2}    run keyword if    ${is_sell_ticket}    catenate
    ...    "unit_price": ${arg_dic.item_unit_price_2},
    ...    "additional_price": ${arg_dic.item_additional_price_2},
    ...    ELSE    catenate    ${EMPTY}
    ${data}    catenate    
    ...    {
    ...        "updater_id": ${arg_dic.updater_id},
    ...        "updater_type": "${arg_dic.updater_type}",
    ...        "updater_name": "${arg_dic.updater_name}",
    ...        "updater_note": "${arg_dic.updater_note}",
    ...        "ticket_items": [
    ...        {
    ...            ${sell_price_1}
    ...            "id": ${arg_dic.item_id_1},
    ...            "item_serials": [{"serial_number": "${arg_dic.item_serial_number_1}"}]
    ...        },
    ...        {
    ...            ${sell_price_2}
    ...            "id": ${arg_dic.item_id_2},
    ...            "quantity": ${arg_dic.item_quantity_2}
    ...        }
    ...        ]
    ...    }
    ${ticket_id}    convert to string    ${arg_dic.ticket_id}
    ${inventory_update_ticket_path}=    replace string    
    ...    ${inventory_update_ticket_path}    
    ...    {ticketId}    
    ...    ${ticket_id}    
    ...    1
    create session    api    ${api_gateway_host}    disable_warnings=1
    ${response}    put request    
    ...    api    
    ...    ${inventory_update_ticket_path}    
    ...    ${data}    
    ...    ${NONE}    
    ...    ${NONE}    
    ...    ${headers}
    log    ${data}
    log    ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}
    
[Inventory] - API update ticket with non-serial
    [Arguments]    ${arg_dic}
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${is_sell_ticket}    run keyword and return status    should be equal as strings    Sell    ${arg_dic.ticket_type}
    ${sell_price}    run keyword if    ${is_sell_ticket}    catenate
    ...    "unit_price": ${arg_dic.item_unit_price},
    ...    "additional_price": ${arg_dic.item_additional_price},
    ...    ELSE    catenate    ${EMPTY}
    ${data}    catenate    
    ...    {
    ...        "updater_id": ${arg_dic.updater_id},
    ...        "updater_type": "${arg_dic.updater_type}",
    ...        "updater_name": "${arg_dic.updater_name}",
    ...        "updater_note": "${arg_dic.updater_note}",
    ...        "ticket_items": [
    ...        {
    ...            ${sell_price}
    ...            "id": ${arg_dic.item_id},
    ...            "quantity": ${arg_dic.item_quantity}
    ...        }
    ...        ]
    ...    }
    ${ticket_id}    convert to string    ${arg_dic.ticket_id}
    ${inventory_update_ticket_path}=    replace string    
    ...    ${inventory_update_ticket_path}    
    ...    {ticketId}    
    ...    ${ticket_id}    
    ...    1
    create session    api    ${api_gateway_host}    disable_warnings=1
    ${response}    put request    
    ...    api    
    ...    ${inventory_update_ticket_path}    
    ...    ${data}    
    ...    ${NONE}    
    ...    ${NONE}    
    ...    ${headers}
    log    ${data}
    log    ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}
    
[Inventory] - API update ticket with serial
    [Arguments]    ${arg_dic}
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${is_sell_ticket}    run keyword and return status    should be equal as strings    Sell    ${arg_dic.ticket_type}
    ${sell_price}    run keyword if    ${is_sell_ticket}    catenate
    ...    "unit_price": ${arg_dic.item_unit_price},
    ...    "additional_price": ${arg_dic.item_additional_price},
    ...    ELSE    catenate    ${EMPTY}
    ${data}    catenate    
    ...    {
    ...        "updater_id": ${arg_dic.updater_id},
    ...        "updater_type": "${arg_dic.updater_type}",
    ...        "updater_name": "${arg_dic.updater_name}",
    ...        "updater_note": "${arg_dic.updater_note}",
    ...        "ticket_items": [
    ...        {
    ...            ${sell_price}
    ...            "id": ${arg_dic.item_id},
    ...            "item_serials": [{"serial_number": "${arg_dic.item_serial_number}"}]
    ...        }
    ...        ]
    ...    }
    ${ticket_id}    convert to string    ${arg_dic.ticket_id}
    ${inventory_update_ticket_path}=    replace string    
    ...    ${inventory_update_ticket_path}    
    ...    {ticketId}    
    ...    ${ticket_id}    
    ...    1
    create session    api    ${api_gateway_host}    disable_warnings=1
    ${response}    put request    
    ...    api    
    ...    ${inventory_update_ticket_path}    
    ...    ${data}    
    ...    ${NONE}    
    ...    ${NONE}    
    ...    ${headers}
    log    ${data}
    log    ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}
    
[Inventory] - API update ticket without serial
    [Arguments]    ${arg_dic}
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${data}    catenate    
    ...    {
    ...        "updater_id": ${arg_dic.updater_id},
    ...        "updater_type": "${arg_dic.updater_type}",
    ...        "updater_name": "${arg_dic.updater_name}",
    ...        "updater_note": "${arg_dic.updater_note}",
    ...        "ticket_items": [
    ...        {
    ...            "id": ${arg_dic.item_id}
    ...        }
    ...        ]
    ...    }
    ${ticket_id}    convert to string    ${arg_dic.ticket_id}
    ${inventory_update_ticket_path}=    replace string    
    ...    ${inventory_update_ticket_path}    
    ...    {ticketId}    
    ...    ${ticket_id}    
    ...    1
    create session    api    ${api_gateway_host}    disable_warnings=1
    ${response}    put request    
    ...    api    
    ...    ${inventory_update_ticket_path}    
    ...    ${data}    
    ...    ${NONE}    
    ...    ${NONE}    
    ...    ${headers}
    log    ${data}
    log    ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}
    
[Inventory] - API update ticket without item
    [Arguments]    ${arg_dic}
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${data}    catenate    
    ...    {
    ...        "updater_id": ${arg_dic.updater_id},
    ...        "updater_type": "${arg_dic.updater_type}",
    ...        "updater_name": "${arg_dic.updater_name}",
    ...        "updater_note": "${arg_dic.updater_note}",
    ...        "ticket_items": [{}]
    ...    }
    ${ticket_id}    convert to string    ${arg_dic.ticket_id}
    ${inventory_update_ticket_path}=    replace string    
    ...    ${inventory_update_ticket_path}    
    ...    {ticketId}    
    ...    ${ticket_id}    
    ...    1
    create session    api    ${api_gateway_host}    disable_warnings=1
    ${response}    put request    
    ...    api    
    ...    ${inventory_update_ticket_path}    
    ...    ${data}    
    ...    ${NONE}    
    ...    ${NONE}    
    ...    ${headers}
    log    ${data}
    log    ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}
    
[Inventory] - API create ticket without item
    [Arguments]    ${arg_dic}
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${data}    catenate    
    ...    {
    ...        "ticket_type":"${arg_dic.ticket_type}",
    ...        "receiver_id": ${arg_dic.receiver_id},
    ...        "receiver_type": "${arg_dic.receiver_type}",
    ...        "receiver_name": "${arg_dic.receiver_name}",
    ...        "creator_id": ${arg_dic.creator_id},
    ...        "creator_type": "${arg_dic.creator_type}",
    ...        "creator_name": "${arg_dic.creator_name}",
    ...        "creator_note": "${arg_dic.creator_note}",
    ...        "ticket_items": [{}]
    ...    }
    create session    api    ${api_gateway_host}    disable_warnings=1
    ${response}    post request
    ...     api
    ...     ${inventory_create_ticket_path}
    ...     ${data}
    ...     ${NONE}
    ...     ${headers}
    log     ${data}
    log     ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}
    
[Inventory] - API get ticket notes
    [Arguments]    ${arg_dic}
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${ticket_id}    convert to string    ${arg_dic.ticket_id}
    ${inventory_get_ticket_note_by_id_path}=    replace string    
    ...    ${inventory_get_ticket_note_by_id_path}    
    ...    {ticketId}    
    ...    ${ticket_id}    
    ...    1
    create session          api     ${api_gateway_host}   disable_warnings=1
    ${response}    get request    
    ...    api    
    ...    ${inventory_get_ticket_note_by_id_path}    
    ...    ${headers}
    log    ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}

[Inventory] - API search items
    [Arguments]    ${arg_dic}
    [Common][Prepare] - Authorization headers   ${arg_dic.client_id}   ${arg_dic.client_secret}    ${arg_dic.access_token}
    ${body_key_dict}    create dictionary   sku=str   name=str  local_sku=str     sku_type=str
    ...    item_type=str    has_serial_number=bool     serial_number=str   is_active=bool  item_status=str
    ...    receiver_name=str    paging=bool  page_index=num
    ${body_dict}    [Common] - build body request dict from list   ${body_key_dict}     ${arg_dic}
    ${data}     [Common] - create json string from dict with value empty check     ${body_dict}    ${body_key_dict}
    create session    api    ${api_gateway_host}    disable_warnings=1
    ${response}    post request
    ...     api
    ...     ${inventory_search_item_path}
    ...     ${data}
    ...     ${NONE}
    ...     ${headers}
    log    ${data}
    log    ${response.text}
    ${dic}    create dictionary    response=${response}
    [Return]    ${dic}