*** Settings ***
Resource        ../../1_common/keywords.robot


*** Keywords ***

Mock delete
    [Arguments]
    ...     ${mock_data}
                            create session          api     ${mb_host}     disable_warnings=1
    ${response}             delete request
        ...     api
        ...     ${mb_path}
        ...     ${mock_data}
        ...     ${NONE}
        ...     ${NONE}
#    log      ${mock_data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

Mock post prepare data
    [Arguments]          ${dic}
                            create session          api     ${mb_host}:${dic.port}    disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${dic.menu}
        ...     ${dic.data}
        ...     ${NONE}
        ...     ${NONE}

    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

Mock get prepare data
    [Arguments]          ${dic}
                            create session          api     ${mb_host}:${dic.port}     disable_warnings=1
    ${response}             get request
        ...     api
        ...     ${dic.path}
        ...     ${NONE}
        ...     ${NONE}

    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

