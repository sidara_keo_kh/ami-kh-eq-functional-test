*** Settings ***
Resource        ../../1_common/keywords.robot


*** Keywords ***
API add card design
    [Arguments]
    ...    ${provider_id}
    ...    ${card_design_name}
	...    ${card_type_id}
	...    ${is_active}
	...    ${currency}
	...    ${pattern}
	...    ${pre_link_url}
	...    ${pre_link_read_timeout}
	...    ${link_url}
	...    ${link_read_timeout}
	...    ${un_link_url}
	...    ${un_link_read_timeout}
	...    ${debit_url}
	...    ${debit_read_timeout}
	...    ${credit_url}
	...    ${credit_read_timeout}
	...    ${check_status_url}
	...    ${check_status_read_timeout}
	...    ${cancel_url}
	...    ${cancel_read_timeout}
	...    ${pre_sof_order_url}
	...    ${pre_sof_order_read_timeout}
    ...    ${access_token}
    ...    ${header_client_id}
    ...    ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}

    ${data}              catenate
        ...     {
        ...         "name":"${card_design_name}",
	    ...           "card_type_id":${card_type_id},
	    ...            "is_active":${is_active},
        ...             "currency":"${currency}",
        ...           "pan_pattern":"${pattern}",
        ...            "pre_link_url":"${pre_link_url}",
        ...            "pre_link_read_timeout":${pre_link_read_timeout},
        ...            "link_url":"${link_url}",
        ...            "link_read_timeout":${link_read_timeout},
        ...            "un_link_url":"${un_link_url}",
        ...            "un_link_read_timeout":${un_link_read_timeout},
        ...            "debit_url":"${debit_url}",
        ...            "debit_read_timeout":${debit_read_timeout},
        ...            "credit_url":"${credit_url}",
        ...            "credit_read_timeout":${credit_read_timeout},
        ...            "check_status_url":"${check_status_url}",
        ...            "check_status_read_timeout":${check_status_read_timeout},
        ...            "cancel_url":"${cancel_url}",
        ...            "cancel_read_timeout":${cancel_read_timeout},
        ...            "pre_sof_order_url":"${pre_sof_order_url}",
        ...            "pre_sof_order_read_timeout":${pre_sof_order_read_timeout}
        ...     }
                            create session          api     ${api_gateway_host}     disable_warnings=1

        log        ${provider_id}
    ${provider_id}=     convert to string    ${provider_id}
    ${card_design_path}=    replace string      ${add_card_design_path}        {provider_id}        ${provider_id}      1

    ${response}             post request
        ...     api
        ...     ${card_design_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
        log        ${data}
        log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API add provider
    [Arguments]
    ...     ${name}
    ...     ${access_token}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}

    ${data}              catenate
        ...     {
        ...         "name":"${name}"
        ...     }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_provider_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
        log        ${data}
        log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API get list of card types
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_list_of_card_types_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API add new provider
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
     ${param_name}=   [Common] - Create string param        name        ${arg_dic.name}

     ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_name}
        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },

                            create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             post request
         ...     api
         ...    ${add_provider_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API get details of provider
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
     ${provider_id}      convert to string     ${arg_dic.provider_id}
     ${get_details_of_provider_path}          replace string          ${get_details_of_provider_path}       {provider_id}       ${provider_id}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_details_of_provider_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API edit provider detail
    [Arguments]         ${arg_dic}

    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${provider_id}      convert to string     ${arg_dic.provider_id}
    ${edit_provider_detail_path}         replace string          ${edit_provider_detail_path}      {provider_id}       ${provider_id}
    ${param_name}=   [Common] - Create string param        name        ${arg_dic.name}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_name}
        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${edit_provider_detail_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    ${return_dic} =	Create Dictionary   response=${response}
    [Return]    ${return_dic}

API get card design detail
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
     ${provider_id}      convert to string     ${arg_dic.provider_id}
     ${card_design_id}      convert to string     ${arg_dic.card_design_id}
     ${get_card_design_detail_path}          replace string          ${get_card_design_detail_path}       {provider_id}       ${provider_id}
     ${get_card_design_detail_path}          replace string          ${get_card_design_detail_path}       {card-design_id}       ${card_design_id}

                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_card_design_detail_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create card design
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
     ${provider_id}      convert to string     ${arg_dic.provider_id}
     ${add_card_design_path}          replace string          ${add_card_design_path}       {provider_id}       ${provider_id}
     ${param_name}=   [Common] - Create string param        name        ${arg_dic.name}
     ${param_card_type_id}=   [Common] - Create int param        card_type_id        ${arg_dic.card_type_id}
     ${param_is_active}=   [Common] - Create boolean param        is_active        ${arg_dic.is_active}
     ${param_currency}=   [Common] - Create string param        currency        ${arg_dic.currency}
     ${param_pan_pattern}=   [Common] - Create string param        pan_pattern        ${arg_dic.pan_pattern}
     ${param_pre_sof_order_url}=   [Common] - Create string param        pre_sof_order_url        ${arg_dic.pre_sof_order_url}
     ${param_pre_sof_order_read_timeout}=   [Common] - Create int param        pre_sof_order_read_timeout        ${arg_dic.pre_sof_order_read_timeout}
     ${param_pre_link_url}=   [Common] - Create string param        pre_link_url        ${arg_dic.pre_link_url}
     ${param_pre_link_read_timeout}=   [Common] - Create int param        pre_link_read_timeout        ${arg_dic.pre_link_read_timeout}
     ${param_link_url}=   [Common] - Create string param        link_url        ${arg_dic.link_url}
     ${param_link_read_timeout}=   [Common] - Create int param        link_read_timeout        ${arg_dic.link_read_timeout}
     ${param_un_link_url}=   [Common] - Create string param        un_link_url        ${arg_dic.un_link_url}
     ${param_un_link_read_timeout}=   [Common] - Create int param        un_link_read_timeout        ${arg_dic.un_link_read_timeout}
     ${param_debit_url}=   [Common] - Create string param        debit_url        ${arg_dic.debit_url}
     ${param_debit_read_timeout}=   [Common] - Create int param        debit_read_timeout       ${arg_dic.debit_read_timeout}
     ${param_credit_url}=   [Common] - Create string param        credit_url        ${arg_dic.credit_url}
     ${param_credit_read_timeout}=   [Common] - Create int param        credit_read_timeout        ${arg_dic.credit_read_timeout}
     ${param_check_status_url}=   [Common] - Create string param        check_status_url        ${arg_dic.check_status_url}
     ${param_check_status_read_timeout}=   [Common] - Create int param        check_status_read_timeout        ${arg_dic.check_status_read_timeout}
     ${param_cancel_url}=   [Common] - Create string param        cancel_url       ${arg_dic.cancel_url}
     ${param_cancel_read_timeout}=   [Common] - Create int param        cancel_read_timeout        ${arg_dic.cancel_read_timeout}
     ${param_pre_sof_check_status_url}=   [Common] - Create string param        pre_sof_check_status_url        ${arg_dic.pre_sof_check_status_url}
     ${param_pre_sof_check_status_read_timeout}=   [Common] - Create int param        pre_sof_check_status_read_timeout        ${arg_dic.pre_sof_check_status_read_timeout}
     ${param_pre_sof_check_status_max_retry}=   [Common] - Create int param        pre_sof_check_status_max_retry        ${arg_dic.pre_sof_check_status_max_retry}




     ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_name}
        ...         	 ${param_card_type_id}
        ...         	 ${param_is_active}
        ...         	 ${param_currency}
        ...              ${param_pan_pattern}
        ...         	 ${param_pre_sof_order_url}
        ...         	 ${param_pre_sof_order_read_timeout}
        ...         	 ${param_pre_link_url}
        ...         	 ${param_pre_link_read_timeout}
        ...              ${param_link_url}
        ...         	 ${param_link_read_timeout}
        ...         	 ${param_un_link_url}
        ...         	 ${param_un_link_read_timeout}
        ...         	 ${param_debit_url}
        ...              ${param_debit_read_timeout}
        ...         	 ${param_credit_url}
        ...         	 ${param_credit_read_timeout}
        ...         	 ${param_check_status_url}
        ...         	 ${param_check_status_read_timeout}
        ...              ${param_cancel_url}
        ...         	 ${param_cancel_read_timeout}
        ...              ${param_pre_sof_check_status_url}
        ...              ${param_pre_sof_check_status_read_timeout}
        ...              ${param_pre_sof_check_status_max_retry}
        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },

                            create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             post request
         ...     api
         ...    ${add_card_design_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get list of providers
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_list_of_providers_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}



API get card design by PAN number
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
     ${provider_id}      convert to string     ${arg_dic.provider_id}
     ${pan}      convert to string     ${arg_dic.pan}
     ${get_card_design_by_PAN_number_path}          replace string          ${get_card_design_by_PAN_number_path}       {provider_id}       ${provider_id}
     ${get_card_design_by_PAN_number_path}         replace string          ${get_card_design_by_PAN_number_path}       {pan}       ${pan}

                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_card_design_by_PAN_number_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API edit card design
    [Arguments]         ${arg_dic}

    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
     ${provider_id}      convert to string     ${arg_dic.provider_id}
     ${card_design_id}      convert to string     ${arg_dic.card_design_id}
     ${edit_card_design_path}          replace string          ${edit_card_design_path}       {provider_id}       ${provider_id}
     ${edit_card_design_path}          replace string          ${edit_card_design_path}       {card-design_id}       ${card_design_id}
     ${param_name}=   [Common] - Create string param        name        ${arg_dic.name}
     ${param_card_type_id}=   [Common] - Create int param        card_type_id        ${arg_dic.card_type_id}
     ${param_is_active}=   [Common] - Create boolean param        is_active        ${arg_dic.is_active}
     ${param_currency}=   [Common] - Create string param        currency        ${arg_dic.currency}
     ${param_pan_pattern}=   [Common] - Create string param        pan_pattern        ${arg_dic.pan_pattern}
     ${param_pre_sof_order_url}=   [Common] - Create string param        pre_sof_order_url        ${arg_dic.pre_sof_order_url}
     ${param_pre_sof_order_read_timeout}=   [Common] - Create int param        pre_sof_order_read_timeout        ${arg_dic.pre_sof_order_read_timeout}
     ${param_pre_link_url}=   [Common] - Create string param        pre_link_url        ${arg_dic.pre_link_url}
     ${param_pre_link_read_timeout}=   [Common] - Create int param        pre_link_read_timeout        ${arg_dic.pre_link_read_timeout}
     ${param_link_url}=   [Common] - Create string param        link_url        ${arg_dic.link_url}
     ${param_link_read_timeout}=   [Common] - Create int param        link_read_timeout        ${arg_dic.link_read_timeout}
     ${param_un_link_url}=   [Common] - Create string param        un_link_url        ${arg_dic.un_link_url}
     ${param_un_link_read_timeout}=   [Common] - Create int param        un_link_read_timeout        ${arg_dic.un_link_read_timeout}
     ${param_debit_url}=   [Common] - Create string param        debit_url        ${arg_dic.debit_url}
     ${param_debit_read_timeout}=   [Common] - Create int param        debit_read_timeout       ${arg_dic.debit_read_timeout}
     ${param_credit_url}=   [Common] - Create string param        credit_url        ${arg_dic.credit_url}
     ${param_credit_read_timeout}=   [Common] - Create int param        credit_read_timeout        ${arg_dic.credit_read_timeout}
     ${param_check_status_url}=   [Common] - Create string param        check_status_url        ${arg_dic.check_status_url}
     ${param_check_status_read_timeout}=   [Common] - Create int param        check_status_read_timeout        ${arg_dic.check_status_read_timeout}
     ${param_cancel_url}=   [Common] - Create string param        cancel_url       ${arg_dic.cancel_url}
     ${param_cancel_read_timeout}=   [Common] - Create int param        cancel_read_timeout        ${arg_dic.cancel_read_timeout}
     ${param_pre_sof_check_status_url}=   [Common] - Create string param        pre_sof_check_status_url        ${arg_dic.pre_sof_check_status_url}
     ${param_pre_sof_check_status_read_timeout}=   [Common] - Create int param        pre_sof_check_status_read_timeout        ${arg_dic.pre_sof_check_status_read_timeout}
     ${param_pre_sof_check_status_max_retry}=   [Common] - Create int param        pre_sof_check_status_max_retry        ${arg_dic.pre_sof_check_status_max_retry}

    ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_name}
        ...         	 ${param_card_type_id}
        ...         	 ${param_is_active}
        ...         	 ${param_currency}
        ...              ${param_pan_pattern}
        ...         	 ${param_pre_sof_order_url}
        ...         	 ${param_pre_sof_order_read_timeout}
        ...         	 ${param_pre_link_url}
        ...         	 ${param_pre_link_read_timeout}
        ...              ${param_link_url}
        ...         	 ${param_link_read_timeout}
        ...         	 ${param_un_link_url}
        ...         	 ${param_un_link_read_timeout}
        ...         	 ${param_debit_url}
        ...              ${param_debit_read_timeout}
        ...         	 ${param_credit_url}
        ...         	 ${param_credit_read_timeout}
        ...         	 ${param_check_status_url}
        ...         	 ${param_check_status_read_timeout}
        ...              ${param_cancel_url}
        ...         	 ${param_cancel_read_timeout}
        ...              ${param_pre_sof_check_status_url}
        ...              ${param_pre_sof_check_status_read_timeout}
        ...              ${param_pre_sof_check_status_max_retry}
        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${edit_card_design_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    ${return_dic} =	Create Dictionary   response=${response}
    [Return]    ${return_dic}