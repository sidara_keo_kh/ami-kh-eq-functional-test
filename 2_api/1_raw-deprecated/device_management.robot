*** Settings ***
Resource        ../../1_common/keywords.robot


*** Keywords ***
API create channel
    [Arguments]  ${arg_dic}
    ${channel_type_id}  [Common] - Create string param in dic    ${arg_dic}     channel_type_id
    ${user_type_id}  [Common] - Create string param in dic    ${arg_dic}     user_type_id
    ${name}  [Common] - Create string param in dic    ${arg_dic}     name

    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	${channel_type_id}
        ...     	${user_type_id}
        ...     	${name}
        ...     }
    ${data}  replace string      ${data}      ,}    }
                create session          api        ${api_gateway_host}    disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_channel_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete channel
    [Arguments]  ${arg_dic}
    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${channel_id}        convert to string           ${arg_dic.channel_id}
    ${delete_channel_path}          replace string          ${delete_channel_path}       {channel_id}       ${channel_id}
                create session          api        ${api_gateway_host}    disable_warnings=1
    ${response}             delete request
        ...     api
        ...     ${delete_channel_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create channel access permission
    [Arguments]  ${arg_dic}
    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	"user_id":${arg_dic.user_id},
        ...     	"shop_id":${arg_dic.shop_id},
        ...     	"user_type":{
        ...                 "id":${arg_dic.id},
        ...                 "name":"${arg_dic.name}"
        ...         }
        ...     }
    ${channel_id}        convert to string           ${arg_dic.channel_id}
    ${create_channel_access_permission_path}          replace string          ${create_channel_access_permission_path}       {channel_id}       ${channel_id}
                create session          api        ${api_gateway_host}    disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_channel_access_permission_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API revoke channel access permission
    [Arguments]  ${arg_dic}
    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	"user_id":${arg_dic.user_id},
        ...     	"user_type":{
        ...                 "id":${arg_dic.id},
        ...                 "name":"${arg_dic.name}"
        ...         }
        ...     }
    ${channel_id}        convert to string           ${arg_dic.channel_id}
    ${revoke_channel_access_permission_path}          replace string          ${revoke_channel_access_permission_path}       {channel_id}       ${channel_id}
                create session          api        ${api_gateway_host}    disable_warnings=1
                create session          api        ${api_gateway_host}    disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${revoke_channel_access_permission_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

