*** Settings ***
Resource        ../../1_common/keywords.robot


*** Keywords ***
API add service
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${data}              catenate
        ...     {
        ...         "name": "${arg_dic.name}",
        ...         "location": "${arg_dic.location}",
        ...         "timeout": ${arg_dic.timeout},
        ...         "max_total_connection": ${arg_dic.max_total_connection},
        ...         "max_per_route": ${arg_dic.max_per_route}
        ...     }
                            create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${add_service_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}

    log         ${data}
    log         ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API channel gateway - delete service
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${serviceId}    Convert To String    ${arg_dic.serviceId}
    ${channel_gateway_delete_service_path}      replace string      ${channel_gateway_delete_service_path}     {serviceId}    ${serviceId}
                            create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             delete request
        ...     api
        ...     ${channel_gateway_delete_service_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API channel gateway - add api
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${permission_required}    [Common] - Create boolean param in dic    ${arg_dic}      permission_required
    ${data}              catenate
        ...     {
        ...         "name": "${arg_dic.name}",
        ...         "http_method": "${arg_dic.http_method}",
        ...         "pattern": "${arg_dic.pattern}",
        ...         "is_required_access_token": ${arg_dic.is_required_access_token},
        ...         ${permission_required}
        ...         "service_id": ${arg_dic.service_id}
        ...     }
                            create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${channel_gateway_add_api_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${data}
    log         ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API channel gateway - agent authentication
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/x-www-form-urlencoded

    ${params}              catenate          SEPARATOR=&
        ...     grant_type=${arg_dic.grant_type}
        ...     username=${arg_dic.username}
        ...     password=${arg_dic.password}
        ...     client_id=${arg_dic.param_client_id}
                            create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${channel_gateway_agent_authenticate}
        ...     ${NONE}
        ...     ${params}
        ...     ${header}
    log     ${params}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API channel gateway - revoke auth token
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     Authorization=Bearer ${arg_dic.access_token}
        ...     content-type=application/x-www-form-urlencoded
                            create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${channel_gateway_agent_auth_revoke}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API channel gateway - get all apis
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${channel_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${channel_gateway_get_all_apis_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API channel gateway - update api
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${apiId}        convert to string           ${arg_dic.apiId}
    ${channel_gateway_update_api_path}          replace string          ${channel_gateway_update_api_path}       {apiId}       ${apiId}

    ${param_update_api_name}    [Common] - Create string param        name        ${arg_dic.name}
    ${param_update_api_http_method}    [Common] - Create string param        http_method        ${arg_dic.http_method}
    ${param_update_api_pattern}    [Common] - Create string param        pattern        ${arg_dic.pattern}
    ${param_update_api_is_required_access_token}    [Common] - Create string param        is_required_access_token        ${arg_dic.is_required_access_token}
    ${param_update_api_service_id}    [Common] - Create int param        service_id        ${arg_dic.service_id}

    ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_update_api_name}
        ...         	 ${param_update_api_http_method}
        ...         	 ${param_update_api_pattern}
        ...         	 ${param_update_api_is_required_access_token}
        ...         	 ${param_update_api_service_id}
        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${channel_gateway_update_api_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    ${return_dic} =	Create Dictionary   response=${response}
#    log to console     ${response.text}
    [Return]    ${return_dic}

API channel gateway - delete api
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${apiId}        convert to string           ${arg_dic.apiId}
    ${channel_gateway_delete_api_path}          replace string          ${channel_gateway_delete_api_path}       {apiId}       ${apiId}
                            create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             delete request
        ...     api
        ...     ${channel_gateway_delete_api_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    ${return_dic} =	Create Dictionary   response=${response}
#    log to console     ${response.text}
    [Return]    ${return_dic}

API channel gateway - search api
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_search_api_id}    [Common] - Create int param        id        ${arg_dic.id}

    ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_search_api_id}
        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${channel_gateway_search_api_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${return_dic} =	Create Dictionary   response=${response}
#    log to console     ${response.text}
    [Return]    ${return_dic}

API channel gateway - get all services
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${channel_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${channel_gateway_get_all_services_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API channel gateway - add service
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_add_location}    [Common] - Create string param        location        ${arg_dic.location}
    ${param_add_service_name}    [Common] - Create string param       name        ${arg_dic.name}
    ${param_add_max_total_connection}    [Common] - Create int param        max_total_connection        ${arg_dic.max_total_connection}
    ${param_add_timeout}    [Common] - Create int param        timeout        ${arg_dic.timeout}
    ${param_add_max_per_route}    [Common] - Create int param        max_per_route        ${arg_dic.max_per_route}

    ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_add_location}
        ...         	 ${param_add_service_name}
        ...         	 ${param_add_max_total_connection}
        ...         	 ${param_add_timeout}
        ...         	 ${param_add_max_per_route}
        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${channel_gateway_add_service_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${return_dic} =	Create Dictionary   response=${response}
    log     ${data}
    log     ${response.text}
    [Return]    ${return_dic}


API channel gateway - update service
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${serviceId}        convert to string           ${arg_dic.serviceId}
    ${channel_gateway_update_service_path}          replace string          ${channel_gateway_update_service_path}       {serviceId}       ${serviceId}

    ${param_add_location}    [Common] - Create string param        location        ${arg_dic.location}
    ${param_add_name}    [Common] - Create string param       name        ${arg_dic.name}
    ${param_add_max_total_connection}    [Common] - Create int param        max_total_connection        ${arg_dic.max_total_connection}
    ${param_add_timeout}    [Common] - Create int param        timeout        ${arg_dic.timeout}
    ${param_add_max_per_route}    [Common] - Create int param        max_per_route        ${arg_dic.max_per_route}

    ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_add_location}
        ...         	 ${param_add_name}
        ...         	 ${param_add_max_total_connection}
        ...         	 ${param_add_timeout}
        ...         	 ${param_add_max_per_route}
        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${channel_gateway_update_service_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    ${return_dic} =	Create Dictionary   response=${response}
    log     ${data}
    log     ${response.text}
    [Return]    ${return_dic}

API channel gateway - search service
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_search_service_id}    [Common] - Create int param        id        ${arg_dic.id}

    ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_search_service_id}
        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${channel_gateway_search_service_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${return_dic} =	Create Dictionary   response=${response}
    [Return]    ${return_dic}

API channel gateway - add scopes to client
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${request_client_id}        convert to string           ${arg_dic.request_client_id}
    ${channel_gateway_add_scopes_to_client_path}          replace string          ${channel_gateway_add_scopes_to_client_path}       {clientId}       ${request_client_id}


    ${data}              catenate           SEPARATOR=
        ...	    {
        ...         	 "scopes": [${arg_dic.scopes}]
        ...	    }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${channel_gateway_add_scopes_to_client_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${return_dic} =	Create Dictionary   response=${response}
    log    ${response.text}
    [Return]    ${return_dic}

API channel gateway - get client scopes
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${channel_gateway_host}     disable_warnings=1

    ${clientId}        convert to string           ${arg_dic.clientId}
    ${channel_gateway_get_client_scopes_path}          replace string          ${channel_gateway_get_client_scopes_path}       {clientId}       ${clientId}

     ${response}             get request
         ...     api
         ...     ${channel_gateway_get_client_scopes_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    log    ${response.text}
    [Return]    ${dic}

API channel gateway - delete scope from client
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${clientId}        convert to string           ${arg_dic.clientId}
    ${channel_gateway_delete_scope_from_client_path}          replace string          ${channel_gateway_delete_scope_from_client_path}       {clientId}       ${clientId}
                        create session          api     ${channel_gateway_host}     disable_warnings=1
    ${data}              catenate           SEPARATOR=
        ...	    {
        ...         	 "scopes": [${arg_dic.scopes}]
        ...	    }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },

    ${response}             delete request
        ...     api
        ...     ${channel_gateway_delete_scope_from_client_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${return_dic} =	Create Dictionary   response=${response}
    log    ${response.text}
    [Return]    ${return_dic}

API channel gateway - get all clients
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${channel_gateway_host}     disable_warnings=1

     ${response}             get request
         ...     api
         ...     ${channel_gateway_get_all_clients_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    log    ${response.text}
    [Return]    ${dic}
