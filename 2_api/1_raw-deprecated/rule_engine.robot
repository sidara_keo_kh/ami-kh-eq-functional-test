*** Settings ***
Resource        ../../1_common/keywords.robot


*** Keywords ***
API create new campaign
    [Arguments]
    ...     ${name}
    ...     ${description}
    ...     ${is_active}
    ...     ${start_active_timestamp}
    ...     ${end_active_timestamp}
    ...     ${access_token}
    ...     ${client_id}
    ...     ${client_secret}
    ${header}               create dictionary
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}

    ${data}              catenate
        ...     {
        ...         "name":"${name}",
        ...         "description":"${description}",
        ...         "is_active":"${is_active}",
        ...         "start_active_timestamp":"${start_active_timestamp}",
        ...         "end_active_timestamp":"${end_active_timestamp}"
        ...     }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_rule_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${data}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update campaign
    [Arguments]
    ...     ${rule_id}
    ...     ${name}
    ...     ${description}
    ...     ${is_active}
    ...     ${access_token}
    ...     ${client_id}
    ...     ${client_secret}
    ${header}               create dictionary
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}

    ${param_name}=  [Common] - Create string param     name        ${name}
    ${param_is_active}=  [Common] - Create boolean param     is_active        ${is_active}
    ${param_description}=  [Common] - Create string param     description        ${description}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...         ${param_name}
        ...         ${param_description}
        ...         ${param_is_active}
        ...     }
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${rule_id}      convert to string   ${rule_id}
    ${path}=    replace string      ${update_rule_path}         {rule_id}       ${rule_id}    1
    ${response}             put request
        ...     api
        ...     ${path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log        ${data}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create new mechanic
    [Arguments]
    ...     ${rule_id}
    ...     ${event_name}
    ...     ${start_timestamp}
    ...     ${end_timestamp}
    ...     ${access_token}
    ...     ${client_id}
    ...     ${client_secret}
    ${header}               create dictionary
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}

    ${data}              catenate
        ...     {
        ...         "event_name":"${event_name}",
        ...         "start_timestamp":"${start_timestamp}",
        ...         "end_timestamp":"${end_timestamp}"
        ...     }
    ${rule_id}      convert to string   ${rule_id}
    ${path}=    replace string      ${create_mechanic_path}         {rule_id}       ${rule_id}    1
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${data}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create new condition
    [Arguments]
    ...     ${rule_id}
    ...     ${mechanic_id}
    ...     ${filter_type}
    ...     ${sum_key_name}
    ...     ${profile_detail_actor}
    ...     ${access_token}
    ...     ${client_id}
    ...     ${client_secret}
    ${header}               create dictionary
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${param_filter_type}=  [Common] - Create string param     filter_type        ${filter_type}
    ${param_sum_key_name}=  [Common] - Create string param     sum_key_name        ${sum_key_name}
    ${param_profile_detail_actor}=  [Common] - Create string param     profile_detail_actor        ${profile_detail_actor}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...         ${param_filter_type}
        ...         ${param_sum_key_name}
        ...         ${param_profile_detail_actor}
        ...     }
    ${data}  replace string      ${data}      ,}    }
    ${rule_id}  convert to string  ${rule_id}
    ${mechanic_id}  convert to string  ${mechanic_id}
    ${path}=    replace string      ${create_condition_path}         {rule_id}       ${rule_id}    1
    ${path}=    replace string      ${path}         {mechanic_id}       ${mechanic_id}    1
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${data}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create new comparison
    [Arguments]
    ...     ${rule_id}
    ...     ${mechanic_id}
    ...     ${condition_id}
    ...     ${key_name}
    ...     ${key_value_type}
    ...     ${key_value}
    ...     ${operator}
    ...     ${access_token}
    ...     ${client_id}
    ...     ${client_secret}
    ${header}               create dictionary
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}

    ${data}              catenate
        ...     {
        ...         "key_name":"${key_name}",
        ...         "key_value_type":"${key_value_type}",
        ...         "key_value":"${key_value}",
        ...         "operator":"${operator}"
        ...     }
    ${rule_id}  convert to string  ${rule_id}
    ${mechanic_id}  convert to string  ${mechanic_id}
    ${condition_id}  convert to string  ${condition_id}
    ${path}=    replace string      ${create_comparison_path}         {rule_id}       ${rule_id}    1
    ${path}=    replace string      ${path}        {mechanic_id}       ${mechanic_id}    1
    ${path}=    replace string      ${path}         {condition_id}       ${condition_id}    1
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${data}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create new rule
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
     ${param_name}=   [Common] - Create string param        name        ${arg_dic.name}
     ${param_start_active_timestamp}=   [Common] - Create string param        start_active_timestamp        ${arg_dic.start_active_timestamp}
     ${param_description}=   [Common] - Create string param        description        ${arg_dic.description}
     ${param_is_active}=   [Common] - Create boolean param        is_active        ${arg_dic.is_active}
     ${param_end_active_timestamp}=   [Common] - Create string param        end_active_timestamp        ${arg_dic.end_active_timestamp}

     ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_name}
        ...         	 ${param_start_active_timestamp}
        ...         	 ${param_description}
        ...         	 ${param_is_active}
        ...              ${param_end_active_timestamp}

        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },

                            create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             post request
         ...     api
         ...    ${create_new_campaign_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get campaign detail by id
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
     ${rule_id}      convert to string     ${arg_dic.rule_id}
     ${get_campaign_detail_by_id_path}          replace string          ${get_campaign_detail_by_id_path}       {rule_id}       ${rule_id}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_campaign_detail_by_id_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get campaign list
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_campaign_list_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update rule
    [Arguments]         ${arg_dic}

    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${update_campaign_path}         replace string          ${update_campaign_path}       {rule_id}       ${rule_id}
    ${param_name}=   [Common] - Create string param        name        ${arg_dic.name}
    ${param_description}=   [Common] - Create string param        description        ${arg_dic.description}
    ${param_is_active}=   [Common] - Create boolean param        is_active        ${arg_dic.is_active}

    ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_name}
        ...         	 ${param_description}
        ...         	 ${param_is_active}

        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${update_campaign_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    ${return_dic} =	Create Dictionary   response=${response}
    [Return]    ${return_dic}

API search campaign
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
     ${param_start_active_timestamp}=   [Common] - Create string param        start_active_timestamp        ${arg_dic.start_active_timestamp}
     ${param_rule_id}=   [Common] - Create int param        rule_id        ${arg_dic.rule_id}
     ${param_is_active}=   [Common] - Create boolean param        is_active        ${arg_dic.is_active}
     ${param_end_active_timestamp}=   [Common] - Create string param        end_active_timestamp        ${arg_dic.end_active_timestamp}

     ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_rule_id}
        ...         	 ${param_start_active_timestamp}
        ...         	 ${param_is_active}
        ...              ${param_end_active_timestamp}

        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },

                            create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             post request
         ...     api
         ...    ${search_campaign_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create mechanic
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
     ${rule_id}      convert to string     ${arg_dic.rule_id}
     ${create_new_mechanic_path}          replace string          ${create_new_mechanic_path}       {rule_id}       ${rule_id}
     ${param_event_name}=   [Common] - Create string param        event_name        ${arg_dic.event_name}
     ${param_start_timestamp}=   [Common] - Create string param        start_timestamp        ${arg_dic.start_timestamp}
     ${param_end_timestamp}=   [Common] - Create string param        end_timestamp        ${arg_dic.end_timestamp}

     ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_event_name}
        ...         	 ${param_start_timestamp}
        ...              ${param_end_timestamp}

        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },

                            create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             post request
         ...     api
         ...    ${create_new_mechanic_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update mechanic
    [Arguments]         ${arg_dic}

    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
    ${update_mechanic_path}         replace string          ${update_mechanic_path}       {rule_id}       ${rule_id}
    ${update_mechanic_path}         replace string          ${update_mechanic_path}       {mechanic_id}       ${mechanic_id}
    ${param_rule_id}=   [Common] - Create int param       rule_id        ${arg_dic.rule_id}
    ${param_event_name}=   [Common] - Create string param        event_name        ${arg_dic.event_name}
    ${param_start_timestamp}=   [Common] - Create string param        start_timestamp        ${arg_dic.start_timestamp}
    ${param_end_timestamp}=   [Common] - Create string param        end_timestamp        ${arg_dic.end_timestamp}

    ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_rule_id}
        ...         	 ${param_event_name}
        ...         	 ${param_start_timestamp}
        ...              ${param_end_timestamp}

        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${update_mechanic_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    ${return_dic} =	Create Dictionary   response=${response}
    [Return]    ${return_dic}


API get mechanic list
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${get_mechanic_list_path}         replace string          ${get_mechanic_list_path}       {rule_id}       ${rule_id}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_mechanic_list_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API get mechanic detail by id
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
    ${get_mechanic_detail_by_id_path}         replace string          ${get_mechanic_detail_by_id_path}       {rule_id}       ${rule_id}
    ${get_mechanic_detail_by_id_path}         replace string          ${get_mechanic_detail_by_id_path}       {mechanic_id}       ${mechanic_id}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_mechanic_detail_by_id_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete mechanic
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
    ${delete_mechanic_path}         replace string          ${delete_mechanic_path}       {rule_id}       ${rule_id}
    ${delete_mechanic_path}         replace string          ${delete_mechanic_path}       {mechanic_id}       ${mechanic_id}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             delete request
         ...     api
         ...     ${delete_mechanic_path}
         ...     ${NONE}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create condition
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
     ${rule_id}      convert to string     ${arg_dic.rule_id}
     ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
     ${create_new_condition_path}         replace string          ${create_new_condition_path}       {rule_id}       ${rule_id}
     ${create_new_condition_path}         replace string          ${create_new_condition_path}       {mechanic_id}       ${mechanic_id}
     ${param_filter_type}=   [Common] - Create string param        filter_type        ${arg_dic.filter_type}
     ${param_sum_key_name}=   [Common] - Create string param        sum_key_name        ${arg_dic.sum_key_name}

     ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_filter_type}
        ...         	 ${param_sum_key_name}

        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },

                            create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             post request
         ...     api
         ...    ${create_new_condition_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get condition list
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
     ${rule_id}      convert to string     ${arg_dic.rule_id}
     ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
     ${get_condition_list_path}         replace string          ${get_condition_list_path}       {rule_id}       ${rule_id}
     ${get_condition_list_path}        replace string         ${get_condition_list_path}       {mechanic_id}       ${mechanic_id}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_condition_list_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get condition detail by id
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
     ${rule_id}      convert to string     ${arg_dic.rule_id}
     ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
     ${condition_id}      convert to string     ${arg_dic.condition_id}
     ${get_condition_detail_by_id_path}         replace string          ${get_condition_detail_by_id_path}      {rule_id}       ${rule_id}
     ${get_condition_detail_by_id_path}         replace string          ${get_condition_detail_by_id_path}      {mechanic_id}       ${mechanic_id}
     ${get_condition_detail_by_id_path}        replace string         ${get_condition_detail_by_id_path}      {condition_id}       ${condition_id}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_condition_detail_by_id_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update condition
    [Arguments]         ${arg_dic}

    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
    ${condition_id}      convert to string     ${arg_dic.condition_id}
    ${update_condition_path}         replace string           ${update_condition_path}      {rule_id}       ${rule_id}
    ${update_condition_path}         replace string           ${update_condition_path}     {mechanic_id}       ${mechanic_id}
    ${update_condition_path}        replace string          ${update_condition_path}      {condition_id}       ${condition_id}
    ${param_mechanic_id}=   [Common] - Create int param       mechanic_id        ${arg_dic.mechanic_id}
    ${param_filter_type}=   [Common] - Create string param        filter_type        ${arg_dic.filter_type}
    ${param_filter_type}=   [Common] - Create string param        sum_key_name       ${arg_dic.sum_key_name}


    ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_mechanic_id}
        ...         	 ${param_filter_type}
        ...         	 ${param_sum_key_name}

        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${update_condition_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    ${return_dic} =	Create Dictionary   response=${response}
    [Return]    ${return_dic}

API delete condition
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
    ${condition_id}      convert to string     ${arg_dic.condition_id}
    ${delete_condition_path}         replace string          ${delete_condition_path}      {rule_id}       ${rule_id}
    ${delete_condition_path}        replace string         ${delete_condition_path}       {mechanic_id}       ${mechanic_id}
    ${delete_condition_path}         replace string          ${delete_condition_path}      {condition_id}       ${condition_id}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             delete request
         ...     api
         ...     ${delete_condition_path}
         ...     ${NONE}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get comparison list
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
    ${condition_id}      convert to string     ${arg_dic.condition_id}
    ${get_all_comparison_path}        replace string          ${get_all_comparison_path}      {rule_id}       ${rule_id}
    ${get_all_comparison_path}        replace string        ${get_all_comparison_path}       {mechanic_id}       ${mechanic_id}
    ${get_all_comparison_path}        replace string          ${get_all_comparison_path}     {condition_id}       ${condition_id}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_all_comparison_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API create comparison
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
    ${condition_id}      convert to string     ${arg_dic.condition_id}
    ${create_new_comparison_path}        replace string          ${create_new_comparison_path}      {rule_id}       ${rule_id}
    ${create_new_comparison_path}       replace string        ${create_new_comparison_path}      {mechanic_id}       ${mechanic_id}
    ${create_new_comparison_path}        replace string         ${create_new_comparison_path}    {condition_id}       ${condition_id}
    ${param_key_name}=   [Common] - Create string param        key_name        ${arg_dic.key_name}
    ${param_key_value_type}=   [Common] - Create string param        key_value_type        ${arg_dic.key_value_type}
    ${param_operator}=   [Common] - Create string param        operator        ${arg_dic.operator}
    ${param_key_value}=   [Common] - Create string param        key_value        ${arg_dic.key_value}


     ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_key_name}
        ...         	 ${param_key_value_type}
        ...         	 ${param_operator}
        ...         	 ${param_key_value}
        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },

                            create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             post request
         ...     api
         ...    ${create_new_comparison_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all filter types
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_all_filter_type_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all data types
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_all_data_type_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all operators
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_all_operator_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all action types
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_action_type_list_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API create action
         [Arguments]    ${arg_dic}
         ${header}               create dictionary
             ...     client_id=${arg_dic.client_id}
             ...     client_secret=${arg_dic.client_secret}
             ...     content-type=application/json
             ...     Authorization=Bearer ${arg_dic.access_token}
        ${rule_id}      convert to string     ${arg_dic.rule_id}
        ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
        ${create_new_action_path}        replace string          ${create_new_action_path}      {rule_id}       ${rule_id}
        ${create_new_action_path}       replace string       ${create_new_action_path}      {mechanic_id}       ${mechanic_id}
        ${param_action_type_id}=   [Common] - Create int param     action_type_id        ${arg_dic.action_type_id}
        ${param_user_id}=   [Common] - Create int param     user_id        ${arg_dic.user_id}
        ${param_user_type}=   [Common] - Create string param     user_type        null


        ${data_json}    set variable    ${empty}
        ${length}   get length      ${arg_dic.data}
        :FOR     ${index}   IN RANGE   0    ${length}
        \      ${action_data_item}    get from list      ${arg_dic.data}      ${index}
        \      ${action_data_item}    strip string         ${action_data_item}
        \      ${data_json}     catenate    SEPARATOR=,  ${data_json}   ${action_data_item}
        log     ${data_json}
        ${action_data_json}              catenate       SEPARATOR=
            ...     	,"data":
            ...     	[
            ...             ${data_json}
            ...     	]
        ${action_data_json}     replace string      ${action_data_json}     [,        [
        ${action_data_json}     replace string          ${action_data_json}     ,]        ]

        ${data}              catenate       SEPARATOR=
            ...     {
            ...         ${param_action_type_id}
            ...         ${param_user_id}
            ...         ${param_user_type}
            ...         ${action_data_json}
            ...     }
        ${data}  replace string        ${data}      ,,      ,
        ${data}  replace string        ${data}      ,}      }
                                create session          api     ${api_gateway_host}     disable_warnings=1
        ${response}             post request
            ...     api
            ...     ${create_new_action_path}
            ...     ${data}
            ...     ${NONE}
            ...     ${header}
        log        ${data}
        log        ${response.text}
        ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all actions
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
     ${rule_id}      convert to string     ${arg_dic.rule_id}
     ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
     ${get_all_actions_path}         replace string         ${get_all_actions_path}       {rule_id}       ${rule_id}
     ${get_all_actions_path}        replace string         ${get_all_actions_path}       {mechanic_id}       ${mechanic_id}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_all_actions_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get action limitation list
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
     ${rule_id}      convert to string     ${arg_dic.rule_id}
     ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
     ${action_id}      convert to string     ${arg_dic.action_id}
     ${list_limitation_path}         replace string         ${list_limitation_path}       {rule_id}       ${rule_id}
     ${list_limitation_path}        replace string        ${list_limitation_path}       {mechanic_id}       ${mechanic_id}
     ${list_limitation_path}        replace string        ${list_limitation_path}       {action_id}       ${action_id}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${list_limitation_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}




API get condition filter list
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
    ${condition_id}      convert to string     ${arg_dic.condition_id}
    ${get_condition_filter_list_path}       replace string          ${get_condition_filter_list_path}      {rule_id}       ${rule_id}
    ${get_condition_filter_list_path}        replace string        ${get_condition_filter_list_path}       {mechanic_id}       ${mechanic_id}
    ${get_condition_filter_list_path}        replace string          ${get_condition_filter_list_path}     {condition_id}       ${condition_id}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_condition_filter_list_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API create condition filter
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
    ${condition_id}      convert to string     ${arg_dic.condition_id}
    ${create_condition_filter_path}       replace string          ${create_condition_filter_path}      {rule_id}       ${rule_id}
    ${create_condition_filter_path}       replace string        ${create_condition_filter_path}      {mechanic_id}       ${mechanic_id}
    ${create_condition_filter_path}       replace string         ${create_condition_filter_path}    {condition_id}       ${condition_id}
    ${param_key_name}=   [Common] - Create string param        key_name        ${arg_dic.key_name}
    ${param_key_value_type}=   [Common] - Create string param        key_value_type        ${arg_dic.key_value_type}
    ${param_operator}=   [Common] - Create string param        operator        ${arg_dic.operator}
    ${param_key_value}=   [Common] - Create string param        key_value        ${arg_dic.key_value}
    ${param_is_consecutive_key}=   [Common] - Create string param        is_consecutive_key        ${arg_dic.is_consecutive_key}


     ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_key_name}
        ...         	 ${param_key_value_type}
        ...         	 ${param_operator}
        ...         	 ${param_key_value}
        ...         	 ${param_is_consecutive_key}
        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },

                            create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             post request
         ...     api
         ...    ${create_new_comparison_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API delete condition filter
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
    ${condition_id}      convert to string     ${arg_dic.condition_id}
    ${filter_id}      convert to string     ${arg_dic.filter_id}
    ${delete_condition_filter_path}         replace string          ${delete_condition_filter_path}      {rule_id}       ${rule_id}
    ${delete_condition_filter_path}        replace string         ${delete_condition_filter_path}       {mechanic_id}       ${mechanic_id}
    ${delete_condition_filter_path}         replace string          ${delete_condition_filter_path}      {condition_id}       ${condition_id}
    ${delete_condition_filter_path}         replace string          ${delete_condition_filter_path}      {filter_id}       ${filter_id}

                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             delete request
         ...     api
         ...     ${delete_condition_filter_path}
         ...     ${NONE}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API create condition reset filter
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
    ${condition_id}      convert to string     ${arg_dic.condition_id}
    ${create_condition_reset_filter_path}        replace string          ${create_condition_reset_filter_path}      {rule_id}       ${rule_id}
    ${create_condition_reset_filter_path}       replace string       ${create_condition_reset_filter_path}      {mechanic_id}       ${mechanic_id}
    ${create_condition_reset_filter_path}        replace string        ${create_condition_reset_filter_path}    {condition_id}       ${condition_id}
    ${param_key_name}=   [Common] - Create string param        key_name        ${arg_dic.key_name}
    ${param_key_value_type}=   [Common] - Create string param        key_value_type        ${arg_dic.key_value_type}
    ${param_operator}=   [Common] - Create string param        operator        ${arg_dic.operator}
    ${param_key_value}=   [Common] - Create string param        key_value        ${arg_dic.key_value}


     ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_key_name}
        ...         	 ${param_key_value_type}
        ...         	 ${param_operator}
        ...         	 ${param_key_value}
        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },

                            create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             post request
         ...     api
         ...    ${create_condition_reset_filter_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get condition reset filter list
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
    ${condition_id}      convert to string     ${arg_dic.condition_id}
    ${get_condition_reset_filter_list_path}       replace string          ${get_condition_reset_filter_list_path}      {rule_id}       ${rule_id}
    ${get_condition_reset_filter_list_path}       replace string        ${get_condition_reset_filter_list_path}       {mechanic_id}       ${mechanic_id}
    ${get_condition_reset_filter_list_path}       replace string         ${get_condition_reset_filter_list_path}    {condition_id}       ${condition_id}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_condition_reset_filter_list_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API delete action
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
    ${action_id}      convert to string     ${arg_dic.action_id}
    ${delete_action_path}         replace string          ${delete_action_path}      {rule_id}       ${rule_id}
    ${delete_action_path}        replace string         ${delete_action_path}       {mechanic_id}       ${mechanic_id}
    ${delete_action_path}       replace string          ${delete_action_path}     {action_id}       ${action_id}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             delete request
         ...     api
         ...     ${delete_action_path}
         ...     ${NONE}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API delete action limit
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
    ${action_id}      convert to string     ${arg_dic.action_id}
    ${limit_id}      convert to string     ${arg_dic.limit_id}

    ${delete_action_limit_path}         replace string          ${delete_action_limit_path}      {rule_id}       ${rule_id}
    ${delete_action_limit_path}        replace string         ${delete_action_limit_path}       {mechanic_id}       ${mechanic_id}
    ${delete_action_limit_path}       replace string          ${delete_action_limit_path}     {action_id}       ${action_id}
    ${delete_action_limit_path}         replace string         ${delete_action_limit_path}      {limit_id}       ${limit_id}

                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             delete request
         ...     api
         ...     ${delete_action_limit_path}
         ...     ${NONE}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API search action history
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
     ${param_action_id}=   [Common] - Create int param        action_id        ${arg_dic.action_id}

     ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_action_id}
        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },

                            create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             post request
         ...     api
         ...    ${search_action_history_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API get rule limit list
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${get_rule_limit list_path}       replace string         ${get_rule_limit list_path}    {rule_id}       ${rule_id}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_rule_limit list_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API delete rule limit
    [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}

    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${rule_limit_id}      convert to string     ${arg_dic.rule_limit_id}

    ${delete_rule_limit_path}          replace string          ${delete_rule_limit_path}       {rule_id}       ${rule_id}
    ${delete_rule_limit_path}         replace string         ${delete_rule_limit_path}        {rule_limit_id}       ${rule_limit_id}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             delete request
         ...     api
         ...     ${delete_rule_limit_path}
         ...     ${NONE}
         ...     ${NONE}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update comparison
    [Arguments]         ${arg_dic}

    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
    ${condition_id}      convert to string     ${arg_dic.condition_id}
    ${comparison_id}      convert to string     ${arg_dic.comparison_id}
    ${update_comparison_path}         replace string            ${update_comparison_path}      {rule_id}       ${rule_id}
    ${update_comparison_path}        replace string           ${update_comparison_path}     {mechanic_id}       ${mechanic_id}
    ${update_comparison_path}       replace string           ${update_comparison_path}      {condition_id}       ${condition_id}
    ${update_comparison_path}       replace string           ${update_comparison_path}      {comparison_id}       ${comparison_id}

    ${param_key_name}=   Create Json data for  param       key_name        ${arg_dic.key_name}
    ${param_key_value_type}=   [Common] - Create string param        key_value_type        ${arg_dic.key_value_type}
    ${param_operator}=   [Common] - Create string param        operator       ${arg_dic.operator}
    ${param_key_value}=   [Common] - Create string param        key_value       ${arg_dic.key_value}



    ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_key_name}
        ...         	 ${param_key_value_type}
        ...         	 ${param_operator}
        ...         	 ${param_key_value}

        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${update_comparison_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log        ${response.text}
    ${return_dic} =	Create Dictionary   response=${response}
    [Return]    ${return_dic}

API update condition filter
    [Arguments]         ${arg_dic}

    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
    ${condition_id}      convert to string     ${arg_dic.condition_id}
    ${filter_id}      convert to string     ${arg_dic.filter_id}
    ${update_condition_filter_path}       replace string          ${update_condition_filter_path}      {rule_id}       ${rule_id}
    ${update_condition_filter_path}       replace string        ${update_condition_filter_path}      {mechanic_id}       ${mechanic_id}
    ${update_condition_filter_path}       replace string         ${update_condition_filter_path}   {condition_id}       ${condition_id}
    ${update_condition_filter_path}       replace string         ${update_condition_filter_path}    {filter_id}       ${filter_id}
    ${param_key_name}=   [Common] - Create string param        key_name        ${arg_dic.key_name}
    ${param_key_value_type}=   [Common] - Create string param        key_value_type        ${arg_dic.key_value_type}
    ${param_operator}=   [Common] - Create string param        operator        ${arg_dic.operator}
    ${param_key_value}=   [Common] - Create string param        key_value        ${arg_dic.key_value}
    ${param_is_consecutive_key}=   [Common] - Create string param        is_consecutive_key        ${arg_dic.is_consecutive_key}


    ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_key_name}
        ...         	 ${param_key_value_type}
        ...         	 ${param_operator}
        ...         	 ${param_key_value}
        ...         	 ${param_is_consecutive_key}
        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${update_condition_filter_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log        ${response.text}
    ${return_dic} =	Create Dictionary   response=${response}
    [Return]    ${return_dic}


API update action
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
    ${action_id}      convert to string     ${arg_dic.action_id}
    ${update_action_path}       replace string          ${update_action_path}      {rule_id}       ${rule_id}
    ${update_action_path}       replace string       ${update_action_path}     {mechanic_id}       ${mechanic_id}
    ${update_action_path}       replace string      ${update_action_path}      {action_id}       ${action_id}
    ${param_action_type_id}=   [Common] - Create int param     action_type_id        ${arg_dic.action_type_id}
    ${param_user_id}=   [Common] - Create int param     user_id        ${arg_dic.user_id}
    ${param_user_type}=   [Common] - Create string param     user_type        ${arg_dic.user_type}

    ${data_json}    set variable    ${empty}
    ${length}   get length      ${arg_dic.data}
    :FOR     ${index}   IN RANGE   0    ${length}
    \      ${action_data_item}    get from list      ${arg_dic.data}      ${index}
    \      ${action_data_item}    strip string         ${action_data_item}
    \      ${data_json}     catenate    SEPARATOR=,  ${data_json}   ${action_data_item}
    log     ${data_json}
    ${action_data_json}              catenate       SEPARATOR=
        ...     	"data":
        ...     	[
        ...             ${data_json}
        ...     	]
    ${action_data_json}     replace string      ${action_data_json}     [,        [
    ${action_data_json}     replace string          ${action_data_json}     ,]        ]

    ${data}              catenate           SEPARATOR=
        ...     {
        ...         ${param_action_type_id}
        ...         ${param_user_id}
        ...         ${param_user_type}
        ...         ${action_data_json}
        ...     }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${update_action_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log        ${data}
    log        ${response.text}
    ${return_dic} =	Create Dictionary   response=${response}
    [Return]    ${return_dic}

API create action limitation
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
    ${action_id}      convert to string     ${arg_dic.action_id}
    ${create_limitation_path}      replace string          ${create_limitation_path}      {rule_id}       ${rule_id}
    ${create_limitation_path}      replace string       ${create_limitation_path}     {mechanic_id}       ${mechanic_id}
    ${create_limitation_path}      replace string      ${create_limitation_path}     {action_id}       ${action_id}
    ${param_limit_type}=   [Common] - Create string param     limit_type        ${arg_dic.limit_type}
    ${param_value}=   [Common] - Create int param     value        ${arg_dic.value}


    ${data_json}    set variable    ${empty}
    ${length}   get length      ${arg_dic.data}
    :FOR     ${index}   IN RANGE   0    ${length}
    \      ${action_limit_data_item}    get from list      ${arg_dic.data}      ${index}
    \      ${action_limit_data_item}    strip string         ${action_limit_data_item}
    \      ${data_json}     catenate    SEPARATOR=,  ${data_json}   ${action_limit_data_item}
    log     ${data_json}
    ${action_limit_data_json}              catenate       SEPARATOR=
        ...     	,"filters":
        ...     	[
        ...             ${data_json}
        ...     	]
    ${action_limit_data_json}     replace string      ${action_limit_data_json}     [,        [
    ${action_limit_data_json}     replace string          ${action_limit_data_json}     ,]        ]

    ${data}              catenate       SEPARATOR=
        ...     {
        ...         ${param_limit_type}
        ...         ${param_value}
        ...         ${action_limit_data_json}
        ...     }
    ${data}  replace string        ${data}      ,,      ,
    ${data}  replace string        ${data}      ,}      }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_limitation_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${data}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API update action limitation
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${mechanic_id}      convert to string     ${arg_dic.mechanic_id}
    ${action_id}      convert to string     ${arg_dic.action_id}
    ${limit_id}      convert to string     ${arg_dic.limit_id}
    ${update_action_limit_path}      replace string          ${update_action_limit_path}      {rule_id}       ${rule_id}
    ${update_action_limit_path}     replace string       ${update_action_limit_path}    {mechanic_id}       ${mechanic_id}
    ${update_action_limit_path}      replace string     ${update_action_limit_path}    {action_id}       ${action_id}
    ${update_action_limit_path}      replace string     ${update_action_limit_path}    {limit_id}       ${limit_id}
    ${param_limit_type}=   [Common] - Create string param     limit_type        ${arg_dic.limit_type}
    ${param_value}=   [Common] - Create int param     value        ${arg_dic.value}

    ${data_json}    set variable    ${empty}
    ${length}   get length      ${arg_dic.data}
    :FOR     ${index}   IN RANGE   0    ${length}
    \      ${action_limit_data_item}    get from list      ${arg_dic.data}      ${index}
    \      ${action_limit_data_item}    strip string         ${action_limit_data_item}
    \      ${data_json}     catenate    SEPARATOR=,  ${data_json}   ${action_limit_data_item}
    log     ${data_json}
    ${action_limit_data_json}              catenate       SEPARATOR=
        ...     	"filters":
        ...     	[
        ...             ${data_json}
        ...     	]
    ${action_limit_data_json}     replace string      ${action_limit_data_json}     [,        [
    ${action_limit_data_json}     replace string          ${action_limit_data_json}     ,]        ]

    ${data}              catenate           SEPARATOR=
        ...     {
        ...         ${param_limit_type}
        ...         ${param_value}
        ...         ${action_limit_data_json}
        ...     }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${update_action_limit_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log        ${data}
    log        ${response.text}
    ${return_dic} =	Create Dictionary   response=${response}
    [Return]    ${return_dic}


API create rule limitation
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
    ${rule_id}      convert to string     ${arg_dic.rule_id}
    ${create_rule_limit_path}      replace string          ${create_rule_limit_path}     {rule_id}       ${rule_id}
    ${param_limit_type}=   [Common] - Create string param     limit_type        ${arg_dic.limit_type}
    ${param_limit_value}=   [Common] - Create int param     limit_value        ${arg_dic.limit_value}


    ${data_json}    set variable    ${empty}
    ${length}   get length      ${arg_dic.data}
    :FOR     ${index}   IN RANGE   0    ${length}
    \      ${rule_limit_data_item}    get from list      ${arg_dic.data}      ${index}
    \      ${rule_limit_data_item}    strip string         ${rule_limit_data_item}
    \      ${data_json}     catenate    SEPARATOR=,  ${data_json}   ${rule_limit_data_item}
    log     ${data_json}
    ${rule_limit_data_json}              catenate       SEPARATOR=
        ...     	,"filters":
        ...     	[
        ...             ${data_json}
        ...     	]
    ${rule_limit_data_json}     replace string      ${rule_limit_data_json}     [,        [
    ${rule_limit_data_json}     replace string          ${rule_limit_data_json}     ,]        ]

    ${data}              catenate       SEPARATOR=
        ...     {
        ...         ${param_limit_type}
        ...         ${param_limit_value}
        ...         ${rule_limit_data_json}
        ...     }
    ${data}  replace string        ${data}      ,,      ,
    ${data}  replace string        ${data}      ,}      }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${create_rule_limit_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log        ${data}
    log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


call deactive rule api
    [Arguments]     ${rule_id}      ${rule_name}
    ${bak_rule_id}=   convert to string          ${rule_id}
    ${path}=    replace string        ${update_rule_api_path}       rule_id         ${bak_rule_id}
    ${header}               create dictionary
    ...     client_id=${setup_admin_client_id}
    ...     client_secret=${setup_admin_client_secret}
    ...     content-type=application/json
    ...     Authorization=Bearer ${suite_admin_access_token}
    create session          api     ${api_gateway_host}     disable_warnings=1
    ${data}              catenate
        ...	{
        ...     "name":"${rule_name}",
        ...     "is_active":"false"
        ...	}
    ${response}             put request
        ...     api
        ...     ${path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    ${response_text}=   set variable    ${response.text}
#    should be equal as integers     ${response.status_code}     200
    [Return]    ${response}
