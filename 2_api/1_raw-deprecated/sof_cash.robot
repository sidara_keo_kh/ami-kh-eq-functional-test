*** Settings ***
Resource        ../../1_common/keywords.robot


*** Keywords ***
API suspend sof cash
    [Arguments]
    ...    ${sof_cash_id}
    ...    ${access_token}
    ...    ${header_client_id}
    ...    ${header_client_secret}
    ...    ${is_suspended}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}

    ${data}              catenate
        ...     {
        ...         "is_suspended":${is_suspended}
        ...     }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${sof_cash_id}    convert to string    ${sof_cash_id}
    ${suspend_sof_cash_path}=    replace string      ${update_status_cash_source_of_fund_path}        {sof_cash_id}       ${sof_cash_id}    1
    ${response}             put request
        ...     api
        ...     ${suspend_sof_cash_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic} =	Create Dictionary   response=${response}    status_message=${response.json()['status']['message']}    status_code=${response.json()['status']['code']}
    [Return]    ${dic}

API get sof cash detail
    [Arguments]
    ...    ${sof_cash_id}
    ...    ${access_token}
    ...    ${header_client_id}
    ...    ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
        ...     correlation_id=${suite_correlation_id}
    ${data}              catenate
        ...     {
        ...         "id":${sof_cash_id}
        ...     }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${get_sof_cash_detail_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log    ${response.json()['data']['cash_sofs'][0]}
    ${dic} =	Create Dictionary   response=${response}
    set to dictionary   ${dic}      data=${response.json()['data']['cash_sofs'][0]}
    [Return]    ${dic}

API update sof cash prefill amount
    [Arguments]    ${dic}

    ${sof_cash_id}=    get from dictionary    ${dic}   sof_cash_id
    ${access_token}=    get from dictionary    ${dic}   access_token
    ${header_client_id}=    get from dictionary    ${dic}   header_client_id
    ${header_client_secret}=    get from dictionary    ${dic}   header_client_secret
    ${prefill_amount}=    get from dictionary    ${dic}   prefill_amount

    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}

    ${data}              catenate
        ...     {
        ...         "prefill_amount":${prefill_amount}
        ...     }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${sof_cash_id}    convert to string    ${sof_cash_id}
    ${update_sof_cash_prefill_amount_path}=    replace string      ${update_prefill_amount_cash_source_of_fund_path}        {sof_cash_id}       ${sof_cash_id}    1
    ${response}             put request
        ...     api
        ...     ${update_sof_cash_prefill_amount_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log    ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    #    status_message=${response.json()['status']['message']}    status_code=${response.json()['status']['code']}
    [Return]    ${dic}