*** Settings ***
Resource        ../../1_common/keywords.robot

*** Keywords ***
API search card history on report
    [Arguments]  ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_trans_id}=  [Common] - Create string param in dic    ${arg_dic}     trans_id
    ${param_card_id}=  [Common] - Create int param in dic    ${arg_dic}     card_id
    ${param_user_id}=  [Common] - Create int param in dic    ${arg_dic}     user_id
    ${param_user_type_id}=  [Common] - Create int param in dic    ${arg_dic}     user_type_id
    ${param_paging}=  [Common] - Create boolean param in dic    ${arg_dic}     paging
    ${param_page_index}=  [Common] - Create int param in dic    ${arg_dic}     page_index
    ${data}              catenate            SEPARATOR=
        ...     {
        ...        ${param_trans_id}
        ...        ${param_card_id}
        ...        ${param_user_id}
        ...        ${param_user_type_id}
        ...        ${param_paging}
        ...        ${param_page_index}
        ...     }
    ${data}  replace string      ${data}      ,}    }
                    create session          api     ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${search_card_history_on_report_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}

        should be equal as integers     ${response.status_code}         200

    ${dic} =   Create Dictionary   response=${response}
    [Return]    ${dic}

API search company representatives profile on report
    [Arguments]  ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_id}=                    [Common] - Create int param in dic          ${arg_dic}     id
    ${param_company_id}=            [Common] - Create int param in dic          ${arg_dic}     company_id
    ${param_type_id}=               [Common] - Create int param in dic          ${arg_dic}     type_id
    ${param_is_deleted}=            [Common] - Create boolean param in dic      ${arg_dic}     is_deleted
    ${param_paging}=                [Common] - Create boolean param in dic      ${arg_dic}     paging
    ${param_page_index}=            [Common] - Create int param in dic          ${arg_dic}     page_index
    ${data}              catenate            SEPARATOR=
        ...     {
        ...        ${param_id}
        ...        ${param_company_id}
        ...        ${param_type_id}
        ...        ${param_paging}
        ...        ${param_is_deleted}
        ...        ${param_page_index}
        ...     }
    ${data}  replace string      ${data}      ,}    }

    REST.post   ${api_gateway_host}/${search_company_representatives_profile_path}
        ...     body=${data}
        ...     headers=${header}
    rest extract

API search OTP
    [Arguments]   &{arg_dic}
    ${param_id}=            [Common] - Create string param in dic    ${arg_dic}     id
    ${param_user_id}=       [Common] - Create string param in dic    ${arg_dic}     user_id
    ${param_user_type}=     [Common] - Create string param in dic    ${arg_dic}     user_type
    ${param_delivery_channel}=  [Common] - Create string param in dic    ${arg_dic}     delivery_channel
    ${param_email}=             [Common] - Create string param in dic    ${arg_dic}     email
    ${param_mobile_number}=     [Common] - Create string param in dic    ${arg_dic}     mobile_number
    ${param_is_deleted}=        [Common] - Create string param in dic    ${arg_dic}     is_deleted
    ${param_is_success_verified}=  [Common] - Create string param in dic    ${arg_dic}     is_success_verified

    ${param_from}   set variable   ${empty}
    ${status}   run keyword and return status       Dictionary Should Contain Key     ${arg_dic}          date_from
    ${param_json}     run keyword if      '${status}'=='True'   [Common] - Create string param      from		${arg_dic.date_from}
    ...   ELSE    set variable    ${empty}

    ${param_to}=                   [Common] - Create string param in dic    ${arg_dic}     to
    ${param_otp_reference_id}=     [Common] - Create string param in dic    ${arg_dic}     otp_reference_id
    ${param_otp_code}=             [Common] - Create string param in dic    ${arg_dic}     otp_code
    ${param_user_reference_code}=  [Common] - Create string param in dic    ${arg_dic}     user_reference_code
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     Authorization=Bearer ${arg_dic.access_token}
        ...     content-type=application/json
    ${request}              catenate    SEPARATOR=
        ...     {
        ...     	${param_id}
        ...     	${param_user_id}
        ...     	${param_user_type}
        ...     	${param_delivery_channel}
        ...     	${param_email}
        ...     	${param_mobile_number}
        ...     	${param_is_deleted}
        ...     	${param_is_success_verified}
        ...     	${param_from}
        ...     	${param_to}
        ...     	${param_otp_reference_id}
        ...     	${param_otp_code}
        ...     	${param_user_reference_code}
        ...     }
    ${request}  replace string      ${request}      ,}    }
                                create session          api     ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${search_otp_path}
        ...     ${request}
        ...     ${NONE}
        ...     ${header}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search customer
    [Arguments]    &{arg_dic}
    ${param_customer_id}=  [Common] - Create int param in dic   ${arg_dic}   customer_id
    ${param_id}=  [Common] - Create int param in dic   ${arg_dic}   id
    ${param_unique_reference}=  [Common] - Create string param in dic   ${arg_dic}   unique_reference
    ${param_mobile_number}=  [Common] - Create string param in dic     ${arg_dic}   mobile_number
    ${param_email}=  [Common] - Create string param in dic             ${arg_dic}   email
    ${param_citizen_card_id}=  [Common] - Create string param in dic  ${arg_dic}   citizen_card_id
    ${param_kyc_status}=  [Common] - Create string param in dic       ${arg_dic}    kyc_status
    ${param_from_created_timestamp}=  [Common] - Create string param in dic    ${arg_dic}   from_created_timestamp
    ${param_to_created_timestamp}=  [Common] - Create string param in dic    ${arg_dic}     to_created_timestamp
    ${param_paging}=  [Common] - Create boolean param in dic  ${arg_dic}    paging
    ${param_page_index}=  [Common] - Create int param in dic  ${arg_dic}    page_index
    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...         ${param_customer_id}
        ...     	${param_id}
        ...     	${param_unique_reference}
        ...     	${param_mobile_number}
        ...     	${param_email}
        ...     	${param_citizen_card_id}
        ...     	${param_kyc_status}
        ...     	${param_from_created_timestamp}
        ...     	${param_to_created_timestamp}
        ...     	${param_paging}
        ...     	${param_page_index}
        ...     }
    ${data}  replace string      ${data}      ,}    }
                create session          api        ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${search_customer_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search system-user
    [Arguments]  ${client_id}   ${client_secret}    ${access_token}
    ...     ${username}
    ...     ${user_id}
    ...     ${is_suspended}
    ...     ${suspend_reason}
    ...     ${email}
    ...     ${role_id}
    ...     ${paging}
    ...     ${page_index}
    ${param_username}=  [Common] - Create string param     username        ${username}
    ${param_user_id}=  [Common] - Create int param     user_id        ${user_id}
    ${param_is_suspended}=  [Common] - Create string param     is_suspended        ${is_suspended}
    ${param_suspend_reason}=  [Common] - Create string param     suspend_reason        ${suspend_reason}
    ${param_email}=  [Common] - Create string param     email        ${email}
    ${param_role_id}=  [Common] - Create int param     role_id        ${role_id}
    ${param_paging}=  [Common] - Create boolean param     paging        ${paging}
    ${param_page_index}=  [Common] - Create int param      page_index        ${page_index}


    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	${param_username}
        ...     	${param_user_id}
        ...     	${param_is_suspended}
        ...         ${param_suspend_reason}
        ...     	${param_email}
        ...     	${param_role_id}
        ...     	${param_paging}
        ...     	${param_page_index}
        ...     }
    ${data}  replace string      ${data}      ,}    }
                create session          api        ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${search_system-user_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search cash source of fund
    [Arguments]  ${client_id}   ${client_secret}    ${access_token}
        ...     ${user_id}
        ...     ${user_type}
        ...     ${currency}
        ...     ${paging}
        ...     ${page_index}
    ${param_user_type}=  [Common] - Create int param    user_type        ${user_type}
    ${param_user_id}=  [Common] - Create int param     user_id        ${user_id}
    ${param_currency}=  [Common] - Create string param     currency        ${currency}
    ${param_paging}=  [Common] - Create boolean param     paging        ${paging}
    ${param_page_index}=  [Common] - Create int param      page_index        ${page_index}


    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	${param_user_id}
        ...     	${param_user_type}
        ...     	${param_currency}
        ...     	${param_paging}
        ...     	${param_page_index}
        ...     }
    ${data}  replace string      ${data}      ,}    }
                create session          api        ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${search_cash_source_of_fund_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search cash transaction
    [Arguments]  ${client_id}   ${client_secret}    ${access_token}
        ...     ${order_id}
        ...     ${sof_id}
        ...     ${action_id}
        ...     ${status_id}
        ...     ${order_detail_id}
        ...     ${paging}
        ...     ${page_index}
    ${param_order_id}=  [Common] - Create string param    order_id        ${order_id}
    ${param_sof_id}=  [Common] - Create int param     sof_id        ${sof_id}
    ${param_action_id}=  [Common] - Create int param      action_id        ${action_id}
    ${param_status_id}=  [Common] - Create int param      status_id        ${status_id}
    ${param_order_detail_id}=  [Common] - Create string param      order_detail_id        ${order_detail_id}
    ${param_paging}=  [Common] - Create boolean param      paging        ${paging}
    ${param_page_index}=  [Common] - Create int param      page_index        ${page_index}


    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	${param_order_id}
        ...     	${param_sof_id}
        ...     	${param_action_id}
        ...     	${param_status_id}
        ...     	${param_order_detail_id}
        ...         ${param_paging}
        ...         ${param_page_index}
        ...     }
    ${data}  replace string      ${data}      ,}    }
                create session          api        ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${search_cash_transaction_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search payment orders
    [Arguments]  ${client_id}   ${client_secret}    ${access_token}
        ...     ${order_id}
        ...     ${ext_transaction_id}
        ...     ${status_id_list}
        ...     ${state}
        ...     ${to_last_updated_timestamp}
        ...     ${from_last_updated_timestamp}
        ...     ${created_client_id}
        ...     ${executed_client_id}
        ...     ${created_channel_type}
        ...     ${created_device_unique_reference}
        ...     ${error_codes}
        ...     ${service_name}
        ...     ${created_channel_type}
        ...     ${service_id_list}
        ...     ${user_type_id}
        ...     ${file_type}
        ...     ${row_number}
        ...     ${from}
        ...     ${to}
        ...     ${product_ref}
        ...     ${product_name}
        ...     ${ref_order_id}
        ...     ${paging}
        ...     ${page_index}
    ${param_order_id}=  [Common] - Create string param    order_id        ${order_id}
    ${param_ext_transaction_id}=  [Common] - Create string param    ext_transaction_id        ${ext_transaction_id}
    ${param_status_id_list}=  [Common] - Create Json data for list param without quote    status_id_list        ${status_id_list}
    ${param_state}=  [Common] - Create string param    state        ${state}
    ${param_to_last_updated_timestamp}=  [Common] - Create string param    to_last_updated_timestamp        ${to_last_updated_timestamp}
    ${param_from_last_updated_timestamp}=  [Common] - Create string param    from_last_updated_timestamp        ${from_last_updated_timestamp}
    ${param_created_client_id}=  [Common] - Create string param    created_client_id        ${created_client_id}
    ${param_executed_client_id}=  [Common] - Create string param    executed_client_id        ${executed_client_id}
    ${param_created_channel_type}=  [Common] - Create string param    created_channel_type        ${created_channel_type}
    ${param_created_device_unique_reference}=  [Common] - Create string param    created_device_unique_reference        ${created_device_unique_reference}
    ${param_error_codes}=  [Common] - Create Json data for list param with quote    error_codes        ${error_codes}
    ${param_service_name}=  [Common] - Create string param    service_name        ${service_name}
    ${param_created_channel_type}=  [Common] - Create string param    created_channel_type        ${created_channel_type}
    ${param_service_id_list}=  [Common] - Create Json data for list param without quote    service_id_list        ${service_id_list}
    ${param_user_type_id}=  [Common] - Create int param    user_type_id        ${user_type_id}
    ${param_file_type}=  [Common] - Create string param    file_type        ${file_type}
    ${param_row_number}=  [Common] - Create int param   row_number        ${row_number}
    ${param_from}=  [Common] - Create string param    from        ${from}
    ${param_to}=  [Common] - Create string param    to        ${to}
    ${param_product_ref}=  [Common] - Create string param    product_ref        ${product_ref}
    ${param_product_name}=  [Common] - Create string param    product_name        ${product_name}
    ${param_ref_order_id}=  [Common] - Create string param    ref_order_id        ${ref_order_id}
    ${param_paging}=  [Common] - Create boolean param    paging        ${paging}
    ${param_page_index}=  [Common] - Create int param    page_index        ${page_index}

    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	${param_order_id}
        ...     	${param_ext_transaction_id}
        ...     	${param_status_id_list}
        ...     	${param_state}
        ...     	${param_to_last_updated_timestamp}
        ...         ${param_from_last_updated_timestamp}
        ...         ${param_created_client_id}
        ...         ${param_executed_client_id}
        ...         ${param_created_channel_type}
        ...         ${param_created_device_unique_reference}
        ...         ${param_error_codes}
        ...         ${param_service_name}
        ...         ${param_created_channel_type}
        ...         ${param_service_id_list}
        ...         ${param_user_type_id}
        ...         ${param_file_type}
        ...         ${param_row_number}
        ...         ${param_from}
        ...         ${param_to}
        ...         ${param_product_ref}
        ...         ${param_product_name}
        ...         ${param_ref_order_id}
        ...         ${param_paging}
        ...         ${param_page_index}
        ...     }
    ${data}  replace string      ${data}      ,}    }
                create session          api        ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${search_payment_orders_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search bank source of fund
    [Arguments]  ${client_id}   ${client_secret}    ${access_token}
        ...     ${user_id}
        ...     ${user_type_id}
        ...     ${currency}
        ...     ${bank_account_number}
        ...     ${ext_bank_reference}
        ...     ${bank_token}
        ...     ${from_created_timestamp}
        ...     ${to_created_timestamp}
        ...     ${is_deleted}
        ...     ${paging}
        ...     ${page_index}

    ${param_user_type_id}=  [Common] - Create int param    user_type_id        ${user_type_id}
    ${param_user_id}=  [Common] - Create int param     user_id        ${user_id}
    ${param_currency}=  [Common] - Create string param     currency        ${currency}
    ${param_bank_account_number}=  [Common] - Create string param     bank_account_number        ${bank_account_number}
    ${param_ext_bank_reference}=  [Common] - Create string param     ext_bank_reference        ${ext_bank_reference}
    ${param_bank_token}=  [Common] - Create string param     bank_token        ${bank_token}
    ${param_from_created_timestamp}=  [Common] - Create string param     from_created_timestamp        ${from_created_timestamp}
    ${param_to_created_timestamp}=  [Common] - Create string param     to_created_timestamp        ${to_created_timestamp}
    ${param_is_deleted}=  [Common] - Create boolean param     is_deleted        ${is_deleted}
    ${param_paging}=  [Common] - Create boolean param     paging        ${paging}
    ${param_page_index}=  [Common] - Create int param      page_index        ${page_index}


    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	${param_user_id}
        ...     	${param_user_type_id}
        ...     	${param_currency}
        ...     	${param_bank_account_number}
        ...     	${param_ext_bank_reference}
        ...     	${param_bank_token}
        ...     	${param_from_created_timestamp}
        ...     	${param_to_created_timestamp}
        ...     	${param_is_deleted}
        ...     	${param_paging}
        ...     	${param_page_index}
        ...     }
    ${data}  replace string      ${data}      ,}    }
                create session          api        ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${search_bank_source_of_fund_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search card source of fund
    [Arguments]  ${client_id}   ${client_secret}    ${access_token}
        ...     ${user_id}
        ...     ${user_type_id}
        ...     ${currency}
        ...     ${from_created_timestamp}
        ...     ${to_created_timestamp}
        ...     ${paging}
        ...     ${page_index}

    ${param_user_type_id}=  [Common] - Create int param    user_type_id        ${user_type_id}
    ${param_user_id}=  [Common] - Create int param     user_id        ${user_id}
    ${param_currency}=  [Common] - Create string param     currency        ${currency}
    ${param_from_created_timestamp}=  [Common] - Create string param     from_created_timestamp        ${from_created_timestamp}
    ${param_to_created_timestamp}=  [Common] - Create string param     to_created_timestamp        ${to_created_timestamp}
    ${param_paging}=  [Common] - Create boolean param     paging        ${paging}
    ${param_page_index}=  [Common] - Create int param      page_index        ${page_index}


    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	${param_user_id}
        ...     	${param_user_type_id}
        ...     	${param_currency}
        ...     	${param_from_created_timestamp}
        ...     	${param_to_created_timestamp}
        ...     	${param_paging}
        ...     	${param_page_index}
        ...     }
    ${data}  replace string      ${data}      ,}    }
                create session          api        ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${search_card_source_of_fund_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search bank
    [Arguments]  ${client_id}   ${client_secret}    ${access_token}
        ...     ${id}
        ...     ${name}
        ...     ${currency}
        ...     ${status}

    ${param_id}=  [Common] - Create int param    id       ${id}
    ${param_name}=  [Common] - Create string param     name        ${name}
    ${param_currency}=  [Common] - Create string param     currency        ${currency}
    ${param_status}=  [Common] - Create boolean param     status        ${status}


    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	${param_id}
        ...     	${param_name}
        ...     	${param_currency}
        ...     	${param_status}
        ...     }
    ${data}  replace string      ${data}      ,}    }
                create session          api        ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${search_bank_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API report - search bank transactions
    [Arguments]  ${client_id}   ${client_secret}    ${access_token}
        ...     ${order_id}
        ...     ${bank_name}
        ...     ${bank_id}
        ...     ${currency}
        ...     ${status}
        ...     ${action_id}
        ...     ${page_index}
        ...     ${paging}


    ${param_order_id}=  [Common] - Create int param    order_id        ${order_id}
    ${param_bank_name}=  [Common] - Create int param     bank_name        ${bank_name}
    ${param_bank_id}=  [Common] - Create int param     bank_id        ${bank_id}
    ${param_currency}=  [Common] - Create string param     currency        ${currency}
    ${param_status}=  [Common] - Create boolean param     status        ${status}
    ${param_action_id}=  [Common] - Create int param    action_id        ${action_id}
    ${param_paging}=  [Common] - Create boolean param     paging        ${paging}
    ${param_page_index}=  [Common] - Create int param      page_index        ${page_index}


    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	${param_order_id}
        ...     	${param_bank_name}
        ...     	${param_bank_id}
        ...     	${param_currency}
        ...     	${param_status}
        ...     	${param_action_id}
        ...     	${param_paging}
        ...     	${param_page_index}
        ...     }
    ${data}  replace string      ${data}      ,}    }
                create session          api        ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${report_-_search_bank_transactions_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search voucher
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_id}     [Common] - Create int param in dic        ${arg_dic}     id
    ${param_voucher_id}		[Common] - Create string param in dic     ${arg_dic}		voucher_id
    ${param_product_ref2}		[Common] - Create string param in dic     ${arg_dic}		product_ref2
    ${param_cash_in_user_id}		[Common] - Create int param in dic     ${arg_dic}		cash_in_user_id
    ${param_cash_in_user_type}		[Common] - Create string param in dic     ${arg_dic}		cash_in_user_type
    ${param_cash_out_user_id}		[Common] - Create int param in dic     ${arg_dic}		cash_out_user_id
    ${param_cash_in_order_id}		[Common] - Create int param in dic     ${arg_dic}		cash_in_order_id
    ${param_cash_out_order_id}		[Common] - Create int param in dic     ${arg_dic}		cash_out_order_id
    ${param_issuer_user_id}		[Common] - Create int param in dic     ${arg_dic}		issuer_user_id
    ${param_issuer_user_type}		[Common] - Create string param in dic     ${arg_dic}		issuer_user_type
    ${param_is_used}		[Common] - Create string param in dic     ${arg_dic}		is_used
    ${param_from_created_timestamp}		[Common] - Create string param in dic     ${arg_dic}		from_created_timestamp
    ${param_to_created_timestamp}		[Common] - Create string param in dic     ${arg_dic}		to_created_timestamp
    ${param_from_expire_date_timestamp}		[Common] - Create string param in dic     ${arg_dic}		from_expire_date_timestamp
    ${param_to_expire_date_timestamp}		[Common] - Create string param in dic     ${arg_dic}		to_expire_date_timestamp
    ${param_is_on_hold}		[Common] - Create string param in dic     ${arg_dic}		is_on_hold
    ${param_is_cancelled}		[Common] - Create string param in dic     ${arg_dic}		is_cancelled
    ${param_paging}		[Common] - Create boolean param in dic     ${arg_dic}      paging
    ${param_page_index}		[Common] - Create int param in dic     ${arg_dic}    page_index

    ${request}              catenate        SEPARATOR=
        ...     {
        ...     	${param_id}
        ...     	${param_voucher_id}
        ...     	${param_product_ref2}
        ...     	${param_cash_in_user_id}
        ...     	${param_cash_in_user_type}
        ...     	${param_cash_out_user_id}
        ...     	${param_cash_in_order_id}
        ...     	${param_cash_out_order_id}
        ...     	${param_issuer_user_id}
        ...     	${param_issuer_user_type}
        ...     	${param_is_used}
        ...     	${param_from_created_timestamp}
        ...     	${param_to_created_timestamp}
        ...     	${param_from_expire_date_timestamp}
        ...     	${param_to_expire_date_timestamp}
        ...     	${param_is_on_hold}
        ...     	${param_is_cancelled}
        ...     	${param_paging}
        ...     	${param_page_index}
        ...     }
    ${request}   replace string      ${request}      ,}    }

                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_voucher_path}
        ...     ${request}
        ...     ${NONE}
        ...     ${header}
    log    ${request}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API search voucher refunds
    [Arguments]
    ...     ${access_token}
    ...     ${id}
    ...     ${original_voucher_id}
    ...     ${requested_username}
    ...     ${status_id}
    ...     ${from_created_timestamp}
    ...     ${to_created_timestamp}
    ...     ${paging}
    ...     ${page_index}
    ...     ${header_client_id}
    ...     ${header_client_secret}
    ${header}               create dictionary
        ...     client_id=${header_client_id}
        ...     client_secret=${header_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}

    ${param_id}		[Common] - Create int param		id		${id}
    ${param_original_voucher_id}		[Common] - Create string param		original_voucher_id		${original_voucher_id}
    ${param_requested_username}		[Common] - Create string param		requested_username		${requested_username}
    ${param_status_id}		[Common] - Create int param		status_id		${status_id}
    ${param_from_created_timestamp}		[Common] - Create string param		from_created_timestamp		${from_created_timestamp}
    ${param_to_created_timestamp}		[Common] - Create string param		to_created_timestamp		${to_created_timestamp}
    ${param_paging}		[Common] - Create boolean param		paging		${paging}
    ${param_page_index}		[Common] - Create int param		page_index		${page_index}

    ${request}              catenate        SEPARATOR=
    ...      {
    ...        ${param_id}
    ...        ${param_original_voucher_id}
    ...        ${param_requested_username}
    ...        ${param_status_id}
    ...        ${param_from_created_timestamp}
    ...        ${param_to_created_timestamp}
    ...        ${param_paging}
    ...        ${param_page_index}
    ...      }
    ${request}   replace string      ${request}      ,}    }

                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_voucher_refunds_path}
        ...     ${request}
        ...     ${NONE}
        ...     ${header}
    log    ${request}
    log    ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API report - search card transactions
    [Arguments]
        ...     ${access_token}
        ...     ${sof_id}
        ...     ${user_id}
        ...     ${user_type}
        ...     ${order_id}
        ...     ${order_detail_id}
        ...     ${provider_name}
        ...     ${currency}
        ...     ${status}
        ...     ${action_id}
        ...     ${card_design_name}
        ...     ${card_design_number}
        ...     ${card_account_name}
        ...     ${card_account_number}
        ...     ${paging}
        ...     ${page_index}
        ...     ${client_id}
        ...     ${client_secret}
    ${param_sof_id}=  [Common] - Create int param        sof_id        ${sof_id}
    ${param_user_id}=  [Common] - Create int param        user_id        ${user_id}
    ${param_user_type}=  [Common] - Create string param      user_type        ${user_type}
    ${param_order_id}=  [Common] - Create string param       order_id        ${order_id}
    ${param_order_detail_id}=  [Common] - Create int param      order_detail_id        ${order_detail_id}
    ${param_provider_name}=  [Common] - Create string param        provider_name        ${provider_name}
    ${param_currency}=  [Common] - Create string param       currency        ${currency}
    ${param_status}=  [Common] - Create boolean param        status        ${status}
    ${param_action_id}=  [Common] - Create int param     action_id        ${action_id}
    ${param_paging}=  [Common] - Create boolean param        paging        ${paging}
    ${param_page_index}=  [Common] - Create int param    page_index        ${page_index}
    ${param_card_design_name}=  [Common] - Create string param        card_design_name        ${card_design_name}
    ${param_card_design_number}=  [Common] - Create string param      card_design_number        ${card_design_number}
    ${param_card_account_name}=  [Common] - Create string param       card_account_name        ${card_account_name}
    ${param_card_account_number}=  [Common] - Create string param     card_account_number        ${card_account_number}

    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	${param_sof_id}
        ...     	${param_user_id}
        ...     	${param_user_type}
        ...     	${param_order_id}
        ...     	${param_order_detail_id}
        ...     	${param_provider_name}
        ...     	${param_currency}
        ...     	${param_status}
        ...     	${param_action_id}
        ...     	${param_paging}
        ...     	${param_page_index}
        ...     	${param_card_design_name}
        ...     	${param_card_design_number}
        ...     	${param_card_account_name}
        ...     	${param_card_account_number}
        ...     }

    ${data}  replace string      ${data}      ,}    }
                create session          api        ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${report_-_search_card_transactions_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

[200] API search token information
    [Arguments]
    ...     ${client_id}
    ...     ${client_secret}
    ...     ${access_token}
    ${correlation_id}       generate random string  20  [UPPER]
    ${header}               create dictionary
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 "is_expired" : false,
        ...              "is_deleted" : false
        ...	}
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_trust_token_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

[200] API search agent devices
    [Arguments]
    ...     ${agent_id}
    ...     ${shop_id}
    ...     ${is_deleted}
    ...     ${access_token}
    ...     ${client_id}
    ...     ${client_secret}
    ${header}               create dictionary
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
    ${param_agent_id}=  [Common] - Create int param        agent_id        ${agent_id}
    ${param_shop_id}=  [Common] - Create int param        shop_id        ${shop_id}
    ${param_is_deleted}=  [Common] - Create boolean param      is_deleted        ${is_deleted}

    ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_agent_id}
        ...              ${param_shop_id}
        ...              ${param_is_deleted}
        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${report_search_agent_device}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search customer classifications
    [Arguments]         ${arg_dic}

    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_id}=  [Common] - Create int param        id        ${arg_dic.id}
    ${param_name}=  [Common] - Create string param      name        ${arg_dic.name}
    ${param_is_deleted}=  [Common] - Create boolean param      is_deleted        ${arg_dic.is_deleted}

    ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_id}
        ...              ${param_name}
        ...              ${param_is_deleted}
        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_customer_classifications}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    ${return_dic} =	Create Dictionary   response=${response}
    [Return]    ${return_dic}

API search service group
    [Arguments]         ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_service_group_id}=  [Common] - Create int param        service_group_id        ${arg_dic.service_group_id}
    ${param_service_group_name}=  [Common] - Create string param      service_group_name        ${arg_dic.service_group_name}
    ${param_to_created_timestamp}=  [Common] - Create string param      to_created_timestamp       ${arg_dic.to_created_timestamp}
    ${param_from_created_timestamp}=  [Common] - Create string param      from_created_timestamp        ${arg_dic.from_created_timestamp}
    ${param_from_last_updated_timestamp}=  [Common] - Create string param      from_last_updated_timestamp        ${arg_dic.from_last_updated_timestamp}
    ${param_to_last_updated_timestamp}=  [Common] - Create string param      to_last_updated_timestamp        ${arg_dic.to_last_updated_timestamp}
    ${param_is_deleted}=  [Common] - Create boolean param      is_deleted        ${arg_dic.is_deleted}
    ${param_file_type}=  [Common] - Create string param      file_type        ${arg_dic.file_type}
    ${param_row_number}=  [Common] - Create int param      row_number        ${arg_dic.row_number}


    ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_service_group_id}
        ...              ${param_service_group_name}
        ...              ${param_to_created_timestamp}
        ...              ${param_from_created_timestamp}
        ...              ${param_from_last_updated_timestamp}
        ...              ${param_to_last_updated_timestamp}
        ...              ${param_is_deleted}
        ...              ${param_file_type}
        ...              ${param_row_number}
        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_service_group}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${response.text}
    ${return_dic} =	Create Dictionary   response=${response}
    [Return]    ${return_dic}

API search services
    [Arguments]         ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_id}=  [Common] - Create int param        id        ${arg_dic.id}
    ${param_service_group_id}=  [Common] - Create int param        service_group_id        ${arg_dic.service_group_id}
    ${param_status}=  [Common] - Create int param      status        ${arg_dic.status}
    ${param_currency}=  [Common] - Create string param      currency       ${arg_dic.currency}
    ${param_service_name}=  [Common] - Create string param      service_name        ${arg_dic.service_name}
    ${param_file_type}=  [Common] - Create string param      file_type        ${arg_dic.file_type}
    ${param_row_number}=  [Common] - Create string param      row_number        ${arg_dic.row_number}
    ${param_is_deleted}=  [Common] - Create boolean param      is_deleted        ${arg_dic.is_deleted}


    ${data}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_service_group_id}
        ...              ${param_id}
        ...              ${param_status}
        ...              ${param_currency}
        ...              ${param_service_name}
        ...              ${param_file_type}
        ...              ${param_is_deleted}
        ...              ${param_row_number}
        ...	}
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_services}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}

    log         ${response.text}
    ${return_dic} =	Create Dictionary   response=${response}
    [Return]    ${return_dic}

API get report order detail
     [Arguments]    ${arg_dic}
     ${header}               create dictionary
         ...     client_id=${arg_dic.client_id}
         ...     client_secret=${arg_dic.client_secret}
         ...     content-type=application/json
         ...     Authorization=Bearer ${arg_dic.access_token}
     ${order_id}        convert to string           ${arg_dic.order_id}
     ${get_report_order_detail_path}          replace string          ${get_report_order_detail_path}       {orderId}       ${order_id}
                 create session          api     ${api_gateway_host}     disable_warnings=1
     ${response}             get request
         ...     api
         ...     ${get_report_order_detail_path}
         ...     ${header}
     log        ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search vouchers distributed to user
    [Arguments]  ${arg_dic}
    ${param_voucher_type}  [Common] - Create string param in dic    ${arg_dic}     voucher_type
    ${param_is_used}  [Common] - Create boolean param in dic   ${arg_dic}     is_used
    ${param_paging}  [Common] - Create boolean param in dic   ${arg_dic}     paging
    ${param_page_index}  [Common] - Create int param in dic   ${arg_dic}     page_index

    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	${param_voucher_type}
        ...     	${param_is_used}
        ...     	${param_paging}
        ...     	${param_page_index}
        ...     }
    ${data}  replace string      ${data}      ,}    }
                create session          api        ${api_gateway_host}    disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_vouchers_distributed_to_user_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get list of cancel voucher in formation by cancel id
    [Arguments]  ${arg_dic}
    ${param_voucher_cancellation_id}  [Common] - Create int param in dic    ${arg_dic}     voucher_cancellation_id

    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	${param_voucher_cancellation_id}
        ...     }
    ${data}  replace string      ${data}      ,}    }
                create session          api        ${api_gateway_host}    disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${get_list_of_cancel_voucher_in_formation_by_cancel_id_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search voucher adjustments
    [Arguments]  ${arg_dic}
    ${param_id}  [Common] - Create int param in dic    ${arg_dic}     id
    ${param_original_voucher_id}  [Common] - Create string param in dic    ${arg_dic}     original_voucher_id
    ${param_requested_username}  [Common] - Create string param in dic    ${arg_dic}     requested_username
    ${param_status_id}  [Common] - Create int param in dic    ${arg_dic}     status_id
    ${param_action}  [Common] - Create string param in dic    ${arg_dic}     action
    ${param_from_created_timestamp}  [Common] - Create string param in dic    ${arg_dic}     from_created_timestamp
    ${param_to_created_timestamp}  [Common] - Create string param in dic    ${arg_dic}     to_created_timestamp
    ${param_paging}  [Common] - Create boolean param in dic    ${arg_dic}     paging
    ${param_page_index}  [Common] - Create int param in dic    ${arg_dic}     page_index

    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	${param_id}
        ...     	${param_original_voucher_id}
        ...     	${param_requested_username}
        ...     	${param_status_id}
        ...     	${param_action}
        ...     	${param_from_created_timestamp}
        ...     	${param_to_created_timestamp}
        ...     	${param_paging}
        ...     	${param_page_index}
        ...     }
    ${data}  replace string      ${data}      ,}    }
                create session          api        ${api_gateway_host}    disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_voucher_adjustments_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API search password rule configuration
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${data}              catenate   SEPARATOR=
         ...    {
        ...    	"identity_type_id":"${arg_dic.identity_type_id}"
        ...    }
     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_password_rule_configuration_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search identity types
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_id}                             [Common] - Create string param in dic     ${arg_dic}      id
    ${param_is_deleted}                     [Common] - Create string param in dic     ${arg_dic}      is_deleted
    ${param_name}                           [Common] - Create string param in dic     ${arg_dic}      name
    ${data}              catenate   SEPARATOR=
        ...    {
        ...    	${param_id}
        ...    	${param_is_deleted}
        ...    	${param_name}
        ...    }
     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_identity_types_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search customer devices
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_customer_id}         [Common] - Create string param in dic     ${arg_dic}      customer_id
    ${param_is_deleted}          [Common] - Create string param in dic     ${arg_dic}      is_deleted

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_customer_id}
         ...    	${param_is_deleted}
         ...    }
    ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_customer_devices_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search customer identity
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_customer_id}         [Common] - Create string param in dic     ${arg_dic}      customer_id
    ${param_id}                  [Common] - Create string param in dic     ${arg_dic}      id

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_customer_id}
         ...    	${param_id}
         ...    }
    ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_customer_identity_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API search agent
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_id}                                [Common] - Create string param in dic     ${arg_dic}      id
    ${param_unique_reference}                  [Common] - Create string param in dic     ${arg_dic}      unique_reference
    ${param_email}                             [Common] - Create string param in dic     ${arg_dic}      email
    ${param_is_testing_account}                [Common] - Create string param in dic     ${arg_dic}      is_testing_account
    ${param_is_system_account}                 [Common] - Create string param in dic     ${arg_dic}      is_system_account
    ${param_accreditation_status_id}           [Common] - Create string param in dic     ${arg_dic}      accreditation_status_id
    ${param_primary_mobile_number}             [Common] - Create string param in dic     ${arg_dic}      primary_mobile_number
    ${param_mobile_device_unique_reference}    [Common] - Create string param in dic     ${arg_dic}      mobile_device_unique_reference
    ${param_edc_serial_number}                 [Common] - Create string param in dic     ${arg_dic}      edc_serial_number
    ${param_pos_serial_number}                 [Common] - Create string param in dic     ${arg_dic}      pos_serial_number
    ${param_from_created_timestamp}            [Common] - Create string param in dic     ${arg_dic}      from_created_timestamp
    ${param_to_created_timestamp}              [Common] - Create string param in dic     ${arg_dic}      to_created_timestamp
    ${param_paging}=  [Common] - Create boolean param in dic  ${arg_dic}    paging
    ${param_page_index}=  [Common] - Create int param in dic  ${arg_dic}    page_index
    ${param_file_type}                         [Common] - Create string param in dic     ${arg_dic}      file_type
    ${param_row_number}                        [Common] - Create string param in dic     ${arg_dic}      row_number

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_id}
         ...    	${param_unique_reference}
         ...    	${param_email}
         ...    	${param_is_testing_account}
         ...    	${param_is_system_account}
         ...    	${param_accreditation_status_id}
         ...    	${param_primary_mobile_number}
         ...    	${param_mobile_device_unique_reference}
         ...    	${param_edc_serial_number}
         ...    	${param_pos_serial_number}
         ...    	${param_from_created_timestamp}
         ...    	${param_to_created_timestamp}
         ...    	${param_paging}
         ...    	${param_page_index}
         ...    	${param_file_type}
         ...    	${param_row_number}
         ...    }
    ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_agent_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search agent company profile
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_id}                              [Common] - Create string param in dic     ${arg_dic}      id
    ${param_name}                            [Common] - Create string param in dic     ${arg_dic}      name
    ${param_business_type}                   [Common] - Create string param in dic     ${arg_dic}      business_type
    ${param_company_owner_first_name}        [Common] - Create string param in dic     ${arg_dic}      company_owner_first_name
    ${param_company_owner_last_name}         [Common] - Create string param in dic     ${arg_dic}      company_owner_last_name
    ${param_paging}                          [Common] - Create boolean param in dic    ${arg_dic}      paging
    ${param_page_index}                      [Common] - Create int param in dic    ${arg_dic}      page_index

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_id}
         ...    	${param_name}
         ...    	${param_business_type}
         ...    	${param_company_owner_first_name}
         ...    	${param_company_owner_last_name}
         ...    	${param_paging}
         ...        ${param_page_index}
         ...    }
     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_agent_company_profile_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search agent accreditation status
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_id}                              [Common] - Create string param in dic     ${arg_dic}      id
    ${param_country_code}                            [Common] - Create string param in dic     ${arg_dic}      country_code

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_id}
         ...    	${param_country_code}
         ...    }
     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_agent_accreditation_status_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search agent mm card type
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_id}                              [Common] - Create string param in dic     ${arg_dic}      id

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_id}
         ...    }
     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_agent_mm_card_type_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all agent identities
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_agent_id}                      [Common] - Create string param in dic     ${arg_dic}      agent_id
    ${param_id}                            [Common] - Create string param in dic     ${arg_dic}      id

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_agent_id}
         ...    	${param_id}
         ...    }
     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${get_all_agent_identities_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search agent classifications
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_id}                      [Common] - Create string param in dic     ${arg_dic}      id
    ${param_name}                    [Common] - Create string param in dic     ${arg_dic}      name
    ${param_is_deleted}              [Common] - Create string param in dic     ${arg_dic}      is_deleted

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_id}
         ...    	${param_name}
         ...        ${param_is_deleted}
         ...    }
     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_agent_classifications_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API search agent smart card
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	"agent_id":"${arg_dic.agent_id}"
         ...    }
     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_agents_mart_card_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search shop type
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_id}                      [Common] - Create string param in dic     ${arg_dic}      id
    ${param_name}                    [Common] - Create string param in dic     ${arg_dic}      name

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_id}
         ...    	${param_name}
         ...    }
     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_shop_type_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API search agent shop
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_id}                                  [Common] - Create string param in dic     ${arg_dic}      id
    ${param_agent_id}                            [Common] - Create string param in dic     ${arg_dic}      agent_id
    ${param_name}                                [Common] - Create string param in dic     ${arg_dic}      name
    ${param_relationship_manager_id}             [Common] - Create string param in dic     ${arg_dic}      relationship_manager_id
    ${param_is_deleted}                          [Common] - Create string param in dic     ${arg_dic}      is_deleted
    ${param_relationship_manager_name}           [Common] - Create string param in dic     ${arg_dic}      relationship_manager_name
    ${param_shop_category_id}                    [Common] - Create string param in dic     ${arg_dic}      shop_category_id
    ${param_shop_type_id}                        [Common] - Create string param in dic     ${arg_dic}      shop_type_id
    ${param_mobile_device_unique_reference}      [Common] - Create string param in dic     ${arg_dic}      mobile_device_unique_reference
    ${param_edc_serial_number}                   [Common] - Create string param in dic     ${arg_dic}      edc_serial_number
    ${param_pos_serial_number}                   [Common] - Create string param in dic     ${arg_dic}      pos_serial_number
    ${param_paging}                              [Common] - Create string param in dic     ${arg_dic}      paging
    ${param_page_index}                          [Common] - Create string param in dic     ${arg_dic}      page_index

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_id}
         ...    	${param_agent_id}
         ...    	${param_name}
         ...    	${param_relationship_manager_id}
         ...    	${param_is_deleted}
         ...    	${param_relationship_manager_name}
         ...    	${param_shop_category_id}
         ...    	${param_shop_type_id}
         ...    	${param_mobile_device_unique_reference}
         ...    	${param_edc_serial_number}
         ...    	${param_pos_serial_number}
         ...    	${param_paging}
         ...    	${param_page_index}
         ...    }
    ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_agent_shop_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search channels permission
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_user_type_id}               [Common] - Create string param in dic     ${arg_dic}      user_type_id
    ${param_shop_id}                    [Common] - Create string param in dic     ${arg_dic}      shop_id
    ${param_user_id}                    [Common] - Create string param in dic     ${arg_dic}      user_id
    ${param_channel_id}                 [Common] - Create string param in dic     ${arg_dic}      channel_id

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_user_type_id}
         ...    	${param_shop_id}
         ...    	${param_user_id}
         ...    	${param_channel_id}
         ...    }

     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_channels_permission_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search edc
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_serial_number}              [Common] - Create string param in dic     ${arg_dic}      serial_number
    ${param_terminal_id}                [Common] - Create string param in dic     ${arg_dic}      terminal_id
    ${param_shop_id}                    [Common] - Create string param in dic     ${arg_dic}      shop_id
    ${param_id}                         [Common] - Create string param in dic     ${arg_dic}      id
    ${param_paging}                     [Common] - Create string param in dic     ${arg_dic}      paging
    ${param_page_index}                 [Common] - Create string param in dic     ${arg_dic}      page_index

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_serial_number}
         ...    	${param_terminal_id}
         ...    	${param_shop_id}
         ...    	${param_id}
         ...    	${param_paging}
         ...    	${param_page_index}
         ...    }

     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_edc_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search agent relationship
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_relationship_type_ids}      [Common] - Create string param in dic     ${arg_dic}      serial_number
    ${param_id}                         [Common] - Create string param in dic     ${arg_dic}      id
    ${param_user_id}                    [Common] - Create string param in dic     ${arg_dic}      user_id
    ${param_is_sharing_benefit}         [Common] - Create string param in dic     ${arg_dic}      is_sharing_benefit
    ${param_main_user_id}               [Common] - Create string param in dic     ${arg_dic}      main_user_id
    ${param_is_sub_user_id}             [Common] - Create string param in dic     ${arg_dic}      sub_user_id
    ${param_paging}                     [Common] - Create string param in dic     ${arg_dic}      paging
    ${param_page_index}                 [Common] - Create string param in dic     ${arg_dic}      page_index

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_relationship_type_ids}
         ...    	${param_id}
         ...    	${param_user_id}
         ...    	${param_is_sharing_benefit}
          ...    	${param_main_user_id}
         ...    	${param_is_sub_user_id}
         ...    	${param_paging}
         ...    	${param_page_index}
         ...    }

     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_agent_relationship_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search categories
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_name}               [Common] - Create string param in dic     ${arg_dic}      name
    ${param_is_active}          [Common] - Create string param in dic     ${arg_dic}      is_active
    ${param_id}                 [Common] - Create string param in dic     ${arg_dic}      id
    ${param_paging}             [Common] - Create string param in dic     ${arg_dic}      paging
    ${param_page_index}         [Common] - Create string param in dic     ${arg_dic}      page_index

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_name}
         ...    	${param_is_active}
         ...    	${param_id}
         ...    	${param_paging}
         ...    	${param_page_index}
         ...    }

     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_categories_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search products
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_name}                     [Common] - Create string param in dic     ${arg_dic}      name
    ${param_is_active}                [Common] - Create string param in dic     ${arg_dic}      is_active
    ${param_product_category_id}      [Common] - Create string param in dic     ${arg_dic}      product_category_id
    ${param_is_deleted}               [Common] - Create string param in dic     ${arg_dic}      is_deleted
    ${param_id}                       [Common] - Create string param in dic     ${arg_dic}      id
    ${param_paging}                   [Common] - Create string param in dic     ${arg_dic}      paging
    ${param_page_index}               [Common] - Create string param in dic     ${arg_dic}      page_index

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_name}
         ...    	${param_is_active}
         ...    	${param_product_category_id}
         ...    	${param_is_deleted}
         ...    	${param_id}
         ...    	${param_paging}
         ...    	${param_page_index}
         ...    }

     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_products_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search channels
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_channel_type_id}          [Common] - Create string param in dic     ${arg_dic}      channel_type_id
    ${param_is_deleted}               [Common] - Create string param in dic     ${arg_dic}      is_deleted
    ${param_user_type_id}             [Common] - Create string param in dic     ${arg_dic}      user_type_id
    ${param_id}                       [Common] - Create string param in dic     ${arg_dic}      id

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_channel_type_id}
         ...    	${param_is_deleted}
         ...    	${param_user_type_id}
         ...    	${param_id}
         ...    }

     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_channels_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search products agent type relation
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_id}                       [Common] - Create string param in dic     ${arg_dic}      id
    ${param_agent_type_id}          [Common] - Create string param in dic     ${arg_dic}      agent_type_id
    ${param_product_id}               [Common] - Create string param in dic     ${arg_dic}      product_id

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_id}
         ...        ${param_agent_type_id}
         ...        ${param_product_id}
         ...    }

     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_products_agent_type_relation_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search card on report
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_token}                      [Common] - Create string param in dic     ${arg_dic}      token
    ${param_user_id}                    [Common] - Create string param in dic     ${arg_dic}      user_id
    ${param_user_type_id}               [Common] - Create int param in dic        ${arg_dic}      user_type_id
    ${param_card_identifier}            [Common] - Create string param in dic     ${arg_dic}      card_identifier
    ${param_paging}                     [Common] - Create boolean param in dic    ${arg_dic}      paging
    ${param_page_index}                 [Common] - Create int param in dic        ${arg_dic}      page_index

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_token}
         ...        ${param_user_id}
         ...        ${param_user_type_id}
         ...        ${param_card_identifier}
         ...    	${param_paging}
         ...    	${param_page_index}
         ...    }

     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_card_on_report_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search shop categories
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_id}                      [Common] - Create string param in dic     ${arg_dic}      id
    ${param_name}                    [Common] - Create string param in dic     ${arg_dic}      name

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_id}
         ...    	${param_name}
         ...    }

     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_shop_categories_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}



API report search SOF bank list
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_user_id}                             [Common] - Create string param in dic     ${arg_dic}      user_id
    ${param_user_type_id}                        [Common] - Create string param in dic     ${arg_dic}      user_type_id
    ${param_currency}                            [Common] - Create string param in dic     ${arg_dic}      currency
    ${param_bank_account_number}                 [Common] - Create string param in dic     ${arg_dic}      bank_account_number
    ${param_ext_bank_reference}                  [Common] - Create string param in dic     ${arg_dic}      ext_bank_reference
    ${param_bank_token}                          [Common] - Create string param in dic     ${arg_dic}      bank_token
    ${param_from_created_timestamp}              [Common] - Create string param in dic     ${arg_dic}      from_created_timestamp
    ${param_to_created_timestamp}                [Common] - Create string param in dic     ${arg_dic}      to_created_timestamp
    ${param_is_deleted}                          [Common] - Create string param in dic     ${arg_dic}      is_deleted
    ${param_paging}                              [Common] - Create string param in dic     ${arg_dic}      paging
    ${param_page_index}                          [Common] - Create string param in dic     ${arg_dic}      page_index

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_user_id}
         ...        ${param_user_type_id}
         ...        ${param_currency}
          ...    	${param_bank_account_number}
         ...        ${param_ext_bank_reference}
         ...        ${param_bank_token}
         ...        ${param_from_created_timestamp}
         ...    	${param_to_created_timestamp}
         ...        ${param_is_deleted}
         ...    	${param_paging}
         ...    	${param_page_index}
         ...    }

     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${report_search_SOF_bank_list_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search reconciled partner file
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_is_on_us}                         [Common] - Create string param in dic     ${arg_dic}      is_on_us
    ${param_service_name}                     [Common] - Create string param in dic     ${arg_dic}      service_name
    ${param_agent_id}                         [Common] - Create string param in dic     ${arg_dic}      agent_id
    ${param_currency}                         [Common] - Create string param in dic     ${arg_dic}      currency
    ${param_status_id}                        [Common] - Create string param in dic     ${arg_dic}      status_id
    ${param_from_last_updated_timestamp}      [Common] - Create string param in dic     ${arg_dic}      from_last_updated_timestamp
    ${param_to_last_updated_timestamp}        [Common] - Create string param in dic     ${arg_dic}      to_last_updated_timestamp

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_is_on_us}
         ...        ${param_service_name}
         ...        ${param_agent_id}
         ...    	${param_currency}
         ...        ${param_status_id}
         ...        ${param_from_last_updated_timestamp}
         ...        ${param_to_last_updated_timestamp}
         ...    }

     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_reconciled_partner_file_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search reconciled partner reconcile result
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_is_on_us}                         [Common] - Create string param in dic     ${arg_dic}      is_on_us
    ${param_service_name}                     [Common] - Create string param in dic     ${arg_dic}      service_name
    ${param_agent_id}                         [Common] - Create string param in dic     ${arg_dic}      agent_id
    ${param_currency}                         [Common] - Create string param in dic     ${arg_dic}      currency
    ${param_status_id}                        [Common] - Create string param in dic     ${arg_dic}      status_id
    ${param_payment_type}                     [Common] - Create string param in dic     ${arg_dic}      payment_type
    ${param_partner_file_id}                  [Common] - Create string param in dic     ${arg_dic}      partner_file_id
    ${param_from_last_updated_timestamp}      [Common] - Create string param in dic     ${arg_dic}      from_last_updated_timestamp
    ${param_to_last_updated_timestamp}        [Common] - Create string param in dic     ${arg_dic}      to_last_updated_timestamp

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_is_on_us}
         ...        ${param_service_name}
         ...        ${param_agent_id}
         ...    	${param_currency}
         ...        ${param_status_id}
         ...    	${param_payment_type}
         ...        ${param_partner_file_id}
         ...        ${param_from_last_updated_timestamp}
         ...        ${param_to_last_updated_timestamp}
         ...    }

     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_reconciled_partner_reconcile_result_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search reconciled sof file
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_is_on_us}                         [Common] - Create string param in dic     ${arg_dic}      is_on_us
    ${param_source_of_fund}                   [Common] - Create string param in dic     ${arg_dic}      source_of_fund
    ${param_sof_code}                         [Common] - Create string param in dic     ${arg_dic}      sof_code
    ${param_currency}                         [Common] - Create string param in dic     ${arg_dic}      currency
    ${param_status_id}                        [Common] - Create string param in dic     ${arg_dic}      status_id
    ${param_file_name}                        [Common] - Create string param in dic     ${arg_dic}      file_name
    ${param_from_last_updated_timestamp}      [Common] - Create string param in dic     ${arg_dic}      from_last_updated_timestamp
    ${param_to_last_updated_timestamp}        [Common] - Create string param in dic     ${arg_dic}      to_last_updated_timestamp

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_is_on_us}
         ...        ${param_source_of_fund}
         ...        ${param_sof_code}
         ...    	${param_currency}
         ...        ${param_status_id}
         ...    	${param_payment_type}
         ...        ${param_file_name}
         ...        ${param_from_last_updated_timestamp}
         ...        ${param_to_last_updated_timestamp}
         ...    }

     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_reconciled_sof_file_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search reconciled sof reconcile result
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_is_on_us}                         [Common] - Create string param in dic     ${arg_dic}      is_on_us
    ${param_source_of_fund}                   [Common] - Create string param in dic     ${arg_dic}      source_of_fund
    ${param_sof_code}                         [Common] - Create string param in dic     ${arg_dic}      sof_code
    ${param_currency}                         [Common] - Create string param in dic     ${arg_dic}      currency
    ${param_status_id}                        [Common] - Create string param in dic     ${arg_dic}      status_id
    ${param_payment_type}                     [Common] - Create string param in dic     ${arg_dic}      payment_type
    ${param_from_last_updated_timestamp}      [Common] - Create string param in dic     ${arg_dic}      from_last_updated_timestamp
    ${param_to_last_updated_timestamp}        [Common] - Create string param in dic     ${arg_dic}      to_last_updated_timestamp
    ${param_sof_file_id}                      [Common] - Create string param in dic     ${arg_dic}      sof_file_id

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_is_on_us}
         ...        ${param_source_of_fund}
         ...        ${param_sof_code}
         ...    	${param_currency}
         ...        ${param_status_id}
         ...    	${param_payment_type}
         ...        ${param_from_last_updated_timestamp}
         ...        ${param_to_last_updated_timestamp}
         ...        ${param_sof_file_id}
         ...    }

     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_reconciled_sof_reconcile_result_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all roles
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_id}                         [Common] - Create string param in dic     ${arg_dic}     id
    ${param_user_id}                   [Common] - Create string param in dic     ${arg_dic}      user_id

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_id}
         ...        ${param_user_id}
         ...    }

     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${get_all_roles_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all permissions
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_id}                        [Common] - Create string param in dic     ${arg_dic}     id
    ${param_role_id}                   [Common] - Create string param in dic     ${arg_dic}     role_id

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_id}
         ...        ${param_role_id}
         ...    }

     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${get_all_permissions_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search card type
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_id}                         [Common] - Create string param in dic     ${arg_dic}     id

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_id}
         ...    }

     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_card_type_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search card sof provider
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_name}         [Common] - Create string param in dic     ${arg_dic}     name

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_name}
         ...    }

     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_card_sof_provider_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search card design
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_provider}                   [Common] - Create string param in dic     ${arg_dic}     provider
    ${param_currency}                   [Common] - Create string param in dic     ${arg_dic}     currency
    ${param_card_type_id}               [Common] - Create string param in dic     ${arg_dic}     card_type_id
    ${param_name}                       [Common] - Create string param in dic     ${arg_dic}     name

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_provider}
         ...        ${param_currency}
         ...    	${param_card_type_id}
         ...        ${param_name}
         ...    }

     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_card_design_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}



API search card transactions
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_sof_id}                         [Common] - Create string param in dic     ${arg_dic}     sof_id
    ${param_currency}                       [Common] - Create string param in dic     ${arg_dic}     currency
    ${param_status_id}                      [Common] - Create string param in dic     ${arg_dic}     status_id
    ${param_action_id}                      [Common] - Create string param in dic     ${arg_dic}     action_id
    ${param_ext_reference}                  [Common] - Create string param in dic     ${arg_dic}     ext_reference
    ${param_from_created_timestamp}         [Common] - Create string param in dic     ${arg_dic}     from_created_timestamp
    ${param_to_created_timestamp}           [Common] - Create string param in dic     ${arg_dic}     to_created_timestamp
    ${param_order_id}                       [Common] - Create string param in dic     ${arg_dic}     order_id
    ${param_user_id}                        [Common] - Create string param in dic     ${arg_dic}     user_id
    ${param_user_type_id}                   [Common] - Create string param in dic     ${arg_dic}     user_type_id
    ${param_from_last_updated_timestamp}    [Common] - Create string param in dic     ${arg_dic}     from_last_updated_timestamp
    ${param_to_last_updated_timestamp}      [Common] - Create string param in dic     ${arg_dic}     to_last_updated_timestamp
    ${param_order_detail_id}                [Common] - Create string param in dic     ${arg_dic}     order_detail_id
    ${param_provider_name}                  [Common] - Create string param in dic     ${arg_dic}     provider_name
    ${param_card_design_name}               [Common] - Create string param in dic     ${arg_dic}     card_design_name
    ${param_card_design_number}             [Common] - Create string param in dic     ${arg_dic}     card_design_number
    ${param_card_account_name}              [Common] - Create string param in dic     ${arg_dic}     card_account_name
    ${param_card_account_number}            [Common] - Create string param in dic     ${arg_dic}     card_account_number
    ${param_paging}                         [Common] - Create string param in dic     ${arg_dic}     paging
    ${param_page_index}                     [Common] - Create string param in dic     ${arg_dic}     page_index

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_sof_id}
         ...        ${param_currency}
         ...    	${param_status_id}
         ...        ${param_action_id}
         ...    	${param_ext_reference}
         ...        ${param_from_created_timestamp}
         ...    	${param_to_created_timestamp}
          ...    	${param_order_id}
         ...        ${param_user_id}
         ...    	${param_user_type_id}
         ...        ${param_from_last_updated_timestamp}
         ...    	${param_to_last_updated_timestamp}
         ...    	${param_order_detail_id}
         ...    	${param_provider_name}
         ...        ${param_card_design_name}
         ...    	${param_card_design_number}
          ...    	${param_card_account_name}
         ...        ${param_card_account_number}
         ...    	${param_paging}
         ...        ${param_page_index}
         ...    }

     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_card_transactions_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search card sof action history
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_user_type_id}                   [Common] - Create string param in dic     ${arg_dic}     user_type_id
    ${param_currency}                       [Common] - Create string param in dic     ${arg_dic}     currency
    ${param_is_success}                     [Common] - Create string param in dic     ${arg_dic}     is_success
    ${param_from_created_timestamp}         [Common] - Create string param in dic     ${arg_dic}     from_created_timestamp
    ${param_to_created_timestamp}           [Common] - Create string param in dic     ${arg_dic}     to_created_timestamp
    ${param_to_user_id}                     [Common] - Create string param in dic     ${arg_dic}     user_id
    ${param_to_action_id}                   [Common] - Create string param in dic     ${arg_dic}     action_id

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_user_type_id}
         ...        ${param_currency}
         ...    	${param_is_success}
         ...        ${param_from_created_timestamp}
         ...    	${param_to_created_timestamp}
         ...    	${param_to_user_id}
         ...        ${param_to_action_id}
         ...    }

     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_card_sof_action_history_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}



API get all balance adjustments
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_reference_id}                   [Common] - Create string param in dic     ${arg_dic}     reference_id
    ${param_product_service_id}             [Common] - Create string param in dic     ${arg_dic}     product_service_id
    ${param_reference_order_id}             [Common] - Create string param in dic     ${arg_dic}     reference_order_id
    ${param_created_user_id}                [Common] - Create string param in dic     ${arg_dic}     created_user_id
    ${param_created_user_type_id}           [Common] - Create string param in dic     ${arg_dic}     created_user_type_id
    ${param_approved_user_id}               [Common] - Create string param in dic     ${arg_dic}     approved_user_id
    ${param_approved_user_type_id}          [Common] - Create string param in dic     ${arg_dic}     approved_user_type_id
    ${param_approved_timestamp_from}        [Common] - Create string param in dic     ${arg_dic}     approved_timestamp_from
    ${param_approved_timestamp_to}          [Common] - Create string param in dic     ${arg_dic}     approved_timestamp_to
    ${param_initiator_user_id}              [Common] - Create string param in dic     ${arg_dic}     initiator_user_id
    ${param_initiator_user_type_id}         [Common] - Create string param in dic     ${arg_dic}     initiator_user_type_id
    ${param_payer_user_id}                  [Common] - Create string param in dic     ${arg_dic}     payer_user_id
    ${param_payer_user_type_id}             [Common] - Create string param in dic     ${arg_dic}     payer_user_type_id
    ${param_payee_user_id}                  [Common] - Create string param in dic     ${arg_dic}     payee_user_id
    ${param_payee_user_type_id}             [Common] - Create string param in dic     ${arg_dic}     payee_user_type_id
    ${param_status}                         [Common] - Create string param in dic     ${arg_dic}     status
    ${param_is_for_voucher_cancellation}    [Common] - Create string param in dic     ${arg_dic}     is_for_voucher_cancellation
    ${param_created_timestamp_from}         [Common] - Create string param in dic     ${arg_dic}     created_timestamp_from
    ${param_created_timestamp_to}           [Common] - Create string param in dic     ${arg_dic}     created_timestamp_to
    ${param_order_id}                       [Common] - Create string param in dic     ${arg_dic}     order_id
    ${param_currency}                       [Common] - Create string param in dic     ${arg_dic}     currency
    ${param_paging}                         [Common] - Create string param in dic     ${arg_dic}     paging
    ${param_page_index}                     [Common] - Create string param in dic     ${arg_dic}     page_index

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_reference_id}
         ...        ${param_product_service_id}
         ...    	${param_reference_order_id}
         ...        ${param_created_user_id}
         ...    	${param_created_user_type_id}
         ...    	${param_approved_user_id}
         ...        ${param_approved_user_type_id}
         ...    	${param_approved_timestamp_from}
         ...        ${param_approved_timestamp_to}
         ...    	${param_initiator_user_id}
         ...        ${param_initiator_user_type_id}
         ...    	${param_payer_user_id}
         ...    	${param_payer_user_type_id}
         ...        ${param_payee_user_id}
         ...    	${param_payee_user_type_id}
         ...        ${param_status}
         ...    	${param_is_for_voucher_cancellation}
         ...        ${param_created_timestamp_from}
         ...    	${param_created_timestamp_to}
         ...    	${param_order_id}
         ...        ${param_currency}
         ...    	${param_paging}
         ...        ${param_page_index}
         ...    }

     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${get_all_balance_adjustments_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API search list of fraud tickets
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_is_deleted}                  [Common] - Create boolean param in dic    ${arg_dic}     is_deleted
    ${param_ticket_id}                   [Common] - Create string param in dic     ${arg_dic}     ticket_id
    ${param_card_id}                     [Common] - Create string param in dic     ${arg_dic}     card_id
    ${param_action}                      [Common] - Create string param in dic     ${arg_dic}     action
    ${param_from_created_date}           [Common] - Create string param in dic     ${arg_dic}     from_created_date
    ${param_to_created_date}             [Common] - Create string param in dic     ${arg_dic}     to_created_date
    ${param_active_date_from}            [Common] - Create string param in dic     ${arg_dic}     active_date_from
    ${param_active_date_to}              [Common] - Create string param in dic     ${arg_dic}     active_date_to
    ${param_paging}                      [Common] - Create string param in dic     ${arg_dic}     paging
    ${param_page_index}                  [Common] - Create string param in dic     ${arg_dic}     page_index
    ${param_card_id_list}                [Common] - Create Json data for list param without quote in dictionary  ${arg_dic}     card_id_list

    ${data}              catenate   SEPARATOR=
         ...    {
         ...    	${param_is_deleted}
         ...        ${param_ticket_id}
         ...    	${param_card_id}
         ...        ${param_action}
         ...    	${param_from_created_date}
         ...    	${param_to_created_date}
         ...        ${param_active_date_from}
         ...        ${param_active_date_to}
         ...        ${param_ticket_id}
         ...    	${param_paging}
         ...        ${param_page_index}
         ...        ${param_card_id_list}
         ...    }

     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_list_of_fraud_tickets_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API add service ids to report service whitelist
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${report_type_id}      Convert To String       ${arg_dic.report_type_id}
    ${add_service_ids_to_report_service_whitelist_path}      replace string    ${add_service_ids_to_report_service_whitelist_path}    {report_type_id}    ${report_type_id}

    ${data}              catenate   SEPARATOR=
         ...    {
        ...    	"service_ids":[${arg_dic.service_ids}]
        ...    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${add_service_ids_to_report_service_whitelist_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all service ids from report service whitelist
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${report_type_id}      Convert To String       ${arg_dic.report_type_id}
    ${get_all_service_ids_from_report_service_whitelist_path}      replace string    ${get_all_service_ids_from_report_service_whitelist_path}    {report_type_id}  ${report_type_id}
                 create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
         ...     api
         ...     ${get_all_service_ids_from_report_service_whitelist_path}
         ...     ${header}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API remove service ids to report service whitelist
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${report_type_id}      Convert To String       ${arg_dic.report_type_id}
    ${remove_service_ids_to_report_service_whitelist_path}      replace string    ${remove_service_ids_to_report_service_whitelist_path}    {report_type_id}   ${report_type_id}

    ${data}              catenate   SEPARATOR=
        ...    {
        ...    	"service_ids":[${arg_dic.service_ids}]
        ...    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             delete request
        ...     api
        ...     ${remove_service_ids_to_report_service_whitelist_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic}	Create Dictionary   response=${response}
    [Return]    ${dic}

API search agent commission
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

        ${param_agent_ids}        [Common] - Create string param of object in dictionary     ${arg_dic}      agent_ids
        ${param_mobile_numbers}   [Common] - Create string param of object in dictionary     ${arg_dic}      mobile_numbers
        ${param_file_type}        [Common] - Create string param in dic               ${arg_dic}      file_type

        ${data}              catenate      SEPARATOR=
        ...    {
        ...    	${param_agent_ids}
        ...    	${param_mobile_numbers}
         ...    "start_timestamp":"${arg_dic.start_timestamp}",
        ...    	"end_timestamp":"${arg_dic.end_timestamp}",
        ...    	${param_file_type}
        ...    }
     ${data}    replace string      ${data}      ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${search_agent_commission_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API summary agents commission
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
        ${param_agent_ids}        [Common] - Create string param of object in dictionary     ${arg_dic}      agent_ids
        ${param_mobile_numbers}   [Common] - Create string param of object in dictionary     ${arg_dic}      mobile_numbers
    ${data}              catenate   SEPARATOR=
        ...    {
        ...    	${param_agent_ids}
        ...    	${param_mobile_numbers}
        ...    	"start_timestamp":"${arg_dic.start_timestamp}",
        ...    	"end_timestamp":"${arg_dic.end_timestamp}",
        ...    }
     ${data}    replace string      ${data}      ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${summary_agents_commission_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API summary payment transaction agent
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${data}              catenate   SEPARATOR=
        ...    {
        ...    	"start_timestamp":"${arg_dic.start_timestamp}",
        ...    	"end_timestamp":"${arg_dic.end_timestamp}",
        ...    	"user_id":"${arg_dic.user_id}"
        ...    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${summary_payment_transaction_agent_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API summary transaction own agent
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${agent_id}      Convert To String       ${arg_dic.agent_id}
    ${summary_transaction_own_agent_path}      replace string    ${summary_transaction_own_agent_path}    {agent_id}   ${agent_id}
    ${data}              catenate   SEPARATOR=
        ...    {
        ...    	"start_timestamp":"${arg_dic.start_timestamp}",
        ...    	"end_timestamp":"${arg_dic.end_timestamp}"
        ...    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${summary_transaction_own_agent_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API summary customer wallet info
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

        create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${summary_customer_wallet_info_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API summary agent wallet info
    [Arguments]     &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

        create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${summary_agent_wallet_info_path}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API summary order balance movements
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
         ${data}              catenate   SEPARATOR=
        ...    {
        ...    	"sof_id":"${arg_dic.sof_id}",
        ...    	"sof_type_id":"${arg_dic.sof_type_id}",
        ...     "user_id":${arg_dic.user_id},
        ...     "user_type_id":"${arg_dic.user_type_id}",
        ...     "from_created_timestamp":"${arg_dic.from_created_timestamp}",
	    ...     "to_created_timestamp":"${arg_dic.to_created_timestamp}",
	    ...     "status_id_list":[${arg_dic.status_id_list}]
        ...    }

        create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${summary_order_balance_movements_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get all report type
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
         ...     api
         ...     ${get_all_report_type_path}
         ...     ${header}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API get report formula
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${report_type_id}               Convert To String       ${arg_dic.report_type_id}
    ${get_report_formula_path}      replace string    ${get_report_formula_path}    {report_type_id}     ${report_type_id}

                create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             get request
         ...     api
         ...     ${get_report_formula_path}
         ...     ${header}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API update report formula
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${report_type_id}    Convert To String    ${arg_dic.report_type_id}
    ${update_report_formula_path}       replace string      ${update_report_formula_path}     {report_type_id}    ${report_type_id}

    ${param_description}    [Common] - Create string param in dic     ${arg_dic}      description
    ${tpv}    [Common] - Create string param    tpv      ${arg_dic.tpv}
    ${fee}    [Common] - Create string param     fee      ${arg_dic.fee}
    ${commission}    [Common] - Create string param     commission      ${arg_dic.commission}

    ${data}              catenate       SEPARATOR=
        ...    {
        ...    	"effective_timestamp":"${arg_dic.effective_timestamp}",
        ...    	${tpv}
        ...    	${fee}
        ...    	${commission}
        ...    }
     ${data}       replace string    ${data}     ,}   }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             put request
        ...     api
        ...     ${update_report_formula_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${NONE}
        ...     ${header}
    log     ${data}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search agent belong to company
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${data}              catenate           SEPARATOR=
        ...	{
        ...	  "page_index": 1,
        ...   "paging": true
        ...	}
    ${data}  replace string      ${data}      ,}    }
             create session          api     ${api_gateway_host}     disable_warnings=1
    ${company_id}=    convert to string   ${arg_dic.company_id}
    ${search_agent_belong_to_company_path}=    replace string      ${search_agent_belong_to_company_path}      {company_id}        ${company_id}   1
    ${response}             post request
        ...     api
        ...     ${search_agent_belong_to_company_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log      ${data}
    log      ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API get all company types
    [Arguments]     ${arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_is_deleted}                     [Common] - Create boolean param in dic    ${arg_dic}      is_deleted
    ${param_name}                           [Common] - Create string param in dic     ${arg_dic}      name
    ${param_id}                             [Common] - Create int param in dic        ${arg_dic}      id

    ${data}              catenate           SEPARATOR=
        ...	{
        ...     ${param_is_deleted}
        ...     ${param_name}
        ...     ${param_id}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${get_all_company_types}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log     ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API search payment orders with arguments = dic
    [Arguments]     ${arg_dic}

    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_user_id}                        [Common] - Create int param in dic    ${arg_dic}      user_id
    ${param_user_type_id}                   [Common] - Create int param in dic    ${arg_dic}      user_type_id
    ${param_order_id}                       [Common] - Create string param in dic     ${arg_dic}      order_id
    ${param_ext_transaction_id}             [Common] - Create string param in dic     ${arg_dic}      ext_transaction_id
    ${param_status_id_list}                 [Common] - Create Json data for list param without quote in dictionary     ${arg_dic}      status_id_list
    ${param_state}                          [Common] - Create string param in dic     ${arg_dic}      state
    ${param_to_last_updated_timestamp}      [Common] - Create string param in dic     ${arg_dic}      to_last_updated_timestamp
    ${param_from_last_updated_timestamp}    [Common] - Create string param in dic     ${arg_dic}      from_last_updated_timestamp
    ${param_created_client_id}              [Common] - Create string param in dic     ${arg_dic}      created_client_id
    ${param_executed_client_id}             [Common] - Create string param in dic     ${arg_dic}      executed_client_id
    ${param_created_channel_type}           [Common] - Create string param in dic     ${arg_dic}      created_channel_type
    ${param_created_device_unique_reference}           [Common] - Create string param in dic     ${arg_dic}      created_device_unique_reference
    ${param_error_codes}                    [Common] - Create Json data for list param with quote in dictionary       ${arg_dic}      error_codes
    ${param_service_name}                   [Common] - Create string param in dic     ${arg_dic}      service_name
    ${param_service_group_name}             [Common] - Create string param in dic     ${arg_dic}      service_group_name
    ${param_payer_user_id}                  [Common] - Create int param in dic    ${arg_dic}      payer_user_id
    ${param_payer_user_type_id}             [Common] - Create int param in dic    ${arg_dic}      payer_user_type_id
    ${param_payee_user_id}                  [Common] - Create int param in dic    ${arg_dic}      payee_user_id
    ${param_payee_user_type_id}             [Common] - Create int param in dic    ${arg_dic}      payee_user_type_id
    ${param_created_channel_type}           [Common] - Create string param in dic     ${arg_dic}      created_channel_type
    ${param_service_id_list}                [Common] - Create Json data for list param without quote in dictionary     ${arg_dic}      service_id_list
    ${param_user_type_id}                   [Common] - Create int param in dic    ${arg_dic}      user_type_id
    ${param_file_type}                      [Common] - Create string param in dic     ${arg_dic}      file_type
    ${param_row_number}                     [Common] - Create int param in dic    ${arg_dic}      row_number
    ${param_from}                           [Common] - Create string param in dic     ${arg_dic}      from
    ${param_to}                             [Common] - Create string param in dic     ${arg_dic}      to
    ${param_product_ref}                    [Common] - Create string param in dic     ${arg_dic}      product_ref
    ${param_product_name}                   [Common] - Create string param in dic     ${arg_dic}      product_name
    ${param_ref_order_id}                   [Common] - Create string param in dic     ${arg_dic}      ref_order_id
    ${param_paging}                         [Common] - Create boolean param in dic    ${arg_dic}      paging
    ${param_page_index}                     [Common] - Create int param in dic    ${arg_dic}      page_index


    ${data}              catenate       SEPARATOR=
        ...     {
        ...         ${param_user_id}
        ...         ${param_user_type_id}
        ...     	${param_order_id}
        ...     	${param_ext_transaction_id}
        ...     	${param_status_id_list}
        ...     	${param_state}
        ...     	${param_to_last_updated_timestamp}
        ...         ${param_from_last_updated_timestamp}
        ...         ${param_created_client_id}
        ...         ${param_executed_client_id}
        ...         ${param_created_channel_type}
        ...         ${param_created_device_unique_reference}
        ...         ${param_error_codes}
        ...         ${param_service_name}
        ...         ${param_service_group_name}
        ...         ${param_payer_user_id}
        ...         ${param_payer_user_type_id}
        ...         ${param_payee_user_id}
        ...         ${param_payee_user_type_id}
        ...         ${param_created_channel_type}
        ...         ${param_service_id_list}
        ...         ${param_user_type_id}
        ...         ${param_file_type}
        ...         ${param_from}
        ...         ${param_to}
        ...         ${param_product_ref}
        ...         ${param_product_name}
        ...         ${param_ref_order_id}
        ...         ${param_paging}
        ...         ${param_page_index}
        ...     }
    ${data}  replace string      ${data}      ,}    }
                create session          api        ${api_gateway_host}

    ${response}             post request
        ...     api
        ...     ${search_payment_orders_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search user profile
    [Arguments]     ${arg_dic}

    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${param_mobile_number}                  [Common] - Create string param in dic     ${arg_dic}      mobile_number
    ${param_paging}                         [Common] - Create boolean param in dic    ${arg_dic}      paging
    ${param_page_index}                     [Common] - Create int param in dic    ${arg_dic}      page_index
    ${data}              catenate       SEPARATOR=
        ...     {
        ...     	${param_mobile_number}
        ...         ${param_paging}
        ...         ${param_page_index}
        ...     }
    ${data}  replace string      ${data}      ,}    }
                create session          api        ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${search_user_profile_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search agent type
    [Arguments]     ${arg_dic}

    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${data}              catenate       SEPARATOR=
        ...     {}
                create session          api        ${host}
    ${response}             post request
        ...     api
        ...     ${get_all_agent_type_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API Get transaction history of company
    [Arguments]     ${arg_dic}
    ${service_id_list}   set variable   ${empty}
    ${status}   run keyword and return status       Dictionary Should Contain Key     ${arg_dic}          service_id_list
    ${service_id_list}     run keyword if      '${status}'=='True'   get from dictionary     ${arg_dic}      service_id_list
    ...   ELSE    set variable    ${empty}

    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}



    ${param_order_id}		[Common] - Create string param in dic    ${arg_dic}		order_id
    ${param_from_updated_date}		[Common] - Create string param in dic    ${arg_dic}		from_last_updated_timestamp
    ${param_to_updated_date}		[Common] - Create string param in dic    ${arg_dic}		to_last_updated_timestamp
    ${status}   run keyword and return status       Dictionary Should Contain Key     ${arg_dic}          from_date
    ${param_from_created_date}     run keyword if      '${status}'=='True'   set variable      "from":"${arg_dic.from_date}",
    ...   ELSE    set variable    ${empty}
    #${param_from_created_date}      [Common] - Create string param in dic      ${arg_dic}      from
    ${param_to_created_date}		[Common] - Create string param in dic    ${arg_dic}		to
    ${param_payee_id}		[Common] - Create int param in dic    ${arg_dic}		payee_user_id
    ${param_payee_type}       [Common] - Create int param in dic     ${arg_dic}      payee_user_type_id
    ${param_payer_id}     [Common] - Create int param in dic     ${arg_dic}      payer_user_id
    ${param_payer_type}       [Common] - Create int param in dic     ${arg_dic}      payer_user_type_id
    ${param_initiator_id}     [Common] - Create int param in dic     ${arg_dic}      initiator_user_id
    ${param_initiator_type}       [Common] - Create int param in dic  ${arg_dic}     initiator_user_type_id
    ${param_order_status}     [Common] - Create int param in dic     ${arg_dic}      status
    ${param_creation_client_id}       [Common] - Create string param in dic     ${arg_dic}      created_client_id
    ${param_creation_channel_type}      [Common] - Create string param in dic       ${arg_dic}             created_channel_type
    ${param_creation_unique_device_ref}      [Common] - Create string param in dic      ${arg_dic}         created_device_unique_reference
    ${param_ext_transaction_id}       [Common] - Create string param in dic     ${arg_dic}       ext_transaction_id
    ${param_ref_order_id}      [Common] - Create string param in dic         ${arg_dic}     ref_order_id
    ${param_short_id}     [Common] - Create string param in dic   ${arg_dic}      short_order_id
    ${param_service_id_list}   [Common] - Create Json data for list param without quote    service_id_list        ${service_id_list}
    ${param_service_group_id}     [Common] - Create int param in dic     ${arg_dic}      service_group_id
    ${param_product_name}     [Common] - Create string param in dic     ${arg_dic}      product_name

    ${data}              catenate       SEPARATOR=
        ...     {
        ...         ${param_order_id}
        ...         ${param_from_created_date}
        ...         ${param_to_created_date}
        ...         ${param_from_updated_date}
        ...         ${param_to_updated_date}
        ...         ${param_payee_id}
        ...         ${param_payee_type}
        ...         ${param_payer_id}
        ...         ${param_payer_type}
        ...         ${param_initiator_id}
        ...         ${param_initiator_type}
        ...         ${param_order_status}
        ...         ${param_creation_client_id}
        ...         ${param_creation_channel_type}
        ...         ${param_creation_unique_device_ref}
        ...         ${param_ext_transaction_id}
        ...         ${param_ref_order_id}
        ...         ${param_short_id}
        ...         ${param_service_id_list}
        ...         ${param_service_group_id}
        ...         ${param_product_name}
        ...     }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                create session          api        ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${search_transactions_of_company_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${data}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API Search Company Transaction History By Connected Agent
    [Arguments]     ${arg_dic}

    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_order_id}		        [Common] - Create string param in dic      ${arg_dic}		order_id
    ${param_from_created_date}		[Common] - Create string param in dic      ${arg_dic}		from
    ${param_to_created_date}		[Common] - Create string param in dic      ${arg_dic}		to
    ${param_from_updated_date}		[Common] - Create string param in dic      ${arg_dic}		from_last_updated_timestamp
    ${param_to_updated_date}		[Common] - Create string param in dic      ${arg_dic}		to_last_updated_timestamp
    ${param_payee_id}		        [Common] - Create int param in dic         ${arg_dic}		payee_user_id
    ${param_payee_type}             [Common] - Create int param in dic         ${arg_dic}      payee_user_type_id
    ${param_payer_id}               [Common] - Create int param in dic         ${arg_dic}      payer_user_id
    ${param_payer_type}             [Common] - Create int param in dic         ${arg_dic}      payer_user_type_id
    ${param_initiator_id}           [Common] - Create int param in dic         ${arg_dic}      initiator_user_id
    ${param_initiator_type}         [Common] - Create int param in dic         ${arg_dic}      initiator_user_type
    ${param_creation_client_id}                 [Common] - Create string param in dic      ${arg_dic}          created_client_id
    ${param_creation_channel_type}              [Common] - Create int param in dic         ${arg_dic}          created_channel_type
    ${param_creation_unique_device_ref}         [Common] - Create int param in dic         ${arg_dic}          created_device_unique_reference
    ${param_ext_transaction_id}                 [Common] - Create int param in dic         ${arg_dic}          ext_transaction_id
    ${param_ref_order_id}                       [Common] - Create string param in dic      ${arg_dic}          ref_order_id
    ${param_short_id}               [Common] - Create int param in dic         ${arg_dic}      short_order_id
    ${param_service_group_id}       [Common] - Create int param in dic         ${arg_dic}      service_group_id
    ${param_product_name}           [Common] - Create string param in dic      ${arg_dic}      product_name
    ${param_status}                 [Common] - Create int param in dic      ${arg_dic}      status

    ${data}              catenate       SEPARATOR=
        ...     {
        ...         "service_id_list":[${suite_service_id}],
        ...         ${param_order_id}
        ...         ${param_from_created_date}
        ...         ${param_to_created_date}
        ...         ${param_from_updated_date}
        ...         ${param_to_updated_date}
        ...         ${param_payee_id}
        ...         ${param_payee_type}
        ...         ${param_payer_id}
        ...         ${param_payer_type}
        ...         ${param_initiator_id}
        ...         ${param_initiator_type}
        ...         ${param_status}
        ...         ${param_creation_client_id}
        ...         ${param_creation_channel_type}
        ...         ${param_creation_unique_device_ref}
        ...         ${param_ext_transaction_id}
        ...         ${param_ref_order_id}
        ...         ${param_short_id}
        ...         ${param_service_group_id}
        ...         ${param_product_name}
        ...     }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                create session          api        ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${search_company_transaction_agents_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${data}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API Search Company Transaction History By Specific Company
    [Arguments]     ${arg_dic}

    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_order_id}		        [Common] - Create string param in dic      ${arg_dic}		order_id
    ${param_from_created_date}		[Common] - Create string param in dic      ${arg_dic}		from
    ${param_to_created_date}		[Common] - Create string param in dic      ${arg_dic}		to
    ${param_from_updated_date}		[Common] - Create string param in dic      ${arg_dic}		from_last_updated_timestamp
    ${param_to_updated_date}		[Common] - Create string param in dic      ${arg_dic}		to_last_updated_timestamp
    ${param_payee_id}		        [Common] - Create int param in dic         ${arg_dic}		payee_user_id
    ${param_payee_type}             [Common] - Create int param in dic         ${arg_dic}      payee_user_type_id
    ${param_payer_id}               [Common] - Create int param in dic         ${arg_dic}      payer_user_id
    ${param_payer_type}             [Common] - Create int param in dic         ${arg_dic}      payer_user_type_id
    ${param_initiator_id}           [Common] - Create int param in dic         ${arg_dic}      initiator_user_id
    ${param_initiator_type}         [Common] - Create int param in dic         ${arg_dic}      initiator_user_type
    ${param_company_agent_id}       [Common] - Create int param in dic         ${arg_dic}      company_agent_id
    ${param_company_name}           [Common] - Create string param in dic      ${arg_dic}      company_name

    ${data}              catenate       SEPARATOR=
        ...     {
        ...         ${param_company_agent_id}
        ...         ${param_company_name}
        ...         ${param_order_id}
        ...         ${param_from_created_date}
        ...         ${param_to_created_date}
        ...         ${param_from_updated_date}
        ...         ${param_to_updated_date}
        ...         ${param_payee_id}
        ...         ${param_payee_type}
        ...         ${param_payer_id}
        ...         ${param_payer_type}
        ...         ${param_initiator_id}
        ...         ${param_initiator_type}
        ...     }
    ${data}  replace string      ${data}      ,}    }
    ${data}  replace string      ${data}      },,    },
                create session          api        ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${search_company_transaction_specific_company_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${response.text}
    log         ${data}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search sales permissions
    [Arguments]     ${arg_dic}

    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${data}              catenate       SEPARATOR=
        ...     {}
                create session          api        ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${search_sales_permissions_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

API search trust token
    [Arguments]     &{arg_dic}
    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}

    ${data}              catenate       SEPARATOR=
        ...     {}
                create session          api        ${api_gateway_host}
    ${response}             post request
        ...     api
        ...     ${report_search_trust_token_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}


API search settlement configurations
    [Arguments]         &{arg_dic}
    ${header}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_id}                             [Common] - Create int param in dic          ${arg_dic}      id
    ${param_is_deleted}                     [Common] - Create boolean param in dic      ${arg_dic}      is_deleted
    ${param_paging}                         [Common] - Create boolean param in dic      ${arg_dic}      paging
    ${param_page_index}                     [Common] - Create int param in dic          ${arg_dic}      page_index
    ${data}              catenate           SEPARATOR=
        ...	{
        ...              ${param_id}
        ...              ${param_is_deleted}
        ...              ${param_paging}
        ...              ${param_page_index}
        ...	}
    ${data}  replace string      ${data}      ,}    }
                            create session          api     ${api_gateway_host}     disable_warnings=1
    ${response}             post request
        ...     api
        ...     ${report_search_settlement_configuration_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}

    log         ${response.text}
    ${return_dic} =	Create Dictionary   response=${response}
    [Return]    ${return_dic}

[Report] Get summary transaction own agent
    [Arguments]     &{arg_dic}
    [Common][Prepare] - Authorization headers
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     access_token=${arg_dic.access_token}
    ${agent_id}      Convert To String       ${arg_dic.agent_id}
    ${path}     replace string    ${summary_transaction_own_agent_path}    {agent_id}   ${agent_id}
    ${body}     catenate
        ...    {
        ...     "start_timestamp":"${arg_dic.start_timestamp}",
        ...    	"end_timestamp":"${arg_dic.end_timestamp}"
        ...    }
    REST.post   ${api_gateway_host}/${path}
        ...     body=${body}
        ...     headers=${headers}
    rest extract

[Report] Search payment orders
    [Arguments]     &{arg_dic}
    [Common][Prepare] - Authorization headers
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     access_token=${arg_dic.access_token}
    ${param_user_id}                        [Common] - Create int param in dic    ${arg_dic}      user_id
    ${param_user_type_id}                   [Common] - Create int param in dic    ${arg_dic}      user_type_id
    ${param_order_id}                       [Common] - Create string param in dic     ${arg_dic}      order_id
    ${param_ext_transaction_id}             [Common] - Create string param in dic     ${arg_dic}      ext_transaction_id
    ${param_status_id_list}                 [Common] - Create Json data for list param without quote in dictionary     ${arg_dic}      status_id_list
    ${param_state}                          [Common] - Create string param in dic     ${arg_dic}      state
    ${param_to_last_updated_timestamp}      [Common] - Create string param in dic     ${arg_dic}      to_last_updated_timestamp
    ${param_from_last_updated_timestamp}    [Common] - Create string param in dic     ${arg_dic}      from_last_updated_timestamp
    ${param_created_client_id}              [Common] - Create string param in dic     ${arg_dic}      created_client_id
    ${param_executed_client_id}             [Common] - Create string param in dic     ${arg_dic}      executed_client_id
    ${param_created_channel_type}           [Common] - Create string param in dic     ${arg_dic}      created_channel_type
    ${param_created_device_unique_reference}           [Common] - Create string param in dic     ${arg_dic}      created_device_unique_reference
    ${param_error_codes}                    [Common] - Create Json data for list param with quote in dictionary       ${arg_dic}      error_codes
    ${param_service_name}                   [Common] - Create string param in dic     ${arg_dic}      service_name
    ${param_service_group_name}             [Common] - Create string param in dic     ${arg_dic}      service_group_name
    ${param_payer_user_id}                  [Common] - Create int param in dic    ${arg_dic}      payer_user_id
    ${param_payer_user_type_id}             [Common] - Create int param in dic    ${arg_dic}      payer_user_type_id
    ${param_payee_user_id}                  [Common] - Create int param in dic    ${arg_dic}      payee_user_id
    ${param_payee_user_type_id}             [Common] - Create int param in dic    ${arg_dic}      payee_user_type_id
    ${param_created_channel_type}           [Common] - Create string param in dic     ${arg_dic}      created_channel_type
    ${param_service_id_list}                [Common] - Create Json data for list param without quote in dictionary     ${arg_dic}      service_id_list
    ${param_user_type_id}                   [Common] - Create int param in dic    ${arg_dic}      user_type_id
    ${param_file_type}                      [Common] - Create string param in dic     ${arg_dic}      file_type
    ${param_row_number}                     [Common] - Create int param in dic    ${arg_dic}      row_number
    ${param_from}                           [Common] - Create string param in dic     ${arg_dic}      from
    ${param_to}                             [Common] - Create string param in dic     ${arg_dic}      to
    ${param_product_ref}                    [Common] - Create string param in dic     ${arg_dic}      product_ref
    ${param_product_name}                   [Common] - Create string param in dic     ${arg_dic}      product_name
    ${param_ref_order_id}                   [Common] - Create string param in dic     ${arg_dic}      ref_order_id
    ${param_paging}                         [Common] - Create boolean param in dic    ${arg_dic}      paging
    ${param_page_index}                     [Common] - Create int param in dic    ${arg_dic}      page_index
    ${body}     catenate        SEPARATOR=
        ...     {
        ...         ${param_user_id}
        ...         ${param_user_type_id}
        ...     	${param_order_id}
        ...     	${param_ext_transaction_id}
        ...     	${param_status_id_list}
        ...     	${param_state}
        ...     	${param_to_last_updated_timestamp}
        ...         ${param_from_last_updated_timestamp}
        ...         ${param_created_client_id}
        ...         ${param_executed_client_id}
        ...         ${param_created_channel_type}
        ...         ${param_created_device_unique_reference}
        ...         ${param_error_codes}
        ...         ${param_service_name}
        ...         ${param_service_group_name}
        ...         ${param_payer_user_id}
        ...         ${param_payer_user_type_id}
        ...         ${param_payee_user_id}
        ...         ${param_payee_user_type_id}
        ...         ${param_created_channel_type}
        ...         ${param_service_id_list}
        ...         ${param_user_type_id}
        ...         ${param_file_type}
        ...         ${param_from}
        ...         ${param_to}
        ...         ${param_product_ref}
        ...         ${param_product_name}
        ...         ${param_ref_order_id}
        ...         ${param_paging}
        ...         ${param_page_index}
        ...     }
    ${body}  replace string      ${body}      ,}    }
    REST.post   ${api_gateway_host}/${search_payment_orders_path}
        ...     body=${body}
        ...     headers=${headers}
    rest extract

[Report] Search settlement resolving histories
    [Arguments]     &{arg_dic}
    [Common][Prepare] - Authorization headers
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     access_token=${arg_dic.access_token}
    ${param_id}                        [Common] - Create int param in dic    ${arg_dic}      id
    ${param_paging}                         [Common] - Create boolean param in dic    ${arg_dic}      paging
    ${param_page_index}                     [Common] - Create int param in dic    ${arg_dic}      page_index
    ${body}     catenate        SEPARATOR=
        ...     {
        ...         ${param_id}
        ...         ${param_paging}
        ...         ${param_page_index}
        ...     }
    ${body}  replace string      ${body}      ,}    }
    REST.post   ${api_gateway_host}/${search_settlement_resolving_histories_path}
        ...     body=${body}
        ...     headers=${headers}
    rest extract
    
API search pending settlement
    [Arguments]     &{arg_dic}
#        ${header}   catenate
#        ...     {
#        ...         "client_id": "${arg_dic.client_id}",
#        ...         "client_secret": "${arg_dic.client_secret}",
#        ...         "content-type": "application/json",
#        ...         "Authorization": "Bearer ${arg_dic.access_token}"
#        ...     }
    ${header}               create dictionary       SEPARATOR=
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_description}		            [Common] - Create string param in dic      ${arg_dic}		description
    ${param_created_by}		                [Common] - Create string param in dic      ${arg_dic}		created_by
    ${param_from_due_date}		            [Common] - Create string param in dic      ${arg_dic}		from_due_date
    ${param_to_due_date}		            [Common] - Create string param in dic      ${arg_dic}		to_due_date
    ${param_payer_name}		                [Common] - Create string param in dic      ${arg_dic}		payer_name
    ${param_payee_name}		                [Common] - Create string param in dic      ${arg_dic}		payee_name
    ${param_internal_transaction_id}		[Common] - Create string param in dic      ${arg_dic}		internal_transaction_id
    ${param_external_transaction_id}		[Common] - Create string param in dic      ${arg_dic}		external_transaction_id
    ${param_from_created_timestamp}		    [Common] - Create string param in dic      ${arg_dic}		from_created_timestamp
    ${param_to_created_timestamp}		    [Common] - Create string param in dic      ${arg_dic}		to_created_timestamp

    ${param_settlement_id}		            [Common] - Create int param in dic         ${arg_dic}		settlement_id
    ${param_payer_id}		                [Common] - Create int param in dic         ${arg_dic}		payer_id
    ${param_payee_id}		                [Common] - Create int param in dic         ${arg_dic}		payee_id
    ${param_reconciliation_id}              [Common] - Create int param in dic         ${arg_dic}       reconciliation_id
    ${param_status}		                    [Common] - Create int param in dic         ${arg_dic}		status

    ${param_is_over_due}                    [Common] - Create boolean param in dic     ${arg_dic}       is_over_due

    ${param_paging}                         [Common] - Create boolean param in dic     ${arg_dic}       paging
    ${param_page_index}                     [Common] - Create int param in dic         ${arg_dic}       page_index

    ${data}              catenate       SEPARATOR=
        ...     {
        ...         ${param_settlement_id}
        ...         ${param_description}
        ...         ${param_created_by}
        ...         ${param_from_due_date}
        ...         ${param_to_due_date}
        ...         ${param_status}
        ...         ${param_payer_id}
        ...         ${param_payer_name}
        ...         ${param_payee_id}
        ...         ${param_payee_name}
        ...         ${param_reconciliation_id}
        ...         ${param_internal_transaction_id}
        ...         ${param_external_transaction_id}
        ...         ${param_from_created_timestamp}
        ...         ${param_to_created_timestamp}
        ...         ${param_is_over_due}
        ...         ${param_paging}
        ...         ${param_page_index}
        ...     }

    ${data}     replace string          ${data}     ,}        }
                create session          api        ${api_gateway_host}
#    REST.post   ${api_gateway_host}/${search_pending_settlement_path}
#        ...     body=${data}
#        ...     headers=${header}
#    rest extract
    ${response}             post request
        ...     api
        ...     ${search_pending_settlement_path}
        ...     ${data}
        ...     ${NONE}
        ...     ${header}
    log         ${data}
    log         ${response.text}
    ${dic} =	Create Dictionary   response=${response}
    [Return]    ${dic}

[Report] Search sof service balance limitation
    [Arguments]     &{arg_dic}
    ${headers}               create dictionary       SEPARATOR=
        ...     client_id=${setup_admin_client_id}
        ...     client_secret=${setup_admin_client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    ${param_service_id}                         [Common] - Create int param in dic    ${arg_dic}      service_id
    ${param_paging}                         [Common] - Create boolean param in dic    ${arg_dic}      paging
    ${param_page_index}                     [Common] - Create int param in dic    ${arg_dic}      page_index
    ${body}     catenate        SEPARATOR=
        ...     {
        ...         ${param_service_id}
        ...         ${param_paging}
        ...         ${param_page_index}
        ...     }
    ${body}  replace string      ${body}      ,}    }
    REST.post   ${api_gateway_host}/${search_sof_service_balance_limitation_path}
        ...     body=${body}
        ...     headers=${headers}
    rest extract