*** Settings ***
Resource          ../3_reuse_function/create_agent.robot
Resource          ../3_reuse_function/create_and_execute_normal_order.robot
Resource          ../3_reuse_function/create_service_structure.robot
Resource          ../3_reuse_function/search.robot
Resource          ../3_reuse_function/create_customer.robot
Resource          ../3_reuse_function/create_and_execute_artifact_order.robot
Resource          ../3_reuse_function/mock.robot
Resource          ../3_reuse_function/create_and_execute_adjustment_order.robot
Resource          ../3_reuse_function/create_and_execute_trust_order.robot
Resource          ../3_reuse_function/inventory.robot
Resource          ../3_reuse_function/system_user.robot