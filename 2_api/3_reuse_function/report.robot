*** Settings ***
Resource        ../2_use_case-deprecated/imports.robot

*** Keywords ***
[Report][Reuse][200] - search card type
    ${response}                         call system user login api for Operation Portal
            ${get_admin_access_token}=          set variable                        ${response.json()['access_token']}
            set suite variable      ${suite_admin_access_token}       ${get_admin_access_token}

            ${arg_dic}     create dictionary
            ...      client_id=${setup_admin_client_id}
            ...      client_secret=${setup_admin_client_secret}
            ...      access_token=${suite_admin_access_token}
            ${response}    API search card type       ${arg_dic}
            [Return]        ${response}
