*** Settings ***
Resource      ../2_use_case-deprecated/imports.robot

*** Keywords ***
[Report][Reuse][200] - Search service IDs belongs to service groups contains name
    [Arguments]         ${arg_dic}
    ${search_service_group_dic}                 create dictionary       service_group_name=${arg_dic.service_group_name}            access_token=${arg_dic.access_token}
    ${return_search_service_group_dic}                 [200] API search service group      ${search_service_group_dic}
    @{final_service_id_list}=    Create List
#    @{service_group_list}       convert to list         @{return_search_service_group_dic.response.json()['data']['service_groups']}
    :FOR   ${service_group}    IN    @{return_search_service_group_dic.response.json()['data']['service_groups']}
#    \   @{service_group}=       convert to list         ${service_group_list}
    \   log dictionary       ${service_group}
    \   log dictionary      ${search_service_group_dic}
    \   ${service_group_id}     get from dictionary     ${service_group}        service_group_id
    \   ${arg_search_service_dic}      Create dictionary       service_group_id=${service_group_id}     access_token=${arg_dic.access_token}
    \   ${return_search_service_dic}            [200] API search services      ${arg_search_service_dic}
    \   log     ${return_search_service_dic.response.json()['data']['services']}
    \   ${service_id_list}     get all values of specific key      ${return_search_service_dic.response.json()['data']['services']}        service_id
    \   @{final_service_id_list}=      combine lists          ${final_service_id_list}          ${service_id_list}

    [Return]      @{final_service_id_list}


