*** Settings ***
Resource      ../2_use_case-deprecated/imports.robot


*** Keywords ***
[Reuse][200] create mock otp otp-management
#    ${data}             set variable        {"protocol":"http","port":${otp_management_otp_port},
#    ...     "name":"otp-management otp mock","stubs":[{"responses":[{"inject":"function (request, state, logger)
#    ...     { \n var data = JSON.parse(request.body)['data']; \n var refId = data['otp_reference_id']; \n var user_reference_code
#    ...     = data['user_reference_code']; \n if (typeof state.requests === 'undefined') { \n state.requests = {}; \n } \n state.requests[refId]
#    ...     = { headers: { 'Content-Type': 'application/json' }, body: JSON.stringify({ 'data': data}) };\n if (typeof state.requests_user_ref
#    ...     === 'undefined') { \n state.requests_user_ref = {}; \n }  \n state.requests_user_ref[user_reference_code] =
#    ...     { headers: { 'Content-Type': 'application/json' }, body: JSON.stringify({ 'data': data}) }; \n return { headers: { 'Content-Type': 'application/json' },
#    ...     body: JSON.stringify({ 'status': { 'code':'success', 'message':'Success' }, 'data': data }) }; \n}"}],"predicates":[{"equals":{"method":"POST","path":"/otp"}}]},{"responses":[{"inject":
#    ...     "function (request, state, logger) { \n var refId = request.path.substr(request.path.lastIndexOf('/') + 1); \n if (typeof state.requests === 'undefined') { \n state.requests = {}; \n } \n if (state.requests[refId]) { \n logger.info('found response for ' + refId + '%s');
#    ...     \n return state.requests[refId]; \n } \n return { headers: { 'Content-Type': 'application/json' }, \n  body: JSON.stringify({ 'data' : null }) }; \n }"}],"predicates":[{"equals":{"method":"GET"}},
#    ...     {"startsWith":{"path":"/otp"}}]},{"responses":[{"inject":"function (request, state, logger) { \n var user_reference_code = request.path.substr(request.path.lastIndexOf('/') + 1); \n if (typeof state.requests_user_ref === 'undefined')
#    ...     { \n state.requests_user_ref = {}; \n } \n if (state.requests_user_ref[user_reference_code]) { \n logger.info('found response for ' + user_reference_code + '%s'); \n return state.requests_user_ref[user_reference_code]; \n } \n return { headers: { 'Content-Type': 'application/json' },
#    ...     \n  body: JSON.stringify({ 'data' : null }) }; \n }"}],"predicates":[{"equals":{"method":"GET"}},{"startsWith":{"path":"/usr_ref_code/otp"}}]}]}





#*** Test Cases ***
#TC_EQP_xxxxxxss
#    [Reuse][200] create mock otp otp-management
