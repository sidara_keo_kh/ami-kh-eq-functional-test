*** Settings ***
Resource      ../2_use_case-deprecated/imports.robot


*** Keywords ***
[Workflow][Reuse][200] - create and execute adjustment order
    [Arguments]    ${arg_dic}
    ${created_order_dic}     [200] API create adjustment order      ${arg_dic}

    ${execute_order_dic}      create dictionary       access_token=${arg_dic.access_token}        reference_id=${created_order_dic.reference_id}
    [200] API execute adjustment order     ${execute_order_dic}
    ${return_dic}       create dictionary   order_id=${created_order_dic.order_id}      reference_id=${created_order_dic.reference_id}
    [Return]    ${return_dic}