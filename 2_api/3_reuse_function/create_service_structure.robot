*** Settings ***
Resource      ../2_use_case-deprecated/imports.robot

*** Keywords ***
[Payment][Reuse][200] - create service structure
    [Arguments]     ${arg_dic}
    ${created_service_group_dic}    [200] API add service group     ${arg_dic.access_token}     ${arg_dic.service_group_name}
    ${created_service_dic}    [200] API create service    ${arg_dic.access_token}     ${created_service_group_dic.service_group_id}     ${arg_dic.service_name}     ${arg_dic.currency}
    [200] API create service command        ${arg_dic.access_token}     ${created_service_dic.service_id}       ${arg_dic.command_id}
    ${get_service_command_dic}      [200] API get service commands by service id    ${arg_dic.access_token}     ${created_service_dic.service_id}

    [200] API create fee tier
    ...     ${arg_dic.access_token}
    ...     ${get_service_command_dic.payment_service_command_id}
    ...     ${arg_dic.fee_tier_condition}
    ...     ${arg_dic.condition_amount}
    ...     ${arg_dic.fee_type}
    ...     ${arg_dic.fee_amount}
    ...     ${arg_dic.bonus_type}
    ...     ${arg_dic.bonus_amount}
    ...     ${arg_dic.amount_type}
    ...     ${arg_dic.settlement_type}

    ${get_fee_tier_dic}      [200] API get list fee tiers by service command id      ${arg_dic.access_token}     ${get_service_command_dic.payment_service_command_id}

    ${status}   run keyword and return status    Dictionary Should Contain Key     ${arg_dic}    balance_distributions
    run keyword if      '${status}'=='True'
    ...       [200] API update all balance distribution   ${get_fee_tier_dic.fee_tier_id}      ${arg_dic.access_token}     ${setup_admin_client_id}       ${setup_admin_client_secret}    ${arg_dic.balance_distributions}

    ${status}   run keyword and return status      Dictionary Should Contain Key     ${arg_dic}     pre_payment_spi_url_type
    run keyword if      '${status}'=='True'    run keyword     [Payment] - create SPI URL for "pre-payment"    ${get_service_command_dic.payment_service_command_id}   ${arg_dic}

    ${status}   run keyword and return status      Dictionary Should Contain Key     ${arg_dic}     post_payment_spi_url_type
    run keyword if      '${status}'=='True'    run keyword      [Payment] - create SPI URL for "post-payment"       ${get_service_command_dic.payment_service_command_id}   ${arg_dic}

    ${return_dic}   create dictionary   service_command_id=${get_service_command_dic.payment_service_command_id}        service_id=${created_service_dic.service_id}
    ...     service_name=${arg_dic.service_name}    service_group_id=${created_service_group_dic.service_group_id}      service_group_name=${arg_dic.service_group_name}    fee_tier_id=${get_fee_tier_dic.fee_tier_id}
    [Return]    ${return_dic}

[Payment][Reuse] - create service under service group id
    [Arguments]     ${service_group_id}     ${arg_dic}
    ${created_service_dic}    [200] API create service  ${arg_dic.access_token}     ${service_group_id}     ${arg_dic.service_name}     ${arg_dic.currency}
    [200] API create service command    ${arg_dic.access_token}     ${created_service_dic.service_id}   ${arg_dic.command_id}
    ${get_service_command_dic}  [200] API get service commands by service id    ${arg_dic.access_token}     ${created_service_dic.service_id}

    [200] API create fee tier
    ...     ${arg_dic.access_token}
    ...     ${get_service_command_dic.payment_service_command_id}
    ...     ${arg_dic.fee_tier_condition}
    ...     ${arg_dic.condition_amount}
    ...     ${arg_dic.fee_type}
    ...     ${arg_dic.fee_amount}
    ...     ${arg_dic.bonus_type}
    ...     ${arg_dic.bonus_amount}
    ...     ${arg_dic.amount_type}
    ...     ${arg_dic.settlement_type}

    ${get_fee_tier_dic}      [200] API get list fee tiers by service command id      ${arg_dic.access_token}     ${get_service_command_dic.payment_service_command_id}

    ${status}   run keyword and return status    Dictionary Should Contain Key     ${arg_dic}    balance_distributions
    run keyword if      '${status}'=='True'
    ...       [200] API update all balance distribution   ${get_fee_tier_dic.fee_tier_id}      ${arg_dic.access_token}     ${setup_admin_client_id}       ${setup_admin_client_secret}    ${arg_dic.balance_distributions}

    ${status}   run keyword and return status      Dictionary Should Contain Key     ${arg_dic}     pre_payment_spi_url_type
    run keyword if      '${status}'=='True'    run keyword     [Payment] - create SPI URL for "pre-payment"    ${get_service_command_dic.payment_service_command_id}   ${arg_dic}

    ${status}   run keyword and return status      Dictionary Should Contain Key     ${arg_dic}     post_payment_spi_url_type
    run keyword if      '${status}'=='True'    run keyword      [Payment] - create SPI URL for "post-payment"       ${get_service_command_dic.payment_service_command_id}   ${arg_dic}

    ${return_dic}   create dictionary   service_command_id=${get_service_command_dic.payment_service_command_id}        service_id=${created_service_dic.service_id}
    ...     service_name=${arg_dic.service_name}    service_group_id=${service_group_id}      service_group_name=${arg_dic.service_group_name}
    [Return]    ${return_dic}

[Payment][Reuse][200] - create service structure with 2 fee tiers
    [Arguments]     ${arg_dic}
    ${created_service_group_dic}    [200] API add service group     ${arg_dic.access_token}     ${arg_dic.service_group_name}
    ${created_service_dic}    [200] API create service    ${arg_dic.access_token}     ${created_service_group_dic.service_group_id}     ${arg_dic.service_name}     ${arg_dic.currency}
    [200] API create service command        ${arg_dic.access_token}     ${created_service_dic.service_id}       ${arg_dic.command_id}
    ${get_service_command_dic}      [200] API get service commands by service id    ${arg_dic.access_token}     ${created_service_dic.service_id}

    [200] API create fee tier
    ...     ${arg_dic.access_token}
    ...     ${get_service_command_dic.payment_service_command_id}
    ...     ${arg_dic.fee_tier_condition_1}
    ...     ${arg_dic.condition_amount_1}
    ...     ${arg_dic.fee_type_1}
    ...     ${arg_dic.fee_amount_1}
    ...     ${arg_dic.bonus_type_1}
    ...     ${arg_dic.bonus_amount_1}
    ...     ${arg_dic.amount_type_1}
    ...     ${arg_dic.settlement_type_1}

        [200] API create fee tier
    ...     ${arg_dic.access_token}
    ...     ${get_service_command_dic.payment_service_command_id}
    ...     ${arg_dic.fee_tier_condition_2}
    ...     ${arg_dic.condition_amount_2}
    ...     ${arg_dic.fee_type_2}
    ...     ${arg_dic.fee_amount_2}
    ...     ${arg_dic.bonus_type_2}
    ...     ${arg_dic.bonus_amount_2}
    ...     ${arg_dic.amount_type_2}
    ...     ${arg_dic.settlement_type_2}

    ${get_fee_tier_dic}      [200] API get list fee tiers by service command id with 2 fee tier configuration for service command      ${arg_dic.access_token}     ${get_service_command_dic.payment_service_command_id}

    ${status}   run keyword and return status    Dictionary Should Contain Key     ${arg_dic}    balance_distributions
    run keyword if      '${status}'=='True'
    ...       [200] API update all balance distribution   ${get_fee_tier_dic.fee_tier_id_1}      ${arg_dic.access_token}     ${setup_admin_client_id}       ${setup_admin_client_secret}    ${arg_dic.balance_distributions}

    run keyword if      '${status}'=='True'
    ...       [200] API update all balance distribution   ${get_fee_tier_dic.fee_tier_id_2}      ${arg_dic.access_token}     ${setup_admin_client_id}       ${setup_admin_client_secret}    ${arg_dic.balance_distributions}


    ${status}   run keyword and return status      Dictionary Should Contain Key     ${arg_dic}     pre_payment_spi_url_type
    run keyword if      '${status}'=='True'    run keyword     [Payment] - create SPI URL for "pre-payment"    ${get_service_command_dic.payment_service_command_id}   ${arg_dic}

    ${status}   run keyword and return status      Dictionary Should Contain Key     ${arg_dic}     post_payment_spi_url_type
    run keyword if      '${status}'=='True'    run keyword      [Payment] - create SPI URL for "post-payment"       ${get_service_command_dic.payment_service_command_id}   ${arg_dic}

    ${return_dic}   create dictionary   service_command_id=${get_service_command_dic.payment_service_command_id}        service_id=${created_service_dic.service_id}
    ...     service_name=${arg_dic.service_name}    service_group_id=${created_service_group_dic.service_group_id}      service_group_name=${arg_dic.service_group_name}
    [Return]    ${return_dic}

[Payment] - create SPI URL for "pre-payment"
    [Arguments]     ${service_command_id}     ${arg_dic}
    ${dic}      create dictionary   service_command_id=${service_command_id}      access_token=${arg_dic.access_token}      spi_url_type=${arg_dic.pre_payment_spi_url_type}    url=${arg_dic.pre_payment_url}  spi_url_call_method=${arg_dic.pre_payment_spi_url_call_method}   expire_in_minute=${arg_dic.pre_payment_expire_in_minute}   max_retry=${arg_dic.pre_payment_max_retry}  retry_delay_millisecond=${arg_dic.pre_payment_retry_delay_millisecond}      read_timeout=${arg_dic.pre_payment_read_timeout}
    [200] API create SPI URL    ${dic}

[Payment] - create SPI URL for "post-payment"
    [Arguments]     ${service_command_id}     ${arg_dic}
    ${dic}  create dictionary  service_command_id=${service_command_id}      access_token=${arg_dic.access_token}   spi_url_type=${arg_dic.post_payment_spi_url_type}    url=${arg_dic.post_payment_url}  spi_url_call_method=${arg_dic.post_payment_spi_url_call_method}   expire_in_minute=${arg_dic.post_payment_expire_in_minute}   max_retry=${arg_dic.post_payment_max_retry}  retry_delay_millisecond=${arg_dic.post_payment_retry_delay_millisecond}      read_timeout=${arg_dic.post_payment_read_timeout}
    [200] API create SPI URL    ${dic}

[Payment][Reuse][200] - add "Cancel" command info for service
    [Arguments]     ${arg_dic}
    [200] API create service command        ${arg_dic.access_token}     ${arg_dic.service_id}       ${arg_dic.command_id}
    ${get_service_command_dic}      [200] API get service commands by service id    ${arg_dic.access_token}     ${arg_dic.service_id}

    [200] API create fee tier
    ...     ${arg_dic.access_token}
    ...     ${get_service_command_dic.cancel_service_command_id}
    ...     ${arg_dic.fee_tier_condition}
    ...     ${arg_dic.condition_amount}
    ...     ${arg_dic.fee_type}
    ...     ${arg_dic.fee_amount}
    ...     ${arg_dic.bonus_type}
    ...     ${arg_dic.bonus_amount}
    ...     ${arg_dic.amount_type}
    ...     ${arg_dic.settlement_type}

    ${get_fee_tier_dic}      [200] API get list fee tiers by service command id      ${arg_dic.access_token}     ${get_service_command_dic.cancel_service_command_id}

    ${status}   run keyword and return status    Dictionary Should Contain Key     ${arg_dic}    balance_distributions
    run keyword if      '${status}'=='True'
    ...       [200] API update all balance distribution   ${get_fee_tier_dic.fee_tier_id}      ${arg_dic.access_token}     ${setup_admin_client_id}       ${setup_admin_client_secret}    ${arg_dic.balance_distributions}

    ${status}   run keyword and return status      Dictionary Should Contain Key     ${arg_dic}     pre_payment_spi_url_type
    run keyword if      '${status}'=='True'    run keyword     [Payment] - create SPI URL for "pre-payment"    ${get_service_command_dic.cancel_service_command_id}   ${arg_dic}

    ${status}   run keyword and return status      Dictionary Should Contain Key     ${arg_dic}     post_payment_spi_url_type
    run keyword if      '${status}'=='True'    run keyword      [Payment] - create SPI URL for "post-payment"       ${get_service_command_dic.cancel_service_command_id}   ${arg_dic}

    ${return_dic}   create dictionary   service_command_id=${get_service_command_dic.cancel_service_command_id}        service_id=${arg_dic.service_id}     fee_tier_id=${get_fee_tier_dic.fee_tier_id}

    [Return]    ${return_dic}

[Payment][Reuse][200] - add "Payment" command info for service
    [Arguments]     ${arg_dic}
    [200] API create service command        ${arg_dic.access_token}     ${arg_dic.service_id}       ${arg_dic.command_id}
    ${get_service_command_dic}      [200] API get service commands by service id    ${arg_dic.access_token}     ${arg_dic.service_id}

    [200] API create fee tier
    ...     ${arg_dic.access_token}
    ...     ${get_service_command_dic.payment_service_command_id}
    ...     ${arg_dic.fee_tier_condition}
    ...     ${arg_dic.condition_amount}
    ...     ${arg_dic.fee_type}
    ...     ${arg_dic.fee_amount}
    ...     ${arg_dic.bonus_type}
    ...     ${arg_dic.bonus_amount}
    ...     ${arg_dic.amount_type}
    ...     ${arg_dic.settlement_type}

    ${get_fee_tier_dic}      [200] API get list fee tiers by service command id      ${arg_dic.access_token}     ${get_service_command_dic.payment_service_command_id}

    ${status}   run keyword and return status    Dictionary Should Contain Key     ${arg_dic}    balance_distributions
    run keyword if      '${status}'=='True'
    ...       [200] API update all balance distribution   ${get_fee_tier_dic.fee_tier_id}      ${arg_dic.access_token}     ${setup_admin_client_id}       ${setup_admin_client_secret}    ${arg_dic.balance_distributions}

    ${status}   run keyword and return status      Dictionary Should Contain Key     ${arg_dic}     pre_payment_spi_url_type
    run keyword if      '${status}'=='True'    run keyword     [Payment] - create SPI URL for "pre-payment"    ${get_service_command_dic.payment_service_command_id}   ${arg_dic}

    ${status}   run keyword and return status      Dictionary Should Contain Key     ${arg_dic}     post_payment_spi_url_type
    run keyword if      '${status}'=='True'    run keyword      [Payment] - create SPI URL for "post-payment"       ${get_service_command_dic.payment_service_command_id}   ${arg_dic}

    ${return_dic}   create dictionary   service_command_id=${get_service_command_dic.payment_service_command_id}        service_id=${arg_dic.service_id}

    [Return]    ${return_dic}

[Payment][Reuse][200] - create fee tier
    [Arguments]     &{arg_dic}
    [200] API create fee tier       ${arg_dic.access_token}
    ...     ${arg_dic.service_command_id}
    ...     ${arg_dic.fee_tier_condition}
    ...     ${arg_dic.condition_amount}
    ...     ${arg_dic.fee_type}
    ...     ${arg_dic.fee_amount}
    ...     ${arg_dic.bonus_type}
    ...     ${arg_dic.bonus_amount}
    ...     ${arg_dic.amount_type}
    ...     ${arg_dic.settlement_type}
    ${get_fee_tier_dic}      [200] API get list fee tiers by service command id      ${arg_dic.access_token}     ${arg_dic.service_command_id}
    [Return]    ${get_fee_tier_dic}