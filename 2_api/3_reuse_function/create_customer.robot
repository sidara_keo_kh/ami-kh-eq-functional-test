*** Settings ***
Resource      ../2_use_case-deprecated/imports.robot

*** Keywords ***
[Customer][Reuse][200] - create customer
    [Documentation]  if having more than 1 currency, pls input likes currency=VND,USD
    [Arguments]     ${arg_dic}
    ${return_dic}   create dictionary

    ${created_customer_dic}    [200] API register customer        ${arg_dic}
    ${customer_id}      set variable    ${created_customer_dic.id}

    ${authen_arg_dic}      create dictionary     username=${arg_dic.username}      password=${arg_dic.password_encrypted_utf8}     grant_type=password     param_client_id=${setup_admin_client_id}
    ${login_customer_dic}      [200] API customer authentication    ${authen_arg_dic}
    ${customer_access_token}      set variable    ${login_customer_dic.access_token}

    ${currency_list}     split string    ${arg_dic.currency}     ,
#    ${currency_list}    convert to list     ${arg_dic.currency}
    ${currency_list_length}   get length      ${currency_list}
    :FOR     ${index}   IN RANGE    0   ${currency_list_length}
    \   ${currency}     get from list     ${currency_list}      ${index}
    \   ${created_sof_cash_dic}     [200] API create user sof cash          ${currency}       ${customer_access_token}
    \   set to dictionary   ${return_dic}      sof_cash_id_in_${currency}    ${created_sof_cash_dic.sof_id}

     set to dictionary    ${return_dic}     customer_id     ${customer_id}
     set to dictionary    ${return_dic}     customer_type     customer
     set to dictionary    ${return_dic}     customer_access_token     ${customer_access_token}

    [Return]    ${return_dic}

[Customer][Reuse][200] - create customer with KYC level
    [Documentation]  create dictionary as normal but for currency we should make it as list
    ...     eg.   \@\{currency_list\}  create list    VND    USD     TBH
    [Arguments]     ${test_kyc_level}
    ${currency_list}    create list    VND
    ${username}                     generate random string      10          [LOWER]
    ${customer_unique_reference}    generate random string      15          [LOWER][NUMBERS]
    ${arg_dic}  create dictionary
    ...     unique_reference=${customer_unique_reference}
    ...     identity_type_id=1
    ...     username=${username}
    ...     kyc_level=${test_kyc_level}
    ...     password=${setup_password_customer_encrypted}
    ...     password_encrypted_utf8=${setup_password_customer_encrypted_utf8}
    ...     currency=${currency_list}

    ${return_dic}   create dictionary

    ${customer_id}    [200] API system user create customer with KYC level         ${arg_dic}

    ${authen_arg_dic}      create dictionary     username=${arg_dic.username}      password=${arg_dic.password_encrypted_utf8}     grant_type=password     param_client_id=${setup_admin_client_id}
    ${login_customer_dic}      [200] API customer authentication    ${authen_arg_dic}
    ${customer_access_token}      set variable    ${login_customer_dic.access_token}

    ${currency_list}    convert to list     ${arg_dic.currency}
    ${currency_list_length}   get length      ${currency_list}
    :FOR     ${index}   IN RANGE    0   ${currency_list_length}
    \   ${currency}     get from list     ${currency_list}      ${index}
    \   ${created_sof_cash_dic}     [200] API create user sof cash          ${currency}       ${customer_access_token}
    \   set to dictionary   ${return_dic}      sof_cash_id_in_${currency}    ${created_sof_cash_dic.sof_id}

     set to dictionary    ${return_dic}     customer_id     ${customer_id}
     set to dictionary    ${return_dic}     customer_type     customer
     set to dictionary    ${return_dic}     customer_access_token     ${customer_access_token}

    [Return]    ${return_dic}



