*** Settings ***
Resource      ../2_use_case-deprecated/imports.robot

*** Keywords ***
[Payment][Reuse][200] - create and execute normal order
    [Arguments]    ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}        header_device_unique_reference    ${param_not_used}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}        header_channel_id    ${param_not_used}
    ${created_order_dic}     [200] API payment create normal order
    ...     ${arg_dic.access_token}
    ...     ${arg_dic.ext_transaction_id}
    ...     ${arg_dic.product_service_id}
    ...     ${arg_dic.product_service_name}
    ...     ${arg_dic.product_name}
    ...     ${arg_dic.product_ref_1}
    ...     ${arg_dic.product_ref_2}
    ...     ${arg_dic.product_ref_3}
    ...     ${arg_dic.product_ref_4}
    ...     ${arg_dic.product_ref_5}
    ...     ${arg_dic.payer_user_ref_type}
    ...     ${arg_dic.payer_user_ref_value}
    ...     ${arg_dic.payer_user_id}
    ...     ${arg_dic.payer_user_type}
    ...     ${arg_dic.payee_user_ref_type}
    ...     ${arg_dic.payee_user_ref_value}
    ...     ${arg_dic.payee_user_id}
    ...     ${arg_dic.payee_user_type}
    ...     ${arg_dic.amount}
    ...     header_device_unique_reference=${arg_dic.header_device_unique_reference}
    ...     header_channel_id=${arg_dic.header_channel_id}

    ${execute_order_dic}      create dictionary       access_token=${arg_dic.access_token}        order_id=${created_order_dic.order_id}
    [200] API payment execute order     ${execute_order_dic}
    ${return_dic}       create dictionary   order_id=${created_order_dic.order_id}
    [Return]    ${return_dic}

[Payment][Reuse][Fail] - execute order
    [Arguments]    ${arg_dic}
    ${created_order_dic}     [200] API payment create normal order
    ...     ${arg_dic.access_token}
    ...     ${arg_dic.ext_transaction_id}
    ...     ${arg_dic.product_service_id}
    ...     ${arg_dic.product_service_name}
    ...     ${arg_dic.product_name}
    ...     ${arg_dic.product_ref_1}
    ...     ${arg_dic.product_ref_2}
    ...     ${arg_dic.product_ref_3}
    ...     ${arg_dic.product_ref_4}
    ...     ${arg_dic.product_ref_5}
    ...     ${arg_dic.payer_user_ref_type}
    ...     ${arg_dic.payer_user_ref_value}
    ...     ${arg_dic.payer_user_id}
    ...     ${arg_dic.payer_user_type}
    ...     ${arg_dic.payee_user_ref_type}
    ...     ${arg_dic.payee_user_ref_value}
    ...     ${arg_dic.payee_user_id}
    ...     ${arg_dic.payee_user_type}
    ...     ${arg_dic.amount}

    ${execute_order_dic}    create dictionary      access_token=${arg_dic.access_token}     order_id=${created_order_dic.order_id}
    ...     code=500
    [Fail] API payment execute order     ${execute_order_dic}
    ${return_dic}       create dictionary   order_id=${created_order_dic.order_id}
    [Return]    ${return_dic}

[Payment][Reuse][Fail] - create and execute normal order
    [Arguments]    ${arg_dic}
    ${created_order_dic}     [200] API payment create normal order
    ...     ${arg_dic.access_token}
    ...     ${arg_dic.ext_transaction_id}
    ...     ${arg_dic.product_service_id}
    ...     ${arg_dic.product_service_name}
    ...     ${arg_dic.product_name}
    ...     ${arg_dic.product_ref_1}
    ...     ${arg_dic.product_ref_2}
    ...     ${arg_dic.product_ref_3}
    ...     ${arg_dic.product_ref_4}
    ...     ${arg_dic.product_ref_5}
    ...     ${arg_dic.payer_user_ref_type}
    ...     ${arg_dic.payer_user_ref_value}
    ...     ${arg_dic.payer_user_id}
    ...     ${arg_dic.payer_user_type}
    ...     ${arg_dic.payee_user_ref_type}
    ...     ${arg_dic.payee_user_ref_value}
    ...     ${arg_dic.payee_user_id}
    ...     ${arg_dic.payee_user_type}
    ...     ${arg_dic.amount}

    ${execute_order_dic}      create dictionary       access_token=${arg_dic.access_token}        order_id=${created_order_dic.order_id}
    [200] API payment execute order     ${execute_order_dic}
    ${return_dic}       create dictionary   order_id=${created_order_dic.order_id}
    [Return]    ${return_dic}

[Payment][Reuse][200] - create normal order
    [Arguments]    ${arg_dic}
    ${created_order_dic}     [200] API payment create normal order
    ...     ${arg_dic.access_token}
    ...     ${arg_dic.ext_transaction_id}
    ...     ${arg_dic.product_service_id}
    ...     ${arg_dic.product_service_name}
    ...     ${arg_dic.product_name}
    ...     ${arg_dic.product_ref_1}
    ...     ${arg_dic.product_ref_2}
    ...     ${arg_dic.product_ref_3}
    ...     ${arg_dic.product_ref_4}
    ...     ${arg_dic.product_ref_5}
    ...     ${arg_dic.payer_user_ref_type}
    ...     ${arg_dic.payer_user_ref_value}
    ...     ${arg_dic.payer_user_id}
    ...     ${arg_dic.payer_user_type}
    ...     ${arg_dic.payee_user_ref_type}
    ...     ${arg_dic.payee_user_ref_value}
    ...     ${arg_dic.payee_user_id}
    ...     ${arg_dic.payee_user_type}
    ...     ${arg_dic.amount}

    ${execute_order_dic}      create dictionary       access_token=${arg_dic.access_token}        order_id=${created_order_dic.order_id}
    ${return_dic}       create dictionary   order_id=${created_order_dic.order_id}
    [Return]    ${return_dic}

[Payment][Reuse][Fail] - create normal order with service_id under blocked service group
    [Arguments]    ${arg_dic}
    ${created_order_dic}    [Fail] API payment create normal order with service under blocked service group
    ...     ${arg_dic.access_token}
    ...     ${arg_dic.ext_transaction_id}
    ...     ${arg_dic.product_service_id}
    ...     ${arg_dic.product_service_name}
    ...     ${arg_dic.product_name}
    ...     ${arg_dic.product_ref_1}
    ...     ${arg_dic.product_ref_2}
    ...     ${arg_dic.product_ref_3}
    ...     ${arg_dic.product_ref_4}
    ...     ${arg_dic.product_ref_5}
    ...     ${arg_dic.payer_user_ref_type}
    ...     ${arg_dic.payer_user_ref_value}
    ...     ${arg_dic.payer_user_id}
    ...     ${arg_dic.payer_user_type}
    ...     ${arg_dic.payee_user_ref_type}
    ...     ${arg_dic.payee_user_ref_value}
    ...     ${arg_dic.payee_user_id}
    ...     ${arg_dic.payee_user_type}
    ...     ${arg_dic.amount}

[Payment][Reuse][Fail] - create and execute normal order with phone number is null and valid voucher_id
    [Arguments]    ${arg_dic}
    ${created_order_dic}    [200] API payment create normal order
    ...     ${arg_dic.access_token}
    ...     ${arg_dic.ext_transaction_id}
    ...     ${arg_dic.product_service_id}
    ...     ${arg_dic.product_service_name}
    ...     ${arg_dic.product_name}
    ...     ${arg_dic.product_ref_1}
    ...     ${arg_dic.product_ref_2}
    ...     ${arg_dic.product_ref_3}
    ...     ${arg_dic.product_ref_4}
    ...     ${arg_dic.product_ref_5}
    ...     ${arg_dic.payer_user_ref_type}
    ...     ${arg_dic.payer_user_ref_value}
    ...     ${arg_dic.payer_user_id}
    ...     ${arg_dic.payer_user_type}
    ...     ${arg_dic.payee_user_ref_type}
    ...     ${arg_dic.payee_user_ref_value}
    ...     ${arg_dic.payee_user_id}
    ...     ${arg_dic.payee_user_type}
    ...     ${arg_dic.amount}

    ${execute_order_dic}      create dictionary       access_token=${arg_dic.access_token}        order_id=${created_order_dic.response.json()['data']['order_id']}
    [Fail] API payment execute order with phone number is null and valid voucher_id     ${execute_order_dic}

[Payment][Reuse][Fail] - create and execute normal order with valid phone number and invalid voucher_id
    [Arguments]    ${arg_dic}
    ${created_order_dic}    [200] API payment create normal order
    ...     ${arg_dic.access_token}
    ...     ${arg_dic.ext_transaction_id}
    ...     ${arg_dic.product_service_id}
    ...     ${arg_dic.product_service_name}
    ...     ${arg_dic.product_name}
    ...     ${arg_dic.product_ref_1}
    ...     ${arg_dic.product_ref_2}
    ...     ${arg_dic.product_ref_3}
    ...     ${arg_dic.product_ref_4}
    ...     ${arg_dic.product_ref_5}
    ...     ${arg_dic.payer_user_ref_type}
    ...     ${arg_dic.payer_user_ref_value}
    ...     ${arg_dic.payer_user_id}
    ...     ${arg_dic.payer_user_type}
    ...     ${arg_dic.payee_user_ref_type}
    ...     ${arg_dic.payee_user_ref_value}
    ...     ${arg_dic.payee_user_id}
    ...     ${arg_dic.payee_user_type}
    ...     ${arg_dic.amount}

    ${execute_order_dic}      create dictionary       access_token=${arg_dic.access_token}        order_id=${created_order_dic.response.json()['data']['order_id']}
    [Fail] API payment execute order with valid phone number and invalid voucher_id     ${execute_order_dic}

[Payment][Reuse][Fail] - create and execute normal order with both invalid phone number and voucher_id
    [Arguments]    ${arg_dic}
    ${created_order_dic}    [200] API payment create normal order
    ...     ${arg_dic.access_token}
    ...     ${arg_dic.ext_transaction_id}
    ...     ${arg_dic.product_service_id}
    ...     ${arg_dic.product_service_name}
    ...     ${arg_dic.product_name}
    ...     ${arg_dic.product_ref_1}
    ...     ${arg_dic.product_ref_2}
    ...     ${arg_dic.product_ref_3}
    ...     ${arg_dic.product_ref_4}
    ...     ${arg_dic.product_ref_5}
    ...     ${arg_dic.payer_user_ref_type}
    ...     ${arg_dic.payer_user_ref_value}
    ...     ${arg_dic.payer_user_id}
    ...     ${arg_dic.payer_user_type}
    ...     ${arg_dic.payee_user_ref_type}
    ...     ${arg_dic.payee_user_ref_value}
    ...     ${arg_dic.payee_user_id}
    ...     ${arg_dic.payee_user_type}
    ...     ${arg_dic.amount}

    ${execute_order_dic}      create dictionary       access_token=${arg_dic.access_token}        order_id=${created_order_dic.response.json()['data']['order_id']}
    [Fail] API payment execute order with both invalid phone number and voucher_id     ${execute_order_dic}

[Payment][Reuse][Fail] - create and execute normal order with invalid phone number and valid voucher_id
    [Arguments]    ${arg_dic}
    ${created_order_dic}    [200] API payment create normal order
    ...     ${arg_dic.access_token}
    ...     ${arg_dic.ext_transaction_id}
    ...     ${arg_dic.product_service_id}
    ...     ${arg_dic.product_service_name}
    ...     ${arg_dic.product_name}
    ...     ${arg_dic.product_ref_1}
    ...     ${arg_dic.product_ref_2}
    ...     ${arg_dic.product_ref_3}
    ...     ${arg_dic.product_ref_4}
    ...     ${arg_dic.product_ref_5}
    ...     ${arg_dic.payer_user_ref_type}
    ...     ${arg_dic.payer_user_ref_value}
    ...     ${arg_dic.payer_user_id}
    ...     ${arg_dic.payer_user_type}
    ...     ${arg_dic.payee_user_ref_type}
    ...     ${arg_dic.payee_user_ref_value}
    ...     ${arg_dic.payee_user_id}
    ...     ${arg_dic.payee_user_type}
    ...     ${arg_dic.amount}

    ${execute_order_dic}      create dictionary       access_token=${arg_dic.access_token}        order_id=${created_order_dic.response.json()['data']['order_id']}
    [Fail] API payment execute order with invalid phone number and valid voucher_id     ${execute_order_dic}

[Payment][Reuse][Fail] - create and execute normal order with valid phone number and blocked voucher_id
    [Arguments]    ${arg_dic}
    ${created_order_dic}    [200] API payment create normal order
    ...     ${arg_dic.access_token}
    ...     ${arg_dic.ext_transaction_id}
    ...     ${arg_dic.product_service_id}
    ...     ${arg_dic.product_service_name}
    ...     ${arg_dic.product_name}
    ...     ${arg_dic.product_ref_1}
    ...     ${arg_dic.product_ref_2}
    ...     ${arg_dic.product_ref_3}
    ...     ${arg_dic.product_ref_4}
    ...     ${arg_dic.product_ref_5}
    ...     ${arg_dic.payer_user_ref_type}
    ...     ${arg_dic.payer_user_ref_value}
    ...     ${arg_dic.payer_user_id}
    ...     ${arg_dic.payer_user_type}
    ...     ${arg_dic.payee_user_ref_type}
    ...     ${arg_dic.payee_user_ref_value}
    ...     ${arg_dic.payee_user_id}
    ...     ${arg_dic.payee_user_type}
    ...     ${arg_dic.amount}

    ${execute_order_dic}      create dictionary       access_token=${arg_dic.access_token}        order_id=${created_order_dic.response.json()['data']['order_id']}
    [Fail] API payment execute order with valid phone number and blocked voucher_id     ${execute_order_dic}

[Payment][Reuse][200] - Create label config
    [Arguments]    &{arg_dic}
    ${label_config}     catenate    SEPARATOR=
    ...    	"${arg_dic.label}":{
    ...    		"type_first": "${arg_dic.type_first}",
    ...    		"from_first": ${arg_dic.from_first},
    ...    		"amount_first": ${arg_dic.amount_first},
    ...    		"type_second": "${arg_dic.type_second}",
    ...    		"from_second": "${arg_dic.from_second}",
    ...    		"amount_second": ${arg_dic.amount_second},
    ...    		"operator": "${arg_dic.operator}"
    ...    	}
    [Return]    ${label_config}