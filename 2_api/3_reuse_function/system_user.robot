*** Settings ***
Resource      ../0_functional/imports.robot
Resource      ../../3_prepare_data/imports.robot

*** Keywords ***
[System_User][Reuse] - Verify permission - api link
    [Arguments]
        ...     ${permission_name}
        ...     ${keyword}
        ...     ${headers}=${SUITE_ADMIN_HEADERS}
        ...     &{args}

    [System_User][200][Prepare] - Create system user without role
        ...     headers=${headers}
        ...     output_user_id=test_system_user_id
        ...     output_user_name=test_system_user_name
        ...     output_password=test_system_user_password
    [Api_Gateway][200][Prepare] - System user authentication with plain password
        ...     username=${test_system_user_name}
        ...     password=${test_system_user_password}
        ...     output_token=test_access_token
    [Common][Prepare] - Access token headers
        ...     access_token=${test_access_token}
        ...     output=test_headers

    run keyword and ignore error
        ...     ${keyword}      headers=${test_headers}      &{args}
    [Common][Verify] - Http status code is "400"
    [Common][Verify] - Status code is "unauthorized_user"
    [Common][Verify] - Status message is "Permission denied"

    [System_User][200][Prepare] - Add permission by name to user
        ...     permission_name=${permission_name}
        ...     user_id=${test_system_user_id}
        ...     headers=${headers}
    [Api_Gateway][200][Prepare] - System user authentication with plain password
        ...     username=${test_system_user_name}
        ...     password=${test_system_user_password}
        ...     output_token=test_access_token
    [Common][Prepare] - Access token headers
        ...     access_token=${test_access_token}
        ...     output=test_headers

    run keyword and ignore error
        ...     ${keyword}      headers=${test_headers}      &{args}
    [Common][Extract] - field or empty if missing
        ...     field=$.status.code
        ...     output=code
    [Common][Extract] - field or empty if missing
        ...     field=$.status.message
        ...     output=message
    should not be equal as strings          ${code}         unauthorized_user
    should not be equal as strings          ${message}         Permission denied