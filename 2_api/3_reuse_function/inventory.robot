*** Settings ***
Resource      ../2_use_case-deprecated/imports.robot

*** Keywords ***
[Inventory][Reuse][200] - create single item
    [Arguments]    ${api_create_single_item_req}
    ${api_create_single_item_res}    [Inventory][200] - API create single item        ${api_create_single_item_req}
    ${api_get_single_item_req}    create dictionary
    ...    access_token=${api_create_single_item_req.access_token}
    ...    item_id=${api_create_single_item_res.id}
    ${api_get_single_item_res}    [Inventory][200] - API get single item    ${api_get_single_item_req}
    [Return]    ${api_get_single_item_res}
    
[Inventory][Reuse][200] - create single item with one serial
    [Arguments]    ${arg_dic}
    ${create_single_item_req}    create dictionary
    ...    access_token=${arg_dic.access_token}    
    ...    name=${arg_dic.item_name}    
    ...    type=${arg_dic.item_type}
    ...    has_serial_number=${arg_dic.item_has_serial_number}
    ${create_single_item_res}    [Inventory][200] - API create single item        ${create_single_item_req}
    ${item_id}    set variable    ${create_single_item_res.id}
    ${create_single_item_serial_req_1}    create dictionary
    ...    access_token=${arg_dic.access_token}
    ...    item_id=${item_id}
    ...    serial_number=${arg_dic.item_serial_number}
    ${create_single_item_serial_res_1}    [Inventory][200] - API create single item serial    ${create_single_item_serial_req_1}
    ${get_single_item_req}    create dictionary
    ...    access_token=${arg_dic.access_token}
    ...    item_id=${item_id}
    ${get_single_item_res}    [Inventory][200] - API get single item    ${get_single_item_req}
    [Return]    ${get_single_item_res}
    
[Inventory][Reuse][200] - create single item with two serials
    [Arguments]    ${arg_dic}
    ${create_single_item_req}    create dictionary
    ...    access_token=${arg_dic.access_token}    
    ...    name=${arg_dic.item_name}    
    ...    type=${arg_dic.item_type}
    ...    has_serial_number=${arg_dic.item_has_serial_number}
    ${create_single_item_res}    [Inventory][200] - API create single item        ${create_single_item_req}
    ${item_id}    set variable    ${create_single_item_res.id}
    ${create_single_item_serial_req_1}    create dictionary
    ...    access_token=${arg_dic.access_token}
    ...    item_id=${item_id}
    ...    serial_number=${arg_dic.item_serial_number_1}
    ${create_single_item_serial_res_1}    [Inventory][200] - API create single item serial    ${create_single_item_serial_req_1}
    ${create_single_item_serial_req_2}    create dictionary
    ...    access_token=${arg_dic.access_token}
    ...    item_id=${item_id}
    ...    serial_number=${arg_dic.item_serial_number_2}
    ${create_single_item_serial_res_2}    [Inventory][200] - API create single item serial    ${create_single_item_serial_req_2}
    ${get_single_item_req}    create dictionary
    ...    access_token=${arg_dic.access_token}
    ...    item_id=${item_id}
    ${get_single_item_res}    [Inventory][200] - API get single item    ${get_single_item_req}
    [Return]    ${get_single_item_res}
    
[Inventory][Reuse][200] - create single item with quantity
    [Arguments]    ${arg_dic}
    ${create_single_item_req}    create dictionary
    ...    access_token=${arg_dic.access_token}    
    ...    name=${arg_dic.item_name}    
    ...    type=${arg_dic.item_type}
    ...    has_serial_number=${arg_dic.item_has_serial_number}
    ${create_single_item_res}    [Inventory][200] - API create single item        ${create_single_item_req}
    ${item_id}    set variable    ${create_single_item_res.id}
    ${update_single_item_quantity_req}    create dictionary
    ...    access_token=${arg_dic.access_token}
    ...    item_id=${item_id}
    ...    available_quantity=${arg_dic.item_available_quantity}
    ${update_single_item_quantity_res}    [Inventory][200] - API update single item quantity    ${update_single_item_quantity_req}
    ${get_single_item_req}    create dictionary
    ...    access_token=${arg_dic.access_token}
    ...    item_id=${item_id}
    ${get_single_item_res}    [Inventory][200] - API get single item    ${get_single_item_req}
    [Return]    ${get_single_item_res}
    
[Inventory][Reuse][200] - create ticket with serial and non-serial
    [Arguments]    ${arg_dic}
    ${create_ticket_res}    [Inventory][200] - API create ticket with serial and non-serial    ${arg_dic}
    ${ticket_id}    set variable    ${create_ticket_res.id}
    ${get_ticket_req}    create dictionary
    ...    access_token=${arg_dic.access_token}
    ...    ticket_id=${ticket_id}
    ${get_ticket_res}    [Inventory][200] - API get ticket    ${get_ticket_req}
    [Return]    ${get_ticket_res}
    
[Inventory][Reuse][200] - create ticket with serial
    [Arguments]    ${arg_dic}
    ${create_ticket_res}    [Inventory][200] - API create ticket with serial    ${arg_dic}
    ${ticket_id}    set variable    ${create_ticket_res.id}
    ${get_ticket_req}    create dictionary
    ...    access_token=${arg_dic.access_token}
    ...    ticket_id=${ticket_id}
    ${get_ticket_res}    [Inventory][200] - API get ticket    ${get_ticket_req}
    [Return]    ${get_ticket_res}   
    
[Inventory][Reuse][200] - create ticket with non-serial
    [Arguments]    ${arg_dic}
    ${create_ticket_res}    [Inventory][200] - API create ticket with non-serial    ${arg_dic}
    ${ticket_id}    set variable    ${create_ticket_res.id}
    ${get_ticket_req}    create dictionary
    ...    access_token=${arg_dic.access_token}
    ...    ticket_id=${ticket_id}
    ${get_ticket_res}    [Inventory][200] - API get ticket    ${get_ticket_req}
    [Return]    ${get_ticket_res}
