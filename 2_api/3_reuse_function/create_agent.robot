*** Settings ***
Resource      ../2_use_case-deprecated/imports.robot

*** Keywords ***
[Agent][Reuse][200] - create agent
    [Documentation]  if having more than 1 currency, pls input likes currency=VND,USD
    [Arguments]     ${arg_dic}
    ${created_agent_type_dic}  [200] API create agent type     ${arg_dic.access_token}      ${arg_dic.agent_type_name}

    set to dictionary   ${arg_dic}     agent_type_id=${created_agent_type_dic.id}
    set to dictionary   ${arg_dic}     identity_type_id=${arg_dic.identity_identity_type_id}
    set to dictionary   ${arg_dic}     username=${arg_dic.agent_username}
    set to dictionary   ${arg_dic}     password=${arg_dic.agent_password}

    ${created_agent_dic}      [200] API create agent      ${arg_dic}
    ${anthen_arg_dic}      create dictionary       username=${arg_dic.agent_username}        password=${arg_dic.agent_password_login}  user_type_id=2
    ${agent_authen_dic}  [200] API agent authentication          ${anthen_arg_dic}

    ${return_dic}        create dictionary

    ${currency_list}     split string    ${arg_dic.currency}     ,
    ${currency_list_length}   get length      ${currency_list}
    :FOR     ${index}   IN RANGE    0   ${currency_list_length}
    \   ${currency}     get from list     ${currency_list}      ${index}
    \   ${created_sof_cash_dic}     [200] API create user sof cash          ${currency}       ${agent_authen_dic.access_token}
    \   set to dictionary   ${return_dic}      sof_cash_id_in_${currency}    ${created_sof_cash_dic.sof_id}

    set to dictionary  ${return_dic}      agent_type_id     ${created_agent_type_dic.id}
    set to dictionary  ${return_dic}      agent_id     ${created_agent_dic.id}
    set to dictionary  ${return_dic}      agent_access_token     ${agent_authen_dic.access_token}

    [Return]    ${return_dic}

[Agent][Reuse][200] - Create agent without sof
    [Arguments]     ${arg_dic}
    ${created_agent_type_dic}  [200] API create agent type     ${arg_dic.access_token}      ${arg_dic.agent_type_name}

    set to dictionary   ${arg_dic}     agent_type_id=${created_agent_type_dic.id}
    set to dictionary   ${arg_dic}     identity_type_id=${arg_dic.identity_identity_type_id}
    set to dictionary   ${arg_dic}     username=${arg_dic.agent_username}
    set to dictionary   ${arg_dic}     password=${arg_dic.agent_password}

    ${created_agent_dic}      [200] API create agent      ${arg_dic}
    ${anthen_arg_dic}      create dictionary       username=${arg_dic.agent_username}        password=${arg_dic.agent_password_login}  user_type_id=2
    ${agent_authen_dic}  [200] API agent authentication          ${anthen_arg_dic}

    ${return_dic}        create dictionary

    set to dictionary  ${return_dic}      agent_type_id     ${created_agent_type_dic.id}
    set to dictionary  ${return_dic}      agent_id     ${created_agent_dic.id}
    set to dictionary  ${return_dic}      agent_access_token     ${agent_authen_dic.access_token}

    [Return]    ${return_dic}