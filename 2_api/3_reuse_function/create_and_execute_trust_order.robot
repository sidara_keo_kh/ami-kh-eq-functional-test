*** Settings ***
Resource      ../2_use_case-deprecated/imports.robot

*** Keywords ***
[Trust_Management][Reuse][200] - create and execute trust order
    [Arguments]    ${arg_dic}
    ${created_order_dic}     [200] API create trust order      ${arg_dic}

    ${execute_order_dic}    create dictionary   access_token=${arg_dic.access_token}    order_id=${created_order_dic.order_id}
    ...     trust_token=${arg_dic.trust_token}

    [200] API execute trust order     ${execute_order_dic}
    ${return_dic}       create dictionary   order_id=${created_order_dic.order_id}
    [Return]    ${return_dic}