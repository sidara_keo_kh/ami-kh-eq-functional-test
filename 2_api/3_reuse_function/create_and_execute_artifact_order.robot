*** Settings ***
Resource      ../2_use_case-deprecated/imports.robot

*** Keywords ***
[Payment][Reuse][200] - create and execute artifact order
    [Arguments]    ${arg_dic}
    ${created_order_dic}     [200] API payment create artifact order
    ...     ${arg_dic.access_token}
    ...     ${arg_dic.ext_transaction_id}
    ...     ${arg_dic.product_service_id}
    ...     ${arg_dic.product_service_name}
    ...     ${arg_dic.product_name}
    ...     ${arg_dic.product_ref_1}
    ...     ${arg_dic.product_ref_2}
    ...     ${arg_dic.product_ref_3}
    ...     ${arg_dic.product_ref_4}
    ...     ${arg_dic.product_ref_5}
    ...     ${arg_dic.payer_user_ref_type}
    ...     ${arg_dic.payer_user_ref_value}
    ...     ${arg_dic.payee_user_ref_type}
    ...     ${arg_dic.payee_user_ref_value}
    ...     ${arg_dic.payee_user_id}
    ...     ${arg_dic.payee_user_type}
    ...     ${arg_dic.amount}
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}
    ...     @{empty}

    ${execute_order_dic}      create dictionary       access_token=${arg_dic.access_token}        order_id=${created_order_dic.order_id}
    [200] API payment execute order     ${execute_order_dic}
    ${return_dic}       create dictionary   order_id=${created_order_dic.order_id}
    [Return]    ${return_dic}

[Payment][Reuse][Fail] - execute artifact order
    [Arguments]    ${arg_dic}
    ${created_order_dic}     [200] API payment create artifact order
    ...     ${arg_dic.access_token}
    ...     ${arg_dic.ext_transaction_id}
    ...     ${arg_dic.product_service_id}
    ...     ${arg_dic.product_service_name}
    ...     ${arg_dic.product_name}
    ...     ${arg_dic.product_ref_1}
    ...     ${arg_dic.product_ref_2}
    ...     ${arg_dic.product_ref_3}
    ...     ${arg_dic.product_ref_4}
    ...     ${arg_dic.product_ref_5}
    ...     ${arg_dic.payer_user_ref_type}
    ...     ${arg_dic.payer_user_ref_value}
    ...     ${arg_dic.payee_user_ref_type}
    ...     ${arg_dic.payee_user_ref_value}
    ...     ${arg_dic.payee_user_id}
    ...     ${arg_dic.payee_user_type}
    ...     ${arg_dic.amount}

    ${execute_order_dic}    create dictionary      access_token=${arg_dic.access_token}     order_id=${created_order_dic.order_id}
    ...     code=500
    [Fail] API payment execute order     ${execute_order_dic}
    ${return_dic}       create dictionary   order_id=${created_order_dic.order_id}
    [Return]    ${return_dic}

[Payment][Reuse][Fail] - create and execute artifact order
    [Arguments]    ${arg_dic}
    ${created_order_dic}     [200] API payment create artifact order
    ...     ${arg_dic.access_token}
    ...     ${arg_dic.ext_transaction_id}
    ...     ${arg_dic.product_service_id}
    ...     ${arg_dic.product_service_name}
    ...     ${arg_dic.product_name}
    ...     ${arg_dic.product_ref_1}
    ...     ${arg_dic.product_ref_2}
    ...     ${arg_dic.product_ref_3}
    ...     ${arg_dic.product_ref_4}
    ...     ${arg_dic.product_ref_5}
    ...     ${arg_dic.payer_user_ref_type}
    ...     ${arg_dic.payer_user_ref_value}
    ...     ${arg_dic.payee_user_ref_type}
    ...     ${arg_dic.payee_user_ref_value}
    ...     ${arg_dic.payee_user_id}
    ...     ${arg_dic.payee_user_type}
    ...     ${arg_dic.amount}

    ${execute_order_dic}      create dictionary       access_token=${arg_dic.access_token}        order_id=${created_order_dic.order_id}
    [200] API payment execute order     ${execute_order_dic}
    ${return_dic}       create dictionary   order_id=${created_order_dic.order_id}
    [Return]    ${return_dic}

[Payment][Reuse][200] - create artifact order
    [Arguments]    ${arg_dic}
    ${created_order_dic}     [200] API payment create artifact order
    ...     ${arg_dic.access_token}
    ...     ${arg_dic.ext_transaction_id}
    ...     ${arg_dic.product_service_id}
    ...     ${arg_dic.product_service_name}
    ...     ${arg_dic.product_name}
    ...     ${arg_dic.product_ref_1}
    ...     ${arg_dic.product_ref_2}
    ...     ${arg_dic.product_ref_3}
    ...     ${arg_dic.product_ref_4}
    ...     ${arg_dic.product_ref_5}
    ...     ${arg_dic.payer_user_ref_type}
    ...     ${arg_dic.payer_user_ref_value}
    ...     ${arg_dic.payee_user_ref_type}
    ...     ${arg_dic.payee_user_ref_value}
    ...     ${arg_dic.payee_user_id}
    ...     ${arg_dic.payee_user_type}
    ...     ${arg_dic.amount}

    ${execute_order_dic}      create dictionary       access_token=${arg_dic.access_token}        order_id=${created_order_dic.order_id}
    ${return_dic}       create dictionary   order_id=${created_order_dic.order_id}
    [Return]    ${return_dic}
