*** Settings ***
Resource        ../../../1_common/keywords.robot


*** Keywords ***
[Password_Center][Prepare] - update password rule config - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "min_length": "string",
        ...		    "max_length": "string",
        ...		    "expire_after": "string",
        ...		    "disallow_last_password": "string",
        ...		    "max_repeated_numeric_characters": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Password_Center][Prepare] - create identity type - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "name": "string",
        ...		    "password_type_id": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Password_Center][Prepare] - update identity type - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "name": "string",
        ...		    "password_type_id": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}