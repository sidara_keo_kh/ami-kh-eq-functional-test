*** Settings ***
Resource        ../../../1_common/keywords.robot


*** Keywords ***
[Channel_Adapter][Prepare] - Get all order fields via channel adapter
    [Arguments]     ${output}=header    &{arg_dic}
    ${header}              catenate          SEPARATOR=&
        ...     content_type=application/json
        ...     authorization=Bearer ${test_admin_access_token}
        ...     client_id=${setup_admin_client_id}
        ...     client_secret=${setup_admin_client_secret}
    [Common] - Set variable       name=${output}      value=${params}