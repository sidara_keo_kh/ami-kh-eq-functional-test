*** Settings ***
Resource        ../../../1_common/keywords.robot


*** Keywords ***
[Sof_Card][Prepare] - add card design - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...		    "name": "string",
        ...		    "card_type_id": "int",
        ...		    "is_active": "boolean",
        ...		    "currency": "string",
        ...		    "pan_pattern": "string",
        ...		    "pre_sof_order_url": "string",
        ...		    "pre_sof_order_read_timeout": "int",
        ...		    "pre_link_url": "string",
        ...		    "pre_link_read_timeout": "int",
        ...		    "link_url": "string",
        ...		    "link_read_timeout": "int",
        ...		    "un_link_url": "string",
        ...		    "un_link_read_timeout": "int",
        ...		    "debit_url": "string",
        ...		    "debit_read_timeout": "int",
        ...		    "credit_url": "string",
        ...		    "credit_read_timeout": "int",
        ...		    "check_status_url": "string",
        ...		    "check_status_read_timeout": "int",
        ...		    "cancel_url": "string",
        ...		    "cancel_read_timeout": "int",
        ...		    "pre_sof_check_status_url": "string",
        ...		    "pre_sof_check_status_read_timeout": "int",
        ...		    "pre_sof_check_status_max_retry": "int"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Sof_Card][Prepare] - add provider - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...         "name": "string"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Sof_Card][Prepare] - Update provider - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	{
        ...         "name": "string"
        ...	}
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Sof_Card][Prepare] - Update card design - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...		    "name": "string",
        ...		    "card_type_id": "int",
        ...		    "is_active": "boolean",
        ...		    "currency": "string",
        ...		    "pan_pattern": "string",
        ...		    "pre_sof_order_url": "string",
        ...		    "pre_sof_order_read_timeout": "int",
        ...		    "pre_link_url": "string",
        ...		    "pre_link_read_timeout": "int",
        ...		    "link_url": "string",
        ...		    "link_read_timeout": "int",
        ...		    "un_link_url": "string",
        ...		    "un_link_read_timeout": "int",
        ...		    "debit_url": "string",
        ...		    "debit_read_timeout": "int",
        ...		    "credit_url": "string",
        ...		    "credit_read_timeout": "int",
        ...		    "check_status_url": "string",
        ...		    "check_status_read_timeout": "int",
        ...		    "cancel_url": "string",
        ...		    "cancel_read_timeout": "int",
        ...		    "pre_sof_check_status_url": "string",
        ...		    "pre_sof_check_status_read_timeout": "int",
        ...		    "pre_sof_check_status_max_retry": "int"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}