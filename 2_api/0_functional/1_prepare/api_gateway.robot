*** Settings ***
Resource        ../../../1_common/keywords.robot

*** Keywords ***
[Api_Gateway][Prepare] - Agent authentication - params
    [Arguments]     ${output}=params    &{arg_dic}
    ${params}              catenate          SEPARATOR=&
        ...     grant_type=${arg_dic.grant_type}
        ...     username=${arg_dic.username}
        ...     password=${arg_dic.password}
        ...     client_id=${arg_dic.client_id}
    [Common] - Set variable       name=${output}      value=${params}

[Api_Gateway][Prepare] - Customer authentication - params
    [Arguments]     ${output}=params    &{arg_dic}
    ${params}              catenate          SEPARATOR=&
        ...     grant_type=${arg_dic.grant_type}
        ...     username=${arg_dic.username}
        ...     password=${arg_dic.password}
        ...     client_id=${arg_dic.client_id}
    [Common] - Set variable       name=${output}      value=${params}

[Api_Gateway][Prepare] - System user authentication - params
    [Arguments]     ${output}=params    &{arg_dic}
    ${params}              catenate          SEPARATOR=&
        ...     grant_type=${arg_dic.grant_type}
        ...     username=${arg_dic.username}
        ...     password=${arg_dic.password}
        ...     client_id=${arg_dic.client_id}
    [Common] - Set variable       name=${output}      value=${params}

[Api_Gateway][Prepare] - Create oauth client - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...         "client_id":"string",
        ...         "client_secret":"string",
        ...         "client_name":"string random_string(10, '${LETTERS}')",
        ...         "authorized_grant_types":"integer 1",
        ...         "web_server_redirect_uri":"string 'http://www.example.com'",
        ...         "access_token_validity":"integer 3600",
        ...         "refresh_token_validity":"integer 3600",
        ...         "authorization_code_validity":"integer 600"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Api_Gateway][Prepare] - Create authorization code - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...       "scope": "string",
        ...       "client_id": "string",
        ...       "user_id": "integer",
        ...       "user_type": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}
    
[Api_Gateway][Prepare] - Revoke token by user - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...       "user_ids": [ "integer" ],
        ...       "user_type": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}
    
[Api_Gateway][Prepare] - Verify token - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...       "client_id": "string",
        ...       "token": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}
    
[Api_Gateway][Prepare] - Create service - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...       "location": "string",
        ...       "name": "string random_string(10, '${LETTERS}')",
        ...       "timeout": "integer 60000",
        ...       "max_total_connection": "integer 300",
        ...       "max_per_route": "integer 30"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Api_Gateway][Prepare] - Create api - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...       "name": "string random_string(10, '${LETTERS}')",
        ...       "http_method": "string",
        ...       "pattern": "string",
        ...       "is_internal": "boolean",
        ...       "is_required_access_token": "boolean",
        ...       "permission_required": "boolean",
        ...       "service_id": "integer",
        ...       "prefix": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Api_Gateway][Prepare] - Add client scope - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}   &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...       "scopes": [ "integer" ]
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}