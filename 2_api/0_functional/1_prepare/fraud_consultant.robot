*** Settings ***
Resource        ../../../1_common/keywords.robot

*** Keywords ***
[Fraud_Consultant][Prepare] - create fraud ticket - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_action}=   [Common] - Create string param in dic      ${arg_dic}      action
    ${start_active_ticket_timestamp}=   [Common] - Create string param in dic      ${arg_dic}      start_active_ticket_timestamp
    ${end_active_ticket_timestamp}=   [Common] - Create string param in dic      ${arg_dic}      end_active_ticket_timestamp
    ${description}=   [Common] - Create string param in dic      ${arg_dic}      description

    ${data_json}    set variable    ${empty}
    ${length}   get length      ${arg_dic.data}
    :FOR     ${index}   IN RANGE   0    ${length}
    \      ${ticket_data_item}    get from list      ${arg_dic.data}      ${index}
    \      ${ticket_data_item}    strip string         ${ticket_data_item}
    \      ${data_json}     catenate    SEPARATOR=,  ${data_json}   ${ticket_data_item}
    log     ${data_json}
    ${ticket_data_json}              catenate       SEPARATOR=
        ...     	,"data":
        ...     	[
        ...             ${data_json}
        ...     	]
    ${ticket_data_json}     replace string      ${ticket_data_json}     [,        [
    ${ticket_data_json}     replace string          ${ticket_data_json}     ,]        ]

    ${body}              catenate       SEPARATOR=
        ...     {
        ...         ${param_action}
        ...         ${start_active_ticket_timestamp}
        ...         ${end_active_ticket_timestamp}
        ...         ${description}
        ...         ${ticket_data_json}
        ...     }
    ${body}  replace string        ${body}      ,,      ,
    ${body}  replace string        ${body}      ,}      }
    [Common] - Set variable       name=${output}      value=${body}

[Fraud_Consultant][Prepare] - unblock ticket with reason - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body_key_dict}    create dictionary   delete_reason=str
    ${body_dict}    [Common] - build body request dict from list    ${body_key_dict}    ${arg_dic}
    ${body}     [Common] - create json string from dict    ${body_dict}     ${body_key_dict}
    [Common] - Set variable       name=${output}      value=${body}
