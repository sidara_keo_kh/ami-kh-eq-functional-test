*** Settings ***
Resource        ../../../1_common/keywords.robot


*** Keywords ***
[Channel_Gateway][Prepare] - add api - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...         "name": "string random_string(10, '${LETTERS}')",
        ...         "http_method": "string random_enum('GET', 'POST', 'PUT', 'DELETE')",
        ...         "pattern": "string random_string(10, '${LETTERS}')",
        ...         "is_required_access_token": "boolean False",
        ...         "permission_required": "boolean False",
        ...         "service_id": "integer"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Channel_Gateway][Prepare] - update api - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...         "name": "string",
        ...         "http_method": "string",
        ...         "pattern": "string",
        ...         "is_required_access_token": "boolean",
        ...         "permission_required": "boolean",
        ...         "service_id": "integer"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Channel_Gateway][Prepare] - search api - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...         "id": "integer",
        ...         "ids": [ "integer" ],
        ...         "name": "string",
        ...         "http_method": "string",
        ...         "service_id": "integer",
        ...         "is_deleted": "boolean",
        ...         "page_index": "integer",
        ...         "paging": "boolean"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Channel_Gateway][Prepare] - agent authentication - params
    [Arguments]     ${output}=params    &{arg_dic}
    ${params}              catenate          SEPARATOR=&
        ...     grant_type=${arg_dic.grant_type}
        ...     username=${arg_dic.username}
        ...     password=${arg_dic.password}
        ...     client_id=${arg_dic.param_client_id}
    [Common] - Set variable       name=${output}      value=${params}


[Channel_Gateway][Prepare] - add service - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...       "location": "string",
        ...       "name": "string random_string(10, '${LETTERS}')",
        ...       "timeout": "integer 60000",
        ...       "max_total_connection": "integer 300",
        ...       "max_per_route": "integer 30"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Channel_Gateway][Prepare] - update service - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...       "location": "string",
        ...       "name": "string",
        ...       "timeout": "integer",
        ...       "max_total_connection": "integer",
        ...       "max_per_route": "integer"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Channel_Gateway][Prepare] - search service - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...         "id": "integer",
        ...         "name": "string",
        ...         "is_deleted": "boolean",
        ...         "page_index": "integer",
        ...         "paging": "boolean"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Channel_Gateway][Prepare] - add scopes to client - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}   &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...       "scopes": [ "integer" ]
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Channel_Gateway][Prepare] - delete scope from client - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}   &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...       "scopes": [ "integer" ]
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}
