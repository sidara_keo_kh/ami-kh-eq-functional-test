*** Settings ***
Resource        ../../../1_common/keywords.robot

*** Keywords ***

[Customer][Prepare] - create customer classification - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...         "name": "string random_string(10, '${LETTERS}')",
        ...		    "description": "string random_string(10, '${LETTERS}')"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Customer][Prepare] - update customer classification - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...		    "name": "string",
        ...		    "description": "string"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Customer][Prepare] - active-suspend customer - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...         "is_suspended": "bool"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Customer][Prepare] - add customer identities - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...         "identity_type_id": "int",
        ...         "username": "string",
        ...         "password": "string"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Customer][Prepare] - system user create customer - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...         "profile": {
        ...		        "is_testing_account": "boolean",
        ...		        "is_system_account": "boolean",
        ...		        "acquisition_source": "string",
        ...		        "customer_classification_id": "int",
        ...             "referrer_user_type": {
        ...                 "id": "int",
        ...                 "name": "string"
        ...             },
        ...             "referrer_user_id": "int",
        ...             "unique_reference": "string",
        ...		        "beneficiary": "string",
        ...		        "mm_card_type_id": "int",
        ...		        "mm_card_level_id": "int",
        ...		        "mm_factory_card_number": "string",
        ...		        "is_require_otp": "boolean",
        ...		        "tin_number": "string",
        ...		        "tin_number_local": "string",
        ...		        "title": "string",
        ...		        "title_local": "string",
        ...		        "first_name": "string",
        ...		        "first_name_local": "string",
        ...		        "middle_name": "string",
        ...		        "middle_name_local": "string",
        ...		        "last_name": "string",
        ...		        "last_name_local": "string",
        ...		        "suffix": "string",
        ...		        "suffix_local": "string",
        ...		        "date_of_birth": "string",
        ...		        "place_of_birth": "string",
        ...		        "place_of_birth_local": "string",
        ...		        "gender": "string",
        ...		        "gender_local": "string",
        ...		        "ethnicity": "string",
        ...		        "employer": "string",
        ...		        "nationality": "string",
        ...		        "occupation": "string",
        ...		        "occupation_local": "string",
        ...		        "occupation_title": "string",
        ...		        "occupation_title_local": "string",
        ...		        "township_code": "string",
        ...		        "township_name": "string",
        ...		        "township_name_local": "string",
        ...		        "mother_name": "string",
        ...		        "mother_name_local": "string",
        ...		        "mother_maiden_name": "string",
        ...		        "mother_maiden_name_local": "string",
        ...		        "civil_status": "string",
        ...		        "email": "string",
        ...		        "primary_mobile_number": "string",
        ...		        "secondary_mobile_number": "string",
        ...		        "tertiary_mobile_number": "string",
        ...		        "telephone_number": "string",
        ...		        "employee_id": "string",
        ...		        "source_of_funds": "string",
        ...		        "payroll_card_number": "string",
        ...             "address": {
        ...                 "current_address": {
        ...		                "citizen_association": "string",
        ...		                "neighbourhood_association": "string",
        ...		                "address": "string",
        ...		                "commune": "string",
        ...		                "district": "string",
        ...		                "city": "string",
        ...		                "province": "string",
        ...		                "postal_code": "string",
        ...		                "country": "string",
        ...		                "landmark": "string",
        ...		                "longitude": "string",
        ...		                "latitude": "string",
        ...		                "address_local": "string",
        ...		                "commune_local": "string",
        ...		                "district_local": "string",
        ...		                "city_local": "string",
        ...		                "province_local": "string",
        ...		                "postal_code_local": "string",
        ...		                "country_local": "string"
        ...                 },
        ...                 "permanent_address": {
        ...		                "citizen_association": "string",
        ...		                "neighbourhood_association": "string",
        ...		                "address": "string",
        ...		                "commune": "string",
        ...		                "district": "string",
        ...		                "city": "string",
        ...		                "province": "string",
        ...		                "postal_code": "string",
        ...		                "country": "string",
        ...		                "landmark": "string",
        ...		                "longitude": "string",
        ...		                "latitude": "string",
        ...		                "address_local": "string",
        ...		                "commune_local": "string",
        ...		                "district_local": "string",
        ...		                "city_local": "string",
        ...		                "province_local": "string",
        ...		                "postal_code_local": "string",
        ...		                "country_local": "string"
        ...                 }
        ...             },
        ...             "kyc": {
        ...                 "primary_identity": {
        ...		                "type": "string",
        ...		                "status": "int",
        ...		                "identity_id": "string",
        ...		                "identity_id_local": "string",
        ...		                "place_of_issue": "string",
        ...		                "issue_date": "string",
        ...		                "expired_date": "string",
        ...		                "front_identity_url": "string",
        ...		                "back_identity_url": "string",
        ...		                "signature_url": "string"
        ...                 },
        ...                 "secondary_identity": {
        ...		                "type": "string",
        ...		                "status": "int",
        ...		                "identity_id": "string",
        ...		                "identity_id_local": "string",
        ...		                "place_of_issue": "string",
        ...		                "issue_date": "string",
        ...		                "expired_date": "string",
        ...		                "front_identity_url": "string",
        ...		                "back_identity_url": "string",
        ...		                "signature_url": "string"
        ...                 },
        ...                 "tertiary_identity": {
        ...		                "type": "string",
        ...		                "status": "int",
        ...		                "identity_id": "string",
        ...		                "identity_id_local": "string",
        ...		                "place_of_issue": "string",
        ...		                "issue_date": "string",
        ...		                "expired_date": "string",
        ...		                "front_identity_url": "string",
        ...		                "back_identity_url": "string",
        ...		                "signature_url": "string"
        ...                 },
        ...		            "level": "int",
        ...		            "remark": "string",
        ...		            "verify_by": "string",
        ...		            "verify_date": "string",
        ...		            "risk_level": "string"
        ...             },
        ...             "additional": {
        ...		            "profile_picture_url": "string",
        ...		            "tax_id_card_photo_url": "string",
        ...		            "field_1_name": "string",
        ...		            "field_1_value": "string",
        ...		            "field_2_name": "string",
        ...		            "field_2_value": "string",
        ...		            "field_3_name": "string",
        ...		            "field_3_value": "string",
        ...		            "field_4_name": "string",
        ...		            "field_4_value": "string",
        ...		            "field_5_name": "string",
        ...		            "field_5_value": "string",
        ...		            "supporting_file_1_url": "string",
        ...		            "supporting_file_2_url": "string",
        ...		            "supporting_file_3_url": "string",
        ...		            "supporting_file_4_url": "string",
        ...		            "supporting_file_5_url": "string",
        ...		            "sss_number": "string",
        ...		            "gsis_number": "string",
        ...		            "tax_income_number": "string",
        ...		            "philhealth_number": "string",
        ...		            "hdmf_number": "string",
        ...		            "additional_id_type": "string",
        ...		            "additional_id_number": "string",
        ...		            "region_id": "string"
        ...             }
        ...         },
        ...         "identity": {
        ...		        "identity_type_id": "int",
        ...		        "username": "string",
        ...		        "password": "string",
        ...		        "auto_generate_password": "boolean"
        ...         }
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Customer][Prepare] - register customer - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...         "profile": {
        ...             "referrer_user_type": {
        ...                 "id": "int",
        ...                 "name": "string"
        ...             },
        ...             "referrer_user_id": "int",
        ...             "unique_reference": "string",
        ...		        "beneficiary": "string",
        ...		        "mm_card_type_id": "int",
        ...		        "mm_card_level_id": "int",
        ...		        "mm_factory_card_number": "string",
        ...		        "is_require_otp": "boolean",
        ...		        "tin_number": "string",
        ...		        "tin_number_local": "string",
        ...		        "title": "string",
        ...		        "title_local": "string",
        ...		        "first_name": "string",
        ...		        "first_name_local": "string",
        ...		        "middle_name": "string",
        ...		        "middle_name_local": "string",
        ...		        "last_name": "string",
        ...		        "last_name_local": "string",
        ...		        "suffix": "string",
        ...		        "suffix_local": "string",
        ...		        "date_of_birth": "string",
        ...		        "place_of_birth": "string",
        ...		        "place_of_birth_local": "string",
        ...		        "gender": "string",
        ...		        "gender_local": "string",
        ...		        "ethnicity": "string",
        ...		        "employer": "string",
        ...		        "nationality": "string",
        ...		        "occupation": "string",
        ...		        "occupation_local": "string",
        ...		        "occupation_title": "string",
        ...		        "occupation_title_local": "string",
        ...		        "township_code": "string",
        ...		        "township_name": "string",
        ...		        "township_name_local": "string",
        ...		        "mother_name": "string",
        ...		        "mother_name_local": "string",
        ...		        "mother_maiden_name": "string",
        ...		        "mother_maiden_name_local": "string",
        ...		        "civil_status": "string",
        ...		        "email": "string",
        ...		        "primary_mobile_number": "string",
        ...		        "secondary_mobile_number": "string",
        ...		        "tertiary_mobile_number": "string",
        ...		        "telephone_number": "string",
        ...             "address": {
        ...                 "current_address": {
        ...		                "citizen_association": "string",
        ...		                "neighbourhood_association": "string",
        ...		                "address": "string",
        ...		                "commune": "string",
        ...		                "district": "string",
        ...		                "city": "string",
        ...		                "province": "string",
        ...		                "postal_code": "string",
        ...		                "country": "string",
        ...		                "landmark": "string",
        ...		                "longitude": "string",
        ...		                "latitude": "string",
        ...		                "address_local": "string",
        ...		                "commune_local": "string",
        ...		                "district_local": "string",
        ...		                "city_local": "string",
        ...		                "province_local": "string",
        ...		                "postal_code_local": "string",
        ...		                "country_local": "string"
        ...                 },
        ...                 "permanent_address": {
        ...		                "citizen_association": "string",
        ...		                "neighbourhood_association": "string",
        ...		                "address": "string",
        ...		                "commune": "string",
        ...		                "district": "string",
        ...		                "city": "string",
        ...		                "province": "string",
        ...		                "postal_code": "string",
        ...		                "country": "string",
        ...		                "landmark": "string",
        ...		                "longitude": "string",
        ...		                "latitude": "string",
        ...		                "address_local": "string",
        ...		                "commune_local": "string",
        ...		                "district_local": "string",
        ...		                "city_local": "string",
        ...		                "province_local": "string",
        ...		                "postal_code_local": "string",
        ...		                "country_local": "string"
        ...                 }
        ...             },
        ...             "kyc": {
        ...                 "primary_identity": {
        ...		                "type": "string",
        ...		                "identity_id": "string",
        ...		                "identity_id_local": "string",
        ...		                "place_of_issue": "string",
        ...		                "issue_date": "string",
        ...		                "expired_date": "string",
        ...		                "front_identity_url": "string",
        ...		                "back_identity_url": "string",
        ...		                "signature_url": "string"
        ...                 },
        ...                 "secondary_identity": {
        ...		                "type": "string",
        ...		                "identity_id": "string",
        ...		                "identity_id_local": "string",
        ...		                "place_of_issue": "string",
        ...		                "issue_date": "string",
        ...		                "expired_date": "string",
        ...		                "front_identity_url": "string",
        ...		                "back_identity_url": "string",
        ...		                "signature_url": "string"
        ...                 },
        ...                 "tertiary_identity": {
        ...		                "type": "string",
        ...		                "identity_id": "string",
        ...		                "identity_id_local": "string",
        ...		                "place_of_issue": "string",
        ...		                "issue_date": "string",
        ...		                "expired_date": "string",
        ...		                "front_identity_url": "string",
        ...		                "back_identity_url": "string",
        ...		                "signature_url": "string"
        ...                 }
        ...             },
        ...             "additional": {
        ...		            "profile_picture_url": "string",
        ...		            "tax_id_card_photo_url": "string"
        ...             }
        ...         },
        ...         "identity": {
        ...		        "identity_type_id": "int",
        ...		        "username": "string",
        ...		        "password": "string",
        ...		        "auto_generate_password": "boolean"
        ...         }
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Customer][Prepare] - system user creates customer profile - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "is_member": "boolean",
        ...		    "is_testing_account": "boolean",
        ...		    "is_system_account": "boolean",
        ...		    "acquisition_source": "string",
        ...		    "customer_classification_id": "int",
        ...         "referrer_user_type": {
        ...             "id": "int",
        ...             "name": "string"
        ...         },
        ...         "referrer_user_id": "int",
        ...         "unique_reference": "string",
        ...		    "beneficiary": "string",
        ...		    "mm_card_type_id": "int",
        ...		    "mm_card_level_id": "int",
        ...		    "mm_factory_card_number": "string",
        ...		    "is_require_otp": "boolean",
        ...		    "tin_number": "string",
        ...		    "tin_number_local": "string",
        ...		    "title": "string",
        ...		    "title_local": "string",
        ...		    "first_name": "string",
        ...		    "first_name_local": "string",
        ...		    "middle_name": "string",
        ...		    "middle_name_local": "string",
        ...		    "last_name": "string",
        ...		    "last_name_local": "string",
        ...		    "suffix": "string",
        ...		    "suffix_local": "string",
        ...		    "date_of_birth": "string",
        ...		    "place_of_birth": "string",
        ...		    "place_of_birth_local": "string",
        ...		    "gender": "string",
        ...		    "gender_local": "string",
        ...		    "ethnicity": "string",
        ...		    "employer": "string",
        ...		    "nationality": "string",
        ...		    "occupation": "string",
        ...		    "occupation_local": "string",
        ...		    "occupation_title": "string",
        ...		    "occupation_title_local": "string",
        ...		    "township_code": "string",
        ...		    "township_name": "string",
        ...		    "township_name_local": "string",
        ...		    "mother_name": "string",
        ...		    "mother_name_local": "string",
        ...		    "mother_maiden_name": "string",
        ...		    "mother_maiden_name_local": "string",
        ...		    "civil_status": "string",
        ...		    "email": "string",
        ...		    "primary_mobile_number": "string",
        ...		    "secondary_mobile_number": "string",
        ...		    "tertiary_mobile_number": "string",
        ...		    "telephone_number": "string",
        ...         "address": {
        ...             "current_address": {
        ...		            "citizen_association": "string",
        ...		            "neighbourhood_association": "string",
        ...		            "address": "string",
        ...		            "commune": "string",
        ...		            "district": "string",
        ...		            "city": "string",
        ...		            "province": "string",
        ...		            "postal_code": "string",
        ...		            "country": "string",
        ...		            "landmark": "string",
        ...		            "longitude": "string",
        ...		            "latitude": "string",
        ...		            "address_local": "string",
        ...		            "commune_local": "string",
        ...		            "district_local": "string",
        ...		            "city_local": "string",
        ...		            "province_local": "string",
        ...		            "postal_code_local": "string",
        ...		            "country_local": "string"
        ...             },
        ...             "permanent_address": {
        ...		            "citizen_association": "string",
        ...		            "neighbourhood_association": "string",
        ...		            "address": "string",
        ...		            "commune": "string",
        ...		            "district": "string",
        ...		            "city": "string",
        ...		            "province": "string",
        ...		            "postal_code": "string",
        ...		            "country": "string",
        ...		            "landmark": "string",
        ...		            "longitude": "string",
        ...		            "latitude": "string",
        ...		            "address_local": "string",
        ...		            "commune_local": "string",
        ...		            "district_local": "string",
        ...		            "city_local": "string",
        ...		            "province_local": "string",
        ...		            "postal_code_local": "string",
        ...		            "country_local": "string"
        ...             }
        ...         },
        ...         "kyc": {
        ...             "primary_identity": {
        ...		            "type": "string",
        ...		            "status": "int",
        ...		            "identity_id": "string",
        ...		            "identity_id_local": "string",
        ...		            "place_of_issue": "string",
        ...		            "issue_date": "string",
        ...		            "expired_date": "string",
        ...		            "front_identity_url": "string",
        ...		            "back_identity_url": "string",
        ...		            "signature_url": "string"
        ...             },
        ...             "secondary_identity": {
        ...		            "type": "string",
        ...		            "status": "int",
        ...		            "identity_id": "string",
        ...		            "identity_id_local": "string",
        ...		            "place_of_issue": "string",
        ...		            "issue_date": "string",
        ...		            "expired_date": "string",
        ...		            "front_identity_url": "string",
        ...		            "back_identity_url": "string",
        ...		            "signature_url": "string"
        ...             },
        ...             "tertiary_identity": {
        ...		            "type": "string",
        ...		            "status": "int",
        ...		            "identity_id": "string",
        ...		            "identity_id_local": "string",
        ...		            "place_of_issue": "string",
        ...		            "issue_date": "string",
        ...		            "expired_date": "string",
        ...		            "front_identity_url": "string",
        ...		            "back_identity_url": "string",
        ...		            "signature_url": "string"
        ...             },
        ...		        "level": "int",
        ...		        "remark": "string",
        ...		        "verify_by": "string",
        ...		        "verify_date": "string",
        ...		        "risk_level": "string"
        ...         },
        ...         "additional": {
        ...		        "profile_picture_url": "string",
        ...		        "tax_id_card_photo_url": "string",
        ...		        "field_1_name": "string",
        ...		        "field_1_value": "string",
        ...		        "field_2_name": "string",
        ...		        "field_2_value": "string",
        ...		        "field_3_name": "string",
        ...		        "field_3_value": "string",
        ...		        "field_4_name": "string",
        ...		        "field_4_value": "string",
        ...		        "field_5_name": "string",
        ...		        "field_5_value": "string",
        ...		        "supporting_file_1_url": "string",
        ...		        "supporting_file_2_url": "string",
        ...		        "supporting_file_3_url": "string",
        ...		        "supporting_file_4_url": "string",
        ...		        "supporting_file_5_url": "string"
        ...         }
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Customer][Prepare] - customer registers customer profile - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...         "referrer_user_type": {
        ...             "id": "int",
        ...             "name": "string"
        ...         },
        ...         "referrer_user_id": "int",
        ...         "unique_reference": "string",
        ...		    "beneficiary": "string",
        ...		    "mm_card_type_id": "int",
        ...		    "mm_card_level_id": "int",
        ...		    "mm_factory_card_number": "string",
        ...		    "is_require_otp": "boolean",
        ...		    "tin_number": "string",
        ...		    "tin_number_local": "string",
        ...		    "title": "string",
        ...		    "title_local": "string",
        ...		    "first_name": "string",
        ...		    "first_name_local": "string",
        ...		    "middle_name": "string",
        ...		    "middle_name_local": "string",
        ...		    "last_name": "string",
        ...		    "last_name_local": "string",
        ...		    "suffix": "string",
        ...		    "suffix_local": "string",
        ...		    "date_of_birth": "string",
        ...		    "place_of_birth": "string",
        ...		    "place_of_birth_local": "string",
        ...		    "gender": "string",
        ...		    "gender_local": "string",
        ...		    "ethnicity": "string",
        ...		    "employer": "string",
        ...		    "nationality": "string",
        ...		    "occupation": "string",
        ...		    "occupation_local": "string",
        ...		    "occupation_title": "string",
        ...		    "occupation_title_local": "string",
        ...		    "township_code": "string",
        ...		    "township_name": "string",
        ...		    "township_name_local": "string",
        ...		    "mother_name": "string",
        ...		    "mother_name_local": "string",
        ...		    "mother_maiden_name": "string",
        ...		    "mother_maiden_name_local": "string",
        ...		    "civil_status": "string",
        ...		    "email": "string",
        ...		    "primary_mobile_number": "string",
        ...		    "secondary_mobile_number": "string",
        ...		    "tertiary_mobile_number": "string",
        ...		    "telephone_number": "string",
        ...         "address": {
        ...             "current_address": {
        ...		            "citizen_association": "string",
        ...		            "neighbourhood_association": "string",
        ...		            "address": "string",
        ...		            "commune": "string",
        ...		            "district": "string",
        ...		            "city": "string",
        ...		            "province": "string",
        ...		            "postal_code": "string",
        ...		            "country": "string",
        ...		            "landmark": "string",
        ...		            "longitude": "string",
        ...		            "latitude": "string",
        ...		            "address_local": "string",
        ...		            "commune_local": "string",
        ...		            "district_local": "string",
        ...		            "city_local": "string",
        ...		            "province_local": "string",
        ...		            "postal_code_local": "string",
        ...		            "country_local": "string"
        ...             },
        ...             "permanent_address": {
        ...		            "citizen_association": "string",
        ...		            "neighbourhood_association": "string",
        ...		            "address": "string",
        ...		            "commune": "string",
        ...		            "district": "string",
        ...		            "city": "string",
        ...		            "province": "string",
        ...		            "postal_code": "string",
        ...		            "country": "string",
        ...		            "landmark": "string",
        ...		            "longitude": "string",
        ...		            "latitude": "string",
        ...		            "address_local": "string",
        ...		            "commune_local": "string",
        ...		            "district_local": "string",
        ...		            "city_local": "string",
        ...		            "province_local": "string",
        ...		            "postal_code_local": "string",
        ...		            "country_local": "string"
        ...             }
        ...         },
        ...         "kyc": {
        ...             "primary_identity": {
        ...		            "type": "string",
        ...		            "identity_id": "string",
        ...		            "identity_id_local": "string",
        ...		            "place_of_issue": "string",
        ...		            "issue_date": "string",
        ...		            "expired_date": "string",
        ...		            "front_identity_url": "string",
        ...		            "back_identity_url": "string",
        ...		            "signature_url": "string"
        ...             },
        ...             "secondary_identity": {
        ...		            "type": "string",
        ...		            "identity_id": "string",
        ...		            "identity_id_local": "string",
        ...		            "place_of_issue": "string",
        ...		            "issue_date": "string",
        ...		            "expired_date": "string",
        ...		            "front_identity_url": "string",
        ...		            "back_identity_url": "string",
        ...		            "signature_url": "string"
        ...             },
        ...             "tertiary_identity": {
        ...		            "type": "string",
        ...		            "identity_id": "string",
        ...		            "identity_id_local": "string",
        ...		            "place_of_issue": "string",
        ...		            "issue_date": "string",
        ...		            "expired_date": "string",
        ...		            "front_identity_url": "string",
        ...		            "back_identity_url": "string",
        ...		            "signature_url": "string"
        ...             }
        ...         },
        ...         "additional": {
        ...		        "profile_picture_url": "string",
        ...		        "tax_id_card_photo_url": "string"
        ...         }
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Customer][Prepare] - system user updates full customer profile - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "is_testing_account": "boolean",
        ...		    "is_system_account": "boolean",
        ...		    "acquisition_source": "string",
        ...		    "customer_classification_id": "int",
        ...         "referrer_user_id": "int",
        ...         "unique_reference": "string",
        ...		    "beneficiary": "string",
        ...		    "mm_card_type_id": "int",
        ...		    "mm_card_level_id": "int",
        ...		    "mm_factory_card_number": "string",
        ...		    "is_require_otp": "boolean",
        ...		    "tin_number": "string",
        ...		    "tin_number_local": "string",
        ...		    "title": "string",
        ...		    "title_local": "string",
        ...		    "first_name": "string",
        ...		    "first_name_local": "string",
        ...		    "middle_name": "string",
        ...		    "middle_name_local": "string",
        ...		    "last_name": "string",
        ...		    "last_name_local": "string",
        ...		    "suffix": "string",
        ...		    "suffix_local": "string",
        ...		    "date_of_birth": "string",
        ...		    "place_of_birth": "string",
        ...		    "place_of_birth_local": "string",
        ...		    "gender": "string",
        ...		    "gender_local": "string",
        ...		    "ethnicity": "string",
        ...		    "employer": "string",
        ...		    "nationality": "string",
        ...		    "occupation": "string",
        ...		    "occupation_local": "string",
        ...		    "occupation_title": "string",
        ...		    "occupation_title_local": "string",
        ...		    "township_code": "string",
        ...		    "township_name": "string",
        ...		    "township_name_local": "string",
        ...		    "mother_name": "string",
        ...		    "mother_name_local": "string",
        ...		    "mother_maiden_name": "string",
        ...		    "mother_maiden_name_local": "string",
        ...		    "civil_status": "string",
        ...		    "email": "string",
        ...		    "primary_mobile_number": "string",
        ...		    "secondary_mobile_number": "string",
        ...		    "tertiary_mobile_number": "string",
        ...		    "telephone_number": "string",
        ...		    "payroll_card_number": "string",
        ...         "address": {
        ...             "current_address": {
        ...		            "citizen_association": "string",
        ...		            "neighbourhood_association": "string",
        ...		            "address": "string",
        ...		            "commune": "string",
        ...		            "district": "string",
        ...		            "city": "string",
        ...		            "province": "string",
        ...		            "postal_code": "string",
        ...		            "country": "string",
        ...		            "landmark": "string",
        ...		            "longitude": "string",
        ...		            "latitude": "string",
        ...		            "address_local": "string",
        ...		            "commune_local": "string",
        ...		            "district_local": "string",
        ...		            "city_local": "string",
        ...		            "province_local": "string",
        ...		            "postal_code_local": "string",
        ...		            "country_local": "string"
        ...             },
        ...             "permanent_address": {
        ...		            "citizen_association": "string",
        ...		            "neighbourhood_association": "string",
        ...		            "address": "string",
        ...		            "commune": "string",
        ...		            "district": "string",
        ...		            "city": "string",
        ...		            "province": "string",
        ...		            "postal_code": "string",
        ...		            "country": "string",
        ...		            "landmark": "string",
        ...		            "longitude": "string",
        ...		            "latitude": "string",
        ...		            "address_local": "string",
        ...		            "commune_local": "string",
        ...		            "district_local": "string",
        ...		            "city_local": "string",
        ...		            "province_local": "string",
        ...		            "postal_code_local": "string",
        ...		            "country_local": "string"
        ...             }
        ...         },
        ...         "kyc": {
        ...             "primary_identity": {
        ...		            "type": "string",
        ...		            "status": "int",
        ...		            "identity_id": "string",
        ...		            "identity_id_local": "string",
        ...		            "place_of_issue": "string",
        ...		            "issue_date": "string",
        ...		            "expired_date": "string",
        ...		            "front_identity_url": "string",
        ...		            "back_identity_url": "string"
        ...             },
        ...             "secondary_identity": {
        ...		            "type": "string",
        ...		            "status": "int",
        ...		            "identity_id": "string",
        ...		            "identity_id_local": "string",
        ...		            "place_of_issue": "string",
        ...		            "issue_date": "string",
        ...		            "expired_date": "string",
        ...		            "front_identity_url": "string",
        ...		            "back_identity_url": "string"
        ...             },
        ...             "tertiary_identity": {
        ...		            "type": "string",
        ...		            "status": "int",
        ...		            "identity_id": "string",
        ...		            "identity_id_local": "string",
        ...		            "place_of_issue": "string",
        ...		            "issue_date": "string",
        ...		            "expired_date": "string",
        ...		            "front_identity_url": "string",
        ...		            "back_identity_url": "string"
        ...             },
        ...		        "level": "int",
        ...		        "remark": "string",
        ...		        "verify_by": "string",
        ...		        "verify_date": "string",
        ...		        "risk_level": "string"
        ...         },
        ...         "additional": {
        ...		        "profile_picture_url": "string",
        ...		        "tax_id_card_photo_url": "string",
        ...		        "field_1_name": "string",
        ...		        "field_1_value": "string",
        ...		        "field_2_name": "string",
        ...		        "field_2_value": "string",
        ...		        "field_3_name": "string",
        ...		        "field_3_value": "string",
        ...		        "field_4_name": "string",
        ...		        "field_4_value": "string",
        ...		        "field_5_name": "string",
        ...		        "field_5_value": "string",
        ...		        "supporting_file_1_url": "string",
        ...		        "supporting_file_2_url": "string",
        ...		        "supporting_file_3_url": "string",
        ...		        "supporting_file_4_url": "string",
        ...		        "supporting_file_5_url": "string"
        ...         }
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Customer][Prepare] - system user updates partial customer profile - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "is_testing_account": "boolean",
        ...		    "is_system_account": "boolean",
        ...		    "acquisition_source": "string",
        ...		    "customer_classification_id": "int",
        ...         "referrer_user_type": {
        ...             "id": "int",
        ...             "name": "string"
        ...         },
        ...         "referrer_user_id": "int",
        ...		    "beneficiary": "string",
        ...		    "mm_card_type_id": "int",
        ...		    "mm_card_level_id": "int",
        ...		    "mm_factory_card_number": "string",
        ...		    "is_require_otp": "boolean",
        ...		    "tin_number": "string",
        ...		    "tin_number_local": "string",
        ...		    "title": "string",
        ...		    "title_local": "string",
        ...		    "first_name": "string",
        ...		    "first_name_local": "string",
        ...		    "middle_name": "string",
        ...		    "middle_name_local": "string",
        ...		    "last_name": "string",
        ...		    "last_name_local": "string",
        ...		    "suffix": "string",
        ...		    "suffix_local": "string",
        ...		    "date_of_birth": "string",
        ...		    "place_of_birth": "string",
        ...		    "place_of_birth_local": "string",
        ...		    "gender": "string",
        ...		    "gender_local": "string",
        ...		    "ethnicity": "string",
        ...		    "employer": "string",
        ...		    "nationality": "string",
        ...		    "occupation": "string",
        ...		    "occupation_local": "string",
        ...		    "occupation_title": "string",
        ...		    "occupation_title_local": "string",
        ...		    "township_code": "string",
        ...		    "township_name": "string",
        ...		    "township_name_local": "string",
        ...		    "mother_name": "string",
        ...		    "mother_name_local": "string",
        ...		    "mother_maiden_name": "string",
        ...		    "mother_maiden_name_local": "string",
        ...		    "civil_status": "string",
        ...		    "email": "string",
        ...		    "primary_mobile_number": "string",
        ...		    "secondary_mobile_number": "string",
        ...		    "tertiary_mobile_number": "string",
        ...		    "telephone_number": "string",
        ...         "address": {
        ...             "current_address": {
        ...		            "citizen_association": "string",
        ...		            "neighbourhood_association": "string",
        ...		            "address": "string",
        ...		            "commune": "string",
        ...		            "district": "string",
        ...		            "city": "string",
        ...		            "province": "string",
        ...		            "postal_code": "string",
        ...		            "country": "string",
        ...		            "landmark": "string",
        ...		            "longitude": "string",
        ...		            "latitude": "string",
        ...		            "address_local": "string",
        ...		            "commune_local": "string",
        ...		            "district_local": "string",
        ...		            "city_local": "string",
        ...		            "province_local": "string",
        ...		            "postal_code_local": "string",
        ...		            "country_local": "string"
        ...             },
        ...             "permanent_address": {
        ...		            "citizen_association": "string",
        ...		            "neighbourhood_association": "string",
        ...		            "address": "string",
        ...		            "commune": "string",
        ...		            "district": "string",
        ...		            "city": "string",
        ...		            "province": "string",
        ...		            "postal_code": "string",
        ...		            "country": "string",
        ...		            "landmark": "string",
        ...		            "longitude": "string",
        ...		            "latitude": "string",
        ...		            "address_local": "string",
        ...		            "commune_local": "string",
        ...		            "district_local": "string",
        ...		            "city_local": "string",
        ...		            "province_local": "string",
        ...		            "postal_code_local": "string",
        ...		            "country_local": "string"
        ...             }
        ...         },
        ...         "kyc": {
        ...             "primary_identity": {
        ...		            "type": "string",
        ...		            "status": "int",
        ...		            "identity_id": "string",
        ...		            "identity_id_local": "string",
        ...		            "place_of_issue": "string",
        ...		            "issue_date": "string",
        ...		            "expired_date": "string",
        ...		            "front_identity_url": "string",
        ...		            "back_identity_url": "string",
        ...		            "signature_url": "string"
        ...             },
        ...             "secondary_identity": {
        ...		            "type": "string",
        ...		            "status": "int",
        ...		            "identity_id": "string",
        ...		            "identity_id_local": "string",
        ...		            "place_of_issue": "string",
        ...		            "issue_date": "string",
        ...		            "expired_date": "string",
        ...		            "front_identity_url": "string",
        ...		            "back_identity_url": "string",
        ...		            "signature_url": "string"
        ...             },
        ...             "tertiary_identity": {
        ...		            "type": "string",
        ...		            "status": "int",
        ...		            "identity_id": "string",
        ...		            "identity_id_local": "string",
        ...		            "place_of_issue": "string",
        ...		            "issue_date": "string",
        ...		            "expired_date": "string",
        ...		            "front_identity_url": "string",
        ...		            "back_identity_url": "string",
        ...		            "signature_url": "string"
        ...             },
        ...		        "level": "int",
        ...		        "remark": "string",
        ...		        "verify_by": "string",
        ...		        "verify_date": "string",
        ...		        "risk_level": "string"
        ...         },
        ...         "additional": {
        ...		        "profile_picture_url": "string",
        ...		        "tax_id_card_photo_url": "string",
        ...		        "field_1_name": "string",
        ...		        "field_1_value": "string",
        ...		        "field_2_name": "string",
        ...		        "field_2_value": "string",
        ...		        "field_3_name": "string",
        ...		        "field_3_value": "string",
        ...		        "field_4_name": "string",
        ...		        "field_4_value": "string",
        ...		        "field_5_name": "string",
        ...		        "field_5_value": "string",
        ...		        "supporting_file_1_url": "string",
        ...		        "supporting_file_2_url": "string",
        ...		        "supporting_file_3_url": "string",
        ...		        "supporting_file_4_url": "string",
        ...		        "supporting_file_5_url": "string"
        ...         }
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Customer][Prepare] - customer updates full customer profile - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...         "referrer_user_type": {
        ...             "id": "int",
        ...             "name": "string"
        ...         },
        ...         "referrer_user_id": "int",
        ...		    "unique_reference": "string",
        ...		    "beneficiary": "string",
        ...		    "mm_card_type_id": "int",
        ...		    "mm_card_level_id": "int",
        ...		    "mm_factory_card_number": "string",
        ...		    "is_require_otp": "boolean",
        ...		    "tin_number": "string",
        ...		    "tin_number_local": "string",
        ...		    "title": "string",
        ...		    "title_local": "string",
        ...		    "first_name": "string",
        ...		    "first_name_local": "string",
        ...		    "middle_name": "string",
        ...		    "middle_name_local": "string",
        ...		    "last_name": "string",
        ...		    "last_name_local": "string",
        ...		    "suffix": "string",
        ...		    "suffix_local": "string",
        ...		    "date_of_birth": "string",
        ...		    "place_of_birth": "string",
        ...		    "place_of_birth_local": "string",
        ...		    "gender": "string",
        ...		    "gender_local": "string",
        ...		    "ethnicity": "string",
        ...		    "employer": "string",
        ...		    "nationality": "string",
        ...		    "occupation": "string",
        ...		    "occupation_local": "string",
        ...		    "occupation_title": "string",
        ...		    "occupation_title_local": "string",
        ...		    "township_code": "string",
        ...		    "township_name": "string",
        ...		    "township_name_local": "string",
        ...		    "mother_name": "string",
        ...		    "mother_name_local": "string",
        ...		    "mother_maiden_name": "string",
        ...		    "mother_maiden_name_local": "string",
        ...		    "civil_status": "string",
        ...		    "email": "string",
        ...		    "primary_mobile_number": "string",
        ...		    "secondary_mobile_number": "string",
        ...		    "tertiary_mobile_number": "string",
        ...		    "telephone_number": "string",
        ...         "address": {
        ...             "current_address": {
        ...		            "citizen_association": "string",
        ...		            "neighbourhood_association": "string",
        ...		            "address": "string",
        ...		            "commune": "string",
        ...		            "district": "string",
        ...		            "city": "string",
        ...		            "province": "string",
        ...		            "postal_code": "string",
        ...		            "country": "string",
        ...		            "landmark": "string",
        ...		            "longitude": "string",
        ...		            "latitude": "string",
        ...		            "address_local": "string",
        ...		            "commune_local": "string",
        ...		            "district_local": "string",
        ...		            "city_local": "string",
        ...		            "province_local": "string",
        ...		            "postal_code_local": "string",
        ...		            "country_local": "string"
        ...             },
        ...             "permanent_address": {
        ...		            "citizen_association": "string",
        ...		            "neighbourhood_association": "string",
        ...		            "address": "string",
        ...		            "commune": "string",
        ...		            "district": "string",
        ...		            "city": "string",
        ...		            "province": "string",
        ...		            "postal_code": "string",
        ...		            "country": "string",
        ...		            "landmark": "string",
        ...		            "longitude": "string",
        ...		            "latitude": "string",
        ...		            "address_local": "string",
        ...		            "commune_local": "string",
        ...		            "district_local": "string",
        ...		            "city_local": "string",
        ...		            "province_local": "string",
        ...		            "postal_code_local": "string",
        ...		            "country_local": "string"
        ...             }
        ...         },
        ...         "kyc": {
        ...             "primary_identity": {
        ...		            "type": "string",
        ...		            "identity_id": "string",
        ...		            "identity_id_local": "string",
        ...		            "place_of_issue": "string",
        ...		            "issue_date": "string",
        ...		            "expired_date": "string",
        ...		            "front_identity_url": "string",
        ...		            "back_identity_url": "string",
        ...		            "signature_url": "string"
        ...             },
        ...             "secondary_identity": {
        ...		            "type": "string",
        ...		            "identity_id": "string",
        ...		            "identity_id_local": "string",
        ...		            "place_of_issue": "string",
        ...		            "issue_date": "string",
        ...		            "expired_date": "string",
        ...		            "front_identity_url": "string",
        ...		            "back_identity_url": "string",
        ...		            "signature_url": "string"
        ...             },
        ...             "tertiary_identity": {
        ...		            "type": "string",
        ...		            "identity_id": "string",
        ...		            "identity_id_local": "string",
        ...		            "place_of_issue": "string",
        ...		            "issue_date": "string",
        ...		            "expired_date": "string",
        ...		            "front_identity_url": "string",
        ...		            "back_identity_url": "string",
        ...		            "signature_url": "string"
        ...             }
        ...         },
        ...         "additional": {
        ...		        "profile_picture_url": "string",
        ...		        "tax_id_card_photo_url": "string"
        ...         }
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Customer][Prepare] - customer updates partial customer profile - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...         "referrer_user_type": {
        ...             "id": "int",
        ...             "name": "string"
        ...         },
        ...         "referrer_user_id": "int",
        ...		    "beneficiary": "string",
        ...		    "mm_card_type_id": "int",
        ...		    "mm_card_level_id": "int",
        ...		    "mm_factory_card_number": "string",
        ...		    "is_require_otp": "boolean",
        ...		    "tin_number": "string",
        ...		    "tin_number_local": "string",
        ...		    "title": "string",
        ...		    "title_local": "string",
        ...		    "first_name": "string",
        ...		    "first_name_local": "string",
        ...		    "middle_name": "string",
        ...		    "middle_name_local": "string",
        ...		    "last_name": "string",
        ...		    "last_name_local": "string",
        ...		    "suffix": "string",
        ...		    "suffix_local": "string",
        ...		    "date_of_birth": "string",
        ...		    "place_of_birth": "string",
        ...		    "place_of_birth_local": "string",
        ...		    "gender": "string",
        ...		    "gender_local": "string",
        ...		    "ethnicity": "string",
        ...		    "employer": "string",
        ...		    "nationality": "string",
        ...		    "occupation": "string",
        ...		    "occupation_local": "string",
        ...		    "occupation_title": "string",
        ...		    "occupation_title_local": "string",
        ...		    "township_code": "string",
        ...		    "township_name": "string",
        ...		    "township_name_local": "string",
        ...		    "mother_name": "string",
        ...		    "mother_name_local": "string",
        ...		    "mother_maiden_name": "string",
        ...		    "mother_maiden_name_local": "string",
        ...		    "civil_status": "string",
        ...		    "email": "string",
        ...		    "primary_mobile_number": "string",
        ...		    "secondary_mobile_number": "string",
        ...		    "tertiary_mobile_number": "string",
        ...		    "telephone_number": "string",
        ...         "address": {
        ...             "current_address": {
        ...		            "citizen_association": "string",
        ...		            "neighbourhood_association": "string",
        ...		            "address": "string",
        ...		            "commune": "string",
        ...		            "district": "string",
        ...		            "city": "string",
        ...		            "province": "string",
        ...		            "postal_code": "string",
        ...		            "country": "string",
        ...		            "landmark": "string",
        ...		            "longitude": "string",
        ...		            "latitude": "string",
        ...		            "address_local": "string",
        ...		            "commune_local": "string",
        ...		            "district_local": "string",
        ...		            "city_local": "string",
        ...		            "province_local": "string",
        ...		            "postal_code_local": "string",
        ...		            "country_local": "string"
        ...             },
        ...             "permanent_address": {
        ...		            "citizen_association": "string",
        ...		            "neighbourhood_association": "string",
        ...		            "address": "string",
        ...		            "commune": "string",
        ...		            "district": "string",
        ...		            "city": "string",
        ...		            "province": "string",
        ...		            "postal_code": "string",
        ...		            "country": "string",
        ...		            "landmark": "string",
        ...		            "longitude": "string",
        ...		            "latitude": "string",
        ...		            "address_local": "string",
        ...		            "commune_local": "string",
        ...		            "district_local": "string",
        ...		            "city_local": "string",
        ...		            "province_local": "string",
        ...		            "postal_code_local": "string",
        ...		            "country_local": "string"
        ...             }
        ...         },
        ...         "kyc": {
        ...             "primary_identity": {
        ...		            "type": "string",
        ...		            "identity_id": "string",
        ...		            "identity_id_local": "string",
        ...		            "place_of_issue": "string",
        ...		            "issue_date": "string",
        ...		            "expired_date": "string",
        ...		            "front_identity_url": "string",
        ...		            "back_identity_url": "string",
        ...		            "signature_url": "string"
        ...             },
        ...             "secondary_identity": {
        ...		            "type": "string",
        ...		            "identity_id": "string",
        ...		            "identity_id_local": "string",
        ...		            "place_of_issue": "string",
        ...		            "issue_date": "string",
        ...		            "expired_date": "string",
        ...		            "front_identity_url": "string",
        ...		            "back_identity_url": "string",
        ...		            "signature_url": "string"
        ...             },
        ...             "tertiary_identity": {
        ...		            "type": "string",
        ...		            "identity_id": "string",
        ...		            "identity_id_local": "string",
        ...		            "place_of_issue": "string",
        ...		            "issue_date": "string",
        ...		            "expired_date": "string",
        ...		            "front_identity_url": "string",
        ...		            "back_identity_url": "string",
        ...		            "signature_url": "string"
        ...             }
        ...         },
        ...         "additional": {
        ...		        "profile_picture_url": "string",
        ...		        "tax_id_card_photo_url": "string"
        ...         }
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Customer][Prepare] - update customer kyc - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...		    "level": "int",
        ...		    "remark": "string",
        ...		    "verify_by": "string",
        ...		    "verify_date": "string",
        ...		    "risk_level": "string"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Customer][Prepare] - update customer password - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...		    "old_password": "string",
        ...		    "new_password": "string"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Customer][Prepare] - update customer identity - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...		    "username": "string"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Customer][Prepare] - customer creates device - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...		    "channel_type_id": "int",
        ...		    "channel_id": "int",
        ...		    "shop_id": "int",
        ...		    "mac_address": "string",
        ...		    "network_provider_name": "string",
        ...		    "public_ip_address": "string",
        ...		    "supporting_file_1": "string",
        ...		    "supporting_file_2": "string",
        ...		    "device_name": "string",
        ...		    "device_model": "string",
        ...		    "device_unique_reference": "string",
        ...		    "os": "string",
        ...		    "os_version": "string",
        ...		    "display_size_in_inches": "string",
        ...		    "pixel_counts": "string",
        ...		    "unique_number": "string",
        ...		    "serial_number": "string",
        ...		    "edc_serial_number": "string",
        ...		    "edc_model": "string",
        ...		    "app_version": "string",
        ...		    "edc_firmware_version": "string",
        ...		    "edc_software_version": "string",
        ...		    "edc_sim_card_number": "string",
        ...		    "edc_battery_serial_number": "string",
        ...		    "edc_adapter_serial_number": "string",
        ...		    "edc_smartcard_1_number": "string",
        ...		    "edc_smartcard_2_number": "string",
        ...		    "edc_smartcard_3_number": "string",
        ...		    "edc_smartcard_4_number": "string",
        ...		    "edc_smartcard_5_number": "string",
        ...		    "pos_serial_number": "string",
        ...		    "pos_model": "string",
        ...		    "pos_firmware_version": "string",
        ...		    "pos_software_version": "string",
        ...		    "pos_smartcard_1_number": "string",
        ...		    "pos_smartcard_2_number": "string",
        ...		    "pos_smartcard_3_number": "string",
        ...		    "pos_smartcard_4_number": "string",
        ...		    "pos_smartcard_5_number": "string"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Customer][Prepare] - customer updates device - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...		    "channel_type_id": "int",
        ...		    "channel_id": "int",
        ...		    "shop_id": "int",
        ...		    "mac_address": "string",
        ...		    "network_provider_name": "string",
        ...		    "public_ip_address": "string",
        ...		    "supporting_file_1": "string",
        ...		    "supporting_file_2": "string",
        ...		    "device_name": "string",
        ...		    "device_model": "string",
        ...		    "device_unique_reference": "string",
        ...		    "os": "string",
        ...		    "os_version": "string",
        ...		    "display_size_in_inches": "string",
        ...		    "pixel_counts": "string",
        ...		    "unique_number": "string",
        ...		    "serial_number": "string",
        ...		    "app_version": "string",
        ...		    "edc_serial_number": "string",
        ...		    "edc_model": "string",
        ...		    "edc_firmware_version": "string",
        ...		    "edc_software_version": "string",
        ...		    "edc_sim_card_number": "string",
        ...		    "edc_battery_serial_number": "string",
        ...		    "edc_adapter_serial_number": "string",
        ...		    "edc_smartcard_1_number": "string",
        ...		    "edc_smartcard_2_number": "string",
        ...		    "edc_smartcard_3_number": "string",
        ...		    "edc_smartcard_4_number": "string",
        ...		    "edc_smartcard_5_number": "string",
        ...		    "pos_serial_number": "string",
        ...		    "pos_model": "string",
        ...		    "pos_firmware_version": "string",
        ...		    "pos_software_version": "string",
        ...		    "pos_smartcard_1_number": "string",
        ...		    "pos_smartcard_2_number": "string",
        ...		    "pos_smartcard_3_number": "string",
        ...		    "pos_smartcard_4_number": "string",
        ...		    "pos_smartcard_5_number": "string"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Customer][Prepare] - system user updates customer device - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...		    "channel_type_id": "int",
        ...		    "channel_id": "int",
        ...		    "shop_id": "int",
        ...		    "mac_address": "string",
        ...		    "network_provider_name": "string",
        ...		    "public_ip_address": "string",
        ...		    "supporting_file_1": "string",
        ...		    "supporting_file_2": "string",
        ...		    "device_name": "string",
        ...		    "device_model": "string",
        ...		    "device_unique_reference": "string",
        ...		    "os": "string",
        ...		    "os_version": "string",
        ...		    "display_size_in_inches": "string",
        ...		    "pixel_counts": "string",
        ...		    "unique_number": "string",
        ...		    "serial_number": "string",
        ...		    "app_version": "string",
        ...		    "edc_serial_number": "string",
        ...		    "edc_model": "string",
        ...		    "edc_firmware_version": "string",
        ...		    "edc_software_version": "string",
        ...		    "edc_sim_card_number": "string",
        ...		    "edc_battery_serial_number": "string",
        ...		    "edc_adapter_serial_number": "string",
        ...		    "edc_smartcard_1_number": "string",
        ...		    "edc_smartcard_2_number": "string",
        ...		    "edc_smartcard_3_number": "string",
        ...		    "edc_smartcard_4_number": "string",
        ...		    "edc_smartcard_5_number": "string",
        ...		    "pos_serial_number": "string",
        ...		    "pos_model": "string",
        ...		    "pos_firmware_version": "string",
        ...		    "pos_software_version": "string",
        ...		    "pos_smartcard_1_number": "string",
        ...		    "pos_smartcard_2_number": "string",
        ...		    "pos_smartcard_3_number": "string",
        ...		    "pos_smartcard_4_number": "string",
        ...		    "pos_smartcard_5_number": "string"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Customer][Prepare] - system user updates customer device status - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...		    "is_active": "bool"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}