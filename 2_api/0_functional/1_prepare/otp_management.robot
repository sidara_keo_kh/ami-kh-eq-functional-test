*** Settings ***
Resource        ../../../1_common/keywords.robot


*** Keywords ***
[OTP_Management][Prepare] - generate OTP - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "user_type": "string",
        ...		    "user_id": "int",
        ...		    "delivery_channel": "string",
        ...		    "email": "string",
        ...		    "mobile_number": "string",
        ...		    "is_generate_ref_code": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[OTP_Management][Prepare] - validate OTP - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "otp_reference_id": "string",
        ...		    "otp_code": "string",
        ...		    "user_reference_code": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[OTP_Management][Prepare] - regenerate OTP - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...    {
        ...    	    "otp_reference_id": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}