*** Settings ***
Resource        ../../../1_common/keywords.robot


*** Keywords ***
[System_User][Prepare] - Create system user - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...          {
        ...                 "username": "string",
        ...                 "password": "string",
        ...                 "email": "string random_string(10, '${LETTERS}')",
        ...                 "mobile_number": "string random_string(10, '${NUMBERS}')",
        ...                 "is_external": "boolean",
        ...                 "unique_reference": "string",
        ...                 "title": "string random_string(10, '${LETTERS}')",
        ...                 "firstname": "string random_string(10, '${LETTERS}')",
        ...                 "middle_name": "string random_string(10, '${LETTERS}')",
        ...                 "lastname": "string random_string(10, '${LETTERS}')",
        ...                 "suffix": "string random_string(10, '${LETTERS}')",
        ...                 "date_of_birth": "string",
        ...                 "place_of_birth": "string random_string(10, '${LETTERS}')",
        ...                 "nationality": "string random_string(10, '${LETTERS}')",
        ...                 "occupation": "string random_string(10, '${LETTERS}')",
        ...                 "occupation_title": "string random_string(10, '${LETTERS}')",
        ...                 "source_of_funds": "string",
        ...                 "national_id_number": "string random_string(10, '${NUMBERS}')",
        ...                 "address": {
        ...                     "current_address": {
        ...      					"citizen_association": "string random_string(10, '${LETTERS}')",
        ...      					"neighbourhood_association": "string random_string(10, '${LETTERS}')",
        ...      					"address": "string random_string(10, '${LETTERS}')",
        ...      					"commune": "string random_string(10, '${LETTERS}')",
        ...      					"district": "string random_string(10, '${LETTERS}')",
        ...      					"city": "string random_string(10, '${LETTERS}')",
        ...      					"province": "string random_string(10, '${LETTERS}')",
        ...      					"postal_code": "string random_string(5, '${LETTERS}')",
        ...      					"country": "string random_string(10, '${LETTERS}')",
        ...      					"landmark": "string random_string(10, '${LETTERS}')",
        ...      					"longitude": "string random_decimal(1, 10, 5)",
        ...      					"latitude": "string random_decimal(1, 10, 5)"
        ...                     },
        ...                     "permanent_address": {
        ...      					"citizen_association": "string random_string(10, '${LETTERS}')",
        ...      					"neighbourhood_association": "string random_string(10, '${LETTERS}')",
        ...      					"address": "string random_string(10, '${LETTERS}')",
        ...      					"commune": "string random_string(10, '${LETTERS}')",
        ...      					"district": "string random_string(10, '${LETTERS}')",
        ...      					"city": "string random_string(10, '${LETTERS}')",
        ...      					"province": "string random_string(10, '${LETTERS}')",
        ...      					"postal_code": "string random_string(5, '${LETTERS}')",
        ...      					"country": "string random_string(10, '${LETTERS}')",
        ...      					"landmark": "string random_string(10, '${LETTERS}')",
        ...      					"longitude": "string random_decimal(1, 10, 5)",
        ...      					"latitude": "string random_decimal(1, 10, 5)"
        ...                     }
        ...                 },
        ...                 "additional": {
        ...      				"field_1_name": "string random_string(10, '${LETTERS}')",
        ...      				"field_1_value": "string random_string(10, '${LETTERS}')",
        ...      				"field_2_name": "string random_string(10, '${LETTERS}')",
        ...      				"field_2_value": "string random_string(10, '${LETTERS}')",
        ...      				"field_3_name": "string random_string(10, '${LETTERS}')",
        ...      				"field_3_value": "string random_string(10, '${LETTERS}')",
        ...      				"field_4_name": "string random_string(10, '${LETTERS}')",
        ...      				"field_4_value": "string random_string(10, '${LETTERS}')",
        ...      				"field_5_name": "string random_string(10, '${LETTERS}')",
        ...      				"field_5_value": "string random_string(10, '${LETTERS}')",
        ...      				"supporting_file_1_url": "string random_string(10, '${LETTERS}')",
        ...      				"supporting_file_2_url": "string random_string(10, '${LETTERS}')",
        ...      				"supporting_file_3_url": "string random_string(10, '${LETTERS}')",
        ...      				"supporting_file_4_url": "string random_string(10, '${LETTERS}')",
        ...      				"supporting_file_5_url": "string random_string(10, '${LETTERS}')"
        ...                 }
        ...          }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[System_User][Prepare] - Add permissions to role - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...		    "permissions": [ "integer" ]
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[System_User][Prepare] - Create role - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...		    "name": "string random_string(10, '${LETTERS}')",
        ...         "description": "string random_string(10, '${LETTERS}')"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[System_User][Prepare] - Update system user - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...    {
        ...    	    "username": "string",
        ...    	    "password": "string",
        ...    	    "firstname": "string random_string(10, '${LETTERS}')",
        ...    	    "lastname": "string random_string(10, '${LETTERS}')",
        ...    	    "email": "string random_string(10, '${LETTERS}')",
        ...    	    "mobile_number": "string random_string(10, '${NUMBERS}')"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[System_User][Prepare] - Update system user password - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...    {
        ...    	    "password": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[System_User][Prepare] - Change system user password - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...    {
        ...    	    "old_password": "string",
        ...    	    "new_password": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[System_User][Prepare] - Create permission - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...    {
        ...    	    "name": "string random_string(10, '${LETTERS}')",
        ...    	    "is_page_level": "boolean",
        ...         "description": "string random_string(10, '${LETTERS}')"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[System_User][Prepare] - Update permission - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...    {
        ...    	    "name": "string random_string(10, '${LETTERS}')",
        ...    	    "is_page_level": "boolean",
        ...         "description": "string random_string(10, '${LETTERS}')"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[System_User][Prepare] - Remove permission from role - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...		    "permissions": [ "integer" ]
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[System_User][Prepare] - Update role - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...		    "name": "string random_string(10, '${LETTERS}')",
        ...         "description": "string random_string(10, '${LETTERS}')"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[System_User][Prepare] - Add user to role - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...    {
        ...    	    "user_id": "integer"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[System_User][Prepare] - Unlink role from user - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...    {
        ...    	    "role_id": "integer"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[System_User][Prepare] - Link system user and company profile - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...         "company_id": "integer"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[System_User][Prepare] - Active suspend system user - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...    {
        ...    	    "is_suspended": "boolean",
        ...    	    "suspend_reason": "string random_string(10, '${LETTERS}')"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}