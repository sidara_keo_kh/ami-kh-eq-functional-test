*** Settings ***
Resource        ../../../1_common/keywords.robot


*** Keywords ***
[Sof_Bank][Prepare] - create bank - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...     {
        ...		    "name": "string random_string(15, '${LETTERS}')",
        ...		    "bin": "string random_string(10, '${NUMBERS}')",
        ...		    "description": "string",
        ...		    "debit_url": "string random_enum('${bank_debit_url}')",
        ...		    "credit_url": "string random_enum('${bank_credit_url}')",
        ...		    "bank_account_number": "string random_string(10, '${NUMBERS}')",
        ...		    "bank_account_name": "string random_string(10, '${LETTERS}')",
        ...		    "currency": "string random_enum('USD')",
        ...		    "cancel_url": "string 'http://\%s' % random_string(10, '${LETTERS}')",
        ...		    "check_status_url": "string 'http://\%s' % random_string(10, '${LETTERS}')",
        ...		    "pre_sof_url": "string",
        ...		    "pre_sof_read_timeout": "int",
        ...		    "read_timeout": "int random_string(4, '${NUMBERS}')"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Sof_Bank][Prepare] - update bank - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "name": "string",
        ...		    "bin": "string",
        ...		    "description": "string",
        ...		    "debit_url": "string",
        ...		    "credit_url": "string",
        ...		    "bank_account_number": "string",
        ...		    "bank_account_name": "string",
        ...		    "currency": "string",
        ...		    "cancel_url": "string",
        ...		    "check_status_url": "string",
        ...		    "pre_sof_url": "string",
        ...		    "pre_sof_read_timeout": "int",
        ...		    "read_timeout": "int",
        ...		    "is_active": "boolean"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}