*** Settings ***
Resource        ../../../1_common/keywords.robot

*** Keywords ***
[Agent][Prepare] - create agent type - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_name}=  [Common] - Create string param in dic    ${arg_dic}   name
    ${param_description}=  [Common] - Create string param in dic    ${arg_dic}   description
    ${param_sale}=  [Common] - Create boolean param in dic  ${arg_dic}  is_sale
    ${param_is_manager}=   [Common] - Create boolean param in dic  ${arg_dic}  is_manager
    ${param_ref1}=  [Common] - Create string param in dic   ${arg_dic}  reference_1
    ${param_ref2}=  [Common] - Create string param in dic   ${arg_dic}  reference_2
    ${param_ref3}=  [Common] - Create string param in dic   ${arg_dic}  reference_3
    ${param_ref4}=  [Common] - Create string param in dic   ${arg_dic}  reference_4
    ${param_ref5}=  [Common] - Create string param in dic   ${arg_dic}  reference_5

    ${body}              catenate           SEPARATOR=
        ...	{
        ...		${param_name}
        ...		${param_description}
        ...     ${param_sale}
        ...		${param_is_manager}
        ...		${param_ref1}
        ...		${param_ref2}
        ...		${param_ref3}
        ...		${param_ref4}
        ...		${param_ref5}

        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - update agent type - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_name}=  [Common] - Create string param in dic    ${arg_dic}   name
    ${param_description}=  [Common] - Create string param in dic    ${arg_dic}   description
    ${param_is_manager}=    [Common] - Create boolean param in dic  ${arg_dic}  is_manager
    ${param_ref1}=  [Common] - Create string param in dic   ${arg_dic}  reference_1
    ${param_ref2}=  [Common] - Create string param in dic   ${arg_dic}  reference_2
    ${param_ref3}=  [Common] - Create string param in dic   ${arg_dic}  reference_3
    ${param_ref4}=  [Common] - Create string param in dic   ${arg_dic}  reference_4
    ${param_ref5}=  [Common] - Create string param in dic   ${arg_dic}  reference_5
    ${body}              catenate           SEPARATOR=
        ...	{
        ...		${param_name}
        ...		${param_description}
        ...		${param_is_manager}
        ...		${param_ref1}
        ...		${param_ref2}
        ...		${param_ref3}
        ...		${param_ref4}
        ...		${param_ref5}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - create agent classification - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_name}=  [Common] - Create string param in dic    ${arg_dic}   name
    ${param_description}=  [Common] - Create string param in dic    ${arg_dic}   description
    ${body}              catenate           SEPARATOR=
        ...	{
        ...		${param_name}
        ...		${param_description}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - update agent classification - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_name}=  [Common] - Create string param in dic    ${arg_dic}   name
    ${param_description}=  [Common] - Create string param in dic    ${arg_dic}   description
    ${body}              catenate           SEPARATOR=
        ...	{
        ...		${param_name}
        ...		${param_description}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - update agent accreditation - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_status_id}=  [Common] - Create int param in dic    ${arg_dic}   status_id
    ${param_remark}=  [Common] - Create string param in dic    ${arg_dic}   remark
    ${param_risk_level}=  [Common] - Create string param in dic    ${arg_dic}   risk_level
    ${param_verify_by}=  [Common] - Create string param in dic    ${arg_dic}   verify_by
    ${param_verify_date}=  [Common] - Create string param in dic    ${arg_dic}   verify_date
    ${body}              catenate           SEPARATOR=
        ...	{
        ...		${param_status_id}
        ...		${param_remark}
        ...		${param_risk_level}
        ...		${param_verify_by}
        ...		${param_verify_date}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - admin updates full agent profiles - body
    [Arguments]     ${output}=body    ${remove_null}=${True}   ${remove_empty}=${True}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
    ...    {
    ...      "is_testing_account": "bool",
    ...      "is_system_account": "bool",
    ...      "acquisition_source": "string",
    ...      "referrer_user_type": {
    ...        "id": "int",
    ...        "name": "string"
    ...      },
    ...      "referrer_user_id": "int",
    ...      "company_id": "int",
    ...      "agent_type_id": "int",
    ...      "unique_reference": "string",
    ...      "mm_card_type_id": "int",
    ...      "mm_card_level_id": "int",
    ...      "mm_factory_card_number": "string",
    ...      "model_type": "string",
    ...      "is_require_otp": "bool",
    ...      "agent_classification_id": "int",
    ...      "tin_number": "string",
    ...      "title": "string",
    ...      "first_name": "string",
    ...      "middle_name": "string",
    ...      "last_name": "string",
    ...      "suffix": "string",
    ...      "date_of_birth": "string",
    ...      "place_of_birth": "string",
    ...      "gender": "string",
    ...      "ethnicity": "string",
    ...      "nationality": "string",
    ...      "occupation": "string",
    ...      "occupation_title": "string",
    ...      "township_code": "string",
    ...      "township_name": "string",
    ...      "national_id_number": "string",
    ...      "mother_name": "string",
    ...      "email": "string",
    ...      "primary_mobile_number": "string",
    ...      "secondary_mobile_number": "string",
    ...      "tertiary_mobile_number": "string",
    ...      "tin_number_local": "string",
    ...      "title_local": "string",
    ...      "first_name_local": "string",
    ...      "middle_name_local": "string",
    ...      "last_name_local": "string",
    ...      "suffix_local": "string",
    ...      "place_of_birth_local": "string",
    ...      "gender_local": "string",
    ...      "occupation_local": "string",
    ...      "occupation_title_local": "string",
    ...      "township_name_local": "string",
    ...      "national_id_number_local": "string",
    ...      "mother_name_local": "string",
    ...      "star_rating": "int",
    ...      "warning_count": "int",
    ...      "sale": {
    ...        "employee_id": "string",
    ...        "calendar_id": "string"
    ...      },
    ...      "address": {
    ...        "current_address": {
    ...          "citizen_association": "string",
    ...          "neighbourhood_association": "string",
    ...          "address": "string",
    ...          "city": "string",
    ...          "postal_code": "string",
    ...          "province": "string",
    ...          "district": "string",
    ...          "commune": "string",
    ...          "country": "string",
    ...          "landmark": "string",
    ...          "latitude": "string",
    ...          "longitude": "string",
    ...          "address_local": "string",
    ...          "commune_local": "string",
    ...          "district_local": "string",
    ...          "city_local": "string",
    ...          "province_local": "string",
    ...          "postal_code_local": "string",
    ...          "country_local": "string"
    ...        },
    ...        "permanent_address": {
    ...          "citizen_association": "string",
    ...          "neighbourhood_association": "string",
    ...          "address": "string",
    ...          "city": "string",
    ...          "postal_code": "string",
    ...          "province": "string",
    ...          "district": "string",
    ...          "commune": "string",
    ...          "country": "string",
    ...          "landmark": "string",
    ...          "latitude": "string",
    ...          "longitude": "string",
    ...          "address_local": "string",
    ...          "commune_local": "string",
    ...          "district_local": "string",
    ...          "city_local": "string",
    ...          "province_local": "string",
    ...          "postal_code_local": "string",
    ...          "country_local": "string"
    ...        }
    ...      },
    ...      "bank": {
    ...        "name": "string",
    ...        "account_status": "int",
    ...        "account_name": "string",
    ...        "branch_city": "string",
    ...        "branch_area": "string",
    ...        "account_number": "string",
    ...        "register_date": "string",
    ...        "register_source": "string",
    ...        "is_verified": "bool",
    ...        "end_date": "string",
    ...        "name_local": "string",
    ...        "branch_area_local": "string",
    ...        "branch_city_local": "string"
    ...      },
    ...      "contract": {
    ...        "type": "string",
    ...        "number": "int",
    ...        "sign_date": "string",
    ...        "issue_date": "string",
    ...        "expired_date": "string",
    ...        "notification_alert": "string",
    ...        "extension_type": "string",
    ...        "day_of_period_reconciliation": "int",
    ...        "release": "string",
    ...        "file_url": "string",
    ...        "assessment_information_url": "string"
    ...      },
    ...      "accreditation": {
    ...        "primary_identity": {
    ...          "type": "string",
    ...          "status": "int",
    ...          "identity_id": "string",
    ...          "place_of_issue": "string",
    ...          "issue_date": "string",
    ...          "expired_date": "string",
    ...          "front_identity_url": "string",
    ...          "back_identity_url": "string",
    ...          "identity_id_local": "string"
    ...        },
    ...        "secondary_identity": {
    ...          "type": "string",
    ...          "status": "int",
    ...          "identity_id": "string",
    ...          "place_of_issue": "string",
    ...          "issue_date": "string",
    ...          "expired_date": "string",
    ...          "front_identity_url": "string",
    ...          "back_identity_url": "string",
    ...          "identity_id_local": "string"
    ...        },
    ...        "status_id": "int",
    ...        "verify_by": "string",
    ...        "verify_date": "string",
    ...        "remark": "string",
    ...        "risk_level": "string"
    ...      },
    ...      "additional": {
    ...        "acquiring_sale_executive_id": "string",
    ...        "acquiring_sale_executive_name": "string",
    ...        "relationship_manager_id": "string",
    ...        "relationship_manager_name": "string",
    ...        "sale_region": "string",
    ...        "commercial_account_manager": "string",
    ...        "profile_picture_url": "string",
    ...        "national_id_photo_url": "string",
    ...        "tax_id_card_photo_url": "string",
    ...        "field_1_name": "string",
    ...        "field_1_value": "string",
    ...        "field_2_name": "string",
    ...        "field_2_value": "string",
    ...        "field_3_name": "string",
    ...        "field_3_value": "string",
    ...        "field_4_name": "string",
    ...        "field_4_value": "string",
    ...        "field_5_name": "string",
    ...        "field_5_value": "string",
    ...        "supporting_file_1_url": "string",
    ...        "supporting_file_2_url": "string",
    ...        "supporting_file_3_url": "string",
    ...        "supporting_file_4_url": "string",
    ...        "supporting_file_5_url": "string",
    ...        "acquiring_sale_executive_name_local": "string",
    ...        "relationship_manager_name_local": "string",
    ...        "sale_region_local": "string",
    ...        "commercial_account_manager_local": "string"
    ...      }
    ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - admin updates partial agent profiles - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_is_testing_account}		[Common] - Create boolean param in dic    ${arg_dic}  	is_testing_account
    ${param_is_system_account}		[Common] - Create boolean param in dic    ${arg_dic}  	is_system_account
    ${param_acquisition_source}		[Common] - Create string param in dic    ${arg_dic}  	acquisition_source
    ${param_referrer_user_type_id}		[Common] - Create int param in dic    ${arg_dic}  	id
    ${param_referrer_user_type_name}		[Common] - Create string param in dic    ${arg_dic}  	name
    ${param_referrer_user_id}		[Common] - Create int param in dic    ${arg_dic}  	referrer_user_id
    ${param_agent_type_id}		[Common] - Create int param in dic    ${arg_dic}  	agent_type_id
    ${param_unique_reference}		[Common] - Create string param in dic    ${arg_dic}  	unique_reference
    ${param_company_id}             [Common] - Create int param in dic    ${arg_dic}  	company_id
    ${param_mm_card_type_id}		[Common] - Create int param in dic    ${arg_dic}  	mm_card_type_id
    ${param_mm_card_level_id}		[Common] - Create int param in dic    ${arg_dic}  	mm_card_level_id
    ${param_mm_factory_card_number}		[Common] - Create string param in dic    ${arg_dic}  	mm_factory_card_number
    ${param_model_type}		[Common] - Create string param in dic    ${arg_dic}  	model_type
    ${param_is_require_otp}		[Common] - Create boolean param in dic    ${arg_dic}  	is_require_otp
    ${param_agent_classification_id}		[Common] - Create int param in dic    ${arg_dic}  	agent_classification_id
    ${param_tin_number}		[Common] - Create string param in dic    ${arg_dic}  	tin_number
    ${param_title}		[Common] - Create string param in dic    ${arg_dic}  	title
    ${param_first_name}		[Common] - Create string param in dic    ${arg_dic}  	first_name
    ${param_middle_name}		[Common] - Create string param in dic    ${arg_dic}  	middle_name
    ${param_last_name}		[Common] - Create string param in dic    ${arg_dic}  	last_name
    ${param_suffix}		[Common] - Create string param in dic    ${arg_dic}  	suffix
    ${param_date_of_birth}		[Common] - Create string param in dic    ${arg_dic}  	date_of_birth
    ${param_place_of_birth}		[Common] - Create string param in dic    ${arg_dic}  	place_of_birth
    ${param_gender}		[Common] - Create string param in dic    ${arg_dic}  	gender
    ${param_ethnicity}		[Common] - Create string param in dic    ${arg_dic}  	ethnicity
    ${param_nationality}		[Common] - Create string param in dic    ${arg_dic}  	nationality
    ${param_occupation}		[Common] - Create string param in dic    ${arg_dic}  	occupation
    ${param_occupation_title}		[Common] - Create string param in dic    ${arg_dic}  	occupation_title
    ${param_township_code}		[Common] - Create string param in dic    ${arg_dic}  	township_code
    ${param_township_name}		[Common] - Create string param in dic    ${arg_dic}  	township_name
    ${param_national_id_number}		[Common] - Create string param in dic    ${arg_dic}  	national_id_number
    ${param_mother_name}		[Common] - Create string param in dic    ${arg_dic}  	mother_name
    ${param_email}		[Common] - Create string param in dic    ${arg_dic}  	email
    ${param_primary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}  	primary_mobile_number
    ${param_secondary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}  	secondary_mobile_number
    ${param_tertiary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}  	tertiary_mobile_number
    ${param_current_address_citizen_association}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     citizen_association
    ${param_current_address_neighbourhood_association}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     neighbourhood_association
    ${param_current_address_address}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     address
    ${param_current_address_commune}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     commune
    ${param_current_address_district}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     district
    ${param_current_address_city}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     city
    ${param_current_address_province}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     province
    ${param_current_address_postal_code}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     postal_code
    ${param_current_address_country}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     country
    ${param_current_address_landmark}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     landmark
    ${param_current_address_longitude}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     longitude
    ${param_current_address_latitude}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     latitude
    ${param_permanent_address_citizen_association}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     citizen_association
    ${param_permanent_address_neighbourhood_association}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     neighbourhood_association
    ${param_permanent_address_address}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     address
    ${param_permanent_address_commune}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     commune
    ${param_permanent_address_district}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     district
    ${param_permanent_address_city}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     city
    ${param_permanent_address_province}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     province
    ${param_permanent_address_postal_code}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     postal_code
    ${param_permanent_address_country}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     country
    ${param_permanent_address_landmark}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     landmark
    ${param_permanent_address_longitude}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     longitude
    ${param_permanent_address_latitude}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     latitude
    ${param_bank_name}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     name
    ${param_bank_account_status}		[Common] - Create int param of object in dic    ${arg_dic}  	bank     account_status
    ${param_bank_account_name}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     account_name
    ${param_bank_account_number}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     account_number
    ${param_bank_branch_area}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     branch_area
    ${param_bank_branch_city}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     branch_city
    ${param_bank_register_date}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     register_date
    ${param_bank_register_source}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     register_source
    ${param_bank_is_verified}		[Common] - Create boolean param of object in dic    ${arg_dic}  	bank     is_verified
    ${param_bank_end_date}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     end_date
    ${param_contract_type}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     type
    ${param_contract_number}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     number
    ${param_contract_extension_type}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     extension_type
    ${param_contract_sign_date}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     sign_date
    ${param_contract_issue_date}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     issue_date
    ${param_contract_expired_date}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     expired_date
    ${param_contract_notification_alert}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     notification_alert
    ${param_contract_day_of_period_reconciliation}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     day_of_period_reconciliation
    ${param_contract_release}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     release
    ${param_contract_file_url}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     file_url
    ${param_contract_assessment_information_url}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     assessment_information_url
    ${param_primary_identity_type}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     type
    ${param_primary_identity_status}		[Common] - Create int param of object in dic    ${arg_dic}  	primary_identity     status
    ${param_primary_identity_identity_id}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     identity_id
    ${param_primary_identity_place_of_issue}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     place_of_issue
    ${param_primary_identity_issue_date}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     issue_date
    ${param_primary_identity_expired_date}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     expired_date
    ${param_primary_identity_front_identity_url}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     front_identity_url
    ${param_primary_identity_back_identity_url}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     back_identity_url
    ${param_secondary_identity_type}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     type
    ${param_secondary_identity_status}		[Common] - Create int param of object in dic    ${arg_dic}  	secondary_identity     status
    ${param_secondary_identity_identity_id}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     identity_id
    ${param_secondary_identity_place_of_issue}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     place_of_issue
    ${param_secondary_identity_issue_date}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     issue_date
    ${param_secondary_identity_expired_date}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     expired_date
    ${param_secondary_identity_front_identity_url}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     front_identity_url
    ${param_secondary_identity_back_identity_url}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     back_identity_url
    ${param_status_id}		[Common] - Create int param in dic    ${arg_dic}  	status_id		
    ${param_remark}		[Common] - Create string param in dic    ${arg_dic}  	remark		
    ${param_verify_by}		[Common] - Create string param in dic    ${arg_dic}  	verify_by		
    ${param_verify_date}		[Common] - Create string param in dic    ${arg_dic}  	verify_date		
    ${param_risk_level}		[Common] - Create string param in dic    ${arg_dic}  	risk_level		
    ${param_additional_acquiring_sales_executive_id}		[Common] - Create int param of object in dic    ${arg_dic}  	additional     acquiring_sales_executive_id
    ${param_additional_acquiring_sales_executive_name}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     acquiring_sales_executive_name
    ${param_additional_relationship_manager_id}		[Common] - Create int param of object in dic    ${arg_dic}  	additional     relationship_manager_id
    ${param_additional_relationship_manager_name}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     relationship_manager_name
    ${param_additional_sale_region}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     sale_region
    ${param_additional_commercial_account_manager}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     commercial_account_manager
    ${param_additional_profile_picture_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     profile_picture_url
    ${param_additional_national_id_photo_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     national_id_photo_url
    ${param_additional_tax_id_card_photo_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     tax_id_card_photo_url
    ${param_additional_field_1_name}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_1_name
    ${param_additional_field_1_value}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_1_value
    ${param_additional_field_2_name}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_2_name
    ${param_additional_field_2_value}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_2_value
    ${param_additional_field_3_name}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_3_name
    ${param_additional_field_3_value}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_3_value
    ${param_additional_field_4_name}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_4_name
    ${param_additional_field_4_value}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_4_value
    ${param_additional_field_5_name}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_5_name
    ${param_additional_field_5_value}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_5_value
    ${param_additional_supporting_file_1_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     supporting_file_1_url
    ${param_additional_supporting_file_2_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     supporting_file_2_url
    ${param_additional_supporting_file_3_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     supporting_file_3_url
    ${param_additional_supporting_file_4_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     supporting_file_4_url
    ${param_additional_supporting_file_5_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     supporting_file_5_url
    ${param_tin_number_local}		[Common] - Create string param in dic    ${arg_dic}  	tin_number_local
    ${param_title_local}            [Common] - Create string param in dic    ${arg_dic}  	title_local
    ${param_first_name_local}       [Common] - Create string param in dic    ${arg_dic}  	first_name_local
    ${param_middle_name_local}      [Common] - Create string param in dic    ${arg_dic}  	middle_name_local		
    ${param_last_name_local}        [Common] - Create string param in dic    ${arg_dic}  	last_name_local		
    ${param_suffix_local}           [Common] - Create string param in dic    ${arg_dic}  	suffix_local		
    ${param_place_of_birth_local}   [Common] - Create string param in dic    ${arg_dic}  	place_of_birth_local		
    ${param_gender_local}           [Common] - Create string param in dic    ${arg_dic}  	gender_local		
    ${param_occupation_local}       [Common] - Create string param in dic    ${arg_dic}  	occupation_local		
    ${param_occupation_title_local}     [Common] - Create string param in dic    ${arg_dic}  	occupation_title_local		
    ${param_township_name_local}        [Common] - Create string param in dic    ${arg_dic}  	township_name_local		
    ${param_national_id_number_local}   [Common] - Create string param in dic    ${arg_dic}  	national_id_number_local		
    ${param_mother_name_local}          [Common] - Create string param in dic    ${arg_dic}  	mother_name_local		
    ${param_current_address_address_local}      [Common] - Create string param of object in dic    ${arg_dic}  	current_address     address_local
    ${param_current_address_commune_local}      [Common] - Create string param of object in dic    ${arg_dic}  	current_address     commune_local
    ${param_current_address_district_local}     [Common] - Create string param of object in dic    ${arg_dic}  	current_address     district_local
    ${param_current_address_city_local}         [Common] - Create string param of object in dic    ${arg_dic}  	current_address     city_local
    ${param_current_address_province_local}     [Common] - Create string param of object in dic    ${arg_dic}  	current_address     province_local
    ${param_current_address_postal_code_local}      [Common] - Create string param of object in dic    ${arg_dic}  	current_address     postal_code_local
    ${param_current_address_country_local}      [Common] - Create string param of object in dic    ${arg_dic}  	current_address     country_local
    ${param_permanent_address_address_local}        [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     address_local
    ${param_permanent_address_commune_local}        [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     commune_local
    ${param_permanent_address_district_local}       [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     district_local
    ${param_permanent_address_city_local}       [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     city_local
    ${param_permanent_address_province_local}       [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     province_local
    ${param_permanent_address_postal_code_local}        [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     postal_code_local
    ${param_permanent_address_country_local}        [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     country_local
    ${param_bank_name_local}        [Common] - Create string param of object in dic    ${arg_dic}  	bank     name_local
    ${param_bank_branch_area_local}     [Common] - Create string param of object in dic    ${arg_dic}  	bank     branch_area_local
    ${param_bank_branch_city_local}     [Common] - Create string param of object in dic    ${arg_dic}  	bank     branch_city_local
    ${param_primary_identity_identity_id_local}     [Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     identity_id_local
    ${param_secondary_identity_identity_id_local}       [Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     identity_id_local
    ${param_acquiring_sale_executive_name_local}        [Common] - Create string param in dic    ${arg_dic}  	acquiring_sale_executive_name_local		
    ${param_relationship_manager_name_local}        [Common] - Create string param in dic    ${arg_dic}  	relationship_manager_name_local		
    ${param_sale_region_local}      [Common] - Create string param in dic    ${arg_dic}  	sale_region_local		
    ${param_commercial_account_manager_local}       [Common] - Create string param in dic    ${arg_dic}  	commercial_account_manager_local		
    ${body}              catenate           SEPARATOR=
        ...          {
        ...                 ${param_is_testing_account}
        ...                 ${param_is_system_account}
        ...                 ${param_acquisition_source}
        ...                 "referrer_user_type": {
        ...                     ${param_referrer_user_type_id}
        ...                     ${param_referrer_user_type_name}
        ...                 },
        ...                 ${param_referrer_user_id}
        ...                 ${param_agent_type_id}
        ...                 ${param_unique_reference}
        ...                 ${param_mm_card_type_id}
        ...                 ${param_mm_card_level_id}
        ...                 ${param_mm_factory_card_number}
        ...                 ${param_model_type}
        ...                 ${param_is_require_otp}
        ...                 ${param_agent_classification_id}
        ...                 ${param_tin_number}
        ...                 ${param_title}
        ...                 ${param_first_name}
        ...                 ${param_middle_name}
        ...                 ${param_last_name}
        ...                 ${param_suffix}
        ...                 ${param_date_of_birth}
        ...                 ${param_place_of_birth}
        ...                 ${param_gender}
        ...                 ${param_ethnicity}
        ...                 ${param_nationality}
        ...                 ${param_occupation}
        ...                 ${param_occupation_title}
        ...                 ${param_township_code}
        ...                 ${param_township_name}
        ...                 ${param_national_id_number}
        ...                 ${param_mother_name}
        ...                 ${param_email}
        ...                 ${param_primary_mobile_number}
        ...                 ${param_secondary_mobile_number}
        ...                 ${param_tertiary_mobile_number}
        ...                 ${param_tin_number_local}
        ...                 ${param_title_local}
        ...                 ${param_first_name_local}
        ...                 ${param_middle_name_local}
        ...                 ${param_last_name_local}
        ...                 ${param_suffix_local}
        ...                 ${param_place_of_birth_local}
        ...                 ${param_gender_local}
        ...                 ${param_occupation_local}
        ...                 ${param_occupation_title_local}
        ...                 ${param_township_name_local}
        ...                 ${param_national_id_number_local}
        ...                 ${param_mother_name_local}
        ...                 "address": {
        ...                     "current_address": {
        ...      					${param_current_address_citizen_association}
        ...      					${param_current_address_neighbourhood_association}
        ...      					${param_current_address_address}
        ...      					${param_current_address_commune}
        ...      					${param_current_address_district}
        ...      					${param_current_address_city}
        ...      					${param_current_address_province}
        ...      					${param_current_address_postal_code}
        ...      					${param_current_address_country}
        ...      					${param_current_address_landmark}
        ...      					${param_current_address_longitude}
        ...      					${param_current_address_latitude}
        ...                         ${param_current_address_address_local}
        ...                         ${param_current_address_commune_local}
        ...                         ${param_current_address_district_local}
        ...                         ${param_current_address_city_local}
        ...                         ${param_current_address_province_local}
        ...                         ${param_current_address_postal_code_local}
        ...                         ${param_current_address_country_local}
        ...                     },
        ...                     "permanent_address": {
        ...      					${param_permanent_address_citizen_association}
        ...      					${param_permanent_address_neighbourhood_association}
        ...      					${param_permanent_address_address}
        ...      					${param_permanent_address_commune}
        ...      					${param_permanent_address_district}
        ...      					${param_permanent_address_city}
        ...      					${param_permanent_address_province}
        ...      					${param_permanent_address_postal_code}
        ...      					${param_permanent_address_country}
        ...      					${param_permanent_address_landmark}
        ...      					${param_permanent_address_longitude}
        ...      					${param_permanent_address_latitude}
        ...                         ${param_permanent_address_address_local}
        ...                         ${param_permanent_address_commune_local}
        ...                         ${param_permanent_address_district_local}
        ...                         ${param_permanent_address_city_local}
        ...                         ${param_permanent_address_province_local}
        ...                         ${param_permanent_address_postal_code_local}
        ...                         ${param_permanent_address_country_local}
        ...                     }
        ...                 },
        ...                 "bank":{
        ...      				${param_bank_name}
        ...      				${param_bank_account_status}
        ...      				${param_bank_account_name}
        ...      				${param_bank_account_number}
        ...      				${param_bank_branch_area}
        ...      				${param_bank_branch_city}
        ...      				${param_bank_register_date}
        ...      				${param_bank_register_source}
        ...      				${param_bank_is_verified}
        ...      				${param_bank_end_date}
        ...                     ${param_bank_name_local}
        ...                     ${param_bank_branch_area_local}
        ...                     ${param_bank_branch_city_local}
        ...                 },
        ...                 "contract": {
        ...      				${param_contract_type}
        ...      				${param_contract_number}
        ...      				${param_contract_extension_type}
        ...      				${param_contract_sign_date}
        ...      				${param_contract_issue_date}
        ...      				${param_contract_expired_date}
        ...      				${param_contract_notification_alert}
        ...      				${param_contract_day_of_period_reconciliation}
        ...      				${param_contract_release}
        ...      				${param_contract_file_url}
        ...      				${param_contract_assessment_information_url}
        ...                  },
        ...                 "accreditation": {
        ...                     "primary_identity": {
        ...      					${param_primary_identity_type}
        ...      					${param_primary_identity_status}
        ...      					${param_primary_identity_identity_id}
        ...      					${param_primary_identity_place_of_issue}
        ...      					${param_primary_identity_issue_date}
        ...      					${param_primary_identity_expired_date}
        ...      					${param_primary_identity_front_identity_url}
        ...      					${param_primary_identity_back_identity_url}
        ...                         ${param_primary_identity_identity_id_local}
        ...                     },
        ...                     "secondary_identity": {
        ...      					${param_secondary_identity_type}
        ...      					${param_secondary_identity_status}
        ...      					${param_secondary_identity_identity_id}
        ...      					${param_secondary_identity_place_of_issue}
        ...      					${param_secondary_identity_issue_date}
        ...      					${param_secondary_identity_expired_date}
        ...      					${param_secondary_identity_front_identity_url}
        ...      					${param_secondary_identity_back_identity_url}
        ...                         ${param_secondary_identity_identity_id_local}
        ...                     },
        ...      				${param_status_id}
        ...      				${param_remark}
        ...      				${param_verify_by}
        ...      				${param_verify_date}
        ...      				${param_risk_level}
        ...                 },
        ...                 "additional": {
        ...      				${param_additional_acquiring_sales_executive_id}
        ...      				${param_additional_acquiring_sales_executive_name}
        ...      				${param_additional_relationship_manager_id}
        ...      				${param_additional_relationship_manager_name}
        ...      				${param_additional_sale_region}
        ...      				${param_additional_commercial_account_manager}
        ...      				${param_additional_profile_picture_url}
        ...      				${param_additional_national_id_photo_url}
        ...      				${param_additional_tax_id_card_photo_url}
        ...      				${param_additional_field_1_name}
        ...      				${param_additional_field_1_value}
        ...      				${param_additional_field_2_name}
        ...      				${param_additional_field_2_value}
        ...      				${param_additional_field_3_name}
        ...      				${param_additional_field_3_value}
        ...      				${param_additional_field_4_name}
        ...      				${param_additional_field_4_value}
        ...      				${param_additional_field_5_name}
        ...      				${param_additional_field_5_value}
        ...      				${param_additional_supporting_file_1_url}
        ...      				${param_additional_supporting_file_2_url}
        ...      				${param_additional_supporting_file_3_url}
        ...      				${param_additional_supporting_file_4_url}
        ...      				${param_additional_supporting_file_5_url}
        ...                     ${param_acquiring_sale_executive_name_local}
        ...                     ${param_relationship_manager_name_local}
        ...                     ${param_sale_region_local}
        ...                     ${param_commercial_account_manager_local}
        ...                 }
        ...          }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - agent updates full agent profiles - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_referrer_user_type_id}		[Common] - Create int param in dic    ${arg_dic}  	id		
    ${param_referrer_user_type_name}		[Common] - Create string param in dic    ${arg_dic}  	name		
    ${param_referrer_user_id}		[Common] - Create int param in dic    ${arg_dic}  	referrer_user_id		
    ${param_unique_reference}		[Common] - Create string param in dic    ${arg_dic}  	unique_reference		
    ${param_company_id}             [Common] - Create int param in dic    ${arg_dic}  	company_id		
    ${param_mm_card_type_id}		[Common] - Create int param in dic    ${arg_dic}  	mm_card_type_id		
    ${param_mm_card_level_id}		[Common] - Create int param in dic    ${arg_dic}  	mm_card_level_id		
    ${param_mm_factory_card_number}		[Common] - Create string param in dic    ${arg_dic}  	mm_factory_card_number		
    ${param_is_require_otp}		[Common] - Create boolean param in dic    ${arg_dic}  	is_require_otp		
    ${param_tin_number}		[Common] - Create string param in dic    ${arg_dic}  	tin_number		
    ${param_title}		[Common] - Create string param in dic    ${arg_dic}  	title		
    ${param_first_name}		[Common] - Create string param in dic    ${arg_dic}  	first_name		
    ${param_middle_name}		[Common] - Create string param in dic    ${arg_dic}  	middle_name		
    ${param_last_name}		[Common] - Create string param in dic    ${arg_dic}  	last_name		
    ${param_suffix}		[Common] - Create string param in dic    ${arg_dic}  	suffix		
    ${param_date_of_birth}		[Common] - Create string param in dic    ${arg_dic}  	date_of_birth		
    ${param_place_of_birth}		[Common] - Create string param in dic    ${arg_dic}  	place_of_birth		
    ${param_gender}		[Common] - Create string param in dic    ${arg_dic}  	gender		
    ${param_ethnicity}		[Common] - Create string param in dic    ${arg_dic}  	ethnicity		
    ${param_nationality}		[Common] - Create string param in dic    ${arg_dic}  	nationality		
    ${param_occupation}		[Common] - Create string param in dic    ${arg_dic}  	occupation		
    ${param_occupation_title}		[Common] - Create string param in dic    ${arg_dic}  	occupation_title		
    ${param_township_code}		[Common] - Create string param in dic    ${arg_dic}  	township_code		
    ${param_township_name}		[Common] - Create string param in dic    ${arg_dic}  	township_name		
    ${param_national_id_number}		[Common] - Create string param in dic    ${arg_dic}  	national_id_number		
    ${param_mother_name}		[Common] - Create string param in dic    ${arg_dic}  	mother_name		
    ${param_email}		[Common] - Create string param in dic    ${arg_dic}  	email		
    ${param_primary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}  	primary_mobile_number		
    ${param_secondary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}  	secondary_mobile_number		
    ${param_tertiary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}  	tertiary_mobile_number		
    ${param_current_address_citizen_association}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     citizen_association
    ${param_current_address_neighbourhood_association}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     neighbourhood_association
    ${param_current_address_address}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     address
    ${param_current_address_commune}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     commune
    ${param_current_address_district}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     district
    ${param_current_address_city}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     city
    ${param_current_address_province}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     province
    ${param_current_address_postal_code}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     postal_code
    ${param_current_address_country}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     country
    ${param_current_address_landmark}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     landmark
    ${param_current_address_longitude}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     longitude
    ${param_current_address_latitude}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     latitude
    ${param_permanent_address_citizen_association}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     citizen_association
    ${param_permanent_address_neighbourhood_association}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     neighbourhood_association
    ${param_permanent_address_address}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     address
    ${param_permanent_address_commune}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     commune
    ${param_permanent_address_district}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     district
    ${param_permanent_address_city}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     city
    ${param_permanent_address_province}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     province
    ${param_permanent_address_postal_code}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     postal_code
    ${param_permanent_address_country}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     country
    ${param_permanent_address_landmark}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     landmark
    ${param_permanent_address_longitude}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     longitude
    ${param_permanent_address_latitude}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     latitude
    ${param_contract_type}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     type
    ${param_contract_number}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     number
    ${param_contract_extension_type}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     extension_type
    ${param_contract_sign_date}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     sign_date
    ${param_contract_issue_date}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     issue_date
    ${param_contract_expired_date}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     expired_date
    ${param_contract_notification_alert}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     notification_alert
    ${param_contract_day_of_period_reconciliation}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     day_of_period_reconciliation
    ${param_contract_release}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     release
    ${param_primary_identity_type}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     type
    ${param_primary_identity_identity_id}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     identity_id
    ${param_primary_identity_place_of_issue}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     place_of_issue
    ${param_primary_identity_issue_date}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     issue_date
    ${param_primary_identity_expired_date}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     expired_date
    ${param_primary_identity_front_identity_url}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     front_identity_url
    ${param_primary_identity_back_identity_url}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     back_identity_url
    ${param_secondary_identity_type}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     type
    ${param_secondary_identity_identity_id}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     identity_id
    ${param_secondary_identity_place_of_issue}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     place_of_issue
    ${param_secondary_identity_issue_date}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     issue_date
    ${param_secondary_identity_expired_date}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     expired_date
    ${param_secondary_identity_front_identity_url}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     front_identity_url
    ${param_secondary_identity_back_identity_url}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     back_identity_url
    ${param_additional_profile_picture_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     profile_picture_url
    ${param_additional_national_id_photo_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     national_id_photo_url
    ${param_additional_tax_id_card_photo_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     tax_id_card_photo_url
    ${param_tin_number_local}		[Common] - Create string param in dic    ${arg_dic}  	tin_number_local		
    ${param_title_local}            [Common] - Create string param in dic    ${arg_dic}  	title_local		
    ${param_first_name_local}       [Common] - Create string param in dic    ${arg_dic}  	first_name_local		
    ${param_middle_name_local}      [Common] - Create string param in dic    ${arg_dic}  	middle_name_local		
    ${param_last_name_local}        [Common] - Create string param in dic    ${arg_dic}  	last_name_local		
    ${param_suffix_local}           [Common] - Create string param in dic    ${arg_dic}  	suffix_local		
    ${param_place_of_birth_local}   [Common] - Create string param in dic    ${arg_dic}  	place_of_birth_local		
    ${param_gender_local}           [Common] - Create string param in dic    ${arg_dic}  	gender_local		
    ${param_occupation_local}       [Common] - Create string param in dic    ${arg_dic}  	occupation_local		
    ${param_occupation_title_local}     [Common] - Create string param in dic    ${arg_dic}  	occupation_title_local		
    ${param_township_name_local}        [Common] - Create string param in dic    ${arg_dic}  	township_name_local		
    ${param_national_id_number_local}   [Common] - Create string param in dic    ${arg_dic}  	national_id_number_local		
    ${param_mother_name_local}          [Common] - Create string param in dic    ${arg_dic}  	mother_name_local		
    ${param_current_address_address_local}      [Common] - Create string param of object in dic    ${arg_dic}  	current_address     address_local
    ${param_current_address_commune_local}      [Common] - Create string param of object in dic    ${arg_dic}  	current_address     commune_local
    ${param_current_address_district_local}     [Common] - Create string param of object in dic    ${arg_dic}  	current_address     district_local
    ${param_current_address_city_local}         [Common] - Create string param of object in dic    ${arg_dic}  	current_address     city_local
    ${param_current_address_province_local}     [Common] - Create string param of object in dic    ${arg_dic}  	current_address     province_local
    ${param_current_address_postal_code_local}      [Common] - Create string param of object in dic    ${arg_dic}  	current_address     postal_code_local
    ${param_current_address_country_local}      [Common] - Create string param of object in dic    ${arg_dic}  	current_address     country_local
    ${param_permanent_address_address_local}        [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     address_local
    ${param_permanent_address_commune_local}        [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     commune_local
    ${param_permanent_address_district_local}       [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     district_local
    ${param_permanent_address_city_local}       [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     city_local
    ${param_permanent_address_province_local}       [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     province_local
    ${param_permanent_address_postal_code_local}        [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     postal_code_local
    ${param_permanent_address_country_local}        [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     country_local
    ${param_primary_identity_identity_id_local}     [Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     identity_id_local
    ${param_secondary_identity_identity_id_local}       [Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     identity_id_local
    ${body}              catenate           SEPARATOR=
        ...          {
        ...                 "referrer_user_type": {
        ...                     ${param_referrer_user_type_id}
        ...                     ${param_referrer_user_type_name}
        ...                 },
        ...                 ${param_referrer_user_id}
        ...                 ${param_unique_reference}
        ...                 ${param_mm_card_type_id}
        ...                 ${param_mm_card_level_id}
        ...                 ${param_mm_factory_card_number}
        ...                 ${param_is_require_otp}
        ...                 ${param_tin_number}
        ...                 ${param_title}
        ...                 ${param_first_name}
        ...                 ${param_middle_name}
        ...                 ${param_last_name}
        ...                 ${param_suffix}
        ...                 ${param_date_of_birth}
        ...                 ${param_place_of_birth}
        ...                 ${param_gender}
        ...                 ${param_ethnicity}
        ...                 ${param_nationality}
        ...                 ${param_occupation}
        ...                 ${param_occupation_title}
        ...                 ${param_township_code}
        ...                 ${param_township_name}
        ...                 ${param_national_id_number}
        ...                 ${param_mother_name}
        ...                 ${param_email}
        ...                 ${param_primary_mobile_number}
        ...                 ${param_secondary_mobile_number}
        ...                 ${param_tertiary_mobile_number}
        ...                 ${param_tin_number_local}
        ...                 ${param_title_local}
        ...                 ${param_first_name_local}
        ...                 ${param_middle_name_local}
        ...                 ${param_last_name_local}
        ...                 ${param_suffix_local}
        ...                 ${param_place_of_birth_local}
        ...                 ${param_gender_local}
        ...                 ${param_occupation_local}
        ...                 ${param_occupation_title_local}
        ...                 ${param_township_name_local}
        ...                 ${param_national_id_number_local}
        ...                 ${param_mother_name_local}
        ...                 "address": {
        ...                     "current_address": {
        ...      					${param_current_address_citizen_association}
        ...      					${param_current_address_neighbourhood_association}
        ...      					${param_current_address_address}
        ...      					${param_current_address_commune}
        ...      					${param_current_address_district}
        ...      					${param_current_address_city}
        ...      					${param_current_address_province}
        ...      					${param_current_address_postal_code}
        ...      					${param_current_address_country}
        ...      					${param_current_address_landmark}
        ...      					${param_current_address_longitude}
        ...      					${param_current_address_latitude}
        ...                         ${param_current_address_address_local}
        ...                         ${param_current_address_commune_local}
        ...                         ${param_current_address_district_local}
        ...                         ${param_current_address_city_local}
        ...                         ${param_current_address_province_local}
        ...                         ${param_current_address_postal_code_local}
        ...                         ${param_current_address_country_local}
        ...                     },
        ...                     "permanent_address": {
        ...      					${param_permanent_address_citizen_association}
        ...      					${param_permanent_address_neighbourhood_association}
        ...      					${param_permanent_address_address}
        ...      					${param_permanent_address_commune}
        ...      					${param_permanent_address_district}
        ...      					${param_permanent_address_city}
        ...      					${param_permanent_address_province}
        ...      					${param_permanent_address_postal_code}
        ...      					${param_permanent_address_country}
        ...      					${param_permanent_address_landmark}
        ...      					${param_permanent_address_longitude}
        ...      					${param_permanent_address_latitude}
        ...                         ${param_permanent_address_address_local}
        ...                         ${param_permanent_address_commune_local}
        ...                         ${param_permanent_address_district_local}
        ...                         ${param_permanent_address_city_local}
        ...                         ${param_permanent_address_province_local}
        ...                         ${param_permanent_address_postal_code_local}
        ...                         ${param_permanent_address_country_local}
        ...                     }
        ...                 },
        ...                 "contract": {
        ...      				${param_contract_type}
        ...      				${param_contract_number}
        ...      				${param_contract_extension_type}
        ...      				${param_contract_sign_date}
        ...      				${param_contract_issue_date}
        ...      				${param_contract_expired_date}
        ...      				${param_contract_notification_alert}
        ...      				${param_contract_day_of_period_reconciliation}
        ...      				${param_contract_release}
        ...                  },
        ...                 "accreditation": {
        ...                     "primary_identity": {
        ...      					${param_primary_identity_type}
        ...      					${param_primary_identity_identity_id}
        ...      					${param_primary_identity_place_of_issue}
        ...      					${param_primary_identity_issue_date}
        ...      					${param_primary_identity_expired_date}
        ...      					${param_primary_identity_front_identity_url}
        ...      					${param_primary_identity_back_identity_url}
        ...                         ${param_primary_identity_identity_id_local}
        ...                     },
        ...                     "secondary_identity": {
        ...      					${param_secondary_identity_type}
        ...      					${param_secondary_identity_identity_id}
        ...      					${param_secondary_identity_place_of_issue}
        ...      					${param_secondary_identity_issue_date}
        ...      					${param_secondary_identity_expired_date}
        ...      					${param_secondary_identity_front_identity_url}
        ...      					${param_secondary_identity_back_identity_url}
        ...                         ${param_secondary_identity_identity_id_local}
        ...                     }
        ...                 },
        ...                 "additional": {
        ...      				${param_additional_profile_picture_url}
        ...      				${param_additional_national_id_photo_url}
        ...      				${param_additional_tax_id_card_photo_url}
        ...                 }
        ...          }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - agent updates partial agent profiles - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_referrer_user_type_id}		[Common] - Create int param in dic    ${arg_dic}  	id		
    ${param_referrer_user_type_name}		[Common] - Create string param in dic    ${arg_dic}  	name		
    ${param_referrer_user_id}		[Common] - Create int param in dic    ${arg_dic}  	referrer_user_id		
    ${param_unique_reference}		[Common] - Create string param in dic    ${arg_dic}  	unique_reference		
    ${param_company_id}             [Common] - Create int param in dic    ${arg_dic}  	company_id		
    ${param_mm_card_type_id}		[Common] - Create int param in dic    ${arg_dic}  	mm_card_type_id		
    ${param_mm_card_level_id}		[Common] - Create int param in dic    ${arg_dic}  	mm_card_level_id		
    ${param_mm_factory_card_number}		[Common] - Create string param in dic    ${arg_dic}  	mm_factory_card_number		
    ${param_is_require_otp}		[Common] - Create boolean param in dic    ${arg_dic}  	is_require_otp		
    ${param_tin_number}		[Common] - Create string param in dic    ${arg_dic}  	tin_number		
    ${param_title}		[Common] - Create string param in dic    ${arg_dic}  	title		
    ${param_first_name}		[Common] - Create string param in dic    ${arg_dic}  	first_name		
    ${param_middle_name}		[Common] - Create string param in dic    ${arg_dic}  	middle_name		
    ${param_last_name}		[Common] - Create string param in dic    ${arg_dic}  	last_name		
    ${param_suffix}		[Common] - Create string param in dic    ${arg_dic}  	suffix		
    ${param_date_of_birth}		[Common] - Create string param in dic    ${arg_dic}  	date_of_birth		
    ${param_place_of_birth}		[Common] - Create string param in dic    ${arg_dic}  	place_of_birth		
    ${param_gender}		[Common] - Create string param in dic    ${arg_dic}  	gender		
    ${param_ethnicity}		[Common] - Create string param in dic    ${arg_dic}  	ethnicity		
    ${param_nationality}		[Common] - Create string param in dic    ${arg_dic}  	nationality		
    ${param_occupation}		[Common] - Create string param in dic    ${arg_dic}  	occupation		
    ${param_occupation_title}		[Common] - Create string param in dic    ${arg_dic}  	occupation_title		
    ${param_township_code}		[Common] - Create string param in dic    ${arg_dic}  	township_code		
    ${param_township_name}		[Common] - Create string param in dic    ${arg_dic}  	township_name		
    ${param_national_id_number}		[Common] - Create string param in dic    ${arg_dic}  	national_id_number		
    ${param_mother_name}		[Common] - Create string param in dic    ${arg_dic}  	mother_name		
    ${param_email}		[Common] - Create string param in dic    ${arg_dic}  	email		
    ${param_primary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}  	primary_mobile_number		
    ${param_secondary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}  	secondary_mobile_number		
    ${param_tertiary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}  	tertiary_mobile_number		
    ${param_current_address_citizen_association}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     citizen_association
    ${param_current_address_neighbourhood_association}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     neighbourhood_association
    ${param_current_address_address}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     address
    ${param_current_address_commune}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     commune
    ${param_current_address_district}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     district
    ${param_current_address_city}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     city
    ${param_current_address_province}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     province
    ${param_current_address_postal_code}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     postal_code
    ${param_current_address_country}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     country
    ${param_current_address_landmark}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     landmark
    ${param_current_address_longitude}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     longitude
    ${param_current_address_latitude}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     latitude
    ${param_permanent_address_citizen_association}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     citizen_association
    ${param_permanent_address_neighbourhood_association}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     neighbourhood_association
    ${param_permanent_address_address}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     address
    ${param_permanent_address_commune}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     commune
    ${param_permanent_address_district}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     district
    ${param_permanent_address_city}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     city
    ${param_permanent_address_province}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     province
    ${param_permanent_address_postal_code}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     postal_code
    ${param_permanent_address_country}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     country
    ${param_permanent_address_landmark}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     landmark
    ${param_permanent_address_longitude}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     longitude
    ${param_permanent_address_latitude}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     latitude
    ${param_contract_type}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     type
    ${param_contract_number}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     number
    ${param_contract_extension_type}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     extension_type
    ${param_contract_sign_date}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     sign_date
    ${param_contract_issue_date}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     issue_date
    ${param_contract_expired_date}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     expired_date
    ${param_contract_notification_alert}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     notification_alert
    ${param_contract_day_of_period_reconciliation}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     day_of_period_reconciliation
    ${param_contract_release}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     release
    ${param_primary_identity_type}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     type
    ${param_primary_identity_identity_id}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     identity_id
    ${param_primary_identity_place_of_issue}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     place_of_issue
    ${param_primary_identity_issue_date}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     issue_date
    ${param_primary_identity_expired_date}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     expired_date
    ${param_primary_identity_front_identity_url}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     front_identity_url
    ${param_primary_identity_back_identity_url}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     back_identity_url
    ${param_secondary_identity_type}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     type
    ${param_secondary_identity_identity_id}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     identity_id
    ${param_secondary_identity_place_of_issue}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     place_of_issue
    ${param_secondary_identity_issue_date}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     issue_date
    ${param_secondary_identity_expired_date}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     expired_date
    ${param_secondary_identity_front_identity_url}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     front_identity_url
    ${param_secondary_identity_back_identity_url}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     back_identity_url
    ${param_additional_profile_picture_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     profile_picture_url
    ${param_additional_national_id_photo_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     national_id_photo_url
    ${param_additional_tax_id_card_photo_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     tax_id_card_photo_url
    ${param_tin_number_local}		[Common] - Create string param in dic    ${arg_dic}  	tin_number_local		
    ${param_title_local}            [Common] - Create string param in dic    ${arg_dic}  	title_local		
    ${param_first_name_local}       [Common] - Create string param in dic    ${arg_dic}  	first_name_local		
    ${param_middle_name_local}      [Common] - Create string param in dic    ${arg_dic}  	middle_name_local		
    ${param_last_name_local}        [Common] - Create string param in dic    ${arg_dic}  	last_name_local		
    ${param_suffix_local}           [Common] - Create string param in dic    ${arg_dic}  	suffix_local		
    ${param_place_of_birth_local}   [Common] - Create string param in dic    ${arg_dic}  	place_of_birth_local		
    ${param_gender_local}           [Common] - Create string param in dic    ${arg_dic}  	gender_local		
    ${param_occupation_local}       [Common] - Create string param in dic    ${arg_dic}  	occupation_local		
    ${param_occupation_title_local}     [Common] - Create string param in dic    ${arg_dic}  	occupation_title_local		
    ${param_township_name_local}        [Common] - Create string param in dic    ${arg_dic}  	township_name_local		
    ${param_national_id_number_local}   [Common] - Create string param in dic    ${arg_dic}  	national_id_number_local		
    ${param_mother_name_local}          [Common] - Create string param in dic    ${arg_dic}  	mother_name_local		
    ${param_current_address_address_local}      [Common] - Create string param of object in dic    ${arg_dic}  	current_address     address_local
    ${param_current_address_commune_local}      [Common] - Create string param of object in dic    ${arg_dic}  	current_address     commune_local
    ${param_current_address_district_local}     [Common] - Create string param of object in dic    ${arg_dic}  	current_address     district_local
    ${param_current_address_city_local}         [Common] - Create string param of object in dic    ${arg_dic}  	current_address     city_local
    ${param_current_address_province_local}     [Common] - Create string param of object in dic    ${arg_dic}  	current_address     province_local
    ${param_current_address_postal_code_local}      [Common] - Create string param of object in dic    ${arg_dic}  	current_address     postal_code_local
    ${param_current_address_country_local}      [Common] - Create string param of object in dic    ${arg_dic}  	current_address     country_local
    ${param_permanent_address_address_local}        [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     address_local
    ${param_permanent_address_commune_local}        [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     commune_local
    ${param_permanent_address_district_local}       [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     district_local
    ${param_permanent_address_city_local}       [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     city_local
    ${param_permanent_address_province_local}       [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     province_local
    ${param_permanent_address_postal_code_local}        [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     postal_code_local
    ${param_permanent_address_country_local}        [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     country_local
    ${param_primary_identity_identity_id_local}     [Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     identity_id_local
    ${param_secondary_identity_identity_id_local}       [Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     identity_id_local
    ${body}              catenate           SEPARATOR=
        ...          {
        ...                 "referrer_user_type": {
        ...                     ${param_referrer_user_type_id}
        ...                     ${param_referrer_user_type_name}
        ...                 },
        ...                 ${param_referrer_user_id}
        ...                 ${param_unique_reference}
        ...                 ${param_mm_card_type_id}
        ...                 ${param_mm_card_level_id}
        ...                 ${param_mm_factory_card_number}
        ...                 ${param_is_require_otp}
        ...                 ${param_tin_number}
        ...                 ${param_title}
        ...                 ${param_first_name}
        ...                 ${param_middle_name}
        ...                 ${param_last_name}
        ...                 ${param_suffix}
        ...                 ${param_date_of_birth}
        ...                 ${param_place_of_birth}
        ...                 ${param_gender}
        ...                 ${param_ethnicity}
        ...                 ${param_nationality}
        ...                 ${param_occupation}
        ...                 ${param_occupation_title}
        ...                 ${param_township_code}
        ...                 ${param_township_name}
        ...                 ${param_national_id_number}
        ...                 ${param_mother_name}
        ...                 ${param_email}
        ...                 ${param_primary_mobile_number}
        ...                 ${param_secondary_mobile_number}
        ...                 ${param_tertiary_mobile_number}
        ...                 ${param_tin_number_local}
        ...                 ${param_title_local}
        ...                 ${param_first_name_local}
        ...                 ${param_middle_name_local}
        ...                 ${param_last_name_local}
        ...                 ${param_suffix_local}
        ...                 ${param_place_of_birth_local}
        ...                 ${param_gender_local}
        ...                 ${param_occupation_local}
        ...                 ${param_occupation_title_local}
        ...                 ${param_township_name_local}
        ...                 ${param_national_id_number_local}
        ...                 ${param_mother_name_local}
        ...                 "address": {
        ...                     "current_address": {
        ...      					${param_current_address_citizen_association}
        ...      					${param_current_address_neighbourhood_association}
        ...      					${param_current_address_address}
        ...      					${param_current_address_commune}
        ...      					${param_current_address_district}
        ...      					${param_current_address_city}
        ...      					${param_current_address_province}
        ...      					${param_current_address_postal_code}
        ...      					${param_current_address_country}
        ...      					${param_current_address_landmark}
        ...      					${param_current_address_longitude}
        ...      					${param_current_address_latitude}
        ...                         ${param_current_address_address_local}
        ...                         ${param_current_address_commune_local}
        ...                         ${param_current_address_district_local}
        ...                         ${param_current_address_city_local}
        ...                         ${param_current_address_province_local}
        ...                         ${param_current_address_postal_code_local}
        ...                         ${param_current_address_country_local}
        ...                     },
        ...                     "permanent_address": {
        ...      					${param_permanent_address_citizen_association}
        ...      					${param_permanent_address_neighbourhood_association}
        ...      					${param_permanent_address_address}
        ...      					${param_permanent_address_commune}
        ...      					${param_permanent_address_district}
        ...      					${param_permanent_address_city}
        ...      					${param_permanent_address_province}
        ...      					${param_permanent_address_postal_code}
        ...      					${param_permanent_address_country}
        ...      					${param_permanent_address_landmark}
        ...      					${param_permanent_address_longitude}
        ...      					${param_permanent_address_latitude}
        ...                         ${param_permanent_address_address_local}
        ...                         ${param_permanent_address_commune_local}
        ...                         ${param_permanent_address_district_local}
        ...                         ${param_permanent_address_city_local}
        ...                         ${param_permanent_address_province_local}
        ...                         ${param_permanent_address_postal_code_local}
        ...                         ${param_permanent_address_country_local}
        ...                     }
        ...                 },
        ...                 "contract": {
        ...      				${param_contract_type}
        ...      				${param_contract_number}
        ...      				${param_contract_extension_type}
        ...      				${param_contract_sign_date}
        ...      				${param_contract_issue_date}
        ...      				${param_contract_expired_date}
        ...      				${param_contract_notification_alert}
        ...      				${param_contract_day_of_period_reconciliation}
        ...      				${param_contract_release}
        ...                  },
        ...                 "accreditation": {
        ...                     "primary_identity": {
        ...      					${param_primary_identity_type}
        ...      					${param_primary_identity_identity_id}
        ...      					${param_primary_identity_place_of_issue}
        ...      					${param_primary_identity_issue_date}
        ...      					${param_primary_identity_expired_date}
        ...      					${param_primary_identity_front_identity_url}
        ...      					${param_primary_identity_back_identity_url}
        ...                         ${param_primary_identity_identity_id_local}
        ...                     },
        ...                     "secondary_identity": {
        ...      					${param_secondary_identity_type}
        ...      					${param_secondary_identity_identity_id}
        ...      					${param_secondary_identity_place_of_issue}
        ...      					${param_secondary_identity_issue_date}
        ...      					${param_secondary_identity_expired_date}
        ...      					${param_secondary_identity_front_identity_url}
        ...      					${param_secondary_identity_back_identity_url}
        ...                         ${param_secondary_identity_identity_id_local}
        ...                     }
        ...                 },
        ...                 "additional": {
        ...      				${param_additional_profile_picture_url}
        ...      				${param_additional_national_id_photo_url}
        ...      				${param_additional_tax_id_card_photo_url}
        ...                 }
        ...          }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - agent resets password - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_username}=  [Common] - Create string param in dic    ${arg_dic}   username
    ${param_new_password}=  [Common] - Create string param in dic    ${arg_dic}   new_password
    ${param_user_id}=  [Common] - Create int param in dic    ${arg_dic}   user_id
    ${param_user_type_id}=  [Common] - Create string param in dic    ${arg_dic}   id
    ${param_user_type_name}=  [Common] - Create string param in dic    ${arg_dic}   name
    ${param_otp_reference_id}=  [Common] - Create string param in dic    ${arg_dic}   otp_reference_id
    ${body}              catenate           SEPARATOR=
        ...	{
        ...		${param_username}
        ...		${param_new_password}
        ...		${param_user_id}
        ...     "user_type":{
        ...         ${param_user_type_id}
        ...         ${param_user_type_name}
        ...     },
        ...		${param_otp_reference_id}
        ...	}
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - change password - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_username}		[Common] - Create int param in dic    ${arg_dic}  	username		
    ${param_password}		[Common] - Create string param in dic    ${arg_dic}  	password		
    ${param_old_password}		[Common] - Create string param in dic    ${arg_dic}  	old_password		
    ${param_new_password}		[Common] - Create string param in dic    ${arg_dic}  	new_password		
    ${body}              catenate           SEPARATOR=
        ...          {
        ...                 "credential_identity": {
		...                         ${param_username}
		...                         ${param_password}
	    ...                  },
	    ...                  ${param_old_password}
	    ...                  ${param_new_password}
        ...          }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - create relationship - body
   [Arguments]     ${output}=body    ${remove_null}=${True}   ${remove_empty}=${True}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
    ...    {
    ...      "relationship_type_id": "int",
    ...      "main_user": {
    ...        "user_id": "int",
    ...        "user_type": {
    ...          "id": "int",
    ...          "name": "string"
    ...        }
    ...      },
    ...      "sub_user": {
    ...        "user_id": "int",
    ...        "user_type": {
    ...          "id": "int",
    ...          "name": "string"
    ...        }
    ...      }
    ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - create agent - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_is_testing_account}		[Common] - Create boolean param in dic    ${arg_dic}		is_testing_account
    ${param_is_system_account}		[Common] - Create boolean param in dic    ${arg_dic}		is_system_account
    ${param_acquisition_source}		[Common] - Create string param in dic    ${arg_dic}		acquisition_source
    ${param_referrer_user_type_id}		[Common] - Create int param in dic    ${arg_dic}		id
    ${param_referrer_user_type_name}		[Common] - Create string param in dic    ${arg_dic}		name
    ${param_referrer_user_id}		[Common] - Create int param in dic    ${arg_dic}		referrer_user_id
    ${param_agent_type_id}		[Common] - Create int param in dic    ${arg_dic}		agent_type_id
    ${param_unique_reference}		[Common] - Create string param in dic    ${arg_dic}		unique_reference
    ${param_mm_card_type_id}		[Common] - Create int param in dic    ${arg_dic}		mm_card_type_id
    ${param_mm_card_level_id}		[Common] - Create int param in dic    ${arg_dic}		mm_card_level_id
    ${param_mm_factory_card_number}		[Common] - Create string param in dic    ${arg_dic}		mm_factory_card_number
    ${param_model_type}		[Common] - Create string param in dic    ${arg_dic}		model_type
    ${param_is_require_otp}		[Common] - Create boolean param in dic    ${arg_dic}		is_require_otp
    ${param_agent_classification_id}		[Common] - Create int param in dic    ${arg_dic}		agent_classification_id
    ${param_tin_number}		[Common] - Create string param in dic    ${arg_dic}		tin_number
    ${param_title}		[Common] - Create string param in dic    ${arg_dic}		title
    ${param_first_name}		[Common] - Create string param in dic    ${arg_dic}		first_name
    ${param_middle_name}		[Common] - Create string param in dic    ${arg_dic}		middle_name
    ${param_last_name}		[Common] - Create string param in dic    ${arg_dic}		last_name
    ${param_suffix}		[Common] - Create string param in dic    ${arg_dic}		suffix
    ${param_date_of_birth}		[Common] - Create string param in dic    ${arg_dic}		date_of_birth
    ${param_place_of_birth}		[Common] - Create string param in dic    ${arg_dic}		place_of_birth
    ${param_gender}		[Common] - Create string param in dic    ${arg_dic}		gender
    ${param_ethnicity}		[Common] - Create string param in dic    ${arg_dic}		ethnicity
    ${param_nationality}		[Common] - Create string param in dic    ${arg_dic}		nationality
    ${param_occupation}		[Common] - Create string param in dic    ${arg_dic}		occupation
    ${param_occupation_title}		[Common] - Create string param in dic    ${arg_dic}		occupation_title
    ${param_township_code}		[Common] - Create string param in dic    ${arg_dic}		township_code
    ${param_township_name}		[Common] - Create string param in dic    ${arg_dic}		township_name
    ${param_national_id_number}		[Common] - Create string param in dic    ${arg_dic}		national_id_number
    ${param_mother_name}		[Common] - Create string param in dic    ${arg_dic}		mother_name
    ${param_email}		[Common] - Create string param in dic    ${arg_dic}		email
    ${param_primary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}		primary_mobile_number
    ${param_secondary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}		secondary_mobile_number
    ${param_tertiary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}		tertiary_mobile_number
    ${param_current_address_citizen_association}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       citizen_association
    ${param_current_address_neighbourhood_association}      [Common] - Create string param of object in dic    ${arg_dic}    current_address       neighbourhood_association
    ${param_current_address_address}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       address
    ${param_current_address_commune}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       commune
    ${param_current_address_district}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       district
    ${param_current_address_city}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       city
    ${param_current_address_province}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       province
    ${param_current_address_postal_code}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       postal_code
    ${param_current_address_country}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       country
    ${param_current_address_landmark}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       landmark
    ${param_current_address_longitude}      [Common] - Create string param of object in dic    ${arg_dic}    current_address       longitude
    ${param_current_address_latitude}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       latitude
    ${param_permanent_address_citizen_association}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       citizen_association
    ${param_permanent_address_neighbourhood_association}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       neighbourhood_association
    ${param_permanent_address_address}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       address
    ${param_permanent_address_commune}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       commune
    ${param_permanent_address_district}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       district
    ${param_permanent_address_city}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       city
    ${param_permanent_address_province}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       province
    ${param_permanent_address_postal_code}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       postal_code
    ${param_permanent_address_country}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       country
    ${param_permanent_address_landmark}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       landmark
    ${param_permanent_address_longitude}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       longitude
    ${param_permanent_address_latitude}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       latitude
    ${param_bank_name}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     name
    ${param_bank_account_status}		[Common] - Create int param of object in dic    ${arg_dic}  	bank     account_status
    ${param_bank_account_name}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     account_name
    ${param_bank_account_number}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     account_number
    ${param_bank_branch_area}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     branch_area
    ${param_bank_branch_city}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     branch_city
    ${param_bank_register_date}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     register_date
    ${param_bank_register_source}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     register_source
    ${param_bank_is_verified}		[Common] - Create boolean param of object in dic    ${arg_dic}  	bank     is_verified
    ${param_bank_end_date}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     end_date
    ${param_contract_type}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     type
    ${param_contract_number}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     number
    ${param_contract_extension_type}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     extension_type
    ${param_contract_sign_date}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     sign_date
    ${param_contract_issue_date}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     issue_date
    ${param_contract_expired_date}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     expired_date
    ${param_contract_notification_alert}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     notification_alert
    ${param_contract_day_of_period_reconciliation}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     day_of_period_reconciliation
    ${param_contract_release}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     release
    ${param_contract_file_url}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     file_url
    ${param_contract_assessment_information_url}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     assessment_information_url
    ${param_primary_identity_type}      [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       type
    ${param_primary_identity_status}        [Common] - Create int param of object in dic    ${arg_dic}    primary_identity      status
    ${param_primary_identity_identity_id}       [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       identity_id
    ${param_primary_identity_place_of_issue}        [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       place_of_issue
    ${param_primary_identity_issue_date}        [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       issue_date
    ${param_primary_identity_expired_date}      [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       expired_date
    ${param_primary_identity_front_identity_url}        [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       front_identity_url
    ${param_primary_identity_back_identity_url}     [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       back_identity_url
    ${param_primary_identity_identity_id_local}     [Common] - Create string param of object in dic    ${arg_dic}      primary_identity    identity_id_local
    ${param_secondary_identity_type}        [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       type
    ${param_secondary_identity_status}      [Common] - Create int param of object in dic    ${arg_dic}    secondary_identity      status
    ${param_secondary_identity_identity_id}     [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       identity_id
    ${param_secondary_identity_place_of_issue}      [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       place_of_issue
    ${param_secondary_identity_issue_date}      [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       issue_date
    ${param_secondary_identity_expired_date}        [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       expired_date
    ${param_secondary_identity_front_identity_url}      [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       front_identity_url
    ${param_secondary_identity_back_identity_url}       [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       back_identity_url
    ${param_secondary_identity_identity_id_local}       [Common] - Create string param of object in dic    ${arg_dic}      secondary_identity		identity_id_local
    ${param_status_id}		[Common] - Create int param in dic    ${arg_dic}		status_id
    ${param_remark}		[Common] - Create string param in dic    ${arg_dic}		remark
    ${param_verify_by}		[Common] - Create string param in dic    ${arg_dic}		verify_by
    ${param_verify_date}		[Common] - Create string param in dic    ${arg_dic}		verify_date
    ${param_risk_level}		[Common] - Create string param in dic    ${arg_dic}		risk_level
    ${param_additional_acquiring_sales_executive_id}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     acquiring_sale_executive_id
    ${param_additional_acquiring_sales_executive_name}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     acquiring_sale_executive_name
    ${param_additional_relationship_manager_id}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     relationship_manager_id
    ${param_additional_relationship_manager_name}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     relationship_manager_name
    ${param_additional_sale_region}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     sale_region
    ${param_additional_commercial_account_manager}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     commercial_account_manager
    ${param_additional_profile_picture_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     profile_picture_url
    ${param_additional_national_id_photo_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     national_id_photo_url
    ${param_additional_tax_id_card_photo_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     tax_id_card_photo_url
    ${param_additional_field_1_name}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_1_name
    ${param_additional_field_1_value}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_1_value
    ${param_additional_field_2_name}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_2_name
    ${param_additional_field_2_value}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_2_value
    ${param_additional_field_3_name}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_3_name
    ${param_additional_field_3_value}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_3_value
    ${param_additional_field_4_name}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_4_name
    ${param_additional_field_4_value}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_4_value
    ${param_additional_field_5_name}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_5_name
    ${param_additional_field_5_value}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_5_value
    ${param_additional_supporting_file_1_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     supporting_file_1_url
    ${param_additional_supporting_file_2_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     supporting_file_2_url
    ${param_additional_supporting_file_3_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     supporting_file_3_url
    ${param_additional_supporting_file_4_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     supporting_file_4_url
    ${param_additional_supporting_file_5_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     supporting_file_5_url
    ${param_identity_identity_type_id}		[Common] - Create int param in dic    ${arg_dic}  	identity_type_id
    ${param_identity_username}		[Common] - Create string param of object in dic    ${arg_dic}  	identity     username
    ${param_identity_password}		[Common] - Create string param of object in dic    ${arg_dic}  	identity     password
    ${param_identity_auto_generate_password}		[Common] - Create boolean param of object in dic    ${arg_dic}  	identity     auto_generate_password
    ${param_tin_number_local}		[Common] - Create string param in dic    ${arg_dic}		tin_number_local
    ${param_title_local}            [Common] - Create string param in dic    ${arg_dic}		title_local
    ${param_first_name_local}       [Common] - Create string param in dic    ${arg_dic}		first_name_local
    ${param_middle_name_local}      [Common] - Create string param in dic    ${arg_dic}		middle_name_local
    ${param_last_name_local}        [Common] - Create string param in dic    ${arg_dic}		last_name_local
    ${param_suffix_local}           [Common] - Create string param in dic    ${arg_dic}		suffix_local
    ${param_place_of_birth_local}   [Common] - Create string param in dic    ${arg_dic}		place_of_birth_local
    ${param_gender_local}           [Common] - Create string param in dic    ${arg_dic}		gender_local
    ${param_occupation_local}       [Common] - Create string param in dic    ${arg_dic}		occupation_local
    ${param_occupation_title_local}     [Common] - Create string param in dic    ${arg_dic}		occupation_title_local
    ${param_township_name_local}        [Common] - Create string param in dic    ${arg_dic}		township_name_local
    ${param_national_id_number_local}   [Common] - Create string param in dic    ${arg_dic}		national_id_number_local
    ${param_mother_name_local}          [Common] - Create string param in dic    ${arg_dic}		mother_name_local
    ${param_current_address_address_local}      [Common] - Create string param of object in dic    ${arg_dic}    current_address		address_local
    ${param_current_address_commune_local}      [Common] - Create string param of object in dic    ${arg_dic}    current_address		commune_local
    ${param_current_address_district_local}     [Common] - Create string param of object in dic    ${arg_dic}    current_address		district_local
    ${param_current_address_city_local}         [Common] - Create string param of object in dic    ${arg_dic}    current_address		city_local
    ${param_current_address_province_local}     [Common] - Create string param of object in dic    ${arg_dic}    current_address		province_local
    ${param_current_address_postal_code_local}      [Common] - Create string param of object in dic    ${arg_dic}    current_address		postal_code_local
    ${param_current_address_country_local}      [Common] - Create string param of object in dic    ${arg_dic}    current_address		country_local
    ${param_permanent_address_address_local}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		address_local
    ${param_permanent_address_commune_local}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		commune_local
    ${param_permanent_address_district_local}       [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		district_local
    ${param_permanent_address_city_local}       [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		city_local
    ${param_permanent_address_province_local}       [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		province_local
    ${param_permanent_address_postal_code_local}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		postal_code_local
    ${param_permanent_address_country_local}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		country_local
    ${param_bank_name_local}        [Common] - Create string param of object in dic    ${arg_dic}  	bank     name_local
    ${param_bank_branch_area_local}     [Common] - Create string param of object in dic    ${arg_dic}  	bank     branch_area_local
    ${param_bank_branch_city_local}     [Common] - Create string param of object in dic    ${arg_dic}  	bank     branch_city_local
    ${param_acquiring_sale_executive_name_local}        [Common] - Create string param in dic    ${arg_dic}		acquiring_sale_executive_name_local
    ${param_relationship_manager_name_local}        [Common] - Create string param in dic    ${arg_dic}		relationship_manager_name_local
    ${param_sale_region_local}      [Common] - Create string param in dic    ${arg_dic}		sale_region_local
    ${param_commercial_account_manager_local}       [Common] - Create string param in dic    ${arg_dic}		commercial_account_manager_local
    ${param_star_rating}      [Common] - Create string param in dic    ${arg_dic}		star_rating
    ${param_warning_count}       [Common] - Create string param in dic    ${arg_dic}		warning_count
    ${param_is_sale}		[Common] - Create boolean param in dic    ${arg_dic}		is_sale
    ${param_sale_employee_id}		[Common] - Create string param in dic    ${arg_dic}		employee_id
    ${param_sale_calendar_id}		[Common] - Create string param in dic    ${arg_dic}		calendar_id
    ${param_company_id}		[Common] - Create int param in dic    ${arg_dic}	 company_id
    ${body}              catenate   SEPARATOR=
    ...          {
    ...              "profile": {
    ...                 ${param_is_testing_account}
    ...                 ${param_is_system_account}
    ...                 ${param_acquisition_source}
    ...                 ${param_star_rating}
    ...                 ${param_warning_count}
    ...                 "referrer_user_type": {
    ...                     ${param_referrer_user_type_id}
    ...                     ${param_referrer_user_type_name}
    ...                 },
    ...                 ${param_referrer_user_id}
    ...                 ${param_agent_type_id}
    ...                 ${param_unique_reference}
    ...                 ${param_mm_card_type_id}
    ...                 ${param_mm_card_level_id}
    ...                 ${param_mm_factory_card_number}
    ...                 ${param_model_type}
    ...                 ${param_is_require_otp}
    ...                 ${param_agent_classification_id}
    ...                 ${param_tin_number}
    ...                 ${param_title}
    ...                 ${param_first_name}
    ...                 ${param_middle_name}
    ...                 ${param_last_name}
    ...                 ${param_suffix}
    ...                 ${param_date_of_birth}
    ...                 ${param_place_of_birth}
    ...                 ${param_gender}
    ...                 ${param_ethnicity}
    ...                 ${param_nationality}
    ...                 ${param_occupation}
    ...                 ${param_occupation_title}
    ...                 ${param_township_code}
    ...                 ${param_township_name}
    ...                 ${param_national_id_number}
    ...                 ${param_mother_name}
    ...                 ${param_email}
    ...                 ${param_primary_mobile_number}
    ...                 ${param_secondary_mobile_number}
    ...                 ${param_tertiary_mobile_number}
    ...                 ${param_tin_number_local}
    ...                 ${param_title_local}
    ...                 ${param_first_name_local}
    ...                 ${param_middle_name_local}
    ...                 ${param_last_name_local}
    ...                 ${param_suffix_local}
    ...                 ${param_place_of_birth_local}
    ...                 ${param_gender_local}
    ...                 ${param_occupation_local}
    ...                 ${param_occupation_title_local}
    ...                 ${param_township_name_local}
    ...                 ${param_national_id_number_local}
    ...                 ${param_mother_name_local}
    ...                 ${param_company_id}
    ...                 "address": {
    ...                     "current_address": {
    ...      					${param_current_address_citizen_association}
    ...      					${param_current_address_neighbourhood_association}
    ...      					${param_current_address_address}
    ...      					${param_current_address_commune}
    ...      					${param_current_address_district}
    ...      					${param_current_address_city}
    ...      					${param_current_address_province}
    ...      					${param_current_address_postal_code}
    ...      					${param_current_address_country}
    ...      					${param_current_address_landmark}
    ...      					${param_current_address_longitude}
    ...      					${param_current_address_latitude}
    ...                         ${param_current_address_address_local}
    ...                         ${param_current_address_commune_local}
    ...                         ${param_current_address_district_local}
    ...                         ${param_current_address_city_local}
    ...                         ${param_current_address_province_local}
    ...                         ${param_current_address_postal_code_local}
    ...                         ${param_current_address_country_local}
    ...                     },
    ...                     "permanent_address": {
    ...      					${param_permanent_address_citizen_association}
    ...      					${param_permanent_address_neighbourhood_association}
    ...      					${param_permanent_address_address}
    ...      					${param_permanent_address_commune}
    ...      					${param_permanent_address_district}
    ...      					${param_permanent_address_city}
    ...      					${param_permanent_address_province}
    ...      					${param_permanent_address_postal_code}
    ...      					${param_permanent_address_country}
    ...      					${param_permanent_address_landmark}
    ...      					${param_permanent_address_longitude}
    ...      					${param_permanent_address_latitude}
    ...                         ${param_permanent_address_address_local}
    ...                         ${param_permanent_address_commune_local}
    ...                         ${param_permanent_address_district_local}
    ...                         ${param_permanent_address_city_local}
    ...                         ${param_permanent_address_province_local}
    ...                         ${param_permanent_address_postal_code_local}
    ...                         ${param_permanent_address_country_local}
    ...                     }
    ...                 },
    ...                 "bank":{
    ...      				${param_bank_name}
    ...      				${param_bank_account_status}
    ...      				${param_bank_account_name}
    ...      				${param_bank_account_number}
    ...      				${param_bank_branch_area}
    ...      				${param_bank_branch_city}
    ...      				${param_bank_register_date}
    ...      				${param_bank_register_source}
    ...      				${param_bank_is_verified}
    ...      				${param_bank_end_date}
    ...                     ${param_bank_name_local}
    ...                     ${param_bank_branch_area_local}
    ...                     ${param_bank_branch_city_local}
    ...                 },
    ...                 "contract": {
    ...      				${param_contract_type}
    ...      				${param_contract_number}
    ...      				${param_contract_extension_type}
    ...      				${param_contract_sign_date}
    ...      				${param_contract_issue_date}
    ...      				${param_contract_expired_date}
    ...      				${param_contract_notification_alert}
    ...      				${param_contract_day_of_period_reconciliation}
    ...      				${param_contract_release}
    ...      				${param_contract_file_url}
    ...      				${param_contract_assessment_information_url}
    ...                  },
    ...                 "accreditation": {
    ...                     "primary_identity": {
    ...      					${param_primary_identity_type}
    ...      					${param_primary_identity_status}
    ...      					${param_primary_identity_identity_id}
    ...      					${param_primary_identity_place_of_issue}
    ...      					${param_primary_identity_issue_date}
    ...      					${param_primary_identity_expired_date}
    ...      					${param_primary_identity_front_identity_url}
    ...      					${param_primary_identity_back_identity_url}
    ...                         ${param_primary_identity_identity_id_local}
    ...                     },
    ...                     "secondary_identity": {
    ...      					${param_secondary_identity_type}
    ...      					${param_secondary_identity_status}
    ...      					${param_secondary_identity_identity_id}
    ...      					${param_secondary_identity_place_of_issue}
    ...      					${param_secondary_identity_issue_date}
    ...      					${param_secondary_identity_expired_date}
    ...      					${param_secondary_identity_front_identity_url}
    ...      					${param_secondary_identity_back_identity_url}
    ...                         ${param_secondary_identity_identity_id_local}
    ...                     },
    ...      				${param_status_id}
    ...      				${param_remark}
    ...      				${param_verify_by}
    ...      				${param_verify_date}
    ...      				${param_risk_level}
    ...                 },
    ...                 "additional": {
    ...      				${param_additional_acquiring_sales_executive_id}
    ...      				${param_additional_acquiring_sales_executive_name}
    ...      				${param_additional_relationship_manager_id}
    ...      				${param_additional_relationship_manager_name}
    ...      				${param_additional_sale_region}
    ...      				${param_additional_commercial_account_manager}
    ...      				${param_additional_profile_picture_url}
    ...      				${param_additional_national_id_photo_url}
    ...      				${param_additional_tax_id_card_photo_url}
    ...      				${param_additional_field_1_name}
    ...      				${param_additional_field_1_value}
    ...      				${param_additional_field_2_name}
    ...      				${param_additional_field_2_value}
    ...      				${param_additional_field_3_name}
    ...      				${param_additional_field_3_value}
    ...      				${param_additional_field_4_name}
    ...      				${param_additional_field_4_value}
    ...      				${param_additional_field_5_name}
    ...      				${param_additional_field_5_value}
    ...      				${param_additional_supporting_file_1_url}
    ...      				${param_additional_supporting_file_2_url}
    ...      				${param_additional_supporting_file_3_url}
    ...      				${param_additional_supporting_file_4_url}
    ...      				${param_additional_supporting_file_5_url}
    ...                     ${param_acquiring_sale_executive_name_local}
    ...                     ${param_relationship_manager_name_local}
    ...                     ${param_sale_region_local}
    ...                     ${param_commercial_account_manager_local}
    ...                 },
    ...                 ${param_is_sale}
    ...                 "sale": {
    ...                     ${param_sale_employee_id}
    ...                     ${param_sale_calendar_id}
    ...                 }
    ...              },
    ...              "identity": {
    ...      			${param_identity_identity_type_id}
    ...      			${param_identity_username}
    ...      			${param_identity_password}
    ...      			${param_identity_auto_generate_password}
    ...              }
    ...          }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - create agent profiles - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_agent_type_id}		[Common] - Create int param in dic    ${arg_dic}  	agent_type_id		
    ${param_unique_reference}		[Common] - Create string param in dic    ${arg_dic}  	unique_reference		
    ${param_is_testing_account}		[Common] - Create boolean param in dic    ${arg_dic}  	is_testing_account		
    ${param_is_system_account}		[Common] - Create boolean param in dic    ${arg_dic}  	is_system_account		
    ${param_acquisition_source}		[Common] - Create string param in dic    ${arg_dic}  	acquisition_source		
    ${param_referrer_user_type_id}		[Common] - Create int param in dic    ${arg_dic}  	referrer_user_type_id
    ${param_referrer_user_type_name}		[Common] - Create string param in dic    ${arg_dic}  	referrer_user_type_name
    ${param_referrer_user_id}		[Common] - Create int param in dic    ${arg_dic}  	referrer_user_id		
    ${param_mm_card_type_id}		[Common] - Create int param in dic    ${arg_dic}  	mm_card_type_id		
    ${param_mm_card_level_id}		[Common] - Create int param in dic    ${arg_dic}  	mm_card_level_id		
    ${param_mm_factory_card_number}		[Common] - Create string param in dic    ${arg_dic}  	mm_factory_card_number		
    ${param_model_type}		[Common] - Create string param in dic    ${arg_dic}  	model_type		
    ${param_is_require_otp}		[Common] - Create boolean param in dic    ${arg_dic}  	is_require_otp		
    ${param_agent_classification_id}		[Common] - Create int param in dic    ${arg_dic}  	agent_classification_id		
    ${param_tin_number}		[Common] - Create string param in dic    ${arg_dic}  	tin_number		
    ${param_title}		[Common] - Create string param in dic    ${arg_dic}  	title		
    ${param_first_name}		[Common] - Create string param in dic    ${arg_dic}  	first_name		
    ${param_middle_name}		[Common] - Create string param in dic    ${arg_dic}  	middle_name		
    ${param_last_name}		[Common] - Create string param in dic    ${arg_dic}  	last_name		
    ${param_suffix}		[Common] - Create string param in dic    ${arg_dic}  	suffix		
    ${param_date_of_birth}		[Common] - Create string param in dic    ${arg_dic}  	date_of_birth		
    ${param_place_of_birth}		[Common] - Create string param in dic    ${arg_dic}  	place_of_birth		
    ${param_gender}		[Common] - Create string param in dic    ${arg_dic}  	gender		
    ${param_ethnicity}		[Common] - Create string param in dic    ${arg_dic}  	ethnicity		
    ${param_nationality}		[Common] - Create string param in dic    ${arg_dic}  	nationality		
    ${param_occupation}		[Common] - Create string param in dic    ${arg_dic}  	occupation		
    ${param_occupation_title}		[Common] - Create string param in dic    ${arg_dic}  	occupation_title		
    ${param_township_code}		[Common] - Create string param in dic    ${arg_dic}  	township_code		
    ${param_township_name}		[Common] - Create string param in dic    ${arg_dic}  	township_name		
    ${param_national_id_number}		[Common] - Create string param in dic    ${arg_dic}  	national_id_number		
    ${param_mother_name}		[Common] - Create string param in dic    ${arg_dic}  	mother_name		
    ${param_email}		[Common] - Create string param in dic    ${arg_dic}  	email		
    ${param_primary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}  	primary_mobile_number		
    ${param_secondary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}  	secondary_mobile_number		
    ${param_tertiary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}  	tertiary_mobile_number		
    ${param_current_address_citizen_association}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     citizen_association
    ${param_current_address_neighbourhood_association}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address    neighbourhood_association
    ${param_current_address_address}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address    address
    ${param_current_address_commune}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address    commune
    ${param_current_address_district}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address    district
    ${param_current_address_city}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address    city
    ${param_current_address_province}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address    province
    ${param_current_address_postal_code}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address    postal_code
    ${param_current_address_country}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address    country
    ${param_current_address_landmark}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address    landmark
    ${param_current_address_longitude}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address    longitude
    ${param_current_address_latitude}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address    latitude
    ${param_permanent_address_citizen_association}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address    citizen_association
    ${param_permanent_address_neighbourhood_association}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address    neighbourhood_association
    ${param_permanent_address_address}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address    address
    ${param_permanent_address_commune}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address    commune
    ${param_permanent_address_district}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address    district
    ${param_permanent_address_city}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address    city
    ${param_permanent_address_province}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address    province
    ${param_permanent_address_postal_code}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address    postal_code
    ${param_permanent_address_country}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address    country
    ${param_permanent_address_landmark}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address    landmark
    ${param_permanent_address_longitude}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address    longitude
    ${param_permanent_address_latitude}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address    latitude
    ${param_bank_name}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     name
    ${param_bank_account_status}		[Common] - Create int param of object in dic    ${arg_dic}  	bank     account_status
    ${param_bank_account_name}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     account_name
    ${param_bank_account_number}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     account_number
    ${param_bank_branch_area}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     branch_area
    ${param_bank_branch_city}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     branch_city
    ${param_bank_register_date}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     register_date
    ${param_bank_register_source}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     register_source
    ${param_bank_is_verified}		[Common] - Create boolean param of object in dic    ${arg_dic}  	bank     is_verified
    ${param_bank_end_date}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     end_date
    ${param_contract_type}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     type
    ${param_contract_number}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     number
    ${param_contract_extension_type}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     extension_type
    ${param_contract_sign_date}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     sign_date
    ${param_contract_issue_date}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     issue_date
    ${param_contract_expired_date}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     expired_date
    ${param_contract_notification_alert}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     notification_alert
    ${param_contract_day_of_period_reconciliation}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     day_of_period_reconciliation
    ${param_contract_release}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     release
    ${param_contract_file_url}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     file_url
    ${param_contract_assessment_information_url}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     assessment_information_url
    ${param_primary_identity_type}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     type
    ${param_primary_identity_status}		[Common] - Create int param of object in dic    ${arg_dic}  	primary_identity     status
    ${param_primary_identity_identity_id}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity    identity_id
    ${param_primary_identity_place_of_issue}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     place_of_issue
    ${param_primary_identity_issue_date}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     issue_date
    ${param_primary_identity_expired_date}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     expired_date
    ${param_primary_identity_front_identity_url}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     front_identity_url
    ${param_primary_identity_back_identity_url}		[Common] - Create string param of object in dic    ${arg_dic}  	primary_identity     back_identity_url
    ${param_secondary_identity_type}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     type
    ${param_secondary_identity_status}		[Common] - Create int param of object in dic    ${arg_dic}  	secondary_identity     status
    ${param_secondary_identity_identity_id}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     identity_id
    ${param_secondary_identity_place_of_issue}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     place_of_issue
    ${param_secondary_identity_issue_date}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     issue_date
    ${param_secondary_identity_expired_date}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     expired_date
    ${param_secondary_identity_front_identity_url}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     front_identity_url
    ${param_secondary_identity_back_identity_url}		[Common] - Create string param of object in dic    ${arg_dic}  	secondary_identity     back_identity_url
    ${param_status_id}		[Common] - Create int param in dic    ${arg_dic}  	status_id		
    ${param_remark}		[Common] - Create string param in dic    ${arg_dic}  	remark		
    ${param_verify_by}		[Common] - Create string param in dic    ${arg_dic}  	verify_by		
    ${param_verify_date}		[Common] - Create string param in dic    ${arg_dic}  	verify_date		
    ${param_risk_level}		[Common] - Create string param in dic    ${arg_dic}  	risk_level		
    ${param_additional_acquiring_sales_executive_id}		[Common] - Create int param of object in dic    ${arg_dic}  	additional     acquiring_sales_executive_id
    ${param_additional_acquiring_sales_executive_name}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     acquiring_sales_executive_name
    ${param_additional_relationship_manager_id}		[Common] - Create int param of object in dic    ${arg_dic}  	additional     relationship_manager_id
    ${param_additional_relationship_manager_name}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     relationship_manager_name
    ${param_additional_sale_region}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     sale_region
    ${param_additional_commercial_account_manager}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     commercial_account_manager
    ${param_additional_profile_picture_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     profile_picture_url
    ${param_additional_national_id_photo_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     national_id_photo_url
    ${param_additional_tax_id_card_photo_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     tax_id_card_photo_url
    ${param_additional_field_1_name}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_1_name
    ${param_additional_field_1_value}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_1_value
    ${param_additional_field_2_name}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_2_name
    ${param_additional_field_2_value}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_2_value
    ${param_additional_field_3_name}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_3_name
    ${param_additional_field_3_value}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_3_value
    ${param_additional_field_4_name}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_4_name
    ${param_additional_field_4_value}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_4_value
    ${param_additional_field_5_name}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_5_name
    ${param_additional_field_5_value}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     field_5_value
    ${param_additional_supporting_file_1_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     supporting_file_1_url
    ${param_additional_supporting_file_2_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     supporting_file_2_url
    ${param_additional_supporting_file_3_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     supporting_file_3_url
    ${param_additional_supporting_file_4_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     supporting_file_4_url
    ${param_additional_supporting_file_5_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     supporting_file_5_url
    ${param_is_sale}		[Common] - Create boolean param in dic    ${arg_dic}  	is_sale		
    ${param_sale_employee_id}		[Common] - Create string param of object in dic    ${arg_dic}  	sale     employee_id
    ${param_sale_calendar_id}		[Common] - Create string param of object in dic    ${arg_dic}  	sale     calendar_id
    ${param_company_id}		        [Common] - Create int param in dic    ${arg_dic}  	company_id

    ${body}              catenate   SEPARATOR=
    ...          {
    ...                 ${param_is_testing_account}
    ...                 ${param_is_system_account}
    ...                 ${param_acquisition_source}
    ...                 "referrer_user_type": {
    ...                     ${param_referrer_user_type_id}
    ...                     ${param_referrer_user_type_name}
    ...                 },
    ...                 ${param_referrer_user_id}
    ...                 ${param_agent_type_id}
    ...                 ${param_unique_reference}
    ...                 ${param_mm_card_type_id}
    ...                 ${param_mm_card_level_id}
    ...                 ${param_mm_factory_card_number}
    ...                 ${param_model_type}
    ...                 ${param_is_require_otp}
    ...                 ${param_agent_classification_id}
    ...                 ${param_tin_number}
    ...                 ${param_title}
    ...                 ${param_first_name}
    ...                 ${param_middle_name}
    ...                 ${param_last_name}
    ...                 ${param_suffix}
    ...                 ${param_date_of_birth}
    ...                 ${param_place_of_birth}
    ...                 ${param_gender}
    ...                 ${param_ethnicity}
    ...                 ${param_nationality}
    ...                 ${param_occupation}
    ...                 ${param_occupation_title}
    ...                 ${param_township_code}
    ...                 ${param_township_name}
    ...                 ${param_national_id_number}
    ...                 ${param_mother_name}
    ...                 ${param_email}
    ...                 ${param_primary_mobile_number}
    ...                 ${param_secondary_mobile_number}
    ...                 ${param_tertiary_mobile_number}
    ...                 "address": {
    ...                     "current_address": {
    ...      					${param_current_address_citizen_association}
    ...      					${param_current_address_neighbourhood_association}
    ...      					${param_current_address_address}
    ...      					${param_current_address_commune}
    ...      					${param_current_address_district}
    ...      					${param_current_address_city}
    ...      					${param_current_address_province}
    ...      					${param_current_address_postal_code}
    ...      					${param_current_address_country}
    ...      					${param_current_address_landmark}
    ...      					${param_current_address_longitude}
    ...      					${param_current_address_latitude}
    ...                     },
    ...                     "permanent_address": {
    ...      					${param_permanent_address_citizen_association}
    ...      					${param_permanent_address_neighbourhood_association}
    ...      					${param_permanent_address_address}
    ...      					${param_permanent_address_commune}
    ...      					${param_permanent_address_district}
    ...      					${param_permanent_address_city}
    ...      					${param_permanent_address_province}
    ...      					${param_permanent_address_postal_code}
    ...      					${param_permanent_address_country}
    ...      					${param_permanent_address_landmark}
    ...      					${param_permanent_address_longitude}
    ...      					${param_permanent_address_latitude}
    ...                     }
    ...                 },
    ...                 "bank":{
    ...      				${param_bank_name}
    ...      				${param_bank_account_status}
    ...      				${param_bank_account_name}
    ...      				${param_bank_account_number}
    ...      				${param_bank_branch_area}
    ...      				${param_bank_branch_city}
    ...      				${param_bank_register_date}
    ...      				${param_bank_register_source}
    ...      				${param_bank_is_verified}
    ...      				${param_bank_end_date}
    ...                 },
    ...                 "contract": {
    ...      				${param_contract_type}
    ...      				${param_contract_number}
    ...      				${param_contract_extension_type}
    ...      				${param_contract_sign_date}
    ...      				${param_contract_issue_date}
    ...      				${param_contract_expired_date}
    ...      				${param_contract_notification_alert}
    ...      				${param_contract_day_of_period_reconciliation}
    ...      				${param_contract_release}
    ...      				${param_contract_file_url}
    ...      				${param_contract_assessment_information_url}
    ...                  },
    ...                 "accreditation": {
    ...                     "primary_identity": {
    ...      					${param_primary_identity_type}
    ...      					${param_primary_identity_status}
    ...      					${param_primary_identity_identity_id}
    ...      					${param_primary_identity_place_of_issue}
    ...      					${param_primary_identity_issue_date}
    ...      					${param_primary_identity_expired_date}
    ...      					${param_primary_identity_front_identity_url}
    ...      					${param_primary_identity_back_identity_url}
    ...                     },
    ...                     "secondary_identity": {
    ...      					${param_secondary_identity_type}
    ...      					${param_secondary_identity_status}
    ...      					${param_secondary_identity_identity_id}
    ...      					${param_secondary_identity_place_of_issue}
    ...      					${param_secondary_identity_issue_date}
    ...      					${param_secondary_identity_expired_date}
    ...      					${param_secondary_identity_front_identity_url}
    ...      					${param_secondary_identity_back_identity_url}
    ...                     },
    ...      				${param_status_id}
    ...      				${param_remark}
    ...      				${param_verify_by}
    ...      				${param_verify_date}
    ...      				${param_risk_level}
    ...                 },
    ...                 "additional": {
    ...      				${param_additional_acquiring_sales_executive_id}
    ...      				${param_additional_acquiring_sales_executive_name}
    ...      				${param_additional_relationship_manager_id}
    ...      				${param_additional_relationship_manager_name}
    ...      				${param_additional_sale_region}
    ...      				${param_additional_commercial_account_manager}
    ...      				${param_additional_profile_picture_url}
    ...      				${param_additional_national_id_photo_url}
    ...      				${param_additional_tax_id_card_photo_url}
    ...      				${param_additional_field_1_name}
    ...      				${param_additional_field_1_value}
    ...      				${param_additional_field_2_name}
    ...      				${param_additional_field_2_value}
    ...      				${param_additional_field_3_name}
    ...      				${param_additional_field_3_value}
    ...      				${param_additional_field_4_name}
    ...      				${param_additional_field_4_value}
    ...      				${param_additional_field_5_name}
    ...      				${param_additional_field_5_value}
    ...      				${param_additional_supporting_file_1_url}
    ...      				${param_additional_supporting_file_2_url}
    ...      				${param_additional_supporting_file_3_url}
    ...      				${param_additional_supporting_file_4_url}
    ...      				${param_additional_supporting_file_5_url}
    ...                 },
    ...                 ${param_is_sale}
    ...                 "sale": {
    ...                     ${param_sale_employee_id}
    ...                     ${param_sale_calendar_id}
    ...                 },
    ...              ${param_company_id}
    ...              }
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - add agent identity - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...	{
        ...     "username":"${arg_dic.username}",
        ...     "password":"${arg_dic.password}",
        ...     "identity_type_id":${arg_dic.identity_type_id}
        ...	}
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - agent adds idenity - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_identity_type_id}=  [Common] - Create int param in dic    ${arg_dic}   identity_type_id
    ${param_username}=  [Common] - Create string param in dic    ${arg_dic}   username
    ${param_password}=  [Common] - Create string param in dic    ${arg_dic}   password
    ${param_user_id}=  [Common] - Create int param in dic    ${arg_dic}   user_id
    ${param_user_type_id}=  [Common] - Create string param in dic    ${arg_dic}   id
    ${param_user_type_name}=  [Common] - Create string param in dic    ${arg_dic}   name
    ${param_otp_reference_id}=  [Common] - Create string param in dic    ${arg_dic}   otp_reference_id
    ${body}              catenate           SEPARATOR=
        ...	{
        ...		${param_identity_type_id}
        ...		${param_username}
        ...		${param_password}
        ...		${param_user_id}
        ...     "user_type":{
        ...         ${param_user_type_id}
        ...         ${param_user_type_name}
        ...     },
        ...		${param_otp_reference_id}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - update agent identity - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_username}=  [Common] - Create string param in dic    ${arg_dic}   username
    ${body}              catenate           SEPARATOR=
        ...	{
        ...		${param_username}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - active-suspend agent - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...	{
        ...     "is_suspended":${arg_dic.is_suspended},
        ...     "active_suspend_reason": "${arg_dic.active_suspend_reason}"
        ...	}
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - create company type - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_name}=  [Common] - Create string param in dic     ${arg_dic}     name
    ${param_description}=  [Common] - Create string param in dic    ${arg_dic}     description
    ${body}              catenate           SEPARATOR=
        ...	{
        ...		${param_name}
        ...		${param_description}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - create company represetative profiles - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_representative_type_id}=                                        [Common] - Create string param in dic    ${arg_dic}             type_id
    ${param_representative_title}		                                    [Common] - Create string param in dic    ${arg_dic}     		title
    ${param_representative_first_name}		                                [Common] - Create string param in dic    ${arg_dic}     		first_name
    ${param_representative_middle_name}		                                [Common] - Create string param in dic    ${arg_dic}     		middle_name
    ${param_representative_last_name}		                                [Common] - Create string param in dic    ${arg_dic}     		last_name
    ${param_representative_suffix}		                                    [Common] - Create string param in dic    ${arg_dic}     		suffix
    ${param_representative_date_of_birth}		                            [Common] - Create string param in dic    ${arg_dic}     		date_of_birth
    ${param_representative_place_of_birth}		                            [Common] - Create string param in dic    ${arg_dic}     		place_of_birth
    ${param_representative_nationality}		                                [Common] - Create string param in dic    ${arg_dic}     		nationality
    ${param_representative_occupation}		                                [Common] - Create string param in dic    ${arg_dic}     		occupation
    ${param_representative_occupation_title}		                        [Common] - Create string param in dic    ${arg_dic}     		occupation_title
    ${param_representative_national_id_number}		                        [Common] - Create string param in dic    ${arg_dic}     		national_id_number
    ${param_representative_source_of_funds}		                            [Common] - Create string param in dic    ${arg_dic}     		source_of_funds
    ${param_representative_email}		                                    [Common] - Create string param in dic    ${arg_dic}     		email
    ${param_representative_mobile_number}		                            [Common] - Create string param in dic    ${arg_dic}     		mobile_number
    ${param_representative_business_phone_number}		                    [Common] - Create string param in dic    ${arg_dic}     		business_phone_number
    ${param_representative_current_address_citizen_association}		        [Common] - Create string param of object in dic    ${arg_dic}  	current_address     citizen_association
    ${param_representative_current_address_neighbourhood_association}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     neighbourhood_association
    ${param_representative_current_address_address}		                    [Common] - Create string param of object in dic    ${arg_dic}  	current_address     address
    ${param_representative_current_address_commune}		                    [Common] - Create string param of object in dic    ${arg_dic}  	current_address     commune
    ${param_representative_current_address_district}		                [Common] - Create string param of object in dic    ${arg_dic}  	current_address     district
    ${param_representative_current_address_city}		                    [Common] - Create string param of object in dic    ${arg_dic}  	current_address     city
    ${param_representative_current_address_province}		                [Common] - Create string param of object in dic    ${arg_dic}  	current_address     province
    ${param_representative_current_address_postal_code}		                [Common] - Create string param of object in dic    ${arg_dic}  	current_address     postal_code
    ${param_representative_current_address_country}		                    [Common] - Create string param of object in dic    ${arg_dic}  	current_address     country
    ${param_representative_current_address_landmark}		                [Common] - Create string param of object in dic    ${arg_dic}  	current_address     landmark
    ${param_representative_current_address_longitude}		                [Common] - Create string param of object in dic    ${arg_dic}  	current_address     longitude
    ${param_representative_current_address_latitude}		                [Common] - Create string param of object in dic    ${arg_dic}  	current_address     latitude
    ${param_representative_permanent_address_citizen_association}		    [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     citizen_association
    ${param_representative_permanent_address_neighbourhood_association}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     neighbourhood_association
    ${param_representative_permanent_address_address}		                [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     address
    ${param_representative_permanent_address_commune}		                [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     commune
    ${param_representative_permanent_address_district}		                [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     district
    ${param_representative_permanent_address_city}		                    [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     city
    ${param_representative_permanent_address_province}		                [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     province
    ${param_representative_permanent_address_postal_code}		            [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     postal_code
    ${param_representative_permanent_address_country}		                [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     country
    ${param_representative_permanent_address_landmark}		                [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     landmark
    ${param_representative_permanent_address_longitude}		                [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     longitude
    ${param_representative_permanent_address_latitude}		                [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     latitude
    ${body}              catenate           SEPARATOR=
        ...          {
        ...                     ${param_representative_type_id}
        ...                     ${param_representative_title}
        ...                     ${param_representative_first_name}
        ...                     ${param_representative_middle_name}
        ...                     ${param_representative_last_name}
        ...                     ${param_representative_suffix}
        ...                     ${param_representative_date_of_birth}
        ...                     ${param_representative_place_of_birth}
        ...                     ${param_representative_nationality}
        ...                     ${param_representative_occupation}
        ...                     ${param_representative_occupation_title}
        ...                     ${param_representative_national_id_number}
        ...                     ${param_representative_source_of_funds}
        ...                     ${param_representative_email}
        ...                     ${param_representative_mobile_number}
        ...                     ${param_representative_business_phone_number}
        ...                     "current_address": {
        ...      					${param_representative_current_address_citizen_association}
        ...      					${param_representative_current_address_neighbourhood_association}
        ...      					${param_representative_current_address_address}
        ...      					${param_representative_current_address_commune}
        ...      					${param_representative_current_address_district}
        ...      					${param_representative_current_address_city}
        ...      					${param_representative_current_address_province}
        ...      					${param_representative_current_address_postal_code}
        ...      					${param_representative_current_address_country}
        ...      					${param_representative_current_address_landmark}
        ...      					${param_representative_current_address_longitude}
        ...      					${param_representative_current_address_latitude}
        ...                     },
        ...                     "permanent_address": {
        ...      					${param_representative_permanent_address_citizen_association}
        ...      					${param_representative_permanent_address_neighbourhood_association}
        ...      					${param_representative_permanent_address_address}
        ...      					${param_representative_permanent_address_commune}
        ...      					${param_representative_permanent_address_district}
        ...      					${param_representative_permanent_address_city}
        ...      					${param_representative_permanent_address_province}
        ...      					${param_representative_permanent_address_postal_code}
        ...      					${param_representative_permanent_address_country}
        ...      					${param_representative_permanent_address_landmark}
        ...      				    ${param_representative_permanent_address_longitude}
        ...      					${param_representative_permanent_address_latitude}
        ...                     },
        ...            }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - update company represetative profiles - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_representative_type_id}=                                        [Common] - Create string param in dic    ${arg_dic}             type_id
    ${param_representative_title}		                                    [Common] - Create string param in dic    ${arg_dic}     		title
    ${param_representative_first_name}		                                [Common] - Create string param in dic    ${arg_dic}     		first_name
    ${param_representative_middle_name}		                                [Common] - Create string param in dic    ${arg_dic}     		middle_name
    ${param_representative_last_name}		                                [Common] - Create string param in dic    ${arg_dic}     		last_name
    ${param_representative_suffix}		                                    [Common] - Create string param in dic    ${arg_dic}     		suffix
    ${param_representative_date_of_birth}		                            [Common] - Create string param in dic    ${arg_dic}     		date_of_birth
    ${param_representative_place_of_birth}		                            [Common] - Create string param in dic    ${arg_dic}     		place_of_birth
    ${param_representative_nationality}		                                [Common] - Create string param in dic    ${arg_dic}     		nationality
    ${param_representative_occupation}		                                [Common] - Create string param in dic    ${arg_dic}     		occupation
    ${param_representative_occupation_title}		                        [Common] - Create string param in dic    ${arg_dic}     		occupation_title
    ${param_representative_national_id_number}		                        [Common] - Create string param in dic    ${arg_dic}     		national_id_number
    ${param_representative_source_of_funds}		                            [Common] - Create string param in dic    ${arg_dic}     		source_of_funds
    ${param_representative_email}		                                    [Common] - Create string param in dic    ${arg_dic}     		email
    ${param_representative_mobile_number}		                            [Common] - Create string param in dic    ${arg_dic}     		mobile_number
    ${param_representative_business_phone_number}		                    [Common] - Create string param in dic    ${arg_dic}     		business_phone_number
    ${param_representative_current_address_citizen_association}		        [Common] - Create string param of object in dic    ${arg_dic}  	current_address     citizen_association
    ${param_representative_current_address_neighbourhood_association}		[Common] - Create string param of object in dic    ${arg_dic}  	current_address     neighbourhood_association
    ${param_representative_current_address_address}		                    [Common] - Create string param of object in dic    ${arg_dic}  	current_address     address
    ${param_representative_current_address_commune}		                    [Common] - Create string param of object in dic    ${arg_dic}  	current_address     commune
    ${param_representative_current_address_district}		                [Common] - Create string param of object in dic    ${arg_dic}  	current_address     district
    ${param_representative_current_address_city}		                    [Common] - Create string param of object in dic    ${arg_dic}  	current_address     city
    ${param_representative_current_address_province}		                [Common] - Create string param of object in dic    ${arg_dic}  	current_address     province
    ${param_representative_current_address_postal_code}		                [Common] - Create string param of object in dic    ${arg_dic}  	current_address     postal_code
    ${param_representative_current_address_country}		                    [Common] - Create string param of object in dic    ${arg_dic}  	current_address     country
    ${param_representative_current_address_landmark}		                [Common] - Create string param of object in dic    ${arg_dic}  	current_address     landmark
    ${param_representative_current_address_longitude}		                [Common] - Create string param of object in dic    ${arg_dic}  	current_address     longitude
    ${param_representative_current_address_latitude}		                [Common] - Create string param of object in dic    ${arg_dic}  	current_address     latitude
    ${param_representative_permanent_address_citizen_association}		    [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     citizen_association
    ${param_representative_permanent_address_neighbourhood_association}		[Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     neighbourhood_association
    ${param_representative_permanent_address_address}		                [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     address
    ${param_representative_permanent_address_commune}		                [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     commune
    ${param_representative_permanent_address_district}		                [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     district
    ${param_representative_permanent_address_city}		                    [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     city
    ${param_representative_permanent_address_province}		                [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     province
    ${param_representative_permanent_address_postal_code}		            [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     postal_code
    ${param_representative_permanent_address_country}		                [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     country
    ${param_representative_permanent_address_landmark}		                [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     landmark
    ${param_representative_permanent_address_longitude}		                [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     longitude
    ${param_representative_permanent_address_latitude}		                [Common] - Create string param of object in dic    ${arg_dic}  	permanent_address     latitude
    ${body}              catenate           SEPARATOR=
        ...          {
        ...                     ${param_representative_type_id}
        ...                     ${param_representative_title}
        ...                     ${param_representative_first_name}
        ...                     ${param_representative_middle_name}
        ...                     ${param_representative_last_name}
        ...                     ${param_representative_suffix}
        ...                     ${param_representative_date_of_birth}
        ...                     ${param_representative_place_of_birth}
        ...                     ${param_representative_nationality}
        ...                     ${param_representative_occupation}
        ...                     ${param_representative_occupation_title}
        ...                     ${param_representative_national_id_number}
        ...                     ${param_representative_source_of_funds}
        ...                     ${param_representative_email}
        ...                     ${param_representative_mobile_number}
        ...                     ${param_representative_business_phone_number}
        ...                     "current_address": {
        ...      					${param_representative_current_address_citizen_association}
        ...      					${param_representative_current_address_neighbourhood_association}
        ...      					${param_representative_current_address_address}
        ...      					${param_representative_current_address_commune}
        ...      					${param_representative_current_address_district}
        ...      					${param_representative_current_address_city}
        ...      					${param_representative_current_address_province}
        ...      					${param_representative_current_address_postal_code}
        ...      					${param_representative_current_address_country}
        ...      					${param_representative_current_address_landmark}
        ...      					${param_representative_current_address_longitude}
        ...      					${param_representative_current_address_latitude}
        ...                     },
        ...                     "permanent_address": {
        ...      					${param_representative_permanent_address_citizen_association}
        ...      					${param_representative_permanent_address_neighbourhood_association}
        ...      					${param_representative_permanent_address_address}
        ...      					${param_representative_permanent_address_commune}
        ...      					${param_representative_permanent_address_district}
        ...      					${param_representative_permanent_address_city}
        ...      					${param_representative_permanent_address_province}
        ...      					${param_representative_permanent_address_postal_code}
        ...      					${param_representative_permanent_address_country}
        ...      					${param_representative_permanent_address_landmark}
        ...      				    ${param_representative_permanent_address_longitude}
        ...      					${param_representative_permanent_address_latitude}
        ...                     },
        ...            }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - create company profiles - body
    [Arguments]     ${output}=body    ${remove_null}=${True}   ${remove_empty}=${True}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
    ...          {
    ...            "company_type_id": "int",
    ...            "business_type": "string",
    ...            "business_field": "string",
    ...            "name": "string random_string(15,'${LETTERS}')",
    ...            "abbreviation": "string",
    ...            "tin_number": "string",
    ...            "risk_level": "string",
    ...            "is_testing_account": "string",
    ...            "mobile_number": "string",
    ...            "telephone_number": "string",
    ...            "email": "string",
    ...            "fax_number": "string",
    ...            "field_1_name": "string",
    ...            "field_1_value": "string",
    ...            "field_2_name": "string",
    ...            "field_2_value": "string",
    ...            "field_3_name": "string",
    ...            "field_3_value": "string",
    ...            "field_4_name": "string",
    ...            "field_4_value": "string",
    ...            "field_5_name": "string",
    ...            "field_5_value": "string",
    ...            "address": {
    ...              "citizen_association": "string",
    ...              "neighbourhood_association": "string",
    ...              "address": "string",
    ...              "city": "string",
    ...              "postal_code": "string",
    ...              "province": "string",
    ...              "district": "string",
    ...              "commune": "string",
    ...              "country": "string",
    ...              "landmark": "string",
    ...              "latitude": "string",
    ...              "longitude": "string"
    ...            },
    ...            "financial": {
    ...              "bank_name": "string",
    ...              "bank_account_status": "int",
    ...              "bank_account_name": "string",
    ...              "bank_account_number": "string",
    ...              "bank_branch_area": "string",
    ...              "bank_branch_city": "string",
    ...              "register_date": "string",
    ...              "register_source": "string",
    ...              "is_verified": true,
    ...              "end_date": "string",
    ...              "source_of_funds": "string"
    ...            },
    ...            "contract": {
    ...              "type": "string",
    ...              "number": "int",
    ...              "sign_date": "string",
    ...              "issue_date": "string",
    ...              "expired_date": "string",
    ...              "notification_alert": "string",
    ...              "extension_type": "string",
    ...              "day_of_period_reconciliation": "int",
    ...              "file_url": "string",
    ...              "assessment_information_url": "string"
    ...            },
    ...            "owner": {
    ...              "tin_number": "string",
    ...              "title": "string",
    ...              "first_name": "string",
    ...              "middle_name": "string",
    ...              "last_name": "string",
    ...              "date_of_birth": "string",
    ...              "gender": "string",
    ...              "ethnicity": "string",
    ...              "occupation": "string",
    ...              "occupation_title": "string",
    ...              "nationality": "string",
    ...              "national_id_number": "string",
    ...              "email": "string",
    ...              "primary_mobile_number": "string",
    ...              "secondary_mobile_number": "string",
    ...              "tertiary_mobile_number": "string",
    ...              "current_address": {
    ...                "citizen_association": "string",
    ...                "neighbourhood_association": "string",
    ...                "address": "string",
    ...                "city": "string",
    ...                "postal_code": "string",
    ...                "province": "string",
    ...                "district": "string",
    ...                "commune": "string",
    ...                "country": "string",
    ...                "landmark": "string",
    ...                "latitude": "string",
    ...                "longitude": "string"
    ...              },
    ...              "permanent_address": {
    ...                "citizen_association": "string",
    ...                "neighbourhood_association": "string",
    ...                "address": "string",
    ...                "city": "string",
    ...                "postal_code": "string",
    ...                "province": "string",
    ...                "district": "string",
    ...                "commune": "string",
    ...                "country": "string",
    ...                "landmark": "string",
    ...                "latitude": "string",
    ...                "longitude": "string"
    ...              }
    ...            }
    ...          }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - create salary group - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_salary_group_name}=  [Common] - Create string param in dic    ${arg_dic}    name
    ${body}          catenate           SEPARATOR=
        ...          {
        ...             ${param_salary_group_name}
        ...          }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - link customer to company - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_company_id}=  [Common] - Create string param in dic     ${arg_dic}    company_id
    ${param_salary_group_id}=  [Common] - Create string param in dic    ${arg_dic}    salary_group_id
    ${body}          catenate           SEPARATOR=
        ...          {
        ...             ${param_company_id}
        ...             ${param_salary_group_id}
        ...          }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - update company profiles - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_business_type}=  [Common] - Create string param in dic    ${arg_dic}   business_type
    ${param_business_field}=  [Common] - Create string param in dic    ${arg_dic}   business_field
    ${param_name}=  [Common] - Create string param in dic    ${arg_dic}   name
    ${param_abbreviation}=  [Common] - Create string param in dic    ${arg_dic}   abbreviation
    ${param_tin_number}		[Common] - Create string param in dic    ${arg_dic}  	tin_number
    ${param_risk_level}		[Common] - Create string param in dic    ${arg_dic}  	risk_level
    ${param_authorized_signatory}=  [Common] - Create string param in dic    ${arg_dic}   authorized_signatory
    ${param_is_testing_account}		[Common] - Create boolean param in dic    ${arg_dic}  	is_testing_account
    ${param_address_citizen_association}		[Common] - Create string param of object in dic    ${arg_dic}  	address     citizen_association
    ${param_address_neighbourhood_association}		[Common] - Create string param of object in dic    ${arg_dic}  	address     neighbourhood_association
    ${param_address_address}		[Common] - Create string param of object in dic    ${arg_dic}  	address     address
    ${param_address_commune}		[Common] - Create string param of object in dic    ${arg_dic}  	address     commune
    ${param_address_district}		[Common] - Create string param of object in dic    ${arg_dic}  	address     district
    ${param_address_city}		[Common] - Create string param of object in dic    ${arg_dic}  	address     city
    ${param_address_province}		[Common] - Create string param of object in dic    ${arg_dic}  	address     province
    ${param_address_postal_code}		[Common] - Create string param of object in dic    ${arg_dic}  	address     postal_code
    ${param_address_country}		[Common] - Create string param of object in dic    ${arg_dic}  	address     country
    ${param_address_landmark}		[Common] - Create string param of object in dic    ${arg_dic}  	address     landmark
    ${param_address_longitude}		[Common] - Create string param of object in dic    ${arg_dic}  	address     longitude
    ${param_address_latitude}		[Common] - Create string param of object in dic    ${arg_dic}  	address     latitude
    ${param_mobile_number}		[Common] - Create string param in dic    ${arg_dic}  	mobile_number
    ${param_telephone_number}		[Common] - Create string param in dic    ${arg_dic}  	telephone_number
    ${param_email}=  [Common] - Create string param in dic    ${arg_dic}   email
    ${param_fax_number}=  [Common] - Create string param in dic    ${arg_dic}   fax_number
    ${param_bank_name}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     name
    ${param_bank_account_status}		[Common] - Create int param of object in dic    ${arg_dic}  	bank     account_status
    ${param_bank_account_name}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     account_name
    ${param_bank_account_number}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     account_number
    ${param_bank_branch_area}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     branch_area
    ${param_bank_branch_city}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     branch_city
    ${param_bank_register_date}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     register_date
    ${param_bank_register_source}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     register_source
    ${param_bank_is_verified}		[Common] - Create boolean param of object in dic    ${arg_dic}  	bank     is_verified
    ${param_bank_end_date}		[Common] - Create string param of object in dic    ${arg_dic}  	bank     end_date
    ${param_bank_source_of_funds}=  [Common] - Create string param of object in dic    ${arg_dic}  	bank     source_of_funds
    ${param_contract_type}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     type
    ${param_contract_number}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     number
    ${param_contract_extension_type}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     extension_type
    ${param_contract_sign_date}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     sign_date
    ${param_contract_issue_date}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     issue_date
    ${param_contract_expired_date}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     expired_date
    ${param_contract_notification_alert}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     notification_alert
    ${param_contract_day_of_period_reconciliation}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     day_of_period_reconciliation
    ${param_contract_file_url}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     file_url
    ${param_contract_assessment_information_url}		[Common] - Create string param of object in dic    ${arg_dic}  	contract     assessment_information_url
    ${param_owner_tin_number}		[Common] - Create string param of object in dic    ${arg_dic}  	owner     tin_number
    ${param_owner_title}		[Common] - Create string param of object in dic    ${arg_dic}  	owner     title
    ${param_owner_first_name}		[Common] - Create string param of object in dic    ${arg_dic}  	owner     first_name
    ${param_owner_middle_name}		[Common] - Create string param of object in dic    ${arg_dic}  	owner     middle_name
    ${param_owner_last_name}		[Common] - Create string param of object in dic    ${arg_dic}  	owner     last_name
    ${param_owner_suffix}		[Common] - Create string param of object in dic    ${arg_dic}  	owner     suffix
    ${param_owner_date_of_birth}		[Common] - Create string param of object in dic    ${arg_dic}  	owner     date_of_birth
    ${param_owner_place_of_birth}		[Common] - Create string param of object in dic    ${arg_dic}  	owner     place_of_birth
    ${param_owner_gender}		[Common] - Create string param of object in dic    ${arg_dic}  	owner     gender
    ${param_owner_ethnicity}		[Common] - Create string param of object in dic    ${arg_dic}  	owner     ethnicity
    ${param_owner_nationality}		[Common] - Create string param of object in dic    ${arg_dic}  	owner     nationality
    ${param_owner_occupation}		[Common] - Create string param of object in dic    ${arg_dic}  	owner     occupation
    ${param_owner_occupation_title}		[Common] - Create string param of object in dic    ${arg_dic}  	owner     occupation_title
    ${param_owner_township_code}		[Common] - Create string param of object in dic    ${arg_dic}  	owner     township_code
    ${param_owner_township_name}		[Common] - Create string param of object in dic    ${arg_dic}  	owner     township_name
    ${param_owner_national_id_number}		[Common] - Create string param of object in dic    ${arg_dic}  	owner     national_id_number
    ${param_owner_mother_name}		[Common] - Create string param of object in dic    ${arg_dic}  	owner     mother_name
    ${param_owner_email}		[Common] - Create string param of object in dic    ${arg_dic}  	owner     email
    ${param_owner_primary_mobile_number}		[Common] - Create string param of object in dic    ${arg_dic}  	owner     primary_mobile_number
    ${param_owner_econdary_mobile_number}		[Common] - Create string param of object in dic    ${arg_dic}  	owner     secondary_mobile_number
    ${param_owner_tertiary_mobile_number}		[Common] - Create string param of object in dic    ${arg_dic}  	owner     tertiary_mobile_number
    ${param_owner_current_address_citizen_association}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_current_address     citizen_association
    ${param_owner_current_address_neighbourhood_association}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_current_address     neighbourhood_association
    ${param_owner_current_address_address}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_current_address     address
    ${param_owner_current_address_commune}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_current_address     commune
    ${param_owner_current_address_district}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_current_address     district
    ${param_owner_current_address_city}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_current_address     city
    ${param_owner_current_address_province}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_current_address     province
    ${param_owner_current_address_postal_code}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_current_address     postal_code
    ${param_owner_current_address_country}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_current_address     country
    ${param_owner_current_address_landmark}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_current_address     landmark
    ${param_owner_current_address_longitude}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_current_address     longitude
    ${param_owner_current_address_latitude}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_current_address     latitude
    ${param_owner_permanent_address_citizen_association}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_permanent_address     citizen_association
    ${param_owner_permanent_address_neighbourhood_association}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_permanent_address     neighbourhood_association
    ${param_owner_permanent_address_address}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_permanent_address     address
    ${param_owner_permanent_address_commune}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_permanent_address     commune
    ${param_owner_permanent_address_district}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_permanent_address     district
    ${param_owner_permanent_address_city}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_permanent_address     city
    ${param_owner_permanent_address_province}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_permanent_address     province
    ${param_owner_permanent_address_postal_code}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_permanent_address     postal_code
    ${param_owner_permanent_address_country}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_permanent_address     country
    ${param_owner_permanent_address_landmark}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_permanent_address     landmark
    ${param_owner_permanent_address_longitude}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_permanent_address     longitude
    ${param_owner_permanent_address_latitude}		[Common] - Create string param of object in dic    ${arg_dic}  	owner_permanent_address     latitude
    ${param_representative_tin_number}		[Common] - Create string param of object in dic    ${arg_dic}  	representative     tin_number
    ${param_representative_title}		[Common] - Create string param of object in dic    ${arg_dic}  	representative     title
    ${param_representative_first_name}		[Common] - Create string param of object in dic    ${arg_dic}  	representative     first_name
    ${param_representative_middle_name}		[Common] - Create string param of object in dic    ${arg_dic}  	representative     middle_name
    ${param_representative_last_name}		[Common] - Create string param of object in dic    ${arg_dic}  	representative     last_name
    ${param_representative_suffix}		[Common] - Create string param of object in dic    ${arg_dic}  	representative     suffix
    ${param_representative_date_of_birth}		[Common] - Create string param of object in dic    ${arg_dic}  	representative     date_of_birth
    ${param_representative_place_of_birth}		[Common] - Create string param of object in dic    ${arg_dic}  	representative     place_of_birth
    ${param_representative_gender}		[Common] - Create string param of object in dic    ${arg_dic}  	representative     gender
    ${param_representative_ethnicity}		[Common] - Create string param of object in dic    ${arg_dic}  	representative     ethnicity
    ${param_representative_nationality}		[Common] - Create string param of object in dic    ${arg_dic}  	representative     nationality
    ${param_representative_occupation}		[Common] - Create string param of object in dic    ${arg_dic}  	representative     occupation
    ${param_representative_occupation_title}		[Common] - Create string param of object in dic    ${arg_dic}  	representative     occupation_title
    ${param_representative_township_code}		[Common] - Create string param of object in dic    ${arg_dic}  	representative     township_code
    ${param_representative_township_name}		[Common] - Create string param of object in dic    ${arg_dic}  	representative     township_name
    ${param_representative_national_id_number}		[Common] - Create string param of object in dic    ${arg_dic}  	representative     national_id_number
    ${param_representative_mother_name}		[Common] - Create string param of object in dic    ${arg_dic}  	representative     mother_name
    ${param_representative_email}		[Common] - Create string param of object in dic    ${arg_dic}  	representative     email
    ${param_representative_primary_mobile_number}		[Common] - Create string param of object in dic    ${arg_dic}  	representative     primary_mobile_number
    ${param_representative_secondary_mobile_number}		[Common] - Create string param of object in dic    ${arg_dic}  	representative     secondary_mobile_number
    ${param_representative_tertiary_mobile_number}		[Common] - Create string param of object in dic    ${arg_dic}  	representative     tertiary_mobile_number
    ${param_representative_current_address_citizen_association}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_current_address     citizen_association
    ${param_representative_current_address_neighbourhood_association}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_current_address     neighbourhood_association
    ${param_representative_current_address_address}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_current_address     address
    ${param_representative_current_address_commune}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_current_address     commune
    ${param_representative_current_address_district}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_current_address     district
    ${param_representative_current_address_city}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_current_address     city
    ${param_representative_current_address_province}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_current_address     province
    ${param_representative_current_address_postal_code}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_current_address     postal_code
    ${param_representative_current_address_country}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_current_address     country
    ${param_representative_current_address_landmark}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_current_address     landmark
    ${param_representative_current_address_longitude}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_current_address     longitude
    ${param_representative_current_address_latitude}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_current_address     latitude
    ${param_representative_permanent_address_citizen_association}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_permanent_address     citizen_association
    ${param_representative_permanent_address_neighbourhood_association}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_permanent_address     neighbourhood_association
    ${param_representative_permanent_address_address}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_permanent_address     address
    ${param_representative_permanent_address_commune}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_permanent_address     commune
    ${param_representative_permanent_address_district}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_permanent_address     district
    ${param_representative_permanent_address_city}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_permanent_address     city
    ${param_representative_permanent_address_province}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_permanent_address     province
    ${param_representative_permanent_address_postal_code}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_permanent_address     postal_code
    ${param_representative_permanent_address_country}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_permanent_address     country
    ${param_representative_permanent_address_landmark}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_permanent_address     landmark
    ${param_representative_permanent_address_longitude}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_permanent_address     longitude
    ${param_representative_permanent_address_latitude}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_permanent_address     latitude
    ${param_representative_primary_identity_type}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_primary_identity     type
    ${param_representative_primary_identity_status}		[Common] - Create int param of object in dic    ${arg_dic}  	representative_primary_identity     status
    ${param_representative_primary_identity_identity_id}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_primary_identity     identity_id
    ${param_representative_primary_identity_place_of_issue}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_primary_identity     place_of_issue
    ${param_representative_primary_identity_issue_date}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_primary_identity     issue_date
    ${param_representative_primary_identity_expired_date}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_primary_identity     expired_date
    ${param_representative_primary_identity_front_identity_url}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_primary_identity     front_identity_url
    ${param_representative_primary_identity_back_identity_url}		[Common] - Create string param of object in dic    ${arg_dic}  	representative_primary_identity     back_identity_url
    ${param_field_1_name}		[Common] - Create string param in dic    ${arg_dic}  	field_1_name
    ${param_field_1_value}		[Common] - Create string param in dic    ${arg_dic}  	field_1_value
    ${param_field_2_name}		[Common] - Create string param in dic    ${arg_dic}  	field_2_name
    ${param_field_2_value}		[Common] - Create string param in dic    ${arg_dic}  	field_2_value
    ${param_field_3_name}		[Common] - Create string param in dic    ${arg_dic}  	field_3_name
    ${param_field_3_value}		[Common] - Create string param in dic    ${arg_dic}  	field_3_value
    ${param_field_4_name}		[Common] - Create string param in dic    ${arg_dic}  	field_4_name
    ${param_field_4_value}		[Common] - Create string param in dic    ${arg_dic}  	field_4_value
    ${param_field_5_name}		[Common] - Create string param in dic    ${arg_dic}  	field_5_name
    ${param_field_5_value}		[Common] - Create string param in dic    ${arg_dic}  	field_5_value
    ${body}              catenate           SEPARATOR=
        ...          {
        ...                ${param_business_type}
        ...                ${param_business_field}
        ...                ${param_name}
        ...                ${param_abbreviation}
        ...                ${param_tin_number}
        ...                ${param_risk_level}
        ...                ${param_authorized_signatory}
        ...                ${param_is_testing_account}
        ...                "address": {
        ...      					${param_address_citizen_association}
        ...      					${param_address_neighbourhood_association}
        ...      					${param_address_address}
        ...      					${param_address_commune}
        ...      					${param_address_district}
        ...      					${param_address_city}
        ...      					${param_address_province}
        ...      					${param_address_postal_code}
        ...      					${param_address_country}
        ...      					${param_address_landmark}
        ...      					${param_address_longitude}
        ...      					${param_address_latitude}
        ...                 },
        ...                 ${param_mobile_number}
        ...                 ${param_telephone_number}
        ...                 ${param_email}
        ...                 ${param_fax_number}
        ...                 "financial":{
        ...      				${param_bank_name}
        ...      				${param_bank_account_status}
        ...      				${param_bank_account_name}
        ...      				${param_bank_account_number}
        ...      				${param_bank_branch_area}
        ...      				${param_bank_branch_city}
        ...      				${param_bank_register_date}
        ...      				${param_bank_register_source}
        ...      				${param_bank_is_verified}
        ...      				${param_bank_end_date}
        ...                     ${param_bank_source_of_funds}
        ...                 },
        ...                 "contract": {
        ...      				${param_contract_type}
        ...      				${param_contract_number}
        ...      				${param_contract_extension_type}
        ...      				${param_contract_sign_date}
        ...      				${param_contract_issue_date}
        ...      				${param_contract_expired_date}
        ...      				${param_contract_notification_alert}
        ...      				${param_contract_day_of_period_reconciliation}
        ...      				${param_contract_file_url}
        ...      				${param_contract_assessment_information_url}
        ...                  },
        ...                 "owner": {
        ...                     ${param_owner_tin_number}
        ...                     ${param_owner_title}
        ...                     ${param_owner_first_name}
        ...                     ${param_owner_middle_name}
        ...                     ${param_owner_last_name}
        ...                     ${param_owner_suffix}
        ...                     ${param_owner_date_of_birth}
        ...                     ${param_owner_place_of_birth}
        ...                     ${param_owner_gender}
        ...                     ${param_owner_ethnicity}
        ...                     ${param_owner_nationality}
        ...                     ${param_owner_occupation}
        ...                     ${param_owner_occupation_title}
        ...                     ${param_owner_township_code}
        ...                     ${param_owner_township_name}
        ...                     ${param_owner_national_id_number}
        ...                     ${param_owner_mother_name}
        ...                     ${param_owner_email}
        ...                     ${param_owner_primary_mobile_number}
        ...                     ${param_owner_econdary_mobile_number}
        ...                     ${param_owner_tertiary_mobile_number}
        ...                     "address": {
        ...                         "current_address": {
        ...      					    ${param_owner_current_address_citizen_association}
        ...      					    ${param_owner_current_address_neighbourhood_association}
        ...      					    ${param_owner_current_address_address}
        ...      					    ${param_owner_current_address_commune}
        ...      					    ${param_owner_current_address_district}
        ...      					    ${param_owner_current_address_city}
        ...      					    ${param_owner_current_address_province}
        ...      					    ${param_owner_current_address_postal_code}
        ...      					    ${param_owner_current_address_country}
        ...      					    ${param_owner_current_address_landmark}
        ...      					    ${param_owner_current_address_longitude}
        ...      					    ${param_owner_current_address_latitude}
        ...                         },
        ...                         "permanent_address": {
        ...      					    ${param_owner_permanent_address_citizen_association}
        ...      					    ${param_owner_permanent_address_neighbourhood_association}
        ...      					    ${param_owner_permanent_address_address}
        ...      					    ${param_owner_permanent_address_commune}
        ...      					    ${param_owner_permanent_address_district}
        ...      					    ${param_owner_permanent_address_city}
        ...      					    ${param_owner_permanent_address_province}
        ...      					    ${param_owner_permanent_address_postal_code}
        ...      					    ${param_owner_permanent_address_country}
        ...      					    ${param_owner_permanent_address_landmark}
        ...      					    ${param_owner_permanent_address_longitude}
        ...      					    ${param_owner_permanent_address_latitude}
        ...                         }
        ...                     }
        ...                 },
        ...                 "representative": {
        ...                     ${param_representative_tin_number}
        ...                     ${param_representative_title}
        ...                     ${param_representative_first_name}
        ...                     ${param_representative_middle_name}
        ...                     ${param_representative_last_name}
        ...                     ${param_representative_suffix}
        ...                     ${param_representative_date_of_birth}
        ...                     ${param_representative_place_of_birth}
        ...                     ${param_representative_gender}
        ...                     ${param_representative_ethnicity}
        ...                     ${param_representative_nationality}
        ...                     ${param_representative_occupation}
        ...                     ${param_representative_occupation_title}
        ...                     ${param_representative_township_code}
        ...                     ${param_representative_township_name}
        ...                     ${param_representative_national_id_number}
        ...                     ${param_representative_mother_name}
        ...                     ${param_representative_email}
        ...                     ${param_representative_primary_mobile_number}
        ...                     ${param_representative_secondary_mobile_number}
        ...                     ${param_representative_tertiary_mobile_number}
        ...                     "address": {
        ...                         "current_address": {
        ...      					    ${param_representative_current_address_citizen_association}
        ...      					    ${param_representative_current_address_neighbourhood_association}
        ...      					    ${param_representative_current_address_address}
        ...      					    ${param_representative_current_address_commune}
        ...      					    ${param_representative_current_address_district}
        ...      					    ${param_representative_current_address_city}
        ...      					    ${param_representative_current_address_province}
        ...      					    ${param_representative_current_address_postal_code}
        ...      					    ${param_representative_current_address_country}
        ...      					    ${param_representative_current_address_landmark}
        ...      					    ${param_representative_current_address_longitude}
        ...      					    ${param_representative_current_address_latitude}
        ...                         },
        ...                         "permanent_address": {
        ...      					    ${param_representative_permanent_address_citizen_association}
        ...      					    ${param_representative_permanent_address_neighbourhood_association}
        ...      					    ${param_representative_permanent_address_address}
        ...      					    ${param_representative_permanent_address_commune}
        ...      					    ${param_representative_permanent_address_district}
        ...      					    ${param_representative_permanent_address_city}
        ...      					    ${param_representative_permanent_address_province}
        ...      					    ${param_representative_permanent_address_postal_code}
        ...      					    ${param_representative_permanent_address_country}
        ...      					    ${param_representative_permanent_address_landmark}
        ...      					    ${param_representative_permanent_address_longitude}
        ...      					    ${param_representative_permanent_address_latitude}
        ...                         }
        ...                     },
        ...                     "primary_identity": {
        ...                         ${param_representative_primary_identity_type}
        ...                         ${param_representative_primary_identity_status}
        ...                         ${param_representative_primary_identity_identity_id}
        ...                         ${param_representative_primary_identity_place_of_issue}
        ...                         ${param_representative_primary_identity_issue_date}
        ...                         ${param_representative_primary_identity_expired_date}
        ...                         ${param_representative_primary_identity_front_identity_url}
        ...                         ${param_representative_primary_identity_back_identity_url}
        ...                     }
        ...                 },
        ...      		    ${param_field_1_name}
        ...      		    ${param_field_1_value}
        ...      			${param_field_2_name}
        ...      			${param_field_2_value}
        ...      			${param_field_3_name}
        ...      			${param_field_3_value}
        ...      			${param_field_4_name}
        ...      			${param_field_4_value}
        ...      			${param_field_5_name}
        ...      			${param_field_5_value}
        ...                 }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - create smart card - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_serial_number}=  [Common] - Create string param in dic    ${arg_dic}   serial_number
    ${body}              catenate           SEPARATOR=
        ...	{
        ...		${param_serial_number}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - update smart card - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_contain_password}=  [Common] - Create string param in dic    ${arg_dic}   contain_password
    ${param_identity_id}=  [Common] - Create string param in dic    ${arg_dic}   identity_id
    ${body}              catenate           SEPARATOR=
        ...	{
        ...		${param_contain_password}
        ...		${param_identity_id}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - create shop type - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...    {
        ...            "name":"${arg_dic.name}",
        ...            "description":"${arg_dic.description}"
        ...    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - update shop type - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_name}=  [Common] - Create string param in dic    ${arg_dic}   name
    ${param_description}=  [Common] - Create string param in dic    ${arg_dic}   description
    ${body}              catenate           SEPARATOR=
        ...	{
        ...		${param_name}
        ...		${param_description}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - create shop category - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...    {
        ...            "name":"${arg_dic.name}",
        ...            "description":"${arg_dic.description}"
        ...    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - update shop category - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_name}=  [Common] - Create string param in dic    ${arg_dic}   name
    ${param_description}=  [Common] - Create string param in dic    ${arg_dic}   description
    ${body}              catenate           SEPARATOR=
        ...	{
        ...		${param_name}
        ...		${param_description}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - create shop - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_agent_id}		[Common] - Create string param in dic    ${arg_dic}  	agent_id
    ${param_shop_category_id}		[Common] - Create string param in dic    ${arg_dic}  	shop_category_id
    ${param_name}		[Common] - Create string param in dic    ${arg_dic}  	name
    ${param_name_local}		[Common] - Create string param in dic    ${arg_dic}  	name_local
    ${param_acquisition_source}		[Common] - Create string param in dic    ${arg_dic}  	acquisition_source
    ${param_shop_mobile_number}		[Common] - Create string param in dic    ${arg_dic}  	shop_mobile_number
    ${param_shop_telephone_number}		[Common] - Create string param in dic    ${arg_dic}  	shop_telephone_number
    ${param_shop_email}		[Common] - Create string param in dic    ${arg_dic}  	shop_email
    ${param_truemoney_signage}		[Common] - Create string param in dic    ${arg_dic}  	truemoney_signage
    ${param_requested_services}		[Common] - Create string param in dic    ${arg_dic}  	requested_services
    ${param_network_coverage}		[Common] - Create string param in dic    ${arg_dic}  	network_coverage
    ${param_premises}		[Common] - Create string param in dic    ${arg_dic}  	premises
    ${param_physical_security}		[Common] - Create string param in dic    ${arg_dic}  	physical_security
    ${param_number_of_staff}		[Common] - Create string param in dic    ${arg_dic}  	number_of_staff
    ${param_business_hour}		[Common] - Create string param in dic    ${arg_dic}  	business_hour
    ${param_shop_type_id}		[Common] - Create string param in dic    ${arg_dic}  	shop_type_id
    ${param_address_citizen_association}		[Common] - Create string param of object in dic    ${arg_dic}  	address     citizen_association
    ${param_address_neighbourhood_association}		[Common] - Create string param of object in dic    ${arg_dic}  	address     neighbourhood_association
    ${param_address_address}		[Common] - Create string param of object in dic    ${arg_dic}  	address     address
    ${param_address_commune}		[Common] - Create string param of object in dic    ${arg_dic}  	address     commune
    ${param_address_district}		[Common] - Create string param of object in dic    ${arg_dic}  	address     district
    ${param_address_city}		[Common] - Create string param of object in dic    ${arg_dic}  	address     city
    ${param_address_province}		[Common] - Create string param of object in dic    ${arg_dic}  	address     province
    ${param_address_postal_code}		[Common] - Create string param of object in dic    ${arg_dic}  	address     postal_code
    ${param_address_country}		[Common] - Create string param of object in dic    ${arg_dic}  	address     country
    ${param_address_landmark}		[Common] - Create string param of object in dic    ${arg_dic}  	address     landmark
    ${param_address_longitude}		[Common] - Create string param of object in dic    ${arg_dic}  	address     longitude
    ${param_address_latitude}		[Common] - Create string param of object in dic    ${arg_dic}  	address     latitude
    ${param_address_citizen_association_local}		[Common] - Create string param of object in dic    ${arg_dic}  	address     citizen_association_local
    ${param_address_neighbourhood_association_local}		[Common] - Create string param of object in dic    ${arg_dic}  	address     neighbourhood_association_local
    ${param_address_address_local}		            [Common] - Create string param of object in dic    ${arg_dic}  	address     address_local
    ${param_address_commune_local}		            [Common] - Create string param of object in dic    ${arg_dic}  	address     commune_local
    ${param_address_district_local}		            [Common] - Create string param of object in dic    ${arg_dic}  	address     district_local
    ${param_address_city_local}		                [Common] - Create string param of object in dic    ${arg_dic}  	address     city_local
    ${param_address_province_local}		            [Common] - Create string param of object in dic    ${arg_dic}  	address     province_local
    ${param_address_postal_code_local}		        [Common] - Create string param of object in dic    ${arg_dic}  	address     postal_code_local
    ${param_address_country_local}		    [Common] - Create string param of object in dic    ${arg_dic}  	address     country_local
    ${param_address_landmark_local}		            [Common] - Create string param of object in dic    ${arg_dic}  	address     landmark_local

    ${param_additional_supporting_file_1_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     supporting_file_1_url
    ${param_additional_supporting_file_2_url}		[Common] - Create string param of object in dic    ${arg_dic}  	additional     supporting_file_2_url
    ${param_ref_1}		                            [Common] - Create string param in dic    ${arg_dic}  	ref1
    ${param_ref_2}		                            [Common] - Create string param in dic    ${arg_dic}  	ref2
    ${param_ref_1_local}		                    [Common] - Create string param in dic    ${arg_dic}  	ref1_local
    ${param_ref_2_local}		                    [Common] - Create string param in dic    ${arg_dic}  	ref2_local
    ${param_additional_acquiring_sale_executive_name}		                            [Common] - Create string param of object in dic    ${arg_dic}  	additional     acquiring_sale_executive_name
    ${param_additional_acquiring_sale_executive_name_local}		                            [Common] - Create string param of object in dic    ${arg_dic}  	additional     acquiring_sale_executive_name_local
    ${param_additional_relationship_manager_name}		                            [Common] - Create string param of object in dic    ${arg_dic}  	additional     relationship_manager_name
    ${param_additional_relationship_manager_name_local}		                            [Common] - Create string param of object in dic    ${arg_dic}  	additional     relationship_manager_name_local
    ${param_additional_relationship_manager_id}		                            [Common] - Create string param of object in dic    ${arg_dic}  	additional     relationship_manager_id
    ${param_additional_relationship_manager_email}		                            [Common] - Create string param of object in dic    ${arg_dic}  	additional     relationship_manager_email
    ${param_additional_sale_region}		                            [Common] - Create string param of object in dic    ${arg_dic}  	additional     sale_region
    ${param_additional_account_manager_name}		                            [Common] - Create string param of object in dic    ${arg_dic}  	additional     account_manager_name
    ${param_additional_account_manager_name_local}		                            [Common] - Create string param of object in dic    ${arg_dic}  	additional     account_manager_name_local
    ${param_additional_shop_photo}		                            [Common] - Create string param of object in dic    ${arg_dic}  	additional     shop_photo
    ${param_additional_shop_map}		                            [Common] - Create string param of object in dic    ${arg_dic}  	additional     shop_map
    ${param_representative_first_name}		                            [Common] - Create string param of object in dic    ${arg_dic}  	representative     first_name
    ${param_representative_first_name_local}		                            [Common] - Create string param of object in dic    ${arg_dic}  	representative     first_name_local
    ${param_representative_middle_name}		                            [Common] - Create string param of object in dic    ${arg_dic}  	representative     middle_name
    ${param_representative_middle_name_local}		                            [Common] - Create string param of object in dic    ${arg_dic}  	representative     middle_name_local
    ${param_representative_last_name}		                            [Common] - Create string param of object in dic    ${arg_dic}  	representative     last_name
    ${param_representative_last_name_local}		                            [Common] - Create string param of object in dic    ${arg_dic}  	representative     last_name_local
    ${param_representative_mobile_number}		                            [Common] - Create string param of object in dic    ${arg_dic}  	representative     mobile_number
    ${param_representative_telephone_number}		                            [Common] - Create string param of object in dic    ${arg_dic}  	representative     telephone_number
    ${param_representative_email}		                            [Common] - Create string param of object in dic    ${arg_dic}  	representative     email


    ${body}              catenate   SEPARATOR=
    ...          {
    ...                 ${param_agent_id}
    ...                 ${param_shop_type_id}
    ...                 ${param_name}
    ...                 ${param_name_local}
    ...                 ${param_shop_category_id}
    ...                 ${param_acquisition_source}
    ...                 ${param_truemoney_signage}
    ...                 ${param_requested_services}
    ...                 ${param_network_coverage}
    ...                 ${param_physical_security}
    ...                 ${param_premises}
    ...                 ${param_number_of_staff}
    ...                 ${param_business_hour}
    ...                 ${param_shop_mobile_number}
    ...                 ${param_shop_telephone_number}
    ...                 ${param_shop_email}
    ...                 "address": {
    ...      					${param_address_citizen_association}
    ...      					${param_address_neighbourhood_association}
    ...      					${param_address_address}
    ...      					${param_address_commune}
    ...      					${param_address_district}
    ...      					${param_address_city}
    ...      					${param_address_province}
    ...      					${param_address_postal_code}
    ...      					${param_address_country}
    ...      					${param_address_landmark}
    ...      					${param_address_longitude}
    ...      					${param_address_latitude}
     ...      					${param_address_citizen_association_local}
    ...      					${param_address_neighbourhood_association_local}
    ...      					${param_address_address_local}
    ...      					${param_address_commune_local}
    ...      					${param_address_district_local}
    ...      					${param_address_city_local}
    ...      					${param_address_province_local}
    ...      					${param_address_postal_code_local}
    ...      					${param_address_country_local}
    ...      					${param_address_landmark_local}
    ...                 },
    ...                 "additional": {
    ...      				${param_additional_supporting_file_1_url}
    ...      				${param_additional_supporting_file_2_url}
    ...      				${param_ref_1}
    ...      				${param_ref_2}
    ...      				${param_ref_1_local}
    ...      				${param_ref_2_local}
    ...      				${param_additional_relationship_manager_id}
    ...      				${param_additional_relationship_manager_name}
    ...      				${param_additional_relationship_manager_name_local}
    ...      				${param_additional_relationship_manager_email}
    ...      				${param_additional_acquiring_sale_executive_name}
    ...      				${param_additional_acquiring_sale_executive_name_local}
    ...      				${param_additional_sale_region}
    ...      				${param_additional_account_manager_name}
    ...      				${param_additional_account_manager_name_local}
    ...      				${param_additional_shop_photo}
    ...      				${param_additional_shop_map}
    ...                 },
    ...                 "representative": {
    ...      				${param_representative_first_name}
    ...      				${param_representative_first_name_local}
    ...      				${param_representative_last_name}
    ...      				${param_representative_last_name_local}
    ...      				${param_representative_middle_name}
    ...      				${param_representative_middle_name_local}
    ...      				${param_representative_mobile_number}
    ...      				${param_representative_telephone_number}
    ...      				${param_representative_email}
    ...                 }
    ...          }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - update shop - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_name}=  [Common] - Create string param in dic    ${arg_dic}   name
    ${param_agent_id}		[Common] - Create int param in dic    ${arg_dic}  	agent_id
    ${param_shop_type_id}		[Common] - Create int param in dic    ${arg_dic}  	shop_type_id
    ${param_shop_category_id}		[Common] - Create int param in dic    ${arg_dic}  	shop_category_id
    ${param_address}		[Common] - Create string param in dic    ${arg_dic}  	address
    ${param_city}		[Common] - Create string param in dic    ${arg_dic}  	city
    ${param_province}		[Common] - Create string param in dic    ${arg_dic}  	province
    ${param_district}		[Common] - Create string param in dic    ${arg_dic}  	district
    ${param_commune}		[Common] - Create string param in dic    ${arg_dic}  	commune
    ${param_country}		[Common] - Create string param in dic    ${arg_dic}  	country
    ${param_landmark}		[Common] - Create string param in dic    ${arg_dic}  	landmark
    ${param_latitude}		[Common] - Create string param in dic    ${arg_dic}  	latitude
    ${param_longitude}		[Common] - Create string param in dic    ${arg_dic}  	longitude
    ${param_relationship_manager_id}		[Common] - Create string param in dic    ${arg_dic}  	relationship_manager_id
    ${param_acquisition_source}		[Common] - Create string param in dic    ${arg_dic}  	acquisition_source
    ${param_postal_code}		[Common] - Create string param in dic    ${arg_dic}  	postal_code
    ${param_representative_first_name}		[Common] - Create string param in dic    ${arg_dic}  	representative_first_name
    ${param_representative_middle_name}		[Common] - Create string param in dic    ${arg_dic}  	representative_middle_name
    ${param_representative_last_name}		[Common] - Create string param in dic    ${arg_dic}  	representative_last_name
    ${param_representative_mobile_number}		[Common] - Create string param in dic    ${arg_dic}  	representative_mobile_number
    ${param_representative_telephone_number}		[Common] - Create string param in dic    ${arg_dic}  	representative_telephone_number
    ${param_representative_email}		[Common] - Create string param in dic    ${arg_dic}  	representative_email
    ${param_shop_mobile_number}		[Common] - Create string param in dic    ${arg_dic}  	shop_mobile_number
    ${param_shop_telephone_number}		[Common] - Create string param in dic    ${arg_dic}  	shop_telephone_number
    ${param_shop_email}		[Common] - Create string param in dic    ${arg_dic}  	shop_email
    ${param_relationship_manager_name}		[Common] - Create string param in dic    ${arg_dic}  	relationship_manager_name
    ${param_relationship_manager_email}		[Common] - Create string param in dic    ${arg_dic}  	relationship_manager_email
    ${param_acquiring_sales_executive_name}		[Common] - Create string param in dic    ${arg_dic}  	acquiring_sales_executive_name
    ${param_sales_region}		[Common] - Create string param in dic    ${arg_dic}  	sales_region
    ${param_account_manager_name}		[Common] - Create string param in dic    ${arg_dic}  	account_manager_name
    ${param_ref1}		[Common] - Create string param in dic    ${arg_dic}  	ref1
    ${param_ref2}		[Common] - Create string param in dic    ${arg_dic}  	ref2
    ${body}              catenate           SEPARATOR=
    ...    {
    ...            ${param_agent_id}
    ...            ${param_shop_type_id}
    ...            ${param_name}
    ...            ${param_shop_category_id}
    ...            "address": {
    ...                ${param_address}
    ...                ${param_city}
    ...                ${param_province}
    ...                ${param_district}
    ...                ${param_commune}
    ...                ${param_country}
    ...                ${param_landmark}
    ...                ${param_latitude}
    ...                ${param_longitude}
    ...            },
    ...            ${param_relationship_manager_id}
    ...            ${param_acquisition_source}
    ...            ${param_postal_code}
    ...            ${param_representative_first_name}
    ...            ${param_representative_middle_name}
    ...            ${param_representative_last_name}
    ...            ${param_representative_mobile_number}
    ...            ${param_representative_telephone_number}
    ...            ${param_representative_email}
    ...            ${param_shop_mobile_number}
    ...            ${param_shop_telephone_number}
    ...            ${param_shop_email}
    ...            ${param_relationship_manager_name}
    ...            ${param_relationship_manager_email}
    ...            ${param_acquiring_sales_executive_name}
    ...            ${param_sales_region}
    ...            ${param_account_manager_name}
    ...            ${param_ref1}
    ...            ${param_ref2}
    ...    }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - create edc - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_shop_id}=  [Common] - Create string param in dic    ${arg_dic}   shop_id
    ${param_terminal_id}=  [Common] - Create string param in dic    ${arg_dic}   terminal_id
    ${param_serial_number}=  [Common] - Create string param in dic    ${arg_dic}   serial_number
    ${param_sim_number}=  [Common] - Create string param in dic    ${arg_dic}   sim_number
    ${param_battery_number}=  [Common] - Create string param in dic    ${arg_dic}   battery_number
    ${param_adapter_number}=  [Common] - Create string param in dic    ${arg_dic}   adapter_number
    ${param_version}=  [Common] - Create string param in dic    ${arg_dic}   version
    ${param_location}=  [Common] - Create string param in dic    ${arg_dic}   location
    ${param_need_repair}=  [Common] - Create boolean param in dic    ${arg_dic}   need_repair
    ${param_need_return}=  [Common] - Create boolean param in dic    ${arg_dic}   need_return
    ${body}              catenate           SEPARATOR=
        ...	{
        ...		${param_shop_id}
        ...		${param_terminal_id}
        ...		${param_serial_number}
        ...		${param_sim_number}
        ...		${param_battery_number}
        ...		${param_adapter_number}
        ...		${param_version}
        ...		${param_location}
        ...		${param_need_repair}
        ...		${param_need_return}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - update edc - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_shop_id}=  [Common] - Create string param in dic    ${arg_dic}   shop_id
    ${param_terminal_id}=  [Common] - Create string param in dic    ${arg_dic}   terminal_id
    ${param_serial_number}=  [Common] - Create string param in dic    ${arg_dic}   serial_number
    ${param_sim_number}=  [Common] - Create string param in dic    ${arg_dic}   sim_number
    ${param_battery_number}=  [Common] - Create string param in dic    ${arg_dic}   battery_number
    ${param_adapter_number}=  [Common] - Create string param in dic    ${arg_dic}   adapter_number
    ${param_version}=  [Common] - Create string param in dic    ${arg_dic}   version
    ${param_location}=  [Common] - Create string param in dic    ${arg_dic}   location
    ${param_need_repair}=  [Common] - Create string param in dic    ${arg_dic}   need_repair
    ${param_need_return}=  [Common] - Create string param in dic    ${arg_dic}   need_return
    ${body}              catenate           SEPARATOR=
        ...    {
        ...     ${param_shop_id}
        ...		${param_terminal_id}
        ...		${param_serial_number}
        ...		${param_sim_number}
        ...		${param_battery_number}
        ...		${param_adapter_number}
        ...		${param_version}
        ...		${param_location}
        ...		${param_need_repair}
        ...		${param_need_return}
        ...    }
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - get relationship - params
    [Arguments]     ${output}=params    &{arg_dic}
    ${params}              catenate          SEPARATOR=&
        ...     relationship_type_id=${arg_dic.relationship_type_id}
        ...     role=${arg_dic.role}
    [Common] - Set variable       name=${output}      value=${params}

[Agent][Prepare] - update relationship - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_is_sharing_benefit}=  [Common] - Create boolean param in dic    ${arg_dic}   is_sharing_benefit
    ${body}              catenate           SEPARATOR=
        ...  {
        ...		${param_is_sharing_benefit}
        ...  }
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[agent][prepare] - update agent relationship sharing benefit - body
   [Arguments]     ${output}=body    ${remove_null}=${True}   ${remove_empty}=${True}    &{arg_dic}
    ${schema}              catenate           SEPARATOR=
        ...  {
        ...		"is_sharing_benefit": "bool"
        ...  }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - agent create device - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_channel_type_id}		[Common] - Create int param in dic    ${arg_dic}  	channel_type_id
    ${param_channel_id}		[Common] - Create int param in dic    ${arg_dic}  	channel_id
    ${param_shop_id}		[Common] - Create int param in dic    ${arg_dic}  	shop_id
    ${param_mac_address}		[Common] - Create string param in dic    ${arg_dic}  	mac_address
    ${param_network_provider_name}		[Common] - Create string param in dic    ${arg_dic}  	network_provider_name
    ${param_public_ip_address}		[Common] - Create string param in dic    ${arg_dic}  	public_ip_address
    ${param_supporting_file_1}		[Common] - Create string param in dic    ${arg_dic}  	supporting_file_1
    ${param_supporting_file_2}		[Common] - Create string param in dic    ${arg_dic}  	supporting_file_2
    ${param_device_name}		[Common] - Create string param in dic    ${arg_dic}  	device_name
    ${param_device_model}		[Common] - Create string param in dic    ${arg_dic}  	device_model
    ${param_device_unique_reference}		[Common] - Create string param in dic    ${arg_dic}  	device_unique_reference
    ${param_os}		[Common] - Create string param in dic    ${arg_dic}  	os
    ${param_os_version}		[Common] - Create string param in dic    ${arg_dic}  	os_version
    ${param_display_size_in_inches}		[Common] - Create string param in dic    ${arg_dic}  	display_size_in_inches
    ${param_pixel_counts}		[Common] - Create string param in dic    ${arg_dic}  	pixel_counts
    ${param_unique_number}		[Common] - Create string param in dic    ${arg_dic}  	unique_number
    ${param_serial_number}		[Common] - Create string param in dic    ${arg_dic}  	serial_number
    ${param_app_version}		[Common] - Create string param in dic    ${arg_dic}  	app_version
    ${param_edc_serial_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_serial_number
    ${param_edc_model}		[Common] - Create string param in dic    ${arg_dic}  	edc_model
    ${param_edc_firmware_version}		[Common] - Create string param in dic    ${arg_dic}  	edc_firmware_version
    ${param_edc_software_version}		[Common] - Create string param in dic    ${arg_dic}  	edc_software_version
    ${param_edc_sim_card_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_sim_card_number
    ${param_edc_battery_serial_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_battery_serial_number
    ${param_edc_adapter_serial_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_adapter_serial_number
    ${param_edc_smartcard_1_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_smartcard_1_number
    ${param_edc_smartcard_2_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_smartcard_2_number
    ${param_edc_smartcard_3_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_smartcard_3_number
    ${param_edc_smartcard_4_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_smartcard_4_number
    ${param_edc_smartcard_5_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_smartcard_5_number
    ${param_pos_serial_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_serial_number
    ${param_pos_model}		[Common] - Create string param in dic    ${arg_dic}  	pos_model
    ${param_pos_firmware_version}		[Common] - Create string param in dic    ${arg_dic}  	pos_firmware_version
    ${param_pos_software_version}		[Common] - Create string param in dic    ${arg_dic}  	pos_software_version
    ${param_pos_smartcard_1_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_smartcard_1_number
    ${param_pos_smartcard_2_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_smartcard_2_number
    ${param_pos_smartcard_3_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_smartcard_3_number
    ${param_pos_smartcard_4_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_smartcard_4_number
    ${param_pos_smartcard_5_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_smartcard_5_number
    ${body}              catenate       SEPARATOR=
    ...    {
    ...            ${param_channel_type_id}
    ...            ${param_channel_id}
    ...            ${param_shop_id}
    ...            ${param_mac_address}
    ...            ${param_network_provider_name}
    ...            ${param_public_ip_address}
    ...            ${param_supporting_file_1}
    ...            ${param_supporting_file_2}
    ...            ${param_device_name}
    ...            ${param_device_model}
    ...            ${param_device_unique_reference}
    ...            ${param_os}
    ...            ${param_os_version}
    ...            ${param_display_size_in_inches}
    ...            ${param_pixel_counts}
    ...            ${param_unique_number}
    ...            ${param_serial_number}
    ...            ${param_edc_serial_number}
    ...            ${param_edc_model}
    ...            ${param_edc_firmware_version}
    ...            ${param_edc_software_version}
    ...            ${param_edc_sim_card_number}
    ...            ${param_edc_battery_serial_number}
    ...            ${param_edc_adapter_serial_number}
    ...            ${param_edc_smartcard_1_number}
    ...            ${param_edc_smartcard_2_number}
    ...            ${param_edc_smartcard_3_number}
    ...            ${param_edc_smartcard_4_number}
    ...            ${param_edc_smartcard_5_number}
    ...            ${param_pos_serial_number}
    ...            ${param_pos_model}
    ...            ${param_pos_firmware_version}
    ...            ${param_pos_software_version}
    ...            ${param_pos_smartcard_1_number}
    ...            ${param_pos_smartcard_2_number}
    ...            ${param_pos_smartcard_3_number}
    ...            ${param_pos_smartcard_4_number}
    ...            ${param_pos_smartcard_5_number}
    ...    }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - system user creates agent device - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_channel_type_id}		[Common] - Create int param in dic    ${arg_dic}  	channel_type_id
    ${param_channel_id}		[Common] - Create int param in dic    ${arg_dic}  	channel_id
    ${param_shop_id}		[Common] - Create int param in dic    ${arg_dic}  	shop_id
    ${param_owner_id}		[Common] - Create int param in dic    ${arg_dic}  	owner_id
    ${param_mac_address}		[Common] - Create string param in dic    ${arg_dic}  	mac_address
    ${param_network_provider_name}		[Common] - Create string param in dic    ${arg_dic}  	network_provider_name
    ${param_public_ip_address}		[Common] - Create string param in dic    ${arg_dic}  	public_ip_address
    ${param_supporting_file_1}		[Common] - Create string param in dic    ${arg_dic}  	supporting_file_1
    ${param_supporting_file_2}		[Common] - Create string param in dic    ${arg_dic}  	supporting_file_2
    ${param_device_name}		[Common] - Create string param in dic    ${arg_dic}  	device_name
    ${param_device_model}		[Common] - Create string param in dic    ${arg_dic}  	device_model
    ${param_device_unique_reference}		[Common] - Create string param in dic    ${arg_dic}  	device_unique_reference
    ${param_os}		[Common] - Create string param in dic    ${arg_dic}  	os
    ${param_os_version}		[Common] - Create string param in dic    ${arg_dic}  	os_version
    ${param_display_size_in_inches}		[Common] - Create string param in dic    ${arg_dic}  	display_size_in_inches
    ${param_pixel_counts}		[Common] - Create string param in dic    ${arg_dic}  	pixel_counts
    ${param_unique_number}		[Common] - Create string param in dic    ${arg_dic}  	unique_number
    ${param_serial_number}		[Common] - Create string param in dic    ${arg_dic}  	serial_number
    ${param_app_version}		[Common] - Create string param in dic    ${arg_dic}  	app_version
    ${param_ssid}		[Common] - Create string param in dic    ${arg_dic}  	ssid
    ${param_edc_serial_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_serial_number
    ${param_edc_model}		[Common] - Create string param in dic    ${arg_dic}  	edc_model
    ${param_edc_firmware_version}		[Common] - Create string param in dic    ${arg_dic}  	edc_firmware_version
    ${param_edc_software_version}		[Common] - Create string param in dic    ${arg_dic}  	edc_software_version
    ${param_edc_sim_card_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_sim_card_number
    ${param_edc_battery_serial_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_battery_serial_number
    ${param_edc_adapter_serial_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_adapter_serial_number
    ${param_edc_smartcard_1_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_smartcard_1_number
    ${param_edc_smartcard_2_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_smartcard_2_number
    ${param_edc_smartcard_3_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_smartcard_3_number
    ${param_edc_smartcard_4_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_smartcard_4_number
    ${param_edc_smartcard_5_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_smartcard_5_number
    ${param_pos_serial_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_serial_number
    ${param_pos_model}		[Common] - Create string param in dic    ${arg_dic}  	pos_model
    ${param_pos_firmware_version}		[Common] - Create string param in dic    ${arg_dic}  	pos_firmware_version
    ${param_pos_software_version}		[Common] - Create string param in dic    ${arg_dic}  	pos_software_version
    ${param_pos_smartcard_1_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_smartcard_1_number
    ${param_pos_smartcard_2_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_smartcard_2_number
    ${param_pos_smartcard_3_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_smartcard_3_number
    ${param_pos_smartcard_4_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_smartcard_4_number
    ${param_pos_smartcard_5_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_smartcard_5_number
    ${body}              catenate           SEPARATOR=
        ...	{
        ...            ${param_channel_type_id}
        ...            ${param_channel_id}
        ...            ${param_shop_id}
        ...            ${param_owner_id}
        ...            ${param_mac_address}
        ...            ${param_network_provider_name}
        ...            ${param_public_ip_address}
        ...            ${param_supporting_file_1}
        ...            ${param_supporting_file_2}
        ...            ${param_device_name}
        ...            ${param_device_model}
        ...            ${param_device_unique_reference}
        ...            ${param_os}
        ...            ${param_os_version}
        ...            ${param_display_size_in_inches}
        ...            ${param_pixel_counts}
        ...            ${param_unique_number}
        ...            ${param_serial_number}
        ...            ${param_app_version}
        ...            ${param_ssid}
        ...            ${param_edc_serial_number}
        ...            ${param_edc_model}
        ...            ${param_edc_firmware_version}
        ...            ${param_edc_software_version}
        ...            ${param_edc_sim_card_number}
        ...            ${param_edc_battery_serial_number}
        ...            ${param_edc_adapter_serial_number}
        ...            ${param_edc_smartcard_1_number}
        ...            ${param_edc_smartcard_2_number}
        ...            ${param_edc_smartcard_3_number}
        ...            ${param_edc_smartcard_4_number}
        ...            ${param_edc_smartcard_5_number}
        ...            ${param_pos_serial_number}
        ...            ${param_pos_model}
        ...            ${param_pos_firmware_version}
        ...            ${param_pos_software_version}
        ...            ${param_pos_smartcard_1_number}
        ...            ${param_pos_smartcard_2_number}
        ...            ${param_pos_smartcard_3_number}
        ...            ${param_pos_smartcard_4_number}
        ...            ${param_pos_smartcard_5_number}

        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - agent updates device - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_channel_type_id}		[Common] - Create int param in dic    ${arg_dic}  	channel_type_id
    ${param_channel_id}		[Common] - Create int param in dic    ${arg_dic}  	channel_id
    ${param_shop_id}		[Common] - Create int param in dic    ${arg_dic}  	shop_id
    ${param_mac_address}		[Common] - Create string param in dic    ${arg_dic}  	mac_address
    ${param_network_provider_name}		[Common] - Create string param in dic    ${arg_dic}  	network_provider_name
    ${param_public_ip_address}		[Common] - Create string param in dic    ${arg_dic}  	public_ip_address
    ${param_supporting_file_1}		[Common] - Create string param in dic    ${arg_dic}  	supporting_file_1
    ${param_supporting_file_2}		[Common] - Create string param in dic    ${arg_dic}  	supporting_file_2
    ${param_device_name}		[Common] - Create string param in dic    ${arg_dic}  	device_name
    ${param_device_model}		[Common] - Create string param in dic    ${arg_dic}  	device_model
    ${param_device_unique_reference}		[Common] - Create string param in dic    ${arg_dic}  	device_unique_reference
    ${param_os}		[Common] - Create string param in dic    ${arg_dic}  	os
    ${param_os_version}		[Common] - Create string param in dic    ${arg_dic}  	os_version
    ${param_display_size_in_inches}		[Common] - Create string param in dic    ${arg_dic}  	display_size_in_inches
    ${param_pixel_counts}		[Common] - Create string param in dic    ${arg_dic}  	pixel_counts
    ${param_unique_number}		[Common] - Create string param in dic    ${arg_dic}  	unique_number
    ${param_serial_number}		[Common] - Create string param in dic    ${arg_dic}  	serial_number
    ${param_app_version}		[Common] - Create string param in dic    ${arg_dic}  	app_version
    ${param_edc_serial_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_serial_number
    ${param_edc_model}		[Common] - Create string param in dic    ${arg_dic}  	edc_model
    ${param_edc_firmware_version}		[Common] - Create string param in dic    ${arg_dic}  	edc_firmware_version
    ${param_edc_software_version}		[Common] - Create string param in dic    ${arg_dic}  	edc_software_version
    ${param_edc_sim_card_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_sim_card_number
    ${param_edc_battery_serial_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_battery_serial_number
    ${param_edc_adapter_serial_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_adapter_serial_number
    ${param_edc_smartcard_1_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_smartcard_1_number
    ${param_edc_smartcard_2_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_smartcard_2_number
    ${param_edc_smartcard_3_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_smartcard_3_number
    ${param_edc_smartcard_4_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_smartcard_4_number
    ${param_edc_smartcard_5_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_smartcard_5_number
    ${param_pos_serial_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_serial_number
    ${param_pos_model}		[Common] - Create string param in dic    ${arg_dic}  	pos_model
    ${param_pos_firmware_version}		[Common] - Create string param in dic    ${arg_dic}  	pos_firmware_version
    ${param_pos_software_version}		[Common] - Create string param in dic    ${arg_dic}  	pos_software_version
    ${param_pos_smartcard_1_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_smartcard_1_number
    ${param_pos_smartcard_2_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_smartcard_2_number
    ${param_pos_smartcard_3_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_smartcard_3_number
    ${param_pos_smartcard_4_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_smartcard_4_number
    ${param_pos_smartcard_5_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_smartcard_5_number
    ${body}              catenate           SEPARATOR=
        ...	{
        ...            ${param_channel_type_id}
        ...            ${param_channel_id}
        ...            ${param_shop_id}
        ...            ${param_mac_address}
        ...            ${param_network_provider_name}
        ...            ${param_public_ip_address}
        ...            ${param_supporting_file_1}
        ...            ${param_supporting_file_2}
        ...            ${param_device_name}
        ...            ${param_device_model}
        ...            ${param_device_unique_reference}
        ...            ${param_os}
        ...            ${param_os_version}
        ...            ${param_display_size_in_inches}
        ...            ${param_pixel_counts}
        ...            ${param_unique_number}
        ...            ${param_serial_number}
        ...            ${param_edc_serial_number}
        ...            ${param_edc_model}
        ...            ${param_edc_firmware_version}
        ...            ${param_edc_software_version}
        ...            ${param_edc_sim_card_number}
        ...            ${param_edc_battery_serial_number}
        ...            ${param_edc_adapter_serial_number}
        ...            ${param_edc_smartcard_1_number}
        ...            ${param_edc_smartcard_2_number}
        ...            ${param_edc_smartcard_3_number}
        ...            ${param_edc_smartcard_4_number}
        ...            ${param_edc_smartcard_5_number}
        ...            ${param_pos_serial_number}
        ...            ${param_pos_model}
        ...            ${param_pos_firmware_version}
        ...            ${param_pos_software_version}
        ...            ${param_pos_smartcard_1_number}
        ...            ${param_pos_smartcard_2_number}
        ...            ${param_pos_smartcard_3_number}
        ...            ${param_pos_smartcard_4_number}
        ...            ${param_pos_smartcard_5_number}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - system user updates agent device - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_channel_type_id}		[Common] - Create int param in dic    ${arg_dic}  	channel_type_id
    ${param_channel_id}		[Common] - Create int param in dic    ${arg_dic}  	channel_id
    ${param_shop_id}		[Common] - Create int param in dic    ${arg_dic}  	shop_id
    ${param_mac_address}		[Common] - Create string param in dic    ${arg_dic}  	mac_address
    ${param_network_provider_name}		[Common] - Create string param in dic    ${arg_dic}  	network_provider_name
    ${param_public_ip_address}		[Common] - Create string param in dic    ${arg_dic}  	public_ip_address
    ${param_supporting_file_1}		[Common] - Create string param in dic    ${arg_dic}  	supporting_file_1
    ${param_supporting_file_2}		[Common] - Create string param in dic    ${arg_dic}  	supporting_file_2
    ${param_device_name}		[Common] - Create string param in dic    ${arg_dic}  	device_name
    ${param_device_model}		[Common] - Create string param in dic    ${arg_dic}  	device_model
    ${param_device_unique_reference}		[Common] - Create string param in dic    ${arg_dic}  	device_unique_reference
    ${param_os}		[Common] - Create string param in dic    ${arg_dic}  	os
    ${param_os_version}		[Common] - Create string param in dic    ${arg_dic}  	os_version
    ${param_display_size_in_inches}		[Common] - Create string param in dic    ${arg_dic}  	display_size_in_inches
    ${param_pixel_counts}		[Common] - Create string param in dic    ${arg_dic}  	pixel_counts
    ${param_unique_number}		[Common] - Create string param in dic    ${arg_dic}  	unique_number
    ${param_serial_number}		[Common] - Create string param in dic    ${arg_dic}  	serial_number
    ${param_app_version}		[Common] - Create string param in dic    ${arg_dic}  	app_version
    ${param_edc_serial_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_serial_number
    ${param_edc_model}		[Common] - Create string param in dic    ${arg_dic}  	edc_model
    ${param_edc_firmware_version}		[Common] - Create string param in dic    ${arg_dic}  	edc_firmware_version
    ${param_edc_software_version}		[Common] - Create string param in dic    ${arg_dic}  	edc_software_version
    ${param_edc_sim_card_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_sim_card_number
    ${param_edc_battery_serial_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_battery_serial_number
    ${param_edc_adapter_serial_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_adapter_serial_number
    ${param_edc_smartcard_1_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_smartcard_1_number
    ${param_edc_smartcard_2_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_smartcard_2_number
    ${param_edc_smartcard_3_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_smartcard_3_number
    ${param_edc_smartcard_4_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_smartcard_4_number
    ${param_edc_smartcard_5_number}		[Common] - Create string param in dic    ${arg_dic}  	edc_smartcard_5_number
    ${param_pos_serial_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_serial_number
    ${param_pos_model}		[Common] - Create string param in dic    ${arg_dic}  	pos_model
    ${param_pos_firmware_version}		[Common] - Create string param in dic    ${arg_dic}  	pos_firmware_version
    ${param_pos_software_version}		[Common] - Create string param in dic    ${arg_dic}  	pos_software_version
    ${param_pos_smartcard_1_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_smartcard_1_number
    ${param_pos_smartcard_2_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_smartcard_2_number
    ${param_pos_smartcard_3_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_smartcard_3_number
    ${param_pos_smartcard_4_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_smartcard_4_number
    ${param_pos_smartcard_5_number}		[Common] - Create string param in dic    ${arg_dic}  	pos_smartcard_5_number
    ${body}              catenate           SEPARATOR=
        ...	{
        ...            ${param_channel_type_id}
        ...            ${param_channel_id}
        ...            ${param_shop_id}
        ...            ${param_mac_address}
        ...            ${param_network_provider_name}
        ...            ${param_public_ip_address}
        ...            ${param_supporting_file_1}
        ...            ${param_supporting_file_2}
        ...            ${param_device_name}
        ...            ${param_device_model}
        ...            ${param_device_unique_reference}
        ...            ${param_os}
        ...            ${param_os_version}
        ...            ${param_display_size_in_inches}
        ...            ${param_pixel_counts}
        ...            ${param_unique_number}
        ...            ${param_serial_number}
        ...            ${param_edc_serial_number}
        ...            ${param_edc_model}
        ...            ${param_edc_firmware_version}
        ...            ${param_edc_software_version}
        ...            ${param_edc_sim_card_number}
        ...            ${param_edc_battery_serial_number}
        ...            ${param_edc_adapter_serial_number}
        ...            ${param_edc_smartcard_1_number}
        ...            ${param_edc_smartcard_2_number}
        ...            ${param_edc_smartcard_3_number}
        ...            ${param_edc_smartcard_4_number}
        ...            ${param_edc_smartcard_5_number}
        ...            ${param_pos_serial_number}
        ...            ${param_pos_model}
        ...            ${param_pos_firmware_version}
        ...            ${param_pos_software_version}
        ...            ${param_pos_smartcard_1_number}
        ...            ${param_pos_smartcard_2_number}
        ...            ${param_pos_smartcard_3_number}
        ...            ${param_pos_smartcard_4_number}
        ...            ${param_pos_smartcard_5_number}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - system user updates agent device status - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_is_active}		[Common] - Create int param in dic    ${arg_dic}  	is_active
    ${body}              catenate           SEPARATOR=
        ...	{
        ...            ${param_is_active}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - system user links smart card to device - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_device_id}=  [Common] - Create string param in dic    ${arg_dic}   device_id
    ${body}              catenate           SEPARATOR=
        ...	{
        ...		${param_device_id}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - create product category - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_name}=  [Common] - Create string param in dic    ${arg_dic}   name
    ${param_description}=  [Common] - Create string param in dic    ${arg_dic}   description
    ${param_image_url}=  [Common] - Create string param in dic    ${arg_dic}   image_url
    ${param_is_active}=  [Common] - Create boolean param in dic    ${arg_dic}   is_active
    ${body}              catenate           SEPARATOR=
        ...	{
        ...		${param_name}
        ...		${param_description}
        ...     ${param_image_url}
        ...     ${param_is_active}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - update product category - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_name}=  [Common] - Create string param in dic    ${arg_dic}   name
    ${param_description}=  [Common] - Create string param in dic    ${arg_dic}   description
    ${param_image_url}=  [Common] - Create string param in dic    ${arg_dic}   image_url
    ${param_is_active}=  [Common] - Create boolean param in dic    ${arg_dic}   is_active
    ${body}              catenate           SEPARATOR=
        ...	{
        ...		${param_name}
        ...		${param_description}
        ...     ${param_image_url}
        ...     ${param_is_active}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - create product - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_name}=  [Common] - Create string param in dic    ${arg_dic}   name
    ${param_description}=  [Common] - Create string param in dic    ${arg_dic}   description
    ${param_image_url}=  [Common] - Create string param in dic    ${arg_dic}   image_url
    ${param_denomination}=  [Common] - Create Json data for list param with quote     denomination        ${denomination}
    ${param_min_price}=  [Common] - Create int param in dic    ${arg_dic}   min_price
    ${param_max_price}=  [Common] - Create int param in dic    ${arg_dic}   max_price
    ${param_is_allow_price_range}=  [Common] - Create boolean param in dic    ${arg_dic}   is_allow_price_range
    ${param_product_category_id}=  [Common] - Create int param in dic    ${arg_dic}   product_category_id
    ${param_payment_service_id}=  [Common] - Create int param in dic    ${arg_dic}   payment_service_id
    ${param_is_active}=  [Common] - Create boolean param in dic    ${arg_dic}   is_active
    ${body}              catenate           SEPARATOR=
        ...	{
        ...		${param_name}
        ...		${param_description}
        ...     ${param_image_url}
        ...     ${param_denomination}
        ...     ${param_min_price}
        ...     ${param_max_price}
        ...     ${param_is_allow_price_range}
        ...     ${param_product_category_id}
        ...     ${param_payment_service_id}
        ...     ${param_is_active}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - update product - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_name}=  [Common] - Create string param in dic    ${arg_dic}   name
    ${param_description}=  [Common] - Create string param in dic    ${arg_dic}   description
    ${param_image_url}=  [Common] - Create string param in dic    ${arg_dic}   image_url
    ${param_denomination}=  [Common] - Create Json data for list param with quote     denomination        ${denomination}
    ${param_min_price}=  [Common] - Create int param in dic    ${arg_dic}   min_price
    ${param_max_price}=  [Common] - Create int param in dic    ${arg_dic}   max_price
    ${param_is_allow_price_range}=  [Common] - Create boolean param in dic    ${arg_dic}   is_allow_price_range
    ${param_product_category_id}=  [Common] - Create int param in dic    ${arg_dic}   product_category_id
    ${param_payment_service_id}=  [Common] - Create int param in dic    ${arg_dic}   payment_service_id
    ${param_is_active}=  [Common] - Create boolean param in dic    ${arg_dic}   is_active
    ${body}              catenate           SEPARATOR=
        ...	{
        ...		${param_name}
        ...		${param_description}
        ...     ${param_image_url}
        ...     ${param_denomination}
        ...     ${param_min_price}
        ...     ${param_max_price}
        ...     ${param_is_allow_price_range}
        ...     ${param_product_category_id}
        ...     ${param_payment_service_id}
        ...     ${param_is_active}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - create relation between product and agent - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_agent_id}=  [Common] - Create string param in dic    ${arg_dic}   agent_id
    ${param_product_id}=  [Common] - Create string param in dic    ${arg_dic}   product_id
    ${body}              catenate           SEPARATOR=
        ...	{
        ...		${param_agent_id}
        ...		${param_product_id}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - create relation between product and agent type - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_agent_type_id}=  [Common] - Create string param in dic    ${arg_dic}   agent_type_id
    ${param_product_id}=  [Common] - Create string param in dic    ${arg_dic}   product_id
    ${body}              catenate           SEPARATOR=
        ...	{
        ...		${param_agent_type_id}
        ...		${param_product_id}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - create relation between services and agent - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate           SEPARATOR=
        ...	{
        ...		"agent_service": [
		...         {
		...         	"service_id":"${arg_dic.service1_id}"
		...         },
		... 		{
		...         	"service_id":"${arg_dic.service2_id}"
		...         }
		...     ]
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - link agent profile with company profile - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate           SEPARATOR=
        ...	{
        ...	  "company_id": ${arg_dic.company_id}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - system user update company wallet user - body
    [Arguments]     ${output}=body    ${remove_null}=${True}   ${remove_empty}=${True}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
    ...		{
    ...		    "wallet_user_id": "string",
    ...		    "wallet_user_type": {
    ...		        "id": "int",
    ...		        "name": "string"
    ...		    }
    ...		}
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - create sale hierachy - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate           SEPARATOR=
        ...	{
        ...	  "root_id": ${arg_dic.root_id},
        ...	  "name": "${arg_dic.name}",
        ...   "description": "${arg_dic.description}"
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - create connection - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate           SEPARATOR=
        ...	{
        ...	  "parent_id": ${arg_dic.parent_id},
        ...	  "child_id": "${arg_dic.child_id}"
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - create sale permissions configurations - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate           SEPARATOR=
        ...	{
        ...	  "hierarchy_id": ${arg_dic.hierarchy_id},
        ...	  "permission_id": "${arg_dic.permission_id}",
        ...	  "actor_type_id": "${arg_dic.actor_type_id}",
        ...	  "target_type_id": "${arg_dic.target_type_id}"
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] API check sale fund in perrmission - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate           SEPARATOR=
        ...	{
        ...	  "permission_code": "${arg_dic.permission_code}"
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] - create sale profile - body
    [Arguments]     ${output}=body    &{arg_dic}
    # Identity
    ${param_identity_type_id}		[Common] - Create int param in dic    ${arg_dic}  	identity_type_id
    ${param_identity_username}		[Common] - Create string param of object in dic    ${arg_dic}  	identity     username
    ${param_identity_password}		[Common] - Create string param of object in dic    ${arg_dic}  	identity     password
    ${param_identity_auto_generate_password}		[Common] - Create boolean param of object in dic    ${arg_dic}  	identity     auto_generate_password
    # Profile
    ${param_is_testing_account}		[Common] - Create boolean param in dic    ${arg_dic}		is_testing_account
    ${param_is_system_account}		[Common] - Create boolean param in dic    ${arg_dic}		is_system_account
    ${param_acquisition_source}		[Common] - Create string param in dic    ${arg_dic}		acquisition_source
    ${param_referrer_user_id}		[Common] - Create int param in dic    ${arg_dic}		referrer_user_id
    ${param_referrer_user_type_id}		[Common] - Create string param of object in dic    ${arg_dic}    referrer_user_type		id
    ${param_referrer_user_type_name}		[Common] - Create string param of object in dic    ${arg_dic}    referrer_user_type		name
    ${param_agent_type_id}		[Common] - Create int param in dic    ${arg_dic}		agent_type_id
    ${param_currency}    [Common] - Create string param in dic    ${arg_dic}		currency
    ${param_unique_reference}		[Common] - Create string param in dic    ${arg_dic}		unique_reference
    ${param_mm_card_type_id}		[Common] - Create int param in dic    ${arg_dic}		mm_card_type_id
    ${param_mm_card_level_id}		[Common] - Create int param in dic    ${arg_dic}		mm_card_level_id
    ${param_mm_factory_card_number}		[Common] - Create string param in dic    ${arg_dic}		mm_factory_card_number
    ${param_model_type}		[Common] - Create string param in dic    ${arg_dic}		model_type
    ${param_is_require_otp}		[Common] - Create boolean param in dic    ${arg_dic}		is_require_otp
    ${param_agent_classification_id}		[Common] - Create int param in dic    ${arg_dic}		agent_classification_id
    ${param_tin_number}		[Common] - Create string param in dic    ${arg_dic}		tin_number
    ${param_tin_number_local}		[Common] - Create string param in dic    ${arg_dic}		tin_number_local
    ${param_title}		[Common] - Create string param in dic    ${arg_dic}		title
    ${param_title_local}            [Common] - Create string param in dic    ${arg_dic}		title_local
    ${param_first_name}		[Common] - Create string param in dic    ${arg_dic}		first_name
    ${param_first_name_local}       [Common] - Create string param in dic    ${arg_dic}		first_name_local
    ${param_middle_name}		[Common] - Create string param in dic    ${arg_dic}		middle_name
    ${param_middle_name_local}      [Common] - Create string param in dic    ${arg_dic}		middle_name_local
    ${param_last_name}		[Common] - Create string param in dic    ${arg_dic}		last_name
    ${param_last_name_local}        [Common] - Create string param in dic    ${arg_dic}		last_name_local
    ${param_suffix}		[Common] - Create string param in dic    ${arg_dic}		suffix
    ${param_suffix_local}           [Common] - Create string param in dic    ${arg_dic}		suffix_local
    ${param_date_of_birth}		[Common] - Create string param in dic    ${arg_dic}		date_of_birth
    ${param_place_of_birth}		[Common] - Create string param in dic    ${arg_dic}		place_of_birth
    ${param_place_of_birth_local}   [Common] - Create string param in dic    ${arg_dic}		place_of_birth_local
    ${param_gender}		[Common] - Create string param in dic    ${arg_dic}		gender
    ${param_gender_local}           [Common] - Create string param in dic    ${arg_dic}		gender_local
    ${param_ethnicity}		[Common] - Create string param in dic    ${arg_dic}		ethnicity
    ${param_nationality}		[Common] - Create string param in dic    ${arg_dic}		nationality
    ${param_occupation}		[Common] - Create string param in dic    ${arg_dic}		occupation
    ${param_occupation_local}       [Common] - Create string param in dic    ${arg_dic}		occupation_local
    ${param_occupation_title}		[Common] - Create string param in dic    ${arg_dic}		occupation_title
    ${param_occupation_title_local}     [Common] - Create string param in dic    ${arg_dic}		occupation_title_local
    ${param_township_code}		[Common] - Create string param in dic    ${arg_dic}		township_code
    ${param_township_name}		[Common] - Create string param in dic    ${arg_dic}		township_name
    ${param_township_name_local}        [Common] - Create string param in dic    ${arg_dic}		township_name_local
    ${param_national_id_number}		[Common] - Create string param in dic    ${arg_dic}		national_id_number
    ${param_national_id_number_local}   [Common] - Create string param in dic    ${arg_dic}		national_id_number_local
    ${param_mother_name}		[Common] - Create string param in dic    ${arg_dic}		mother_name
    ${param_mother_name_local}          [Common] - Create string param in dic    ${arg_dic}		mother_name_local
    ${param_email}		[Common] - Create string param in dic    ${arg_dic}		email
    ${param_primary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}		primary_mobile_number
    ${param_secondary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}		secondary_mobile_number
    ${param_tertiary_mobile_number}		[Common] - Create string param in dic    ${arg_dic}		tertiary_mobile_number
    ${param_star_rating}      [Common] - Create string param in dic    ${arg_dic}		star_rating
    ${param_warning_count}       [Common] - Create string param in dic    ${arg_dic}		warning_count
    ${param_is_sale}		[Common] - Create boolean param in dic    ${arg_dic}		is_sale
    # Address
    ${param_current_address_citizen_association}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       citizen_association
    ${param_current_address_neighbourhood_association}      [Common] - Create string param of object in dic    ${arg_dic}    current_address       neighbourhood_association
    ${param_current_address_address}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       address
    ${param_current_address_address_local}      [Common] - Create string param of object in dic    ${arg_dic}    current_address		address_local
    ${param_current_address_commune}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       commune
    ${param_current_address_commune_local}      [Common] - Create string param of object in dic    ${arg_dic}    current_address		commune_local
    ${param_current_address_district}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       district
    ${param_current_address_district_local}     [Common] - Create string param of object in dic    ${arg_dic}    current_address		district_local
    ${param_current_address_city}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       city
    ${param_current_address_city_local}         [Common] - Create string param of object in dic    ${arg_dic}    current_address		city_local
    ${param_current_address_province}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       province
    ${param_current_address_province_local}     [Common] - Create string param of object in dic    ${arg_dic}    current_address		province_local
    ${param_current_address_postal_code}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       postal_code
    ${param_current_address_postal_code_local}      [Common] - Create string param of object in dic    ${arg_dic}    current_address		postal_code_local
    ${param_current_address_country}        [Common] - Create string param of object in dic    ${arg_dic}    current_address       country
    ${param_current_address_country_local}      [Common] - Create string param of object in dic    ${arg_dic}    current_address		country_local
    ${param_current_address_landmark}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       landmark
    ${param_current_address_longitude}      [Common] - Create string param of object in dic    ${arg_dic}    current_address       longitude
    ${param_current_address_latitude}       [Common] - Create string param of object in dic    ${arg_dic}    current_address       latitude
    ${param_permanent_address_citizen_association}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       citizen_association
    ${param_permanent_address_neighbourhood_association}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       neighbourhood_association
    ${param_permanent_address_address}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       address
    ${param_permanent_address_address_local}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		address_local
    ${param_permanent_address_commune}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       commune
    ${param_permanent_address_commune_local}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		commune_local
    ${param_permanent_address_district}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       district
    ${param_permanent_address_district_local}       [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		district_local
    ${param_permanent_address_city}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       city
    ${param_permanent_address_city_local}       [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		city_local
    ${param_permanent_address_province}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       province
    ${param_permanent_address_province_local}       [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		province_local
    ${param_permanent_address_postal_code}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       postal_code
    ${param_permanent_address_postal_code_local}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address		postal_code_local
    ${param_permanent_address_country}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       country
    ${param_permanent_address_country_local}      [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       country_local
    ${param_permanent_address_landmark}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       landmark
    ${param_permanent_address_longitude}        [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       longitude
    ${param_permanent_address_latitude}     [Common] - Create string param of object in dic    ${arg_dic}    permanent_address       latitude
    # Company
    ${param_company_id}		[Common] - Create int param in dic    ${arg_dic}	 company_id
    # Bank
    ${param_bank_name}		[Common] - Create string param of object in dic    ${arg_dic}		bank    name
    ${param_bank_name_local}        [Common] - Create string param of object in dic    ${arg_dic}		bank    name_local
    ${param_bank_account_status}		[Common] - Create int param of object in dic    ${arg_dic}		bank    account_status
    ${param_bank_account_name}		[Common] - Create string param of object in dic    ${arg_dic}		bank    account_name
    ${param_bank_account_name_local}		[Common] - Create string param of object in dic    ${arg_dic}		bank    account_name_local
    ${param_bank_account_number}		[Common] - Create string param of object in dic    ${arg_dic}		bank    account_number
    ${param_bank_account_number_local}		[Common] - Create string param of object in dic    ${arg_dic}		bank    account_number_local
    ${param_bank_branch_area}		[Common] - Create string param of object in dic    ${arg_dic}		bank    branch_area
    ${param_bank_branch_area_local}     [Common] - Create string param of object in dic    ${arg_dic}		bank    branch_area_local
    ${param_bank_branch_city}		[Common] - Create string param of object in dic    ${arg_dic}		bank    branch_city
    ${param_bank_branch_city_local}     [Common] - Create string param of object in dic    ${arg_dic}		bank    branch_city_local
    ${param_bank_register_date}		[Common] - Create string param of object in dic    ${arg_dic}		bank    register_date
    ${param_bank_register_source}		[Common] - Create string param of object in dic    ${arg_dic}		bank    register_source
    ${param_bank_is_verified}		[Common] - Create boolean param in dic    ${arg_dic}		is_verified
    ${param_bank_end_date}		[Common] - Create string param of object in dic    ${arg_dic}		bank    end_date
    # Contract
    ${param_contract_release}		[Common] - Create string param of object in dic    ${arg_dic}		contract    release
    ${param_contract_type}		[Common] - Create string param of object in dic    ${arg_dic}		contract    type
    ${param_contract_number}		[Common] - Create string param of object in dic    ${arg_dic}		contract    number
    ${param_contract_extension_type}		[Common] - Create string param of object in dic    ${arg_dic}		contract    extension_type
    ${param_contract_sign_date}		[Common] - Create string param of object in dic    ${arg_dic}		contract    sign_date
    ${param_contract_issue_date}		[Common] - Create string param of object in dic    ${arg_dic}		contract    issue_date
    ${param_contract_expired_date}		[Common] - Create string param of object in dic    ${arg_dic}		contract    expired_date
    ${param_contract_notification_alert}		[Common] - Create string param of object in dic    ${arg_dic}		contract    notification_alert
    ${param_contract_day_of_period_reconciliation}		[Common] - Create string param of object in dic    ${arg_dic}		contract    day_of_period_reconciliation
    ${param_contract_file_url}		[Common] - Create string param of object in dic    ${arg_dic}		contract    file_url
    ${param_contract_assessment_information_url}		[Common] - Create string param of object in dic    ${arg_dic}		contract    assessment_information_url
    # Accreditation
    ${param_primary_identity_type}      [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       type
    ${param_primary_identity_status}        [Common] - Create int param of object in dic    ${arg_dic}    primary_identity      status
    ${param_primary_identity_identity_id}       [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       identity_id
    ${param_primary_identity_identity_id_local}       [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       identity_id_local
    ${param_primary_identity_place_of_issue}        [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       place_of_issue
    ${param_primary_identity_issue_date}        [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       issue_date
    ${param_primary_identity_expired_date}      [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       expired_date
    ${param_primary_identity_front_identity_url}        [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       front_identity_url
    ${param_primary_identity_back_identity_url}     [Common] - Create string param of object in dic    ${arg_dic}    primary_identity       back_identity_url
    ${param_secondary_identity_type}        [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       type
    ${param_secondary_identity_status}      [Common] - Create int param of object in dic    ${arg_dic}    secondary_identity      status
    ${param_secondary_identity_identity_id}     [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       identity_id
    ${param_secondary_identity_identity_id_local}     [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       identity_id_local
    ${param_secondary_identity_place_of_issue}      [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       place_of_issue
    ${param_secondary_identity_issue_date}      [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       issue_date
    ${param_secondary_identity_expired_date}        [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       expired_date
    ${param_secondary_identity_front_identity_url}      [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       front_identity_url
    ${param_secondary_identity_back_identity_url}       [Common] - Create string param of object in dic    ${arg_dic}    secondary_identity       back_identity_url
    ${param_status_id}		[Common] - Create int param in dic    ${arg_dic}		status_id
    ${param_verify_by}		[Common] - Create string param in dic    ${arg_dic}		verify_by
    ${param_remark}		[Common] - Create string param in dic    ${arg_dic}		remark
    ${param_risk_level}		[Common] - Create string param in dic    ${arg_dic}		risk_level
    # Sale
    ${param_sale_employee_id}		[Common] - Create string param in dic    ${arg_dic}		employee_id
    ${param_sale_calendar_id}		[Common] - Create string param in dic    ${arg_dic}		calendar_id
    # Additional
    ${param_additional_acquiring_sales_executive_name}		[Common] - Create string param in dic    ${arg_dic}		acquiring_sale_executive_name
    ${param_additional_acquiring_sales_executive_name_local}        [Common] - Create string param in dic    ${arg_dic}		acquiring_sale_executive_name_local
    ${param_additional_acquiring_sales_executive_id}		[Common] - Create string param in dic    ${arg_dic}		acquiring_sale_executive_id
    ${param_additional_relationship_manager_name}		[Common] - Create string param in dic    ${arg_dic}		relationship_manager_name
    ${param_additional_relationship_manager_name_local}        [Common] - Create string param in dic    ${arg_dic}		relationship_manager_name_local
    ${param_additional_relationship_manager_id}		[Common] - Create string param in dic    ${arg_dic}		relationship_manager_id
    ${param_additional_sale_region}		[Common] - Create string param in dic    ${arg_dic}		sale_region
    ${param_additional_sale_region_local}      [Common] - Create string param in dic    ${arg_dic}		sale_region_local
    ${param_additional_commercial_account_manager}		[Common] - Create string param in dic    ${arg_dic}		commercial_account_manager
    ${param_additional_commercial_account_manager_local}       [Common] - Create string param in dic    ${arg_dic}		commercial_account_manager_local
    ${param_additional_profile_picture_url}		[Common] - Create string param in dic    ${arg_dic}		profile_picture_url
    ${param_additional_national_id_photo_url}		[Common] - Create string param in dic    ${arg_dic}		national_id_photo_url
    ${param_additional_tax_id_card_photo_url}		[Common] - Create string param in dic    ${arg_dic}		tax_id_card_photo_url
    ${param_additional_field_1_name}		[Common] - Create string param in dic    ${arg_dic}		field_1_name
    ${param_additional_field_1_value}		[Common] - Create string param in dic    ${arg_dic}		field_1_value
    ${param_additional_field_2_name}		[Common] - Create string param in dic    ${arg_dic}		field_2_name
    ${param_additional_field_2_value}		[Common] - Create string param in dic    ${arg_dic}		field_2_value
    ${param_additional_field_3_name}		[Common] - Create string param in dic    ${arg_dic}		field_3_name
    ${param_additional_field_3_value}		[Common] - Create string param in dic    ${arg_dic}		field_3_value
    ${param_additional_field_4_name}		[Common] - Create string param in dic    ${arg_dic}		field_4_name
    ${param_additional_field_4_value}		[Common] - Create string param in dic    ${arg_dic}		field_4_value
    ${param_additional_field_5_name}		[Common] - Create string param in dic    ${arg_dic}		field_5_name
    ${param_additional_field_5_value}		[Common] - Create string param in dic    ${arg_dic}		field_5_value
    ${param_additional_supporting_file_1_url}		[Common] - Create string param in dic    ${arg_dic}		supporting_file_1_url
    ${param_additional_supporting_file_2_url}		[Common] - Create string param in dic    ${arg_dic}		supporting_file_2_url
    ${param_additional_supporting_file_3_url}		[Common] - Create string param in dic    ${arg_dic}		supporting_file_3_url
    ${param_additional_supporting_file_4_url}		[Common] - Create string param in dic    ${arg_dic}		supporting_file_4_url
    ${param_additional_supporting_file_5_url}		[Common] - Create string param in dic    ${arg_dic}		supporting_file_5_url

    ${body}              catenate   SEPARATOR=
    ...          {
    ...              "profile": {
    ...                 ${param_is_testing_account}
    ...                 ${param_is_system_account}
    ...                 ${param_acquisition_source}
    ...                 ${param_referrer_user_id}
    ...                 "referrer_user_type": {
    ...                     ${param_referrer_user_type_id}
    ...                     ${param_referrer_user_type_name}
    ...                 },
    ...                 ${param_agent_type_id}
    ...                 ${param_currency}
    ...                 ${param_unique_reference}
    ...                 ${param_mm_card_type_id}
    ...                 ${param_mm_card_level_id}
    ...                 ${param_mm_factory_card_number}
    ...                 ${param_model_type}
    ...                 ${param_is_require_otp}
    ...                 ${param_agent_classification_id}
    ...                 ${param_tin_number}
    ...                 ${param_tin_number_local}
    ...                 ${param_title}
    ...                 ${param_title_local}
    ...                 ${param_first_name}
    ...                 ${param_first_name_local}
    ...                 ${param_middle_name}
    ...                 ${param_middle_name_local}
    ...                 ${param_last_name}
    ...                 ${param_last_name_local}
    ...                 ${param_suffix}
    ...                 ${param_suffix_local}
    ...                 ${param_date_of_birth}
    ...                 ${param_place_of_birth}
    ...                 ${param_place_of_birth_local}
    ...                 ${param_gender}
    ...                 ${param_gender_local}
    ...                 ${param_ethnicity}
    ...                 ${param_nationality}
    ...                 ${param_occupation}
    ...                 ${param_occupation_local}
    ...                 ${param_occupation_title}
    ...                 ${param_occupation_title_local}
    ...                 ${param_township_code}
    ...                 ${param_township_name}
    ...                 ${param_township_name_local}
    ...                 ${param_national_id_number}
    ...                 ${param_national_id_number_local}
    ...                 ${param_mother_name}
    ...                 ${param_mother_name_local}
    ...                 ${param_email}
    ...                 ${param_primary_mobile_number}
    ...                 ${param_secondary_mobile_number}
    ...                 ${param_tertiary_mobile_number}
    ...                 ${param_star_rating}
    ...                 ${param_warning_count}
    ...                 ${param_is_sale}
    ...                 ${param_company_id}
    ...                 "address": {
    ...                     "current_address": {
    ...      					${param_current_address_citizen_association}
    ...      					${param_current_address_neighbourhood_association}
    ...      					${param_current_address_address}
    ...                         ${param_current_address_address_local}
    ...      					${param_current_address_commune}
    ...                         ${param_current_address_commune_local}
    ...      					${param_current_address_district}
    ...                         ${param_current_address_district_local}
    ...      					${param_current_address_city}
    ...                         ${param_current_address_city_local}
    ...      					${param_current_address_province}
    ...                         ${param_current_address_province_local}
    ...      					${param_current_address_postal_code}
    ...                         ${param_current_address_postal_code_local}
    ...      					${param_current_address_country}
    ...                         ${param_current_address_country_local}
    ...      					${param_current_address_landmark}
    ...      					${param_current_address_longitude}
    ...      					${param_current_address_latitude}
    ...                     },
    ...                     "permanent_address": {
    ...      					${param_permanent_address_citizen_association}
    ...      					${param_permanent_address_neighbourhood_association}
    ...      					${param_permanent_address_address}
    ...                         ${param_permanent_address_address_local}
    ...      					${param_permanent_address_commune}
    ...                         ${param_permanent_address_commune_local}
    ...      					${param_permanent_address_district}
    ...                         ${param_permanent_address_district_local}
    ...      					${param_permanent_address_city}
    ...                         ${param_permanent_address_city_local}
    ...      					${param_permanent_address_province}
    ...                         ${param_permanent_address_province_local}
    ...      					${param_permanent_address_postal_code}
    ...                         ${param_permanent_address_postal_code_local}
    ...      					${param_permanent_address_country}
    ...                         ${param_permanent_address_country_local}
    ...      					${param_permanent_address_landmark}
    ...      					${param_permanent_address_longitude}
    ...      					${param_permanent_address_latitude}
    ...                     }
    ...                 },
    ...                 "bank":{
    ...      				${param_bank_name}
    ...                     ${param_bank_name_local}
    ...      				${param_bank_account_status}
    ...      				${param_bank_account_name}
    ...      				${param_bank_account_name_local}
    ...      				${param_bank_account_number}
    ...      				${param_bank_account_number_local}
    ...      				${param_bank_branch_area}
    ...                     ${param_bank_branch_area_local}
    ...      				${param_bank_branch_city}
    ...                     ${param_bank_branch_city_local}
    ...      				${param_bank_register_date}
    ...      				${param_bank_register_source}
    ...      				${param_bank_is_verified}
    ...      				${param_bank_end_date}
    ...                 },
    ...                 "contract": {
    ...      				${param_contract_release}
    ...      				${param_contract_type}
    ...      				${param_contract_number}
    ...      				${param_contract_extension_type}
    ...      				${param_contract_sign_date}
    ...      				${param_contract_issue_date}
    ...      				${param_contract_expired_date}
    ...      				${param_contract_notification_alert}
    ...      				${param_contract_day_of_period_reconciliation}
    ...      				${param_contract_file_url}
    ...      				${param_contract_assessment_information_url}
    ...                  },
    ...                 "accreditation": {
    ...                     "primary_identity": {
    ...      					${param_primary_identity_type}
    ...      					${param_primary_identity_status}
    ...      					${param_primary_identity_identity_id}
    ...                         ${param_primary_identity_identity_id_local}
    ...      					${param_primary_identity_place_of_issue}
    ...      					${param_primary_identity_issue_date}
    ...      					${param_primary_identity_expired_date}
    ...      					${param_primary_identity_front_identity_url}
    ...      					${param_primary_identity_back_identity_url}
    ...                     },
    ...                     "secondary_identity": {
    ...      					${param_secondary_identity_type}
    ...      					${param_secondary_identity_status}
    ...      					${param_secondary_identity_identity_id}
    ...                         ${param_secondary_identity_identity_id_local}
    ...      					${param_secondary_identity_place_of_issue}
    ...      					${param_secondary_identity_issue_date}
    ...      					${param_secondary_identity_expired_date}
    ...      					${param_secondary_identity_front_identity_url}
    ...      					${param_secondary_identity_back_identity_url}
    ...                     },
    ...      				${param_status_id}
    ...      				${param_verify_by}
    ...      				${param_remark}
    ...      				${param_risk_level}
    ...                 },
    ...                 "additional": {
    ...      				${param_additional_acquiring_sales_executive_name}
    ...      				${param_additional_acquiring_sales_executive_name_local}
    ...      				${param_additional_acquiring_sales_executive_id}
    ...      				${param_additional_relationship_manager_name}
    ...      				${param_additional_relationship_manager_name_local}
    ...      				${param_additional_relationship_manager_id}
    ...      				${param_additional_sale_region}
    ...      				${param_additional_sale_region_local}
    ...      				${param_additional_commercial_account_manager}
    ...      				${param_additional_commercial_account_manager_local}
    ...      				${param_additional_profile_picture_url}
    ...      				${param_additional_national_id_photo_url}
    ...      				${param_additional_tax_id_card_photo_url}
    ...      				${param_additional_field_1_name}
    ...      				${param_additional_field_1_value}
    ...      				${param_additional_field_2_name}
    ...      				${param_additional_field_2_value}
    ...      				${param_additional_field_3_name}
    ...      				${param_additional_field_3_value}
    ...      				${param_additional_field_4_name}
    ...      				${param_additional_field_4_value}
    ...      				${param_additional_field_5_name}
    ...      				${param_additional_field_5_value}
    ...      				${param_additional_supporting_file_1_url}
    ...      				${param_additional_supporting_file_2_url}
    ...      				${param_additional_supporting_file_3_url}
    ...      				${param_additional_supporting_file_4_url}
    ...      				${param_additional_supporting_file_5_url}
    ...                 },
    ...                 "sale": {
    ...                     ${param_sale_employee_id}
    ...                     ${param_sale_calendar_id}
    ...                 }
    ...              },
    ...              "identity": {
    ...      			${param_identity_type_id}
    ...      			${param_identity_username}
    ...      			${param_identity_password}
    ...      			${param_identity_auto_generate_password}
    ...              }
    ...          }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Agent][Prepare] API agent authentication - header
    [Arguments]     ${output}=authen_header    &{arg_dic}
    ${header}               create dictionary
            ...     client_id=${arg_dic.client_id}
            ...     client_secret=${arg_dic.client_secret}
            ...     Content-Type=application/x-www-form-urlencoded
            ...     channel_id=${arg_dic.channel_id}

    [Common] - Set variable       name=${output}      value=${header}

[Agent][Prepare] API agent authentication - param
    [Arguments]     ${output}=params      &{arg_dic}
    log         ${arg_dic.username}
    ${params}              catenate           SEPARATOR=&
        ...     grant_type=${arg_dic.grant_type}
        ...     username=${arg_dic.username}
        ...     password=${arg_dic.password}
        ...     client_id=${arg_dic.param_client_id}
    log         ${params}
    [Common] - Set variable       name=${output}      value=${params}

[Agent][Prepare] API create sale hierachy - header
    [Arguments]     ${output}=headers        &{arg_dic}
    ${headers}               create dictionary
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${arg_dic.access_token}
    [Common] - Set variable       name=${output}      value=${headers}

[Agent][Prepare] API create sale hierachy - body
    [Arguments]     ${output}=body      &{arg_dic}
    ${body}              catenate           SEPARATOR=
        ...	{
        ...	  "root_id": ${arg_dic.root_id},
        ...	  "name": "${arg_dic.name}",
        ...   "description": "${arg_dic.description}"
        ...	}
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}