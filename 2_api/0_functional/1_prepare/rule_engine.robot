*** Settings ***
Resource        ../../../1_common/keywords.robot


*** Keywords ***
[Rule_Engine][Prepare] - create rule - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_name}=   [Common] - Create string param in dic      ${arg_dic}      name
    ${param_start_active_timestamp}=   [Common] - Create string param in dic      ${arg_dic}      start_active_timestamp
    ${param_description}=   [Common] - Create string param in dic      ${arg_dic}      description
    ${param_is_active}=   [Common] - Create boolean param in dic      ${arg_dic}      is_active
    ${param_end_active_timestamp}=   [Common] - Create string param in dic      ${arg_dic}      end_active_timestamp
    ${body}              catenate           SEPARATOR=
        ...	    {
        ...         	 ${param_name}
        ...         	 ${param_start_active_timestamp}
        ...         	 ${param_description}
        ...         	 ${param_is_active}
        ...              ${param_end_active_timestamp}
        ...	    }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Rule_Engine][Prepare] - update rule - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_name}=   [Common] - Create string param in dic      ${arg_dic}      name
    ${param_description}=   [Common] - Create string param in dic      ${arg_dic}      description
    ${param_is_active}=   [Common] - Create boolean param in dic      ${arg_dic}      is_active
    ${body}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_name}
        ...         	 ${param_description}
        ...         	 ${param_is_active}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Rule_Engine][Prepare] - search rule - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_start_active_timestamp}=   [Common] - Create string param in dic      ${arg_dic}      start_active_timestamp
    ${param_rule_id}=   [Common] - Create int param in dic      ${arg_dic}      rule_id
    ${param_is_active}=   [Common] - Create boolean param in dic      ${arg_dic}      is_active
    ${param_end_active_timestamp}=   [Common] - Create string param in dic      ${arg_dic}      end_active_timestamp
    ${body}              catenate           SEPARATOR=
        ...	    {
        ...         	 ${param_rule_id}
        ...         	 ${param_start_active_timestamp}
        ...         	 ${param_is_active}
        ...              ${param_end_active_timestamp}
        ...	    }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Rule_Engine][Prepare] - create mechanic - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_event_name}=   [Common] - Create string param in dic      ${arg_dic}      event_name
    ${param_start_timestamp}=   [Common] - Create string param in dic      ${arg_dic}      start_timestamp
    ${param_end_timestamp}=   [Common] - Create string param in dic      ${arg_dic}      end_timestamp
    ${body}              catenate           SEPARATOR=
        ...	    {
        ...         	 ${param_event_name}
        ...         	 ${param_start_timestamp}
        ...              ${param_end_timestamp}
        ...	    }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Rule_Engine][Prepare] - update mechanic - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_rule_id}=   [Common] - Create int param in dic      ${arg_dic}      rule_id
    ${param_event_name}=   [Common] - Create string param in dic      ${arg_dic}      event_name
    ${param_start_timestamp}=   [Common] - Create string param in dic      ${arg_dic}      start_timestamp
    ${param_end_timestamp}=   [Common] - Create string param in dic      ${arg_dic}      end_timestamp
    ${body}              catenate           SEPARATOR=
        ...	    {
        ...         	 ${param_rule_id}
        ...         	 ${param_event_name}
        ...         	 ${param_start_timestamp}
        ...              ${param_end_timestamp}
        ...	    }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Rule_Engine][Prepare] - create condition - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_filter_type}=   [Common] - Create string param in dic      ${arg_dic}      filter_type
    ${param_sum_key_name}=   [Common] - Create string param in dic      ${arg_dic}      sum_key_name
    ${body}              catenate           SEPARATOR=
        ...	    {
        ...         	 ${param_filter_type}
        ...         	 ${param_sum_key_name}
        ...	    }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Rule_Engine][Prepare] - update condition - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_mechanic_id}=   [Common] - Create int param in dic      ${arg_dic}      mechanic_id
    ${param_filter_type}=   [Common] - Create string param in dic      ${arg_dic}      filter_type
    ${param_filter_type}=   [Common] - Create string param in dic      ${arg_dic}      sum_key_name
    ${body}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_mechanic_id}
        ...         	 ${param_filter_type}
        ...         	 ${param_sum_key_name}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Rule_Engine][Prepare] - create comparison - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_key_name}=   [Common] - Create string param in dic      ${arg_dic}      key_name
    ${param_key_value_type}=   [Common] - Create string param in dic      ${arg_dic}      key_value_type
    ${param_operator}=   [Common] - Create string param in dic      ${arg_dic}      operator
    ${param_key_value}=   [Common] - Create string param in dic      ${arg_dic}      key_value
    ${body}              catenate           SEPARATOR=
        ...	    {
        ...         	 ${param_key_name}
        ...         	 ${param_key_value_type}
        ...         	 ${param_operator}
        ...         	 ${param_key_value}
        ...	    }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Rule_Engine][Prepare] - update comparison - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_key_name}=   Create Json data for   param       key_name        ${arg_dic.key_name}
    ${param_key_value_type}=   [Common] - Create string param in dic      ${arg_dic}      key_value_type
    ${param_operator}=   [Common] - Create string param in dic      ${arg_dic}      operator
    ${param_key_value}=   [Common] - Create string param in dic      ${arg_dic}      key_value
    ${body}              catenate           SEPARATOR=
        ...	{
        ...         	 ${param_key_name}
        ...         	 ${param_key_value_type}
        ...         	 ${param_operator}
        ...         	 ${param_key_value}
        ...	}
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Rule_Engine][Prepare] - create action - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_action_type_id}=   [Common] - Create int param in dic      ${arg_dic}      action_type_id
    ${param_user_id}=   [Common] - Create int param in dic      ${arg_dic}      user_id
    ${param_user_type}=   [Common] - Create string param in dic      ${arg_dic}      user_type
    ${data_json}    set variable    ${empty}
    ${length}   get length      ${arg_dic.data}
    :FOR     ${index}   IN RANGE   0    ${length}
    \      ${action_data_item}    get from list      ${arg_dic.data}      ${index}
    \      ${action_data_item}    strip string         ${action_data_item}
    \      ${data_json}     catenate    SEPARATOR=,  ${data_json}   ${action_data_item}
    log     ${data_json}
    ${action_data_json}              catenate       SEPARATOR=
        ...     	,"data":
        ...     	[
        ...             ${data_json}
        ...     	]
    ${action_data_json}     replace string      ${action_data_json}     [,        [
    ${action_data_json}     replace string          ${action_data_json}     ,]        ]
    ${body}              catenate       SEPARATOR=
        ...     {
        ...         ${param_action_type_id}
        ...         ${param_user_id}
        ...         ${param_user_type}
        ...         ${action_data_json}
        ...     }
    ${body}  replace string        ${body}      ,,      ,
    ${body}  replace string        ${body}      ,}      }
    [Common] - Set variable       name=${output}      value=${body}

[Rule_Engine][Prepare] - update action - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_action_type_id}=   [Common] - Create int param in dic      ${arg_dic}      action_type_id
    ${param_user_id}=   [Common] - Create int param in dic      ${arg_dic}      user_id
    ${data_json}    set variable    ${empty}
    ${length}   get length      ${arg_dic.data}
    :FOR     ${index}   IN RANGE   0    ${length}
    \      ${action_data_item}    get from list      ${arg_dic.data}      ${index}
    \      ${action_data_item}    strip string         ${action_data_item}
    \      ${data_json}     catenate    SEPARATOR=,  ${data_json}   ${action_data_item}
    log     ${data_json}
    ${action_data_json}              catenate       SEPARATOR=
        ...     	"data":
        ...     	[
        ...             ${data_json}
        ...     	]
    ${action_data_json}     replace string      ${action_data_json}     [,        [
    ${action_data_json}     replace string          ${action_data_json}     ,]        ]
    ${body}              catenate           SEPARATOR=
        ...     {
        ...         ${param_action_type_id}
        ...         ${param_user_id}
        ...         ${param_user_type}
        ...         ${action_data_json}
        ...     }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Rule_Engine][Prepare] - create condition filter - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_key_name}=   [Common] - Create string param in dic      ${arg_dic}      key_name
    ${param_key_value_type}=   [Common] - Create string param in dic      ${arg_dic}      key_value_type
    ${param_operator}=   [Common] - Create string param in dic      ${arg_dic}      operator
    ${param_key_value}=   [Common] - Create string param in dic      ${arg_dic}      key_value
    ${param_is_consecutive_key}=   [Common] - Create string param in dic      ${arg_dic}      is_consecutive_key
    ${body}              catenate           SEPARATOR=
        ...	    {
        ...         	 ${param_key_name}
        ...         	 ${param_key_value_type}
        ...         	 ${param_operator}
        ...         	 ${param_key_value}
        ...         	 ${param_is_consecutive_key}
        ...	    }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Rule_Engine][Prepare] - update condition filter - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_key_name}=   [Common] - Create string param in dic      ${arg_dic}      key_name
    ${param_key_value_type}=   [Common] - Create string param in dic      ${arg_dic}      key_value_type
    ${param_operator}=   [Common] - Create string param in dic      ${arg_dic}      operator
    ${param_key_value}=   [Common] - Create string param in dic      ${arg_dic}      key_value
    ${param_is_consecutive_key}=   [Common] - Create string param in dic      ${arg_dic}      is_consecutive_key
    ${body}              catenate           SEPARATOR=
        ...	    {
        ...         	 ${param_key_name}
        ...         	 ${param_key_value_type}
        ...         	 ${param_operator}
        ...         	 ${param_key_value}
        ...         	 ${param_is_consecutive_key}
        ...	    }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Rule_Engine][Prepare] - create condition reset filter - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_key_name}=   [Common] - Create string param in dic      ${arg_dic}      key_name
    ${param_key_value_type}=   [Common] - Create string param in dic      ${arg_dic}      key_value_type
    ${param_operator}=   [Common] - Create string param in dic      ${arg_dic}      operator
    ${param_key_value}=   [Common] - Create string param in dic      ${arg_dic}      key_value
    ${body}              catenate           SEPARATOR=
        ...	    {
        ...         	 ${param_key_name}
        ...         	 ${param_key_value_type}
        ...         	 ${param_operator}
        ...         	 ${param_key_value}
        ...	    }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Rule_Engine][Prepare] - search action history - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_action_id}=   [Common] - Create int param in dic      ${arg_dic}      action_id
    ${body}              catenate           SEPARATOR=
        ...	    {
        ...         	 ${param_action_id}
        ...	    }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Rule_Engine][Prepare] - create action limitation - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_limit_type}=   [Common] - Create string param in dic      ${arg_dic}      limit_type
    ${param_value}=   [Common] - Create int param in dic      ${arg_dic}      value
    ${data_json}    set variable    ${empty}
    ${length}   get length      ${arg_dic.data}
    :FOR     ${index}   IN RANGE   0    ${length}
    \      ${action_limit_data_item}    get from list      ${arg_dic.data}      ${index}
    \      ${action_limit_data_item}    strip string         ${action_limit_data_item}
    \      ${data_json}     catenate    SEPARATOR=,  ${data_json}   ${action_limit_data_item}
    log     ${data_json}
    ${action_limit_data_json}              catenate       SEPARATOR=
        ...     	,"filters":
        ...     	[
        ...             ${data_json}
        ...     	]
    ${action_limit_data_json}     replace string      ${action_limit_data_json}     [,        [
    ${action_limit_data_json}     replace string          ${action_limit_data_json}     ,]        ]

    ${body}              catenate       SEPARATOR=
        ...     {
        ...         ${param_limit_type}
        ...         ${param_value}
        ...         ${action_limit_data_json}
        ...     }
    ${body}  replace string        ${body}      ,,      ,
    ${body}  replace string        ${body}      ,}      }
    [Common] - Set variable       name=${output}      value=${body}

[Rule_Engine][Prepare] - update action limitation - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_limit_type}=   [Common] - Create string param in dic      ${arg_dic}      limit_type
    ${param_value}=   [Common] - Create int param in dic      ${arg_dic}      value

    ${data_json}    set variable    ${empty}
    ${length}   get length      ${arg_dic.data}
    :FOR     ${index}   IN RANGE   0    ${length}
    \      ${action_limit_data_item}    get from list      ${arg_dic.data}      ${index}
    \      ${action_limit_data_item}    strip string         ${action_limit_data_item}
    \      ${data_json}     catenate    SEPARATOR=,  ${data_json}   ${action_limit_data_item}
    log     ${data_json}
    ${action_limit_data_json}              catenate       SEPARATOR=
        ...     	"filters":
        ...     	[
        ...             ${data_json}
        ...     	]
    ${action_limit_data_json}     replace string      ${action_limit_data_json}     [,        [
    ${action_limit_data_json}     replace string          ${action_limit_data_json}     ,]        ]

    ${body}              catenate           SEPARATOR=
        ...     {
        ...         ${param_limit_type}
        ...         ${param_value}
        ...         ${action_limit_data_json}
        ...     }
    ${body}  replace string      ${body}      ,}    }
    ${body}  replace string      ${body}      },,    },
    [Common] - Set variable       name=${output}      value=${body}

[Rule_Engine][Prepare] - create rule limitation - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_limit_type}=   [Common] - Create string param in dic      ${arg_dic}      limit_type
    ${param_limit_value}=   [Common] - Create int param in dic      ${arg_dic}      limit_value
    ${data_json}    set variable    ${empty}
    ${length}   get length      ${arg_dic.data}
    :FOR     ${index}   IN RANGE   0    ${length}
    \      ${rule_limit_data_item}    get from list      ${arg_dic.data}      ${index}
    \      ${rule_limit_data_item}    strip string         ${rule_limit_data_item}
    \      ${data_json}     catenate    SEPARATOR=,  ${data_json}   ${rule_limit_data_item}
    log     ${data_json}
    ${rule_limit_data_json}              catenate       SEPARATOR=
        ...     	,"filters":
        ...     	[
        ...             ${data_json}
        ...     	]
    ${rule_limit_data_json}     replace string      ${rule_limit_data_json}     [,        [
    ${rule_limit_data_json}     replace string          ${rule_limit_data_json}     ,]        ]

    ${body}              catenate       SEPARATOR=
        ...     {
        ...         ${param_limit_type}
        ...         ${param_limit_value}
        ...         ${rule_limit_data_json}
        ...     }
    ${body}  replace string        ${body}      ,,      ,
    ${body}  replace string        ${body}      ,}      }
    [Common] - Set variable       name=${output}      value=${body}