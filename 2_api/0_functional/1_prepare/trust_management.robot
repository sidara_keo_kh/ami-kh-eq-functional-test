*** Settings ***
Resource        ../../../1_common/keywords.robot


*** Keywords ***
[Trust_Management][Prepare] - generate trust token - body
    [Arguments]     ${output}=body    ${remove_null}=${True}   ${remove_empty}=${True}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...        	"trusts":	[{
        ...        		    "truster_user_id": "int",
        ...        		    "truster_user_type_id": "int",
        ...        		    "trusted_user_id": "int",
        ...        		    "trusted_user_type_id": "int",
        ...        		    "expired_unit": "string random_enum('year')",
        ...        		    "expired_duration": "int random_enum('1')"
        ...        		}]
        ...        }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Trust_Management][Prepare] - create trust order - body
    [Arguments]     ${output}=body    ${remove_null}=${True}   ${remove_empty}=${True}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
    ...    {
    ...      "trust_token": "string",
    ...      "ext_transaction_id": "string",
    ...      "product_service": {
    ...        "currency": "string",
    ...        "id": "int",
    ...        "name": "string"
    ...      },
    ...      "product": {
    ...        "product_name": "string",
    ...        "product_ref1": "string",
    ...        "product_ref2": "string",
    ...        "product_ref3": "string",
    ...        "product_ref4": "string",
    ...        "product_ref5": "string"
    ...      },
    ...      "payer_user": {
    ...        "user_id": "int",
    ...        "user_type": "string",
    ...        "sof": {
    ...          "id": "int",
    ...          "type_id": "int"
    ...        }
    ...      },
    ...      "requestor_user": {
    ...        "user_id": "int",
    ...        "user_type": "string",
    ...        "sof": {
    ...          "id": "int",
    ...          "type_id": "int"
    ...        }
    ...      },
    ...      "payee_user": {
    ...        "user_id": "int",
    ...        "user_type": "string",
    ...        "sof": {
    ...          "id": "int",
    ...          "type_id": "int"
    ...        }
    ...      },
    ...      "amount": "string",
    ...      "additional_references": [
    ...        {
    ...          "key": "string",
    ...          "value": "string"
    ...        }
    ...      ]
    ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Trust_Management][Prepare] - execute trust order - body
    [Arguments]     ${output}=body    ${remove_null}=${True}   ${remove_empty}=${True}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...	        "trust_token": "string"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Trust_Management][Prepare] - trusted agent can see their token list - params
    [Arguments]     ${output}=params    &{arg_dic}
    ${params}              catenate          SEPARATOR=&
        ...     role=${arg_dic.trust_role}
    [Common] - Set variable       name=${output}      value=${params}