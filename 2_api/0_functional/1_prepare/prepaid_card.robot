*** Settings ***
Resource        ../../../1_common/keywords.robot


*** Keywords ***
[Prepaid_Card][Prepare] - create prepaid card - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...     	"firstname": "string",
        ...     	"lastname": "string",
        ...     	"card_type_id": "int",
        ...     	"card_lifetime_in_month": "int"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Prepaid_Card][Prepare] - create virtual prepaid card - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...		    "firstname": "string",
        ...		    "lastname": "string",
        ...		    "card_type_id": "int",
        ...		    "card_lifetime_in_month": "int"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Prepaid_Card][Prepare] - create payroll card - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...         "card_identifier": "string",
        ...     	"card_type_id": "int",
        ...     	"card_lifetime_in_month": "int",
        ...         "user": {
        ...             "type": "string",
        ...             "id": "int"
        ...         }
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Prepaid_Card][Prepare] - stop prepaid card - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...		    "is_stopped": "boolean"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Prepaid_Card][Prepare] - admin stop prepaid card - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...		    "is_stopped": "boolean"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Prepaid_Card][Prepare] - update card type - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...	    {
        ...		    "name": "string",
        ...		    "timeout_create_card": "int",
        ...		    "timeout_get_card_detail": "int",
        ...		    "create_card_endpoint_host": "string",
        ...		    "card_detail_endpoint_host": "string",
        ...		    "timeout_update_card_status": "int",
        ...		    "update_card_status_endpoint_host": "string"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}