*** Settings ***
Resource        ../../../1_common/keywords.robot

*** Keywords ***
[Report][Prepare] - Search card history - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate            SEPARATOR=
        ...     {
        ...		    "trans_id": "string",
        ...		    "card_id": "int",
        ...		    "user_id": "int",
        ...		    "user_type_id": "int",
        ...		    "paging": "boolean",
        ...		    "page_index": "int"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search OTP - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate    SEPARATOR=
        ...     {
        ...		    "id": "string",
        ...		    "user_id": "string",
        ...		    "user_type": "string",
        ...		    "delivery_channel": "string",
        ...		    "email": "string",
        ...		    "mobile_number": "string",
        ...		    "is_deleted": "string",
        ...		    "is_success_verified": "string",
        ...		    "from": "string",
        ...		    "to": "string",
        ...		    "otp_reference_id": "string",
        ...		    "otp_code": "string",
        ...		    "user_reference_code": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search customer - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "id": "int",
        ...		    "unique_reference": "string",
        ...		    "mobile_number": "string",
        ...		    "email": "string",
        ...		    "citizen_card_id": "string",
        ...		    "kyc_status": "string",
        ...		    "from_created_timestamp": "string",
        ...		    "to_created_timestamp": "string",
        ...		    "from_last_updated_timestamp": "string",
        ...		    "to_last_updated_timestamp": "string",
        ...		    "timezone": "string",
        ...		    "paging": "boolean",
        ...		    "page_index": "int"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}


[Report][Prepare] - export customer - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "id": "int",
        ...		    "unique_reference": "string",
        ...		    "mobile_number": "string",
        ...		    "email": "string",
        ...		    "citizen_card_id": "string",
        ...		    "kyc_status": "string",
        ...		    "from_created_timestamp": "string",
        ...		    "to_created_timestamp": "string",
        ...		    "from_last_updated_timestamp": "string",
        ...		    "to_last_updated_timestamp": "string",
        ...		    "timezone": "string",
        ...		    "file_type": "string",
        ...		    "row_number": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search system-user - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "username": "string",
        ...		    "user_id": "int",
        ...		    "is_suspended": "string",
        ...		    "suspend_reason": "string",
        ...		    "email": "string",
        ...		    "role_id": "int",
        ...		    "paging": "boolean",
        ...		    "page_index": "int"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search cash source of fund - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "id": "string",
        ...		    "user_type": "int",
        ...		    "": "int",
        ...		    "currency": "string",
        ...		    "user_id_list": [ "int" ],
        ...		    "paging": "boolean",
        ...		    "page_index": "int",
        ...		    "from_created_timestamp": "string",
        ...		    "to_created_timestamp": "string",
        ...		    "timezone": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
        ...     default_array_items=-1
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - export cash source of fund - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "id": "string",
        ...		    "user_type": "int",
        ...		    "user_id": "int",
        ...		    "currency": "string",
        ...		    "from_created_timestamp": "string",
        ...		    "to_created_timestamp": "string",
        ...		    "timezone": "string",
        ...		    "file_type": "string",
        ...		    "row_number": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search cash transaction - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "order_id": "string",
        ...		    "sof_id": "int",
        ...		    "action_id": "int",
        ...		    "status_id": "int",
        ...		    "order_detail_id": "string",
        ...		    "paging": "boolean",
        ...		    "page_index": "int"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search bank source of fund - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "user_type_id": "int",
        ...		    "user_id": "int",
        ...		    "currency": "string",
        ...		    "bank_account_number": "string",
        ...		    "ext_bank_reference": "string",
        ...		    "bank_token": "string",
        ...		    "from_created_timestamp": "string",
        ...		    "to_created_timestamp": "string",
        ...		    "is_deleted": "boolean",
        ...		    "timezone": "string",
        ...		    "paging": "boolean",
        ...		    "page_index": "int"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}


[Report][Prepare] - export bank source of fund - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "user_type_id": "int",
        ...		    "user_id": "int",
        ...		    "currency": "string",
        ...		    "bank_account_number": "string",
        ...		    "ext_bank_reference": "string",
        ...		    "bank_token": "string",
        ...		    "from_created_timestamp": "string",
        ...		    "to_created_timestamp": "string",
        ...		    "is_deleted": "boolean",
        ...		    "timezone": "string",
        ...		    "file_type": "string",
        ...		    "row_number": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search card source of fund - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "user_type_id": "int",
        ...		    "user_id": "int",
        ...		    "currency": "string",
        ...		    "from_created_timestamp": "string",
        ...		    "to_created_timestamp": "string",
        ...		    "paging": "boolean",
        ...		    "page_index": "int",
        ...		    "timezone": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - export card source of fund - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "user_type_id": "int",
        ...		    "user_id": "int",
        ...		    "currency": "string",
        ...		    "from_created_timestamp": "string",
        ...		    "to_created_timestamp": "string",
        ...		    "file_type": "string",
        ...		    "row_number": "int",
        ...		    "timezone": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search bank - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "id": "int",
        ...		    "name": "string",
        ...		    "currency": "string",
        ...		    "status": "boolean"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search bank transactions - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "order_id": "int",
        ...		    "bank_name": "int",
        ...		    "bank_id": "int",
        ...		    "currency": "string",
        ...		    "status": "boolean",
        ...		    "action_id": "int",
        ...		    "paging": "boolean",
        ...		    "page_index": "int"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search voucher - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate        SEPARATOR=
        ...     {
        ...		    "id": "int",
        ...		    "voucher_id": "string",
        ...		    "product_ref2": "string",
        ...		    "cash_in_user_id": "int",
        ...		    "cash_in_user_type": "string",
        ...		    "cash_out_user_id": "int",
        ...		    "cash_in_order_id": "int",
        ...		    "cash_out_order_id": "int",
        ...		    "issuer_user_id": "int",
        ...		    "issuer_user_type": "string",
        ...		    "is_used": "string",
        ...		    "from_created_timestamp": "string",
        ...		    "to_created_timestamp": "string",
        ...		    "from_expire_date_timestamp": "string",
        ...		    "to_expire_date_timestamp": "string",
        ...		    "is_on_hold": "string",
        ...		    "is_cancelled": "string",
        ...		    "paging": "boolean",
        ...		    "page_index": "int"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search voucher refunds - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate        SEPARATOR=
        ...      {
        ...		    "id": "int",
        ...		    "original_voucher_id": "string",
        ...		    "requested_username": "string",
        ...		    "status_id": "int",
        ...		    "from_created_timestamp": "string",
        ...		    "to_created_timestamp": "string",
        ...		    "paging": "boolean",
        ...		    "page_index": "int"
        ...      }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search token information - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate           SEPARATOR=
        ...	    {
        ...         "is_expired" : "bool",
        ...         "is_deleted" : "bool"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search agent devices - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate           SEPARATOR=
        ...	    {
        ...		    "agent_id": "int",
        ...		    "shop_id": "int",
        ...		    "is_deleted": "boolean"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search customer classifications - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate           SEPARATOR=
        ...	    {
        ...		    "id": "int",
        ...		    "name": "string",
        ...		    "is_deleted": "boolean"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search payment service group - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate           SEPARATOR=
        ...	    {
        ...		    "service_group_id": "int",
        ...		    "service_group_name": "string",
        ...		    "to_created_timestamp": "string",
        ...		    "from_created_timestamp": "string",
        ...		    "from_last_updated_timestamp": "string",
        ...		    "to_last_updated_timestamp": "string",
        ...		    "is_deleted": "boolean",
        ...		    "timezone": "string"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - export payment service group - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate           SEPARATOR=
        ...	    {
        ...		    "service_group_id": "int",
        ...		    "service_group_name": "string",
        ...		    "to_created_timestamp": "string",
        ...		    "from_created_timestamp": "string",
        ...		    "from_last_updated_timestamp": "string",
        ...		    "to_last_updated_timestamp": "string",
        ...		    "is_deleted": "boolean",
        ...		    "file_type": "string",
        ...		    "row_number": "int",
        ...		    "timezone": "string"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search payment services - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate           SEPARATOR=
        ...	{
        ...		    "id": "int",
        ...		    "service_group_id": "int",
        ...		    "status": "int",
        ...		    "currency": "string",
        ...		    "service_name": "string",
        ...		    "timezone": "string",
        ...		    "file_type": "string",
        ...		    "row_number": "string",
        ...		    "is_deleted": "boolean"
        ...	}
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - export payment services - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate           SEPARATOR=
        ...	{
        ...		    "id": "int",
        ...		    "service_group_id": "int",
        ...		    "status": "int",
        ...		    "currency": "string",
        ...		    "service_name": "string",
        ...		    "timezone": "string",
        ...		    "file_type": "string",
        ...		    "row_number": "string",
        ...		    "is_deleted": "boolean"
        ...	}
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search vouchers distributed to user - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "voucher_type": "string",
        ...		    "is_used": "boolean",
        ...		    "paging": "boolean",
        ...		    "page_index": "int"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - get list of cancel voucher in formation by cancel id - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "voucher_cancellation_id": "int"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search voucher adjustments - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "id": "int",
        ...		    "original_voucher_id": "string",
        ...		    "requested_username": "string",
        ...		    "status_id": "int",
        ...		    "action": "string",
        ...		    "from_created_timestamp": "string",
        ...		    "to_created_timestamp": "string",
        ...		    "paging": "boolean",
        ...		    "page_index": "int"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search password rule configuration - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...    	    "identity_type_id": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search identity types - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "id": "string",
        ...		    "is_deleted": "string",
        ...		    "name": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search customer devices - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "customer_id": "string",
        ...		    "is_deleted": "bool"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search customer identity - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "customer_id": "string",
        ...		    "id": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search agent - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "id": "string",
        ...		    "unique_reference": "string",
        ...		    "email": "string",
        ...		    "is_testing_account": "string",
        ...		    "is_system_account": "string",
        ...		    "accreditation_status_id": "string",
        ...		    "primary_mobile_number": "string",
        ...		    "mobile_device_unique_reference": "string",
        ...		    "edc_serial_number": "string",
        ...		    "pos_serial_number": "string",
        ...		    "from_created_timestamp": "string",
        ...		    "to_created_timestamp": "string",
        ...		    "paging": "boolean",
        ...		    "page_index": "int",
        ...		    "file_type": "string",
        ...		    "row_number": "string",
        ...		    "timezone": "string",
        ...		    "company_name": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search agent company profile - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "id": "string",
        ...		    "name": "string",
        ...		    "business_type": "string",
        ...		    "company_owner_first_name": "string",
        ...		    "company_owner_last_name": "string",
        ...		    "paging": "boolean",
        ...		    "page_index": "int"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search agent accreditation status - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "id": "string",
        ...		    "country_code": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search agent mm card type - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "id": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - get all agent identities - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "agent_id": "string",
        ...		    "id": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search agent classifications - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "id": "string",
        ...		    "name": "string",
        ...		    "is_deleted": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search agent types - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "id": "int",
        ...		    "is_sale": "boolean",
        ...		    "is_deleted": "boolean"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search agent smart card - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...    	    "agent_id":"int",
        ...         "is_deleted": "bool"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search shop type - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "id": "string",
        ...		    "name": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search agent shop - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "id": "string",
        ...		    "agent_id": "string",
        ...		    "name": "string",
        ...		    "relationship_manager_id": "string",
        ...		    "is_deleted": "string",
        ...		    "relationship_manager_name": "string",
        ...		    "shop_category_id": "string",
        ...		    "shop_type_id": "string",
        ...		    "mobile_device_unique_reference": "string",
        ...		    "edc_serial_number": "string",
        ...		    "pos_serial_number": "string",
        ...		    "paging": "string",
        ...		    "page_index": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search shop simple profile - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "city": "string",
        ...		    "province": "string",
        ...		    "is_deleted": "boolean",
        ...		    "paging": "string",
        ...		    "page_index": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search channels permission - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "user_type_id": "string",
        ...		    "shop_id": "string",
        ...		    "user_id": "string",
        ...		    "channel_id": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search edc - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "serial_number": "string",
        ...		    "terminal_id": "string",
        ...		    "shop_id": "string",
        ...		    "id": "string",
        ...		    "paging": "string",
        ...		    "page_index": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search agent relationship - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "serial_number": "string",
        ...		    "id": "string",
        ...		    "user_id": "string",
        ...		    "is_sharing_benefit": "string",
        ...		    "main_user_id": "string",
        ...		    "sub_user_id": "string",
        ...		    "paging": "string",
        ...		    "page_index": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search categories - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "name": "string",
        ...		    "is_active": "string",
        ...		    "id": "string",
        ...		    "paging": "string",
        ...		    "page_index": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search products - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "name": "string",
        ...		    "is_active": "string",
        ...		    "product_category_id": "string",
        ...		    "is_deleted": "string",
        ...		    "id": "string",
        ...		    "paging": "string",
        ...		    "page_index": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search channels - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "channel_type_id": "string",
        ...		    "is_deleted": "string",
        ...		    "user_type_id": "string",
        ...		    "id": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search products agent type relation - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "id": "string",
        ...		    "agent_type_id": "string",
        ...		    "product_id": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search card on report - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "token": "string",
        ...		    "user_id": "string",
        ...		    "user_type_id": "string",
        ...		    "card_identifier": "string",
        ...		    "paging": "string",
        ...		    "page_index": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search shop categories - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "id": "string",
        ...		    "name": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - report search SOF bank list - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "user_id": "string",
        ...		    "user_type_id": "string",
        ...		    "currency": "string",
        ...		    "bank_account_number": "string",
        ...		    "ext_bank_reference": "string",
        ...		    "bank_token": "string",
        ...		    "from_created_timestamp": "string",
        ...		    "to_created_timestamp": "string",
        ...		    "is_deleted": "string",
        ...		    "paging": "string",
        ...		    "page_index": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search reconciled partner file - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "is_on_us": "string",
        ...		    "service_name": "string",
        ...		    "agent_id": "string",
        ...		    "currency": "string",
        ...		    "status_id": "string",
        ...		    "from_last_updated_timestamp": "string",
        ...		    "to_last_updated_timestamp": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search reconciled partner reconcile result - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "is_on_us": "string",
        ...		    "service_name": "string",
        ...		    "agent_id": "string",
        ...		    "currency": "string",
        ...		    "status_id": "string",
        ...		    "payment_type": "string",
        ...		    "partner_file_id": "string",
        ...		    "from_last_updated_timestamp": "string",
        ...		    "to_last_updated_timestamp": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search reconciled sof file - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "is_on_us": "string",
        ...		    "source_of_fund": "string",
        ...		    "sof_code": "string",
        ...		    "currency": "string",
        ...		    "status_id": "string",
        ...		    "file_name": "string",
        ...		    "from_last_updated_timestamp": "string",
        ...		    "to_last_updated_timestamp": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search reconciled sof reconcile result - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "is_on_us": "string",
        ...		    "source_of_fund": "string",
        ...		    "sof_code": "string",
        ...		    "currency": "string",
        ...		    "status_id": "string",
        ...		    "payment_type": "string",
        ...		    "from_last_updated_timestamp": "string",
        ...		    "to_last_updated_timestamp": "string",
        ...		    "sof_file_id": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - get all roles - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "id": "string",
        ...		    "user_id": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - get all permissions - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "id": "string",
        ...		    "role_id": "int"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search card type - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "id": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search card sof provider - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "name": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search card design - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "provider": "string",
        ...		    "currency": "string",
        ...		    "card_type_id": "string",
        ...		    "name": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search card transactions - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "sof_id": "string",
        ...		    "currency": "string",
        ...		    "status_id": "string",
        ...		    "action_id": "string",
        ...		    "ext_reference": "string",
        ...		    "from_created_timestamp": "string",
        ...		    "to_created_timestamp": "string",
        ...		    "order_id": "string",
        ...		    "user_id": "string",
        ...		    "user_type_id": "string",
        ...		    "from_last_updated_timestamp": "string",
        ...		    "to_last_updated_timestamp": "string",
        ...		    "order_detail_id": "string",
        ...		    "provider_name": "string",
        ...		    "card_design_name": "string",
        ...		    "card_design_number": "string",
        ...		    "card_account_name": "string",
        ...		    "card_account_number": "string",
        ...		    "paging": "string",
        ...		    "page_index": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search card sof action history - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "user_type_id": "string",
        ...		    "currency": "string",
        ...		    "is_success": "string",
        ...		    "from_created_timestamp": "string",
        ...		    "to_created_timestamp": "string",
        ...		    "user_id": "string",
        ...		    "action_id": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - get all balance adjustments - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "reference_id": "string",
        ...		    "product_service_id": "string",
        ...		    "reference_order_id": "string",
        ...		    "created_user_id": "string",
        ...		    "created_user_type_id": "string",
        ...		    "approved_user_id": "string",
        ...		    "approved_user_type_id": "string",
        ...		    "approved_timestamp_from": "string",
        ...		    "approved_timestamp_to": "string",
        ...		    "initiator_user_id": "string",
        ...		    "initiator_user_type_id": "string",
        ...		    "payer_user_id": "string",
        ...		    "payer_user_type_id": "string",
        ...		    "payee_user_id": "string",
        ...		    "payee_user_type_id": "string",
        ...		    "status": "string",
        ...		    "is_for_voucher_cancellation": "string",
        ...		    "created_timestamp_from": "string",
        ...		    "created_timestamp_to": "string",
        ...		    "order_id": "string",
        ...		    "currency": "string",
        ...		    "paging": "string",
        ...		    "page_index": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search list of fraud tickets - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "is_deleted": "string",
        ...		    "ticket_id": "string",
        ...		    "card_id": "string",
        ...		    "action": "string",
        ...		    "from_created_date": "string",
        ...		    "to_created_date": "string",
        ...		    "active_date_from": "string",
        ...		    "active_date_to": "string",
        ...		    "paging": "string",
        ...		    "page_index": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - Add service to report service whitelist - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...    	    "service_ids": [ "int" ]
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - remove service from report service whitelist - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...    	    "service_ids": [ "int" ]
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search agent commission - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "agent_ids": [ "int" ],
        ...		    "mobile_numbers": "string",
        ...         "start_timestamp":"string",
        ...    	    "end_timestamp":"string",
        ...    	    "timezone":"string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
        ...     default_array_items=-1
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - export agent commission - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "agent_ids": [ "int" ],
        ...		    "mobile_numbers": "string",
        ...         "start_timestamp":"string",
        ...    	    "end_timestamp":"string",
        ...    	    "timezone":"string",
        ...		    "file_type": "string",
        ...		    "row_number": "int"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
        ...     default_array_items=-1
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - summary agents commission - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "agent_ids": "string",
        ...		    "mobile_numbers": "string",
        ...         "start_timestamp":"string",
        ...    	    "end_timestamp":"string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - summary payment transaction agent - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...         "start_timestamp":"string",
        ...    	    "end_timestamp":"string",
        ...    	    "user_id":"int"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - summary transaction own agent - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...         "start_timestamp":"string",
        ...    	    "end_timestamp":"string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - summary order balance movements - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...    	    "sof_id":"int",
        ...    	    "sof_type_id":"int",
        ...         "user_id": "int",
        ...         "user_type_id":"int",
        ...         "from_created_timestamp":"string",
        ...         "to_created_timestamp":"string",
        ...         "status_id_list": [ "int" ]
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
        ...     default_array_items=-1
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - update report formula - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...    {
        ...    	    "effective_timestamp":"string",
        ...		    "tpv": "string",
        ...		    "fee": "string",
        ...		    "commission": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search agent belong to company - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate           SEPARATOR=
        ...	    {
        ...	        "page_index": "int",
        ...         "paging": "bool"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - get all company types - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate           SEPARATOR=
        ...	    {
        ...         "is_deleted": "bool"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search payment orders - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "user_id": "int",
        ...		    "user_type_id": "int",
        ...		    "order_id": "string",
        ...		    "ext_transaction_id": "string",
        ...		    "status_id_list": "Json",
        ...		    "state": "string",
        ...		    "to_last_updated_timestamp": "string",
        ...		    "from_last_updated_timestamp": "string",
        ...		    "created_client_id": "string",
        ...		    "executed_client_id": "string",
        ...		    "created_channel_type": "string",
        ...		    "created_device_unique_reference": "string",
        ...		    "error_codes": "Json",
        ...		    "service_name": "string",
        ...		    "service_group_name": "string",
        ...		    "payer_user_id": "int",
        ...		    "payer_user_type_id": "int",
        ...		    "payee_user_id": "int",
        ...		    "payee_user_type_id": "int",
        ...		    "created_channel_type": "string",
        ...		    "service_id_list": "Json",
        ...		    "user_type_id": "int",
        ...		    "file_type": "string",
        ...		    "row_number": "int",
        ...		    "from": "string",
        ...		    "to": "string",
        ...		    "product_ref": "string",
        ...		    "product_name": "string",
        ...		    "ref_order_id": "string",
        ...		    "timezone": "string",
        ...		    "paging": "boolean",
        ...		    "page_index": "int"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - export payment order - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "user_id": "int",
        ...		    "user_type_id": "int",
        ...		    "order_id": "string",
        ...		    "ext_transaction_id": "string",
        ...		    "status_id_list": "Json",
        ...		    "state": "string",
        ...		    "to_last_updated_timestamp": "string",
        ...		    "from_last_updated_timestamp": "string",
        ...		    "created_client_id": "string",
        ...		    "executed_client_id": "string",
        ...		    "created_channel_type": "string",
        ...		    "created_device_unique_reference": "string",
        ...		    "error_codes": "Json",
        ...		    "service_name": "string",
        ...		    "service_group_name": "string",
        ...		    "payer_user_id": "int",
        ...		    "payer_user_type_id": "int",
        ...		    "payee_user_id": "int",
        ...		    "payee_user_type_id": "int",
        ...		    "created_channel_type": "string",
        ...		    "service_id_list": "Json",
        ...		    "user_type_id": "int",
        ...		    "from": "string",
        ...		    "to": "string",
        ...		    "product_ref": "string",
        ...		    "product_name": "string",
        ...		    "ref_order_id": "string",
        ...		    "timezone": "string",
        ...		    "file_type": "string",
        ...		    "row_number": "int"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search user profile - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "mobile_number": "string",
        ...		    "paging": "boolean",
        ...		    "page_index": "int"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - Get transaction history of company - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "order_id": "string",
        ...		    "from": "string",
        ...		    "to": "string",
        ...		    "from_last_updated_timestamp": "string",
        ...		    "to_last_updated_timestamp": "string",
        ...		    "payee_user_id": "int",
        ...		    "payee_user_type_id": "int",
        ...		    "payer_user_id": "int",
        ...		    "payer_user_type_id": "int",
        ...		    "initiator_user_id": "int",
        ...		    "initiator_user_type_id": "int",
        ...		    "status": "int",
        ...		    "created_client_id": "string",
        ...		    "created_channel_type": "string",
        ...		    "created_device_unique_reference": "string",
        ...		    "ext_transaction_id": "string",
        ...		    "ref_order_id": "string",
        ...		    "short_order_id": "string",
        ...         "service_id_list": [ "int" ],
        ...		    "service_group_id": "int",
        ...		    "product_name": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
        ...     default_array_items=-1
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - Search Company Transaction History By Connected Agent - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...         "service_id_list": [ "int" ],
        ...		    "order_id": "string",
        ...		    "from": "string",
        ...		    "to": "string",
        ...		    "from_last_updated_timestamp": "string",
        ...		    "to_last_updated_timestamp": "string",
        ...		    "payee_user_id": "int",
        ...		    "payee_user_type_id": "int",
        ...		    "payer_user_id": "int",
        ...		    "payer_user_type_id": "int",
        ...		    "initiator_user_id": "int",
        ...		    "initiator_user_type": "int",
        ...		    "created_client_id": "string",
        ...		    "created_channel_type": "int",
        ...		    "created_device_unique_reference": "int",
        ...		    "ext_transaction_id": "int",
        ...		    "ref_order_id": "string",
        ...		    "short_order_id": "int",
        ...		    "service_group_id": "int",
        ...		    "product_name": "string",
        ...		    "status": "int"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
        ...     default_array_items=-1
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - Search Company Transaction History By Specific Company - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "order_id": "string",
        ...		    "from": "string",
        ...		    "to": "string",
        ...		    "from_last_updated_timestamp": "string",
        ...		    "to_last_updated_timestamp": "string",
        ...		    "payee_user_id": "int",
        ...		    "payee_user_type_id": "int",
        ...		    "payer_user_id": "int",
        ...		    "payer_user_type_id": "int",
        ...		    "initiator_user_id": "int",
        ...		    "initiator_user_type": "int",
        ...		    "company_agent_id": "int",
        ...		    "company_name": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search settlement configurations - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate           SEPARATOR=
        ...	    {
        ...		    "id": "int",
        ...		    "is_deleted": "boolean",
        ...		    "paging": "boolean",
        ...		    "page_index": "int"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - export agent - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "id": "int",
        ...		    "unique_reference": "string",
        ...		    "mobile_number": "string",
        ...		    "email": "string",
        ...		    "citizen_card_id": "string",
        ...		    "kyc_status": "string",
        ...		    "from_created_timestamp": "string",
        ...		    "to_created_timestamp": "string",
        ...		    "from_last_updated_timestamp": "string",
        ...		    "to_last_updated_timestamp": "string",
        ...		    "timezone": "string",
        ...		    "file_type": "string",
        ...		    "row_number": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - Search api - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...     {
        ...		    "id": "int",
        ...		    "ids": [ "int" ],
        ...		    "paging": "boolean",
        ...		    "page_index": "int"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
        ...     default_array_items=-1
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - Customer search orders - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "service_group_id_list": [ "int" ],
        ...		    "service_id_list": [ "int" ],
        ...		    "status_id_list": [ "int" ],
        ...		    "from_created_timestamp": "string",
        ...		    "to_created_timestamp": "string",
        ...		    "paging": "boolean",
        ...		    "page_index": "int"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
        ...     default_array_items=-1
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - Search sale hierarchy nodes - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "manager_id": "int",
        ...		    "manager_first_name": "string",
        ...		    "manager_middle_name": "string",
        ...		    "manager_last_name": "string",
        ...		    "manager_agent_type_id": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - Agent search last order transaction - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "service_id_list": [ "int" ],
        ...		    "service_group_id_list": [ "int" ],
        ...		    "status": "int",
        ...		    "user_id": "int",
        ...		    "actor_type": "string",
        ...		    "created_device_unique_reference": "string",
        ...		    "created_channel_type_id": "int"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
        ...     default_array_items=-1
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - search sub balance transactions - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		    "sof_id": "int",
        ...		    "order_id": "string",
        ...		    "paging": "boolean",
        ...		    "page_index": "int"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - Search pending settlement - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...		    "description": "string",
        ...		    "created_by": "string",
        ...		    "from_due_date": "string",
        ...		    "to_due_date": "string",
        ...		    "payer_name": "string",
        ...		    "payee_name": "string",
        ...		    "internal_transaction_id": "string",
        ...		    "external_transaction_id": "string",
        ...		    "from_created_timestamp": "string",
        ...		    "to_created_timestamp": "string",
        ...		    "settlement_id": "int",
        ...		    "payer_id": "int",
        ...		    "payee_id": "int",
        ...		    "reconciliation_id": "int",
        ...		    "status": [ "int" ],
        ...		    "is_over_due": "boolean",
        ...		    "paging": "boolean",
        ...		    "page_index": "int"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
        ...     default_array_items=-1
    [Common] - Set variable       name=${output}      value=${body}

[Report][Prepare] - Search sof service balance limitation - body
    [Arguments]     ${output}=body    ${remove_null}=${True}   ${remove_empty}=${True}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...       "service_id": "integer",
        ...       "sof_id": "integer",
        ...       "sof_type_id": "integer",
        ...       "service_name": "string",
        ...       "is_deleted": "boolean",
        ...       "page_index": "integer",
        ...       "paging": "boolean"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}