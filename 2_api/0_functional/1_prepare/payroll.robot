*** Settings ***
Resource        ../../../1_common/keywords.robot


*** Keywords ***
[Payroll][Prepare] - bank agent create and execute payroll order - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...    {
        ...         "ext_transaction_id": "string",
        ...         "product_service": {
        ...             "id": "int",
        ...             "name": "string"
        ...         },
        ...         "product": {
        ...             "product_name": "string",
        ...             "product_ref1": "string",
        ...             "product_ref2": "string",
        ...             "product_ref3": "string",
        ...             "product_ref4": "string",
        ...             "product_ref5": "string"
        ...         },
        ...         "requestor_user": {
        ...             "user_id": "int",
        ...             "user_type": "string"
        ...         },
        ...         "payer_user": {
        ...             "user_id": "int",
        ...             "user_type": "string",
        ...             "ref": {
        ...                 "type": "string",
        ...                 "value": "string"
        ...             }
        ...         },
        ...         "payee_user": {
        ...             "user_id": "int",
        ...             "user_type": "string",
        ...             "ref": {
        ...                 "type": "string",
        ...                 "value": "string"
        ...             }
        ...         },
        ...         "amount": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Payroll][Prepare] - system create and execute payroll order - body
    [Arguments]     ${output}=body    ${remove_null}=${True}   ${remove_empty}=${True}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...    {
        ...         "ext_transaction_id": "string",
        ...         "product_service": {
        ...             "id": "int",
        ...             "name": "string"
        ...         },
        ...         "product": {
        ...             "product_name": "string",
        ...             "product_ref1": "string",
        ...             "product_ref2": "string",
        ...             "product_ref3": "string",
        ...             "product_ref4": "string",
        ...             "product_ref5": "string"
        ...         },
        ...         "requestor_user": {
        ...             "user_id": "int",
        ...             "user_type": "string",
        ...             "ref": {
        ...                 "type": "string",
        ...                 "value": "string"
        ...             }
        ...         },
        ...         "payer_user": {
        ...             "user_id": "int",
        ...             "user_type": "string",
        ...             "ref": {
        ...                 "type": "string",
        ...                 "value": "string"
        ...             }
        ...         },
        ...         "payee_user": {
        ...             "user_id": "int",
        ...             "user_type": "string",
        ...             "ref": {
        ...                 "type": "string",
        ...                 "value": "string"
        ...             }
        ...         },
        ...         "amount": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}