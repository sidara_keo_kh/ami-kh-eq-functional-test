*** Settings ***
Resource        ../../../1_common/keywords.robot

*** Variables ***
${CONTENT_TYPE_JSON}        application/json
${CONTENT_TYPE_FORM}        application/x-www-form-urlencoded

*** Keywords ***
[Common][Prepare] - Client headers
    [Arguments]
        ...     ${client_id}
        ...     ${client_secret}
        ...     ${content_type}=${CONTENT_TYPE_JSON}
        ...     ${output}=headers
        ...     &{arg_dic}

    ${headers}   create dictionary
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     Content-Type=${content_type}

    :FOR    ${key}    IN    @{arg_dic.keys()}
        \       Set To Dictionary       ${headers}      ${key}=${arg_dic["${key}"]}

    [Common] - Set variable       name=${output}       value=${headers}

[Common][Prepare] - Access token headers
    [Arguments]
        ...     ${access_token}
        ...     ${output}=headers
        ...     ${client_id}=${setup_admin_client_id}
        ...     ${client_secret}=${setup_admin_client_secret}
        ...     ${content_type}=${CONTENT_TYPE_JSON}
        ...     &{arg_dic}

    ${headers}   create dictionary
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     Content-Type=${content_type}
        ...     Authorization=Bearer ${access_token}

    :FOR    ${key}    IN    @{arg_dic.keys()}
        \       Set To Dictionary       ${headers}      ${key}=${arg_dic["${key}"]}

    [Common] - Set variable       name=${output}       value=${headers}

