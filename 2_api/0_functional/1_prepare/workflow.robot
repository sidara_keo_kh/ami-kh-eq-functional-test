*** Settings ***
Resource        ../../../1_common/keywords.robot


*** Keywords ***
[Workflow][Prepare] - create request to cancel voucher - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...     {
        ...     	"currency": "${arg_dic.currency}",
        ...     	"vouchers": [
        ...     		{"id": ${arg_dic.voucher_id}, "amount": ${arg_dic.voucher_amount}}
        ...     	]
        ...     }
    [Common] - Set variable       name=${output}      value=${body}

[Workflow][Prepare] - create refund voucher - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate       SEPARATOR=
        ...      {
        ...        "voucher_id": ${arg_dic.voucher_id},
        ...        "product_ref2": "${arg_dic.product_ref2}",
        ...        "product_ref3": "${arg_dic.product_ref3}",
        ...        "product_ref4": "${arg_dic.product_ref4}",
        ...        "product_ref5": "${arg_dic.product_ref5}",
        ...        "reason_for_refund": "${arg_dic.reason_for_refund}",
        ...        "original_voucher_id": "${arg_dic.original_voucher_id}",
        ...        "amount": ${arg_dic.amount},
        ...        "currency": "${arg_dic.currency}"
        ...      }
    [Common] - Set variable       name=${output}      value=${body}

[Workflow][Prepare] - approve voucher refunds - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_refund_request_id_1}      [Common] - Create Json data for integer value in array         ${arg_dic.refund_request_id_1}
    ${param_refund_request_id_2}      [Common] - Create Json data for integer value in array         ${arg_dic.refund_request_id_2}
    ${param_refund_request_id_3}      [Common] - Create Json data for integer value in array         ${arg_dic.refund_request_id_3}
    ${body}              catenate       SEPARATOR=
        ...      {
        ...        "refund_request_ids": [
        ...          ${param_refund_request_id_1}
        ...          ${param_refund_request_id_2}
        ...          ${param_refund_request_id_3}
        ...        ],
        ...        "reason": "${reason}"
        ...      }
    ${body}     replace string      ${body}      ,]    ]
    [Common] - Set variable       name=${output}      value=${body}

[Workflow][Prepare] - confirm cancel vouchers - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...     {
        ...     	"amount":"${arg_dic.amount}"
        ...     }
   [Common] - Set variable       name=${output}      value=${body}

[Workflow][Prepare] - execute adjustment order - body
   [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate   SEPARATOR=
        ...    {
        ...		   "reason": "string"
        ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Workflow][Prepare] - create adjustment order - body
    [Arguments]     ${output}=body    ${remove_null}=${True}   ${remove_empty}=${True}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
    ...        {
    ...          "product_service_id": "int",
    ...          "reference_order_id": "string",
    ...          "product": {
    ...            "product_name": "string",
    ...            "product_ref1": "string",
    ...            "product_ref2": "string",
    ...            "product_ref3": "string",
    ...            "product_ref4": "string",
    ...            "product_ref5": "string"
    ...          },
    ...          "reason": "string",
    ...          "initiator": {
    ...            "user_id": "int",
    ...            "user_type": {
    ...              "id": "int"
    ...            },
    ...            "sof": {
    ...              "id": "int",
    ...              "type_id": "int"
    ...            }
    ...          },
    ...          "payer_user": {
    ...            "user_id": "int",
    ...            "user_type": {
    ...              "id": "int"
    ...            },
    ...            "sof": {
    ...              "id": "int",
    ...              "type_id": "int"
    ...            }
    ...          },
    ...          "payee_user": {
    ...            "user_id": "int",
    ...            "user_type": {
    ...              "id": "int"
    ...            },
    ...            "sof": {
    ...              "id": "int",
    ...              "type_id": "int"
    ...            }
    ...          },
    ...          "amount": "string",
    ...          "cancel_ref_id": "int",
    ...          "reference_service_id": "int",
    ...          "reference_service_group_id": "int",
    ...          "additional_references": [
    ...            {
    ...              "key": "string",
    ...              "value": "string"
    ...            }
    ...          ],
    ...          "batch_code": "string"
    ...        }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Workflow][Prepare] - reject voucher refunds - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}     catenate       SEPARATOR=
        ...    {
        ...        "refund_request_ids":[${arg_dic.refund_request_ids}],
        ...        "reason": "${arg_dic.reason}"
        ...    }
    [Common] - Set variable       name=${output}      value=${body}

[Workflow][Prepare] - approve multiple balance adjustments - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate           SEPARATOR=
        ...    {
        ...        "reference_ids": [${arg_dic.reference_id}],
        ...        "reason": "${arg_dic.reason}"
        ...    }
    ${body}  replace string      ${body}      ,"}    }
    ${body}  replace string      ${body}      ,],    ],
    [Common] - Set variable       name=${output}      value=${body}

[Workflow][Prepare] - reject multiple-balance adjustment orders - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate           SEPARATOR=
        ...    {
        ...        "reference_ids": [${arg_dic.reference_id}],
        ...        "reason": "${arg_dic.reason}"
        ...    }
    [Common] - Set variable       name=${output}      value=${body}

