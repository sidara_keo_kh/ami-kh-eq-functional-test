*** Settings ***
Resource        ../../../1_common/keywords.robot


*** Keywords ***
[Bulk_Upload][Prepare] - post bulk file - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate           SEPARATOR=
        ...	    {
        ...		    "file_id": "int"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Bulk_Upload][Prepare] - update bulk file - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${function_id}    convert to integer      ${arg_dic.function_id}
    ${file_data}  Get Binary File  ${arg_dic.file_path}
    ${files}  Create Dictionary  file_data  ${file_data}
    ${body}  Create Dictionary  function_id  ${function_id}
    [Common] - Set variable       name=${output}      value=${body}

[Bulk_Upload][Prepare] - download bulk file - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate           SEPARATOR=
        ...	    {
        ...		    "file_id": "int",
        ...		    "status_id": "int"
        ...	    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}
