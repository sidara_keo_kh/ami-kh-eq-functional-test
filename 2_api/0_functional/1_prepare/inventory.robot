*** Settings ***
Resource        ../../../1_common/keywords.robot

*** Keyword ***
[Inventory][Prepare] - Create body
    [Arguments]    ${output}=body    &{arg_dic}
    #${body}    evaluate    json.dumps(${arg_dic})    json
    ${body}    REST.input    ${arg_dic}
    [Common] - Set variable       name=${output}      value=${body}