*** Settings ***
Resource        ../../../1_common/keywords.robot


*** Keywords ***
[Payment][Prepare] - Create wallet limitation - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_user_type_id}		    [Common] - Create string param in dic        ${arg_dic}      user_type_id 
    ${param_type_id}		        [Common] - Create string param in dic        ${arg_dic}      type_id 
    ${param_maximum_amount}		    [Common] - Create string param in dic        ${arg_dic}      maximum_amount 
    ${param_classification_id}		[Common] - Create string param in dic        ${arg_dic}      classification_id 
    ${param_currency}		        [Common] - Create string param in dic        ${arg_dic}      currency 
    ${param_service_id}		        [Common] - Create string param in dic        ${arg_dic}      service_id 
    ${param_effective_date_from}    [Common] - Create string param in dic        ${arg_dic}      effective_date_from 
    ${param_effective_date_to}		[Common] - Create string param in dic        ${arg_dic}      effective_date_to 
    ${param_is_sale}		        [Common] - Create string param in dic        ${arg_dic}      is_sale 
    ${body}              catenate        SEPARATOR=
        ...     {
        ...     	${param_effective_date_from}
        ...     	${param_effective_date_to}
        ...     	${param_is_sale}
        ...     	${param_service_id}
        ...         ${param_maximum_amount}
        ...         ${param_user_type_id}
        ...         ${param_type_id}
        ...         ${param_classification_id}
        ...         ${param_currency}
        ...     }
    ${body}   replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - Create Service Group - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_service_group_name}		[Common] - Create string param in dic        ${arg_dic}      service_group_name 
    ${param_description}		[Common] - Create string param in dic        ${arg_dic}      description 
    ${param_image_url}		[Common] - Create string param in dic        ${arg_dic}      image_url 
    ${param_display_name}		[Common] - Create string param in dic        ${arg_dic}      display_name 
    ${param_display_name_local}		[Common] - Create string param in dic        ${arg_dic}      display_name_local 
     ${body}              catenate        SEPARATOR=
        ...     {
        ...     	${param_service_group_name}
        ...     	${param_description}
        ...     	${param_image_url}
        ...     	${param_display_name}
        ...         ${param_display_name_local}
        ...     }

    ${body}   replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - update service group - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...     {
        ...         "service_group_name":"${arg_dic.service_group_name}",
        ...         "description":"${arg_dic.description}"
        ...     }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - Create service - body
    [Arguments]     ${output}=body    ${remove_null}=${True}   ${remove_empty}=${True}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...       "service_group_id": "integer",
        ...       "service_name": "string random_string(20, '${LETTERS}')",
        ...       "currency": "string",
        ...       "description": "string",
        ...       "image_url": "string",
        ...       "display_name": "string",
        ...       "display_name_local": "string",
        ...       "apply_to_all_customer_classification": "boolean",
        ...       "is_allow_debt": "boolean"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - update service - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...     {
        ...         "service_group_id":"${arg_dic.service_group_id}",
        ...         "service_name":"${arg_dic.service_name}",
        ...         "currency":"${arg_dic.currency}",
        ...         "status":"${arg_dic.status}",
        ...         "description":"${arg_dic.description}"
        ...     }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - active-suspend service - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...     {
        ...         "status":"${arg_dic.status}"
        ...     }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - create service command - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...     {
        ...         "service_id":"${arg_dic.service_id}",
        ...         "command_id":"${arg_dic.command_id}"
        ...     }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - create fee tier - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_fee_tier_condition}		[Common] - Create string param in dic        ${arg_dic}      fee_tier_condition
    ${param_condition_amount}		[Common] - Create int param in dic        ${arg_dic}      condition_amount
    ${param_fee_type}		[Common] - Create string param in dic        ${arg_dic}      fee_type
    ${param_fee_amount}		[Common] - Create int param in dic        ${arg_dic}      fee_amount
    ${param_bonus_type}		[Common] - Create string param in dic        ${arg_dic}      bonus_type
    ${param_bonus_amount}		[Common] - Create int param in dic        ${arg_dic}      bonus_amount
    ${param_amount_type}		[Common] - Create string param in dic        ${arg_dic}      amount_type
    ${param_settlement_type}		[Common] - Create string param in dic        ${arg_dic}      settlement_type
    ${body}              catenate        SEPARATOR=
        ...     {
        ...     	${param_fee_tier_condition}
        ...     	${param_condition_amount}
        ...     	${param_fee_type}
        ...     	${param_fee_amount}
        ...     	${param_bonus_type}
        ...     	${param_bonus_amount}
        ...     	${param_amount_type}
        ...     	${param_settlement_type}
        ...         ${arg_dic.list_fee_tier_data}
        ...     }

    ${body}   replace string      ${body}      ,}    }
    ${body}   replace string      ${body}      ,[    ,
    ${body}   replace string      ${body}      ]}    }
    ${body}     replace string          ${body}     u'        ${empty}
    ${body}     replace string          ${body}     }'        }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - update fee tier from A-O - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate        SEPARATOR=
     ...     {
     ...     	"fee_tier_condition":"${arg_dic.fee_tier_condition}",
     ...     	"condition_amount":"${arg_dic.condition_amount}",
     ...        "settlement_type":"${arg_dic.settlement_type}",
     ...        ${arg_dic.list_fee_tier_data}
     ...     }

    ${body}   replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - update all banlance distribution - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate       SEPARATOR=
        ...     {
        ...     	"balance_distributions": ${arg_dic.balance_distributions}
        ...     }
    ${body}     replace string          ${body}     u'        ${empty}
    ${body}     replace string          ${body}     }'        }
    ${body}     replace string          ${body}     }',        },
    ${body}     replace string      ${body}     :,[{        :[{
    ${body}     replace string      ${body}     [,        [
    ${body}     replace string          ${body}     ,]        ]
    ${body}     replace string          ${body}     \\'s        's
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - create SPI URL - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...     {
        ...         "spi_url_type":"${arg_dic.spi_url_type}",
        ...         "url":"${arg_dic.url}",
        ...         "spi_url_call_method":"${arg_dic.spi_url_call_method}",
        ...         "expire_in_minute":${arg_dic.expire_in_minute},
        ...         "max_retry":${arg_dic.max_retry},
        ...         "retry_delay_millisecond":${arg_dic.retry_delay_millisecond},
        ...         "read_timeout":${arg_dic.read_timeout}
        ...     }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - update SPI URL - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...     {
        ...         "service_command_id":"${arg_dic.service_command_id}",
        ...         "spi_url_type":"${arg_dic.spi_url_type}",
        ...         "url":"${arg_dic.url}",
        ...         "spi_url_call_method":"${arg_dic.spi_url_call_method}",
        ...         "expire_in_minute":${arg_dic.expire_in_minute},
        ...         "max_retry":${arg_dic.max_retry},
        ...         "retry_delay_millisecond":${arg_dic.retry_delay_millisecond},
        ...         "read_timeout":${arg_dic.read_timeout}
        ...     }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - create SPI URL configuration - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...     {
        ...         "spi_url_configuration_type":"${arg_dic.spi_url_configuration_type}",
        ...         "url":"${arg_dic.url}",
        ...         "read_timeout":"${arg_dic.read_timeout}",
        ...         "max_retry":${arg_dic.max_retry},
        ...         "expire_in_minute":${arg_dic.expire_in_minute},
        ...         "retry_delay_millisecond":${arg_dic.retry_delay_millisecond}
        ...     }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - update SPI URL configuration - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...     {
        ...         "spi_url_configuration_type":"${arg_dic.spi_url_configuration_type}",
        ...         "url":"${arg_dic.url}",
        ...         "read_timeout":"${arg_dic.read_timeout}",
        ...         "max_retry":${arg_dic.max_retry},
        ...         "expire_in_minute":${arg_dic.expire_in_minute},
        ...         "retry_delay_millisecond":${arg_dic.retry_delay_millisecond}
        ...     }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - create payment pin - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
            ...     {
            ...         "pin":"${arg_dic.pin}"
            ...     }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - update tier level - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...     {
        ...         	"label":"${arg_dic.label}"
        ...     }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - Create normal order - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${payer_user_ref_param}    create json data for user ref object        ${arg_dic.payer_user_ref_type}      ${arg_dic.payer_user_ref_value}
    ${payer_user_id_param}     create json data for user id object         ${arg_dic.payer_user_id}      ${arg_dic.payer_user_type}
    ${payee_user_ref_param}    create json data for user ref object         ${arg_dic.payee_user_ref_type}      ${arg_dic.payee_user_ref_value}
    ${payee_user_id_param}     create json data for user id object         ${arg_dic.payee_user_id}      ${arg_dic.payee_user_type}

    ${data_json}    set variable    ${empty}
    ${length}   get length      ${arg_dic.additional_references}
    :FOR     ${index}   IN RANGE   0    ${length}
    \      ${additional_reference}    get from list      ${arg_dic.additional_references}      ${index}
    \      ${additional_reference}    strip string         ${additional_reference}
    \      ${data_json}     catenate    SEPARATOR=,  ${data_json}   ${additional_reference}

    ${additional_references_data}              catenate       SEPARATOR=
        ...     	,"additional_references":
        ...     	[
        ...             ${data_json}
        ...     	]
    ${additional_references_data}     replace string      ${additional_references_data}     [,        [
    ${additional_references_data}     replace string          ${additional_references_data}     ,]        ]

    ${body}              catenate       SEPARATOR=
        ...    {
        ...     "ext_transaction_id": "${arg_dic.ext_transaction_id}",
        ...     "product_service": {
        ...         "id": ${arg_dic.product_service_id},
        ...         "name": "${arg_dic.product_service_name}"
        ...         },
        ...     "product": {
        ...         "product_name": "${arg_dic.product_name}",
        ...         "product_ref1": "${arg_dic.product_ref_1}",
        ...         "product_ref2": "${arg_dic.product_ref_2}",
        ...         "product_ref3": "${arg_dic.product_ref_3}",
        ...         "product_ref4": "${arg_dic.product_ref_4}",
        ...         "product_ref5": "${arg_dic.product_ref_5}"
        ...         },
        ...     "payer_user": {
        ...         ${payer_user_ref_param}
        ...         ${payer_user_id_param}
        ...     },
        ...     "payee_user": {
        ...         ${payee_user_ref_param}
        ...         ${payee_user_id_param}
        ...     },
        ...     "amount": ${arg_dic.amount}
        ...     ${additional_references_data}
        ...     }
    [Common] - Set variable       name=${output}      value=${body}

[payment][pre_request] - create normal order - body
    [Arguments]     ${output}=body    ${remove_null}=${True}   ${remove_empty}=${True}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
    ...    {
    ...      "ext_transaction_id": "str",
    ...      "product_service": {
    ...        "id": "int",
    ...        "name": "str"
    ...      },
    ...      "product": {
    ...        "product_name": "str",
    ...        "product_ref1": "str",
    ...        "product_ref2": "str",
    ...        "product_ref3": "str",
    ...        "product_ref4": "str",
    ...        "product_ref5": "str"
    ...      },
    ...      "payer_user": {
    ...        "ref": {
    ...          "type": "str",
    ...          "value": "str"
    ...        },
    ...        "user_id": "int",
    ...        "user_type": "str"
    ...      },
    ...      "payee_user": {
    ...        "ref": {
    ...          "type": "str",
    ...          "value": "str"
    ...        },
    ...        "user_id": "int",
    ...        "user_type": "str"
    ...      },
    ...      "requestor_user": {
    ...        "sof": {
    ...          "id": "int",
    ...          "type_id": "int"
    ...        },
    ...        "user_id": "int",
    ...        "user_type": "str"
    ...      },
    ...      "amount": "str",
    ...      "additional_references": [
    ...        {
    ...          "key": "str",
    ...          "value": "str"
    ...        }
    ...      ]
    ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - Create artifact order - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${payer_user_ref_param}    create json data for user ref object        ${arg_dic.payer_user_ref_type}      ${arg_dic.payer_user_ref_value}
    ${payee_user_ref_param}    create json data for user ref object         ${arg_dic.payee_user_ref_type}      ${arg_dic.payee_user_ref_value}
    ${payee_user_id_param}     create json data for user id object         ${arg_dic.payee_user_id}      ${arg_dic.payee_user_type}

    ${data_json}    set variable    ${empty}
    ${length}   get length      ${arg_dic.additional_references}
    :FOR     ${index}   IN RANGE   0    ${length}
    \      ${additional_reference}    get from list      ${arg_dic.additional_references}      ${index}
    \      ${additional_reference}    strip string         ${additional_reference}
    \      ${data_json}     catenate    SEPARATOR=,  ${data_json}   ${additional_reference}

    ${additional_references_data}              catenate       SEPARATOR=
        ...     	,"additional_references":
        ...     	[
        ...             ${data_json}
        ...     	]
    ${additional_references_data}     replace string      ${additional_references_data}     [,        [
    ${additional_references_data}     replace string          ${additional_references_data}     ,]        ]

    ${body}              catenate       SEPARATOR=
        ...    {
        ...     "ext_transaction_id": "${arg_dic.ext_transaction_id}",
        ...     "product_service": {
        ...         "id": ${arg_dic.product_service_id},
        ...         "name": "${arg_dic.product_service_name}"
        ...         },
        ...     "product": {
        ...         "product_name": "${arg_dic.product_name}",
        ...         "product_ref1": "${arg_dic.product_ref_1}",
        ...         "product_ref2": "${arg_dic.product_ref_2}",
        ...         "product_ref3": "${arg_dic.product_ref_3}",
        ...         "product_ref4": "${arg_dic.product_ref_4}",
        ...         "product_ref5": "${arg_dic.product_ref_5}"
        ...         },
        ...     "payer_user": {
        ...         ${payer_user_ref_param}
        ...     },
        ...     "payee_user": {
        ...         ${payee_user_ref_param}
        ...         ${payee_user_id_param}
        ...     },
        ...     "amount": ${arg_dic.amount}
        ...     ${additional_references_data}
        ...     }
    [Common] - Set variable       name=${output}      value=${body}

[payment][pre_request] - create artifact order - body
        [Arguments]     ${output}=body    ${remove_null}=${True}   ${remove_empty}=${True}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
    ...    {
    ...      "ext_transaction_id": "str",
    ...      "product_service": {
    ...        "id": "int",
    ...        "name": "str"
    ...      },
    ...      "product": {
    ...        "product_name": "str",
    ...        "product_ref1": "str",
    ...        "product_ref2": "str",
    ...        "product_ref3": "str",
    ...        "product_ref4": "str",
    ...        "product_ref5": "str"
    ...      },
    ...      "payer_user": {
    ...        "ref": {
    ...          "type": "str",
    ...          "value": "str"
    ...        },
    ...        "user_id": "int",
    ...        "user_type": "str"
    ...      },
    ...      "payee_user": {
    ...        "ref": {
    ...          "type": "str",
    ...          "value": "str"
    ...        },
    ...        "user_id": "int",
    ...        "user_type": "str"
    ...      },
    ...      "requestor_user": {
    ...        "sof": {
    ...          "id": "int",
    ...          "type_id": "int"
    ...        },
    ...        "user_id": "int",
    ...        "user_type": "str"
    ...      },
    ...      "amount": "str",
    ...      "additional_references": [
    ...        {
    ...          "key": "str",
    ...          "value": "str"
    ...        }
    ...      ]
    ...    }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - Cancel artifact order - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${payer_user_ref_param}    create json data for user ref object        ${arg_dic.payer_user_ref_type}      ${arg_dic.payer_user_ref_value}
    ${body}              catenate       SEPARATOR=
        ...    {
        ...     "ext_transaction_id": "${arg_dic.ext_transaction_id}",
        ...     "payer_user": {
        ...         ${payer_user_ref_param}
        ...     }
        ...     }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - Cancel normal order - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${payer_user_ref_param}    create json data for user ref object        ${arg_dic.payer_user_ref_type}      ${arg_dic.payer_user_ref_value}
    ${body}              catenate       SEPARATOR=
        ...    {
        ...     "ext_transaction_id": "${arg_dic.ext_transaction_id}",
        ...     "payer_user": {
        ...         ${payer_user_ref_param}
        ...     }
        ...     }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - Create trust order - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate       SEPARATOR=
        ...    {
        ...     "ext_transaction_id": "${arg_dic.ext_transaction_id}",
        ...     "trust_token": "${arg_dic.trust_token}",
        ...     "product_service": {
        ...         "id": ${arg_dic.product_service_id},
        ...         "name": "${arg_dic.product_service_name}"
        ...         },
        ...     "product": {
        ...         "product_name": "${arg_dic.product_name}",
        ...         "product_ref1": "${arg_dic.product_ref_1}",
        ...         "product_ref2": "${arg_dic.product_ref_2}",
        ...         "product_ref3": "${arg_dic.product_ref_3}",
        ...         "product_ref4": "${arg_dic.product_ref_4}",
        ...         "product_ref5": "${arg_dic.product_ref_5}"
        ...         },
        ...     "payer_user": {
        ...         "user_id": ${arg_dic.payer_user_id},
        ...         "user_type": "${arg_dic.payer_user_type_name}",
        ...         "sof": {
    	...             	"id": ${arg_dic.payer_sof_id},
    	...         	    "type_id": ${arg_dic.payer_sof_type_id}
        ...                 }
        ...             },
        ...     "payee_user": {
        ...         "user_id": ${arg_dic.payee_user_id},
        ...         "user_type": "${arg_dic.payee_user_type_name}",
        ...         "sof": {
    	...             	"id": ${arg_dic.payee_sof_id},
    	...         	    "type_id": ${arg_dic.payee_sof_type_id}
        ...                 }
        ...             },
        ...     "amount": ${arg_dic.amount},
        ...     "additional_references": [ ${arg_dic.additional_references}]
        ...     }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - create user sof cash - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...     {
        ...         "currency":"${arg_dic.currency}"
        ...     }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - system user create user sof cash - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...     {
        ...         "user_id":${arg_dic.user_id},
        ...         "user_type_id":${arg_dic.user_type_id},
        ...         "currency":"${arg_dic.currency}"
        ...     }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - system user create a company agent cash source of fund with currency - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...     {
        ...         "currency":"${arg_dic.currency}"
        ...     }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - system user update company agent cash balance - body
    [Arguments]     ${output}=body    ${remove_null}=${True}   ${remove_empty}=${True}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...       "amount": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - Link bank by bank - body
    [Arguments]     ${output}=body    ${remove_null}=${True}   ${remove_empty}=${True}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...         "user_id":"int",
        ...         "user_type_id":"int",
        ...         "bank_id":"int",
        ...         "bank_account_name":"string random_string(10, '${LETTERS}')",
        ...         "bank_account_number":"int random_string(10, '${NUMBERS}')",
        ...         "currency":"string",
        ...         "ext_bank_reference":"string random_string(20, '${LETTERS}')"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - verify (pre-link) card information - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...    {
        ...        "card_account_name":"${arg_dic.card_account_name}",
        ...        "card_account_number":"${arg_dic.card_account_number}",
        ...        "citizen_id":"${arg_dic.citizen_id}",
        ...        "cvv_cvc":"${arg_dic.cvv_cvc}",
        ...        "pin": "${arg_dic.pin}",
        ...        "expiry_date":"${arg_dic.expiry_date}",
        ...        "issue_date":"${arg_dic.issue_date}",
        ...        "mobile_number":"${arg_dic.mobile_number}",
        ...        "billing_address":"${arg_dic.billing_address}",
        ...        "card_type": "${arg_dic.card_type}",
        ...        "currency":"${arg_dic.currency}"
        ...    }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - user link card source of fund - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...    {
        ...        "card_account_name":"${arg_dic.card_account_name}",
        ...        "card_account_number":"${arg_dic.card_account_number}",
        ...        "citizen_id":"${arg_dic.citizen_id}",
        ...        "cvv_cvc":"${arg_dic.cvv_cvc}",
        ...        "pin": "${arg_dic.pin}",
        ...        "expiry_date":"${arg_dic.expiry_date}",
        ...        "issue_date":"${arg_dic.issue_date}",
        ...        "mobile_number":"${arg_dic.mobile_number}",
        ...        "billing_address":"${arg_dic.billing_address}",
        ...        "currency":"${arg_dic.currency}",
        ...    	"security": {
        ...	    "code": "${arg_dic.security_code}",
        ...   	    "ref":"${arg_dic.security_ref}"
        ...         }
        ...        }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - add company balance - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...     {
        ...     	"amount":"${arg_dic.amount}"
        ...     }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - apply discount code - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...     {
        ...         "voucher_id":"${arg_dic.voucher_id}"
        ...     }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - Execute order with security - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...    {
        ...     "security": {
        ...            "code":"${arg_dic.code}",
        ...            "ref":null,
        ...            "type":{
        ...                 "id":${arg_dic.type_id},
        ...                 "name":"${arg_dic.type_name}"
        ...            }
        ...         }
        ...     }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - delete sof service balance limitation - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...     {
        ...         "sof_service_balance_limitation": ${arg_dic.list_sof_service_balance_limitation}
        ...     }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - apply tier level mask to an order - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...     {
        ...     	"tier_level_masks": [
        ...     		${arg_dic.tier_level_masks}
        ...     	]
        ...     }
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - create service limitation - body
    [Arguments]     ${output}=body    &{arg_dic}
     ${param_equation_id}         [Common] - Create int param in dic    ${arg_dic}         equation_id
     ${body}              catenate
         ...     {
         ...         "conditions":${arg_dic.conditions},
         ...         "limitation_type":"${arg_dic.limitation_type}",
         ...         ${param_equation_id}
         ...         "limitation_value":"${arg_dic.limitation_value}",
         ...         "reset_time_block_value":${arg_dic.reset_time_block_value},
         ...         "reset_time_block_unit":"${arg_dic.reset_time_block_unit}",
         ...         "start_effective_timestamp":"${arg_dic.start_effective_timestamp}",
         ...         "end_effective_timestamp":"${arg_dic.end_effective_timestamp}"
         ...     }

     ${body}     replace string          ${body}     u'        ${empty}
     ${body}     replace string          ${body}     }'        }
     ${body}     replace string          ${body}     }',        },
     ${body}     replace string      ${body}     :,[{        :[{
     ${body}     replace string      ${body}     [,        [
     ${body}     replace string          ${body}     ,]        ]
     ${body}     replace string          ${body}     \\'s        's
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - create tier mask for service - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_requestor_user_type}        [Common] - Create string param of object in dic    ${arg_dic}    requestor   user_type
    ${param_requestor_type_id}        [Common] - Create int param of object in dic    ${arg_dic}    requestor   type_id
    ${param_requestor_classification_id}        [Common] - Create int param of object in dic    ${arg_dic}    requestor   classification_id
    ${param_requestor_postal_code}        [Common] - Create string param of object in dic    ${arg_dic}    requestor   postal_code

    ${param_initiator_user_type}        [Common] - Create string param of object in dic    ${arg_dic}    initiator   user_type
    ${param_initiator_type_id}        [Common] - Create int param of object in dic    ${arg_dic}    initiator   type_id
    ${param_initiator_classification_id}        [Common] - Create int param of object in dic    ${arg_dic}    initiator   classification_id
    ${param_initiator_postal_code}        [Common] - Create string param of object in dic    ${arg_dic}    initiator   postal_code

    ${param_payer_user_type}        [Common] - Create string param of object in dic    ${arg_dic}    payer   user_type
    ${param_payer_type_id}        [Common] - Create int param of object in dic    ${arg_dic}    payer   type_id
    ${param_payer_classification_id}        [Common] - Create int param of object in dic    ${arg_dic}    payer   classification_id
    ${param_payer_postal_code}        [Common] - Create string param of object in dic    ${arg_dic}    payer   postal_code

    ${param_payee_user_type}        [Common] - Create string param of object in dic    ${arg_dic}    payee   user_type
    ${param_payee_type_id}        [Common] - Create int param of object in dic    ${arg_dic}    payee   type_id
    ${param_payee_classification_id}        [Common] - Create int param of object in dic    ${arg_dic}    payee   classification_id
    ${param_payee_postal_code}        [Common] - Create string param of object in dic    ${arg_dic}    payee   postal_code

    ${body}              catenate    SEPARATOR=
    ...     {
    ...    	"name":"${arg_dic.tier_mask_name}",
    ...        "requestor":  {
    ...    		${param_requestor_user_type}
    ...    		${param_requestor_type_id}
    ...    		${param_requestor_classification_id}
    ...    		${param_requestor_postal_code}
    ...    	},
    ...        "initiator":  {
    ...    		${param_initiator_user_type}
    ...    		${param_initiator_type_id}
    ...    		${param_initiator_classification_id}
    ...    		${param_initiator_postal_code}
    ...    	},
    ...        "payer":  {
    ...    		${param_payer_user_type}
    ...    		${param_payer_type_id}
    ...    		${param_payer_classification_id}
    ...    		${param_payer_postal_code}
    ...    	},
    ...        "payee":  {
    ...    		${param_payee_user_type}
    ...    		${param_payee_type_id}
    ...    		${param_payee_classification_id}
    ...    		${param_payee_postal_code}
    ...    	},
    ...    	"from_effective_timestamp":"${arg_dic.from_effective_timestamp}",
    ...     "to_effective_timestamp": "${arg_dic.to_effective_timestamp}"
    ...     }
    ${body}   replace string      ${body}      ,}    }
    ${body}   replace string      ${body}      {}    null
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - create service fee tier mask - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_condition_amount}     [Common] - Create int param in dic   ${arg_dic}      condition_amount
    ${param_amount_type}     [Common] - Create string param in dic   ${arg_dic}      amount_type
    ${body}              catenate    SEPARATOR=
    ...    {
    ...    	"fee_tier_id": ${arg_dic.fee_tier_id},
    ...    	"command_id": ${arg_dic.command_payment_id},
    ...    	"command_name": "${arg_dic.command_payment_name}",
    ...    	"fee_tier_condition":"${arg_dic.fee_tier_condition}",
    ...     ${param_amount_type}
    ...    	${param_condition_amount}
    ...     ${arg_dic.label}
    ...    }
    ${body}   replace string      ${body}      ,}    }
    ${body}   replace string      ${body}      ,,    ,
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - system user create settlement configuration - body
    [Arguments]     ${output}=body    &{arg_dic}
     ${body}              catenate    SEPARATOR=
        ...    {
        ...    	"service_group_id": ${arg_dic.service_group_id},
        ...    	"service_id": ${arg_dic.service_id},
        ...    	"currency": "${arg_dic.currency}"
        ...    }
    ${body}   replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

#[Payment][Prepare] - Update sof service balance limitation - body
#    [Arguments]     ${output}=body    &{arg_dic}
#    ${param_maximum_amount}=  [Common] - Create string param in dic    ${arg_dic}     maximum_amount
#    ${param_minimum_amount}=  [Common] - Create string param in dic    ${arg_dic}     minimum_amount
#
#    ${body}              catenate       SEPARATOR=
#        ...     {
#        ...         ${param_maximum_amount}
#        ...         ${param_minimum_amount}
#        ...     }
#    ${body}   replace string      ${body}      ,}    }
#    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - Create sof service balance limitation - body
    [Arguments]     ${output}=body    ${remove_null}=${True}   ${remove_empty}=${True}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...       "maximum_amount": "string",
        ...       "minimum_amount": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Payment][Prepare] - Update sof service balance limitation - body
    [Arguments]     ${output}=body    ${remove_null}=${True}   ${remove_empty}=${True}    &{arg_dic}
    ${schema}              catenate     SEPARATOR=
        ...     {
        ...       "maximum_amount": "string",
        ...       "minimum_amount": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}