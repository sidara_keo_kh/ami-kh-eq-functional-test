*** Settings ***
Resource        ../../../1_common/keywords.robot


*** Keywords ***
[Device_Management][Prepare] - create channel - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate       SEPARATOR=
        ...     {
        ...		    "channel_type_id": "string",
        ...		    "user_type_id": "string",
        ...		    "name": "string"
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Device_Management][Prepare] - create channel access permission - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate       SEPARATOR=
        ...     {
        ...     	"user_id": "int",
        ...     	"shop_id": "int",
        ...     	"user_type":{
        ...             "id": "int",
        ...             "name": "string"
        ...         }
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}

[Device_Management][Prepare] - revoke channel access permission - body
    [Arguments]     ${output}=body    ${remove_null}=${False}   ${remove_empty}=${False}    &{arg_dic}
    ${schema}              catenate       SEPARATOR=
        ...     {
        ...     	"user_id": "int",
        ...     	"user_type":{
        ...             "id": "int",
        ...             "name": "string"
        ...         }
        ...     }
    ${body}     generate json
        ...     json_schema=${schema}
        ...     args=${arg_dic}
        ...     remove_null=${remove_null}
        ...     remove_empty=${remove_empty}
    [Common] - Set variable       name=${output}      value=${body}