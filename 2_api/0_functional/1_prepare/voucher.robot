*** Settings ***
Resource        ../../../1_common/keywords.robot


*** Keywords ***

[Voucher][Prepare] - update voucher status - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate
        ...   {
        ...     "is_on_hold":${arg_dic.is_on_hold}
        ...	  }
   [Common] - Set variable       name=${output}      value=${body}

[Voucher][Prepare] - create multiple discount vouchers - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${body}              catenate       SEPARATOR=
        ...     {
        ...       "prefix": "${arg_dic.prefix}",
        ...       "number_of_digit": ${arg_dic.number_of_digit},
        ...       "discount_type": "${arg_dic.discount_type}",
        ...       "discount_amount": ${arg_dic.discount_amount},
        ...       "currency": "${arg_dic.currency}",
        ...       "payer_user": {
        ...         "id": ${arg_dic.payer_user_id},
        ...         "type": "${arg_dic.payer_user_type}",
        ...         "sof_id": ${arg_dic.payer_user_sof_id},
        ...         "sof_type_id": ${arg_dic.payer_user_sof_type_id}
        ...       },
        ...       "applied_to": "${arg_dic.applied_to}",
        ...       "min_amount": ${arg_dic.min_amount},
        ...       "max_discount": ${arg_dic.max_discount},
        ...       "permitted_service_group_id": "${arg_dic.permitted_service_group_id}",
        ...       "permitted_service_id": "${arg_dic.permitted_service_id}",
        ...       "voucher_group": "${arg_dic.voucher_group}",
        ...       "number_of_needed_voucher": ${arg_dic.number_of_needed_voucher}
        ...     }
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}

[Voucher][Prepare] - create multiple-use vouchers - body
    [Arguments]     ${output}=body    &{arg_dic}
    ${param_expiration_date}        [Common] - Create string param in dic      ${arg_dic}       expiration_date
    ${body}              catenate       SEPARATOR=
        ...     {
        ...       "voucher_code": "${arg_dic.voucher_code}",
        ...       "discount_type": "${arg_dic.discount_type}",
        ...       "discount_amount": ${arg_dic.discount_amount},
        ...       "currency": "${arg_dic.currency}",
        ...       "payer_user": {
        ...         "id": ${arg_dic.payer_user_id},
        ...         "type": "${arg_dic.payer_user_type}",
        ...         "sof_id": ${arg_dic.payer_user_sof_id},
        ...         "sof_type_id": ${arg_dic.payer_user_sof_type_id}
        ...       },
        ...       "applied_to": "${arg_dic.applied_to}",
        ...       "min_amount": ${arg_dic.min_amount},
        ...       "max_discount": ${arg_dic.max_discount},
        ...       "permitted_service_group_id": "${arg_dic.permitted_service_group_id}",
        ...       "permitted_service_id": "${arg_dic.permitted_service_id}",
        ...       "max_per_user": ${arg_dic.max_per_user},
        ...       ${param_expiration_date}
        ...     }
    ${body}  replace string      ${body}      ,}    }
    [Common] - Set variable       name=${output}      value=${body}