*** Settings ***
Resource        ../../../1_common/keywords.robot

*** Keywords ***
[Report][Extract] Pagination - total pages
    [Arguments]     ${output}=test_total_pages
    ${total_pages}        rest extract    $.data.page.total_pages
    [Common] - Set variable       name=${output}      value=${total_pages}

[Report][Common][Verify] Pagination - verify the first page
    ${total_element}    rest extract    $.data.page.total_elements
    ${expect_total_pages}=      Evaluate        ${total_element}/50+1
    REST.integer    $.data.page.total_elements
    REST.boolean    $.data.page.has_previous        ${FALSE}
    run keyword if     ${total_element} > 50     REST.boolean    $.data.page.has_next            ${TRUE}
                        ...     ELSE             REST.boolean    $.data.page.has_next            ${FALSE}
    REST.integer    $.data.page.current_page        1
    REST.integer    $.data.page.total_pages         ${expect_total_pages}

[Report][Common][Verify] Pagination - verify the last page
    ${total_element}    rest extract    $.data.page.total_elements
    ${expect_total_pages}=      Evaluate        ${total_element}/50+1
    REST.integer    $.data.page.total_elements
    REST.boolean    $.data.page.has_next        ${FALSE}
    run keyword if     ${total_element} > 50     REST.boolean    $.data.page.has_previous            ${TRUE}
                        ...     ELSE             REST.boolean    $.data.page.has_previous            ${FALSE}
    REST.integer    $.data.page.current_page        ${expect_total_pages}
    REST.integer    $.data.page.total_pages         ${expect_total_pages}

[Report][Common][Verify] Pagination - verify non-existing page
    REST.integer    $.data.page.total_elements
    REST.null       $.data.page.has_next
    REST.null       $.data.page.has_previous
    REST.null       $.data.page.current_page
    REST.integer    $.data.page.total_pages

[Report][Common][Verify] Pagination - verify non paging
    REST.integer    $.data.page.total_elements
    REST.boolean    $.data.page.has_next        ${FALSE}
    REST.boolean    $.data.page.has_previous    ${FALSE}
    REST.integer    $.data.page.current_page    1
    REST.integer    $.data.page.total_pages     1