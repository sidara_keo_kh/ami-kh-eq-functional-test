*** Settings ***
Resource        ../../../1_common/keywords.robot

*** Keywords ***
[Payment][Extract] Create service group - service group id
    [Arguments]     ${output}=test_service_group_id
    ${id}        rest extract    $.data.service_group_id
    [Common] - Set variable       name=${output}      value=${id}

[Payment][Extract] Create service - service id
    [Arguments]     ${output}=test_service_id
    ${id}        rest extract    $.data.service_id
    [Common] - Set variable       name=${output}      value=${id}

[Payment][Extract] Create user sof cash - sof id
    [Arguments]     ${output}=test_sof_id
    ${id}        rest extract    $.data.sof_id
    [Common] - Set variable       name=${output}      value=${id}

[Payment][Extract] Verify (pre-link) card information - security ref
    [Arguments]     ${output}=test_security_ref
    ${ref}        rest extract    $.data.security_ref
    [Common] - Set variable       name=${output}      value=${ref}

[Payment][Extract] user link card source of fund - token
    [Arguments]     ${output}=test_token
    ${token}        rest extract    $.data.token
    [Common] - Set variable       name=${output}      value=${token}

[Payment][Extract] Create sof service balance limitation - balance_limitation_id
    [Arguments]     ${output}=test_balance_limitation_id
    ${id}        rest extract    $.data.id
    [Common] - Set variable       name=${output}      value=${id}