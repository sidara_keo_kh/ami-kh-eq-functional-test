*** Settings ***
Resource        ../../../1_common/keywords.robot

*** Keywords ***
[Channel Adapter][Extract] Order fields
    [Arguments]     ${field}=fields
    ${pre_balance_wallet}       REST.array          $.data.properties
    [Common] - Set variable       name=${field}      value=${pre_balance_wallet}

[Channel_Adapter][Extract] - Get hierarchy list by agent id
    [Arguments]     ${output_hierarchies}=hierarchies
    ${data}        rest extract    $.data
    [Common] - Set variable       name=${output_hierarchies}      value=${data}