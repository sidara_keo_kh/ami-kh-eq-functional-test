*** Settings ***
Resource        ../../../1_common/keywords.robot

*** Keywords ***
[Api Gateway][Extract] Authentication - access token
    [Arguments]     ${output}=test_access_token
    ${token}        rest extract    $.access_token
    [Common] - Set variable       name=${output}      value=${token}