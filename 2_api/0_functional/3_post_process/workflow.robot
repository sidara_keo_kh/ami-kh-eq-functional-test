*** Settings ***
Resource        ../../../1_common/keywords.robot

*** Keywords ***
[Workflow][Extract] - create order - reference_id
    [Arguments]     ${output_reference_id}=test_reference_id
    ${reference_id}        rest extract    $.data.reference_id
    [Common] - Set variable       name=${output_reference_id}      value=${reference_id}