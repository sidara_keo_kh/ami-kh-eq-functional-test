*** Settings ***
Resource        ../../../1_common/keywords.robot

*** Keywords ***
[Inventory][Extract] - Response body data
    [Arguments]    ${output_prefix}=test
    ${data}    rest extract    $.data
    :FOR    ${key}    IN    @{data.keys()}
    \    [Common] - Set variable       name=${output_prefix}_${key}      value=${data['${key}']}
    
[Inventory][Verify] - Response body data null
    REST.null    $.data