*** Settings ***
Resource        ../../../1_common/keywords.robot

*** Keywords ***
[Common][Verify] - String field or null
    [Arguments]     ${field}        ${value}=${EMPTY}
    ${is_null}      run keyword and return status       REST.null       ${field}
    run keyword if     '${is_null}'=='False'     REST.string    ${field}     ${value}

[Common][Extract] - field or empty if missing
    [Arguments]     ${field}        ${output}=test_value
    ${has_field}      rest has field       ${field}
    ${value}        run keyword if     '${has_field}'=='True'     rest extract    ${field}
        ...     ELSE        set variable        ${EMPTY}
    [Common] - Set variable       name=${output}      value=${value}

[Common][Verify] - Http status code is "${code}"
    REST.integer    response status     ${code}

[Common][Verify] - Status code is "${code}"
    REST.string    $.status.code     ${code}

[Common][Verify] - Status message is "${message}"
    REST.string    $.status.message     ${message}

[Common][Extract] - ID
    [Arguments]     ${output}=test_id
    ${id}        rest extract    $.data.id
    [Common] - Set variable       name=${output}      value=${id}

[Common][Extract] - token
    [Arguments]     ${output}=test_token
    ${id}        rest extract    $.data[0].token
    [Common] - Set variable       name=${output}      value=${id}

[common][extract] - order_id
    [Arguments]     ${output}=test_order_id
    ${id}        rest extract    $.data.order_id
    [Common] - Set variable       name=${output}      value=${id}