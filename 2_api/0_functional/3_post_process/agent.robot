*** Settings ***
Resource        ../../../1_common/keywords.robot

*** Keywords ***
[Agent][Extract] - Create relationship - id
    [Arguments]     ${output_relationship_id}=test_relationship_id
    ${token}        rest extract    $.data.id
    [Common] - Set variable       name=${output_relationship_id}      value=${token}

[Agent][Extract] - Authentication agent - access_token
    [Arguments]     ${output_authentication_agent}=agent_access_token
    ${access_token}        rest extract    $.access_token
    [Common] - Set variable       name=${output_authentication_agent}      value=${access_token}

[Agent][Extract] - API create sale hierachy
    [Arguments]     ${output_hierarchy_id}=hierarchy_id
    ${hierarchy_id}        rest extract    $.data.id
    [Common] - Set variable       name=${output_hierarchy_id}      value=${hierarchy_id}