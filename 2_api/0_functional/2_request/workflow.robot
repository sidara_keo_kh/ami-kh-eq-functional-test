*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot


*** Keywords ***
[Workflow][200] - create request to cancel voucher
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${create_request_to_cancel_voucher_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Workflow][200] - create refund voucher
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${create_refund_voucher_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Workflow][200] - approve voucher refunds
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${approve_voucher_refunds_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Workflow][200] - confirm cancel vouchers
    [Arguments]     ${headers}      ${body}      ${voucher_cancellation_id}
    ${voucher_cancellation_id}     convert to string       ${voucher_cancellation_id}
    ${path}=    replace string      ${confirm_cancel_vouchers_path}        {id}        ${voucher_cancellation_id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Workflow][200] - execute adjustment order
    [Arguments]     ${headers}      ${reference_id}
    ${reference_id}     convert to string     ${reference_id}
    ${path}=    replace string      ${execute_adjustment_order_path}        {referenceId}        ${reference_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Workflow][400] - execute adjustment order
    [Arguments]     ${headers}      ${reference_id}
    ${reference_id}     convert to string     ${reference_id}
    ${path}=    replace string      ${execute_adjustment_order_path}        {referenceId}        ${reference_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "400"

[Workflow][200] - create adjustment order
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${create_adjustment_order_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Workflow][200] - reject voucher refunds
    [Arguments]     ${headers}      ${body}
    REST.delete       ${api_gateway_host}/${reject_voucher_refunds_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Workflow][200] - reject adjustment order
    [Arguments]     ${headers}      ${reference_id}
    ${reference_id}     convert to string       ${reference_id}
    ${path}          replace string          ${reject_adjustment_order_path}       {referenceId}       ${reference_id}
    REST.delete       ${api_gateway_host}/${reject_adjustment_order_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Workflow][200] - approve multiple balance adjustments
    [Arguments]     ${headers}      ${body}
    REST.put       ${api_gateway_host}/${approve_multiple_balance_adjustments}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Workflow][200] - reject multiple balance adjustment orders
    [Arguments]     ${headers}      ${body}
    delete with body       ${api_gateway_host}/${reject_multiple_balance_adjustment_orders_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Workflow][200] - cancel balance adjustment order
    [Arguments]     ${headers}       ${refOrderId}
    ${refOrderId}     convert to string       ${refOrderId}
    ${cancel_balance_adjustment_order_path}    replace string    ${cancel_balance_adjustment_order_path}    {refOrderId}   ${refOrderId}
    REST.delete       ${api_gateway_host}/${cancel_balance_adjustment_order_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"