*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot


*** Keywords ***
[Prepaid_Card][200] - create prepaid card
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${create_virtual_prepaid_card_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Prepaid_Card][200] - create virtual prepaid card
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${prepaid_card_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Prepaid_Card][200] - create payroll card
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${create_payroll_card_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Prepaid_Card][200] - get full detail prepaid card
    [Arguments]     ${headers}      ${card_id}
    ${card_id}     convert to string       ${card_id}
    ${path}          replace string          ${get_full_detail_prepaid_card_path}       {card_id}       ${card_id}
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Prepaid_Card][200] - get prepaid card list
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${prepaid_card_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Prepaid_Card][200] - stop prepaid card
    [Arguments]     ${headers}      ${body}
    REST.put       ${api_gateway_host}/${stop_prepaid_card_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Prepaid_Card][200] - admin stop prepaid card
    [Arguments]     ${headers}      ${body}     ${card_id}
    ${card_id}     convert to string       ${card_id}
    ${path}          replace string          ${admin_stop_prepaid_card_path}       {card_id}       ${card_id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Prepaid_Card][200] - update card type
    [Arguments]     ${headers}      ${body}
    REST.put       ${api_gateway_host}/${update_card_type_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"
