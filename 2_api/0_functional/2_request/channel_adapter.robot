*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot

*** Keywords ***
[Channel_Adapter][200] - Get extension order fields via channel adapter
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_ext_order_fields_path}
        ...     headers=${headers}
    [Common][Verify] - Http status code is "200"
    rest extract

[Channel_Adapter][200] - Get hierarchy list by agent id
    [Arguments]     ${headers}
    REST.get        ${channel_gateway_host}/${get_hierarchy_list_by_agent_id_path}
    ...     headers=${headers}

    [Common][Verify] - Http status code is "200"
    rest extract