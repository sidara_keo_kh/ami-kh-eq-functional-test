*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot


*** Keywords ***
[Rule_Engine][200] - create rule
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${create_rule_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - update rule
    [Arguments]     ${headers}      ${body}     ${rule_id}
    ${rule_id}     convert to string       ${rule_id}
    ${path}=    replace string      ${update_rule_path}         {rule_id}       ${rule_id}    1
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - get rule detail
    [Arguments]     ${headers}      ${rule_id}
    ${rule_id}     convert to string       ${rule_id}
    ${path}          replace string          ${get_campaign_detail_by_id_path}       {rule_id}       ${rule_id}
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - get rule list
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_campaign_list_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - search rule
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_campaign_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - create mechanic
    [Arguments]     ${headers}      ${body}     ${rule_id}
    ${rule_id}     convert to string       ${rule_id}
    ${path}=    replace string      ${create_mechanic_path}         {rule_id}       ${rule_id}    1
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - update mechanic
    [Arguments]     ${headers}      ${body}     ${rule_id}      ${mechanic_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${path}         replace string          ${update_mechanic_path}       {rule_id}       ${rule_id}
    ${path}         replace string          ${path}       {mechanic_id}       ${mechanic_id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - get mechanic list
    [Arguments]     ${headers}     ${rule_id}
    ${rule_id}     convert to string       ${rule_id}
    ${path}         replace string          ${get_mechanic_list_path}       {rule_id}       ${rule_id}
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - get mechanic detail
    [Arguments]     ${headers}      ${rule_id}      ${mechanic_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${path}         replace string          ${get_mechanic_detail_by_id_path}       {rule_id}       ${rule_id}
    ${path}         replace string          ${path}       {mechanic_id}       ${mechanic_id}
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - delete mechanic
    [Arguments]     ${headers}      ${rule_id}      ${mechanic_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${path}         replace string          ${delete_mechanic_path}       {rule_id}       ${rule_id}
    ${path}         replace string          ${path}       {mechanic_id}       ${mechanic_id}
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - create condition
    [Arguments]     ${headers}      ${body}     ${rule_id}      ${mechanic_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${path}=    replace string      ${create_condition_path}         {rule_id}       ${rule_id}    1
    ${path}=    replace string      ${path}         {mechanic_id}       ${mechanic_id}    1
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - update condition
    [Arguments]     ${headers}      ${body}     ${rule_id}      ${mechanic_id}      ${condition_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${condition_id}     convert to string       ${condition_id}
    ${path}         replace string          ${update_condition_path}       {rule_id}       ${rule_id}
    ${path}         replace string          ${path}       {mechanic_id}       ${mechanic_id}
    ${path}         replace string          ${path}      {condition_id}       ${condition_id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - get condition list
    [Arguments]     ${headers}      ${rule_id}      ${mechanic_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${path}=    replace string      ${get_condition_list_path}         {rule_id}       ${rule_id}    1
    ${path}=    replace string      ${path}         {mechanic_id}       ${mechanic_id}    1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - get condition detail
    [Arguments]     ${headers}      ${rule_id}      ${mechanic_id}      ${condition_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${condition_id}     convert to string       ${condition_id}
    ${path}=    replace string      ${get_condition_detail_by_id_path}         {rule_id}       ${rule_id}    1
    ${path}=    replace string      ${path}         {mechanic_id}       ${mechanic_id}    1
    ${path}=    replace string         ${path}      {condition_id}       ${condition_id}
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - delete condition
    [Arguments]     ${headers}      ${rule_id}      ${mechanic_id}      ${condition_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${condition_id}     convert to string       ${condition_id}
    ${path}=    replace string      ${delete_condition_path}         {rule_id}       ${rule_id}    1
    ${path}=    replace string      ${path}         {mechanic_id}       ${mechanic_id}    1
    ${path}=    replace string         ${path}      {condition_id}       ${condition_id}
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - create comparison
    [Arguments]     ${headers}      ${body}     ${rule_id}      ${mechanic_id}      ${condition_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${condition_id}     convert to string       ${condition_id}
    ${path}=    replace string      ${create_comparison_path}         {rule_id}       ${rule_id}    1
    ${path}=    replace string      ${path}        {mechanic_id}       ${mechanic_id}    1
    ${path}=    replace string      ${path}         {condition_id}       ${condition_id}    1
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - update comparison
    [Arguments]     ${headers}      ${body}     ${rule_id}      ${mechanic_id}      ${condition_id}     ${comparison_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${condition_id}     convert to string       ${condition_id}
    ${comparison_id}     convert to string       ${comparison_id}
    ${path}=    replace string      ${update_comparison_path}         {rule_id}       ${rule_id}    1
    ${path}=    replace string      ${path}        {mechanic_id}       ${mechanic_id}    1
    ${path}=    replace string      ${path}         {condition_id}       ${condition_id}    1
    ${path}=    replace string      ${path}     {comparison_id}       ${comparison_id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - get comparison list
    [Arguments]     ${headers}      ${rule_id}      ${mechanic_id}      ${condition_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${condition_id}     convert to string       ${condition_id}
    ${path}=    replace string      ${get_all_comparison_path}         {rule_id}       ${rule_id}    1
    ${path}=    replace string      ${path}        {mechanic_id}       ${mechanic_id}    1
    ${path}=    replace string      ${path}         {condition_id}       ${condition_id}    1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - get all filter types
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_filter_type_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - get all data types
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_data_type_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - get all operators
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_operator_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - get all action types
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_action_type_list_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - create action
    [Arguments]     ${headers}      ${body}     ${rule_id}      ${mechanic_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${path}        replace string          ${create_new_action_path}      {rule_id}       ${rule_id}
    ${path}       replace string       ${path}      {mechanic_id}       ${mechanic_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - update action
    [Arguments]     ${headers}      ${body}     ${rule_id}      ${mechanic_id}      ${action_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${action_id}     convert to string       ${action_id}
    ${path}        replace string          ${update_action_path}      {rule_id}       ${rule_id}
    ${path}       replace string       ${path}      {mechanic_id}       ${mechanic_id}
    ${path}       replace string       ${path}      {action_id}       ${action_id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - get all actions
    [Arguments]     ${headers}      ${rule_id}      ${mechanic_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${path}        replace string          ${get_all_actions_path}      {rule_id}       ${rule_id}
    ${path}       replace string       ${path}      {mechanic_id}       ${mechanic_id}
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - delete action
    [Arguments]     ${headers}      ${rule_id}      ${mechanic_id}      ${action_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${action_id}     convert to string       ${action_id}
    ${path}        replace string          ${delete_action_path}      {rule_id}       ${rule_id}
    ${path}       replace string       ${path}      {mechanic_id}       ${mechanic_id}
    ${path}       replace string       ${path}     {action_id}       ${action_id}
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - create action limitation
    [Arguments]     ${headers}      ${body}     ${rule_id}      ${mechanic_id}      ${action_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${action_id}     convert to string       ${action_id}
    ${path}        replace string          ${create_limitation_path}      {rule_id}       ${rule_id}
    ${path}       replace string       ${path}      {mechanic_id}       ${mechanic_id}
    ${path}       replace string       ${path}     {action_id}       ${action_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - update action limitation
    [Arguments]     ${headers}      ${body}     ${rule_id}      ${mechanic_id}      ${action_id}         ${limit_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${action_id}     convert to string       ${action_id}
    ${limit_id}     convert to string       ${limit_id}
    ${path}        replace string          ${update_action_limit_path}      {rule_id}       ${rule_id}
    ${path}       replace string       ${path}      {mechanic_id}       ${mechanic_id}
    ${path}       replace string       ${path}     {action_id}       ${action_id}
    ${path}       replace string       ${path}    {limit_id}       ${limit_id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - get action limitation list
    [Arguments]     ${headers}      ${rule_id}      ${mechanic_id}       ${action_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${action_id}     convert to string       ${action_id}
    ${path}        replace string        ${list_limitation_path}      {rule_id}       ${rule_id}
    ${path}        replace string        ${path}      {mechanic_id}       ${mechanic_id}
    ${path}        replace string        ${path}       {action_id}       ${action_id}
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - delete action limit
    [Arguments]     ${headers}      ${rule_id}      ${mechanic_id}      ${action_id}        ${limit_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${action_id}     convert to string       ${action_id}
    ${limit_id}     convert to string       ${limit_id}
    ${path}        replace string          ${delete_action_limit_path}      {rule_id}       ${rule_id}
    ${path}       replace string       ${path}      {mechanic_id}       ${mechanic_id}
    ${path}       replace string       ${path}     {action_id}       ${action_id}
    ${path}       replace string       ${path}      {limit_id}       ${limit_id}
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - create condition filter
    [Arguments]     ${headers}      ${body}     ${rule_id}      ${mechanic_id}      ${condition_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${condition_id}     convert to string       ${condition_id}
    ${path}       replace string         ${create_condition_filter_path}      {rule_id}       ${rule_id}
    ${path}       replace string         ${path}      {mechanic_id}       ${mechanic_id}
    ${path}       replace string         ${path}    {condition_id}       ${condition_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - update condition filter
    [Arguments]     ${headers}      ${body}     ${rule_id}      ${mechanic_id}      ${condition_id}     ${filter_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${condition_id}     convert to string       ${condition_id}
    ${filter_id}     convert to string       ${filter_id}
    ${path}       replace string         ${update_condition_filter_path}      {rule_id}       ${rule_id}
    ${path}       replace string         ${path}      {mechanic_id}       ${mechanic_id}
    ${path}       replace string         ${path}    {condition_id}       ${condition_id}
    ${path}       replace string         ${path}    {filter_id}       ${filter_id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - get condition filter list
    [Arguments]     ${headers}      ${rule_id}      ${mechanic_id}       ${condition_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${condition_id}     convert to string       ${condition_id}
    ${path}        replace string        ${get_condition_filter_list_path}      {rule_id}       ${rule_id}
    ${path}        replace string        ${path}      {mechanic_id}       ${mechanic_id}
    ${path}        replace string        ${path}     {condition_id}       ${condition_id}
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - delete condition filter
    [Arguments]     ${headers}      ${rule_id}      ${mechanic_id}      ${condition_id}     ${filter_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${condition_id}     convert to string       ${condition_id}
    ${filter_id}     convert to string       ${filter_id}
    ${path}       replace string         ${delete_condition_filter_path}      {rule_id}       ${rule_id}
    ${path}       replace string         ${path}      {mechanic_id}       ${mechanic_id}
    ${path}       replace string         ${path}    {condition_id}       ${condition_id}
    ${path}       replace string         ${path}      {filter_id}       ${filter_id}
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - create condition reset filter
    [Arguments]     ${headers}      ${body}     ${rule_id}      ${mechanic_id}      ${condition_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${condition_id}     convert to string       ${condition_id}
    ${path}       replace string         ${create_condition_reset_filter_path}      {rule_id}       ${rule_id}
    ${path}       replace string         ${path}      {mechanic_id}       ${mechanic_id}
    ${path}       replace string         ${path}    {condition_id}       ${condition_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - get condition reset filter list
    [Arguments]     ${headers}      ${rule_id}      ${mechanic_id}      ${condition_id}
    ${rule_id}     convert to string       ${rule_id}
    ${mechanic_id}     convert to string       ${mechanic_id}
    ${condition_id}     convert to string       ${condition_id}
    ${path}       replace string         ${get_condition_reset_filter_list_path}      {rule_id}       ${rule_id}
    ${path}       replace string         ${path}      {mechanic_id}       ${mechanic_id}
    ${path}       replace string         ${path}    {condition_id}       ${condition_id}
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - search action history
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_action_history_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - create rule limitation
    [Arguments]     ${headers}      ${body}     ${rule_id}
    ${rule_id}     convert to string       ${rule_id}
    ${path}        replace string          ${create_rule_limit_path}      {rule_id}       ${rule_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - get rule limit list
    [Arguments]     ${headers}      ${rule_id}
    ${rule_id}     convert to string       ${rule_id}
    ${path}       replace string         ${get_rule_limit list_path}    {rule_id}       ${rule_id}
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Rule_Engine][200] - delete rule limit
    [Arguments]     ${headers}      ${rule_id}          ${rule_limit_id}
    ${rule_id}     convert to string       ${rule_id}
    ${rule_limit_id}     convert to string       ${rule_limit_id}
    ${path}         replace string         ${delete_rule_limit_path}       {rule_id}       ${rule_id}
    ${path}         replace string         ${path}        {rule_limit_id}       ${rule_limit_id}
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"
