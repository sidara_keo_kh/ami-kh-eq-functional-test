*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot


*** Keywords ***
[Payroll][200] - bank agent create and execute payroll order
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${bank_agent_create_and_execute_payroll_order_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payroll][200] - system create and execute payroll order
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${system_create_and_execute_payroll_order_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payroll][400] - system create and execute payroll order
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${system_create_and_execute_payroll_order_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "400"

[Payroll][200] - cancel payroll order
    [Arguments]     ${headers}       ${refOrderId}
    ${refOrderId}     convert to string       ${refOrderId}
    ${cancel_payroll_order_path}    replace string    ${cancel_payroll_order_path}    {refOrderId}   ${refOrderId}
    REST.delete       ${api_gateway_host}/${cancel_payroll_order_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"
