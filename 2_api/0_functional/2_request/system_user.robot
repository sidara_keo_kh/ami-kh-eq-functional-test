*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot

*** Keywords ***
[System_User][200] - Create system user
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${create_system_user_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[System_User][200] - Add permission to role
    [Arguments]     ${headers}      ${body}     ${role_id}
    ${role_id}     convert to string       ${role_id}
    ${path}     replace string      ${add_permissions_to_role_path}     {roleId}    ${role_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[System_User][200] - Create role
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${create_role_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[System_User][200] - Delete system user
    [Arguments]     ${headers}     ${user_id}
    ${user_id}     convert to string       ${user_id}
    ${path}     replace string      ${delete_system_user_path}     {user_id}    ${user_id}
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[System_User][200] - Get system user by access token
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_system_user_by_access_token_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[System_User][200] - Update system user
    [Arguments]     ${headers}      ${body}    ${user_id}
    ${user_id}     convert to string       ${user_id}
    ${path}      replace string    ${update_system_user_path}    {id}        ${user_id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[System_User][200] - Update system user password
    [Arguments]     ${headers}      ${body}     ${user_id}
    ${user_id}     convert to string       ${user_id}
    ${path}      replace string    ${update_system_-_user_password_path}    {userId}        ${user_id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[System_User][200] - Change system user password
    [Arguments]     ${headers}      ${body}
    REST.put       ${api_gateway_host}/${change_system_-_user_password_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[System_User][200] - Get role and permission
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_role_and_permission_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[System_User][200] - Create permission
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${create_permission_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[System_User][200] - Update permission
    [Arguments]     ${headers}      ${body}     ${permission_id}
    ${permission_id}     convert to string       ${permission_id}
    ${path}       replace string      ${update_permission_path}     {permissionId}    ${permission_id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[System_User][200] - Delete permission
    [Arguments]     ${headers}      ${permission_id}
    ${permission_id}     convert to string       ${permission_id}
    ${path}       replace string      ${delete_permission_path}     {permissionId}    ${permission_id}
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[System_User][200] - Remove permission from role
    [Arguments]     ${headers}      ${body}    ${role_id}
    ${role_id}     convert to string       ${role_id}
    ${path}     replace string      ${remove_permissions_from_role_path}     {roleId}      ${role_id}
    delete with body       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[System_User][200] - Update role
    [Arguments]     ${headers}      ${body}     ${role_id}
    ${role_id}     convert to string       ${role_id}
    ${path}       replace string      ${update_role_path}     {roleId}    ${role_id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[System_User][200] - Delete role
    [Arguments]     ${headers}      ${role_id}
    ${role_id}     convert to string       ${role_id}
    ${path}       replace string      ${delete_role_path}     {roleId}    ${role_id}
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[System_User][200] - Add user to role
    [Arguments]     ${headers}      ${body}     ${role_id}
    ${role_id}     convert to string       ${role_id}
    ${path}     replace string      ${add_user_to_role_path}     {roleId}    ${role_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[System_User][200] - Unlink role from user
    [Arguments]     ${headers}      ${body}     ${user_id}
    ${user_id}     convert to string       ${user_id}
    ${path}     replace string      ${unlink_role_from_user_path}     {userId}      ${user_id}
    delete with body       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"
    
[System_User][200] - link system user and company profile
    [Arguments]     ${headers}      ${body}     ${user_id}
    ${user_id}     convert to string       ${user_id}
    ${path}     replace string      ${link_system_user_company_profile_path}     {userId}      ${user_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"
    
[System_User][200] - Active suspend system user
    [Arguments]     ${headers}      ${body}     ${user_id}
    ${user_id}     convert to string       ${user_id}
    ${path}      replace string    ${system_user_active_suspend_system_user_path}    {system_user_id}   ${user_id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"