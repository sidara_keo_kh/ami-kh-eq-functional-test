*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot


*** Keywords ***
[Password_Center][200] - update password rule config
    [Arguments]     ${headers}      ${body}          ${identity_type_id}
    ${identity_type_id}     convert to string       ${identity_type_id}
    ${path}          replace string          ${update_password_rule_config_path}       {identity_type_id}       ${identity_type_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Password_Center][200] - create identity type
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${create_identity_type}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Password_Center][200] - update identity type
    [Arguments]     ${headers}      ${body}          ${identity_type_id}
    ${identity_type_id}     convert to string       ${identity_type_id}
    ${path}          replace string          ${update_identity_type}       {identity_type_id}       ${identity_type_id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Password_Center][200] - delete identity type
    [Arguments]     ${headers}      ${identity_type_id}
    ${identity_type_id}     convert to string       ${identity_type_id}
    ${path}          replace string          ${delete_identity_type}       {identity_type_id}       ${identity_type_id}
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"