*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot

*** Keywords ***

[Customer][200] - create customer classification
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${customer_classification_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - update customer classification
    [Arguments]     ${headers}      ${body}         ${customer_classification_id}
    ${customer_classification_id}     convert to string       ${customer_classification_id}
    ${path}=    replace string      ${update_customer_classification_path}      {customer_classification_id}        ${customer_classification_id}   1
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - delete customer classification
    [Arguments]     ${headers}      ${customer_classification_id}
    ${customer_classification_id}     convert to string       ${customer_classification_id}
    ${path}=    replace string      ${delete_customer_classification_path}      {customer_classification_id}        ${customer_classification_id}   1
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - get customer profiles
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_customer_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - active-suspend customer
    [Arguments]     ${headers}      ${body}         ${customer_id}
    ${customer_id}     convert to string       ${customer_id}
    ${path}=    replace string        ${active-suspend_customer_path}       {customer_id}         ${customer_id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - get all customer identities
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_customer_identities_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - add customer identities
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${add_customer_identities_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - system user create customer
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${system_user_create_customer_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - register customer
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${register_customer_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - system user creates customer profile
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${system_user_create_customer_profile_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - customer registers customer profile
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${register_customer_profile_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - system user updates full customer profile
    [Arguments]     ${headers}      ${body}         ${customer_id}
    ${customer_id}     convert to string       ${customer_id}
    ${path}=    replace string      ${system_user_update_customer_path}      {customer_id}        ${customer_id}   1
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - system user updates partial customer profile
    [Arguments]     ${headers}      ${body}         ${customer_id}
    ${customer_id}     convert to string       ${customer_id}
    ${path}=    replace string      ${system_user_update_customer_path}      {customer_id}        ${customer_id}   1
    REST.patch       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - customer updates full customer profile
    [Arguments]     ${headers}      ${body}
    REST.put       ${api_gateway_host}/${customer_update_profile_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - customer updates partial customer profile
    [Arguments]     ${headers}      ${body}
    REST.patch       ${api_gateway_host}/${customer_update_profile_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - delete customer
    [Arguments]     ${headers}      ${customer_id}
    ${customer_id}     convert to string       ${customer_id}
    ${path}=    replace string      ${delete_customer_path}      {customer_id}        ${customer_id}   1
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - update customer kyc
    [Arguments]     ${headers}      ${body}         ${customer_id}
    ${customer_id}     convert to string       ${customer_id}
    ${path}=    replace string      ${update_customer_kyc_path}      {customer_id}        ${customer_id}   1
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - reset customer password
    [Arguments]     ${headers}      ${customer_id}          ${identity_id}
    ${customer_id}     convert to string       ${customer_id}
    ${identity_id}     convert to string       ${identity_id}
    ${path}=    replace string      ${reset_customer_password_path}      {customer_id}        ${customer_id}   1
    ${path}=    replace string      ${path}      {identity_id}        ${identity_id}   1
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - delete customer identity
    [Arguments]     ${headers}      ${identity_id}
    ${identity_id}     convert to string       ${identity_id}
    ${path}=    replace string      ${delete_customer_identity_path}      {identity_id}        ${identity_id}   1
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - update customer password
    [Arguments]     ${headers}      ${body}         ${identity_id}
    ${identity_id}     convert to string       ${identity_id}
    ${path}=    replace string      ${update_customer_password_path}      {identity_id}        ${identity_id}   1
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - update customer identity
    [Arguments]     ${headers}      ${body}         ${identity_id}
    ${identity_id}     convert to string       ${identity_id}
    ${path}=    replace string      ${update_customer_identity_path}      {identity_id}        ${identity_id}   1
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - customer creates device
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${customer_create_devices_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - customer updates device
    [Arguments]     ${headers}      ${body}         ${device_id}
    ${device_id}     convert to string       ${device_id}
    ${path}=    replace string      ${customer_update_devices_path}      {device_id}        ${device_id}   1
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - system user updates customer device
    [Arguments]     ${headers}      ${body}         ${device_id}
    ${device_id}     convert to string       ${device_id}
    ${path}=    replace string      ${system_user_update_customer_devices_path}      {device_id}        ${device_id}   1
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - system user updates customer device status
    [Arguments]     ${headers}      ${body}         ${device_id}
    ${device_id}     convert to string       ${device_id}
    ${path}=    replace string      ${system_user_update_customer_device_status_path}      {device_id}        ${device_id}   1
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - customer deletes device
    [Arguments]     ${headers}      ${device_id}
    ${device_id}     convert to string       ${device_id}
    ${path}=    replace string      ${customer_delete_devices_path}      {device_id}        ${device_id}   1
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - system user deletes customer device
    [Arguments]     ${headers}      ${device_id}
    ${device_id}     convert to string       ${device_id}
    ${path}=    replace string      ${system_user_delete_customer_devices_path}      {device_id}        ${device_id}   1
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - customer gets all devices
    [Arguments]     ${headers}      ${channel_id}
    ${channel_id}     convert to string       ${channel_id}
    ${path}=    replace string      ${customer_get_all_devices_path}      {channel_id}        ${channel_id}   1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Customer][200] - system user gets customer device
    [Arguments]     ${headers}      ${device_id}
    ${device_id}     convert to string       ${device_id}
    ${path}=    replace string      ${system_user_get_customer_devices_path}      {device_id}        ${device_id}  1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"