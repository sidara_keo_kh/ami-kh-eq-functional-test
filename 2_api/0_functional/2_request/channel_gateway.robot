*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot


*** Keywords ***
[Channel_Gateway][200] - add service
    [Arguments]     ${headers}      ${body}
    REST.post       ${channel_gateway_host}/${channel_gateway_add_service_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Channel_Gateway][200] - update service
    [Arguments]     ${headers}      ${body}     ${service_id}
    ${service_id}     convert to string       ${service_id}
    ${path}          replace string          ${channel_gateway_update_service_path}       {serviceId}       ${service_id}
    REST.put       ${channel_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Channel_Gateway][200] - search service
    [Arguments]     ${headers}       ${body}
    REST.post       ${channel_gateway_host}/${channel_gateway_search_service_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Channel_Gateway][200] - delete service
    [Arguments]     ${headers}      ${service_id}
    ${service_id}     convert to string       ${service_id}
    ${path}      replace string      ${channel_gateway_delete_service_path}     {serviceId}    ${service_id}
    REST.delete       ${channel_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Channel_Gateway][200] - get all services
    [Arguments]     ${headers}
    REST.get       ${channel_gateway_host}/${channel_gateway_get_all_services_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Channel_Gateway][200] - get all apis
    [Arguments]     ${headers}
    REST.get       ${channel_gateway_host}/${channel_gateway_get_all_apis_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Channel_Gateway][200] - update api
    [Arguments]     ${headers}      ${body}     ${api_id}
    ${api_id}     convert to string       ${api_id}
    ${path}          replace string          ${channel_gateway_update_api_path}       {apiId}       ${api_id}
    REST.put       ${channel_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Channel_Gateway][200] - delete api
    [Arguments]     ${headers}      ${api_id}
    ${api_id}     convert to string       ${api_id}
    ${path}          replace string          ${channel_gateway_delete_api_path}       {apiId}       ${api_id}
    REST.delete       ${channel_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Channel_Gateway][200] - search api
    [Arguments]     ${headers}       ${body}
    REST.post       ${channel_gateway_host}/${channel_gateway_search_api_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Channel_Gateway][200] - add api
    [Arguments]     ${headers}      ${body}
    REST.post       ${channel_gateway_host}/${channel_gateway_add_api_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Channel_Gateway][200] - agent authentication
    [Arguments]     ${headers}      ${params}
    REST.post       ${channel_gateway_host}/${channel_gateway_agent_authenticate}?${params}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Channel_Gateway][200] - revoke auth token
    [Arguments]     ${headers}
    REST.post       ${channel_gateway_host}/${channel_gateway_agent_auth_revoke}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Channel_Gateway][200] - add scopes to client
    [Arguments]     ${headers}       ${body}        ${client_id}
    ${client_id}     convert to string       ${client_id}
    ${path}          replace string          ${channel_gateway_add_scopes_to_client_path}       {clientId}       ${client_id}
    REST.post       ${channel_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Channel_Gateway][200] - get client scopes
    [Arguments]     ${headers}       ${client_id}
    ${client_id}     convert to string       ${client_id}
    ${path}          replace string          ${channel_gateway_get_client_scopes_path}       {clientId}       ${client_id}
    REST.get       ${channel_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Channel_Gateway][200] - delete scope from client
    [Arguments]     ${headers}       ${body}        ${client_id}
    ${client_id}     convert to string       ${client_id}
    ${path}          replace string          ${channel_gateway_delete_scope_from_client_path}       {clientId}       ${client_id}
    REST.delete       ${channel_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Channel_Gateway][200] - get all clients
    [Arguments]     ${headers}
    REST.get       ${channel_gateway_host}/${channel_gateway_get_all_clients_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"