*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot

*** Keywords ***
[Inventory][200] - Create single item
    [Arguments]    ${headers}    ${body}
    REST.post    ${api_gateway_host}/${inventory_create_single_sku_item_path}
    ...    headers=${headers}
    ...    body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"
    [Common][Verify] - Status code is "${inventory_success_code}"
    [Common][Verify] - Status message is "${inventory_success_message}"

[Inventory][200] - Get single item
    [Arguments]    ${headers}    ${item_id}
    ${item_id}    convert to string    ${item_id}
    ${path}    replace string    ${inventory_get_single_sku_item_by_id_path}    {itemId}    ${item_id}    1
    REST.get    ${api_gateway_host}/${path}
    ...    headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"
    [Common][Verify] - Status code is "${inventory_success_code}"
    [Common][Verify] - Status message is "${inventory_success_message}"
    
[Inventory][200] - Create single item serial
    [Arguments]    ${headers}    ${body}    ${item_id}
    ${item_id}    convert to string    ${item_id}
    ${path}    replace string    ${inventory_create_single_sku_item_serial_path}    {itemId}    ${item_id}    1
    REST.post    ${api_gateway_host}/${path}
    ...    headers=${headers}
    ...    body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"
    [Common][Verify] - Status code is "${inventory_success_code}"
    [Common][Verify] - Status message is "${inventory_success_message}"

[Inventory][200] - Get single item serial
    [Arguments]    ${headers}    ${query}    ${item_id}
    ${item_id}    convert to string    ${item_id}
    ${path}    replace string    ${inventory_get_single_sku_item_serial_path}    {itemId}    ${item_id}    1
    REST.get    ${api_gateway_host}/${path}
    ...    headers=${headers}
    ...    query=${query}
    rest extract
    [Common][Verify] - Http status code is "200"
    [Common][Verify] - Status code is "${inventory_success_code}"
    [Common][Verify] - Status message is "${inventory_success_message}"

[Inventory][200] - Update single item quantity
    [Arguments]    ${headers}    ${body}    ${item_id}
    ${item_id}    convert to string    ${item_id}
    ${path}    replace string    ${inventory_update_single_sku_item_quantity_path}    {itemId}    ${item_id}    1
    REST.put    ${api_gateway_host}/${path}
    ...    headers=${headers}
    ...    body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"
    [Common][Verify] - Status code is "${inventory_success_code}"
    [Common][Verify] - Status message is "${inventory_success_message}"

[Inventory][200] - Create group item
    [Arguments]    ${headers}    ${body}
    REST.post    ${api_gateway_host}/${inventory_create_group_item_path}
    ...    headers=${headers}
    ...    body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"
    [Common][Verify] - Status code is "${inventory_success_code}"
    [Common][Verify] - Status message is "${inventory_success_message}"

[Inventory][200] - Get group item
    [Arguments]    ${headers}    ${group_id}
    ${group_id}    convert to string    ${group_id}
    ${path}    replace string    ${inventory_get_group_by_id_path}    {groupId}    ${group_id}    1
    REST.get    ${api_gateway_host}/${path}
    ...    headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"
    [Common][Verify] - Status code is "${inventory_success_code}"
    [Common][Verify] - Status message is "${inventory_success_message}"
    
[Inventory][200] - Create ticket
    [Arguments]    ${headers}    ${body}
    REST.post    ${api_gateway_host}/${inventory_create_ticket_path}
    ...    headers=${headers}
    ...    body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"
    [Common][Verify] - Status code is "${inventory_success_code}"
    [Common][Verify] - Status message is "${inventory_success_message}"
    
[Inventory][200] - Create change ticket
    [Arguments]    ${headers}    ${body}
    REST.post    ${api_gateway_host}/${inventory_create_change_ticket_path}
    ...    headers=${headers}
    ...    body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"
    [Common][Verify] - Status code is "${inventory_success_code}"
    [Common][Verify] - Status message is "${inventory_success_message}"
    
[Inventory][400] - Create ticket
    [Arguments]    ${headers}    ${body}
    REST.post    ${api_gateway_host}/${inventory_create_ticket_path}
    ...    headers=${headers}
    ...    body=${body}
    rest extract
    [Common][Verify] - Http status code is "400"
    
[Inventory][400] - Create change ticket
    [Arguments]    ${headers}    ${body}
    REST.post    ${api_gateway_host}/${inventory_create_change_ticket_path}
    ...    headers=${headers}
    ...    body=${body}
    rest extract
    [Common][Verify] - Http status code is "400"    

[Inventory][400] - update ticket
    [Arguments]    ${headers}    ${body}    ${ticket_id}
    ${ticket_id}    convert to string    ${ticket_id}
    ${path}    replace string    ${inventory_update_ticket_path}    {ticketId}    ${ticket_id}    1
    REST.put    ${api_gateway_host}/${path}
    ...    headers=${headers}
    ...    body=${body}
    rest extract
    [Common][Verify] - Http status code is "400"
    
[Inventory][200] - Get ticket
    [Arguments]    ${headers}    ${ticket_id}
    ${ticket_id}    convert to string    ${ticket_id}
    ${path}    replace string    ${inventory_get_ticket_by_id_path}    {ticketId}    ${ticket_id}    1
    REST.get    ${api_gateway_host}/${path}
    ...    headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"
    [Common][Verify] - Status code is "${inventory_success_code}"
    [Common][Verify] - Status message is "${inventory_success_message}"

[Inventory][200] - Get change ticket
    [Arguments]    ${headers}    ${ticket_id}
    ${ticket_id}    convert to string    ${ticket_id}
    ${path}    replace string    ${inventory_get_change_ticket_by_id_path}    {ticketId}    ${ticket_id}    1
    REST.get    ${api_gateway_host}/${path}
    ...    headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"
    [Common][Verify] - Status code is "${inventory_success_code}"
    [Common][Verify] - Status message is "${inventory_success_message}"

[Inventory][400] - Get change ticket
    [Arguments]    ${headers}    ${ticket_id}
    ${ticket_id}    convert to string    ${ticket_id}
    ${path}    replace string    ${inventory_get_change_ticket_by_id_path}    {ticketId}    ${ticket_id}    1
    REST.get    ${api_gateway_host}/${path}
    ...    headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "400"
    
[Inventory][200] - Search tickets
    [Arguments]    ${headers}    ${body}
    REST.post    ${api_gateway_host}/${inventory_search_ticket_path}
    ...    headers=${headers}
    ...    body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"
    [Common][Verify] - Status code is "${inventory_success_code}"
    [Common][Verify] - Status message is "${inventory_success_message}"
    
[Inventory][200] - Update ticket status
    [Arguments]    ${headers}    ${body}    ${ticket_id}
    sleep    1s
    ${ticket_id}    convert to string    ${ticket_id}
    ${path}    replace string    ${inventory_update_ticket_status_path}    {ticketId}    ${ticket_id}    1
    REST.put    ${api_gateway_host}/${path}
    ...    headers=${headers}
    ...    body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"
    [Common][Verify] - Status code is "${inventory_success_code}"
    [Common][Verify] - Status message is "${inventory_success_message}"

[Inventory][400] - Update ticket status
    [Arguments]    ${headers}    ${body}    ${ticket_id}
    ${ticket_id}    convert to string    ${ticket_id}
    ${path}    replace string    ${inventory_update_ticket_status_path}    {ticketId}    ${ticket_id}    1
    REST.put    ${api_gateway_host}/${path}
    ...    headers=${headers}
    ...    body=${body}
    rest extract
    [Common][Verify] - Http status code is "400"

[Inventory][200] - update ticket
    [Arguments]    ${headers}    ${body}    ${ticket_id}
    ${ticket_id}    convert to string    ${ticket_id}
    ${path}    replace string    ${inventory_update_ticket_path}    {ticketId}    ${ticket_id}    1
    REST.put    ${api_gateway_host}/${path}
    ...    headers=${headers}
    ...    body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"
    [Common][Verify] - Status code is "${inventory_success_code}"
    [Common][Verify] - Status message is "${inventory_success_message}"
    
[Inventory][200] - Delete single item serial
    [Arguments]    ${headers}    ${item_id}    ${serial_number}
    ${item_id}    convert to string    ${item_id}
    ${serial_number}    convert to string    ${serial_number}
    ${path}    replace string    ${inventory_delete_single_sku_item_serial_path}    {itemId}    ${item_id}    1
    ${path}    replace string    ${path}    {serial_number}    ${serial_number}    1
    REST.delete   ${api_gateway_host}/${path}
    ...    headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"
    
[Inventory][200] - Get all single items
    [Arguments]    ${headers}
    REST.get    ${api_gateway_host}/${inventory_get_all_single_sku_items}
    ...    headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"    
    
[Inventory][200] - Get group item by sku
    [Arguments]    ${headers}    ${sku}
    ${sku}    convert to string    ${sku}
    ${path}    replace string    ${inventory_get_group_by_sku_path}    {sku}    ${sku}    1
    REST.get    ${api_gateway_host}/${path}
    ...    headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"
    
[Inventory][200] - Get single item by sku
    [Arguments]    ${headers}    ${sku}
    ${sku}    convert to string    ${sku}
    ${path}    replace string    ${inventory_get_single_sku_item_by_sku_path}    {sku}    ${sku}    1
    REST.get    ${api_gateway_host}/${path}
    ...    headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"
    
[Inventory][200] - Get ticket note
    [Arguments]    ${headers}    ${ticket_id}
    ${ticket_id}    convert to string    ${ticket_id}
    ${path}    replace string    ${inventory_get_ticket_note_by_id_path}    {ticketId}    ${ticket_id}    1
    REST.get    ${api_gateway_host}/${path}
    ...    headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"
    
[Inventory][200] - Search item
    [Arguments]    ${headers}    ${body}
    REST.post    ${api_gateway_host}/${inventory_search_item_path}
    ...    headers=${headers}
    ...    body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Inventory][200] - Update group item
    [Arguments]    ${headers}    ${body}    ${group_id}
    ${group_id}    convert to string    ${group_id}
    ${path}    replace string    ${inventory_update_group_item_by_id_path}    {groupId}    ${group_id}    1
    REST.put    ${api_gateway_host}/${path}
    ...    headers=${headers}
    ...    body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"
    
[Inventory][200] - Update single item
    [Arguments]    ${headers}    ${body}    ${item_id}
    ${item_id}    convert to string    ${item_id}
    ${path}    replace string    ${inventory_update_single_sku_item_by_id_path}    {itemId}    ${item_id}    1
    REST.put    ${api_gateway_host}/${path}
    ...    headers=${headers}
    ...    body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"
    
[Inventory][200] - Update single item serial
    [Arguments]    ${headers}    ${body}    ${item_id}
    ${item_id}    convert to string    ${item_id}
    ${path}    replace string    ${inventory_update_single_sku_item_serial_path}    {itemId}    ${item_id}    1
    REST.put    ${api_gateway_host}/${path}
    ...    headers=${headers}
    ...    body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"
    
[Inventory][200] - Update single item serial by id
    [Arguments]    ${headers}    ${body}    ${item_id}    ${serial_id}
    ${item_id}    convert to string    ${item_id}
    ${serial_id}    convert to string    ${serial_id}
    ${path}    replace string    ${inventory_update_single_sku_item_serial_by_id_path}    {itemId}    ${item_id}    1
    ${path}    replace string    ${path}    {serial_id}    ${serial_id}    1
    REST.put    ${api_gateway_host}/${path}
    ...    headers=${headers}
    ...    body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"
    
[Inventory][200] - Update single item serial status
    [Arguments]    ${headers}    ${body}    ${item_id}    ${serial_number}
    ${item_id}    convert to string    ${item_id}
    ${serial_number}    convert to string    ${serial_number}
    ${path}    replace string    ${inventory_update_single_sku_item_serial_status_path}    {itemId}    ${item_id}    1
    ${path}    replace string    ${path}    {serial_number}    ${serial_number}    1
    REST.put    ${api_gateway_host}/${path}
    ...    headers=${headers}
    ...    body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"
    
[Inventory][200] - Update single item status
    [Arguments]    ${headers}    ${body}    ${item_id}
    ${item_id}    convert to string    ${item_id}
    ${path}    replace string    ${inventory_update_sku_item_status_by_id_path}    {itemId}    ${item_id}    1
    REST.put    ${api_gateway_host}/${path}
    ...    headers=${headers}
    ...    body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"