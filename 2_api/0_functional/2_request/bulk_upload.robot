*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot


*** Keywords ***
[Bulk_Upload][200] - post bulk file
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${post_bulk_file_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Bulk_Upload][200] - upload bulk file
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${upload_bulk_file_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Bulk_Upload][200] - download bulk file
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${download_bulk_file_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"