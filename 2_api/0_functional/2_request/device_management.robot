*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot


*** Keywords ***
[Device_Management][200] - create channel
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${create_channel_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Device_Management][200] - delete channel
    [Arguments]     ${headers}      ${channel_id}
    ${channel_id}     convert to string       ${channel_id}
    ${path}          replace string          ${delete_channel_path}       {channel_id}       ${channel_id}
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Device_Management][200] - create channel access permission
    [Arguments]     ${headers}      ${body}     ${channel_id}
    ${channel_id}     convert to string       ${channel_id}
    ${path}          replace string          ${create_channel_access_permission_path}       {channel_id}       ${channel_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Device_Management][200] - revoke channel access permission
    [Arguments]     ${headers}      ${body}     ${channel_id}
    ${channel_id}     convert to string       ${channel_id}
    ${path}          replace string          ${revoke_channel_access_permission_path}       {channel_id}       ${channel_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"
