*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot

*** Keywords ***
[Report][200] - search card history
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_card_history_on_report_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search OTP
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_otp_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search customer
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_customer_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Export customer
    [Arguments]     ${headers}      ${body}
    Enable rest content base 64
    REST.post       ${api_gateway_host}/${search_customer_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search system-user
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_system-user_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search cash source of fund
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_cash_source_of_fund_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Export cash source of fund
    [Arguments]     ${headers}      ${body}
    Enable rest content base 64
    REST.post       ${api_gateway_host}/${search_cash_source_of_fund_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search cash transaction
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_cash_transaction_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search payment orders
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_payment_orders_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search bank source of fund
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_bank_source_of_fund_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Export bank source of fund
    [Arguments]     ${headers}      ${body}
    Enable rest content base 64
    REST.post       ${api_gateway_host}/${search_bank_source_of_fund_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search bank
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_bank_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Report - search bank transactions
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${report_-_search_bank_transactions_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search voucher
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_voucher_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search voucher refunds
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_voucher_refunds_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search token information
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${report_search_trust_token_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search agent devices
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${report_search_agent_device}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search customer classifications
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_customer_classifications}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search service group
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_service_group}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Export payment service group
    [Arguments]     ${headers}      ${body}
    Enable rest content base 64
    REST.post       ${api_gateway_host}/${search_service_group}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search payment services
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_services}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Export payment services
    [Arguments]     ${headers}      ${body}
    Enable rest content base 64
    REST.post       ${api_gateway_host}/${search_services}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Get payment order detail
    [Arguments]     ${headers}      ${order_id}
    ${order_id}     convert to string       ${order_id}
    ${path}          replace string          ${get_report_order_detail_path}       {orderId}       ${order_id}
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search vouchers distributed to user
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_vouchers_distributed_to_user_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Get list of cancel voucher in formation by cancel id
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${get_list_of_cancel_voucher_in_formation_by_cancel_id_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search voucher adjustments
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_voucher_adjustments_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search password rule configuration
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_password_rule_configuration_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search identity types
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_identity_types_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search customer devices
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_customer_devices_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search customer identity
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_customer_identity_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search agent
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_agent_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Export agent
    [Arguments]     ${headers}      ${body}
    Enable rest content base 64
    REST.post       ${api_gateway_host}/${search_agent_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search agent company profile
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_agent_company_profile_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search agent accreditation status
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_agent_accreditation_status_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search agent mm card type
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_agent_mm_card_type_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Get all agent identities
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${get_all_agent_identities_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search agent classifications
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_agent_classifications_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search agent smart card
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_agents_mart_card_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search shop type
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_shop_type_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search agent shop
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_agent_shop_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search shop simple profile
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_agent_shop_simple_profile_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search channels permission
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_channels_permission_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search edc
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_edc_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search agent relationship
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_agent_relationship_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search categories
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_categories_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search products
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_products_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search channels
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_channels_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search products agent type relation
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_products_agent_type_relation_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search card on report
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_card_on_report_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search shop categories
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_shop_categories_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search SOF bank list
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${report_search_SOF_bank_list_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search reconciled partner file
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_reconciled_partner_file_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search reconciled partner reconcile result
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_reconciled_partner_reconcile_result_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search reconciled sof file
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_reconciled_sof_file_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search reconciled sof reconcile result
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_reconciled_sof_reconcile_result_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Get all roles
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${get_all_roles_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Get all permissions
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${get_all_permissions_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - search card source of fund
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_card_source_of_fund_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Export card source of fund
    [Arguments]     ${headers}      ${body}
    Enable rest content base 64
    REST.post       ${api_gateway_host}/${search_card_source_of_fund_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search card type
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_card_type_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search card sof provider
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_card_sof_provider_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search card design
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_card_design_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search card transactions
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_card_transactions_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search card sof action history
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_card_sof_action_history_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Get all balance adjustments
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${get_all_balance_adjustments_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search list of fraud tickets
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_list_of_fraud_tickets_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Add service to report service whitelist
    [Arguments]     ${headers}      ${body}     ${report_type_id}
    ${report_type_id}     convert to string       ${report_type_id}
    ${path}      replace string    ${add_service_ids_to_report_service_whitelist_path}    {report_type_id}    ${report_type_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Get all service from report service whitelist
    [Arguments]     ${headers}      ${report_type_id}
    ${path}      replace string    ${get_all_service_ids_from_report_service_whitelist_path}    {report_type_id}    ${report_type_id}
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Remove service from report service whitelist
    [Arguments]     ${headers}      ${body}     ${report_type_id}
    ${report_type_id}     convert to string       ${report_type_id}
    ${path}      replace string    ${remove_service_ids_to_report_service_whitelist_path}    {report_type_id}    ${report_type_id}
    delete with body       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search agent commission
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_agent_commission_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Export agent commission
    [Arguments]     ${headers}      ${body}
    Enable rest content base 64
    REST.post       ${api_gateway_host}/${search_agent_commission_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Summary agents commission
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${summary_agents_commission_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Summary payment transaction agent
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${summary_payment_transaction_agent_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Summary transaction own agent
    [Arguments]     ${headers}      ${body}     ${agent_id}
    ${agent_id}     convert to string       ${agent_id}
    ${path}         replace string    ${summary_transaction_own_agent_path}    {agent_id}   ${agent_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Summary customer wallet info
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${summary_customer_wallet_info_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Summary agent wallet info
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${summary_agent_wallet_info_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Summary order balance movements
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${summary_order_balance_movements_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Get all report type
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_report_type_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Get report formula
    [Arguments]     ${headers}      ${report_type_id}
    ${report_type_id}     convert to string       ${report_type_id}
    ${path}      replace string    ${get_report_formula_path}    {report_type_id}     ${report_type_id}
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Update report formula
    [Arguments]     ${headers}      ${body}         ${report_type_id}
    ${report_type_id}     convert to string       ${report_type_id}
    ${path}      replace string    ${update_report_formula_path}    {report_type_id}     ${report_type_id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search agent belong to company
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_agent_belong_to_company_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Get all company types
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${get_all_company_types}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search user profiles
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_user_profile_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Get all agent type
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${get_all_agent_type_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Get transaction history of company
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_transactions_of_company_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search Company Transaction History By Connected Agent
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_company_transaction_agents_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search Company Transaction History By Specific Company
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_company_transaction_specific_company_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search sales permissions
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_sales_permissions_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search trust token
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_trust_token_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search settlement configurations
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${report_search_settlement_configuration_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - search upload files
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_upload_files_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - search functions
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_functions_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Export payment order
    [Arguments]     ${headers}      ${body}
    Enable rest content base 64
    REST.post       ${api_gateway_host}/${search_payment_orders_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - search api
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_apis_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"


[Report][200] - Customer search orders
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${customer_search_orders_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"


[Report][200] - Search sale hierarchy nodes
    [Arguments]     ${headers}      ${body}     ${hierarchy_id}
    ${hierarchy_id}     convert to string      ${hierarchy_id}
    ${search_sale_hierarchy_nodes_path}  replace string      ${search_sale_hierarchy_nodes_path}   {hierarchy_id}      ${hierarchy_id}    1
    REST.post       ${api_gateway_host}/${search_sale_hierarchy_nodes_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Agent search last order transaction
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${report_search_last_order_transaction_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search pending settlement
    [Arguments]     ${headers}      ${body}
     REST.post       ${api_gateway_host}/${search_pending_settlement_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - search sub balance transactions
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_sub_balance_transactions_path}
        ...     headers=${headers}
        ...     body=${body}
    REST.output
    [Common][Verify] - Http status code is "200"

[Report][200] - Search get all threshold limits
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${get_all_threshold_limits_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search sof service balance limitation
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_sof_service_balance_limitation_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search command tier mask
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${search_command_tier_mask_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search sale hierarchy node children
    [Arguments]     ${headers}      ${body}       ${node_id}
    ${node_id}     convert to string      ${node_id}
    ${search_sale_hierarchy_node_children_path}  replace string      ${search_sale_hierarchy_node_children_path}   {node_id}      ${node_id}    1
    REST.post       ${api_gateway_host}/${search_sale_hierarchy_node_children_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Search tier level masks
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${report_search_tier_level_masks_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Report][200] - Get report order detail
    [Arguments]     ${headers}      ${order_id}
    ${order_id}     convert to string      ${order_id}
    ${report_get_report_order_detail}  replace string      ${report_get_report_order_detail}   {order_id}      ${order_id}    1

    REST.get       ${api_gateway_host}/${report_get_report_order_detail}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"


