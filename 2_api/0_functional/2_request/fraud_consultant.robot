*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot

*** Keywords ***
[Fraud_Consultant][200] - create fraud ticket
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${create_fraud_ticket_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Fraud_Consultant][200] - delete fraud ticket
    [Arguments]     ${headers}      ${ticket_id}
    ${ticket_id}     convert to string       ${ticket_id}
    ${path}       replace string          ${delete_fraud_ticket_path}     {ticket_id}       ${ticket_id}
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Fraud_Consultant][200] - get all unblock ticket reasons
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_unblock_reasons_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Fraud_Consultant][200] - unblock ticket with reason
    [Arguments]     ${headers}      ${body}     ${ticket_id}
    ${ticket_id}     convert to string       ${ticket_id}
    ${path}    replace string  ${delete_ticket_with_reason_path}   {ticket_id}     ${ticket_id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"
