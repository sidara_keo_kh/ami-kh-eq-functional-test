*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot


*** Keywords ***
[Sof_Bank][200] - create bank
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${create_bank_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Sof_Bank][200] - update bank
    [Arguments]     ${headers}      ${body}     ${bank_id}
    ${bank_id}     convert to string       ${bank_id}
    ${path}      replace string    ${update_bank_path}    {bankId}   ${bank_id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Sof_Bank][200] - delete bank
    [Arguments]     ${headers}      ${bank_id}
    ${bank_id}     convert to string       ${bank_id}
    ${path}      replace string    ${delete_bank_path}    {bankId}   ${bank_id}
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"