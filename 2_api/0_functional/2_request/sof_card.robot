*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot


*** Keywords ***
[Sof_Card][200] - add card design
    [Arguments]     ${headers}      ${body}     ${provider_id}
    ${provider_id}     convert to string       ${provider_id}
    ${path}=    replace string      ${add_card_design_path}        {provider_id}        ${provider_id}      1
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Sof_Card][200] - add provider
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${create_provider_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Sof_Card][200] - get list of card types
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_list_of_card_types_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Sof_Card][200] - get details of Provider under Card SOF
    [Arguments]     ${headers}       ${provider_id}
    ${provider_id}     convert to string       ${provider_id}
    ${path}          replace string          ${get_details_of_Provider_under_Card_SOF_path}       {provider_id}       ${provider_id}
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Sof_Card][200] - edit Provider detail in Card SOF
    [Arguments]     ${headers}      ${body}         ${provider_id}
    ${provider_id}     convert to string       ${provider_id}
    ${path}         replace string          ${edit_Provider_detail_in_Card_SOF_path}      {provider_id}       ${provider_id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Sof_Card][200] - get card design detail
    [Arguments]     ${headers}       ${provider_id}     ${card_design_id}
    ${provider_id}     convert to string       ${provider_id}
    ${card_design_id}     convert to string       ${card_design_id}
    ${path}          replace string          ${get_card_design_detail_path}       {provider_id}       ${provider_id}
    ${path}          replace string          ${path}       {card-design_id}       ${card_design_id}
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Sof_Card][200] - get list of providers
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_list_of_providers_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Sof_Card][200] - get card design by PAN number
    [Arguments]     ${headers}      ${provider_id}      ${pan}
    ${provider_id}     convert to string       ${provider_id}
    ${pan}     convert to string       ${pan}
    ${path}          replace string          ${get_card_design_by_PAN_number_path}       {provider_id}       ${provider_id}
    ${path}         replace string          ${path}       {pan}       ${pan}
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Sof_Card][200] - edit card design
    [Arguments]     ${headers}      ${body}         ${provider_id}          ${card_design_id}
    ${provider_id}     convert to string       ${provider_id}
    ${card_design_id}     convert to string       ${card_design_id}
    ${path}          replace string          ${edit_card_design_path}       {provider_id}       ${provider_id}
    ${path}          replace string          ${path}       {card-design_id}       ${card_design_id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"
