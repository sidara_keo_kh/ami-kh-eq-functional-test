*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot


*** Keywords ***
[Api_Gateway][200] - Agent authentication
    [Arguments]     ${headers}      ${params}
    REST.post       ${api_gateway_host}/${agent_authentication_path}?${params}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Api_Gateway][200] - Customer authentication
    [Arguments]     ${headers}      ${params}
    REST.post       ${api_gateway_host}/${customer_authentication_path}?${params}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Api_Gateway][200] - System user authentication
    [Arguments]     ${headers}      ${params}
    REST.post       ${api_gateway_host}/${system_user_authentication_path}?${params}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Api_Gateway][200] - Get all services
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_service_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Api_Gateway][200] - Get service detail
    [Arguments]     ${headers}      ${service_id}
    ${service_id}     convert to string       ${service_id}
    ${path}     replace string          ${get_service_detail_path}       {serviceId}       ${service_id}
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Api_Gateway][200] - Create oauth client
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${create_oauth_client_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Api_Gateway][200] - Get agent payload
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_payload_agent_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Api_Gateway][200] - Get customer payload
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_customer_payload_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Api_Gateway][200] - Get all apis
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_api_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Api_Gateway][200] - Delete api
    [Arguments]     ${headers}      ${api_id}
    ${api_id}     convert to string       ${api_id}
    ${path}       replace string  ${delete_api_path}     {apiId}      ${api_id}   1
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Api_Gateway][200] - Get api detail
    [Arguments]     ${headers}      ${api_id}
    ${api_id}     convert to string       ${api_id}
    ${path}       replace string  ${get_api_detail_path}     {apiId}      ${api_id}   1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Api_Gateway][200] - Update api detail
    [Arguments]     ${headers}      ${api_id}
    ${api_id}     convert to string       ${api_id}
    ${path}       replace string  ${update_api_detail_path}     {apiId}      ${api_id}   1
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Api_Gateway][200] - Get client detail
    [Arguments]     ${headers}      ${client_id}
    ${client_id}     convert to string       ${client_id}
    ${path}       replace string  ${get_client_detail_path}     {clientId}      ${client_id}   1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Api_Gateway][200] - Get all oauth clients
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_oauth_client_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Api_Gateway][200] - Get client scope detail
    [Arguments]     ${headers}      ${client_id}
    ${client_id}     convert to string       ${client_id}
    ${path}       replace string  ${get_client_scope_detail_path}     {clientId}      ${client_id}   1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Api_Gateway][200] - Get all grant types
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_grant_types_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Api_Gateway][200] - Get api detail by name
    [Arguments]     ${headers}      ${api_name}
    ${path}       replace string  ${get_api_detail_by_name_path}     {apiName}      ${api_name}   1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Api_Gateway][200] - Create authorization code
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${create_authorization_code_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Api_Gateway][200] - Revoke token by user
    [Arguments]     ${headers}      ${body}
    REST.delete       ${api_gateway_host}/${revoke_token_by_user_id_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Api_Gateway][200] - Verify token
    [Arguments]     ${headers}      ${body}
    REST.delete       ${api_gateway_host}/${token_verification_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Api_Gateway][200] - Create service
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${api_gateway_create_service_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Api_Gateway][200] - Create api
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${add_api_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Api_Gateway][200] - Add client scope
    [Arguments]     ${headers}      ${body}     ${client_id}
    ${client_id}     convert to string       ${client_id}
    ${path}     replace string          ${api_gateway_add_scope_path}       {clientId}       ${client_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"