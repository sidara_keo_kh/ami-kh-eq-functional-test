*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot

*** Keywords ***
[Agent][200] - create agent type
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${create_agent_type_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"