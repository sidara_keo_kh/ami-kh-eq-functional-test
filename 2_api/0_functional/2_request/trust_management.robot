*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot


*** Keywords ***
[Trust_Management][200] - generate trust token
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${generate_trust_token_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Trust_Management][200] - create trust order
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${create_trus_order_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Trust_Management][200] - execute trust order
    [Arguments]     ${headers}      ${body}     ${order_id}
    ${order_id}     convert to string       ${order_id}
    ${path}          replace string          ${execute_trust_order_path}       {order_id}       ${order_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Trust_Management][400] - execute trust order
    [Arguments]     ${headers}      ${body}     ${order_id}
    ${order_id}     convert to string       ${order_id}
    ${path}          replace string          ${execute_trust_order_path}       {order_id}       ${order_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "400"

[Trust_Management][200] - delete trust token
    [Arguments]     ${headers}      ${id}
    ${id}     convert to string       ${id}
    ${path}          replace string          ${delete_trust_token_path}       {id}       ${id}
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Trust_Management][200] - trusted agent can see their token list
    [Arguments]     ${headers}      ${params}
    REST.get       ${api_gateway_host}/${trusted_agent_can_see_their_token_list_path}?${params}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Trust_Management][200] - get list trust order
    [Arguments]     ${headers}      ${trust_token}
    ${path}          replace string          ${get_list_trust_order_path}       {trust_token}       ${trust_token}
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Trust_Management][200] - get list truster sof
    [Arguments]     ${headers}      ${trust_token}
    ${path}          replace string          ${get_list_truster_sof_path}       {trust_token}       ${trust_token}
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Trust_Management][200] - get trust order detail
    [Arguments]     ${headers}      ${trust_token}          ${order_id}
    ${order_id}     convert to string       ${order_id}
    ${path}          replace string          ${get_trust_order_detail_path}       {trust_token}       ${trust_token}
    ${path}          replace string          ${path}       {order_id}       ${order_id}
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Trust_Management][200] - get truster profile
    [Arguments]     ${headers}      ${trust_token}
    ${path}          replace string          ${get_truster_profile_path}       {trust_token}       ${trust_token}
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Trust_Management][200] - system user cancel trust order
    [Arguments]     ${headers}       ${refOrderId}
    ${refOrderId}     convert to string       ${refOrderId}
    ${system_user_cancel_trust_order_path}    replace string    ${system_user_cancel_trust_order_path}    {refOrderId}   ${refOrderId}
    REST.delete       ${api_gateway_host}/${system_user_cancel_trust_order_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"