*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot


*** Keywords ***
[Sof_Cash][200] - update status cash source of fund
    [Arguments]     ${headers}      ${body}     ${sof_cash_id}
    ${sof_cash_id}     convert to string       ${sof_cash_id}
    ${path}=    replace string      ${update_status_cash_source_of_fund_path}        {sof_cash_id}       ${sof_cash_id}    1
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Sof_Cash][200] - update prefill amount cash source of fund
    [Arguments]     ${headers}      ${body}     ${sof_cash_id}
    ${sof_cash_id}     convert to string       ${sof_cash_id}
    ${path}=    replace string      ${update_prefill_amount_cash_source_of_fund_path}        {sof_cash_id}       ${sof_cash_id}    1
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Sof_Cash][200] - get sof cash detail
    [Arguments]     ${headers}         ${sof_cash_id}
    ${sof_cash_id}     convert to string       ${sof_cash_id}
    ${path}=    replace string      ${get_sof_cash_path}        {sof_cash_id}       ${sof_cash_id}    1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"