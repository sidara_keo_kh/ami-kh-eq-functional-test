*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot


*** Keywords ***
[Payment][200] - Add service group
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${add_service_group_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - Create wallet limitation
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${add_profile_wallet_limitation}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get a service group
    [Arguments]     ${headers}      ${service_group_id}
    ${service_group_id}     convert to string       ${service_group_id}
    ${path}=    replace string      ${get_a_service_group_path}        {serviceGroupId}        ${service_group_id}    1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - remove service group
    [Arguments]     ${headers}      ${service_group_id}
    ${service_group_id}     convert to string       ${service_group_id}
    ${path}=    replace string      ${remove_service_group_path}        {serviceGroupId}        ${service_group_id}    1
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get all service groups
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_service_group_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - update service group
    [Arguments]     ${headers}      ${body}         ${service_group_id}
    ${service_group_id}     convert to string       ${service_group_id}
    ${path}=    replace string      ${update_service_group_path}        {serviceGroupId}        ${service_group_id}    1
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - Create service
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${create_service_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get service details
    [Arguments]     ${headers}      ${service_id}
    ${service_id}     convert to string       ${service_id}
    ${path}=    replace string      ${get_service_details_path}        {serviceId}        ${service_id}    1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get all services
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_services_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - update service
    [Arguments]     ${headers}      ${body}         ${service_id}
    ${service_id}     convert to string       ${service_id}
    ${path}=    replace string      ${update_service_path}        {serviceId}        ${service_id}    1
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - active-suspend service
    [Arguments]     ${headers}      ${body}         ${service_id}
    ${service_id}     convert to string       ${service_id}
    ${path}=    replace string      ${update_service_status_path}        {service_id}        ${service_id}    1
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - delete service
    [Arguments]     ${headers}      ${service_id}
    ${service_id}     convert to string       ${service_id}
    ${path}=    replace string      ${delete_service_path}        {serviceId}        ${service_id}    1
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get agent type service
    [Arguments]     ${headers}      ${agent_type_id}
    ${agent_type_id}     convert to string       ${agent_type_id}
    ${path}=    replace string      ${get_agent_type_service_path}        {agent_type_id}        ${agent_type_id}    1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - create service command
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${create_service_command_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get service commands by service id
    [Arguments]     ${headers}      ${service_id}
    ${service_id}     convert to string       ${service_id}
    ${path}=    replace string      ${get_service_commands_by_service_id_path}         {service_id}       ${service_id}    1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - delete service command
    [Arguments]     ${headers}      ${service_command_id}
    ${service_command_id}     convert to string       ${service_command_id}
    ${path}=    replace string      ${remove_service_command_path}        {serviceCommandId}        ${service_command_id}    1
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - create fee tier
    [Arguments]     ${headers}      ${body}         ${service_command_id}
    ${service_command_id}     convert to string       ${service_command_id}
    ${path}=    replace string      ${create_fee_tier_path}         {service_command_id}       ${service_command_id}    1
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - update fee tier
    [Arguments]     ${headers}      ${body}         ${fee_tier_id}
    ${fee_tier_id}     convert to string       ${fee_tier_id}
    ${path}=    replace string      ${update_fee_tier_path}         {fee_tier_id}       ${fee_tier_id}    1
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get list fee tiers by service command id
    [Arguments]     ${headers}      ${service_command_id}
    ${service_command_id}     convert to string       ${service_command_id}
    ${path}=    replace string      ${get_list_fee_tiers_by_service_command_id_path}        {service_command_id}       ${service_command_id}    1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - delete fee tier
    [Arguments]     ${headers}      ${fee_tier_id}
    ${fee_tier_id}     convert to string       ${fee_tier_id}
    ${path}=    replace string      ${remove_fee_tier_path}        {fee_tier_id}        ${fee_tier_id}    1
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get list of tier from
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_tier_amount_from}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - update all banlance distribution
    [Arguments]     ${headers}      ${body}         ${fee_tier_id}
    ${fee_tier_id}     convert to string       ${fee_tier_id}
    ${path}=    replace string      ${update_all_banlance_distribution_path}        {fee_tier_id}        ${fee_tier_id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get all banlance distributions
    [Arguments]     ${headers}      ${fee_tier_id}
    ${fee_tier_id}     convert to string       ${fee_tier_id}
    ${path}=    replace string      ${get_all_balance_distribution_by_fee_tier_path}        {fee_tier_id}       ${fee_tier_id}    1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - delete balance distribution
    [Arguments]     ${headers}      ${balance_distribution_id}
    ${balance_distribution_id}     convert to string       ${balance_distribution_id}
    ${path}=    replace string      ${delete_balance_distribution_path}        {balance_distribution_id}       ${balance_distribution_id}    1
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get list fee tier conditions
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_fee_tier_condition_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get all commands
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_command_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get all action types
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_action_types_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get all amount types
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_amount_types_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get all actor types
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_actor_types_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get all sof types
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_sof_types_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get all bonus type
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_bonus_types_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get all fee type
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_fee_types_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - Search service group by name
    [Arguments]     ${headers}      ${service_group_name}
    ${path}=    replace string      ${payment_search_service_group_path}        {service_group_name}        ${service_group_name}    1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - Search service by name
    [Arguments]     ${headers}      ${service_name}
    ${path}=    replace string      ${payment_search_service_path}        {service_name}        ${service_name}    1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get all spi url types
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_spi_url_types_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get all spi url call methods
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_spi_url_call_methods_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get all payment methods
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_payment_methods_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - create SPI URL
    [Arguments]     ${headers}      ${body}         ${service_command_id}
    ${service_command_id}     convert to string       ${service_command_id}
    ${path}=    replace string      ${create_SPI_URL_path}        {service_command_id}       ${service_command_id}    1
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get SPI URL by service command
    [Arguments]     ${headers}      ${service_command_id}
    ${service_command_id}     convert to string       ${service_command_id}
    ${path}=    replace string      ${get_SPI_URL_by_service_command_path}        {service_command_id}       ${service_command_id}    1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get SPI URL by URL id
    [Arguments]     ${headers}      ${spi_url_id}
    ${spi_url_id}     convert to string       ${spi_url_id}
    ${path}=    replace string      ${get_SPI_URL_by_URL_id_path}        {spi_url_id}       ${spi_url_id}    1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - update SPI URL
    [Arguments]     ${headers}      ${body}         ${spi_url_id}
    ${spi_url_id}     convert to string       ${spi_url_id}
    ${path}=    replace string      ${update_SPI_URL_path}        {spi_url_id}       ${spi_url_id}    1
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - delete SPI URL
    [Arguments]     ${headers}      ${spi_url_id}
    ${spi_url_id}     convert to string       ${spi_url_id}
    ${path}=    replace string      ${delete_SPI_URL_path}        {spi_url_id}        ${spi_url_id}    1
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get SPI URL configuration types
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_spi_url_configuration_types_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - create SPI URL configuration
    [Arguments]     ${headers}      ${body}         ${spi_url_id}
    ${spi_url_id}     convert to string       ${spi_url_id}
    ${path}=    replace string      ${create_spi_url_configuration_path}        {spi_url_id}        ${spi_url_id}    1
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get SPI URL configuration details from ID
    [Arguments]     ${headers}      ${spi_url_id}
    ${spi_url_id}     convert to string       ${spi_url_id}
    ${path}=    replace string      ${get_spi_url_configuration_by_spi_url_path}        {spi_url_id}       ${spi_url_id}    1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - update SPI URL configuration
    [Arguments]     ${headers}      ${body}         ${spi_url_configuration_id}
    ${spi_url_configuration_id}     convert to string       ${spi_url_configuration_id}
    ${path}=    replace string      ${update_spi_url_configuration_path}        {spi_url_configuration_id}        ${spi_url_configuration_id}    1
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - delete SPI URL configuration
    [Arguments]     ${headers}      ${spi_url_configuration_id}
    ${spi_url_configuration_id}     convert to string       ${spi_url_configuration_id}
    ${path}=    replace string      ${delete_spi_url_configuration_path}        {spi_url_configuration_id}        ${spi_url_configuration_id}    1
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get all Payment security types
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_payment_security_types_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - create payment pin
    [Arguments]     ${headers}      ${body}
    REST.put       ${api_gateway_host}/${create_payment_pin_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - Get all tier levels
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_tier_level_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - update tier level
    [Arguments]     ${headers}      ${body}         ${tier_level_id}
    ${tier_level_id}     convert to string       ${tier_level_id}
    ${path}=    replace string      ${update_tier_level_label_path}        {tier_level_id}        ${tier_level_id}    1
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - Create normal order
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${payment_create_normal_order_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - Create artifact order
    [Arguments]     ${headers}=${headers}      ${body}=${body}
    REST.post       ${api_gateway_host}/${payment_create_artifact_order_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - Cancel artifact order
    [Arguments]     ${headers}      ${body}     ${ref_order_id}
    ${ref_order_id}     convert to string       ${ref_order_id}
    ${path}=    replace string      ${create_cancel_artifacts_payment_path}        {ref_order_id}       ${ref_order_id}    1
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - Cancel normal order
    [Arguments]     ${headers}      ${body}     ${ref_order_id}
    ${ref_order_id}     convert to string       ${ref_order_id}
    ${path}=    replace string      ${create_cancel_normals_payment_path}        {ref_order_id}       ${ref_order_id}    1
    create session          api     ${api_gateway_host}
    ${response}             delete request          api     ${path}       ${body}     ${NONE}       ${headers}
    should be equal as integers     ${response.status_code}     200

[Payment][200] - Admin cancel normal order
    [Arguments]     ${headers}      ${body}     ${ref_order_id}
    ${ref_order_id}     convert to string       ${ref_order_id}
    ${path}=    replace string      ${admin_create_cancel_normals_payment_path}        {ref_order_id}       ${ref_order_id}    1
    create session          api     ${api_gateway_host}
    ${response}             delete request          api     ${path}       ${body}     ${NONE}       ${headers}
    should be equal as integers     ${response.status_code}     200
    set test variable     ${test_cancel_order_id}       ${response.json()['data']['order_id']}

[Payment][200] - Create trust order
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${payment_create_trust_order_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - Get order detail
    [Arguments]     ${headers}      ${order_id}
    ${order_id}     convert to string       ${order_id}
    ${path}=    replace string      ${get_order_detail_path}        {order_id}        ${order_id}    1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - get order detail properties
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_order_detail_prop_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - Payment execute order
    [Arguments]     ${headers}       ${order_id}
    ${order_id}     convert to string       ${order_id}
    ${path}=    replace string      ${payment_execute_order_path}        {orderId}        ${order_id}    1
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][400] - Execute order
    [Arguments]     ${headers}       ${order_id}
    ${order_id}     convert to string       ${order_id}
    ${path}=    replace string      ${payment_execute_order_path}        {orderId}        ${order_id}    1
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "400"

[Payment][200] - Get order
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${user_get_all_orders_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - create user sof cash
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${create_user_sof_cash_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - user get all his/her sof cash
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${user_get_their_sof_cash_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - user get his/her sof cash by currency
    [Arguments]     ${headers}      ${currency}
    ${path}=    replace string      ${user_get_sof_cash_by_currency_path}        {currency}       ${currency}    1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - system user create user sof cash
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${admin_create_user_sof_cash_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - system user create a company agent cash source of fund with currency
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${system_user_create_a_company_agent_cash_source_of_fund_with_currency_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - system user update company agent cash balance
    [Arguments]     ${headers}      ${body}     ${currency}
    ${path}=    replace string      ${system_user_update_company_agent_cash_balance_path}        {currency}       ${currency}    1
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - system user get company agent cash balance history
    [Arguments]     ${headers}      ${currency}
    ${path}=    replace string      ${system_user_get_company_agent_cash_balance_history_path}        {currency}       ${currency}    1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - user get all his/her source of fund
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${user_get_list_sof_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - user get his/her bank source of fund
    [Arguments]     ${headers}      ${bank_sof_id}
    ${bank_sof_id}     convert to string       ${bank_sof_id}
    ${path}=    replace string      ${user_get_bank_sof_by_sof_id_path}        {bank_sof_id}       ${bank_sof_id}    1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - user link bank by bank
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${user_can_link_bank_by_bank_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - user unlink bank by bank
    [Arguments]     ${headers}      ${bank_sof_id}
    ${bank_sof_id}     convert to string       ${bank_sof_id}
    ${path}=    replace string      ${admin_unlink_bank_by_bank_path}        {bank_sof_id}        ${bank_sof_id}    1
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - user unlink bank by themselves
    [Arguments]     ${headers}      ${bank_sof_id}
    ${bank_sof_id}     convert to string       ${bank_sof_id}
    ${path}=    replace string      ${user_unlink_bank_themself_path}        {bank_sof_id}        ${bank_sof_id}    1
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - user get all customer bank source of fund
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${user_get_bank_sof_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - verify (pre-link) card information
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${verify_(pre-link)_card_information_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - user link card source of fund
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${user_link_card_source_of_fund_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - user get list of his/her linked card source of fund detail
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${user_get_all_sof_card_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - user get his/her linked card source of fund detail by sof ID
    [Arguments]     ${headers}      ${card_sof_id}
    ${card_sof_id}     convert to string       ${card_sof_id}
    ${path}=    replace string      ${user_get_sof_card_by_sof_id_path}        {card_sof_id}        ${card_sof_id}    1
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - user delete (un-link) his/her linked card source of fund
    [Arguments]     ${headers}      ${card_sof_id}
    ${card_sof_id}     convert to string       ${card_sof_id}
    ${path}=    replace string      ${user_unlink_card_by_sof_id_path}        {card_sof_id}        ${card_sof_id}    1
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - add company balance
    [Arguments]     ${headers}      ${body}         ${currency}
    ${path}=    replace string      ${system_user_update_company_agent_cash_balance_path}   {currency}   ${currency}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - apply discount code
    [Arguments]     ${headers}      ${body}         ${order_id}
    ${order_id}     convert to string       ${order_id}
    ${path}=    replace string    ${apply_discount_code_path}    {order_id}   ${order_id}    1
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - Execute order with security
    [Arguments]     ${headers}      ${body}         ${order_id}
    ${order_id}     convert to string       ${order_id}
    ${path}=    replace string      ${get_order_detail_path}    {order_id}    ${order_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - Create sof service balance limitation
    [Arguments]     ${headers}      ${body}     ${sof_cash_id}        ${service_id}
    ${sof_cash_id}     convert to string       ${sof_cash_id}
    ${service_id}     convert to string       ${service_id}
    ${path}    replace string    ${create_sof_service_balance_limitation_path}    {sof_id}   ${sof_cash_id}
    ${path}    replace string    ${path}    {service_id}   ${service_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][400] - Create sof service balance limitation
    [Arguments]     ${headers}      ${body}     ${sof_cash_id}        ${service_id}
    ${sof_cash_id}     convert to string       ${sof_cash_id}
    ${service_id}     convert to string       ${service_id}
    ${path}    replace string    ${create_sof_service_balance_limitation_path}    {sof_id}   ${sof_cash_id}
    ${path}    replace string    ${path}    {service_id}   ${service_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "400"

[Payment][200] - get tier config allow zero
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_tier_cofig_allow_zero}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - delete sof service balance limitation
    [Arguments]     ${headers}
    REST.delete       ${api_gateway_host}/${delete_sof_service_balance_limitation_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - apply tier level mask to an order
    [Arguments]     ${headers}      ${body}     ${order_id}
    ${order_id}     convert to string       ${order_id}
    ${path}    replace string    ${apply_tier_level_mask_to_an_order_path}    {order_id}   ${order_id}    1
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - create service limitation
    [Arguments]     ${headers}      ${body}     ${service_id}
    ${service_id}     convert to string       ${service_id}
    ${path}    replace string      ${create_service_limitation_path}        {service_id}        ${service_id}    1
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - delete service limitation
    [Arguments]     ${headers}      ${service_limitation_id}
    ${service_limitation_id}     convert to string       ${service_limitation_id}
    ${path}    replace string      ${delete_service_limitation_path}        {service_limitation_id}        ${service_limitation_id}    1
    REST.delete       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - create tier mask for service
    [Arguments]     ${headers}      ${body}     ${service_id}
    ${service_id}     convert to string       ${service_id}
    ${path}    replace string    ${create_service_tier_mask_path}    {service_id}   ${service_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - create command tier mask
    [Arguments]     ${headers}      ${body}     ${service_command_id}
    ${service_command_id}     convert to string       ${service_command_id}
    ${path}    replace string    ${create_command_tier_mask_path}    {service_command_id}   ${service_command_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - update command tier mask
    [Arguments]     ${headers}      ${body}     ${service_command_id}       ${tier_mask_id}
    ${service_command_id}     convert to string       ${service_command_id}
    ${path}    replace string    ${update_command_tier_mask_path}    {service_command_id}   ${service_command_id}
    ${tier_mask_id}     convert to string       ${tier_mask_id}
    ${path}    replace string    ${path}    {tier_mask_id}   ${tier_mask_id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - create service fee tier mask
    [Arguments]     ${headers}      ${body}     ${service_id}        ${service_tier_mask_id}
    ${service_id}     convert to string       ${service_id}
    ${service_tier_mask_id}     convert to string       ${service_tier_mask_id}
    ${path}    replace string    ${payment_create_service_fee_tier_mask_path}    {service_id}   ${service_id}
    ${path}    replace string    ${path}    {service_tier_mask_id}   ${service_tier_mask_id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - update sof service balance limitation
    [Arguments]     ${headers}      ${body}
    REST.put       ${api_gateway_host}/${update_sof_service_balance_limitation_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - System user update sof cash threshold limits
    [Arguments]     ${headers}      ${body}      ${id}
    ${sof_cash_id}     convert to string       ${id}
    ${path}    replace string    ${system_user_update_sof_cash_threshold_limits_path}    {id}   ${id}
    REST.post       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - Update sof service balance limitation with the id parameter
    [Arguments]     ${headers}      ${body}      ${id}
    ${id}     convert to string       ${id}
    ${path}    replace string    ${update_sof_service_balance_limitation_path}    {id}   ${id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - system user cancel normal order
    [Arguments]     ${headers}      ${refOrderId}
    ${refOrderId}     convert to string       ${refOrderId}
    ${path}    replace string    ${system_user_cancel_normal_order}    {refOrderId}   ${refOrderId}
    REST.delete       ${api_gateway_host}/${system_user_cancel_normal_order}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Payment][200] - system user cancel artifact order
    [Arguments]     ${headers}       ${refOrderId}
    ${refOrderId}     convert to string       ${refOrderId}
    ${path}    replace string    ${system_user_cancel_artifact_order}    {refOrderId}   ${refOrderId}
    REST.delete       ${api_gateway_host}/${system_user_cancel_artifact_order}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"


[Payment][400] - Update sof service balance limitation
    [Arguments]     ${headers}      ${body}     ${id}
    ${id}     convert to string       ${id}
    ${path}    replace string    ${update_sof_service_balance_limitation_path}    {id}   ${id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "400"