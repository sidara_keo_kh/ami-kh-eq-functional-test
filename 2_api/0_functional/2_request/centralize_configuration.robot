*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot


*** Keywords ***
[Centralize_Configuration][200] - update configuration detail
    [Arguments]     ${headers}      ${body}         ${scopeName}         ${key}
    ${path}    replace string  ${update_configuration_detail_path}    {scopeName}     ${scopeName}
    ${path}    replace string  ${path}    {key}     ${key}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Centralize_Configuration][200] - get configuration detail
    [Arguments]     ${headers}      ${scopeName}         ${key}
    ${path}    replace string  ${get_configuration_detail_by_key_path}    {scopeName}     ${scopeName}
    ${path}    replace string  ${path}    {key}     ${key}
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Centralize_Configuration][200] - get all configurations
    [Arguments]     ${headers}      ${scopeName}
    ${path}    replace string  ${get_all_configurations_path}    {scopeName}     ${scopeName}
    REST.get       ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Centralize_Configuration][200] - get all pre-load countries
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_pre-load_countries_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Centralize_Configuration][200] - get_all_pre-load_currencies
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_pre-load_currencies_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Centralize_Configuration][200] - get all configuration scope
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_all_configuration_socpe_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"