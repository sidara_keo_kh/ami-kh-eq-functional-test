*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot


*** Keywords ***
[Voucher][200] - get list voucher groups
    [Arguments]     ${headers}
    REST.get       ${api_gateway_host}/${get_list_voucher_groups_path}
        ...     headers=${headers}
    rest extract
    [Common][Verify] - Http status code is "200"

[Voucher][200] - update voucher status
    [Arguments]     ${headers}      ${body}     ${id}
    ${id}     convert to string       ${id}
    ${path}=    replace string        ${update_voucher_status_path}       {id}         ${id}
    REST.put       ${api_gateway_host}/${path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Voucher][200] - create multiple discount vouchers
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${create_multiple_discount_vouchers_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[Voucher][200] - create multiple-use vouchers
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${create_multiple-use_vouchers_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"