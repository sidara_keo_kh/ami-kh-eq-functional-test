*** Settings ***
Resource        ../../../1_common/keywords.robot
Resource        ../3_post_process/common.robot


*** Keywords ***
[OTP_Management][200] - generate OTP
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${generate_otp_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[OTP_Management][200] - validate OTP
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${validate_otp_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"

[OTP_Management][200] - regenerate OTP
    [Arguments]     ${headers}      ${body}
    REST.post       ${api_gateway_host}/${regenerate_otp_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"