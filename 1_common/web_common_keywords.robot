*** Settings ***
Resource        ../0_resources/imports.robot

*** Keywords ***
[Common][Web] - Verify row is visible by data row
    [Arguments]  ${table_id}    @{row_data}
    ${each_data_xpath}=  set variable   @{EMPTY}
    :FOR    ${each_data}    in  @{row_data}
    \   ${status}= 	  run keyword and return status     should contain      ${each_data}  -
    \   ${each_data_xpath}=     Run Keyword If      ${status}==True     set variable    ${each_data_xpath}td[contains(text(),'${each_data}')] and${SPACE}
    \   ...      ELSE        set variable    ${each_data_xpath}td[text()='${each_data}'] and${SPACE}
    ${table_id1}=     replace string      ${table_id}      =     ='
    ${table_id2}=     replace string using regexp         ${table_id1}    $     '
    ${xpath}=   set variable    xpath=//table[@${table_id2}]//tr[${each_data_xpath}]
    ${xpath_processed}=   replace string      ${xpath}        ${SPACE}and${SPACE}]       ]
    wait until page contains element  ${xpath_processed}    60

[Common][Web] - Verify row is not visible by data row
    [Arguments]  ${table_id}    @{row_data}
    ${each_data_xpath}=  set variable   ${EMPTY}
    :FOR    ${each_data}    in  @{row_data}
    \   ${status}= 	  run keyword and return status     should contain      ${each_data}  -
    \   ${each_data_xpath}=     Run Keyword If      ${status}==True     set variable    ${each_data_xpath}td[contains(text(),"${each_data}")] and${space}
    \   ...      ELSE        set variable    ${each_data_xpath}td[contains(text(),"${each_data}")] and${SPACE}
    ${table_id1}=     replace string     ${table_id}      =     ="
    ${table_id2}=     replace string using regexp     ${table_id1}    $     "
    ${xpath}=   set variable    xpath=//table[@${table_id2}]//tr[${each_data_xpath}]
    ${xpath_processed}=   replace string      ${xpath}    ${space}and${space}]    ]
    wait until page does not contain element  ${xpath_processed}    60