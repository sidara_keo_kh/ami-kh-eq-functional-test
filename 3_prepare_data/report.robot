*** Settings ***
Resource         ../2_api/3_reuse_function/imports.robot
Resource         ../3_prepare_data/imports.robot

*** Keywords ***
[Report][200][Prepare] - Update agent commission report formula with effective time is today
    [Arguments]     ${commission}=default       ${time}=00:00:00        ${headers}=${SUITE_ADMIN_HEADERS}
    ${timestamp}        Get Current Date    result_format=%Y-%m-%dT${time}Z
    [Report][Prepare] - update report formula - body
        ...     $.effective_timestamp=${timestamp}
        ...     $.tpv=${None}
        ...     $.fee=${None}
        ...     $.commission=${commission}
    [Report][200] - Update report formula
        ...     headers=${headers}
        ...     body=${body}
        ...     report_type_id=2

[Report][200][Prepare] - Update agent transaction report formula with effective time is today
    [Arguments]     ${tpv}=default      ${fee}=default      ${commission}=default       ${time}=00:00:00        ${headers}=${SUITE_ADMIN_HEADERS}
    ${timestamp}        Get Current Date    result_format=%Y-%m-%dT${time}Z
    [Report][Prepare] - update report formula - body
        ...     $.effective_timestamp=${timestamp}
        ...     $.tpv=${tpv}
        ...     $.fee=${fee}
        ...     $.commission=${commission}
    [Report][200] - Update report formula
        ...     headers=${headers}
        ...     body=${body}
        ...     report_type_id=1

[Report][200][Prepare] - Add service ids to report service whitelist
    [Arguments]     ${report_type_id}       ${service_ids}      ${headers}=${SUITE_ADMIN_HEADERS}
    [Report][Prepare] - Add service to report service whitelist - body
        ...     $.service_ids=[${service_ids}]
    [Report][200] - Add service to report service whitelist
        ...     headers=${headers}
        ...     body=${body}
        ...     report_type_id=${report_type_id}

[Report][200][Prepare] - Remove service ids to report service whitelist
    [Arguments]     ${report_type_id}       ${service_ids}      ${headers}=${SUITE_ADMIN_HEADERS}
    [Report][Prepare] - remove service from report service whitelist - body
        ...     $.service_ids=[${service_ids}]
    [Report][200] - Remove service from report service whitelist
        ...     headers=${headers}
        ...     body=${body}
        ...     report_type_id=${report_type_id}

[Report][200][Prepare] - Search sale hierarchy nodes
    [Arguments]     &{arg_dic}
    [Common][Prepare] - Access token headers
        ...     access_token=${arg_dic.access_token}
        ...     output=headers
    [Report][Prepare] - Search sale hierarchy nodes - body   output=body  &{arg_dic}
    [Report][200] - Search sale hierarchy nodes
        ...     headers=${headers}
        ...     body=${body}
        ...     hierarchy_id=${arg_dic.hierarchy_id}

[Report][200][Prepare] - Agent search last order transaction
    [Arguments]     ${headers}=${SUITE_COMPANY_AGENT_HEADERS}   ${output}=test_order_detail  &{arg_dic}
    [Report][Prepare] - Agent search last order transaction - body
        ...     &{arg_dic}
    [Report][200] - Agent search last order transaction
        ...     headers=${headers}
        ...     body=${body}
    ${data}   rest_util.rest_extract     $.data
    [Common] - Set variable       name=${output}      value=${data}

[Report][200][Prepare] - Search sof service balance limitation
    [Arguments]     ${headers}=${SUITE_ADMIN_HEADERS}   &{arg_dic}
    [Report][Prepare] - Search sof service balance limitation - body
    ...     &{arg_dic}
    [Report][200] - Search sof service balance limitation
        ...     headers=${headers}
        ...     body=${body}

[Report][200][Prepare] - Get order detail
    [Arguments]   &{arg_dic}
    [Common][Prepare] - Access token headers
        ...     access_token=${arg_dic.access_token}
        ...     output=headers
    [Report][200] - Get payment order detail
        ...     headers=${headers}
        ...     order_id=${arg_dic.order_id}
