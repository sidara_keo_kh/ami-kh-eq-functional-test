*** Settings ***
Resource         ../2_api/3_reuse_function/imports.robot

*** Keywords ***
[Channel_Gateway][200][Prepare] - Create service
    [Arguments]
        ...     ${headers}=${SUITE_ADMIN_HEADERS}
        ...     ${output_id}=test_service_id
        ...     &{arg_dic}
    [Channel_Gateway][Prepare] - add service - body
        ...     &{arg_dic}
    [Channel_Gateway][200] - add service
        ...     headers=${headers}
        ...     body=${body}
    [Common][Extract] - ID
        ...     output=${output_id}

[Channel_Gateway][200][Prepare] - Create api
    [Arguments]
        ...     ${headers}=${SUITE_ADMIN_HEADERS}
        ...     ${output_id}=test_api_id
        ...     &{arg_dic}
    [Channel_Gateway][Prepare] - add api - body
        ...     &{arg_dic}
    [Channel_Gateway][200] - add api
        ...     headers=${headers}
        ...     body=${body}
    [Common][Extract] - ID
        ...     output=${output_id}