*** Settings ***
Resource          ../3_prepare_data/payment.robot
Resource          ../3_prepare_data/agent.robot
Resource          ../3_prepare_data/customer.robot
Resource          ../3_prepare_data/api_gateway.robot
Resource          ../3_prepare_data/channel_gateway.robot
Resource          ../3_prepare_data/channel_adapter.robot
Resource          ../3_prepare_data/system_user.robot
Resource          ../3_prepare_data/report.robot
Resource          ../3_prepare_data/customer.robot
Resource          ../3_prepare_data/sof_bank.robot
Resource          ../3_prepare_data/workflow.robot
Resource          ../3_prepare_data/payroll.robot
Resource          ../3_prepare_data/trust_management.robot