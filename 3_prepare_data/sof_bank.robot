*** Settings ***
Resource         ../2_api/3_reuse_function/imports.robot
Resource         ../3_prepare_data/imports.robot

*** Keywords ***
[Sof_bank][200][Prepare] - Create bank
    [Arguments]     ${headers}=${SUITE_ADMIN_HEADERS}  ${output}=test_bank_id     &{arg_dic}
    [Sof_Bank][Prepare] - create bank - body
        ...     &{arg_dic}
    [Sof_Bank][200] - create bank
        ...     headers=${headers}
        ...     body=${body}
    [Common][Extract] - ID
        ...     output=${output}