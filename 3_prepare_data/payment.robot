*** Settings ***
Resource         ../2_api/3_reuse_function/imports.robot
Resource         ../3_prepare_data/imports.robot

*** Variables ***
${VAR_DEFAULT_BALANCE_DISTRIBUTIONS}        {"action_type":"debit","actor_type":"Payer","sof_type_id":2,"specific_sof":null,"amount_type":"Amount","rate":null},
...                                         {"action_type":"credit","actor_type":"Payee","sof_type_id":2,"specific_sof":null,"amount_type":"Amount","rate":null},
...                                         {"action_type":"debit","actor_type":"Payer","sof_type_id":2,"specific_sof":null,"amount_type":"Fee","rate":null},
...                                         {"action_type":"credit","actor_type":"Payer","sof_type_id":2,"specific_sof":null,"amount_type":"Fee Rate","rate":50}

*** Keywords ***
[Payment][200][Prepare] - Create beneficiary service structure
   [Arguments]   &{arg_dic}
   [Common] - Set default value for keyword in dictionary     ${arg_dic}      debit_actor_type      Payer
   [Common] - Set default value for keyword in dictionary     ${arg_dic}      credit_actor_type      Payee
   [Common] - Set default value for keyword in dictionary     ${arg_dic}      debit_sof_type_id      null
   [Common] - Set default value for keyword in dictionary     ${arg_dic}      debit_specific_sof      null
   [Common] - Set default value for keyword in dictionary     ${arg_dic}      debit_specific_actor_id      null
   ${text}=   generate random string    10      [LETTERS]
   ${number}=   generate random string    1      123
   ${number1}=   generate random string    3      123456789
   ${service_group_name}      set variable   	${text}
   ${service_name}      set variable   	${text}
   ${currency}      set variable   	${arg_dic.currency}
   ${command_id}     set variable    1
   ${fee_tier_condition}     set variable    unlimit
   ${condition_amount}     set variable    null
   ${fee_type}     set variable    % rate
   ${fee_amount}     set variable    10
   ${bonus_type}     set variable    NON
   ${bonus_amount}     set variable    null
   ${amount_type}     set variable    null
   ${settlement_type}     set variable    Amount

    ${debit}  set variable    {"action_type":"debit","actor_type":"${arg_dic.debit_actor_type}","sof_type_id":${arg_dic.debit_sof_type_id},"specific_sof":${arg_dic.debit_specific_sof},"specific_actor_id": ${arg_dic.debit_specific_actor_id},"amount_type":"Amount","rate":null}
    ${credit}  set variable    {"action_type":"credit","actor_type":"${arg_dic.credit_actor_type}","sof_type_id":null,"specific_sof":null,"specific_actor_id": null,"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${debit}  ${credit}

    ${inputted_dic}      create dictionary   access_token=${arg_dic.access_token}    service_group_name=${service_group_name}
    ...     service_name=${service_name}    currency=${currency}    command_id=${command_id}    fee_tier_condition=${fee_tier_condition}
    ...     condition_amount=${condition_amount}        fee_type=${fee_type}    fee_amount=${fee_amount}
    ...     bonus_type=${bonus_type}        bonus_amount=${bonus_amount}    amount_type=${amount_type}      settlement_type=${settlement_type}
    ...     balance_distributions=${balance_distributions}

   ${return_dic}   [Payment][Reuse][200] - create service structure       ${inputted_dic}

    [Common] - Set variable     name=${arg_dic.output_service_beneficiary_id}       value=${return_dic.service_id}
    [Common] - Set variable     name=${arg_dic.output_service_beneficiary_group_id}       value=${return_dic.service_group_id}

[Agent][200][Prepare] - Create agent with currency
    [Arguments]     &{arg_dic}
    ${text}=   generate random string    10      [LETTERS]
    ${current_address_postal_code}   generate random string    10      123456789
    ${agent_type_name}      set variable   	${text}
    ${agent_type_id}      set variable   	${setup_company_agent_type_id}
    ${agent_unique_reference}      set variable   	${text}
    ${agent_username}     set variable    ${text}
    ${agent_password}      set variable   	${setup_agent_password_encrypted}
    ${agent_password_login}     set variable    ${setup_agent_password_encrypted_utf8}

    ${created_dic}      create dictionary   access_token=${arg_dic.access_token}      agent_type_name=${agent_type_name}
    ...     unique_reference=${agent_unique_reference}      identity_identity_type_id=1
    ...     agent_username=${agent_username}      agent_password=${agent_password}
    ...     agent_password_login=${agent_password_login}      currency=${arg_dic.currency}
    ...     user_type_id=2      current_address_postal_code=${current_address_postal_code}

    ${return_dic}  [Agent][Reuse][200] - create agent      ${created_dic}

    ${currency_list}     split string    ${arg_dic.currency}     ,
    ${currency_list_length}   get length      ${currency_list}
    :FOR     ${index}   IN RANGE    0   ${currency_list_length}
    \   ${currency}     get from list     ${currency_list}      ${index}
    \   [Common] - Set variable     name=${arg_dic.output_sof_cash_id}_${currency}     value=${return_dic.sof_cash_id_in_${currency}}

    [Common] - Set variable     name=${arg_dic.output_agent_id}     value=${return_dic.agent_id}
    [Common] - Set variable     name=${arg_dic.output_agent_type}     value=agent

[Payment][200][Prepare] - Company agent fund in
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      product_service_id      ${suite_service_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      product_service_name      ${suite_service_name}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      payer_user_ref_value    ${suite_company_agent_access_token}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      payee_user_id    ${suite_water_agent_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      payee_user_type    ${suite_water_agent_type}
    ${text}=     generate random string      15      [LETTERS]
    ${created_dic}      create dictionary   access_token=${arg_dic.access_token}    ext_transaction_id=${text}
    ...     product_service_id=${arg_dic.product_service_id}      product_service_name=${arg_dic.product_service_name}      product_name=${text}
    ...     product_ref_1=${text}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}     product_ref_3=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${arg_dic.payer_user_ref_value}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${arg_dic.payee_user_id}       payee_user_type=${arg_dic.payee_user_type}
    ...     amount=${arg_dic.amount}
    [Payment][Reuse][200] - create and execute normal order    ${created_dic}

[Agent][200][Prepare] - Create agent without sof
    [Arguments]     &{arg_dic}
    ${text}=   generate random string    10      [LETTERS]
    ${current_address_postal_code}   generate random string    10      123456789
    ${agent_type_name}      set variable   	${text}
    ${agent_type_id}      set variable   	${setup_company_agent_type_id}
    ${agent_unique_reference}      set variable   	${text}
    ${agent_username}     set variable    ${text}
    ${agent_password}      set variable   	${setup_agent_password_encrypted}
    ${agent_password_login}     set variable    ${setup_agent_password_encrypted_utf8}

    ${created_dic}      create dictionary   access_token=${arg_dic.access_token}      agent_type_name=${agent_type_name}
    ...     unique_reference=${agent_unique_reference}      identity_identity_type_id=1
    ...     agent_username=${agent_username}      agent_password=${agent_password}
    ...     agent_password_login=${agent_password_login}
    ...     user_type_id=2

    ${return_dic}  [Agent][Reuse][200] - create agent without sof      ${created_dic}

    [Common] - Set variable     name=${arg_dic.output_agent_id}     value=${return_dic.agent_id}
    [Common] - Set variable     name=${arg_dic.output_agent_type}     value=agent

[Payment][200][Prepare] - System user create settlement configuration
    [Arguments]     &{arg_dic}
    ${return_dic}  [200] API system user create settlement configuration     &{arg_dic}
    [Common] - Set variable     name=${arg_dic.output_settlement_configuration_id}    value=${return_dic.id}


[Payment][200][Prepare] - Create service group
    [Arguments]
        ...     ${headers}=${SUITE_ADMIN_HEADERS}
        ...     ${output_id}=test_service_group_id
        ...     ${output_name}=test_service_group_name
    ${name}      generate random string      10    [LETTERS]
    [Common] - Set variable     name=${output_name}     value=service_group_${name}
    ${service_group_description}      generate random string      30        prepare_[LETTERS]
    ${service_group_display_name}       generate random string      20      prepare_[LETTERS]
    ${service_group_display_name_local}     generate random string   20      prepare_[LETTERS]
    ${image_url}    generate random string  20      prepare_[LETTERS]
    [Payment][Prepare] - Create Service Group - body
        ...     service_group_name=${${output_name}}
        ...     description=${service_group_description}
        ...     image_url=${image_url}
        ...     display_name=${service_group_display_name}
        ...     display_name_local=${service_group_display_name_local}
    [Payment][200] - Add service group
        ...     headers=${headers}
        ...     body=${body}
    [Payment][Extract] Create service group - service group id
        ...     output=${output_id}

[Payment][200][Prepare] - Create service by service group id
    [Arguments]
        ...     ${service_group_id}
        ...     ${currency}=USD
        ...     ${headers}=${SUITE_ADMIN_HEADERS}
        ...     ${output_id}=test_service_id
        ...     ${output_name}=test_service_name
    ${name}      generate random string      20   [LETTERS]
    [Common] - Set variable     name=${output_name}         value=prepare_${name}
    ${description}      set variable    prepare_description_${name}
    [Payment][Prepare] - Create service - body
    ...   $.service_group_id=${service_group_id}
    ...   $.service_name=${${output_name}}
    ...   $.currency=${currency}
    ...   $.description=${description}
    [Payment][200] - Create service
        ...     headers=${headers}
        ...     body=${body}
    [Payment][Extract] Create service - service id
        ...     output=${output_id}

[Payment][200][Prepare] - Create service command by service id
    [Arguments]
        ...     ${service_id}
        ...     ${headers}=${SUITE_ADMIN_HEADERS}
        ...     ${output_id}=test_service_command_id
    [Payment][Prepare] - Create service command - body
        ...     service_id=${service_id}
        ...     command_id=1
    [Payment][200] - Create service command
        ...     headers=${headers}
        ...     body=${body}
    [Payment][200] - get service commands by service id
        ...     headers=${headers}
        ...     service_id=${service_id}
    ${service_command_id}       rest extract     $.data[0].service_command_id
    [Common] - Set variable     name=${output_id}       value=${service_command_id}

[Payment][200][Prepare] - Create cancel service command by service id
    [Arguments]
        ...     ${service_id}
        ...     ${headers}=${SUITE_ADMIN_HEADERS}
        ...     ${output_id}=test_service_command_id
    [Payment][Prepare] - Create service command - body
        ...     service_id=${service_id}
        ...     command_id=2
    [Payment][200] - Create service command
        ...     headers=${headers}
        ...     body=${body}
    [Payment][200] - get service commands by service id
        ...     headers=${headers}
        ...     service_id=${service_id}
    ${service_command_id}       rest extract     $.data[1].service_command_id
    [Common] - Set variable     name=${output_id}       value=${service_command_id}

[Payment][200][Prepare] - Create fee tier unlimited by service command id
    [Arguments]
        ...     ${service_command_id}
        ...     ${headers}=${SUITE_ADMIN_HEADERS}
        ...     ${list_fee_tier_data}=${empty}
        ...     ${output_id}=test_fee_tier_id
    [Payment][Prepare] - Create fee tier - body
        ...     fee_tier_condition=unlimit
        ...     condition_amount=${param_is_null}
        ...     fee_type=% rate
        ...     fee_amount=10
        ...     bonus_type=NON
        ...     bonus_amount=${param_is_null}
        ...     amount_type=${param_is_null}
        ...     settlement_type=Amount
        ...     list_fee_tier_data=${list_fee_tier_data}
    [Payment][200] - Create fee tier
        ...     headers=${headers}
        ...     body=${body}
        ...     service_command_id=${service_command_id}
    [Payment][200] - get list fee tiers by service command id
        ...     headers=${headers}
        ...     service_command_id=${service_command_id}
    ${fee_tier_id}       rest extract     $.data[0].fee_tier_id
    [Common] - Set variable     name=${output_id}       value=${fee_tier_id}

[Payment][200][Prepare] - Create fee tier unlimited by service command id for agent commissions
    [Arguments]
        ...     ${service_command_id}
        ...     ${headers}=${SUITE_ADMIN_HEADERS}
        ...     ${output_id}=test_fee_tier_id
    ${b}  	set variable    "b":{"type_first":"% rate","from_first":"Amount","amount_first":5.12}
    [Payment][Prepare] - Create fee tier - body
        ...     fee_tier_condition=unlimit
        ...     condition_amount=${param_is_null}
        ...     fee_type=% rate
        ...     fee_amount=10
        ...     bonus_type=NON
        ...     bonus_amount=${param_is_null}
        ...     amount_type=${param_is_null}
        ...     settlement_type=Amount
        ...     list_fee_tier_data=[${b}]
    [Payment][200] - Create fee tier
        ...     headers=${headers}
        ...     body=${body}
        ...     service_command_id=${service_command_id}
    [Payment][200] - get list fee tiers by service command id
        ...     headers=${headers}
        ...     service_command_id=${service_command_id}
    ${fee_tier_id}       rest extract     $.data[0].fee_tier_id
    [Common] - Set variable     name=${output_id}       value=${fee_tier_id}

[Payment][200][Prepare] - Create balance distribution by fee tier id
    [Arguments]
        ...     ${fee_tier_id}
        ...     ${headers}=${SUITE_ADMIN_HEADERS}
        ...     ${balance_distributions}=${VAR_DEFAULT_BALANCE_DISTRIBUTIONS}
    [Payment][Prepare] - update all banlance distribution - body
        ...     balance_distributions=[${balance_distributions}]
    [Payment][200] - update all banlance distribution
        ...     headers=${headers}
        ...     body=${body}
        ...     fee_tier_id=${fee_tier_id}

[Payment][200][Prepare] - Create service structure
    [Arguments]
        ...     ${currency}=USD
        ...     ${headers}=${SUITE_ADMIN_HEADERS}
        ...     ${list_fee_tier_data}=${empty}
        ...     ${balance_distributions}=${VAR_DEFAULT_BALANCE_DISTRIBUTIONS}
        ...     ${output_service_group_id}=suite_fund_in_service_group_id
        ...     ${output_service_group_name}=suite_fund_in_service_group_name
        ...     ${output_service_id}=suite_fund_in_service_id
        ...     ${output_service_name}=suite_fund_in_service_name
        ...     ${output_service_command_id}=suite_fund_in_service_command_id
        ...     ${output_fee_tier_id}=suite_fund_in_fee_tier_id
    [Payment][200][Prepare] - Create service group
        ...     headers=${headers}
        ...     output_id=${output_service_group_id}
        ...     output_name=${output_service_group_name}
    [Payment][200][Prepare] - Create service by service group id
        ...     service_group_id=${${output_service_group_id}}
        ...     currency=${currency}
        ...     headers=${headers}
        ...     output_id=${output_service_id}
        ...     output_name=${output_service_name}
    [Payment][200][Prepare] - Create service command by service id
        ...     service_id=${${output_service_id}}
        ...     headers=${headers}
        ...     output_id=${output_service_command_id}
    [Payment][200][Prepare] - Create fee tier unlimited by service command id
        ...     service_command_id=${${output_service_command_id}}
        ...     headers=${headers}
        ...     list_fee_tier_data=${list_fee_tier_data}
        ...     output_id=${output_fee_tier_id}
    [Payment][200][Prepare] - Create balance distribution by fee tier id
        ...     fee_tier_id=${${output_fee_tier_id}}
        ...     balance_distributions=${balance_distributions}

[Payment][200][Prepare] - Create service structure for cancellation
    [Arguments]
        ...     ${currency}=USD
        ...     ${headers}=${SUITE_ADMIN_HEADERS}
        ...     ${list_fee_tier_data}=${empty}
        ...     ${balance_distributions}=${VAR_DEFAULT_BALANCE_DISTRIBUTIONS}
        ...     ${output_service_group_id}=suite_fund_in_service_group_id
        ...     ${output_service_group_name}=suite_fund_in_service_group_name
        ...     ${output_service_id}=suite_fund_in_service_id
        ...     ${output_service_name}=suite_fund_in_service_name
        ...     ${output_service_command_id}=suite_fund_in_service_command_id
        ...     ${output_cancel_service_command_id}=suite_fund_in_cancel_service_command_id
        ...     ${output_fee_tier_id}=suite_fund_in_fee_tier_id
        ...     ${output_cancel_fee_tier_id}=suite_cancel_fee_tier_id
    [Payment][200][Prepare] - Create service group
        ...     headers=${headers}
        ...     output_id=${output_service_group_id}
        ...     output_name=${output_service_group_name}
    [Payment][200][Prepare] - Create service by service group id
        ...     service_group_id=${${output_service_group_id}}
        ...     currency=${currency}
        ...     headers=${headers}
        ...     output_id=${output_service_id}
        ...     output_name=${output_service_name}
    [Payment][200][Prepare] - Create service command by service id
        ...     service_id=${${output_service_id}}
        ...     headers=${headers}
        ...     output_id=${output_service_command_id}
    [Payment][200][Prepare] - Create cancel service command by service id
        ...     service_id=${${output_service_id}}
        ...     headers=${headers}
        ...     output_id=${output_cancel_service_command_id}
    [Payment][200][Prepare] - Create fee tier unlimited by service command id
        ...     service_command_id=${${output_service_command_id}}
        ...     headers=${headers}
        ...     list_fee_tier_data=${list_fee_tier_data}
        ...     output_id=${output_fee_tier_id}
    [Payment][200][Prepare] - Create balance distribution by fee tier id
        ...     fee_tier_id=${${output_fee_tier_id}}
        ...     balance_distributions=${balance_distributions}

    [Payment][200][Prepare] - Create fee tier unlimited by service command id
        ...     service_command_id=${${output_cancel_service_command_id}}
        ...     headers=${headers}
        ...     list_fee_tier_data=${list_fee_tier_data}
        ...     output_id=${output_cancel_fee_tier_id}
    [Payment][200][Prepare] - Create balance distribution by fee tier id
        ...     fee_tier_id=${${output_cancel_fee_tier_id}}
        ...     balance_distributions=${balance_distributions}

[Payment][200][Prepare] - Create service structure for agent commission
    [Arguments]
        ...     ${currency}=USD
        ...     ${headers}=${SUITE_ADMIN_HEADERS}
        ...     ${output_service_group_id}=suite_fund_in_service_group_id
        ...     ${output_service_group_name}=suite_fund_in_service_group_name
        ...     ${output_service_id}=suite_fund_in_service_id
        ...     ${output_service_name}=suite_fund_in_service_name
        ...     ${output_service_command_id}=suite_fund_in_service_command_id
        ...     ${output_fee_tier_id}=suite_fund_in_fee_tier_id
    [Payment][200][Prepare] - Create service group
        ...     headers=${headers}
        ...     output_id=${output_service_group_id}
        ...     output_name=${output_service_group_name}
    [Payment][200][Prepare] - Create service by service group id
        ...     service_group_id=${${output_service_group_id}}
        ...     currency=${currency}
        ...     headers=${headers}
        ...     output_id=${output_service_id}
        ...     output_name=${output_service_name}
    [Payment][200][Prepare] - Create service command by service id
        ...     service_id=${${output_service_id}}
        ...     headers=${headers}
        ...     output_id=${output_service_command_id}
    [Payment][200][Prepare] - Create fee tier unlimited by service command id for agent commissions
        ...     service_command_id=${${output_service_command_id}}
        ...     headers=${headers}
        ...     output_id=${output_fee_tier_id}
    [Payment][200][Prepare] - Create balance distribution by fee tier id
        ...     fee_tier_id=${${output_fee_tier_id}}

[Payment][200][Prepare] - Get order detail
    [Arguments]   &{arg_dic}
    [Common][Prepare] - Access token headers
        ...     access_token=${arg_dic.access_token}
        ...     output=headers
    [Payment][200] - Get order detail
        ...     headers=${headers}
        ...     order_id=${arg_dic.order_id}

[Payment][200] - Create normal orders
    [Arguments]        ${output_id}=test_order_id      ${headers}=${headers}      ${body}=${body}
    REST.post       ${api_gateway_host}/${payment_create_normal_order_path}
        ...     headers=${headers}
        ...     body=${body}
    rest extract
    [Common][Verify] - Http status code is "200"
    ${order_id}       rest extract     $.data.order_id
    [Common] - Set variable     name=${output_id}       value=${order_id}

[Payment][Order][Suite] - Create list of additional_references
    [Arguments]
    ...         ${output_list_additional_references}=suite_list_additional_references
    ...         ${output_size_of_list_additional_references}=suite_size_of_list_additional_references
    ...         &{arg_dic}
    @{list_additional_references}    create list
        :FOR     ${index}   IN RANGE    0   ${arg_dic.number}
    \   ${key}                       generate random string    10    [LETTERS]
    \   ${value}                     generate random string    20    [LETTERS]
    \   set suite variable    ${suite_additionnal_refference_key_${index}}                 ${key}${index}
    \   set suite variable    ${suite_additionnal_refference_value_${index}}               ${value}${index}
    \   ${index}        set variable    {"key":"${suite_additionnal_refference_key_${index}}","value":"${suite_additionnal_refference_value_${index}}"}
    \   append to list      ${list_additional_references}      ${index}
    \   log                 ${list_additional_references}

    [Common] - Set variable     name=${output_list_additional_references}       value=${list_additional_references}
    [Common] - Set variable     name=${output_size_of_list_additional_references}       value=${arg_dic.number}

[Payment][Order][Suite] - Create and execute payment normal with list of additional_references
    [Arguments]     &{arg_dic}
    [Payment][Order][Suite] - Create list of additional_references    number=2       output_list_additional_references=suite_list_additional_references   output_size_of_list_additional_references=suite_size_of_list_additional_references

    ${text}=     generate random string      15      [LETTERS]
    [Common][Prepare] - Access token headers
             ...     client_id=${setup_admin_client_id}
             ...     client_secret=${setup_admin_client_secret}
             ...     access_token=${suite_company_agent_access_token}
             ...     output=SUITE_COMPANY_AGENT_HEADERS
    [Payment][Prepare] - Create normal order - body
             ...     output=create_order_body
             ...     ext_transaction_id=Suite_Setup_${text}
             ...     product_service_id=${suite_service_id_${arg_dic.currency}}      product_service_name=${suite_service_name_${arg_dic.currency}}      product_name=${text}
             ...     product_ref_1=${text}     product_ref_2=${text}     product_ref_3=${text}
             ...     product_ref_4=${text}     product_ref_5=${text}     product_ref_3=${text}
             ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_company_agent_access_token}
             ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
             ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
             ...     payee_user_id=${suite_customer_id}       payee_user_type=${suite_customer_type}
             ...     amount=500000          additional_references=${suite_list_additional_references}
    [Payment][200] - Create normal orders
             ...     output_id=suite_order_id_${arg_dic.currency}
             ...     headers=${SUITE_COMPANY_AGENT_HEADERS}
             ...     body=${create_order_body}

    [Payment][200] - Payment execute order
             ...     headers=${SUITE_COMPANY_AGENT_HEADERS}
             ...     order_id=${suite_order_id_${arg_dic.currency}}

[Payment][Order][Suite] - Create payment normal with list of additional_references
    [Arguments]     &{arg_dic}
    [Payment][Order][Suite] - Create list of additional_references    number=2       output_list_additional_references=suite_list_additional_references   output_size_of_list_additional_references=suite_size_of_list_additional_references

    ${text}=     generate random string      15      [LETTERS]
    [Common][Prepare] - Access token headers
             ...     client_id=${setup_admin_client_id}
             ...     client_secret=${setup_admin_client_secret}
             ...     access_token=${suite_company_agent_access_token}
             ...     output=SUITE_COMPANY_AGENT_HEADERS
    [Payment][Prepare] - Create normal order - body
             ...     output=create_order_body
             ...     ext_transaction_id=Suite_Setup_${text}
             ...     product_service_id=${suite_service_id_${arg_dic.currency}}      product_service_name=${suite_service_name_${arg_dic.currency}}      product_name=${text}
             ...     product_ref_1=${text}     product_ref_2=${text}     product_ref_3=${text}
             ...     product_ref_4=${text}     product_ref_5=${text}     product_ref_3=${text}
             ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_company_agent_access_token}
             ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
             ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
             ...     payee_user_id=${suite_customer_id}       payee_user_type=${suite_customer_type}
             ...     amount=500000          additional_references=${suite_list_additional_references}
    [Payment][200] - Create normal orders
             ...     output_id=suite_order_id_${arg_dic.currency}
             ...     headers=${SUITE_COMPANY_AGENT_HEADERS}
             ...     body=${create_order_body}

[Payment][Suite] Save service_group_id and service_group_name for currency
    [Arguments]    &{arg_dic}
    set suite variable      ${suite_service_group_id_${arg_dic.currency}}            ${arg_dic.service_group_id}
    set suite variable      ${suite_service_group_name_${arg_dic.currency}}          ${arg_dic.service_group_name}

[Payment][200] - User update sof service balance limitation
    [Arguments]   ${id}     ${headers}=${SUITE_ADMIN_HEADERS}    &{arg_dic}
    [Payment][Prepare] - Update sof service balance limitation - body
        ...     &{arg_dic}
    [Payment][200] - Update sof service balance limitation with the id parameter
        ...     headers=${headers}
        ...     body=${body}
        ...     id=${id}

[Payment][200][Prepare] - Create service
    [Arguments]    ${headers}=${SUITE_ADMIN_HEADERS}  ${output}=test_service_id     &{arg_dic}
    [Payment][Prepare] - Create service - body
    ...       &{arg_dic}
    [Payment][200] - Create service
    ...     ${headers}
    ...     ${body}
    [Payment][Extract] Create service - service id      ${output}


[Payment][200][Prepare] - Create sof service balance limitation
    [Arguments]    ${sof_cash_id}   ${service_id}   ${headers}=${SUITE_ADMIN_HEADERS}  ${output}=test_balance_limitation_id     &{arg_dic}
    [Payment][Prepare] - Create sof service balance limitation - body
    ...       &{arg_dic}
    [Payment][200] - Create sof service balance limitation
    ...     ${headers}
    ...     ${body}
    ...     ${sof_cash_id}
    ...     ${service_id}
    [Payment][Extract] Create sof service balance limitation - balance_limitation_id      ${output}

[Payment][400][Prepare] - Create sof service balance limitation
    [Arguments]    ${status_code}    ${status_message}    ${sof_cash_id}   ${service_id}   ${headers}=${SUITE_ADMIN_HEADERS}    &{arg_dic}
    [Payment][Prepare] - Create sof service balance limitation - body
    ...       &{arg_dic}
    [Payment][400] - Create sof service balance limitation
    ...     ${headers}
    ...     ${body}
    ...     ${sof_cash_id}
    ...     ${service_id}
    rest.string    $.status.code       ${status_code}
    rest.string    $.status.message    ${status_message}

[Payment][400][Prepare] - Update sof service balance limitation
    [Arguments]    ${status_code}    ${status_message}    ${id}   ${headers}=${SUITE_ADMIN_HEADERS}    &{arg_dic}
    [Payment][Prepare] - Update sof service balance limitation - body
    ...       &{arg_dic}
    [Payment][400] - Update sof service balance limitation
    ...     ${headers}
    ...     ${body}
    ...     ${id}
    rest.string    $.status.code       ${status_code}
    rest.string    $.status.message    ${status_message}

[Payment][200][Prepare] - system user update company agent cash balance
    [Arguments]     ${currency}     ${headers}=${SUITE_ADMIN_HEADERS}   &{arg_dic}
    [Payment][Prepare] - system user update company agent cash balance - body
    ...     &{arg_dic}
    [Payment][200] - System user update company agent cash balance
    ...     headers=${headers}
    ...     body=${body}
    ...     currency=${currency}

[Payment][200][Prepare] - link bank by bank
    [Arguments]    ${output_sof_id}=test_sof_id    ${output_bank_token}=test_bank_token   ${headers}=${SUITE_ADMIN_HEADERS}   &{arg_dic}
    [Payment][Prepare] - Link bank by bank - body
    ...     &{arg_dic}
    [Payment][200] - User Link Bank By Bank
    ...     headers=${headers}
    ...     body=${body}
    ${sof_id}  rest extract    $.data.sof_id
    ${bank_token}  rest extract    $.data.bank_token
    [Common] - Set variable     name=${output_sof_id}   value=${sof_id}
    [Common] - Set variable     name=${output_bank_token}   value=${bank_token}

[Payment][400][Prepare] - Execute order
    [Arguments]    ${status_code}  ${status_message}     ${order_id}     ${headers}=${SUITE_ADMIN_HEADERS}
    [Payment][400] - Execute order
    ...     headers=${headers}
    ...     order_id=${order_id}
    rest.string    $.status.code       ${status_code}
    rest.string    $.status.message       ${status_message}

[payment][200][prepare] - create normal order
    [Arguments]  ${output_order_id}=test_order_id  ${headers}=${SUITE_ADMIN_HEADERS}   &{arg_dic}
    [payment][pre_request] - create normal order - body
    ...     &{arg_dic}
    [Payment][200] - Create normal order
    ...     headers=${headers}
    ...     body=${body}
    [common][extract] - order_id
    ...     output=${output_order_id}

[payment][200][prepare] - create artifact order
    [Arguments]  ${output_order_id}=test_order_id  ${headers}=${SUITE_ADMIN_HEADERS}   &{arg_dic}
    [payment][pre_request] - create artifact order - body
    ...     &{arg_dic}
    [Payment][200] - Create artifact order
    ...     headers=${headers}
    ...     body=${body}
    [common][extract] - order_id
    ...     output=${output_order_id}

