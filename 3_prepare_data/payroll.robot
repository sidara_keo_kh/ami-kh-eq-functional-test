*** Settings ***
Resource         ../2_api/3_reuse_function/imports.robot
Resource         ../3_prepare_data/imports.robot

*** Keywords ***
[Payroll][200][Prepare] - system create and execute payroll order
    [Arguments]     ${headers}=${SUITE_ADMIN_HEADERS}  ${output}=test_order_id     &{arg_dic}
    [Payroll][Prepare] - system create and execute payroll order - body
    ...     &{arg_dic}
    [Payroll][200] - system create and execute payroll order
    ...     headers=${headers}
    ...     body=${body}

[Payroll][400][Prepare] - system create and execute payroll order
    [Arguments]    ${status_code}  ${status_message}    ${headers}=${SUITE_ADMIN_HEADERS}  ${output}=test_order_id     &{arg_dic}
    [Payroll][Prepare] - system create and execute payroll order - body
    ...     &{arg_dic}
    [Payroll][400] - system create and execute payroll order
    ...     headers=${headers}
    ...     body=${body}
    rest.string    $.status.code       ${status_code}
    rest.string    $.status.message       ${status_message}