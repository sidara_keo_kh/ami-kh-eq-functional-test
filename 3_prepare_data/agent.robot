*** Settings ***
Resource         ../2_api/3_reuse_function/imports.robot
Resource         ../3_prepare_data/imports.robot

*** Keywords ***
[Agent][200][Prepare] - Create agent relationship
    [Arguments]     &{arg_dic}
    [Common][Prepare] - Access token headers
        ...     access_token=${arg_dic.access_token}
        ...     output=headers
    [Agent][Prepare] - create relationship - body   output=body  &{arg_dic}
    [Agent][200] - create relationship
        ...     headers=${headers}
        ...     body=${body}
    [Agent][Extract] - Create relationship - id    output_relationship_id=${arg_dic.output_relationship_id}


[Agent][200][Prepare] - Update agent relationship
    [Arguments]     &{arg_dic}
    [Common][Prepare] - Access token headers
        ...     access_token=${arg_dic.access_token}
        ...     output=headers
    [Agent][Prepare] - update relationship - body   output=body  &{arg_dic}
    [Agent][200] - update relationship
        ...     headers=${headers}
        ...     body=${body}
        ...     relationship_id=${arg_dic.relationship_id}

[Agent][200][Prepare] - Delete agent classification
    [Arguments]     &{arg_dic}
    [Common][Prepare] - Access token headers
        ...     access_token=${arg_dic.access_token}
        ...     output=headers
    [Agent][200] - delete agent classification
        ...     headers=${headers}
        ...     agent_classification_id=${arg_dic.agent_classification_id}

[Agent][200][Prepare] - Delete agent
    [Arguments]     &{arg_dic}
    [Common][Prepare] - Access token headers
        ...     access_token=${arg_dic.access_token}
        ...     output=headers
    [Agent][200] - delete agent
        ...     headers=${headers}
        ...     agent_id=${arg_dic.agent_id}

[Agent][200][Prepare] - Create agent type
    [Arguments]     &{arg_dic}
    [Common][Prepare] - Access token headers
        ...     access_token=${arg_dic.access_token}
        ...     output=headers
    ${name}=            generate random string  20  [LETTERS]
    ${description}=     generate random string  20  [LETTERS]
    ${reference}=     generate random string  20  [LETTERS]
    [Common] - Set default value for keyword in dictionary  ${arg_dic}  name                    ${name}
    [Common] - Set default value for keyword in dictionary  ${arg_dic}  description             ${description}
    [Common] - Set default value for keyword in dictionary  ${arg_dic}  output_agent_type_id    test_agent_type_id
    [Common] - Set default value for keyword in dictionary  ${arg_dic}  is_manager          ${arg_dic.is_manager}
    [Common] - Set default value for keyword in dictionary  ${arg_dic}  reference_1         add_${reference}
    [Common] - Set default value for keyword in dictionary  ${arg_dic}  reference_2         add_${reference}
    [Common] - Set default value for keyword in dictionary  ${arg_dic}  reference_3         add_${reference}
    [Common] - Set default value for keyword in dictionary  ${arg_dic}  reference_4         add_${reference}
    [Common] - Set default value for keyword in dictionary  ${arg_dic}  reference_5         add_${reference}

    [Agent][Prepare] - create agent type - body     output=body     &{arg_dic}
    [Agent][200] - create agent type
        ...     headers=${headers}
        ...     body=${body}
    ${agent_type_id}    rest extract    $.data.id
    [Common] - Set variable     name=${arg_dic.output_agent_type_id}    value=${agent_type_id}
    [Common] - Set variable     name=test_created_agent_type_name   value=${name}
    [Common] - Set variable     name=test_created_agent_type_desc   value=${description}
    [Common] - Set variable     name=test_created_agent_type_ref    value=add_${reference}

[Agent][200][Prepare] - Update agent type
    [Arguments]     &{arg_dic}
    [Common][Prepare] - Access token headers
        ...     access_token=${arg_dic.access_token}
        ...     output=headers
    ${name}=            generate random string  20  [LETTERS]
    ${description}=     generate random string  20  [LETTERS]
    [Common] - Set default value for keyword in dictionary  ${arg_dic}  name                    ${name}
    [Common] - Set default value for keyword in dictionary  ${arg_dic}  description             ${description}
    ${ref}=     generate random string  20  [LETTERS]
    [Common] - Set default value for keyword in dictionary  ${arg_dic}  reference_1             update_${ref}
    [Common] - Set default value for keyword in dictionary  ${arg_dic}  reference_2             update_${ref}
    [Common] - Set default value for keyword in dictionary  ${arg_dic}  reference_3             update_${ref}
    [Common] - Set default value for keyword in dictionary  ${arg_dic}  reference_4             update_${ref}
    [Common] - Set default value for keyword in dictionary  ${arg_dic}  reference_5             update_${ref}
    [Common] - Set default value for keyword in dictionary  ${arg_dic}  output_updated_agent_type_name  test_updated_agent_type_name
    [Common] - Set default value for keyword in dictionary  ${arg_dic}  output_updated_agent_type_desc  test_updated_agent_type_desc
    [Common] - Set default value for keyword in dictionary  ${arg_dic}  output_updated_agent_type_ref   test_updated_agent_type_ref
    [Common] - Set default value for keyword in dictionary  ${arg_dic}  output_updated_agent_type_manager   test_updated_agent_type_manager
    [Agent][Prepare] - update agent type - body     output=body     &{arg_dic}
    [Agent][200] - update agent type
        ...     headers=${headers}
        ...     body=${body}
        ...     agent_type_id=${arg_dic.agent_type_id}
    [Common] - Set variable     name=${arg_dic.output_updated_agent_type_name}      value=${name}
    [Common] - Set variable     name=${arg_dic.output_updated_agent_type_desc}      value=${description}
    [Common] - Set variable     name=${arg_dic.output_updated_agent_type_ref}       value=update_${ref}
    [Common] - Set variable     name=${arg_dic.output_updated_agent_type_manager}   value=${arg_dic.is_manager}

[Agent][200][Prepare] - Search all agent types
    [Arguments]     &{arg_dic}
    [Common][Prepare] - Access token headers
        ...     access_token=${arg_dic.access_token}
        ...     output=headers
    [Common] - Set default value for keyword in dictionary  ${arg_dic}  output_search_agent_types   test_agent_types
    [Report][Prepare] - search agent types - body   output=body     &{arg_dic}
    [Report][200] - Get all agent type
        ...     headers=${headers}
        ...     body=${body}
    ${agent_types}  rest extract     $.data
    [Common] - Set variable     name=${arg_dic.output_search_agent_types}   value=${agent_types}

[Agent][200][Prepare] - Create an agent
    [Arguments]
        ...     ${headers}=${SUITE_ADMIN_HEADERS}
        ...     ${output_agent_type_id}=test_agent_type_id
        ...     ${output_agent_id}=test_agent_id
        ...     ${output_agent_username}=test_agent_username
        ...     ${output_agent_password}=test_agent_password
        ...     ${output_agent_unique_reference}=test_agent_unique_reference
    ${agent_type_name}   generate random string  10    [LETTERS]
    [Agent][Prepare] - create agent type - body
        ...     name=${agent_type_name}
        ...     description=description_${agent_type_name}
        ...     is_sale=true
    [Agent][200] - create agent type
        ...     headers=${headers}
        ...     body=${body}
    [Common][Extract] - ID
        ...     output=${output_agent_type_id}
    ${unique_reference}   generate random string  10    [NUMBERS]
    ${username}   generate random string  10    [NUMBERS]
    [Common] - Set variable
        ...     name=${output_agent_username}
        ...     value=${username}
    [Common] - Set variable
        ...     name=${output_agent_password}
        ...     value=${setup_agent_password}
    ${encrypted_password}     rsa encrypt     ${setup_agent_password}     ${agent_rsa_public_key}     ${False}
    [Agent][Prepare] - create agent - body
        ...     agent_type_id=${${output_agent_type_id}}
        ...     unique_reference=${unique_reference}
        ...     identity_username=${username}
        ...     identity_password=${encrypted_password}
        ...     identity_type_id=1
    [Agent][200] - create agent
        ...     headers=${headers}
        ...     body=${body}
    [Common][Extract] - ID
        ...     output=${output_agent_id}
    [Common] - Set variable      name=${output_agent_unique_reference}  value=${unique_reference}

[Agent][200][Prepare] - Create agent shop
    [Arguments]
        ...     ${headers}=${SUITE_ADMIN_HEADERS}
        ...     ${agent_id}=${test_agent_id}
        ...     ${output_shop_id}=test_shop_id
        ...     ${output_shop_city}=test_shop_city
        ...     ${output_shop_province}=test_shop_province
    ${shop_name}        generate random string    10    [LETTERS]
    ${shop_city}        generate random string    10    [LETTERS]
    ${shop_province}    generate random string    10    [LETTERS]
    [Common] - Set variable
        ...     name=${output_shop_city}
        ...     value=${shop_city}
    [Common] - Set variable
        ...     name=${output_shop_province}
        ...     value=${shop_province}
    [Agent][Prepare] - create shop - body
        ...     agent_id=${agent_id}
        ...     name=${shop_name}
        ...     address_city=${shop_city}
        ...     address_province=${shop_province}
    [Agent][200] - create shop
        ...     headers=${headers}
        ...     body=${body}
    [Common][Extract] - ID
        ...     output=${output_shop_id}

[Agent][200][Prepare] - Create an agent with sof_card
    [Arguments]
        ...     ${headers}=${SUITE_ADMIN_HEADERS}
        ...     ${currency}=USD
        ...     ${output_agent_type_id}=test_agent_type_id
        ...     ${output_agent_id}=test_agent_id
        ...     ${output_sof_card_token}=test_sof_card_token
    [Agent][200][Prepare] - Create an agent
        ...     headers=${headers}
        ...     output_agent_type_id=${output_agent_type_id}
        ...     output_agent_id=${output_agent_id}
        ...     output_agent_username=agent_username
        ...     output_agent_password=agent_password
    [Api_Gateway][200][Prepare] - Agent authentication with plain password
        ...     username=${agent_username}
        ...     password=${agent_password}
        ...     client_id=${headers["client_id"]}
        ...     client_secret=${headers["client_secret"]}
        ...     output_token=agent_access_token
    [Common][Prepare] - Access token headers
        ...     access_token=${agent_access_token}
        ...     output=agent_headers
    ${provider_name}    generate random string  10    [LETTERS]
    [Sof_Card][Prepare] - add provider - body
        ...     $.name=${provider_name}
    [Sof_Card][200] - add provider
        ...     headers=${headers}
        ...     body=${body}
    [Common][Extract] - ID
        ...     output=provider_id
    ${card_design_name}  generate random string     20    [LETTERS]
    ${pattern}          generate random string  6   [NUMBERS]
    [Sof_Card][Prepare] - add card design - body
        ...     $.name=${card_design_name}
        ...     $.card_type_id=1
        ...     $.is_active=true
        ...     $.currency=${currency}
        ...     $.pan_pattern=${pattern}
        ...     $.pre_link_url=${pre_link_url}
        ...     $.pre_link_read_timeout=10000
        ...     $.link_url=${link_url}
        ...     $.link_read_timeout=10000
        ...     $.un_link_url=${un_link_url}
        ...     $.un_link_read_timeout=10000
        ...     $.debit_url=${debit_url}
        ...     $.debit_read_timeout=10000
        ...     $.credit_url=${credit_url}
        ...     $.credit_read_timeout=10000
        ...     $.check_status_url=${check_status_url}
        ...     $.check_status_read_timeout=10000
        ...     $.cancel_url=${cancel_url}
        ...     $.cancel_read_timeout=10000
        ...     $.pre_sof_order_url=${pre_sof_order_url}
        ...     $.pre_sof_order_read_timeout=10000
    [Sof_Card][200] - add card design
        ...     headers=${headers}
        ...     body=${body}
        ...     provider_id=${provider_id}
    [Common][Extract] - ID
        ...     output=card_design_id
    ${citizen_id}   generate random string  6   [NUMBERS]
    ${cvv_cvc}    generate random string  3   [NUMBERS]
    ${pin}  generate random string  15   [NUMBERS]
    ${mobile_number}  generate random string  15   [NUMBERS]
    ${billing_address}  generate random string  15   [LETTERS]
    ${card_number_last_8}   generate random string  8   [NUMBERS]
    ${card_number}     catenate     ${pattern}     ${card_number_last_8}
    [Payment][Prepare] - verify (pre-link) card information - body
        ...     card_account_name=${card_design_name}
        ...     card_account_number=${card_number}
        ...     citizen_id=${citizen_id}
        ...     cvv_cvc=${cvv_cvc}
        ...     pin=${pin}
        ...     expiry_date=12/08
        ...     issue_date=11/08
        ...     mobile_number=${mobile_number}
        ...     billing_address=${billing_address}
        ...     card_type=ATM
        ...     currency=${currency}
    [Payment][200] - verify (pre-link) card information
        ...     headers=${agent_headers}
        ...     body=${body}
    [Payment][Extract] Verify (pre-link) card information - security ref
        ...     output=security_ref
    [Payment][Prepare] - user link card source of fund - body
        ...     card_account_name=${card_design_name}
        ...     card_account_number=${card_number}
        ...     citizen_id=${citizen_id}
        ...     cvv_cvc=${cvv_cvc}
        ...     pin=${pin}
        ...     expiry_date=12/08
        ...     issue_date=11/08
        ...     mobile_number=${mobile_number}
        ...     billing_address=${billing_address}
        ...     currency=${currency}
        ...     security_code=123456
        ...     security_ref=${security_ref}
    [Payment][200] - user link card source of fund
        ...     headers=${agent_headers}
        ...     body=${body}
    [Payment][Extract] user link card source of fund - token
        ...     output=${output_sof_card_token}

[Agent][200][Prepare] - Create an agent with sof_cash
    [Arguments]
        ...     ${headers}=${SUITE_ADMIN_HEADERS}
        ...     ${currency}=USD
        ...     ${output_agent_type_id}=test_agent_type_id
        ...     ${output_agent_id}=test_agent_id
        ...     ${output_sof_cash_id}=test_sof_cash_id
        ...     ${output_agent_headers}=test_agent_headers
        ...     ${output_agent_access_token}=agent_access_token
        ...     ${output_agent_unique_reference}=test_agent_unique_reference

    [Agent][200][Prepare] - Create an agent
        ...     headers=${headers}
        ...     output_agent_type_id=${output_agent_type_id}
        ...     output_agent_id=${output_agent_id}
        ...     output_agent_username=agent_username
        ...     output_agent_password=agent_password
        ...     output_agent_unique_reference=${output_agent_unique_reference}
    [Api_Gateway][200][Prepare] - Agent authentication with plain password
        ...     username=${agent_username}
        ...     password=${agent_password}
        ...     client_id=${headers["client_id"]}
        ...     client_secret=${headers["client_secret"]}
        ...     output_token=${output_agent_access_token}
    [Common][Prepare] - Access token headers
        ...     access_token=${${output_agent_access_token}}
        ...     output=${output_agent_headers}
    [Payment][Prepare] - create user sof cash - body
        ...     currency=${currency}
    [Payment][200] - create user sof cash
        ...     headers=${${output_agent_headers}}
        ...     body=${body}
    [Payment][Extract] Create user sof cash - sof id
        ...     output=${output_sof_cash_id}

[Agent][200][Prepare] - Create an sale agent
    [Arguments]
        ...     ${company_id}
        ...     ${headers}=${SUITE_ADMIN_HEADERS}
        ...     ${output_agent_type_id}=test_agent_type_id
        ...     ${output_agent_id}=test_agent_id
        ...     ${output_agent_username}=test_agent_username
        ...     ${output_agent_password}=test_agent_password
    ${agent_type_name}   generate random string  10    [LETTERS]
    [Agent][Prepare] - create agent type - body
        ...     name=${agent_type_name}
        ...     description=description_${agent_type_name}
        ...     is_sale=true
    [Agent][200] - create agent type
        ...     headers=${headers}
        ...     body=${body}
    [Common][Extract] - ID
        ...     output=${output_agent_type_id}
    ${unique_reference}   generate random string  10    [NUMBERS]
    ${username}   generate random string  10    [NUMBERS]
    [Common] - Set variable
        ...     name=${output_agent_username}
        ...     value=${username}
    [Common] - Set variable
        ...     name=${output_agent_password}
        ...     value=${setup_agent_password}
    ${encrypted_password}     rsa encrypt     ${setup_agent_password}     ${agent_rsa_public_key}     ${False}
    ${acquisition_source}   generate random string  10    [LETTERS]
    ${mm_factory_card_number}		   generate random string  10    [NUMBERS]
    ${model_type}			   generate random string  10    [LETTERS]
    ${tin_number}		    generate random string  10    [NUMBERS]
    ${title}		        generate random string  10    [LETTERS]
    ${first_name}		    generate random string  10    [LETTERS]
    ${middle_name}		    generate random string  10    [LETTERS]
    ${last_name}		    generate random string  10    [LETTERS]
    ${suffix}		        generate random string  10    [LETTERS]
    ${date_of_birth}        set variable    1988-01-01T00:00:00Z
    ${place_of_birth}	    generate random string  10    [LETTERS]
    ${gender}         set variable      male
    ${ethnicity}		    generate random string  10    [LETTERS]
    ${nationality}		    generate random string  10    [LETTERS]
    ${occupation}		    generate random string  10    [LETTERS]
    ${occupation_title}		generate random string  10    [LETTERS]
    ${township_code}		generate random string  10    [LETTERS]
    ${township_name}		   generate random string  10    [LETTERS]
    ${nationality_id_number}		   generate random string  10    [NUMBERS]
    ${mother_name}		    generate random string  10    [LETTERS]
    ${email}		        generate random string  10    [LETTERS]
    ${email}                set variable    ${email}@email.com
    ${primary_mobile_number}		   generate random string  10    [NUMBERS]
    ${secondary_mobile_number}		   generate random string  10    [NUMBERS]
    ${tertiary_mobile_number}		   generate random string  10    [NUMBERS]
    ${current_address_citizen_association}	generate random string  10    [LETTERS]
    ${current_address_neighbourhood_association}		   generate random string  10    [LETTERS]
    ${current_address_address}		   generate random string  10    [LETTERS]
    ${current_address_commune}		   generate random string  10    [LETTERS]
    ${current_address_district}		   generate random string  10    [LETTERS]
    ${current_address_city}		   generate random string  10    [LETTERS]
    ${current_address_province}		   generate random string  10    [LETTERS]
    ${current_address_postal_code}		   generate random string  10    [LETTERS]
    ${current_address_country}		   generate random string  10    [LETTERS]
    ${current_address_landmark}		   generate random string  10    [LETTERS]
    ${current_address_longitude}		   generate random string  10    [LETTERS]
    ${current_address_latitude}		   generate random string  10    [LETTERS]
    ${permanent_address_citizen_association}		   generate random string  10    [LETTERS]
    ${permanent_address_neighbourhood_association}		   generate random string  10    [LETTERS]
    ${permanent_address_address}		   generate random string  10    [LETTERS]
    ${permanent_address_commune}		   generate random string  10    [LETTERS]
    ${permanent_address_district}		   generate random string  10    [LETTERS]
    ${permanent_address_city}		   generate random string  10    [LETTERS]
    ${permanent_address_province}		   generate random string  10    [LETTERS]
    ${permanent_address_postal_code}		   generate random string  10    [LETTERS]
    ${permanent_address_country}		   generate random string  10    [LETTERS]
    ${permanent_address_landmark}		   generate random string  10    [LETTERS]
    ${permanent_address_longitude}		   generate random string  10    [LETTERS]
    ${permanent_address_latitude}		   generate random string  10    [LETTERS]
    ${bank_name}		   generate random string  10    [LETTERS]
    ${bank_account_name}		   generate random string  10    [LETTERS]
    ${bank_account_number}		   generate random string  10    [NUMBERS]
    ${bank_branch_area}		   generate random string  10    [LETTERS]
    ${bank_branch_city}		   generate random string  10    [LETTERS]
    ${bank_register_source}		   generate random string  10    [LETTERS]
    ${contract_type}		   generate random string  10    [LETTERS]
    ${contract_number}		   generate random string  10    [NUMBERS]
    ${contract_extension_type}		   generate random string  10    [LETTERS]
    ${contract_notification_alert}		   generate random string  10    [LETTERS]
    ${contract_day_of_period_reconciliation}		   generate random string  5    [NUMBERS]
    ${contract_release}		   generate random string  10    [LETTERS]
    ${contract_file_url}		   generate random string  10    [LETTERS]
    ${contract_assessment_information_url}		   generate random string  10    [LETTERS]
    ${primary_identity_type}		   generate random string  10    [LETTERS]
    ${primary_identity_status}		   generate random string  10    [LETTERS]
    ${primary_identity_identity_id}		   generate random string  10    [LETTERS]
    ${primary_identity_place_of_issue}		   generate random string  10    [LETTERS]
    ${primary_identity_issue_date}        set variable      2000-01-01T00:00:00Z
    ${primary_identity_expired_date}      set variable      2220-01-01T00:00:00Z
    ${primary_identity_front_identity_url}		   generate random string  10    [LETTERS]
    ${primary_identity_back_identity_url}		   generate random string  10    [LETTERS]
    ${secondary_identity_type}		   generate random string  10    [LETTERS]
    ${secondary_identity_status}		   generate random string  10    [LETTERS]
    ${secondary_identity_identity_id}		   generate random string  10    [LETTERS]
    ${secondary_identity_place_of_issue}		   generate random string  10    [LETTERS]
    ${secondary_identity_front_identity_url}		   generate random string  10    [LETTERS]
    ${secondary_identity_back_identity_url}		   generate random string  10    [LETTERS]
    ${remark}		   generate random string  10    [LETTERS]
    ${verify_by}		   generate random string  10    [LETTERS]
    ${risk_level}		   generate random string  10    [LETTERS]
    ${additional_acquiring_sales_executive_name}		   generate random string  10    [LETTERS]
    ${additional_relationship_manager_name}		   generate random string  10    [LETTERS]
    ${additional_sale_region}		   generate random string  10    [LETTERS]
    ${additional_commercial_account_manager}		   generate random string  10    [LETTERS]
    ${additional_profile_picture_url}		   generate random string  10    [LETTERS]
    ${additional_national_id_photo_url}		   generate random string  10    [LETTERS]
    ${additional_tax_id_card_photo_url}		   generate random string  10    [LETTERS]
    ${additional_field_1_name}		   generate random string  10    [LETTERS]
    ${additional_field_1_value}		   generate random string  10    [LETTERS]
    ${additional_field_2_name}		   generate random string  10    [LETTERS]
    ${additional_field_2_value}		   generate random string  10    [LETTERS]
    ${additional_field_3_name}		   generate random string  10    [LETTERS]
    ${additional_field_3_value}		   generate random string  10    [LETTERS]
    ${additional_field_4_name}		   generate random string  10    [LETTERS]
    ${additional_field_4_value}		   generate random string  10    [LETTERS]
    ${additional_field_5_name}		   generate random string  10    [LETTERS]
    ${additional_field_5_value}		   generate random string  10    [LETTERS]
    ${additional_supporting_file_1_url}		   generate random string  10    [LETTERS]
    ${additional_supporting_file_2_url}		   generate random string  10    [LETTERS]
    ${additional_supporting_file_3_url}		   generate random string  10    [LETTERS]
    ${additional_supporting_file_4_url}		   generate random string  10    [LETTERS]
    ${additional_supporting_file_5_url}		   generate random string  10    [LETTERS]
    ${is_testing_acc}      set variable    1
    ${is_system_acc}       set variable     0
    ${employee_id}		   generate random string  10    [LETTERS]
    ${calendar_id}		   generate random string  10    [LETTERS]

    [Agent][Prepare] - create agent profiles - body
        ...     username=${username}
        ...     password=${encrypted_password}
        ...     identity_type_id=1
        ...     agent_type_id=${${output_agent_type_id}}
        ...     unique_reference=${unique_reference}
        ...     is_testing_account=${is_testing_acc}
        ...     is_system_account=${is_system_acc}
        ...     acquisition_source=${param_not_used}
        ...     referrer_user_type_id=3
        ...     referrer_user_type_name=system-user
        ...     referrer_user_id=${param_not_used}
        ...     mm_card_type_id=${param_not_used}
        ...     mm_card_level_id=${param_not_used}
        ...     mm_factory_card_number=${mm_factory_card_number}
        ...     model_type=${model_type}
        ...     is_require_otp=${param_not_used}
        ...     agent_classification_id=${param_not_used}
        ...     tin_number=${tin_number}
        ...     title=${title}
        ...     first_name=${first_name}
        ...     middle_name=${middle_name}
        ...     last_name=${last_name}
        ...     suffix=${suffix}
        ...     date_of_birth=${date_of_birth}
        ...     place_of_birth=${place_of_birth}
        ...     gender=${gender}
        ...     ethnicity=${ethnicity}
        ...     nationality=${nationality}
        ...     occupation=${occupation}
        ...     occupation_title=${occupation_title}
        ...     township_code=${township_code}
        ...     township_name=${township_name}
        ...     nationality_id_number=${nationality_id_number}
        ...     mother_name=${mother_name}
        ...     email=${email}
        ...     primary_mobile_number=${primary_mobile_number}
        ...     secondary_mobile_number=${secondary_mobile_number}
        ...     tertiary_mobile_number=${tertiary_mobile_number}
        ...     current_address_citizen_association=${current_address_citizen_association}
        ...     current_address_neighbourhood_association=${current_address_neighbourhood_association}
        ...     current_address_address=${current_address_address}
        ...     current_address_commune=${current_address_commune}
        ...     current_address_district=${current_address_district}
        ...     current_address_city=${current_address_city}
        ...     current_address_province=${current_address_province}
        ...     current_address_postal_code=${current_address_postal_code}
        ...     current_address_country=${current_address_country}
        ...     current_address_landmark=${current_address_landmark}
        ...     current_address_longitude=${current_address_longitude}
        ...     current_address_latitude=${current_address_latitude}
        ...     permanent_address_citizen_association=${permanent_address_citizen_association}
        ...     permanent_address_neighbourhood_association=${permanent_address_neighbourhood_association}
        ...     permanent_address_address=${permanent_address_address}
        ...     permanent_address_commune=${permanent_address_commune}
        ...     permanent_address_district=${permanent_address_district}
        ...     permanent_address_city=${permanent_address_city}
        ...     permanent_address_province=${permanent_address_province}
        ...     permanent_address_postal_code=${permanent_address_postal_code}
        ...     permanent_address_country=${permanent_address_country}
        ...     permanent_address_landmark=${permanent_address_landmark}
        ...     permanent_address_longitude=${permanent_address_longitude}
        ...     permanent_address_latitude=${permanent_address_latitude}
        ...     bank_name=${bank_name}
        ...     bank_account_status=${param_not_used}
        ...     bank_account_name=${bank_account_name}
        ...     bank_account_number=${bank_account_number}
        ...     bank_branch_area=${bank_branch_area}
        ...     bank_branch_city=${bank_branch_city}
        ...     bank_register_date=${param_not_used}
        ...     bank_register_source=${bank_register_source}
        ...     bank_is_verified=${param_not_used}
        ...     bank_end_date=${param_not_used}
        ...     contract_type=${contract_type}
        ...     contract_number=${contract_number}
        ...     contract_extension_type=${contract_extension_type}
        ...     contract_sign_date=${param_not_used}
        ...     contract_issue_date=${param_not_used}
        ...     contract_expired_date=${param_not_used}
        ...     contract_notification_alert=${contract_notification_alert}
        ...     contract_day_of_period_reconciliation=${contract_day_of_period_reconciliation}
        ...     contract_release=${contract_release}
        ...     contract_file_url=${contract_file_url}
        ...     contract_assessment_information_url=${contract_assessment_information_url}
        ...     primary_identity_type=${primary_identity_type}
        ...     primary_identity_status=1
        ...     primary_identity_identity_id=${primary_identity_identity_id}
        ...     primary_identity_place_of_issue=${primary_identity_place_of_issue}
        ...     primary_identity_issue_date=${primary_identity_issue_date}
        ...     primary_identity_expired_date=${primary_identity_expired_date}
        ...     primary_identity_front_identity_url=${primary_identity_front_identity_url}
        ...     primary_identity_back_identity_url=${primary_identity_back_identity_url}
        ...     secondary_identity_type=${secondary_identity_type}
        ...     secondary_identity_status=1
        ...     secondary_identity_identity_id=${secondary_identity_identity_id}
        ...     secondary_identity_place_of_issue=${secondary_identity_place_of_issue}
        ...     secondary_identity_issue_date=${param_not_used}
        ...     secondary_identity_expired_date=${param_not_used}
        ...     secondary_identity_front_identity_url=${secondary_identity_front_identity_url}
        ...     secondary_identity_back_identity_url=${secondary_identity_back_identity_url}
        ...     status_id=${param_not_used}
        ...     remark=${remark}
        ...     verify_by=${verify_by}
        ...     verify_date=${param_not_used}
        ...     risk_level=${risk_level}
        ...     additional_acquiring_sales_executive_id=${param_not_used}
        ...     additional_acquiring_sales_executive_name=${additional_acquiring_sales_executive_name}
        ...     additional_relationship_manager_id=${param_not_used}
        ...     additional_relationship_manager_name=${additional_relationship_manager_name}
        ...     additional_sale_region=${additional_sale_region}
        ...     additional_commercial_account_manager=${additional_commercial_account_manager}
        ...     additional_profile_picture_url=${additional_profile_picture_url}
        ...     additional_national_id_photo_url=${additional_national_id_photo_url}
        ...     additional_tax_id_card_photo_url=${additional_tax_id_card_photo_url}
        ...     additional_field_1_name=${additional_field_1_name}
        ...     additional_field_1_value=${additional_field_1_value}
        ...     additional_field_2_name=${additional_field_2_name}
        ...     additional_field_2_value=${additional_field_2_value}
        ...     additional_field_3_name=${additional_field_3_name}
        ...     additional_field_3_value=${additional_field_3_value}
        ...     additional_field_4_name=${additional_field_4_name}
        ...     additional_field_4_value=${additional_field_4_value}
        ...     additional_field_5_name=${additional_field_5_name}
        ...     additional_field_5_value=${additional_field_5_value}
        ...     additional_supporting_file_1_url=${additional_supporting_file_1_url}
        ...     additional_supporting_file_2_url=${additional_supporting_file_2_url}
        ...     additional_supporting_file_3_url=${additional_supporting_file_3_url}
        ...     additional_supporting_file_4_url=${additional_supporting_file_4_url}
        ...     additional_supporting_file_5_url=${additional_supporting_file_5_url}
        ...     is_sale=1
        ...     sale_employee_id=${employee_id}
        ...     sale_calendar_id=${calendar_id}
        ...     company_id=${company_id}
    [Agent][200] - create agent profiles
        ...     headers=${headers}
        ...     body=${body}
    [Common][Extract] - ID
        ...     output=${output_agent_id}

[Agent][200][Prepare] - API agent authentication
    [Arguments]      ${output_agent_access_token}=test_agent_access_token    &{arg_dic}
    [Common] - Set default value for keyword in dictionary  ${arg_dic}     client_id        ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary  ${arg_dic}     client_secret    ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary  ${arg_dic}     channel_id       1
    [Agent][Prepare] API agent authentication - header
        ...     client_id=${arg_dic.client_id}
        ...     client_secret=${arg_dic.client_secret}
        ...     channel_id=${arg_dic.channel_id}
        ...     output=authen_header

    [Common] - Set default value for keyword in dictionary  ${arg_dic}     grant_type          password
    [Common] - Set default value for keyword in dictionary  ${arg_dic}     param_client_id     ${setup_admin_client_id}

    [Agent][Prepare] API agent authentication - param
        ...     username=${arg_dic.username}
        ...     grant_type=${arg_dic.grant_type}
        ...     password=${arg_dic.password}
        ...     param_client_id=${arg_dic.param_client_id}

    [Agent][200] API agent authentication
        ...     headers=${authen_header}
        ...     params=${params}

    [Agent][Extract] - Authentication agent - access_token    ${output_agent_access_token}

[Agent][200][Prepare] - Create sale hierarchy
    [Arguments]     ${output_hierarchy_id}=hierarchy_id     &{arg_dic}
    ${name}     generate random string      6      [NUMBERS]
    ${description}      generate random string      100      [NUMBERS]
    [Common] - Set default value for keyword in dictionary  ${arg_dic}     name             ${name}
    [Common] - Set default value for keyword in dictionary  ${arg_dic}     description      ${description}
    [Agent][Prepare] API create sale hierachy - body
        ...     root_id=${arg_dic.root_id}
        ...     name=${arg_dic.name}
        ...     description=${arg_dic.description}

    [Agent][200] API create sale hierachy
        ...     ${arg_dic.headers}
        ...     ${body}

    [Agent][Extract] - API create sale hierachy
        ...     ${output_hierarchy_id}

[Agent][200][Prepare] - Create hierarchy connection
    [Arguments]     ${headers}      &{arg_dic}
    [Agent][Prepare] - create connection - body
        ...     parent_id=${arg_dic.parent_id}
        ...     child_id=${arg_dic.child_id}

    [Agent][200] - create connection
        ...     headers=${headers}
        ...     body=${body}
        ...     hierarchy_id=${arg_dic.hierarchy_id}

[Agent][200][Prepare] - create company profiles
    [Arguments]    ${output}=test_company_id  ${headers}=${SUITE_ADMIN_HEADERS}      &{arg_dic}
    [Agent][Prepare] - create company profiles - body
    ...     &{arg_dic}
    [Agent][200] - create company profiles
    ...     headers=${headers}
    ...     body=${body}
    [Common][Extract] - ID
    ...     output=${output}

[Agent][200][Prepare] - admin updates full agent profiles
    [Arguments]  ${agent_id}   ${output}=test_company_id   ${headers}=${SUITE_ADMIN_HEADERS}      &{arg_dic}
    [Agent][Prepare] - admin updates full agent profiles - body
    ...     &{arg_dic}
    [Agent][200] - admin updates full agent profiles
    ...     headers=${headers}
    ...     body=${body}
    ...     agent_id=${agent_id}

[Agent][200][Prepare] - system user update company wallet user
    [Arguments]  ${company_id}   ${headers}=${SUITE_ADMIN_HEADERS}      &{arg_dic}
    [Agent][Prepare] - system user update company wallet user - body
    ...     &{arg_dic}
    [Agent][200] - system user update company wallet user
    ...     headers=${headers}
    ...     body=${body}
    ...     company_id=${company_id}

[Agent][200][Prepare] - create relationship
    [Arguments]  ${output_relationship_id}=test_relationship_id   ${headers}=${SUITE_ADMIN_HEADERS}      &{arg_dic}
    [Agent][Prepare] - create relationship - body
    ...     &{arg_dic}
    [Agent][200] - create relationship
    ...     headers=${headers}
    ...     body=${body}
    [Common][Extract] - ID
    ...     output=${output_relationship_id}

[agent][200][prepare] - update agent relationship sharing benefit
    [Arguments]  ${relationship_id}     ${headers}=${SUITE_ADMIN_HEADERS}      &{arg_dic}
    [Agent][prepare] - update agent relationship sharing benefit - body
    ...     &{arg_dic}
    [Agent][200] - update agent relationship sharing benefit
    ...     headers=${headers}
    ...     body=${body}
    ...     relationship_id=${relationship_id}