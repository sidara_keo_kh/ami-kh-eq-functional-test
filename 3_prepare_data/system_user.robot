*** Settings ***
Resource         ../2_api/3_reuse_function/imports.robot
Resource         ../3_prepare_data/imports.robot

*** Keywords ***
[System_User][200][Prepare] - Add permission by name to user
    [Arguments]     ${permission_name}      ${user_id}      ${headers}=${SUITE_ADMIN_HEADERS}
    [Report][200] Search permission by name
        ...     permission_name=${permission_name}
        ...     output_permission_id=permission_id
        ...     headers=${headers}
    [System_User][Prepare] - Create role - body
    [System_User][200] - Create role
        ...     headers=${headers}
        ...     body=${body}
    [Common][Extract] - ID
        ...     output=role_id
    [System_User][Prepare] - Add permissions to role - body
        ...     $.permissions=[ ${permission_id} ]
    [System_User][200] - Add permission to role
        ...     headers=${headers}
        ...     body=${body}
        ...     role_id=${role_id}
    [System_User][Prepare] - Add user to role - body
        ...     $.user_id=${user_id}
    [System_User][200] - Add user to role
        ...     headers=${headers}
        ...     body=${body}
        ...     role_id=${role_id}

[System_User][200][Prepare] - Create system user
    [Arguments]
        ...     ${output_user_id}=test_system_user_id
        ...     ${output_user_name}=test_system_user_name
        ...     ${output_password}=test_system_user_password
    [Api_Gateway][200][Prepare] - System user authentication with plain password
        ...     username=${setup_admin_username}
        ...     password=${setup_admin_password}
        ...     output_token=admin_access_token
    [Common][Prepare] - Access token headers
        ...     access_token=${admin_access_token}
        ...     output=admin_headers
    ${name}    generate random string   10     [LETTERS]
    [Common] - Set variable     name=${output_user_name}        value=user_${name}
    ${initial_password}     set variable        ${setup_password_robot}1
    ${encrypted_initial_password}       rsa encrypt         ${initial_password}     ${system_user_rsa_public_key}     ${False}
    [System_User][Prepare] - Create system user - body
        ...     $.username=${${output_user_name}}
        ...     $.password=${encrypted_initial_password}
        ...     $.firstname=${name} firstName
        ... 	$.lastname=${name} lastName
        ...     $.email=${name}@email.com
        ...     $.middle_name=${name}_middleNname
        ...     $.is_external=true
    [System_User][200] - Create system user
        ...     headers=${admin_headers}
        ...     body=${body}
    [Common][Extract] - ID
        ...     output=${output_user_id}
    [System_User][Prepare] - Add user to role - body
        ...     $.user_id=${${output_user_id}}
    [System_User][200] - Add user to role
        ...     headers=${admin_headers}
        ...     body=${body}
        ...     role_id=1

    [Api_Gateway][200][Prepare] - System user authentication with plain password
        ...     username=${${output_user_name}}
        ...     password=${initial_password}
        ...     output_token=admin_access_token
    [Common][Prepare] - Access token headers
        ...     access_token=${admin_access_token}
        ...     output=admin_headers
    [Common] - Set variable     name=${output_password}     value=${setup_password_robot}
    ${encrypted_password}       rsa encrypt         ${${output_password}}     ${system_user_rsa_public_key}     ${False}
    [System_User][Prepare] - Change system user password - body
        ...     $.old_password=${encrypted_initial_password}
        ...     $.new_password=${encrypted_password}
    [System_User][200] - Change system user password
        ...     headers=${admin_headers}
        ...     body=${body}
    [Api_Gateway][200][Prepare] - System user authentication with plain password
        ...     username=${${output_user_name}}
        ...     password=${setup_password_robot}
        ...     output_token=suite_admin_access_token
    [Common][Prepare] - Access token headers
        ...     access_token=${suite_admin_access_token}
        ...     output=SUITE_ADMIN_HEADERS

[System_User][200][Prepare] - Create system user without role
    [Arguments]
        ...     ${headers}=${SUITE_ADMIN_HEADERS}
        ...     ${output_user_id}=test_system_user_id
        ...     ${output_user_name}=test_system_user_name
        ...     ${output_password}=test_system_user_password
    ${name}    generate random string   10     [LETTERS]
    [Common] - Set variable     name=${output_user_name}        value=user_${name}
    [Common] - Set variable     name=${output_password}     value=${setup_password_robot}
    ${encrypted_password}       rsa encrypt         ${${output_password}}     ${system_user_rsa_public_key}     ${False}
    [System_User][Prepare] - Create system user - body
        ...     $.username=${${output_user_name}}
        ...     $.password=${encrypted_password}
        ...     $.firstname=${name} firstName
        ... 	$.lastname=${name} lastName
        ...     $.email=${name}@email.com
        ...     $.middle_name=${name}_middleNname
        ...     $.is_external=true
    [System_User][200] - Create system user
        ...     headers=${headers}
        ...     body=${body}
    [Common][Extract] - ID
        ...     output=${output_user_id}

[System_User][200][Prepare] - link system user and company profile
    [Arguments]   ${user_id}  ${headers}=${SUITE_ADMIN_HEADERS}   &{arg_dic}
    [System_User][Prepare] - Link system user and company profile - body
    ...     &{arg_dic}
    [System_User][200] - link system user and company profile
    ...     headers=${headers}
    ...     body=${body}
    ...     user_id=${user_id}

