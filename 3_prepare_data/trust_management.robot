*** Settings ***
Resource         ../2_api/3_reuse_function/imports.robot
Resource         ../3_prepare_data/imports.robot

*** Keywords ***
[Trust_Management][200][Prepare] - generate trust token
    [Arguments]    ${output_token}=test_token     ${headers}=${SUITE_ADMIN_HEADERS}   &{arg_dic}
    [Trust_Management][Prepare] - generate trust token - body
    ...     &{arg_dic}
    [Trust_Management][200] - generate trust token
    ...     headers=${headers}
    ...     body=${body}
    [Common][Extract] - token
    ...     output=${output_token}

[Payment][200][Prepare] - create trust order
    [Arguments]     ${ouput_trust_order_id}=test_trust_order_id    ${headers}=${SUITE_AGENT_HEADERS}   &{arg_dic}
    [Trust_Management][Prepare] - create trust order - body
    ...     &{arg_dic}
    [Trust_Management][200] - create trust order
    ...     headers=${headers}
    ...     body=${body}
    [common][extract] - order_id
    ...     output=${ouput_trust_order_id}

[Payment][200][Prepare] - Execute trust order
    [Arguments]   ${order_id}  ${headers}=${SUITE_AGENT_HEADERS}   &{arg_dic}
    [Trust_Management][Prepare] - execute trust order - body
    ...     &{arg_dic}
    [Trust_Management][200] - execute trust order
    ...     headers=${headers}
    ...     body=${body}
    ...     order_id=${order_id}

[Payment][400][Prepare] - Execute trust order
    [Arguments]  ${status_code}  ${status_message}     ${order_id}  ${headers}=${SUITE_AGENT_HEADERS}   &{arg_dic}
    [Trust_Management][Prepare] - execute trust order - body
    ...     &{arg_dic}
    [Trust_Management][400] - execute trust order
    ...     headers=${headers}
    ...     body=${body}
    ...     order_id=${order_id}
    rest.string    $.status.code       ${status_code}
    rest.string    $.status.message       ${status_message}
