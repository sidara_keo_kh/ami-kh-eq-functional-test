*** Settings ***
Resource         ../2_api/3_reuse_function/imports.robot

*** Keywords ***
[Api_Gateway][200][Prepare] - System user authentication with plain password
    [Arguments]
        ...     ${username}
        ...     ${password}
        ...     ${client_id}=${setup_admin_client_id}
        ...     ${client_secret}=${setup_admin_client_secret}
        ...     ${output_token}=test_access_token
    ${encrypted_password}     rsa encrypt     ${password}     ${system_user_rsa_public_key}     ${True}
    [Common][Prepare] - Client headers
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content_type=${CONTENT_TYPE_FORM}
        ...     output=login_headers
    [Api_Gateway][Prepare] - System user authentication - params
        ...     username=${username}
        ...     password=${encrypted_password}
        ...     grant_type=password
        ...     client_id=${setup_admin_client_id}
    [Api_Gateway][200] - System user authentication
        ...     headers=${login_headers}
        ...     params=${params}
    [Api Gateway][Extract] Authentication - access token
        ...     output=${output_token}

[Api_Gateway][200][Prepare] - Agent authentication with plain password
    [Arguments]
        ...     ${username}
        ...     ${password}
        ...     ${client_id}=${setup_admin_client_id}
        ...     ${client_secret}=${setup_admin_client_secret}
        ...     ${output_token}=test_access_token
    ${encrypted_password}     rsa encrypt     ${password}     ${agent_rsa_public_key}     ${True}
    [Common][Prepare] - Client headers
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content_type=${CONTENT_TYPE_FORM}
        ...     output=login_headers
    [Api_Gateway][Prepare] - Agent authentication - params
        ...     username=${username}
        ...     password=${encrypted_password}
        ...     grant_type=password
        ...     client_id=${client_id}
    [Api_Gateway][200] - Agent authentication
        ...     headers=${login_headers}
        ...     params=${params}
    [Api Gateway][Extract] Authentication - access token
        ...     output=${output_token}