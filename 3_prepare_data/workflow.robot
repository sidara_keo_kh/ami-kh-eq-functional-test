*** Settings ***
Resource         ../2_api/3_reuse_function/imports.robot
Resource         ../3_prepare_data/imports.robot

*** Keywords ***
[workflow][200][prepare] - create adjustment order
    [Arguments]     ${output_reference_id}=test_reference_id    ${headers}=${SUITE_ADMIN_HEADERS}   &{arg_dic}
    [Workflow][Prepare] - create adjustment order - body
        ...     &{arg_dic}
    [Workflow][200] - create adjustment order
        ...     headers=${headers}
        ...     body=${body}
    [Workflow][Extract] - create order - reference_id
        ...     output_reference_id=${output_reference_id}

[Workflow][400][Prepare] - Execute adjustment order
    [Arguments]   ${status_code}  ${status_message}     ${order_id}      ${headers}=${SUITE_ADMIN_HEADERS}
    [Workflow][400] - execute adjustment order
    ...     headers=${headers}
    ...     reference_id=${order_id}
    rest.string    $.status.code       ${status_code}
    rest.string    $.status.message       ${status_message}
