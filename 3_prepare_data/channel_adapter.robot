*** Settings ***
Resource         ../2_api/3_reuse_function/imports.robot
Resource         ../3_prepare_data/imports.robot

*** Keywords ***
[Channel_Adapter][200][Prepare] - Get hierarchy list by agent id
    [Arguments]     ${headers}
    [Channel_Adapter][200] - Get hierarchy list by agent id
            ...     ${headers}

    [Channel_Adapter][Extract] - Get hierarchy list by agent id