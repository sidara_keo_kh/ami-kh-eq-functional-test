*** Settings ***
Resource         ../2_api/3_reuse_function/imports.robot

*** Keywords ***
[Suite] Local Portal System - Prepare data to create embbeded system
    ${embbeded_system_name}             generate random string      10  [LOWER]
    set suite variable       ${suite_embbeded_system_name}              ${embbeded_system_name}
    set suite variable       ${suite_embbeded_system_login_url}         http://${embbeded_system_name}.com/login
    set suite variable       ${suite_embbeded_system_logout_url}        http://${embbeded_system_name}.com/logout

[Test] Local Portal System - Prepare data to create embbeded system
    ${embbeded_system_name}             generate random string      10  [LOWER]
    set test variable       ${test_embbeded_system_name}              ${embbeded_system_name}
    set test variable       ${test_embbeded_system_login_url}         http://${embbeded_system_name}.com/login
    set test variable       ${test_embbeded_system_logout_url}        http://${embbeded_system_name}.com/logout