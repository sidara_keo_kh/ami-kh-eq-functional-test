*** Settings ***
Resource         ../../2_api/3_reuse_function/imports.robot

*** Keywords ***

[Suite][200] prepare a Prepaid Card History record
    [Customer][Suite][200] - Create customer with currency    currency=VND
    [Suite][200] create virtual prepaid card   ${suite_customer_access_token}
    [Suite][200] search card history on report by user_id

[Test][200] prepare a Prepaid Card History record with Card Not Found error
    [Customer][Test][200] - create customer in "VND" currency
    set test variable   ${test_customer_access_token_1}       ${test_customer_access_token}
    [Test][200] create virtual prepaid card     ${test_customer_access_token_1}
    [Customer][Test][200] - create customer in "VND" currency
    set test variable   ${test_customer_access_token_2}       ${test_customer_access_token}
    [Test][Fail] stop prepaid card       ${test_customer_access_token_2}     ${test_card_id}     true   400


[Suite][200] create virtual prepaid card
    [Arguments]     ${access_token}
    ${firstname}   generate random string  10    [LETTERS]
    ${lastname}    generate random string  10    [LETTERS]
    ${card_type_id}   set variable      1
    ${card_lifetime_in_month}   set variable      12
    set suite variable       ${suite_firstname}          ${firstname}
    set suite variable       ${suite_lastname}           ${lastname}
    set suite variable       ${suite_card_type_id}       ${card_type_id}
    set suite variable       ${suite_card_lifetime_in_month}       ${card_lifetime_in_month}
    ${arg_dic}     create dictionary       firstname=${suite_firstname}     lastname=${suite_lastname}
    ...     access_token=${access_token}
    ...     card_type_id=${suite_card_type_id}
    ...     card_lifetime_in_month=${suite_card_lifetime_in_month}

    ${dic}      [200] API create virtual prepaid card       ${arg_dic}
    set suite variable      ${suite_card_id}        ${dic.id}
    set suite variable      ${suite_external_identifier}        ${dic.external_identifier}
    set suite variable      ${suite_pan}        ${dic.pan}
    set suite variable      ${suite_is_stopped}        ${dic.is_stopped}
    set suite variable      ${suite_expiry_date}        ${dic.expiry_date}
    set suite variable      ${suite_created_date}        ${dic.created_timestamp}

[Suite][200] Customer create virtual prepaid card
    [Suite][200] create virtual prepaid card        ${suite_customer_access_token}
[Test][200] Customer create virtual prepaid card
    [Test][200] create virtual prepaid card        ${suite_customer_access_token}

[Test][200] create virtual prepaid card
    [Arguments]    ${access_token}
    ${firstname}   generate random string  10    [LETTERS]
    ${lastname}    generate random string  10    [LETTERS]
    ${card_type_id}   set variable      1
    ${card_lifetime_in_month}   set variable      12
    set test variable       ${test_firstname}          ${firstname}
    set test variable       ${test_lastname}           ${lastname}
    set test variable       ${test_card_type_id}       ${card_type_id}
    set test variable       ${test_card_lifetime_in_month}       ${card_lifetime_in_month}
    ${arg_dic}     create dictionary       firstname=${test_firstname}     lastname=${test_lastname}
    ...     access_token=${access_token}
    ...     card_type_id=${test_card_type_id}
    ...     card_lifetime_in_month=${test_card_lifetime_in_month}

    ${dic}      [200] API create virtual prepaid card       ${arg_dic}
    set test variable      ${test_card_id}        ${dic.id}
    set test variable      ${test_external_identifier}        ${dic.external_identifier}

[Suite][200] search card history on report by user_id
    ${arg_dic}      create dictionary      access_token=${suite_customer_access_token}      user_id=${suite_customer_id}
    ${dic}      [200] API search card history on report     ${arg_dic}
    set suite variable      ${suite_trans_id}       ${dic.trans_id}

[Test][Fail] stop prepaid card
    [Arguments]     ${access_token}     ${card_id}      ${is_stopped}    ${code}
    ${arg_dic}      create dictionary       access_token=${access_token}    card_id=${card_id}    is_stopped=${is_stopped}       code=${code}
    [Fail] API stop prepaid card     ${arg_dic}

[Suite][200] create payroll card
    [Arguments]     ${access_token}     ${user_id}     ${user_type}
    ${card_identifier}       generate random string    10   123456789
    ${lastname}       generate random string    30  [LETTERS]
    ${arg_dic}  create dictionary   access_token=${access_token}    card_identifier=${card_identifier}     card_type_id=2   card_lifetime_in_month=12    user_type=${user_type}     user_id=${user_id}
    ${dic}      [200] API create payroll card       ${arg_dic}
    ${card_id}=        get from dictionary     ${dic}          id
    ${str_card_id}=         Convert To String         ${card_id}
    set suite variable       ${suite_payroll_card_id}         ${str_card_id}
    set suite variable       ${suite_card_identifier}         ${card_identifier}
    set suite variable       ${suite_payroll_card_number}         ${card_identifier}

[Suite][200] create payroll card for prepared customer
    [Suite][200] create payroll card    ${suite_admin_access_token}     ${suite_customer_id}       ${suite_customer_type}

[Test][200] create payroll card for prepared customer
    ${card_identifier}       generate random string    10   123456789
    ${lastname}       generate random string    30  [LETTERS]
    ${arg_dic}  create dictionary   access_token=${test_admin_access_token}    card_identifier=${card_identifier}     card_type_id=2   card_lifetime_in_month=12    user_type=${test_customer_type}     user_id=${test_customer_id}
    ${dic}      [200] API create payroll card       ${arg_dic}
    ${card_id}=        get from dictionary     ${dic}          id
    ${str_card_id}=         Convert To String         ${card_id}
    set suite variable       ${test_payroll_card_id}         ${str_card_id}
    set suite variable       ${test_card_identifier}         ${card_identifier}
    set suite variable       ${test_payroll_card_number}         ${card_identifier}