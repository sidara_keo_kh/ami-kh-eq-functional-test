*** Settings ***
Resource         ../../2_api/3_reuse_function/imports.robot

*** Keywords ***
[Inventory][Prepare][200] - Create single item
    [Arguments]    ${output}=test    &{arg_dic}
    [Documentation]    by default    -    type=Physical,    has_serial_number=true
    [Common] - Generate a random string    output=${output}_random_string
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    name    ${test_id}_${${output}_random_string}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    type    Physical
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    has_serial_number    true
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    description    Create single item
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    image_url    http://single-item-image.url
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    local_sku    SI${${output}_random_string}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    upc    UPC${${output}_random_string}
    [Common][Prepare] - Access token headers    ${suite_setup_admin_access_token}
    [Inventory][Prepare] - Create body
    ...    name=${arg_dic.name}
    ...    type=${arg_dic.type}
    ...    has_serial_number=${arg_dic.has_serial_number}
    ...    description=${arg_dic.description}
    ...    image_url=${arg_dic.image_url}
    ...    local_sku=${arg_dic.local_sku}
    ...    upc=${arg_dic.upc}
    [Inventory][200] - Create single item
    ...    headers=${headers}
    ...    body=${body}
    [Common][Extract] - ID    output=${output}_id
    [Inventory][200] - Get single item
    ...    headers=${headers}
    ...    item_id=${${output}_id}
    [Inventory][Extract] - Response body data    output_prefix=${output}

[Inventory][Prepare][200] - Create single item with serial
    [Arguments]    ${output}=test    &{arg_dic}
    [Documentation]    by default    -    count=2
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    count    2
    [Inventory][Prepare][200] - Create single item    ${output}    &{arg_dic}
    :FOR    ${i}    IN RANGE    0    ${arg_dic.count}
    \    [Common] - Set default value for keyword in dictionary    ${arg_dic}    serial_number_${i+1}    ${test_id}_SN_${${output}_random_string}_${i+1}
    \    [Common] - Set variable       name=${output}_serial_number_${i+1}      value=${arg_dic.serial_number_${i+1}}
    \    [Inventory][Prepare] - Create body    serial_number=${arg_dic.serial_number_${i+1}}
    \    [Inventory][200] - Create single item serial    headers=${headers}    body=${body}    item_id=${${output}_id}

[Inventory][Prepare][200] - Create single item with quantity
    [Arguments]    ${output}=test    &{arg_dic}
    [Documentation]    by default    -    available_quantity=2
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    type    Digital
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    has_serial_number    false
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    available_quantity    2
    [Inventory][Prepare][200] - Create single item    ${output}    &{arg_dic}
    [Common] - Set variable       name=${output}_available_quantity    value=${arg_dic.available_quantity}
    [Inventory][Prepare] - Create body    available_quantity=${arg_dic.available_quantity}
    [Inventory][200] - Update single item quantity    headers=${headers}    body=${body}    item_id=${${output}_id}

[Inventory][Prepare][200] - Create group item
    [Arguments]    ${output}=test_group    &{arg_dic}
    [Documentation]    by default    -    create single item with 1 serial and single item with 1 available quantity
    [Common] - Generate a random string    output=${output}_random_string
    [Common][Prepare] - Access token headers    ${suite_setup_admin_access_token}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    name    ${test_id}_${${output}_random_string}
    ${has_items}   run keyword and return status       Dictionary Should Contain Key     ${arg_dic}          items
    run keyword unless    '${has_items}'=='True'    [Inventory][Prepare][200] - Create single item with serial    output=1_test_item    count=1
    run keyword unless    '${has_items}'=='True'    [Inventory][Prepare][200] - Create single item with quantity    output=2_test_item    available_quantity=1
    ${item_1}    run keyword unless    '${has_items}'=='True'    create dictionary    sku=${1_test_item_sku}    quantity=1
    ${item_2}    run keyword unless    '${has_items}'=='True'    create dictionary    sku=${2_test_item_sku}    quantity=1
    ${items}    run keyword unless    '${has_items}'=='True'    create list    ${item_1}    ${item_2}
    run keyword unless    '${has_items}'=='True'    [Common] - Set default value for keyword in dictionary    ${arg_dic}    items    ${items}
    [Inventory][Prepare] - Create body
    ...    name=${arg_dic.name}
    ...    items=${arg_dic['items']}
    [Inventory][200] - Create group item    headers=${headers}    body=${body}
    [Common][Extract] - ID    output=${output}_id
    [Inventory][200] - Get group item    headers=${headers}    group_id=${${output}_id}
    [Inventory][Extract] - Response body data    output_prefix=${output}

[Inventory][Prepare][200] - Create ticket
    [Arguments]    ${output}=test_ticket    &{arg_dic}
    [Documentation]    by default    -    create single item with 1 serial and single item with 1 available quantity
    [Common] - Generate a random string    output=${output}_random_string
    [Common][Prepare] - Access token headers    ${suite_setup_admin_access_token}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    ticket_type    Bind
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    receiver_id    1
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    receiver_type    Agent
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    receiver_name    ${test_id}_receiver_${${output}_random_string}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    creator_id    1
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    creator_type    System user
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    creator_name    ${test_id}_creator_${${output}_random_string}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    creator_note    Create ticket
    ${has_ticket_items}   run keyword and return status       Dictionary Should Contain Key     ${arg_dic}          ticket_items
    run keyword unless    '${has_ticket_items}'=='True'    [Inventory][Prepare][200] - Create single item with serial    output=1_test_item    count=1
    run keyword unless    '${has_ticket_items}'=='True'    [Inventory][Prepare][200] - Create single item with quantity    output=2_test_item    available_quantity=1
    ${item_serial_1}    run keyword unless    '${has_ticket_items}'=='True'    create dictionary    serial_number=${1_test_item_serial_number_1}
    ${item_serials_1}    run keyword unless    '${has_ticket_items}'=='True'    create list    ${item_serial_1}
    ${ticket_item_1}    run keyword unless    '${has_ticket_items}'=='True'    create dictionary    id=${1_test_item_id}    item_serials=${item_serials_1}
    run keyword if    '${has_ticket_items}'=='False' and '${arg_dic.ticket_type}' == 'Sell'    set to dictionary    ${ticket_item_1}    unit_price=1234567890.12345    additional_price=0.1
    ${ticket_item_2}    run keyword unless    '${has_ticket_items}'=='True'    create dictionary    id=${2_test_item_id}    quantity=1
    run keyword if    '${has_ticket_items}'=='False' and '${arg_dic.ticket_type}' == 'Sell'    set to dictionary    ${ticket_item_2}    unit_price=0    additional_price=0
    ${ticket_items}    run keyword unless    '${has_ticket_items}'=='True'    create list    ${ticket_item_1}    ${ticket_item_2}
    run keyword unless    '${has_ticket_items}'=='True'    [Common] - Set default value for non-string keyword in dictionary    ${arg_dic}    ticket_items    ${ticket_items}
    [Inventory][Prepare] - Create body
    ...    ticket_type=${arg_dic.ticket_type}
    ...    receiver_id=${arg_dic.receiver_id}
    ...    receiver_type=${arg_dic.receiver_type}
    ...    receiver_name=${arg_dic.receiver_name}
    ...    creator_id=${arg_dic.creator_id}
    ...    creator_type=${arg_dic.creator_type}
    ...    creator_name=${arg_dic.creator_name}
    ...    creator_note=${arg_dic.creator_note}
    ...    ticket_items=${arg_dic.ticket_items}
    [Inventory][200] - Create ticket    headers=${headers}    body=${body}
    [Common][Extract] - ID    output=${output}_id
    [Inventory][200] - Get ticket    headers=${headers}    ticket_id=${${output}_id}
    [Inventory][Extract] - Response body data    output_prefix=${output}

[Inventory][Prepare][200] - Create bind ticket with serial and non-serial
    ${random_string}    generate random string    10    [LETTERS]
    [Inventory][Prepare][200] - Create single item with serial    output=1_test_item
    [Inventory][Prepare][200] - Create single item with quantity    output=2_test_item
    set test variable    ${test_receiver_id}    1
    set test variable    ${test_receiver_name}    ${test_id}_receiver_${random_string}
    set test variable    ${test_receiver_type}    Agent
    set test variable    ${test_creator_id}    1
    set test variable    ${test_creator_name}    ${test_id}_creator_${random_string}
    set test variable    ${test_creator_type}    System user
    set test variable    ${test_ticket_type}    Bind
    set test variable    ${test_creator_note}    Create binding ticket with and without serial
    set test variable    ${2_test_item_quantity}    1
    ${arg_dic}    create dictionary
    ...    access_token=${suite_setup_admin_access_token}
    ...    ticket_type=${test_ticket_type}
    ...    receiver_id=${test_receiver_id}
    ...    receiver_type=${test_receiver_type}
    ...    receiver_name=${test_receiver_name}
    ...    creator_id=${test_creator_id}
    ...    creator_type=${test_creator_type}
    ...    creator_name=${test_creator_name}
    ...    creator_note=${test_creator_note}
    ...    item_id_1=${1_test_item_id}
    ...    item_serial_number_1=${1_test_item_serial_number_1}
    ...    item_id_2=${2_test_item_id}
    ...    item_quantity_2=${2_test_item_quantity}
    ${dic}    [Inventory][Reuse][200] - create ticket with serial and non-serial    ${arg_dic}
    set test variable    ${test_ticket_id}    ${dic.id}
    set test variable    ${test_ticket_status}    ${dic.ticket_status}
    set test variable    ${test_physical_item}    ${dic.physical_item}

[Inventory][Prepare][200] - Create sell ticket with serial and non-serial
    ${random_string}    generate random string    10    [LETTERS]
    [Inventory][Prepare][200] - Create single item with serial    output=1_test_item
    [Inventory][Prepare][200] - Create single item with quantity    output=2_test_item
    set test variable    ${test_receiver_id}                1
    set test variable    ${test_receiver_name}              ${test_id}_receiver_${random_string}
    set test variable    ${test_receiver_type}              Receiver type
    set test variable    ${test_creator_id}                 2
    set test variable    ${test_creator_name}               ${test_id}_creator_${random_string}
    set test variable    ${test_creator_type}               Creator type
    set test variable    ${test_ticket_type}                Sell
    set test variable    ${test_creator_note}               ${test_id}_Create selling ticket with and without serial
    set test variable    ${2_test_item_quantity}            1
    set test variable    ${1_test_item_unit_price}          1234567890.12345
    set test variable    ${1_test_item_additional_price}    0.1
    set test variable    ${2_test_item_unit_price}          0
    set test variable    ${2_test_item_additional_price}    0
    ${arg_dic}    create dictionary
    ...    access_token=${suite_setup_admin_access_token}
    ...    ticket_type=${test_ticket_type}
    ...    receiver_id=${test_receiver_id}
    ...    receiver_type=${test_receiver_type}
    ...    receiver_name=${test_receiver_name}
    ...    creator_id=${test_creator_id}
    ...    creator_type=${test_creator_type}
    ...    creator_name=${test_creator_name}
    ...    creator_note=${test_creator_note}
    ...    item_id_1=${1_test_item_id}
    ...    item_serial_number_1=${1_test_item_serial_number_1}
    ...    item_unit_price_1=${1_test_item_unit_price}
    ...    item_additional_price_1=${1_test_item_additional_price}
    ...    item_id_2=${2_test_item_id}
    ...    item_quantity_2=${2_test_item_quantity}
    ...    item_unit_price_2=${2_test_item_unit_price}
    ...    item_additional_price_2=${2_test_item_additional_price}
    ${dic}    [Inventory][Reuse][200] - create ticket with serial and non-serial    ${arg_dic}
    set test variable    ${test_ticket_id}        ${dic.id}
    set test variable    ${test_ticket_status}    ${dic.ticket_status}
    set test variable    ${test_physical_item}    ${dic.physical_item}

[Inventory][Prepare][200] - Create bind ticket with non-serial
    [Arguments]    ${item_id}    ${item_quantity}
    [Inventory][Prepare][200] - Create any ticket with non-serial    ${item_id}    ${item_quantity}    Bind

[Inventory][Prepare][200] - Create unbind ticket with non-serial
    [Arguments]    ${item_id}    ${item_quantity}
    [Inventory][Prepare][200] - Create any ticket with non-serial    ${item_id}    ${item_quantity}    Unbind

[Inventory][Prepare][200] - Create any ticket with non-serial
    [Arguments]    ${item_id}    ${item_quantity}    ${ticket_type}
    ${random_string}    generate random string    10    [LETTERS]
    set test variable    ${test_receiver_id}    1
    set test variable    ${test_receiver_name}    ${test_id}_receiver_${random_string}
    set test variable    ${test_receiver_type}    Agent
    set test variable    ${test_creator_id}    1
    set test variable    ${test_creator_name}    ${test_id}_creator_${random_string}
    set test variable    ${test_creator_type}    System user
    set test variable    ${test_ticket_type}    ${ticket_type}
    set test variable    ${test_creator_note}    Create binding ticket with non-serial
    set test variable    ${test_item_quantity}    ${item_quantity}
    ${arg_dic}    create dictionary
    ...    access_token=${suite_setup_admin_access_token}
    ...    ticket_type=${test_ticket_type}
    ...    receiver_id=${test_receiver_id}
    ...    receiver_type=${test_receiver_type}
    ...    receiver_name=${test_receiver_name}
    ...    creator_id=${test_creator_id}
    ...    creator_type=${test_creator_type}
    ...    creator_name=${test_creator_name}
    ...    creator_note=${test_creator_note}
    ...    item_id=${item_id}
    ...    item_quantity=${item_quantity}
    ${dic}    [Inventory][Reuse][200] - create ticket with non-serial    ${arg_dic}
    set test variable    ${test_ticket_id}    ${dic.id}
    set test variable    ${test_ticket_status}    ${dic.ticket_status}
    set test variable    ${test_physical_item}    ${dic.physical_item}

[Inventory][Prepare][200] - Create sell ticket with non-serial
    [Arguments]    ${item_id}    ${item_quantity}
    ${random_string}    generate random string    10    [LETTERS]
    set test variable    ${test_receiver_id}    1
    set test variable    ${test_receiver_name}    ${test_id}_receiver_${random_string}
    set test variable    ${test_receiver_type}    Agent
    set test variable    ${test_creator_id}    1
    set test variable    ${test_creator_name}    ${test_id}_creator_${random_string}
    set test variable    ${test_creator_type}    System user
    set test variable    ${test_ticket_type}    Sell
    set test variable    ${test_creator_note}    Create selling ticket with non-serial
    set test variable    ${test_item_quantity}    ${item_quantity}
    ${arg_dic}    create dictionary
    ...    access_token=${suite_setup_admin_access_token}
    ...    ticket_type=${test_ticket_type}
    ...    receiver_id=${test_receiver_id}
    ...    receiver_type=${test_receiver_type}
    ...    receiver_name=${test_receiver_name}
    ...    creator_id=${test_creator_id}
    ...    creator_type=${test_creator_type}
    ...    creator_name=${test_creator_name}
    ...    creator_note=${test_creator_note}
    ...    item_id=${item_id}
    ...    item_quantity=${item_quantity}
    ...    item_unit_price=0
    ...    item_additional_price=0
    ${dic}    [Inventory][Reuse][200] - create ticket with non-serial    ${arg_dic}
    set test variable    ${test_ticket_id}    ${dic.id}
    set test variable    ${test_ticket_status}    ${dic.ticket_status}
    set test variable    ${test_physical_item}    ${dic.physical_item}
    set test variable    ${test_receiver_name}    ${dic.receiver_name}

[Inventory][Prepare][200] - Create bind ticket with serial
    [Arguments]    ${output}=test_ticket    &{arg_dic}
    [Common] - Generate a random string    output=${output}_random_string
    [Common][Prepare] - Access token headers    ${suite_setup_admin_access_token}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    ticket_type    Bind
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    receiver_id    1
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    receiver_type    Agent
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    receiver_name    ${test_id}_receiver_${${output}_random_string}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    creator_id    1
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    creator_type    System user
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    creator_name    ${test_id}_creator_${${output}_random_string}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    creator_note    Create binding ticket with serial
    ${has_item}   run keyword and return status       Dictionary Should Contain Key     ${arg_dic}          item_id
    run keyword unless    '${has_item}'=='True'    [Inventory][Prepare][200] - Create single item with serial    output=test_item    count=1
    run keyword unless    '${has_item}'=='True'    [Common] - Set default value for keyword in dictionary    ${arg_dic}    item_id    ${test_item_id}
    run keyword unless    '${has_item}'=='True'    [Common] - Set default value for keyword in dictionary    ${arg_dic}    item_serial_number    ${test_item_serial_number_1}
    ${item_serial}      create dictionary    serial_number=${arg_dic.item_serial_number}
    ${item_serials}     create list          ${item_serial}
    ${ticket_item}      create dictionary    id=${arg_dic.item_id}    item_serials=${item_serials}
    ${ticket_items}       create list          ${ticket_item}
    [Inventory][Prepare] - Create body
    ...    ticket_type=${arg_dic.ticket_type}
    ...    receiver_id=${arg_dic.receiver_id}
    ...    receiver_type=${arg_dic.receiver_type}
    ...    receiver_name=${arg_dic.receiver_name}
    ...    creator_id=${arg_dic.creator_id}
    ...    creator_type=${arg_dic.creator_type}
    ...    creator_name=${arg_dic.creator_name}
    ...    creator_note=${arg_dic.creator_note}
    ...    ticket_items=${ticket_items}
    [Inventory][200] - Create ticket    headers=${headers}    body=${body}
    [Common][Extract] - ID    output=${output}_id
    [Inventory][200] - Get ticket    headers=${headers}    ticket_id=${${output}_id}
    [Inventory][Extract] - Response body data    output_prefix=${output}

[Inventory][Prepare][200] - Create sell ticket with serial
    [Arguments]    ${item_id}    ${item_serial_number}
    ${random_string}    generate random string    10    [LETTERS]
    set test variable    ${test_receiver_id}    1
    set test variable    ${test_receiver_name}    ${test_id}_receiver_${random_string}
    set test variable    ${test_receiver_type}    Agent
    set test variable    ${test_creator_id}    1
    set test variable    ${test_creator_name}    ${test_id}_creator_${random_string}
    set test variable    ${test_creator_type}    System user
    set test variable    ${test_ticket_type}    Sell
    set test variable    ${test_creator_note}    Create selling ticket with serial
    ${arg_dic}    create dictionary
    ...    access_token=${suite_setup_admin_access_token}
    ...    ticket_type=${test_ticket_type}
    ...    receiver_id=${test_receiver_id}
    ...    receiver_type=${test_receiver_type}
    ...    receiver_name=${test_receiver_name}
    ...    creator_id=${test_creator_id}
    ...    creator_type=${test_creator_type}
    ...    creator_name=${test_creator_name}
    ...    creator_note=${test_creator_note}
    ...    item_id=${item_id}
    ...    item_serial_number=${item_serial_number}
    ...    item_unit_price=0
    ...    item_additional_price=0
    ${dic}    [Inventory][Reuse][200] - create ticket with serial    ${arg_dic}
    set test variable    ${test_ticket_id}    ${dic.id}
    set test variable    ${test_ticket_status}    ${dic.ticket_status}
    set test variable    ${test_physical_item}    ${dic.physical_item}

[Inventory][Prepare][200] - Update ticket status with owner
    [Arguments]    ${ticket_id}    ${ticket_status}
    ${random_string}    generate random string    10    [LETTERS]
    set test variable     ${new_updater_id}         1
    set test variable     ${new_updater_type}       Agent
    set test variable     ${new_updater_name}       ${test_id}_updater__${random_string}
    set test variable     ${new_updater_note}       Update ticket status to ${ticket_status}
    set test variable     ${new_owner_id}           2
    set test variable     ${new_owner_type}         System user
    set test variable     ${new_owner_name}         ${test_id}_owner_${random_string}
    ${current_date}   Get Current Date     UTC       result_format=%Y-%m-%dT%H:%M:%SZ 
    ${arg_dic}    create dictionary
    ...    access_token=${suite_setup_admin_access_token}
    ...    ticket_id=${ticket_id}
    ...    status=${ticket_status}
    ...    updater_id=${new_updater_id}
    ...    updater_type=${new_updater_type}
    ...    updater_name=${new_updater_name}
    ...    updater_note=${new_updater_note}
    ...    owner_id=${new_owner_id}
    ...    owner_type=${new_owner_type}
    ...    owner_name=${new_owner_name}
    ...    owner_timestamp=${current_date}
    ${dic}    [Inventory][200] - API update ticket status with owner    ${arg_dic}
    [Return]    ${dic}

[Inventory][Prepared][200] - Create sell ticket with Prepared status
    [Inventory][Prepare][200] - Create single item with quantity      output=1_test_item                         available_quantity=1
    [Inventory][Prepare][200] - Create sell ticket with non-serial    ${1_test_item_id}                          1
    set test variable    ${test_ticket_id_sell}    ${test_ticket_id}
    ${api_request_info}    create dictionary
    ...    ticket_id    ${test_ticket_id_sell}
    ...    status    Prepared
    [Inventory][Ticket][200] - Update ticket status with owner    ${api_request_info}

[Inventory][Prepared][200] - Create bind ticket with Prepared status
    [Inventory][Prepare][200] - Create single item with quantity      output=2_test_item                         available_quantity=1
    [Inventory][Prepare][200] - create bind ticket with non-serial    ${2_test_item_id}                          1
    set test variable    ${test_ticket_id_bind}    ${test_ticket_id}
    ${api_request_info}    create dictionary
    ...    ticket_id    ${test_ticket_id_bind}
    ...    status    Prepared
    [Inventory][Ticket][200] - Update ticket status with owner    ${api_request_info}

[Inventory][Prepared][200] - Update ticket status
    [Arguments]    ${output}=test_ticket    &{arg_dic}
    ${random_string}    generate random string    10    [LETTERS]
    [Common][Prepare] - Access token headers    ${suite_setup_admin_access_token}
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    updater_id      1
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    updater_type    Agent
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    updater_name    ${test_id}_updater
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    updater_note    ${random_string}_Update ticket status
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    owner_id        1
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    owner_type      System user
    [Common] - Set default value for keyword in dictionary    ${arg_dic}    owner_name      ${test_id}_owner
    ${get_key_result}         Evaluate            $arg_dic.get("ticket_items", ${EMPTY})
    Set to dictionary           ${arg_dic}          ticket_items       ${get_key_result}
    ${current_date}   Get Current Date     UTC       result_format=%Y-%m-%dT%H:%M:%SZ 
    [Inventory][Prepare] - Create body
    ...    status=${arg_dic.status}
    ...    updater_id=${arg_dic.updater_id}
    ...    updater_type=${arg_dic.updater_type}
    ...    updater_name=${arg_dic.updater_name}
    ...    updater_note=${arg_dic.updater_note}
    ...    owner_id=${arg_dic.owner_id}
    ...    owner_type=${arg_dic.owner_type}
    ...    owner_name=${arg_dic.owner_name}
    ...    owner_timestamp=${current_date}
    ...    ticket_items=${arg_dic.ticket_items}
    [Inventory][200] - Update ticket status    headers=${headers}    body=${body}   ticket_id=${arg_dic.ticket_id}
