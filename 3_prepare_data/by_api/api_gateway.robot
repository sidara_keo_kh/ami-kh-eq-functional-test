*** Settings ***
Resource         ../../2_api/3_reuse_function/imports.robot

*** Keywords ***
[Test][200] API system user authentication
    [Arguments]     ${username}     ${password}
    ${arg_dic}      create dictionary   username=${username}       password=${password}
    ${dic}     [200] API system user authentication     ${arg_dic}
    ${access_token}=        get from dictionary     ${dic}          access_token
    set test variable    ${test_access_token}       ${access_token}

[Test][200] API system user authentication with plain password
    [Arguments]     ${username}     ${password}     ${output_token}=test_access_token
    ${encrypted_password}     rsa encrypt     ${password}     ${system_user_rsa_public_key}     ${True}
    [Common][Prepare] - Client headers
        ...     client_id=${setup_admin_client_id}
        ...     client_secret=${setup_admin_client_secret}
        ...     content_type=${CONTENT_TYPE_FORM}
    [Api_Gateway][Prepare] - System user authentication - params
        ...     username=${username}
        ...     password=${encrypted_password}
        ...     grant_type=password
        ...     client_id=${setup_admin_client_id}
    [Api_Gateway][200] - System user authentication
        ...     headers=${headers}
        ...     params=${params}
    [Api Gateway][Extract] Authentication - access token
        ...     output=${output_token}

[Suite][200] API customer authentication with plain password
    [Arguments]     ${username}     ${password}     ${output_token}=suite_customer_access_token
    ${encrypted_password}     rsa encrypt     ${password}     ${customer_rsa_public_key}     ${True}
    [Common][Prepare] - Client headers
        ...     client_id=${setup_admin_client_id}
        ...     client_secret=${setup_admin_client_secret}
        ...     content_type=${CONTENT_TYPE_FORM}
    [Api_Gateway][Prepare] - Customer authentication - params
        ...     username=${username}
        ...     password=${encrypted_password}
        ...     grant_type=password
        ...     client_id=${setup_admin_client_id}
    [Api_Gateway][200] - Customer authentication
        ...     headers=${headers}
        ...     params=${params}
    [Api Gateway][Extract] Authentication - access token
        ...     output=${output_token}

[Api Gateway][200] agent authentication with plain password
    [Arguments]     ${username}     ${password}     ${client_id}=${setup_admin_client_id}       ${client_secret}=${setup_admin_client_secret}       ${output_token}=test_access_token
    ${encrypted_password}     rsa encrypt     ${password}     ${agent_rsa_public_key}     ${True}
    [Common][Prepare] - Client headers
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content_type=${CONTENT_TYPE_FORM}
        ...     output=login_headers
    [Api_Gateway][Prepare] - Agent authentication - params
        ...     username=${username}
        ...     password=${encrypted_password}
        ...     grant_type=password
        ...     client_id=${client_id}
    [Api_Gateway][200] - Agent authentication
        ...     headers=${login_headers}
        ...     params=${params}
    [Api Gateway][Extract] Authentication - access token
        ...     output=${output_token}

[Test][200] Add api
    ${arg_dic}      create dictionary   username=${setup_admin_username}       password=${setup_password_admin_encrypted_utf8}
    ${dic}     [200] API system user authentication     ${arg_dic}
    ${access_token}=        get from dictionary     ${dic}          access_token

    ${name}            generate random string      10     [LETTERS]
    ${api_random}      generate random string      5     [LETTERS]
    ${pattern}      set variable        /v6/api/${api_random}/{id}
    ${http}     set variable   GET
    ${prefix}       generate random string      5     [LETTERS]
    ${arg_dic}      create dictionary    access_token=${access_token}
                                  ...    name=${name}
                                  ...    http_method=${http}
                                  ...    pattern=${pattern}
                                  ...    is_internal=true
                                  ...    is_required_access_token=true
                                  ...    service_id=1
                                  ...    prefix=${prefix}
    ${dic}  [200] API add api       ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    ${actual_name}  set variable     ${response.json()['data']['name']}
    ${actual_http_method}  set variable     ${response.json()['data']['http_method']}
    ${actual_pattern}  set variable     ${response.json()['data']['pattern']}
    ${actual_is_required_access_token}  set variable     ${response.json()['data']['is_required_access_token']}
    ${actual_service_id}  set variable     ${response.json()['data']['service']['id']}
    ${actual_prefix}  set variable     ${response.json()['data']['prefix']}

    should be equal as strings     ${name}        ${actual_name}
    should be equal as strings      ${pattern}     ${actual_pattern}
    should be equal as strings      ${prefix}        ${actual_prefix}
    should be equal as strings      ${http}        ${actual_http_method}
    should be equal as strings      true      ${actual_is_required_access_token}
    should be equal as strings      1        ${actual_service_id}

[Test][200] Create api-gateway service
    [Arguments]     ${location}     ${output_id}=test_service_id
    [Common][Prepare] - Access token headers
        ...     access_token=${suite_admin_access_token}
    [Api_Gateway][Prepare] - Create service - body
        ...     $.max_total_connection=40
        ...     $.max_per_route=20
        ...     $.timeout=120000
        ...     $.location=${location}
    [Api_Gateway][200] - Create service
        ...     headers=${headers}
        ...     body=${body}
    [Common][Extract] - ID    output=${output_id}

[Test][200] Create mock headers api
    [Arguments]
        ...     ${permission_required}
        ...     ${service_id}=${test_service_id}
        ...     ${output_id}=test_api_id
        ...     ${output_url}=test_api_url

    ${api_prefix}      generate random string  5   [LETTERS]
    [Common][Prepare] - Access token headers
        ...     access_token=${suite_admin_access_token}
    [Api_Gateway][Prepare] - Create api - body
        ...   $.http_method=GET
        ...   $.is_required_access_token=true
        ...   $.is_internal=false
        ...   $.pattern=/${api_prefix}/headers
        ...   $.service_id=${service_id}
        ...   $.permission_required=${permission_required}
    [Api_Gateway][200] - Create api
        ...     headers=${headers}
        ...     body=${body}
    [Common][Extract] - ID    output=${output_id}
    [Common] - Set variable   name=${output_url}      value=${api_gateway_context_root}/${api_prefix}/headers

[Test][200] Add client scope
    [Arguments]   ${api_id}     ${client_id}=${setup_admin_client_id}
    [Common][Prepare] - Access token headers
        ...     access_token=${suite_admin_access_token}
    [Api_Gateway][Prepare] - Add client scope - body
        ...     $.scopes=[${api_id}]
    [Api_Gateway][200] - Add client scope
        ...     headers=${headers}
        ...     body=${body}
        ...     client_id=${client_id}

[Test][200] Get api detail
    [Arguments]     &{arg_dic}
    ${dic}      [200] API get api detail        access_token=${arg_dic.access_token}    api_id=${arg_dic.api_id}
    ${response}     get from dictionary    ${dic}       response
    set test variable       ${test_service_name}   ${response.json()['data']['service']['name']}

[Test][200] Call GET api
    [Arguments]
        ...     ${path}
        ...     ${code}
        ...     ${message}
        ...     ${access_token}=${test_access_token}
    [Common][Prepare] - Access token headers
        ...     content_type=${CONTENT_TYPE_JSON}
        ...     access_token=${access_token}
    REST.get        ${api_gateway_host}/${path}
        ...     headers=${headers}
    rest extract
    REST.integer            response status     ${code}
    run keyword if      '${code}'!='200'        REST.string         $.status.message        ${message}
