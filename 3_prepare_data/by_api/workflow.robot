*** Settings ***
Resource         ../../2_api/3_reuse_function/imports.robot

*** Keywords ***
[Test][200] API create refund voucher
      [Arguments]
      ...     ${access_token}
      ...     ${voucher_id}
      ...     ${product_ref2}
      ...     ${product_ref3}
      ...     ${product_ref4}
      ...     ${product_ref5}
      ...     ${reason_for_refund}
      ...     ${original_voucher_id}
      ...     ${amount}
      ...     ${currency}
    ${dic}  [200] API create refund voucher
      ...     ${access_token}
      ...     ${voucher_id}
      ...     ${product_ref2}
      ...     ${product_ref3}
      ...     ${product_ref4}
      ...     ${product_ref5}
      ...     ${reason_for_refund}
      ...     ${original_voucher_id}
      ...     ${amount}
      ...     ${currency}
    ${voucher_refund_id}=    get from dictionary    ${dic}   voucher_refund_id
    set test variable       ${test_voucher_refund_id}    ${voucher_refund_id}

[Suite][200] API create refund voucher
      [Arguments]
      ...     ${access_token}
      ...     ${voucher_id}
      ...     ${product_ref2}
      ...     ${product_ref3}
      ...     ${product_ref4}
      ...     ${product_ref5}
      ...     ${reason_for_refund}
      ...     ${original_voucher_id}
      ...     ${amount}
      ...     ${currency}
    ${dic}  [200] API create refund voucher
      ...     ${access_token}
      ...     ${voucher_id}
      ...     ${product_ref2}
      ...     ${product_ref3}
      ...     ${product_ref4}
      ...     ${product_ref5}
      ...     ${reason_for_refund}
      ...     ${original_voucher_id}
      ...     ${amount}
      ...     ${currency}
    ${voucher_refund_id}=    get from dictionary    ${dic}   voucher_refund_id
    set suite variable       ${suite_voucher_refund_id}    ${voucher_refund_id}

[Test][200][Data] execute adjustment order
    ${arg_dic}  create dictionary   access_token=${suite_admin_access_token}    reference_id=${test_reference_id}
    [200] API execute adjustment order       ${arg_dic}

[Test][200] create balance adjustment then execute
    ${arg_dic}   create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     product_service_id=${test_service_id}
    ...     reference_order_id=123123
    ...     initiator_user_id=${test_initiator_id}
    ...     initiator_user_type_id=2
    ...     initiator_user_sof_id=${test_initiator_sof_id}
    ...     initiator_sof_type_id=2
    ...     payer_user_id=${test_payer_id}
    ...     payer_user_type_id=2
    ...     payer_user_sof_id=${test_payer_sof_id}
    ...     payer_user_sof_type_id=2
    ...     payee_user_id=${test_payee_id}
    ...     payee_user_type_id=2
    ...     payee_user_sof_id=${test_payee_sof_id}
    ...     payee_user_sof_type_id=2
    ...     amount=10000
    ${dic}  [200] API create adjustment order    ${arg_dic}
    ${test_order_id}       get from dictionary     ${dic}     order_id
    ${get_reference_id}       get from dictionary     ${dic}     reference_id
    ${execute_order_dic}     create dictionary      access_token=${test_payer_access_token}    order_id=${test_order_id}
    set test variable   ${test_order_id}      ${test_order_id}
    ${dic}        [200] API payment execute order     ${execute_order_dic}

[Test][200] create balance adjustment with requestor then execute
    ${arg_dic}   create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     product_service_id=${test_service_id}
    ...     reference_order_id=123123
    ...     requestor_user_user_id=${test_initiator_id}
    ...     requestor_user_user_type_id=2
    ...     requestor_user_user_type_name=${test_initiator_type_name}
    ...     requestor_user_sof_id=${test_initiator_sof_id}
    ...     requestor_user_sof_type_id=2
    ...     initiator_user_id=${test_initiator_id}
    ...     initiator_user_type_id=2
    ...     initiator_user_sof_id=${test_initiator_sof_id}
    ...     initiator_sof_type_id=2
    ...     payer_user_id=${test_payer_id}
    ...     payer_user_type_id=2
    ...     payer_user_sof_id=${test_payer_sof_id}
    ...     payer_user_sof_type_id=2
    ...     payee_user_id=${test_payee_id}
    ...     payee_user_type_id=2
    ...     payee_user_sof_id=${test_payee_sof_id}
    ...     payee_user_sof_type_id=2
    ...     amount=10000
    ${dic}  [200] API create adjustment order    ${arg_dic}
    ${test_order_id}       get from dictionary     ${dic}     order_id
    ${get_reference_id}       get from dictionary     ${dic}     reference_id
    ${execute_order_dic}     create dictionary      access_token=${test_payer_access_token}    order_id=${test_order_id}
    set test variable   ${test_order_id}      ${test_order_id}
    ${arg_dic}      create dictionary    access_token=${suite_admin_access_token}        reference_id=${get_reference_id}        reason=Reason for approve
    ${dic}      [200] API execute adjustment order  ${arg_dic}

    ${arg_dic}  create dictionary   access_token=${suite_admin_access_token}        reference_id=${get_reference_id}
    ${dic}      [200] API get all balance adjustments   ${arg_dic}
    ${response}     get from dictionary     ${dic}      response
    set test variable   ${test_apporoved_user_id}     ${response.json()['data']['balance_adjustment_reference'][0]['approved_user']['user_id']}

[Test][200] create balance adjustment and not yet execute order
    ${batch_code}    generate random string             10       [NUMBERS]
    ${reference_order_id}   generate random string      10      [NUMBERS]

    ${arg_dic}   create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     product_service_id=${suite_service_id}
    ...     reference_order_id=${reference_order_id}
    ...     requestor_user_user_id=${suite_initiator_id}
    ...     requestor_user_user_type_id=2
    ...     requestor_user_user_type_name=${suite_initiator_type_name}
    ...     requestor_user_sof_id=${suite_initiator_sof_id}
    ...     requestor_user_sof_type_id=2
    ...     initiator_user_id=${suite_initiator_id}
    ...     initiator_user_type_id=2
    ...     initiator_user_sof_id=${suite_initiator_sof_id}
    ...     initiator_sof_type_id=2
    ...     payer_user_id=${suite_payer_id}
    ...     payer_user_type_id=2
    ...     payer_user_sof_id=${suite_payer_sof_id}
    ...     payer_user_sof_type_id=2
    ...     payee_user_id=${suite_payee_id}
    ...     payee_user_type_id=2
    ...     payee_user_sof_id=${suite_payee_sof_id}
    ...     payee_user_sof_type_id=2
    ...     amount=10000
    ...     reference_service_id=${suite_service_id}
    ...     reference_service_group_id=${suite_service_group_id}
    ...     batch_code=${batch_code}

    ${dic}  [200] API create adjustment order    ${arg_dic}
    ${test_order_id}         get from dictionary     ${dic}     order_id
    ${get_reference_id}       get from dictionary     ${dic}     reference_id
    ${execute_order_dic}      create dictionary      access_token=${suite_payer_access_token}    order_id=${test_order_id}

    set test variable   ${test_order_id}              ${test_order_id}
    set test variable   ${test_reference_id}          ${get_reference_id}
    set test variable   ${test_batch_code}            ${batch_code}
    set test variable   ${test_reference_order_id}    ${reference_order_id}

[Test][200] create balance adjustment and execute order
    ${batch_code}    generate random string             10       [NUMBERS]
    ${reference_order_id}   generate random string      10      [NUMBERS]

    ${arg_dic}   create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     product_service_id=${suite_service_id}
    ...     reference_order_id=${reference_order_id}
    ...     requestor_user_user_id=${suite_initiator_id}
    ...     requestor_user_user_type_id=2
    ...     requestor_user_user_type_name=${suite_initiator_type_name}
    ...     requestor_user_sof_id=${suite_initiator_sof_id}
    ...     requestor_user_sof_type_id=2
    ...     initiator_user_id=${suite_initiator_id}
    ...     initiator_user_type_id=2
    ...     initiator_user_sof_id=${suite_initiator_sof_id}
    ...     initiator_sof_type_id=2
    ...     payer_user_id=${suite_payer_id}
    ...     payer_user_type_id=2
    ...     payer_user_sof_id=${suite_payer_sof_id}
    ...     payer_user_sof_type_id=2
    ...     payee_user_id=${suite_payee_id}
    ...     payee_user_type_id=2
    ...     payee_user_sof_id=${suite_payee_sof_id}
    ...     payee_user_sof_type_id=2
    ...     amount=10000
    ...     reference_service_id=${suite_service_id}
    ...     reference_service_group_id=${suite_service_group_id}
    ...     batch_code=${batch_code}

     ${dic}  [200] API create adjustment order    ${arg_dic}
    ${test_order_id}         get from dictionary     ${dic}     order_id
    ${get_reference_id}       get from dictionary     ${dic}     reference_id
    ${execute_order_dic}      create dictionary      access_token=${suite_payer_access_token}    order_id=${test_order_id}

    set test variable   ${test_order_id}              ${test_order_id}
    set test variable   ${test_reference_id}          ${get_reference_id}
    set test variable   ${test_batch_code}            ${batch_code}
    set test variable   ${test_reference_order_id}    ${reference_order_id}

    ${arg_dic}      create dictionary    access_token=${suite_admin_access_token}        reference_id=${get_reference_id}        reason=Reason for approve
    ${dic}      [200] API execute adjustment order   ${arg_dic}

[Test][200] Create balance adjustment with setup service to create debt_balance
    ${reference_order_id}     generate random string  12    [NUMBERS]
    ${amount}           generate random string  3     123456789
    set test variable       ${order_amount}     ${amount}000
    ${arg_dic}   create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     product_service_id=${suite_service_id}
    ...     reference_order_id=${reference_order_id}
    ...     initiator_user_id=${test_agent_id}
    ...     initiator_user_type_id=2
    ...     initiator_user_sof_id=${test_agent_sof_id_VN}
    ...     initiator_sof_type_id=2
    ...     payer_user_id=${test_agent_id}
    ...     payer_user_type_id=2
    ...     payer_user_sof_id=${test_agent_sof_id_VN}
    ...     payer_user_sof_type_id=2
    ...     payee_user_id=${suite_setup_agent_id}
    ...     payee_user_type_id=2
    ...     payee_user_sof_id=${suite_setup_agent_sof_id_VN}
    ...     payee_user_sof_type_id=2
    ...     amount=${order_amount}
    ${dic}  [200] API create adjustment order    ${arg_dic}
    ${test_order_id}       get from dictionary     ${dic}     order_id
    ${get_reference_id}       get from dictionary     ${dic}     reference_id
    ${execute_order_dic}     create dictionary      access_token=${suite_admin_access_token}    order_id=${test_order_id}
    set test variable       ${test_debt_order_id}        ${test_order_id}
    set test variable       ${debt_amount}          ${order_amount}
    ${dic}        [200] API payment execute order     ${execute_order_dic}

[Test] Save first balance adjustment to create debt
    set test variable       ${test_first_debt_order_id}        ${test_debt_order_id}
    set test variable       ${test_first_debt_amount}          ${debt_amount}

[Test] Save second balance adjustment to create debt
    set test variable       ${test_second_debt_order_id}        ${test_debt_order_id}
    set test variable       ${test_second_debt_amount}          ${debt_amount}

[Test][200] Create balance adjustment to resolve debt_balance by Setup agent
    ${reference_order_id}     generate random string  12    [NUMBERS]
    ${arg_dic}   create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     product_service_id=${suite_fund_in_service_id}
    ...     reference_order_id=${reference_order_id}
    ...     initiator_user_id=${suite_setup_agent_id}
    ...     initiator_user_type_id=2
    ...     initiator_user_sof_id=${suite_setup_agent_sof_id_VN}
    ...     initiator_sof_type_id=2
    ...     payer_user_id=${suite_setup_agent_id}
    ...     payer_user_type_id=2
    ...     payer_user_sof_id=${suite_setup_agent_sof_id_VN}
    ...     payer_user_sof_type_id=2
    ...     payee_user_id=${test_agent_id}
    ...     payee_user_type_id=2
    ...     payee_user_sof_id=${test_agent_sof_id_VN}
    ...     payee_user_sof_type_id=2
    ...     amount=${debt_amount}
    ${dic}  [200] API create adjustment order    ${arg_dic}
    ${test_order_id}       get from dictionary     ${dic}     order_id
    ${get_reference_id}       get from dictionary     ${dic}     reference_id
    ${execute_order_dic}     create dictionary      access_token=${suite_admin_access_token}    order_id=${test_order_id}
    set test variable       ${test_resolve_order_id}        ${test_order_id}
    ${dic}        [200] API payment execute order     ${execute_order_dic}
    