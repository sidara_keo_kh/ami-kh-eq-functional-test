*** Settings ***
Resource         ../../2_api/3_reuse_function/imports.robot

*** Keywords ***
[Suite][200] generate trust token between truster and trusted agent
    ${arg_dic}      create dictionary       access_token=${suite_admin_access_token}    truster_user_id=${suite_water_agent_id}
    ...     truster_user_type_id=2      trusted_user_id=${suite_trusted_agent_id}       trusted_user_type_id=2
    ...     expired_unit=year       expired_duration=2
    ${return_dic}   [200] API generate trust token      ${arg_dic}
    set suite variable     ${suite_trust_token}     ${return_dic.token}

[Trust_Management][Test][200] generate trust token between truster and trusted agent
    [Arguments]  &{arg_dic}
    ${arg_dic_1}      create dictionary
    ...     access_token=${arg_dic.access_token}
    ...     truster_user_id=${test_truster_id}
    ...     truster_user_type_id=2
    ...     trusted_user_id=${test_trusted_id}
    ...     trusted_user_type_id=2
    ...     expired_unit=year
    ...     expired_duration=2
    ${return_dic}   [200] API generate trust token      ${arg_dic_1}
    set test variable     ${test_trust_token}     ${return_dic.token}

[Test] Create trust order then execute
    ${ext_transaction_id}     generate random string  20    Robot_[LETTERS]
    ${arg_dic}      create dictionary
    ...     access_token=${test_trusted_access_token}
    ...     ext_transaction_id=${ext_transaction_id}
    ...     trust_token=${test_trust_token}
    ...     product_service_id=${test_service_id}
    ...     product_service_name=${test_service_name}
    ...     requestor_user_user_id=${test_trusted_id}
    ...     requestor_user_user_type=agent
    ...     requestor_user_sof_id=${test_trusted_sof_id}
    ...     requestor_user_sof_type_id=2
    ...     payer_user_id=${test_truster_id}
    ...     payer_user_type_name=agent
    ...     payer_sof_id=${test_truster_sof_id}
    ...     payer_sof_type_id=2
    ...     payee_user_id=${test_payee_id}
    ...     payee_user_type_name=agent
    ...     payee_sof_id=${test_payee_sof_id}
    ...     payee_sof_type_id=2
    ...     amount=10000
    ${dic}  [200] API create trust order    ${arg_dic}
    ${response}=    get from dictionary    ${dic}   response
    ${test_order_id}=  get from dictionary   ${dic}   order_id
    set test variable  ${test_order_id}     ${test_order_id}
    ${execute_order_dic}     create dictionary      access_token=${suite_admin_access_token}    order_id=${test_order_id}
    ${dic}      [200] API payment execute order     ${execute_order_dic}

[Trust_Management][Test][Fail] Generate trust token between truster and trusted agent
    [Arguments]  &{arg_dic}
    ${arg_dic_1}      create dictionary
    ...     access_token=${arg_dic.access_token}
    ...     truster_user_id=${test_truster_id}
    ...     truster_user_type_id=2
    ...     trusted_user_id=${test_trusted_id}
    ...     trusted_user_type_id=2
    ...     expired_unit=year
    ...     expired_duration=2
    ...     code=${arg_dic.code}
    ...     status_code=${arg_dic.status_code}
    ...     status_message=${arg_dic.status_message}
    ${return_dic}   [Fail] API generate trust token      ${arg_dic_1}

[Trust_Management][Test][Fail] Delete trust token by Id
    [Arguments]  &{arg_dic}
    ${arg_dic_1}      create dictionary
    ...     access_token=${arg_dic.access_token}
    ...     id=${arg_dic.id}
    ...     code=${arg_dic.code}
    ...     status_code=${arg_dic.status_code}
    ...     status_message=${arg_dic.status_message}
    ${return_dic}   [Fail] API delete trust token     ${arg_dic_1}

[Trust_Management][Test][200] Delete trust token by Id
    [Arguments]  &{arg_dic}
    ${arg_dic_1}      create dictionary
    ...     access_token=${arg_dic.access_token}
    ...     id=${arg_dic.id}
    ${return_dic}   [200] API delete trust token     ${arg_dic_1}

[Trust_Management][Test][200] Trusted agent can see their token list
    [Arguments]  &{arg_dic}
    ${arg_dic_1}      create dictionary
    ...     access_token=${arg_dic.access_token}
    ...     trust_role=${arg_dic.trust_role}
    ${return_dic}   [200] API trusted agent can see their token list     ${arg_dic_1}
    set test variable     ${test_trust_token_id}     ${return_dic.id}