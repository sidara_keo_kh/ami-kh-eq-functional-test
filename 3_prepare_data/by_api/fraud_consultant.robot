*** Settings ***
Resource         ../../2_api/3_reuse_function/imports.robot

*** Keywords ***
[Fraud_Consultant][Reuse][Data] - create customer info dictionary
    ${customer_unique_reference}    generate random string      15          [LOWER][NUMBERS]
    ${username}     generate random string  13  [LETTERS]
    ${dic}  create dictionary
    ...     unique_reference=${customer_unique_reference}
    ...     identity_type_id=1
    ...     username=${username}
    ...     password=${setup_password_customer_encrypted}
    ...     referrer_user_type_id=${param_not_used}
    ...     referrer_user_type_name=${param_not_used}
    ...     referrer_user_id=${param_not_used}
    [Return]    ${dic}

[Fraud_Consultant][Test][200] - get value of configuration by scope and key
    [Arguments]  ${scope}   ${key}
    ${arg_dic}  create dictionary  access_token=${suite_admin_access_token}
    ${dic}  [200] API get configuration detail  ${suite_admin_access_token}     ${scope}    ${key}
    set test variable  ${test_retry_number}     ${dic.response.json()['data']['value']}

[Fraud_Consultant][Test][200] - create fraud ticket with action register customer and device_id in list
    [Arguments]     ${number}
    ${device_ids}   create list
    :FOR    ${index}    IN RANGE    0   ${number}
    \   ${device_id}    generate random string  20  [LETTERS]
    \   append to list  ${device_ids}   ${device_id}
    set test variable   ${test_device_ids}     ${device_ids}
    ${device_description}   generate random string  20  [LETTERS]
    set test variable   ${test_device_description}  ${device_description}

    ${data}  create list
    ${count}    get length  ${test_device_ids}
    :FOR    ${index}    IN RANGE    ${count}
    \   ${item}     set variable    {"device_id":"${device_ids[${index}]}", "device_description": "${device_description}"}
    \   append to list  ${data}     ${item}
    ${current_from_date}    Prepare current start date
    ${current_end_date}     Prepare current end date
    set test variable   ${test_current_from_date}   ${current_from_date}
    set test variable   ${test_current_end_date}    ${current_end_date}
    ${ticket_description}   generate random string  20  [LETTERS]
    ${arg_dic}    create dictionary
    ...     data=${data}
    ...     action=register customer
    ...     start_active_ticket_timestamp=${current_from_date}
    ...     end_active_ticket_timestamp=${current_end_date}
    ...     description=${ticket_description}

    ${dic}  [200] API create new fraud ticket    ${arg_dic}
    set test variable   ${test_tickets}  ${dic.tickets}
    set test variable   ${test_result_dic}  ${dic}
    set test variable   ${test_action_id}      register customer
    ${response}         get from dictionary      ${test_result_dic}     response
    set test variable   ${test_ticket_register_customer_id}          ${response.json()['data']['tickets'][0]['ticket_id']}

[Fraud_Consultant][Test][200] - register multi-customer with device_id
    [Arguments]     ${number}
    ${device_id}      generate random string    20  [LETTERS]
    set test variable   ${test_device_id}  ${device_id}
    :FOR    ${index}    IN RANGE    0   ${number}
    \   ${customer_dic}    [Fraud_Consultant][Reuse][Data] - create customer info dictionary
    \   [Common] - Set default value for keyword in dictionary     ${customer_dic}     header_device_id    ${device_id}
    \   ${dic}  [200] API register customer     ${customer_dic}

[Fraud_Consultant][Test][200] - search all fraud tickets by device_id
    [Arguments]     ${device_id}
    ${arg_dic}      create dictionary   device_id=${device_id}
    ${dic}  [200] API search all fraud tickets  ${arg_dic}
    set test variable   ${test_tickets}  ${dic.tickets}
    set test variable   ${test_result_dic}      ${dic}

[Fraud_Consultant][Test][200] - get setup customer profile with blocked device_id
    [Arguments]     ${device_id}
    ${arg_dic}      create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     device_id=${device_id}
    ${dic}  [200] API get customer profiles    ${arg_dic}

[Fraud_Consultant][Test][200] - get setup customer profile with list blocked device_id
    [Arguments]     ${device_ids}
    ${count}    get length  ${device_ids}
    :FOR    ${index}    IN RANGE    0   ${count}
    \   [Fraud_Consultant][Test][200] - get setup customer profile with blocked device_id     ${device_ids[${index}]}

[Fraud_Consultant][Test][Fail] - register new customer with device_id in list
    [Arguments]     ${device_ids}
    ${count}    get length  ${device_ids}
    :FOR    ${index}    IN RANGE    ${count}
    \   [Fraud_Consultant][Test][Test][Fail] - register a new customer with device_id   ${device_ids[${index}]}

[Fraud_Consultant][Test][200] - register a new customer with device_id
    [Arguments]     ${device_id}
    ${customer_dic}=    [Fraud_Consultant][Reuse][Data] - create customer info dictionary
    [Common] - Set default value for keyword in dictionary     ${customer_dic}     device_id   ${device_id}
    [Common] - Set default value for keyword in dictionary     ${customer_dic}     device_description  test device
    ${dic}  [200] API register customer profile with device_id  ${customer_dic}

[Fraud_Consultant][Test][Test][Fail] - register a new customer with device_id
    [Arguments]     ${device_id}
    ${customer_dic}=    [Fraud_Consultant][Reuse][Data] - create customer info dictionary
    [Common] - Set default value for keyword in dictionary     ${customer_dic}     device_id   ${device_id}
    [Common] - Set default value for keyword in dictionary     ${customer_dic}     device_description  test device
    ${dic}  [Fail] API register customer profile with device_id  ${customer_dic}

[Fraud_Consultant][Suite][200] - create fraud ticket
    [Suite][200] System user create customer

    ${device_id}                 generate random string      10      [NUMBERS]
    ${device_description}        generate random string      30      [LETTERS]
    set suite variable      ${suite_device_id}             ${device_id}
    set suite variable      ${suite_device_description}    ${device_description}

    ${arg_dic}      create dictionary     username=${suite_customer_username}      password=${setup_password_customer_encrypted_utf8}     grant_type=password     param_client_id=${setup_admin_client_id}
    ${dic}      [200] API customer authentication    ${arg_dic}
    ${customer_access_token}   get from dictionary  ${dic}       access_token

    ${firstname}=        generate random string      10      [LETTERS]
    ${lastname}=         generate random string      10      [LETTERS]
    ${arg_dic}  create dictionary   access_token=${customer_access_token}    firstname=${firstname}    lastname=${lastname}    card_type_id=1   card_lifetime_in_month=12
    ${dic}      [200] API create prepaid card       ${arg_dic}
    ${card_id}=        get from dictionary     ${dic}          id
    ${str_card_id}=         Convert To String         ${card_id}
    set suite variable          ${suite_card_id}         ${str_card_id}

    ${start_active_ticket_timestamp}=     Get Current Date	    UTC       result_format=%Y-%m-%dT%H:%M:%SZ
    ${end_active_ticket_timestamp}=       Add time to date      ${start_active_ticket_timestamp}        2 days      result_format=%Y-%m-%dT%H:%M:%SZ        date_format=%Y-%m-%dT%H:%M:%SZ
    ${arg_dic}     create dictionary      access_token=${suite_admin_access_token}        card_id=${suite_card_id}        start_active_ticket_timestamp=${start_active_ticket_timestamp}        end_active_ticket_timestamp=${end_active_ticket_timestamp}
    ${dic}      [200] API create fraud ticket        ${arg_dic}
    ${ticket_id}=        get from dictionary     ${dic}          ticket_id
    set suite variable          ${suite_ticket_id}       ${ticket_id}
    set suite variable          ${suite_card_id}        ${card_id}
    set suite variable          ${suite_start_active_ticket_timestamp}       ${start_active_ticket_timestamp}
    set suite variable          ${suite_end_active_ticket_timestamp}        ${end_active_ticket_timestamp}

[Fraud_Consultant][Test][200] - create fraud ticket with rule_action_id
    ${device_id}                 generate random string      10      [NUMBERS]
    ${device_description}        generate random string      30      [LETTERS]
    set test variable      ${test_device_id}             ${device_id}
    set test variable      ${test_device_description}             ${device_description}
    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}       device_id=${test_device_id}       device_description=${test_device_description}
    : FOR    ${index}    IN RANGE    0    4
        \   [200] API register customer profile with device_id      ${arg_dic}
    sleep      2s
    ${response_dic}=     [200] API search fraud tickets by device_id     ${arg_dic}     ${test_device_id}
    ${rule_action_id}   get from dictionary     ${response_dic}       rule_action_id
    set test variable   ${test_rule_action_id}    ${rule_action_id}

[Fraud_Consultant][Test][200] - Create a fraud ticket to block agent B using service belong service group B
    ${data}  create list
    :FOR    ${index}    IN RANGE    1
    \   ${item}  set variable    {"agent_id":${test_agent_b_id}, "service_id":${suite_remittance_cash_out_service_id}, "service_group_id": ${suite_service_group_b_id}}
    \   append to list  ${data}     ${item}
    ${current_from_date}    Prepare current start date
    set test variable   ${test_current_from_date}   ${current_from_date}
    ${arg_dic}    create dictionary
    ...     data=${data}
    ...     action=Agents Remittance blocked
    ...     description=Incorrect phone number and voucher code.
    ...     start_active_ticket_timestamp=${current_from_date}
    ...     end_active_ticket_timestamp=${empty}

    ${dic}  [200] API create new fraud ticket    ${arg_dic}
    set test variable   ${test_tickets}     ${dic.tickets}
    set test variable   ${test_result_dic}      ${dic}
    set test variable   ${test_action_id}      Agents Remittance blocked
    ${response}         get from dictionary      ${test_result_dic}     response
    set test variable   ${test_ticket_agents_block_id}      ${response.json()['data']['tickets'][0]['ticket_id']}

[Fraud_Consultant][Test][200] - create a fraud ticket to hold the voucher_id
    ${data}  create list
    :FOR    ${index}    IN RANGE    1
    \   ${item}  set variable    {"voucher_id":${voucher_detail_dic.id}}
    \   append to list  ${data}     ${item}
    ${current_from_date}    Prepare current start date
    set test variable   ${test_current_from_date}   ${current_from_date}
    ${arg_dic}    create dictionary
    ...     data=${data}
    ...     action=Customer Remittance voucher - On Hold
    ...     description=Incorrect phone number with valid voucher code.
    ...     start_active_ticket_timestamp=${current_from_date}
    ...     end_active_ticket_timestamp=${empty}

    ${dic}  [200] API create new fraud ticket    ${arg_dic}
    set test variable   ${test_tickets}     ${dic.tickets}
    set test variable   ${test_result_dic}      ${dic}
    set test variable   ${test_action_id}      Customer Remittance voucher - On Hold
    ${response}         get from dictionary      ${test_result_dic}     response
    set test variable   ${test_ticket_hold_id}          ${response.json()['data']['tickets'][0]['ticket_id']}

[Fraud_Consultant][Test][200] - search all fraud tickets by agent_id
    ${arg_dic}      create dictionary   agent_id=${test_agent_b_id}
    ${dic}  [200] API search all fraud tickets  ${arg_dic}
    set test variable   ${test_tickets}     ${dic.tickets}
    set test variable   ${test_result_dic}      ${dic}

[Fraud_Consultant][Test][200] - search all fraud tickets by non-existing agent_id
    ${arg_dic}      create dictionary   agent_id=0
    ${dic}  [200] API search all fraud tickets  ${arg_dic}
    set test variable   ${test_tickets}     ${dic.tickets}
    set test variable   ${test_result_dic}      ${dic}

[Fraud_Consultant][Test][200] - search all fraud tickets by ticket_id
    [Arguments]     ${ticket_id}
    ${arg_dic}      create dictionary   ticket_id=${ticket_id}
    ${dic}  [200] API search all fraud tickets  ${arg_dic}
    set test variable   ${test_tickets}     ${dic.tickets}
    set test variable   ${test_result_dic}      ${dic}

[Fraud_Consultant][Test][Fail] - Agent B create and execute cash out order service belongs to service group B with phone number is null and valid voucher in X times
    [Arguments]     ${number}
    :FOR    ${index}    IN RANGE    0   ${number}
    \   [Payment][Test][Fail] - Create and execute remittance cash out order with phone number is null and valid voucher_id

[Fraud_Consultant][Test][Fail] - Agent B create and execute cash out order service belongs to service group B with valid phone number and invalid voucher in x times
    [Arguments]     ${number}
    :FOR    ${index}    IN RANGE    0   ${number}
    \   [Payment][Test][Fail] - Create and execute remittance cash out order with valid phone number and invalid voucher_id

[Fraud_Consultant][Test][Fail] - Agent B create and execute cash out order service belongs to service group B with invalid both phone number and voucher in x times
    [Arguments]     ${number}
    :FOR    ${index}    IN RANGE    0   ${number}
    \   [Payment][Test][Fail] - Create and execute remittance cash out order with invalid both phone number and voucher_id

[Fraud_Consultant][Test][200] - Search all fraud tickets by voucher_id
    [Arguments]     ${voucher_id}
    ${arg_dic}      create dictionary   voucher_id=${voucher_id}
    ${dic}  [200] API search all fraud tickets  ${arg_dic}
    set test variable   ${test_tickets}     ${dic.tickets}
    set test variable   ${test_result_dic}      ${dic}

[Fraud_Consultant][Test][200] - get all unblock ticket reasons
    ${arg_dic}      create dictionary
    ${dic}  [200] API get all unblock ticket reasons    ${arg_dic}
    set test variable   ${test_unblock_reasons}     ${dic.response.json()['data']}

[Fraud_Consultant][Test][200] - unblock ticket with reason
    [Arguments]  ${ticket_id}
    ${reason}      [Fraud_Consultant][Reuse][Data] - get random value from list of unblock reasons
    ${arg_dic}      create dictionary   ticket_id=${ticket_id}  delete_reason=${reason}
    ${dic}  [200] API unblock ticket with reason    ${arg_dic}
    set test variable   ${test_reason}      ${reason}

[Fraud_Consultant][Test][Fail] - Unblock ticket with reason for Customer Remittance voucher - On Hold
    [Arguments]  ${ticket_id}
    ${reason}      [Fraud_Consultant][Reuse][Data] - get random value from list of unblock reasons
    ${arg_dic}      create dictionary   ticket_id=${ticket_id}  delete_reason=${reason}
    ${dic}  [Fail] API unblock ticket with reason for Customer Remittance voucher - On Hold    ${arg_dic}
    set test variable   ${test_reason}      ${reason}

[Fraud_Consultant][Test][Fail] - Unblock ticket without reason
    [Arguments]  ${ticket_id}
    ${arg_dic}      create dictionary   ticket_id=${ticket_id}
    ${dic}  [Fail] API unblock ticket without reason  ${arg_dic}

[Fraud_Consultant][Test][Fail] - Unblock ticket with invalid reason
    [Arguments]  ${ticket_id}   ${test_reason}
    ${arg_dic}      create dictionary   ticket_id=${ticket_id}  delete_reason=${test_reason}
    ${dic}  [Fail] API unblock ticket without reason  ${arg_dic}

[Fraud_Consultant][Reuse][Data] - get random value from list of unblock reasons
    ${expected_unblock_reason_list}     create list     Inbound call - Customer called back CLT for confirmation    Outbound call - Confirmed via phone call    Confirmed via SMS/Email     Other
    ${reason}   [Common] - get random value from list       ${expected_unblock_reason_list}
    [Return]    ${reason}

[Fraud_Consultant][Test][200] - search all fraud tickets
    ${arg_dic}      create dictionary
    ${dic}  [200] API search all fraud tickets  ${arg_dic}
    set test variable   ${test_tickets}  ${dic.tickets}
    set test variable   ${test_result_dic}      ${dic}

[Fraud_Consultant][Test][200] - search all fraud tickets by action
    [Arguments]     ${action_id}
    ${arg_dic}      create dictionary   event_name=${action_id}
    ${dic}  [200] API search all fraud tickets    ${arg_dic}
    set test variable   ${test_tickets}     ${dic.tickets}
    set test variable   ${test_result_dic}      ${dic}

[Fraud_Consultant][Test][200] - search all fraud tickets by from_start_active_ticket_timestamp
    [Arguments]     ${active_date_from}
    ${arg_dic}      create dictionary   from_start_active_ticket_timestamp=${active_date_from}
    ${dic}  [200] API search all fraud tickets   ${arg_dic}
    set test variable   ${test_tickets}     ${dic.tickets}
    set test variable   ${test_result_dic}      ${dic}

[Fraud_Consultant][Test][200] - search all fraud tickets by to_start_active_ticket_timestamp
    [Arguments]     ${active_date_to}
    ${arg_dic}      create dictionary   to_start_active_ticket_timestamp=${active_date_to}
    ${dic}  [200] API search all fraud tickets   ${arg_dic}
    set test variable   ${test_tickets}     ${dic.tickets}
    set test variable   ${test_result_dic}      ${dic}

[Fraud_Consultant][Test][200] - search all fraud tickets by from_start_active_ticket_timestamp and to_start_active_ticket_timestamp
    [Arguments]     ${active_date_from}    ${active_date_to}
    ${arg_dic}      create dictionary   SEPARATOR=
    ...     from_start_active_ticket_timestamp=${active_date_from}
    ...     to_start_active_ticket_timestamp=${active_date_to}

    ${dic}  [200] API search all fraud tickets  ${arg_dic}
    set test variable   ${test_tickets}     ${dic.tickets}
    set test variable   ${test_result_dic}      ${dic}

[Fraud_Consultant][Test][200] - search all fraud tickets by agent_id and to_start_active_ticket_timestamp
    [Arguments]     ${active_date_to}
    ${arg_dic}      create dictionary   SEPARATOR=
    ...     agent_id=${test_agent_b_id}
    ...     to_start_active_ticket_timestamp=${active_date_to}

    ${dic}  [200] API search all fraud tickets  ${arg_dic}
    set test variable   ${test_tickets}     ${dic.tickets}
    set test variable   ${test_result_dic}      ${dic}

[Fraud_Consultant][Test][200] - search all fraud tickets by agent_id, action, from_start_active_ticket_timestamp and to_start_active_ticket_timestamp
    [Arguments]     ${action_id}    ${active_date_from}    ${active_date_to}
    ${arg_dic}      create dictionary   SEPARATOR=
    ...     event_name=${action_id}
    ...     agent_id=${test_agent_b_id}
    ...     from_start_active_ticket_timestamp=${active_date_from}
    ...     to_start_active_ticket_timestamp=${active_date_to}

    ${dic}  [200] API search all fraud tickets  ${arg_dic}
    set test variable   ${test_tickets}     ${dic.tickets}
    set test variable   ${test_result_dic}      ${dic}

[Fraud_Consultant][Suite][200] - create a fraud ticket to block service_id for agent B
    ${data}  create list
    :FOR    ${index}    IN RANGE    1
    \   ${item}  set variable    {"agent_id":${suite_agent_b_id}, "service_id":${suite_remittance_cash_out_service_id}}
    \   append to list  ${data}     ${item}
    ${current_from_date}    Prepare current start date
    ${arg_dic}    create dictionary
    ...     data=${data}
    ...     action=Agents Remittance blocked
    ...     description=Incorrect phone number and voucher code.
    ...     start_active_ticket_timestamp=${current_from_date}
    ...     end_active_ticket_timestamp=${empty}

    ${dic}  [200] API create new fraud ticket    ${arg_dic}
    set suite variable   ${suite_tickets}     ${dic.tickets}
    set suite variable   ${suite_result_dic}      ${dic}
    ${response}         get from dictionary      ${suite_result_dic}     response
    set suite variable   ${suite_ticket_agents_block_id}      ${response.json()['data']['tickets'][0]['ticket_id']}
    set suite variable   ${suite_current_from_date}      ${current_from_date}

[Fraud_Consultant][Suite][200] - create a fraud ticket to hold the voucher_id
    ${data}  create list
    :FOR    ${index}    IN RANGE    1
    \   ${item}  set variable    {"voucher_id":${voucher_detail_dic.id}}
    \   append to list  ${data}     ${item}
    ${current_from_date}    Prepare current start date
    ${arg_dic}    create dictionary
    ...     data=${data}
    ...     action=Customer Remittance voucher - On Hold
    ...     description=Incorrect phone number with valid voucher code.
    ...     start_active_ticket_timestamp=${current_from_date}
    ...     end_active_ticket_timestamp=${empty}

    ${dic}  [200] API create new fraud ticket    ${arg_dic}
    set suite variable   ${suite_tickets}     ${dic.tickets}
    set suite variable   ${suite_result_dic}      ${dic}
    ${response}         get from dictionary      ${suite_result_dic}     response
    set suite variable   ${suite_ticket_hold_id}          ${response.json()['data']['tickets'][0]['ticket_id']}
    set suite variable   ${suite_current_from_date}      ${current_from_date}

[Fraud_Consultant][Suite][200] - create fraud ticket with action register customer and device_id in list
    [Arguments]     ${number}
    ${device_ids}   create list
    :FOR    ${index}    IN RANGE    0   ${number}
    \   ${device_id}    generate random string  20  [LETTERS]
    \   append to list  ${device_ids}   ${device_id}
    set suite variable   ${suite_device_ids}     ${device_ids}
    ${device_description}   generate random string  20  [LETTERS]
    set suite variable   ${suite_device_description}  ${device_description}

    ${data}  create list
    ${count}    get length  ${suite_device_ids}
    :FOR    ${index}    IN RANGE    ${count}
    \   ${item}     set variable    {"device_id":"${device_ids[${index}]}", "device_description": "${device_description}"}
    \   append to list  ${data}     ${item}
    ${current_from_date}    Prepare current start date
    ${current_end_date}     Prepare current end date
    ${ticket_description}   generate random string  20  [LETTERS]
    ${arg_dic}    create dictionary
    ...     data=${data}
    ...     action=register customer
    ...     start_active_ticket_timestamp=${current_from_date}
    ...     end_active_ticket_timestamp=${current_end_date}
    ...     description=${ticket_description}

    ${dic}  [200] API create new fraud ticket    ${arg_dic}
    set suite variable   ${suite_tickets}  ${dic.tickets}
    set suite variable   ${suite_result_dic}  ${dic}
    set suite variable   ${suite_action_id}      register customer
    log  ${dic}
    ${response}         get from dictionary      ${suite_result_dic}     response
    set suite variable   ${suite_ticket_register_customer_id}          ${response.json()['data']['tickets'][0]['ticket_id']}
    set suite variable   ${suite_current_from_date}      ${current_from_date}
    set suite variable   ${suite_current_end_date}      ${current_end_date}

[Fraud_Consultant][Suite][200] - prepare fraud tickets
    [System_User][Suite][200] - create account login via api
    [Agent][Suite][200] - log in as company agent
    [Payment][Suite][200] - add money for company agent in "VND" currency
    [Suite][200] create agent remittance in "VND" currency
    [Payment][Suite][200] - create normal service structure in "VND" currency
    [Payment][Suite][200] - Create "Remittance Cash-In" service structure belongs to service group A
    [Payment][Suite][200] - Create "Remittance Cash-Out" service structure belongs to service group B
    [Agent][Suite][200] - create agent A in "VND" currency
    [Payment][Suite][200] - company agent fund in for agent A
    [Agent][Suite][200] - create agent B in "VND" currency
    [Suite][200] company agent fund in for agent B
    [Payment][Suite][200] - Create and execute remittance cash in order by agent A
    [Payment][Suite][200] - Get order detail of remittance cash in order
    [Fraud_Consultant][Suite][200] - create a fraud ticket to block service_id for agent B
    [Report][Suite][200] - Search voucher by voucher code and passcode
    [Fraud_Consultant][Suite][200] - create a fraud ticket to hold the voucher_id
    [Fraud_Consultant][Suite][200] - create fraud ticket with action register customer and device_id in list     1

[Fraud_Consultant][Test][200] - search all fraud tickets by is_deleted is False
    ${arg_dic}      create dictionary   is_deleted=False
    ${dic}  [200] API search all fraud tickets    ${arg_dic}
    set test variable   ${test_tickets}     ${dic.tickets}
    set test variable   ${test_result_dic}      ${dic}
    ${response}        get from dictionary     ${test_result_dic}     response
    set test variable   ${test_total_tickets}   ${response.json()['data']['page']['total_elements']}

[Fraud_Consultant][Reuse][Data] - get random value from list of events
    ${expected_event_list}     create list      Register Customer   Customer Remittance voucher  Agents Remittance blocked
    ${event}   get random value from list       ${expected_event_list}
    [Return]    ${event}
    ${length}   get length  ${test_tickets}
    set test variable   ${test_length}      ${length}

[Fraud_Consultant][Test][200] - search all fraud tickets by type and is_deleted
    [Arguments]    ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      is_deleted       False
    ${dic}  [200] API search all fraud tickets      ${arg_dic}
    set test variable   ${test_tickets}     ${dic.tickets}
    set test variable   ${test_result_dic}      ${dic}
    ${response}        get from dictionary     ${test_result_dic}     response
    set test variable   ${test_total_tickets}   ${response.json()['data']['page']['total_elements']}
    log  ${test_result_dic}
    set test variable   ${test_ticket_start_block}          ${response.json()['data']['tickets'][0]['start_active_ticket_timestamp']}
    set test variable   ${test_ticket_end_block}            ${response.json()['data']['tickets'][0]['end_active_ticket_timestamp']}

[Fraud_Consultant][Test][Fail] - Unblock ticket with reason but ticket does not exist
    [Arguments]  ${ticket_id}
    ${reason}      [Fraud_Consultant][Reuse][Data] - get random value from list of unblock reasons
    ${arg_dic}      create dictionary   ticket_id=${ticket_id}  delete_reason=${reason}
    ${dic}  [Fail] API unblock ticket with reason but ticket does not exist   ${arg_dic}
    set test variable   ${test_reason}      ${reason}

[Fraud_Consultant][Test][Fail] - Unblock ticket with reason but ticket already deleted
    [Arguments]  ${ticket_id}
    ${reason}      [Fraud_Consultant][Reuse][Data] - get random value from list of unblock reasons
    ${arg_dic}      create dictionary   ticket_id=${ticket_id}  delete_reason=${reason}
    ${dic}  [Fail] API unblock ticket with reason but ticket already deleted   ${arg_dic}
    set test variable   ${test_reason}      ${reason}

[Fraud_Consultant][Test][Fail] - Search all fraud tickets by invalid from_start_active_ticket_timestamp
    [Arguments]     ${active_date_from}
    ${arg_dic}      create dictionary   from_start_active_ticket_timestamp=${active_date_from}
    ${dic}  [Fail] API search all fraud tickets  ${arg_dic}

[Fraud_Consultant][Test][Fail] - Search all fraud tickets by invalid to_start_active_ticket_timestamp
    [Arguments]     ${active_date_to}
    ${arg_dic}      create dictionary   to_start_active_ticket_timestamp=${active_date_to}
    ${dic}  [Fail] API search all fraud tickets  ${arg_dic}

[Fraud_Consultant][Test][200] - Create fraud ticket to unstop card
    [Test][200] System user create customer

    ${device_id}                 generate random string      10      [NUMBERS]
    ${device_description}        generate random string      30      [LETTERS]
    set test variable      ${test_device_id}             ${device_id}
    set test variable      ${test_device_description}    ${device_description}

    ${arg_dic}      create dictionary     username=${test_customer_username}      password=${setup_password_customer_encrypted_utf8}     grant_type=password     param_client_id=${setup_admin_client_id}
    ${dic}      [200] API customer authentication    ${arg_dic}
    ${customer_access_token}   get from dictionary  ${dic}       access_token

    ${firstname}=        generate random string      10      [LETTERS]
    ${lastname}=         generate random string      10      [LETTERS]
    ${arg_dic}  create dictionary   access_token=${customer_access_token}    firstname=${firstname}    lastname=${lastname}    card_type_id=1   card_lifetime_in_month=12
    ${dic}      [200] API create prepaid card       ${arg_dic}
    ${card_id}=        get from dictionary     ${dic}          id
    ${str_card_id}=         Convert To String         ${card_id}
    set test variable          ${test_card_id}         ${str_card_id}

    ${start_active_ticket_timestamp}=     Get Current Date     UTC       result_format=%Y-%m-%dT%H:%M:%SZ
    ${end_active_ticket_timestamp}=       Add time to date      ${start_active_ticket_timestamp}        2 days      result_format=%Y-%m-%dT%H:%M:%SZ        date_format=%Y-%m-%dT%H:%M:%SZ
    ${arg_dic}     create dictionary      access_token=${suite_admin_access_token}        card_id=${test_card_id}        start_active_ticket_timestamp=${start_active_ticket_timestamp}        end_active_ticket_timestamp=${end_active_ticket_timestamp}
    ${dic}      [200] API create fraud ticket        ${arg_dic}
    ${ticket_id}=        get from dictionary     ${dic}          ticket_id
    set test variable          ${test_ticket_id}       ${ticket_id}
    set test variable          ${test_card_id}        ${card_id}
    set test variable          ${test_start_active_ticket_timestamp}       ${start_active_ticket_timestamp}
    set test variable          ${test_end_active_ticket_timestamp}        ${end_active_ticket_timestamp}

[Fraud_Consultant][Test][Fail] - Create normal order with invalid KYC level
    [Arguments]
    ...     ${payer_user_ref_value}
    ...     ${payee_user_id}
    ...     ${payee_user_type}
    ...     ${product_ref_1}

    ${product_name}         generate random string      20   prepare_product_name_[LETTERS]
    ${product_ref_2}        generate random string      20   prepare_service_ref_2_[LETTERS]
    ${product_ref_3}        generate random string      20   prepare_service_ref_3_[LETTERS]
    ${product_ref_4}        generate random string      20   prepare_service_ref_4_[LETTERS]
    ${product_ref_5}        generate random string      20   prepare_service_ref_5_[LETTERS]
    ${ext_transaction_id}      generate random string      12   [NUMBERS]
    ${product_service_id}      generate random string      12   [NUMBERS]
    ${amount}      generate random string      3   123456789
    ${payer_user_ref_type}=    set variable    access token
    ${dic}     [Fail] API payment create normal order with invalid KYC level
    ...     ${payer_user_ref_value}
    ...     ${ext_transaction_id}
    ...     ${suite_service_id}
    ...     ${suite_service_name}
    ...     ${product_name}
    ...     ${product_ref_1}
    ...     ${product_ref_2}
    ...     ${product_ref_3}
    ...     ${product_ref_4}
    ...     ${product_ref_5}
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${payee_user_id}
    ...     ${payee_user_type}
    ...     ${amount}
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}

[Fraud_Consultant][Test][200] - Create normal order
    [Arguments]
    ...     ${payer_user_ref_value}
    ...     ${payee_user_id}
    ...     ${payee_user_type}
    ...     ${product_ref_1}

    ${product_name}         generate random string      20   prepare_product_name_[LETTERS]
    ${product_ref_2}        generate random string      20   prepare_service_ref_2_[LETTERS]
    ${product_ref_3}        generate random string      20   prepare_service_ref_3_[LETTERS]
    ${product_ref_4}        generate random string      20   prepare_service_ref_4_[LETTERS]
    ${product_ref_5}        generate random string      20   prepare_service_ref_5_[LETTERS]
    ${ext_transaction_id}      generate random string      12   [NUMBERS]
    ${product_service_id}      generate random string      12   [NUMBERS]
    ${amount}      generate random string      3   123456789
    ${payer_user_ref_type}=    set variable    access token
    ${dic}     [200] API payment create normal order
    ...     ${payer_user_ref_value}
    ...     ${ext_transaction_id}
    ...     ${suite_service_id}
    ...     ${suite_service_name}
    ...     ${product_name}
    ...     ${product_ref_1}
    ...     ${product_ref_2}
    ...     ${product_ref_3}
    ...     ${product_ref_4}
    ...     ${product_ref_5}
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${payee_user_id}
    ...     ${payee_user_type}
    ...     ${amount}
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}