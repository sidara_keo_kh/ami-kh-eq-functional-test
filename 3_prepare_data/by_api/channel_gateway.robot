*** Settings ***
Resource         ../../2_api/3_reuse_function/imports.robot

*** Keywords ***

[Test][200] Create channel gateway service
    [Arguments]     ${location}
    ${random_name}      generate random string  15  [LETTERS]
    ${arg_dic}     Create dictionary      location=${location}
                                    ...   name=${random_name}
                                    ...   max_total_connection=40
                                    ...   max_per_route=20
                                    ...   timeout=120000
                                    ...   access_token=${suite_admin_access_token}
    ${dic}      [200] API channel gateway - add service     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    ${service_id}       set variable    ${response.json()['data']['id']}
    set test variable       ${test_service_id}    ${service_id}

[Test][200] Create channel gateway api
    [Arguments]     ${http_method}        ${permission_required}
    ${api_suffix}      generate random string  5  [LETTERS]
    ${random_name}      generate random string  15  [LETTERS]
    ${arg_dic}         Create dictionary
        ...     http_method=${http_method}
        ...     is_required_access_token=true
        ...     is_internal=false
        ...     pattern=/headers/${api_suffix}
        ...     service_id=${test_service_id}
        ...     permission_required=${permission_required}
        ...     name=${random_name}
        ...     access_token=${suite_admin_access_token}
    ${dic}      [200] API channel gateway - add api    ${arg_dic}
    set test variable       ${test_api_url}         ${context_root_channel_gateway}/headers/${api_suffix}
    ${response}    get from dictionary     ${dic}    response
    ${api_id}       set variable    ${response.json()['data']['id']}
    set test variable       ${test_api_id}    ${api_id}

[Test][200] Add channel gateway client scope
    [Arguments]  ${api_id}
    ${arg_dic}       Create dictionary
        ...     scopes=${test_api_id}
        ...     request_client_id=${setup_admin_client_id}
        ...     access_token=${suite_admin_access_token}
    ${dic}     [200] API channel gateway - add scopes to client   ${arg_dic}

[Test][200] Channel Gateway call GET api
    [Arguments]
        ...     ${path}
        ...     ${code}
        ...     ${message}
        ...     ${client_id}=${setup_admin_client_id}
        ...     ${client_secret}=${setup_admin_client_secret}
        ...     ${access_token}=${test_access_token}

    ${header}  create dictionary
        ...     client_id=${client_id}
        ...     client_secret=${client_secret}
        ...     content-type=application/json
        ...     Authorization=Bearer ${access_token}
               create session          api     ${channel_gateway_host}     disable_warnings=1
    ${response}    get request
        ...     api
        ...     ${path}
        ...     ${header}
   log        ${response.text}
    should be equal as integers     ${response.status_code}                         ${code}
    run keyword if      '${code}'!='200'        run keywords
                        ...   set test variable      ${actual_message}    ${response.json()['status']['message']}
                        ...   AND       should be equal as strings     ${actual_message}      ${message}