*** Settings ***
Resource         ../../2_api/3_reuse_function/imports.robot

*** Keywords ***
[Report][Suite][200] - system user updates payroll card number for prepared customer
    ${dic}  create dictionary     access_token=${suite_admin_access_token}   customer_id=${suite_customer_id}
    ...     first_name=updated_${suite_customer_first_name}    payroll_card_number=${suite_payroll_card_number}
    ...     last_name=updated_${suite_customer_last_name}       unique_reference=${suite_customer_unique_reference}
    [200] API system user updates customer profile      ${dic}

[Report][Test][200] - get company transaction history by connected agent
    ${dic}     create dictionary     access_token=${suite_admin_access_token}
    ...         order_id=${suite_wallet_agent_normal_order_id}
    ...         payee_user_id=${suite_normal_agent_id}                  payee_user_type_id=2
    ...         payer_user_id=${suite_wallet_agent_id}                  payer_user_type=2
    ...         initiator_user_id=${suite_wallet_agent_id}              initiator_user_type_id=2
    ...         company_agent_id=${suite_company_id}                    company_name=${suite_company_name}
    ...         status=2                                                ext_transaction_id=${suite_wallet_agent_ext_transaction_id}
    ...         product_name=${suite_wallet_agent_product_name}
    ${response}         [200] Get Company Transaction History By Connected Agent      ${dic}
    set test variable       ${company_response}         ${response}

[Report][Test][Fail] - system user with no link company get company transaction history of connected agent
    ${dic}     create dictionary     access_token=${suite_system_user_no_link_access_token}
    ...         order_id=${suite_wallet_agent_normal_order_id}
    ...         payee_user_id=${suite_normal_agent_id}                  payee_user_type_id=2
    ...         payer_user_id=${suite_wallet_agent_id}                  payer_user_type=2
    ...         initiator_user_id=${suite_wallet_agent_id}              initiator_user_type_id=2
    ...         company_agent_id=${suite_company_id}                    company_name=${suite_company_name}
    ...         status=2                                                ext_transaction_id=${suite_wallet_agent_ext_transaction_id}
    ...         product_name=${suite_wallet_agent_product_name}
    ${response}         [200] Get Company Transaction History By Connected Agent      ${dic}
    set test variable       ${company_response}         ${response}

[Report][Test][Fail] - get company transaction history by connected agent
    ${wrong_order_id}     generate random string  20  [UPPER]
    ${dic}     create dictionary     access_token=${suite_admin_access_token}
    ...         order_id=${wrong_order_id}
    ...         payee_user_id=${suite_normal_agent_id}                  payee_user_type_id=2
    ...         payer_user_id=${suite_wallet_agent_id}                  payer_user_type=2
    ...         initiator_user_id=${suite_wallet_agent_id}              initiator_user_type_id=2
    ...         company_agent_id=${suite_company_id}                    company_name=${suite_company_name}
    ...         status=2                                                ext_transaction_id=${suite_wallet_agent_ext_transaction_id}
    ...         product_name=${suite_wallet_agent_product_name}
    ${response}         [200] Get Company Transaction History By Connected Agent      ${dic}
    set test variable       ${company_response}         ${response}

[Report][Test][200] - get company transaction history by connected agent filter by Agent Type
     ${dic}     create dictionary     access_token=${suite_admin_access_token}
    ...         order_id=${suite_wallet_agent_normal_order_id}          agent_type_name=${suite_agent_type_name}
    ...         payee_user_id=${suite_normal_agent_id}                  payee_user_type_id=2
    ...         payer_user_id=${suite_wallet_agent_id}                  payer_user_type=2
    ...         initiator_user_id=${suite_wallet_agent_id}              initiator_user_type_id=2
    ...         company_agent_id=${suite_company_id}                    company_name=${suite_company_name}
    ...         ext_transaction_id=${suite_wallet_agent_ext_transaction_id}
    ...         product_name=${suite_wallet_agent_product_name}
    ${response}         [200] Get Company Transaction History By Connected Agent      ${dic}
    set test variable   ${company_response}     ${response}

[Report][Test][Fail] - get company transaction history by connected agent with no link company and system user
    ${dic}     create dictionary     access_token=${suite_setup_admin_access_token}
    ...         order_id=${suite_wallet_agent_normal_order_id}
    ...         payee_user_id=${suite_normal_agent_id}                  payee_user_type_id=2
    ...         payer_user_id=${suite_wallet_agent_id}                  payer_user_type=2
    ...         initiator_user_id=${suite_wallet_agent_id}              initiator_user_type=2
    ...         company_agent_id=${suite_company_id}                    company_name=${suite_company_name}
    ...         ext_transaction_id=${suite_wallet_agent_ext_transaction_id}
    ...         product_name=${suite_wallet_agent_product_name}
    [Failed] Get Company Transaction History By Connected Agent      ${dic}

[Report][Test][200] - get company transaction history with system user by company name
    ${dic}     create dictionary     access_token=${suite_admin_access_token}
    ...        order_id=${suite_wallet_agent_normal_order_id}
    ...        payee_user_id=${suite_normal_agent_id}                 payee_user_type_id=2
    ...        payer_user_id=${suite_wallet_agent_id}                 payer_user_type=2
    ...        initiator_user_id=${suite_wallet_agent_id}             initiator_user_type_id=2
    ...        company_agent_id=${suite_company_id}                   company_name=${suite_company_name}
    ${response}     [200] Get Company Transaction History By Specific Company      ${dic}
    set test variable   ${company_response}     ${response}

[Report][Test][Fail] - get company transaction history with system user by company name
    ${wrong_company_name}     generate random string  20  [UPPER]
    ${dic}     create dictionary     access_token=${suite_admin_access_token}
    ...        order_id=${suite_wallet_agent_normal_order_id}
    ...        payee_user_id=${suite_normal_agent_id}                 payee_user_type_id=2
    ...        payer_user_id=${suite_wallet_agent_id}                 payer_user_type=2
    ...        initiator_user_id=${suite_wallet_agent_id}             initiator_user_type_id=2
    ...        company_name=${wrong_company_name}
    ${response}     [200] Get Company Transaction History By Specific Company      ${dic}
    set test variable   ${company_response}     ${response}

[Report][Test][200] - get company transaction history with system user by company ID
    ${dic}     create dictionary     access_token=${suite_admin_access_token}
    ...        order_id=${suite_wallet_agent_normal_order_id}
    ...        payee_user_id=${suite_normal_agent_id}                 payee_user_type_id=2
    ...        payer_user_id=${suite_wallet_agent_id}                 payer_user_type=2
    ...        initiator_user_id=${suite_wallet_agent_id}             initiator_user_type_id=2
    ...        company_agent_id=${suite_company_id}
    ${response}     [200] Get Company Transaction History By Specific Company      ${dic}
    set test variable   ${company_response}     ${response}

[Report][Test][200] - get transaction history of company
    ${dic}  create dictionary
    ...     access_token=${suite_admin_access_token}
    ${response}     [200] API Get transaction history of company   ${dic}
    set test variable   ${company_response}     ${response}


[Report][Test][Fail] - get transaction history of company with no link user and company
    ${dic}  create dictionary
    ...     access_token=${suite_system_user_no_link_access_token}
    ${response}     [200] API Get transaction history of company   ${dic}
    set test variable   ${company_response}     ${response}

[Report][Test][Fail] - get transaction history of company with connected agent
    ${dic}  create dictionary
    ...     access_token=${suite_connected_agent_access_token}
    ${response}     [200] API Get transaction history of company   ${dic}
    set test variable   ${company_response}     ${response}


[Report][Test][Fail] - Summary agents commission
    [Arguments]     &{arg_dic}
    ${start_timestamp}             Get Current Date	    UTC       result_format=%Y-%m-%dT%H:%M:%SZ
    ${end_timestamp}               Get Current Date	    UTC       result_format=%Y-%m-%dT%H:%M:%SZ
    ${dic}    [Fail] API summary agents commission    access_token=${arg_dic.access_token}
        ...     start_timestamp=${start_timestamp}
        ...     end_timestamp=${end_timestamp}
        ...     code=${arg_dic.code}
        ...     status_code=${arg_dic.status_code}
        ...     status_message=${arg_dic.status_message}

[Report][Test][Fail] - Search agent commission
    [Arguments]  &{arg_dic}
    ${start_timestamp}             Get Current Date	    UTC       result_format=%Y-%m-%dT%H:%M:%SZ
    ${end_timestamp}               Get Current Date	    UTC       result_format=%Y-%m-%dT%H:%M:%SZ
    ${dic}    [Fail] API search agent commission    access_token=${arg_dic.access_token}
        ...     start_timestamp=${start_timestamp}
        ...     end_timestamp=${end_timestamp}
        ...     code=${arg_dic.code}
        ...     status_code=${arg_dic.status_code}
        ...     status_message=${arg_dic.status_message}

[Report][Test][200] - API get all service groups
    [Arguments]  &{arg_dic}
    ${dic}    [200] API get all service groups    &{arg_dic}
    ${response}     get from dictionary     ${dic}      response
    should be equal as integers     ${dic.response.status_code}     200
    set to dictionary       ${dic}      code         ${response.json()['status']['code']}
    [Return]    ${dic}

[Report][Test][200] - Summary agents commission
    [Arguments]     &{arg_dic}
    ${start_timestamp}             Get Current Date	    UTC       result_format=%Y-%m-%dT%H:%M:%SZ
    ${end_timestamp}               Get Current Date	    UTC       result_format=%Y-%m-%dT%H:%M:%SZ
    ${dic}  [200] API summary agents commission    access_token=${arg_dic.access_token}     start_timestamp=${start_timestamp}   end_timestamp=${end_timestamp}

[Report][Test][200] - Search agent commission
    [Arguments]     &{arg_dic}
    ${start_timestamp}             Get Current Date	    UTC       result_format=%Y-%m-%dT%H:%M:%SZ
    ${end_timestamp}               Get Current Date	    UTC       result_format=%Y-%m-%dT%H:%M:%SZ
    ${dic}  [200] API search agent commission         access_token=${arg_dic.access_token}     start_timestamp=${start_timestamp}   end_timestamp=${end_timestamp}

[Report][Test][200] - get transaction history of company with all filter
    ${service_id_list}=     Create List
    append to list      ${service_id_list}      ${suite_service_id}
    ${current_date}       Get Current Date    result_format=%Y-%m-%d
    ${yesterday_date}=    add time to date    ${current_date}       -1 days     result_format=%Y-%m-%dT00:00:00Z
    ${tomorrow_date}=       add time to date    ${current_date}       1 days     result_format=%Y-%m-%dT00:00:00Z
    ${order_detail}     [200] API get order detail
    ...     access_token=${suite_wallet_agent_access_token}
    ...     order_id=${suite_wallet_agent_normal_order_id}
    ...     header_client_id=${setup_admin_client_id}
    ...     header_client_secret=${setup_admin_client_secret}
    ${dic}  create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     order_id=${suite_wallet_agent_normal_order_id}
    ...     from_date=${yesterday_date}
    ...     to=${tomorrow_date}
    ...     from_last_updated_timestamp=${yesterday_date}
    ...     to_last_updated_timestamp=${tomorrow_date}
    ...     payee_user_id=${suite_normal_agent_id}
    ...     payee_user_type_id=2
    ...     payer_user_id=${suite_wallet_agent_id}
    ...     payer_user_type_id=2
    ...     initiator_user_id=${suite_wallet_agent_id}
    ...     initiator_user_type_id=2
    ...     status=2
    ...     created_client_id=${setup_admin_client_id}
    ...     created_channel_type=${param_is_null}
    ...     created_device_unique_reference=${param_is_null}
    ...     ext_transaction_id=${suite_wallet_agent_ext_transaction_id}
    ...     ref_order_id=${param_is_null}
    ...     short_order_id=${order_detail.short_order_id}
    ...     service_id_list=${service_id_list}
    ...     service_group_id=${suite_service_group_id}
    ...     product_name=${suite_wallet_agent_product_name}
    ${response}     [200] API Get transaction history of company   ${dic}
    set test variable   ${company_response}     ${response}

[Report][Test][200] - system user updates payroll card number for prepared customer
    ${dic}  create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     customer_id=${test_customer_id}
    ...     first_name=updated_${test_customer_first_name}
    ...     payroll_card_number=${test_payroll_card_number}
    ...     last_name=updated_${test_customer_last_name}
    ...     unique_reference=${test_customer_unique_reference}
    [200] API system user updates customer profile      ${dic}

[Report][Test][200] - System user search settlement resolving histories
    [Arguments]  &{arg_dic}
    ${Return_dic}  [Report][200] - Search settlement resolving histories      &{arg_dic}

[Report][Test][200] - Search pending settlement with settlement_id
    [Arguments]    &{arg_dic}
    ${dic}    [200] API search pending settlement    access_token=${suite_setup_admin_access_token}    settlement_id=${arg_dic.settlement_id}
    [Return]  ${dic}

[Report][200] - Get user balance
    [Arguments]     ${user_id}       ${user_type}       ${currency}     ${output}   ${headers}=${SUITE_ADMIN_HEADERS}
    [Report][Prepare] - search cash source of fund - body
        ...     $.user_id=${user_id}
        ...     $.user_type=${user_type}
        ...     $.currency=${currency}
    [Report][200] - search cash source of fund
        ...     headers=${headers}
        ...     body=${body}
    ${balance}  rest extract     $.data.cash_sofs[0].balance
    [Common] - Set variable       name=${output}      value=${balance}

[Report][200] Search permission by name
    [Arguments]     ${permission_name}      ${output_permission_id}=test_permission_id      ${headers}=${SUITE_ADMIN_HEADERS}
    [Report][200] - Get all permissions
        ...     headers=${headers}
        ...     body={}
    ${permission_id}    rest extract      $.data[?(@.name == "${permission_name}")].id
    [Common] - Set variable       name=${output_permission_id}      value=${permission_id}

[Report][200] - System use search agent commission
    [Arguments]     ${timezone}=00:00       ${headers}=${SUITE_ADMIN_HEADERS}        &{arg_dic}
#    ${from_timestamp}        Get Current Date    result_format=%Y-%m-%dT00:00:00Z
#    ${to_timestamp}        Get Current Date    result_format=%Y-%m-%dT23:59:59Z
    [Report][Prepare] - search agent commission - body
        ...     $.timezone=${timezone}
        ...     $.agent_ids=[${arg_dic.agent_ids}]
        ...     $.start_timestamp=${arg_dic.from_timestamp}
        ...     $.end_timestamp=${arg_dic.to_timestamp}
    [Report][200] - Search agent commission
        ...     headers=${headers}
        ...     body=${body}

[Report][Extract] System use search agent commission
    [Arguments]     ${agent_id}     ${service_group_id}     ${currency}      ${output}=test_commissions
    ${commissions}      rest extract           $.data.agents[?(@.id == ${agent_id})].service_groups[?(@.id == ${service_group_id})].summaries[?(@.currency == "${currency}")]
    [Common] - Set variable       name=${output}      value=${commissions}

[Report][200] Search order by order id
    [Arguments]     ${order_id}=${test_order_id}     ${headers}=${SUITE_ADMIN_HEADERS}
    [Report][Prepare] - search payment orders - body
        ...     $.order_id=${order_id}
    [Report][200] - Search payment orders
        ...     headers=${headers}
        ...     body=${body}

[Report][200] Search sof cash by list agent ID
    [Arguments]     ${user_id_list}       ${headers}=${SUITE_ADMIN_HEADERS}     ${output}=length_response
    [Report][Prepare] - search cash source of fund - body
        ...     $.user_id_list=${user_id_list}
    ${data}     [Report][200] - search cash source of fund
        ...     headers=${headers}
        ...     body=${body}

