*** Settings ***
Resource         ../../2_api/3_reuse_function/imports.robot

*** Keywords ***
[Payment][Test][200] - Create service group
    [Arguments]  &{arg_dic}
    ${agent_type_name}      generate random string      20   prepare_[LETTERS]
    ${agent_type_description}      generate random string      30   prepare_[LETTERS]
    ${agent_type_name}  set variable    prepare_${agent_type_name}
    ${agent_type_description}  set variable    prepare_${agent_type_description}
    ${dic}     [200] API add service group
    ...     ${arg_dic.access_token}
    ...     ${agent_type_name}
    ...     ${agent_type_description}
    ${response}     get from dictionary   ${dic}   response
    ${service_group_id}     get from dictionary   ${dic}   service_group_id
    set test variable   ${service_group_id}       ${service_group_id}
    set test variable   ${test_service_group_id}       ${service_group_id}
    set test variable   ${test_service_gsuiteroup_name}       ${agent_type_name}
    set test variable   ${suite_service_group_id}       ${service_group_id}

[Payment][Suite][200] - Create service group
    [Arguments]  &{arg_dic}
    ${agent_type_name}      generate random string      20   prepare_[LETTERS]
    ${agent_type_description}      generate random string      30   prepare_[LETTERS]
    ${agent_type_name}  set variable    prepare_${agent_type_name}
    ${agent_type_description}  set variable    prepare_${agent_type_description}
    ${dic}     [200] API add service group
    ...     ${arg_dic.access_token}
    ...     ${agent_type_name}
    ...     ${agent_type_description}
    ${response}     get from dictionary   ${dic}   response
    ${service_group_id}     get from dictionary   ${dic}   service_group_id
    set suite variable   ${suite_service_group_id}       ${service_group_id}
    set suite variable   ${suite_service_group_id}       ${service_group_id}
    set suite variable   ${suite_service_group_name}       ${agent_type_name}

[Test][200] API add service group with all fields
    ${service_group_name}      generate random string      20    prepare_[LETTERS]
    ${service_group_description}      generate random string      30        prepare_[LETTERS]
    ${service_group_display_name}       generate random string      20      prepare_[LETTERS]
    ${service_group_display_name_local}     generate random string   20      prepare_[LETTERS]
    ${image_url}    generate random string  20      prepare_[LETTERS]
    ${arg_dic}  create dictionary
    ...     access_token=${test_admin_access_token}
    ...     service_group_name=${service_group_name}
    ...     description=${service_group_description}
    ...     image_url=${image_url}
    ...     display_name=${service_group_display_name}
    ...     display_name_local=${service_group_display_name_local}
    ${dic}     [200] API add service group with all fields      ${arg_dic}
    ${response}     get from dictionary   ${dic}   response
    ${service_group_id}     get from dictionary   ${dic}   service_group_id
    set test variable   ${service_group_id}       ${service_group_id}

[Test][200] API create service in "${service_group_id}" service group id
    ${service_name}      generate random string      20   prepare_service_name_[LETTERS]
    ${currency}     set variable    VND
    ${description}      generate random string      20   prepare_service_description_[LETTERS]
    ${service_name}  set variable    prepare_${service_name}
    ${description}  set variable    prepare_${description}
    ${dic}     [200] API create service
    ...     ${test_admin_access_token}
    ...     ${service_group_id}
    ...     ${service_name}
    ...     ${currency}
    ...     ${description}
    ${response}     get from dictionary   ${dic}   response
    ${service_id}     get from dictionary   ${dic}   service_id
    set test variable      ${test_service_id}      ${service_id}
    set test variable      ${test_service_name}      ${service_name}

[Test][200] API create service in "${service_group_id}" service group id and currency "${currency}"
    ${service_name}      generate random string      20   prepare_service_name_[LETTERS]
    ${description}      generate random string      20   prepare_service_description_[LETTERS]
    ${service_name}  set variable    prepare_${service_name}
    ${description}  set variable    prepare_${description}
    ${dic}     [200] API create service
    ...     ${test_admin_access_token}
    ...     ${service_group_id}
    ...     ${service_name}
    ...     ${currency}
    ...     ${description}
    ${response}     get from dictionary   ${dic}   response
    ${service_id}     get from dictionary   ${dic}   service_id
    set test variable      ${test_service_id}      ${service_id}
    set test variable      ${test_service_name}      ${service_name}

[Test][200] API create service from name
    [Arguments]     ${service_name}          ${display_service_name}        ${display_service_name_local}
    ${currency}     set variable    VND
    ${description}      generate random string      20   prepare_service_description_[LETTERS]
    ${image_url}        set variable         https://goo.gl/s93acK
    ${service_name}  set variable    prepare_${service_name}
    ${display_service_name}  set variable    prepare_${display_service_name}
    ${display_service_name_local}   set variable    prepare_${display_service_name_local}
    ${description}  set variable    prepare_${description}
    ${dic}     [200] API create service
    ...     ${test_admin_access_token}
    ...     ${service_group_id}
    ...     ${service_name}
    ...     ${currency}
    ...     ${description}
    ...     ${image_url}
    ...     ${display_service_name}
    ...     ${display_service_name_local}

    ${response}     get from dictionary   ${dic}   response
    ${service_id}     get from dictionary   ${dic}   service_id
    set test variable      ${test_service_id}      ${service_id}
    set test variable      ${test_service_name}      ${service_name}
    set test variable      ${test_display_service_name}      ${display_service_name}

[Suite][200] API create service from name
    [Arguments]     ${service_name}          ${display_service_name}        ${display_service_name_local}
    ${currency}        set variable    VND
    ${description}      generate random string      10   prepare_[LETTERS]
    ${image_url}        set variable         https://goo.gl/s93acK
    ${service_name}  set variable    prepare_${service_name}
    ${display_service_name}  set variable    prepare_${display_service_name}
    ${display_service_name_local}   set variable    prepare_${display_service_name_local}
    ${description}  set variable    prepare_${description}
    ${dic}     [200] API create service
    ...     ${suite_admin_access_token}
    ...     ${suite_service_group_id}
    ...     ${service_name}
    ...     ${currency}
    ...     ${description}
    ...     ${image_url}
    ...     ${display_service_name}
    ...     ${display_service_name_local}

    ${response}     get from dictionary   ${dic}   response
    ${service_id}     get from dictionary   ${dic}   service_id
    set suite variable      ${suite_service_id}        ${service_id}
    set suite variable      ${suite_service_name}      ${service_name}

[Test][200] API create service command
    [Arguments]     ${command_id}   ${service_id}
    [200] API create service command
    ...     ${test_admin_access_token}
    ...     ${service_id}
    ...     ${command_id}


[Suite][200] API create service command
    [Arguments]     ${command_id}   ${service_id}
    [200] API create service command
    ...     ${suite_admin_access_token}
    ...     ${service_id}
    ...     ${command_id}

[200] API create profile wallet limitation
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      access_token     ${suite_admin_access_token}
    ${dict}     [200] API add profile wallet limitation   ${arg_dic}
    ${id}     get from dictionary      ${dict}             profile_wallet_limitation_id
    [Return]    ${dict}

[Suite][200] API create profile wallet limitation
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_id    ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     	${arg_dic}     client_secret    ${setup_admin_client_secret}
    ${dic}   API Create wallet limitation
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    set to dictionary   ${dic}   profile_wallet_limitation_id    ${response.json()['data']['id']}
    [Return]   ${dic}

[Test][200] API get service commands by service id
    [Arguments]     ${service_id}
    ${dic}     [200] API get service commands by service id
    ...     ${test_admin_access_token}
    ...     ${service_id}
    [Return]    ${dic}

[Suite][200] API get service commands by service id
    [Arguments]     ${service_id}
    ${dic}     [200] API get service commands by service id
    ...     ${suite_admin_access_token}
    ...     ${service_id}
    [Return]    ${dic}

[Test][200] API get order detail
    ${dic}     [200] API get order detail
    ...     ${test_agent_access_token}
    ...     ${test_order_id}
    [Return]    ${dic}

[Test][200] API create fee tier unlimited by service command id "${service_command_id}"
    [200] API create fee tier
    ...     ${test_admin_access_token}
    ...     ${service_command_id}
    ...     unlimit
    ...     ${param_is_null}
    ...     % rate
    ...     10
    ...     NON
    ...     ${param_is_null}
    ...     ${param_is_null}
    ...     Amount

    ${dic}     [200] API get list fee tiers by service command id
    ...     ${test_admin_access_token}
    ...     ${service_command_id}
    ${response}=    get from dictionary    ${dic}   response
    ${fee_tier_id}=  set variable    ${response.json()['data'][0]['fee_tier_id']}
    set test variable       ${fee_tier_id}      ${fee_tier_id}

[Suite][200] API create fee tier unlimited by service command id "${service_command_id}"
    [200] API create fee tier
    ...     ${suite_admin_access_token}
    ...     ${service_command_id}
    ...     unlimit
    ...     ${param_is_null}
    ...     % rate
    ...     10
    ...     NON
    ...     ${param_is_null}
    ...     ${param_is_null}
    ...     Amount

    ${dic}     [200] API get list fee tiers by service command id
    ...     ${suite_admin_access_token}
    ...     ${service_command_id}
    ${response}=    get from dictionary    ${dic}   response
    ${fee_tier_id}=  set variable    ${response.json()['data'][0]['fee_tier_id']}
    set suite variable       ${fee_tier_id}      ${fee_tier_id}

[Suite][200] API create fee tier unlimited by service command id "${service_command_id}" for agent commissions
    ${b}  	set variable    "b":{"type_first":"% rate","from_first":"Amount","amount_first":5.12}
    @{list_fee_tier_data}  create list     ${b}

    ${arg_dic}               create dictionary
    ...     fee_tier_condition=unlimit
    ...     condition_amount=null
    ...     fee_type=% rate
    ...     fee_amount=10
    ...     access_token=${suite_admin_access_token}
    ...     service_command_id=${service_command_id}
    ...     list_fee_tier_data=${list_fee_tier_data}
    [200] API create fee tier from A-O      ${arg_dic}

    ${dic}     [200] API get list fee tiers by service command id
    ...     ${suite_admin_access_token}
    ...     ${service_command_id}
    ${response}=    get from dictionary    ${dic}   response
    ${fee_tier_id}=  set variable    ${response.json()['data'][0]['fee_tier_id']}
    set suite variable       ${fee_tier_id}      ${fee_tier_id}

[Test][200] API create service command Payment with service id "${test_service_id}"
    [Test][200] API create service command
    ...     1
    ...     ${test_service_id}
    ${dic}     [Test][200] API get service commands by service id      ${test_service_id}
    set test variable       ${test_service_command_id}      ${dic.payment_service_command_id}

[Test][200] API create balance distribution
    ${1}  set variable    {"action_type":"Debit","actor_type":"Payer","sof_type_id":2,"specific_sof":null,"amount_type":"Amount","rate":null}
    ${2}  set variable    {"action_type":"Credit","actor_type":"Payee","sof_type_id":2,"specific_sof":null,"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${1}    ${2}
    [200] API update all balance distribution
    ...     ${fee_tier_id}
    ...     ${test_admin_access_token}
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}
    ...     ${balance_distributions}

[Test][200] API system user create user sof cash
    [Arguments]
    ...     ${user_id}
    ...     ${user_type_id}
    ...     ${currency}
    ${dic}      [200] API system user create user sof cash
    ...     ${user_id}
    ...     ${user_type_id}
    ...     ${currency}
    ...     ${test_admin_access_token}
    ${response}=    get from dictionary    ${dic}   response
    set test variable    ${test_sof_id}     ${response.json()['data']['sof_id']}
    [Return]    ${dic}

[Test][200] API create normal order
    [Arguments]
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${payee_user_id}
    ...     ${payee_user_type}
    ...     ${payee_user_ref_type}
    ...     ${payee_user_ref_value}
    ...     ${amount}
    ${product_name}         generate random string      20   prepare_product_name_[LETTERS]
    ${product_ref_1}        generate random string      20   prepare_service_ref_1_[LETTERS]
    ${product_ref_2}        generate random string      20   prepare_service_ref_2_[LETTERS]
    ${product_ref_3}        generate random string      20   prepare_service_ref_3_[LETTERS]
    ${product_ref_4}        generate random string      20   prepare_service_ref_4_[LETTERS]
    ${product_ref_5}        generate random string      20   prepare_service_ref_5_[LETTERS]

    ${ext_transaction_id}      generate random string      12   [NUMBERS]
    ${product_service_id}      generate random string      12   [NUMBERS]
    ${dic}     [200] API payment create normal order
    ...     ${payer_user_ref_value}
    ...     ${ext_transaction_id}
    ...     ${test_service_id}
    ...     ${test_service_name}
    ...     ${product_name}
    ...     ${product_ref_1}
    ...     ${product_ref_2}
    ...     ${product_ref_3}
    ...     ${product_ref_4}
    ...     ${product_ref_5}
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${payee_user_ref_type}
    ...     ${payee_user_ref_value}
    ...     ${payee_user_id}
    ...     ${payee_user_type}
    ...     ${amount}
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    ${order_id}=  get from dictionary   ${dic}   order_id
    set test variable       ${test_order_id}     ${order_id}

[Suite][200] API create normal order
    [Arguments]
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${payee_user_id}
    ...     ${payee_user_type}
    ...     ${payee_user_ref_type}
    ...     ${payee_user_ref_value}
    ...     ${amount}
    ${product_name}         generate random string      20   prepare_product_name_[LETTERS]
    ${product_ref_1}        generate random string      20   prepare_service_ref_1_[LETTERS]
    ${product_ref_2}        generate random string      20   prepare_service_ref_2_[LETTERS]
    ${product_ref_3}        generate random string      20   prepare_service_ref_3_[LETTERS]
    ${product_ref_4}        generate random string      20   prepare_service_ref_4_[LETTERS]
    ${product_ref_5}        generate random string      20   prepare_service_ref_5_[LETTERS]

    ${ext_transaction_id}      generate random string      12   [NUMBERS]
    ${product_service_id}      generate random string      12   [NUMBERS]
    ${dic}     [200] API payment create normal order
    ...     ${payer_user_ref_value}
    ...     ${ext_transaction_id}
    ...     ${suite_service_id}
    ...     ${suite_service_name}
    ...     ${product_name}
    ...     ${product_ref_1}
    ...     ${product_ref_2}
    ...     ${product_ref_3}
    ...     ${product_ref_4}
    ...     ${product_ref_5}
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${payee_user_ref_type}
    ...     ${payee_user_ref_value}
    ...     ${payee_user_id}
    ...     ${payee_user_type}
    ...     ${amount}
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    ${order_id}=  get from dictionary   ${dic}   order_id
    set suite variable       ${suite_order_id}     ${order_id}

[Test][200] API create artifact order
    [Arguments]
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${payee_user_id}
    ...     ${payee_user_type}
    ...     ${payee_user_ref_type}
    ...     ${payee_user_ref_value}
    ...     ${amount}
    ${product_name}         generate random string      20   prepare_product_name_[LETTERS]
    ${product_ref_1}        generate random string      20   prepare_service_ref_1_[LETTERS]
    ${product_ref_2}        generate random string      20   prepare_service_ref_2_[LETTERS]
    ${product_ref_3}        generate random string      20   prepare_service_ref_3_[LETTERS]
    ${product_ref_4}        generate random string      20   prepare_service_ref_4_[LETTERS]
    ${product_ref_5}        generate random string      20   prepare_service_ref_5_[LETTERS]

    ${ext_transaction_id}      generate random string      12   [NUMBERS]
    ${product_service_id}      generate random string      12   [NUMBERS]
    ${dic}     [200] API payment create artifact order
    ...     ${payer_user_ref_value}
    ...     ${ext_transaction_id}
    ...     ${test_service_id}
    ...     ${test_service_name}
    ...     ${product_name}
    ...     ${product_ref_1}
    ...     ${product_ref_2}
    ...     ${product_ref_3}
    ...     ${product_ref_4}
    ...     ${product_ref_5}
    ...     ${payer_user_ref_type}
    ...     ${payer_user_ref_value}
    ...     ${payee_user_ref_type}
    ...     ${payee_user_ref_value}
    ...     ${payee_user_id}
    ...     ${payee_user_type}
    ...     ${amount}
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    ${order_id}=  get from dictionary   ${dic}   order_id
    set test variable       ${test_order_id}     ${order_id}

[Test][200] API execute order
    [Arguments]     ${arg_dic}
    ${dic}      [200] API payment execute order     ${arg_dic}
    [Return]    ${dic}

[Test][200] API create SPI URL
     [Arguments]     ${arg_dic}
     ${dic}     [200] API create SPI URL    ${arg_dic}
     [Return]    ${dic}

[Test][200] API create payment pin
    [Arguments]
    ...     ${pin}
    ...     ${access_token}
    ${dic}      [200] API create payment pin
    ...     ${pin}
    ...     ${access_token}
    [Return]    ${dic}

[Suite][200] API add service group
    ${agent_type_name}             generate random string      10   agent_type_name_[LETTERS]
    ${agent_type_description}      generate random string      10   agent_type_description_[LETTERS]
    ${agent_type_name}  set variable    prepare_${agent_type_name}
    ${agent_type_description}  set variable    prepare_${agent_type_description}
    ${dic}     [200] API add service group
    ...     ${suite_admin_access_token}
    ...     ${agent_type_name}
    ...     ${agent_type_description}
    ${response}     get from dictionary   ${dic}   response
    ${service_group_id}     get from dictionary   ${dic}   service_group_id
    set suite variable   ${suite_service_group_id}       ${service_group_id}
    set suite variable   ${suite_service_group_name}     ${agent_type_name}

[Payment][Suite][200] - Create service in service group id
    [Arguments]     &{arg_dic}
    ${service_name}      generate random string      20   robot_prepare_service_name_[LETTERS]
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      currency          VND
    ${currency}     set variable    ${arg_dic.currency}
    ${description}      generate random string      20   robor_prepare_service_description_[LETTERS]
    ${service_name}  set variable    prepare_${service_name}
    ${description}  set variable    prepare_${description}
    ${dic}     [200] API create service
    ...     ${arg_dic.access_token}
    ...     ${arg_dic.service_group_id}
    ...     ${service_name}
    ...     ${currency}
    ...     ${description}
    ${response}     get from dictionary   ${dic}   response
    ${service_id}     get from dictionary   ${dic}   service_id
    set suite variable      ${suite_service_id}      ${service_id}
    set suite variable      ${suite_service_name}      ${service_name}

[Suite][200] API create service in "${service_group_id}" service group id allow debt
    ${service_name}      generate random string      20   robot_prepare_service_name_[LETTERS]
    ${currency}     set variable    VND
    ${description}      generate random string      20   robor_prepare_service_description_[LETTERS]
    ${service_name}  set variable    prepare_${service_name}
    ${description}  set variable    prepare_${description}
    ${dic}     [200] API create service
    ...     ${suite_admin_access_token}
    ...     ${service_group_id}
    ...     ${service_name}
    ...     ${currency}
    ...     ${description}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${EMPTY}
    ...     ${TRUE}

    ${response}     get from dictionary   ${dic}   response
    ${service_id}     get from dictionary   ${dic}   service_id
    set suite variable      ${suite_service_id}      ${service_id}
    set suite variable      ${suite_service_name}      ${service_name}

[Suite][200] API create service command Payment with service id "${service_id}"
    ${dic}     [200] API create service command
                ...     ${suite_admin_access_token}
                ...     ${service_id}
                ...     1

    ${dic}     [200] API get service commands by service id
                ...     ${suite_admin_access_token}
                ...     ${service_id}
    set suite variable       ${suite_service_command_id}        ${dic.payment_service_command_id}

[Suite][200] API create fee tier unlimited from A-O by service command id "${service_command_id}"
     [Arguments]
     ${a}  	set variable    "a":{"type_first":"Flat value","amount_first":10}
     ${b}  	set variable    "b":{"type_first":"% rate","from_first":"Amount","amount_first":5.12}
     ${c}  	set variable    "c":{"type_first":"Flat value","amount_first":15}
     @{list_fee_tier_data}  create list     ${a}, ${b}, ${c}
#     @{items}=    Create List   a   b   c   d   e   f   g   h   i   j   k   l   m   n   o

#    ${list_fee_tier_data}   set variable      ${empty}
#      : FOR    ${item}    IN    @{items}
#    \     ${item_json}  set variable    "${item}":{"type_first":"Flat value","amount_first":0},
#    \     ${list_fee_tier_data}   set variable  ${list_fee_tier_data}${item_json}

    Log List     ${list_fee_tier_data}

    ${arg_dic}               create dictionary
        ...     fee_tier_condition=unlimit
        ...     access_token=${suite_admin_access_token}
        ...     service_command_id=${suite_service_command_id}
        ...     list_fee_tier_data=${list_fee_tier_data}
        ...     fee_type=Flat value
        ...     fee_amount=10

    [200] API create fee tier from A-O      ${arg_dic}

    ${dic}     [200] API get list fee tiers by service command id
    ...     ${suite_admin_access_token}
    ...     ${service_command_id}
    ${response}=    get from dictionary    ${dic}   response
    ${fee_tier_id}=  set variable    ${response.json()['data'][0]['fee_tier_id']}
    set suite variable       ${fee_tier_id}      ${fee_tier_id}

[Suite][200] API create balance distribution with fee commission
    ${1}  set variable    {"action_type":"debit","actor_type":"Payer","sof_type_id":1,"specific_sof":null,"amount_type":"Amount","rate":null}
    ${2}  set variable    {"action_type":"credit","actor_type":"Payee","sof_type_id":1,"specific_sof":null,"amount_type":"Amount","rate":null}
    ${3}  set variable    {"action_type":"debit","actor_type":"Payer","sof_type_id":1,"specific_sof":null,"amount_type":"Fee","rate":null}
    ${4}  set variable    {"action_type":"credit","actor_type":"Payee","sof_type_id":1,"specific_sof":null,"amount_type":"Fee","rate":null}
    ${5}  set variable    {"action_type":"debit","actor_type":"Payer","sof_type_id":1,"specific_sof":null,"amount_type":"Fee Rate","rate":50}
    ${6}  set variable    {"action_type":"credit","actor_type":"Payee","sof_type_id":1,"specific_sof":null,"amount_type":"Fee Rate","rate":50}
    @{balance_distributions}  create list     ${1}    ${2}      ${3}    ${4}        ${5}    ${6}
    [200] API update all balance distribution
    ...     ${fee_tier_id}
    ...     ${suite_admin_access_token}
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}
    ...     ${balance_distributions}

[Suite][200] API create balance distribution
    ${1}  set variable    {"action_type":"debit","actor_type":"Payer","sof_type_id":2,"specific_sof":null,"amount_type":"Amount","rate":null}
    ${2}  set variable    {"action_type":"credit","actor_type":"Payee","sof_type_id":2,"specific_sof":null,"amount_type":"Amount","rate":null}
    ${3}  set variable    {"action_type":"debit","actor_type":"Payer","sof_type_id":2,"specific_sof":null,"amount_type":"Fee","rate":null}
    ${4}  set variable    {"action_type":"credit","actor_type":"Payer","sof_type_id":2,"specific_sof":null,"amount_type":"Fee Rate","rate":50}

    @{balance_distributions}  create list     ${1}    ${2}   ${3}    ${4}
    [200] API update all balance distribution
    ...     ${fee_tier_id}
    ...     ${suite_admin_access_token}
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}
    ...     ${balance_distributions}

[200] Company Agent fund in "${amount}" money for "${payee_user_type}": "${payee_user_id}" with "${service_id}" and "${service_name}"
    ${arg_dic}   create dictionary      username=${setup_company_agent_account_id}      password=${setup_company_agent_password_encrypted}
    ${dic}    [200] API agent authentication        ${arg_dic}
    ${company_agent_access_token}     get from dictionary      ${dic}     access_token

    [200] API add company balance    VND         50000000        ${setup_admin_access_token}

    ${product_name}         generate random string      20   company_agent_fund_in_prepare_product_name_[LETTERS]
    ${product_ref_1}        generate random string      20   prepare_service_ref_1_[LETTERS]
    ${product_ref_2}        generate random string      20   prepare_service_ref_2_[LETTERS]
    ${product_ref_3}        generate random string      20   prepare_service_ref_3_[LETTERS]
    ${product_ref_4}        generate random string      20   prepare_service_ref_4_[LETTERS]
    ${product_ref_5}        generate random string      20   prepare_service_ref_5_[LETTERS]
    ${ext_transaction_id}      generate random string      12   [NUMBERS]
    ${dic}     [200] API payment create normal order
    ...     ${company_agent_access_token}
    ...     ${ext_transaction_id}
    ...     ${service_id}
    ...     ${service_name}
    ...     ${product_name}
    ...     ${product_ref_1}
    ...     ${product_ref_2}
    ...     ${product_ref_3}
    ...     ${product_ref_4}
    ...     ${product_ref_5}
    ...     access token
    ...     ${company_agent_access_token}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${NULL}
    ...     ${NULL}
    ...     ${payee_user_id}
    ...     ${payee_user_type}
    ...     ${amount}
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    ${order_id}=  get from dictionary   ${dic}   order_id

    ${execute_order_dic}     create dictionary      access_token=${company_agent_access_token}    order_id=${order_id}
    ${dic}      [200] API payment execute order     ${execute_order_dic}

[Test][200] Company Agent fund in "${amount}" money for "${payee_user_type}": "${payee_user_id}"
    ${arg_dic}   create dictionary      username=${setup_company_agent_account_id}      password=${setup_company_agent_password_encrypted}
    ${dic}    [200] API agent authentication        ${arg_dic}
    ${company_agent_access_token}     get from dictionary      ${dic}     access_token

    [200] API add company balance    VND         50000000        ${suite_admin_access_token}

    [Test][200] API create normal order
    ...     access token
    ...     ${company_agent_access_token}
    ...     ${payee_user_id}
    ...     agent
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${amount}
    ${response}=    get from dictionary    ${dic}   response

    ${execute_order_dic}     create dictionary      access_token=${company_agent_access_token}    order_id=${test_order_id}
    ${dic}      [200] API payment execute order     ${execute_order_dic}

[Suite][200] Company Agent fund in "${amount}" money for "${payee_user_type}": "${payee_user_id}"
    ${arg_dic}   create dictionary      username=${setup_company_agent_account_id}      password=${setup_company_agent_password_encrypted}
    ${dic}    [200] API agent authentication        ${arg_dic}
    ${company_agent_access_token}     get from dictionary      ${dic}     access_token

    [200] API add company balance    VND         50000000        ${suite_admin_access_token}

    [Suite][200] API create normal order
    ...     access token
    ...     ${company_agent_access_token}
    ...     ${payee_user_id}
    ...     agent
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${amount}
    ${response}=    get from dictionary    ${dic}   response

    ${execute_order_dic}     create dictionary      access_token=${company_agent_access_token}    order_id=${suite_order_id}
    ${dic}      [200] API payment execute order     ${execute_order_dic}

[Suite] Prepare service structure with tier from A-O
    [Arguments]    &{arg_dic}
    [Suite][200] API add service group
     set suite variable   ${suite_setup_service_group_id}       ${suite_service_group_id}
    [Payment][Suite][200] - Create service in service group id   access_token=${suite_admin_access_token}   service_group_id=${suite_setup_service_group_id}    currency=${arg_dic.currency}
     set suite variable   ${suite_setup_service_id}       ${suite_service_id}
     set suite variable   ${suite_setup_service_name}       ${suite_service_name}
    [Suite][200] API create service command Payment with service id "${suite_setup_service_id}"
     set suite variable   ${suite_setup_service_command_id}       ${suite_service_command_id}
    [Suite][200] API create fee tier unlimited from A-O by service command id "${suite_setup_service_command_id}"
    [Suite][200] API create balance distribution

[Suite] Prepare service structure for fund_in money
    [Suite][200] API add service group
     set suite variable   ${suite_fund_in_service_group_id}       ${suite_service_group_id}
    [Payment][Suite][200] - Create service in service group id   access_token=${suite_admin_access_token}   service_group_id=${suite_fund_in_service_group_id}       currency=VND
     set suite variable   ${suite_fund_in_service_id}       ${suite_service_id}
     set suite variable   ${suite_fund_in_service_name}       ${suite_service_name}
    [Suite][200] API create service command Payment with service id "${suite_fund_in_service_id}"
     set suite variable   ${suite_fund_in_service_command_id}       ${suite_service_command_id}
    [Suite][200] API create fee tier unlimited by service command id "${suite_fund_in_service_command_id}"
    set suite variable   ${setup_admin_access_token}   ${suite_admin_access_token}
    [Suite][200] API create balance distribution

[Suite] Prepare service structure
    [Arguments]    &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      currency     VND
    [Suite][200] API add service group
    set suite variable   ${suite_fund_in_service_group_id}       ${suite_service_group_id}
    [Payment][Suite][200] - Create service in service group id   access_token=${suite_admin_access_token}   service_group_id=${suite_fund_in_service_group_id}    currency=${arg_dic.currency}
    set suite variable   ${suite_fund_in_service_id}       ${suite_service_id}
    set suite variable   ${suite_fund_in_service_name}       ${suite_service_name}
    [Suite][200] API create service command Payment with service id "${suite_fund_in_service_id}"
    set suite variable   ${suite_fund_in_service_command_id}       ${suite_service_command_id}
    [Suite][200] API create fee tier unlimited by service command id "${suite_fund_in_service_command_id}"
    set suite variable   ${setup_admin_access_token}   ${suite_admin_access_token}
    [Suite][200] API create balance distribution

[Suite] Prepare service structure for agent commission
    [Arguments]    &{arg_dic}
    [Suite][200] API add service group
    set suite variable   ${suite_fund_in_service_group_id}       ${suite_service_group_id}
    [Payment][Suite][200] - Create service in service group id   access_token=${suite_admin_access_token}   service_group_id=${suite_fund_in_service_group_id}    currency=${arg_dic.currency}
    set suite variable   ${suite_fund_in_service_id}       ${suite_service_id}
    set suite variable   ${suite_fund_in_service_name}       ${suite_service_name}
    [Suite][200] API create service command Payment with service id "${suite_fund_in_service_id}"
    set suite variable   ${suite_fund_in_service_command_id}       ${suite_service_command_id}
    [Suite][200] API create fee tier unlimited by service command id "${suite_fund_in_service_command_id}" for agent commissions
    set suite variable   ${setup_admin_access_token}   ${suite_admin_access_token}
    [Suite][200] API create balance distribution with fee commission

[Test][200] add service group
    [Arguments]     ${access_token}     ${service_group_name}
    ${dic}  [200] API add service group     ${access_token}     ${service_group_name}
    ${service_group_id}     get from dictionary     ${dic}      service_group_id
    set test variable       ${test_service_group_id}     ${service_group_id}
    set test variable       ${test_service_group_name}     ${service_group_name}

[Test][200] create service
    [Arguments]     ${access_token}        ${service_group_id}      ${service_name}      ${currency}
    ${dic}  [200] API create service    ${access_token}        ${service_group_id}      ${service_name}      ${currency}
    ${service_id}   get from dictionary     ${dic}      service_id
    set test variable   ${test_service_id}      ${service_id}
    set test variable   ${test_service_name}      ${service_name}

[Test][200] create service command
    [Arguments]     ${access_token}    ${service_id}    ${command_id}
    ${dic}  [200] API create service command    ${access_token}    ${service_id}    ${command_id}

[Test][200][Data] create service in "${currency}" currency
    ${service_group_name}=    generate random string  10   [LETTERS]
    [Test][200] add service group      ${suite_admin_access_token}     ${service_group_name}

    ${service_name}=    generate random string  10   [LETTERS]
    [Test][200] create service      ${suite_admin_access_token}        ${test_service_group_id}      ${service_name}      ${currency}

[Test][200][Data] delete service by "${service_id}" service ID
    [200] API delete service     ${suite_admin_access_token}    ${service_id}

[Test] Unlink bank by bank for ${bank_sof_id}
    ${arg_dic}     create dictionary    bank_sof_id=${bank_sof_id}
    [200] API user can unlink bank by bank      ${arg_dic}

[Test] Delete customer by ${customer_id}
    ${arg_dic}     create dictionary    customer_id=${customer_id}
    [200] Call API to delete customer      ${arg_dic}

[200] Call API to delete customer
    [Arguments]     ${arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_id     ${setup_admin_client_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      client_secret     ${setup_admin_client_secret}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}     access_token       ${suite_admin_access_token}
    ${dic}  API delete customer    ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Suite] Register customer
    ${username}         generate random string      10          [LOWER]
    set suite variable        ${suite_first_name}       first_name_${username}
    set suite variable        ${suite_last_name}       last_name_${username}
    ${mobile}         generate random string      15          [NUMBERS]
    ${customer_unique_reference}    generate random string      15          [LOWER][NUMBERS]
    ${customer_arg_dic}     create dictionary       unique_reference=${customer_unique_reference}       identity_type_id=1      username=${mobile}      password=${setup_password_customer_encrypted}       first_name=${suite_first_name}      last_name=${suite_last_name}    primary_mobile_number=${mobile}
    ${customer_dic}    [200] API register customer        ${customer_arg_dic}
    ${customer_id}      get from dictionary      ${customer_dic}     id
    set suite variable           ${suite_customer_id}         ${customer_id}
    set suite variable           ${suite_mobile_number}         ${mobile}

[Test] Register customer
    ${username}         generate random string      10          [LOWER]
    set test variable        ${test_first_name}       first_name_${username}
    set test variable        ${test_last_name}       last_name_${username}
    ${mobile}         generate random string      15          [NUMBERS]
    ${customer_unique_reference}    generate random string      15          [LOWER][NUMBERS]
    ${customer_arg_dic}     create dictionary       unique_reference=${customer_unique_reference}       identity_type_id=1      username=${mobile}      password=${setup_password_customer_encrypted}       first_name=${test_first_name}      last_name=${test_last_name}    primary_mobile_number=${mobile}
    ${customer_dic}    [200] API register customer        ${customer_arg_dic}
    ${customer_id}      get from dictionary      ${customer_dic}     id
    set test variable           ${test_customer_id}         ${customer_id}
    set test variable           ${test_mobile_number}         ${mobile}

[Suite][200] Create bank for '${currency}'
    ${name}    generate random string      15          [LOWER][NUMBERS]
    ${bin}    generate random string      10         [NUMBERS]
    ${account_number}    generate random string      10         [NUMBERS]
    ${account_name}    generate random string      10         [LETTERS]
    ${connection_timeout}    generate random string      4         123456789
    ${read_timeout}    generate random string      4         123456789
    ${cancel_url}    generate random string      10         [LETTERS]
    ${cancel_url}    set variable      http://${cancel_url}
    ${check_status_url}    generate random string      10         [LETTERS]
    ${check_status_url}    set variable      http://${check_status_url}
    ${debit_url}    set variable      ${bank_debit_url}
    ${credit_url}    set variable      ${bank_debit_url}

    ${bank_arg_dic}     create dictionary       name=${name}    pin=${bin}     debit_url=${debit_url}           credit_url=${credit_url}
    ...     bank_account_number=${account_number}     bank_account_name=${account_name}           currency=${currency}
    ...     connection_timeout=${connection_timeout}    read_timeout=${read_timeout}    cancel_url=${cancel_url}
    ...     check_status_url=${check_status_url}
    ${bank_dic}     [200] API create bank       ${bank_arg_dic}
    ${bank_id}      get from dictionary      ${bank_dic}     id
    set suite variable           ${suite_bank_id}         ${bank_id}

[Suite][200] Prepare bank for '${currency}' with name '${bank_name}' and account number '${account_number}'
    ${bin}    generate random string      10         [NUMBERS]
    ${account_name}    generate random string      10         [LETTERS]
    ${connection_timeout}    generate random string      4         123456789
    ${read_timeout}    generate random string      4         123456789
    ${cancel_url}    generate random string      10         [LETTERS]
    ${cancel_url}    set variable      http://${cancel_url}
    ${check_status_url}    generate random string      10         [LETTERS]
    ${check_status_url}    set variable      http://${check_status_url}
    ${debit_url}    set variable      ${bank_debit_url}
    ${credit_url}    set variable      ${bank_debit_url}

    ${bank_arg_dic}     create dictionary       name=${bank_name}    pin=${bin}     debit_url=${debit_url}           credit_url=${credit_url}
    ...     bank_account_number=${account_number}     bank_account_name=${account_name}           currency=${currency}
    ...     connection_timeout=${connection_timeout}    read_timeout=${read_timeout}    cancel_url=${cancel_url}
    ...     check_status_url=${check_status_url}
    ${bank_dic}     [200] API create bank       ${bank_arg_dic}
    ${bank_id}      get from dictionary      ${bank_dic}     id
    set suite variable           ${suite_bank_id}         ${bank_id}
    set test variable           ${test_bank_name}         ${bank_name}

[Test] Prepare sof-bank for customer '${customer_id}' for '${currency}'
    ${bank_account_name}    generate random string      10          [LOWER]
    ${bank_account_number}    generate random string      10         [NUMBERS]
    ${ext_bank_reference}   generate random string      10          [LOWER]
    ${sof_bank_arg_dic}     create dictionary    user_id=${customer_id}    user_type_id=1        bank_id=${suite_bank_id}      bank_account_name=${bank_account_name}      bank_account_number=${bank_account_number}        currency=${currency}      ext_bank_reference=${ext_bank_reference}
    ...     access_token=${suite_admin_access_token}
    ${sof_bank_dic}         [200] API user can link bank by bank      ${sof_bank_arg_dic}
    ${sof_bank_id}      get from dictionary      ${sof_bank_dic}     sof_id
    set test variable           ${test_sof_bank_id}         ${sof_bank_id}
    set test variable           ${test_bank_account_number}         ${bank_account_number}
    set test variable           ${test_bank_account_name}         ${bank_account_name}
    set test variable           ${test_currency}         ${currency}

[Payment][Suite] Prepare sof-bank for customer '${customer_id}' for '${currency}'
    ${bank_account_name}    generate random string      10          [LOWER]
    ${bank_account_number}    generate random string      10         [NUMBERS]
    ${ext_bank_reference}   generate random string      10          [LOWER]
    ${sof_bank_arg_dic}     create dictionary    user_id=${customer_id}    user_type_id=1        bank_id=${suite_bank_id}      bank_account_name=${bank_account_name}      bank_account_number=${bank_account_number}        currency=${currency}      ext_bank_reference=${ext_bank_reference}
    ...     access_token=${suite_admin_access_token}
    ${sof_bank_dic}         [200] API user can link bank by bank      ${sof_bank_arg_dic}
    ${sof_bank_id}      get from dictionary      ${sof_bank_dic}     sof_id
    set suite variable           ${suite_sof_bank_id}         ${sof_bank_id}

[Test] Prepare sof-bank for setup agent for '${currency}'
    ${bank_account_name}    generate random string      10          [LOWER]
    ${bank_account_number}    generate random string      10         [NUMBERS]
    ${ext_bank_reference}   generate random string      10          [LOWER]
    ${sof_bank_arg_dic}     create dictionary    user_id=${suite_agent_id}    user_type_id=2        bank_id=${suite_bank_id}      bank_account_name=${bank_account_name}      bank_account_number=${bank_account_number}        currency=${currency}       ext_bank_reference=${ext_bank_reference}
    ...     access_token=${suite_admin_access_token}
    ${sof_bank_dic}         [200] API user can link bank by bank      ${sof_bank_arg_dic}
    ${sof_bank_id}      get from dictionary      ${sof_bank_dic}     sof_id
    set test variable           ${test_sof_bank_id}         ${sof_bank_id}
    set test variable           ${test_bank_account_number}         ${bank_account_number}
    set test variable           ${test_bank_account_name}         ${bank_account_name}
    set test variable           ${test_ext_bank_reference}         ${ext_bank_reference}
    set test variable           ${test_currency}         ${currency}

[Test] Create customer and fundin
    ${username}                     generate random string      10          [LOWER]
    ${customer_unique_reference}    generate random string      15          [LOWER][NUMBERS]
    ${arg_dic}  create dictionary
    ...     unique_reference=${customer_unique_reference}
    ...     identity_type_id=1
    ...     username=${username}
    ...     password=${setup_password_customer_encrypted}
    ...     password_encrypted_utf8=${setup_password_customer_encrypted_utf8}
    ...     currency=VND,USD

    ${cus_dic}  [Customer][Reuse][200] - create customer    ${arg_dic}
    ${test_customer_id}      get from dictionary      ${cus_dic}     customer_id
    set test variable   ${test_customer_id}      ${test_customer_id}
    Login as Company Agent
    API system user create a company agent cash source of fund with currency
    ...     VND
    ...     ${test_admin_access_token}
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}

    [Prepare] - Prepare service structure
    [200] API system user update company agent cash balance
    ...     VND
    ...     5000000
    ...     ${test_admin_access_token}
    [Test][200] API create normal order
    ...     access token
    ...     ${test_agent_company_access_token}
    ...     ${test_customer_id}
    ...     customer
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     500000

    ${execute_order_dic}     create dictionary    access_token=${test_agent_company_access_token}    order_id=${test_order_id}
    [Test][200] API execute order      ${execute_order_dic}

[Prepare] - Create an order and execute order
    [Arguments]     ${service_name}          ${display_service_name}        ${display_service_name_local}
    [System_User][Test][200] - login as setup system user
    [Prepare] - Prepare service structure from service name       ${service_name}          ${display_service_name}        ${display_service_name_local}
    set test variable   ${test_order_service_name}      ${test_service_name}
    set test variable   ${test_service_id}      ${service_group_id}
    [Test][200] API create normal order
    ...     access token
    ...     ${test_customer_access_token}
    ...     ${test_customer_id}
    ...     agent
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     10000
     ${execute_order_dic}     create dictionary      access_token=${test_agent_access_token}    order_id=${test_order_id}
     set test variable   ${test_order_id}      ${test_order_id}
     ${response}        [200] API payment execute order     ${execute_order_dic}

[Prepare] - Prepare service structure from service name
    ${service_name}     generate random string  12    [LETTERS]
    ${display_service_name}     generate random string  12    [LETTERS]
    ${display_service_name_local}     generate random string  12    [LETTERS]
    [Payment][Test][200] - Create service group  access_token=${test_admin_access_token}
    [Test][200] API create service from name       ${service_name}          ${display_service_name}        ${display_service_name_local}
    [Test][200] API create service command Payment with service id "${test_service_id}"
    [Test][200] API create fee tier unlimited by service command id "${test_service_command_id}"
    [Test][200] API create balance distribution
    [Test][200] API add service whitelist into summary transaction report

[Test][200] Prepare service structure from service name
    ${service_name}     generate random string  12    [LETTERS]
    ${display_service_name}     generate random string  12    [LETTERS]
    ${display_service_name_local}     generate random string  12    [LETTERS]
    set test variable   ${test_admin_access_token}      ${suite_admin_access_token}
    [Payment][Test][200] - Create service group  access_token=${test_admin_access_token}
    [Test][200] API create service from name       ${service_name}          ${display_service_name}        ${display_service_name_local}
    [Test][200] API create service command Payment with service id "${test_service_id}"
    [Test][200] API create fee tier unlimited by service command id "${test_service_command_id}"
    [Test][200] API create balance distribution
    [Test][200] API add service whitelist into summary transaction report

[Suite][200] Prepare service structure from service name
    ${service_name}             generate random string  12    [LETTERS]
    ${display_service_name}     generate random string  12    [LETTERS]
    ${display_service_name_local}     generate random string  12    [LETTERS]
    [Suite][200] API add service group
    [Suite][200] API create service from name       ${service_name}          ${display_service_name}        ${display_service_name_local}
    [Suite][200] API create service command Payment with service id "${suite_service_id}"
    [Suite][200] API create fee tier unlimited by service command id "${suite_service_command_id}"
    [Suite][200] API create balance distribution
    [Suite][200] API add service whitelist into summary transaction report

[Suite] Prepare service structure allow debt
    [Suite] Prepare service structure
    [Suite][200] API add service group
    [Suite][200] API create service in "${suite_service_group_id}" service group id allow debt
    [Suite][200] API create service command Payment with service id "${suite_service_id}"
    [Suite][200] API create fee tier unlimited by service command id "${suite_service_command_id}"
    [Suite][200] API create balance distribution
    
[Test][200] Prepare service structure from service name without balance distribution
    ${service_name}     generate random string  12    [LETTERS]
    ${display_service_name}     generate random string  12    [LETTERS]
    ${display_service_name_local}     generate random string  12    [LETTERS]
    set test variable   ${test_admin_access_token}      ${suite_admin_access_token}
    [Payment][Test][200] - Create service group  access_token=${test_admin_access_token}
    [Test][200] API create service from name       ${service_name}          ${display_service_name}        ${display_service_name_local}
    [Test][200] API create service command Payment with service id "${test_service_id}"
    [Test][200] API create fee tier unlimited by service command id "${test_service_command_id}"
    [Test][200] API add service whitelist into summary transaction report

[Test][200] API create balance distribution with actor type of debit is payee
    ${1}  set variable    {"action_type":"Debit","actor_type":"Payee","sof_type_id":2,"specific_sof":null,"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${1}
    [200] API update all balance distribution
    ...     ${fee_tier_id}
    ...     ${test_admin_access_token}
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}
    ...     ${balance_distributions}

[Test][200] API create balance distribution with actor type of credit is payee
    ${1}  set variable    {"action_type":"Credit","actor_type":"Payee","sof_type_id":2,"specific_sof":null,"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${1}
    [200] API update all balance distribution
    ...     ${fee_tier_id}
    ...     ${test_admin_access_token}
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}
    ...     ${balance_distributions}

[Test][200] API create balance distribution with actor type is specific
    ${1}  set variable    {"action_type":"Debit","actor_type":"Specific ID","sof_type_id":2,"specific_sof":${test_specific_sof_id},"specific_actor_id":${test_specific_id},"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${1}
    [200] API update all balance distribution
    ...     ${fee_tier_id}
    ...     ${test_admin_access_token}
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}
    ...     ${balance_distributions}

[Test][200] API create balance distribution with action type is credit and actor type is Beneficiary
    ${1}  set variable    {"action_type":"Credit","actor_type":"Payee's Beneficiary","amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${1}
    [200] API update all balance distribution
    ...     ${fee_tier_id}
    ...     ${test_admin_access_token}
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}
    ...     ${balance_distributions}

[Test][200] API create balance distribution with actor type is initiator
    ${1}  set variable    {"action_type":"Credit","actor_type":"Payee's Beneficiary","amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${1}
    [200] API update all balance distribution
    ...     ${fee_tier_id}
    ...     ${test_admin_access_token}
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}
    ...     ${balance_distributions}

[Test][200] API add service whitelist into summary transaction report
    [200] API add service ids to report service whitelist   access_token=${suite_admin_access_token}    report_type_id=1     service_ids=${test_service_id}

[Suite][200] API add service whitelist into summary transaction report
    [200] API add service ids to report service whitelist   access_token=${suite_admin_access_token}    report_type_id=1     service_ids=${suite_service_id}

[Payment][Test][200] - create normal order and execute
    [Test][200] API create normal order
    ...     access token
    ...     ${test_agent_access_token}
    ...     ${test_agent_id}
    ...     agent
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     10000
    ${execute_order_dic}     create dictionary      access_token=${test_agent_access_token}    order_id=${test_order_id}
    set test variable   ${test_order_id}      ${test_order_id}
    ${dic}        [200] API payment execute order     ${execute_order_dic}
    ${response}    get from dictionary     ${dic}    response
    should be equal as strings        ${response.json()['status']['code']}    success
    should be equal as strings        ${response.json()['status']['message']}      Success

[Test][Fail] Create normal order and execute fail
    [Test][200] API create normal order
    ...     access token
    ...     ${test_agent_access_token}
    ...     ${test_agent_id}
    ...     agent
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     10000
    ${execute_order_dic}     create dictionary      access_token=${test_agent_access_token}    order_id=${test_order_id}    code=500
    set test variable   ${test_order_id}      ${test_order_id}
    ${dic}        [Fail] API payment execute order     ${execute_order_dic}

[Payment][Test][200] - create normal order for payer and payee then execute
      [Test][200] API create normal order
    ...     access token
    ...     ${test_payer_access_token}
    ...     ${test_payee_id}
    ...     agent
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     10000
    ${execute_order_dic}     create dictionary      access_token=${test_payer_access_token}    order_id=${test_order_id}
    set test variable   ${test_order_id}      ${test_order_id}
    ${dic}        [200] API payment execute order     ${execute_order_dic}
    ${response}    get from dictionary     ${dic}    response
    set test variable   ${test_result_dic}  ${dic}
    should be equal as strings        ${response.json()['status']['code']}    success
    should be equal as strings        ${response.json()['status']['message']}      Success

[Test][Fail] Create normal order for payer and payee then execute
      [Test][200] API create normal order
    ...     access token
    ...     ${test_payer_access_token}
    ...     ${test_payee_id}
    ...     agent
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     10000
    ${execute_order_dic}     create dictionary      access_token=${test_payer_access_token}    order_id=${test_order_id}   code=400
    set test variable   ${test_order_id}      ${test_order_id}
    ${dic}        [Fail] API payment execute order     ${execute_order_dic}
    set test variable   ${test_result_dic}  ${dic}

[Test][Fail] Create normal order for beneficiary then execute
      [Test][200] API create normal order
    ...     access token
    ...     ${test_main_beneficiary_access_token}
    ...     ${test_payee_id}
    ...     agent
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     10000
    ${execute_order_dic}     create dictionary      access_token=${test_payer_access_token}    order_id=${test_order_id}   code=400
    set test variable   ${test_order_id}      ${test_order_id}
    ${dic}        [Fail] API payment execute order     ${execute_order_dic}
    set test variable   ${test_result_dic}  ${dic}

[Suite][200] User link bank by bank
    [Arguments]     ${access_token}     ${user_id}      ${user_type_id}     ${bank_id}      ${currency}
    ${bank_account_name}    generate random string      10          [LOWER]
    ${bank_account_number}    generate random string      10         [NUMBERS]
    ${ext_bank_reference}   generate random string      10          [LOWER]
    ${sof_bank_arg_dic}     create dictionary    user_id=${user_id}    user_type_id=${user_type_id}        bank_id=${bank_id}
    ...     bank_account_name=${bank_account_name}      bank_account_number=${bank_account_number}        currency=${currency}
    ...     ext_bank_reference=${ext_bank_reference}    access_token=${access_token}
    ${bank_dic}         [200] API user can link bank by bank      ${sof_bank_arg_dic}
    set suite variable           ${suite_sof_bank_id}         ${bank_dic.sof_id}
    set suite variable           ${suite_bank_account_number}         ${bank_account_number}
    set suite variable           ${suite_bank_account_name}         ${bank_account_name}
    set suite variable           ${suite_ext_bank_reference}         ${ext_bank_reference}
    set suite variable           ${suite_currency}         ${currency}
    set suite variable           ${suite_bank_token}         ${bank_dic.bank_token}

[Suite][200] - User link bank by bank
    [Arguments]     ${access_token}     ${user_id}      ${user_type_id}     ${bank_id}      ${currency}     ${output}
    ${bank_account_name}    generate random string      10          [LOWER]
    ${bank_account_number}    generate random string      10         [NUMBERS]
    ${ext_bank_reference}   generate random string      10          [LOWER]
    ${sof_bank_arg_dic}     create dictionary    user_id=${user_id}    user_type_id=${user_type_id}        bank_id=${bank_id}
    ...     bank_account_name=${bank_account_name}      bank_account_number=${bank_account_number}        currency=${currency}
    ...     ext_bank_reference=${ext_bank_reference}    access_token=${access_token}
    ${bank_dic}         [200] API user can link bank by bank      ${sof_bank_arg_dic}
    [Common] - Set variable   name=${output}      value=${bank_dic.sof_id}

[Suite][200] link bank for water agent in "${currency}" currency
    [Suite][200] User link bank by bank      ${suite_admin_access_token}     ${suite_water_agent_id}     2      ${suite_bank_id}     ${currency}

[Suite][200] create card design
    [Arguments]    ${access_token}     ${currency}
    ${provider_name}    generate random string  10    [LETTERS]
    set suite variable   ${suite_provider_name}       ${provider_name}
    ${dic}   [200] API add provider
    ...     ${suite_provider_name}
    ...     ${access_token}
    ${provider_id}     get from dictionary      ${dic}      id
    set suite variable   ${suite_provider_id}    ${provider_id}
    ${card_design_name}  generate random string  10    [LETTERS]
    set suite variable    ${suite_card_design_name}    ${card_design_name}

    ${card_account_name}   generate random string  20    [LETTERS]
    set suite variable    ${suite_card_account_name}       ${card_account_name}
    ${pattern}          generate random string  6   [NUMBERS]
    set suite variable    ${suite_pattern}       ${pattern}

    ${dic}  [200] API add card design
    ...    ${suite_provider_id}
    ...    ${suite_card_account_name}
    ...    1
    ...    true
    ...    ${currency}
    ...    ${suite_pattern}
    ...    ${pre_link_url}
    ...    10000
    ...    ${link_url}
    ...    10000
    ...    ${un_link_url}
    ...    10000
    ...    ${debit_url}
    ...    10000
    ...    ${credit_url}
    ...    10000
    ...    ${check_status_url}
    ...    10000
    ...    ${cancel_url}
    ...    10000
    ...    ${pre_sof_order_url}
    ...    10000
    ...    ${access_token}
    set suite variable      ${suite_card_design_id}    ${dic.id}

[Suite][200] create card design in "${currency}" currency
    [Suite][200] create card design     ${suite_admin_access_token}     ${currency}

[Suite][200] prelink card
    [Arguments]     ${access_token}     ${currency}
    ${card_account_name}   generate random string  20   [NUMBERS]
    set suite variable    ${suite_card_account_name}         ${card_account_name}
    ${citizen_id}   generate random string  6   [NUMBERS]
    set suite variable    ${suite_citizen_id}         ${citizen_id}
    ${cvv_cvc}    generate random string  3   [NUMBERS]
    set suite variable   ${suite_cvv_cvc}  ${cvv_cvc}
    ${pin}  generate random string  15   [NUMBERS]
    set suite variable  ${suite_pin}   ${pin}
    ${mobile_number}  generate random string  15   [NUMBERS]
    set suite variable  ${suite_mobile_number}  ${mobile_number}
    ${billing_address}  generate random string  15   [LETTERS]
    set suite variable    ${suite_billing_address}   ${billing_address}
    ${card_number_last_8}   generate random string  8   [NUMBERS]
    ${card_number}     catenate     ${suite_pattern}     ${card_number_last_8}
    set suite variable    ${suite_card_number}   ${card_number}

    ${dic}         [200] API verify (pre-link) card information
    ...     ${access_token}
    ...     ${suite_card_account_name}
    ...     ${suite_card_number}
    ...     ${suite_citizen_id}
    ...     ${suite_cvv_cvc}
    ...     ${suite_pin}
    ...     12/08
    ...     11/08
    ...     ${suite_mobile_number}
    ...     ${suite_billing_address}
    ...     ATM
    ...     ${currency}
    ${security_ref}     get from dictionary      ${dic}     security_ref
    set suite variable    ${suite_security_ref}     ${security_ref}

[Suite][200] prelink sof card for prepared customer in "${currency}" currency
    [Suite][200] prelink card     ${suite_customer_access_token}   ${currency}

[Suite][200] link sof card
    [Arguments]     ${access_token}     ${currency}
    ${dic}      [200] API user link card source of fund
    ...     ${access_token}
    ...     ${suite_card_account_name}
    ...     ${suite_card_number}
    ...     ${suite_citizen_id}
    ...     ${suite_cvv_cvc}
    ...     ${suite_pin}
    ...     12/08
    ...     11/08
    ...     ${suite_mobile_number}
    ...     ${suite_billing_address}
    ...     ${currency}
    ...     123456
    ...     ${suite_security_ref}
    set suite variable    ${suite_card_token}     ${dic.token}

[Suite][200] link sof card for prepared customer in "${currency}" currency
    [Suite][200] link sof card     ${suite_customer_access_token}   ${currency}

[Prepare] - Create an artifact order with suspend sof
    [System_User][Test][200] - login as setup system user
    set test variable   ${test_order_service_name}      ${test_service_name}

    ${unique_reference}     generate random string  12    [LETTERS]
    [Test][200] Prepare an agent with only sof_card

    ${arg_dic}     create dictionary
    ...         access_token=${test_admin_access_token}
    ...         agent_type_id=1
    ...         unique_reference=${unique_reference}
    ...         identity_type_id=1
    ...         username=${unique_reference}
    ...         password=${setup_agent_password_encrypted}

    ${dic}      [200] API create agent      ${arg_dic}
    ${agent_id}     get from dictionary   ${dic}   id
    [Test][200] API system user create user sof cash
    ...     ${agent_id}
    ...     2
    ...     VND
    [200] API suspend sof cash      ${test_sof_id}
    [Test][200] As agent, Create Order
    ...     ${agent_id}
    ...     ${test_agent_access_token}
    ...     ${test_agent_card_token}

[Prepare] - Create an artifact order with requestor
    [System_User][Test][200] - login as setup system user
    set test variable   ${test_order_service_name}      ${test_service_name}

    ${unique_reference}     generate random string  12    [LETTERS]
    [Test][200] Prepare an agent with only sof_card

    ${arg_dic}     create dictionary
    ...         access_token=${test_admin_access_token}
    ...         agent_type_id=1
    ...         unique_reference=${unique_reference}
    ...         identity_type_id=1
    ...         username=${unique_reference}
    ...         password=${setup_agent_password_encrypted}

    ${dic}      [200] API create agent      ${arg_dic}
    ${agent_id}     get from dictionary   ${dic}   id
    [Test][200] API system user create user sof cash
    ...     ${agent_id}
    ...     2
    ...     VND
    [Test][200] As agent, Create Order with requestor
    ...     ${agent_id}
    ...     ${test_agent_access_token}
    ...     ${test_agent_card_token}

[Prepare] - Create an artifact order with suspend sof of specific ID
    [System_User][Test][200] - login as setup system user
    set test variable   ${test_order_service_name}      ${test_service_name}

    ${unique_reference}     generate random string  12    [LETTERS]
    [Test][200] Prepare an agent with only sof_card

    ${arg_dic}     create dictionary
    ...         access_token=${test_admin_access_token}
    ...         agent_type_id=1
    ...         unique_reference=${unique_reference}
    ...         identity_type_id=1
    ...         username=${unique_reference}
    ...         password=${setup_agent_password_encrypted}

    ${dic}      [200] API create agent      ${arg_dic}
    ${agent_id}     get from dictionary   ${dic}   id
    [Test][200] API system user create user sof cash
    ...     ${agent_id}
    ...     2
    ...     VND
    set test variable    ${test_sof_id}      ${test_specific_sof_id}
    [200] API suspend sof cash      ${test_sof_id}
    [Test][200] As agent, Create Order
    ...     ${agent_id}
    ...     ${test_agent_access_token}
    ...     ${test_agent_card_token}

[Prepare] - Create an artifact order with suspend sof of beneficiary
    [System_User][Test][200] - login as setup system user
    set test variable   ${test_order_service_name}      ${test_service_name}

    ${unique_reference}     generate random string  12    [LETTERS]
    [Test][200] Prepare an agent with only sof_card

    ${arg_dic}     create dictionary
    ...         access_token=${test_admin_access_token}
    ...         agent_type_id=1
    ...         unique_reference=${unique_reference}
    ...         identity_type_id=1
    ...         username=${unique_reference}
    ...         password=${setup_agent_password_encrypted}

    set test variable    ${test_sof_id}      ${test_main_beneficiary_sof_id}
    [200] API suspend sof cash      ${test_sof_id}
    [Test][200] As agent, Create Order
    ...     ${test_sub_beneficiary_id}
    ...     ${test_agent_access_token}
    ...     ${test_agent_card_token}


[Test][Fail]Call Execute Order With Security
    ${dic}  [Fail] API call execute order with security     ${test_agent_access_token}      ${test_order_id}      123456    1      EXTERNAL
    set test variable    ${test_result_dic}   ${dic}

[Test] Prepare payer and payee sof cash
    [Test][200] Prepare service structure from service name
    [Test][200] Create a sof_cash and fund in
    set test variable   ${test_payer_id}    ${test_agent_id}
    set test variable   ${test_payer_access_token}    ${test_agent_access_token}
    set test variable   ${test_payer_sof_id}    ${test_agent_sof_id_VN}
    set test variable   ${sale_type_1_id}        ${test_agent_type_id}
    [Test][200] Create a sof_cash and fund in
    set test variable   ${test_payee_id}    ${test_agent_id}
    set test variable   ${test_payee_access_token}    ${test_agent_access_token}
    set test variable   ${test_payee_sof_id}    ${test_agent_sof_id_VN}
    set test variable   ${sale_type_2_id}        ${test_agent_type_id}

[Test] Prepare payer and payee sof cash for sale
    [Test][200] Prepare service structure from service name
    [Test][200] Create a sof_cash and fund in for sale
    set test variable   ${test_payer_id}    ${test_agent_id}
    set test variable   ${test_payer_access_token}    ${test_agent_access_token}
    set test variable   ${test_payer_sof_id}    ${test_agent_sof_id_VN}
    set test variable   ${sale_type_1_id}        ${test_agent_type_id}
    set test variable   ${test_payer_username}    ${test_agent_username}

    [Test][200] Create a sof_cash and fund in for sale
    set test variable   ${test_payee_id}    ${test_agent_id}
    set test variable   ${test_payee_access_token}    ${test_agent_access_token}
    set test variable   ${test_payee_sof_id}    ${test_agent_sof_id_VN}
    set test variable   ${sale_type_2_id}        ${test_agent_type_id}
    set test variable   ${test_payee_username}    ${test_agent_username}

[Test] Prepare initiator sof cash
    [Test][200] Prepare service structure from service name
    [Test][200] Create a sof_cash and fund in
    set test variable   ${test_initiator_id}    ${test_agent_id}
    set test variable   ${test_initiator_access_token}    ${test_agent_access_token}
    set test variable   ${test_initiator_sof_id}    ${test_agent_sof_id_VN}

[Test] Prepare payer and payee and initiator sof cash
    [None] API system user create a company agent cash source of fund with currency    VND     ${suite_admin_access_token}
    [Test][200] Prepare service structure from service name
    [Test][200] Create a sof_cash and fund in
    set test variable   ${test_payee_id}    ${test_agent_id}
    set test variable   ${test_payee_access_token}    ${test_agent_access_token}
    set test variable   ${test_payee_sof_id}    ${test_agent_sof_id_VN}
    set test variable   ${test_payee_type_id}   ${test_agent_type_id}
    set test variable   ${test_payee_type_name}     ${test_agent_type_name}
    [Test][200] Create a sof_cash and fund in
    set test variable   ${test_initiator_id}    ${test_agent_id}
    set test variable   ${test_initiator_access_token}    ${test_agent_access_token}
    set test variable   ${test_initiator_sof_id}    ${test_agent_sof_id_VN}
    set test variable   ${test_initiator_type_id}    ${test_agent_type_id}
    set test variable   ${test_initiator_type_name}     ${test_agent_type_name}
    [Test][200] Create a sof_cash and fund in
    set test variable   ${test_payer_id}    ${test_agent_id}
    set test variable   ${test_payer_access_token}    ${test_agent_access_token}
    set test variable   ${test_payer_sof_id}    ${test_agent_sof_id_VN}
    set test variable   ${test_payer_type_id}   ${test_agent_type_id}
    set test variable   ${test_payer_type_name}     ${test_agent_type_name}

[Suite] Prepare payer and payee and initiator sof cash
    [None] API system user create a company agent cash source of fund with currency    VND     ${suite_admin_access_token}
    [Suite][200] Prepare service structure from service name
    [Suite][200] Create a sof_cash and fund in
    set suite variable   ${suite_payer_id}              ${suite_agent_id}
    set suite variable   ${suite_payer_access_token}    ${suite_agent_access_token}
    set suite variable   ${suite_payer_sof_id}          ${suite_agent_sof_id_VN}
    set suite variable   ${suite_payer_type_id}         ${suite_agent_type_id}
    set suite variable   ${suite_payer_type_name}       ${suite_agent_type_name}
    [Suite][200] Create a sof_cash and fund in
    set suite variable   ${suite_payee_id}              ${suite_agent_id}
    set suite variable   ${suite_payee_access_token}    ${suite_agent_access_token}
    set suite variable   ${suite_payee_sof_id}          ${suite_agent_sof_id_VN}
    set suite variable   ${suite_payee_type_id}         ${suite_agent_type_id}
    set suite variable   ${suite_payee_type_name}       ${suite_agent_type_name}
    [Suite][200] Create a sof_cash and fund in
    set suite variable   ${suite_initiator_id}    ${suite_agent_id}
    set suite variable   ${suite_initiator_access_token}    ${suite_agent_access_token}
    set suite variable   ${suite_initiator_sof_id}    ${suite_agent_sof_id_VN}
    set suite variable   ${suite_initiator_type_id}    ${suite_agent_type_id}
    set suite variable   ${suite_initiator_type_name}     ${suite_agent_type_name}

[Test] Prepare specific ID sof cash
    [Test][200] Prepare service structure from service name
    [Test][200] Create a sof_cash and fund in
    set test variable   ${test_specific_id}    ${test_agent_id}
    set test variable   ${test_specific_access_token}    ${test_agent_access_token}
    set test variable   ${test_specific_sof_id}    ${test_agent_sof_id_VN}

[Test] Prepare beneficiary and payee sof cash
    [Test][200] Prepare service structure from service name
    [Test][200] Create a sof_cash and fund in
    set test variable   ${test_main_beneficiary_id}    ${test_agent_id}
    set test variable   ${test_main_beneficiary_access_token}    ${test_agent_access_token}
    set test variable   ${test_main_beneficiary_sof_id}    ${test_agent_sof_id_VN}
    [Test][200] Create a sof_cash and fund in
    set test variable   ${test_sub_beneficiary_id}    ${test_agent_id}
    set test variable   ${test_sub_beneficiary_access_token}    ${test_agent_access_token}
    set test variable   ${test_sub_beneficiary_sof_id}    ${test_agent_sof_id_VN}
    ${dic}      create dictionary
    ...     relationship_type_id=2
    ...     main_user_id=${test_main_beneficiary_id}
    ...     sub_user_id=${test_sub_beneficiary_id}
    ${dic_relationship}     [200] API create relationship   ${dic}
    ${response}    get from dictionary     ${dic_relationship}    response
    set test variable    ${test_relationship_id}    ${response.json()['data']['id']}
    ${dic}      create dictionary
    ...     relationship_id=${test_relationship_id}
    ...     access_token=${suite_admin_access_token}
    [200] API update relationship   ${dic}
    set test variable  ${test_agent_sof_id_VN}  ${test_main_beneficiary_sof_id}

[Test] Prepare sof service balance limitation for "${service_id}" and "${sof_id}"
    ${maximum_amount_pre}     generate random string  3    123456789
    ${maximum_amount_suf}     generate random string  2    [NUMBERS]
    set test variable         ${test_maximum_amount}       ${maximum_amount_pre}.${maximum_amount_suf}
    
    ${minimum_amount_pre}     generate random string  3    123456789
    ${minimum_amount_suf}     generate random string  2    [NUMBERS]
    set test variable         ${test_minimum_amount}       ${minimum_amount_pre}.${minimum_amount_suf}

    ${create_limitation_dic}     create dictionary      access_token=${suite_admin_access_token}    maximum_amount=${test_maximum_amount}    minimum_amount=${test_minimum_amount}    service_id=${service_id}       sof_id=${sof_id}
    ${dic}      [200] API create sof service balance limitation     ${create_limitation_dic}
    ${response}=    get from dictionary    ${dic}   response
    ${balance_limitation_id}=  get from dictionary   ${dic}   balance_limitation_id
    set test variable       ${test_balance_limitation_id}     ${balance_limitation_id}

[Payment][Test][200] - Create service
    [Arguments]    &{arg_dic}
    ${service_name}      generate random string      20   prepare_service_name_[LETTERS]
    ${description}      generate random string      20   prepare_service_description_[LETTERS]
    ${service_name}  set variable    prepare_${service_name}
    ${description}  set variable    prepare_${description}
    ${dic}     [200] API create service
    ...     ${arg_dic.access_token}
    ...     ${arg_dic.service_group_id}
    ...     ${service_name}
    ...     ${arg_dic.currency}
    ...     ${description}
    ${response}     get from dictionary   ${dic}   response
    ${service_id}     get from dictionary   ${dic}   service_id
    set test variable      ${test_service_id}      ${service_id}
    set test variable      ${test_service_name}      ${service_name}

[Payment][Suite][200] - Create service
    [Arguments]    &{arg_dic}
    ${service_name}      generate random string      20   prepare_service_name_[LETTERS]
    ${description}      generate random string      20   prepare_service_description_[LETTERS]
    ${service_name}  set variable    prepare_${service_name}
    ${description}  set variable    prepare_${description}
    ${dic}     [200] API create service
    ...     ${arg_dic.access_token}
    ...     ${arg_dic.service_group_id}
    ...     ${service_name}
    ...     ${arg_dic.currency}
    ...     ${description}
    ${response}     get from dictionary   ${dic}   response
    ${service_id}     get from dictionary   ${dic}   service_id
    set suite variable      ${suite_service_id}      ${service_id}
    set suite variable      ${suite_service_name}      ${service_name}

[Test] Prepare service limit under "${service_group_id}" with currency "${currency}" for SOF "${sof_id}"
    [Payment][Test][200] - Create service   access_token=${suite_admin_access_token}    service_group_id=${service_group_id}   currency=${currency}
    [Test] Prepare sof service balance limitation for "${test_service_id}" and "${sof_id}"

[Test] Save first service limit
    set test variable       ${test_service_id_first}        ${test_service_id}
    set test variable       ${test_service_name_first}        ${test_service_name}
    set test variable       ${test_limit_id_first}        ${test_balance_limitation_id}
    set test variable       ${test_maximum_amount_first}        ${test_maximum_amount}

[Test] Save second service limit
    set test variable       ${test_service_id_second}        ${test_service_id}
    set test variable       ${test_service_name_second}        ${test_service_name}
    set test variable       ${test_limit_id_second}        ${test_balance_limitation_id}
    set test variable       ${test_maximum_amount_second}        ${test_maximum_amount}

[Suite] Get tier config allow zero
    ${get_tier_config_dic}     create dictionary
    ${dic}      [200] API get tier config allow zero     ${get_tier_config_dic}
    ${response}     get from dictionary   ${dic}   response
    ${value}     get from dictionary   ${dic}   value
    set suite variable      ${suite_tier_config_allow_zero}      ${value}

[Suite][200] Create normal order for wallet agent then execute
    ${product_name}         generate random string      20   prepare_product_name_[LETTERS]
    ${product_ref_1}        generate random string      20   prepare_service_ref_1_[LETTERS]
    ${product_ref_2}        generate random string      20   prepare_service_ref_2_[LETTERS]
    ${product_ref_3}        generate random string      20   prepare_service_ref_3_[LETTERS]
    ${product_ref_4}        generate random string      20   prepare_service_ref_4_[LETTERS]
    ${product_ref_5}        generate random string      20   prepare_service_ref_5_[LETTERS]

    ${ext_transaction_id}      generate random string      12   [NUMBERS]
    ${product_service_id}      generate random string      12   [NUMBERS]
    ${dic}     [200] API payment create normal order
    ...     ${suite_wallet_agent_access_token}
    ...     ${ext_transaction_id}
    ...     ${suite_service_id}
    ...     ${suite_service_name}
    ...     ${product_name}
    ...     ${product_ref_1}
    ...     ${product_ref_2}
    ...     ${product_ref_3}
    ...     ${product_ref_4}
    ...     ${product_ref_5}
    ...     access token
    ...     ${suite_wallet_agent_access_token}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${suite_normal_agent_id}
    ...     agent
    ...     10000
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    ${order_id}=  get from dictionary   ${dic}   order_id
    set suite variable       ${suite_wallet_agent_normal_order_id}     ${order_id}
    set suite variable       ${suite_wallet_agent_ext_transaction_id}   ${ext_transaction_id}
    set suite variable       ${suite_wallet_agent_product_name}         ${product_name}

    ${execute_order_dic}    create dictionary
    ...     order_id=${suite_wallet_agent_normal_order_id}
    ...     access_token=${suite_wallet_agent_access_token}
    [200] API payment execute order     ${execute_order_dic}

[Suite][200] Create normal order for connected agent then execute
    ${product_name}         generate random string      20   prepare_product_name_[LETTERS]
    ${product_ref_1}        generate random string      20   prepare_service_ref_1_[LETTERS]
    ${product_ref_2}        generate random string      20   prepare_service_ref_2_[LETTERS]
    ${product_ref_3}        generate random string      20   prepare_service_ref_3_[LETTERS]
    ${product_ref_4}        generate random string      20   prepare_service_ref_4_[LETTERS]
    ${product_ref_5}        generate random string      20   prepare_service_ref_5_[LETTERS]

    ${ext_transaction_id}      generate random string      12   [NUMBERS]
    ${product_service_id}      generate random string      12   [NUMBERS]
    ${dic}     [200] API payment create normal order
    ...     ${suite_connected_agent_access_token}
    ...     ${ext_transaction_id}
    ...     ${suite_service_id}
    ...     ${suite_service_name}
    ...     ${product_name}
    ...     ${product_ref_1}
    ...     ${product_ref_2}
    ...     ${product_ref_3}
    ...     ${product_ref_4}
    ...     ${product_ref_5}
    ...     access token
    ...     ${suite_connected_agent_access_token}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${suite_normal_agent_id}
    ...     agent
    ...     10000
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    ${order_id}=  get from dictionary   ${dic}   order_id
    set suite variable       ${suite_connected_agent_normal_order_id}     ${order_id}

    ${execute_order_dic}    create dictionary
    ...     order_id=${suite_connected_agent_normal_order_id}
    ...     access_token=${suite_connected_agent_access_token}
    [200] API payment execute order     ${execute_order_dic}

[Test] Prepare truster and trusted and payee sof cash
    [Test][200] Prepare service structure from service name
    [Test][200] Create a sof_cash and fund in
    set test variable   ${test_trusted_id}    ${test_agent_id}
    set test variable   ${test_trusted_access_token}    ${test_agent_access_token}
    set test variable   ${test_trusted_sof_id}    ${test_agent_sof_id_VN}
    [Test][200] Create a sof_cash and fund in
    set test variable   ${test_truster_id}    ${test_agent_id}
    set test variable   ${test_truster_access_token}    ${test_agent_access_token}
    set test variable   ${test_truster_sof_id}    ${test_agent_sof_id_VN}
    [Test][200] Create a sof_cash and fund in
    set test variable   ${test_payee_id}    ${test_agent_id}
    set test variable   ${test_payee_access_token}    ${test_agent_access_token}
    set test variable   ${test_payee_sof_id}    ${test_agent_sof_id_VN}

Prepare data for remove SOF service limitation balance for limit_id "${limit_id}"
    ${limit_id}=    set variable        {"id":${limit_id}}
    @{list_sof_service_balance_limitation}      create list     	${limit_id}
    ${delete_limit_dic}     create dictionary       list_sof_service_balance_limitation=${list_sof_service_balance_limitation}
    ${dic}      [200] API delete sof service balance limitation     ${delete_limit_dic}

[Payment][Test][200] - create normal order with requestor
    ${payer_user_ref_type}=    set variable    access token
    ${payer_user_ref_value}=    set variable   ${test_payer_access_token}
    ${payer_user_id}=    set variable    ${test_payer_id}
    ${payer_user_type}=    set variable    agent
    ${payee_user_ref_type}=    set variable    ${param_not_used}
    ${payee_user_ref_value}=    set variable    ${param_not_used}
    ${payee_user_id}=    set variable    ${test_payee_id}
    ${payee_user_type}=    set variable    agent
    ${requestor_user_id}=    set variable  ${test_requestor_id}
    ${requestor_user_type}=    set variable    agent
    ${requestor_user_sof_id}=    set variable    ${test_requestor_sof_id}
    ${requestor_user_sof_type_id}=    set variable    2
    ${amount}=    set variable    10000
    ${product_name}         generate random string      20   prepare_product_name_[LETTERS]
    ${product_ref_1}        generate random string      20   prepare_service_ref_1_[LETTERS]
    ${product_ref_2}        generate random string      20   prepare_service_ref_2_[LETTERS]
    ${product_ref_3}        generate random string      20   prepare_service_ref_3_[LETTERS]
    ${product_ref_4}        generate random string      20   prepare_service_ref_4_[LETTERS]
    ${product_ref_5}        generate random string      20   prepare_service_ref_5_[LETTERS]
    ${ext_transaction_id}      generate random string      12   [NUMBERS]
    ${product_service_id}      generate random string      12   [NUMBERS]

    ${arg_dic}  create dictionary
    ...     access_token=${test_payer_access_token}
    ...     test_service_id=${test_service_id}
    ...     test_service_name=${test_service_name}
    ...     product_name=${product_name}
    ...     product_ref_1=${product_ref_1}
    ...     product_ref_2=${product_ref_2}
    ...     product_ref_3=${product_ref_3}
    ...     product_ref_4=${product_ref_4}
    ...     product_ref_5=${product_ref_5}
    ...     ext_transaction_id=${ext_transaction_id}
    ...     product_service_id=${product_service_id}
    ...     payee_user_id=${payee_user_id}
    ...     payee_user_type=${payee_user_type}
    ...     payee_user_ref_type=${payee_user_ref_type}
    ...     payee_user_ref_value=${payee_user_ref_value}
    ...     payer_user_ref_type=${payer_user_ref_type}
    ...     payer_user_ref_value=${payer_user_ref_value}
    ...     payer_user_id=${payer_user_id}
    ...     payer_user_type=${payer_user_type}
    ...     requestor_user_user_id=${requestor_user_id}
    ...     requestor_user_user_type=agent
    ...     requestor_sof_id=${requestor_user_sof_id}
    ...     requestor_sof_type_id=${requestor_user_sof_type_id}
    ...     amount=${amount}
    ...     setup_admin_client_id=${setup_admin_client_id}
    ...     setup_admin_client_secret=${setup_admin_client_secret}
    ...     product_service_id=${test_service_id}
    ...     product_service_name=${test_service_name}
    ${dic}  [200] API create normal order   ${arg_dic}
    ${order_id}   Get from dictionary   ${dic}  order_id
    set test variable   ${test_order_id}    ${order_id}

[Test] Prepare requestor sof cash
    [Test][200] Create a sof_cash and fund in
    set test variable   ${test_requestor_id}    ${test_agent_id}
    set test variable   ${test_requestor_access_token}    ${test_agent_access_token}
    set test variable   ${test_requestor_sof_id}    ${test_agent_sof_id_VN}

[Payment][Test][200] - Create a tier mask for service with payee is customer
    ${tier_mask_name}=    generate random string      10   [LETTERS]
    ${requestor_user_type} =    set variable  agent
    ${requestor_type_id}=    set variable    2
    ${requestor_classification_id}=    generate random string  4    123456789
    ${requestor_postal_code}=    generate random string      10   [LETTERS]
    ${initiator_user_type}=    set variable    agent
    ${initiator_type_id}=    set variable    2
    ${initiator_classification_id}=    generate random string  4    123456789
    ${initiator_postal_code}=    generate random string      10   [LETTERS]
    ${payer_user_type}=    set variable    agent
    ${payer_type_id}=    set variable       2
    ${payer_classification_id}=    generate random string  4    123456789
    ${payer_postal_code}=    generate random string      10   [LETTERS]
    ${payee_user_type}=    set variable    customer
    ${payee_type_id}=    set variable    1
    ${payee_classification_id}=    generate random string  4    123456789
    ${payee_postal_code}=    generate random string      10   [LETTERS]
    [Common] - get current start date
    set test variable  ${from_effective_timestamp}      ${test_current_from_date}
    [Common] - get current end date
    set test variable  ${to_effective_timestamp}      ${test_current_end_date}

    ${arg_dic}  create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     service_id=${suite_service_id}
    ...     tier_mask_name=${tier_mask_name}
    ...     requestor_user_type=${requestor_user_type}
    ...     requestor_type_id=${requestor_type_id}
    ...     requestor_classification_id=${requestor_classification_id}
    ...     requestor_postal_code=${requestor_postal_code}
    ...     initiator_user_type=${initiator_user_type}
    ...     initiator_type_id=${initiator_type_id}
    ...     initiator_classification_id=${initiator_classification_id}
    ...     initiator_postal_code=${initiator_postal_code}
    ...     payer_user_type=${payer_user_type}
    ...     payer_type_id=${payer_type_id}
    ...     payer_classification_id=${payer_classification_id}
    ...     payer_postal_code=${payer_postal_code}
    ...     payee_user_type=${payee_user_type}
    ...     payee_type_id=${payee_type_id}
    ...     payee_classification_id=${payee_classification_id}
    ...     payee_postal_code=${payee_postal_code}
    ...     from_effective_timestamp=${from_effective_timestamp}
    ...     to_effective_timestamp=${to_effective_timestamp}
    ${dic}  [200] API create tier mask for service   ${arg_dic}
    set test variable   ${test_service_tier_mask_id}    ${dic.service_tier_mask_id}
    set test variable   ${test_service_tier_mask_name}    ${tier_mask_name}

[Payment][Test][200] - Create multiple tier masks for service
    ${tier_mask_name}     generate random string      10   [LETTERS]
    [Common] - get current start date
    set test variable  ${from_effective_timestamp}      ${test_current_from_date}
    [Common] - get current end date
    set test variable  ${to_effective_timestamp}      ${test_current_end_date}

    ${arg_dic}  create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     service_id=${suite_service_id}
    ...     tier_mask_name=${tier_mask_name}
    ...     from_effective_timestamp=${from_effective_timestamp}
    ...     to_effective_timestamp=${to_effective_timestamp}
    ${dic}  [200] API create tier mask for service   ${arg_dic}
    set test variable   ${test_service_tier_mask_1_id}    ${dic.service_tier_mask_id}
    set test variable   ${test_service_tier_mask_1_name}    ${tier_mask_name}

    set test variable   ${test_service_tier_mask_1_priority}      1
    set test variable   ${test_service_tier_mask_1_display_name}      1. ${test_service_tier_mask_1_name}

    ${tier_mask_name}     generate random string      10   [LETTERS]

    ${requestor_user_type} =    set variable  agent
    ${requestor_type_id}=    set variable    ${suite_water_agent_type_id}
    ${requestor_type_name}=    set variable    ${suite_water_agent_type_name}
    ${requestor_classification_id}=    set variable     ${suite_agent_classification_id}
    ${requestor_classification_name}=    set variable     ${suite_agent_classification_name}
    ${requestor_postal_code}=    set variable   ${suite_water_agent_current_address_postal_code}

    ${initiator_user_type}=    set variable    agent
    ${initiator_type_id}=    set variable    ${suite_water_agent_type_id}
    ${initiator_type_name}=    set variable    ${suite_water_agent_type_name}
    ${initiator_classification_id}=    set variable    ${suite_agent_classification_id}
    ${initiator_classification_name}=    set variable     ${suite_agent_classification_name}
    ${initiator_postal_code}=    set variable   ${suite_water_agent_current_address_postal_code}

    ${payer_user_type}=    set variable    agent
    ${payer_type_id}=    set variable       ${suite_water_agent_type_id}
    ${payer_type_name}=    set variable    ${suite_water_agent_type_name}
    ${payer_classification_id}=    set variable     ${suite_agent_classification_id}
    ${payer_classification_name}=    set variable     ${suite_agent_classification_name}
    ${payer_postal_code}=    set variable   ${suite_water_agent_current_address_postal_code}

    ${payee_user_type}=    set variable    customer
    ${payee_user_type_name}=    set variable    customer
    ${payee_classification_id}=    set variable  ${suite_customer_classification_id}
    ${payee_classification_name}=    set variable  ${suite_customer_classification_name}
    ${payee_postal_code}=    set variable   ${suite_customer_current_address_postal_code}

    [Common] - get current start date
    set test variable  ${from_effective_timestamp}      ${test_current_from_date}
    [Common] - get current end date
    set test variable  ${to_effective_timestamp}      ${test_current_end_date}

    ${arg_dic}  create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     service_id=${suite_service_id}
    ...     tier_mask_name=${tier_mask_name}
    ...     requestor_user_type=${requestor_user_type}
    ...     requestor_type_id=${requestor_type_id}
    ...     requestor_classification_id=${requestor_classification_id}
    ...     requestor_postal_code=${requestor_postal_code}
    ...     initiator_user_type=${initiator_user_type}
    ...     initiator_type_id=${initiator_type_id}
    ...     initiator_classification_id=${initiator_classification_id}
    ...     payer_user_type=${payer_user_type}
    ...     payer_type_id=${payer_type_id}
    ...     payee_user_type=${payee_user_type}
    ...     from_effective_timestamp=${from_effective_timestamp}
    ...     to_effective_timestamp=${to_effective_timestamp}
    ${dic}  [200] API create tier mask for service   ${arg_dic}

    set test variable   ${test_service_tier_mask_2_id}    ${dic.service_tier_mask_id}
    set test variable   ${test_service_tier_mask_2_name}    ${tier_mask_name}

    set test variable   ${test_service_tier_mask_2_priority}      2
    set test variable   ${test_service_tier_mask_2_display_name}      2. ${test_service_tier_mask_2_name}

    #    Requestor condition
    set test variable   ${test_service_tier_mask_2_requestor_actor}      Requestor
    set test variable   ${test_service_tier_mask_2_requestor_actor_user_type}      ${requestor_user_type}
    set test variable   ${test_service_tier_mask_2_requestor_actor_type_name}      ${requestor_type_name}
    set test variable   ${test_service_tier_mask_2_requestor_actor_classification_name}      ${requestor_classification_name}
    set test variable   ${test_service_tier_mask_2_requestor_actor_postal_code}      ${requestor_postal_code}

    #    Initiator condition
    set test variable   ${test_service_tier_mask_2_initiator_actor}      Initiator
    set test variable   ${test_service_tier_mask_2_initiator_actor_user_type}      ${initiator_user_type}
    set test variable   ${test_service_tier_mask_2_initiator_actor_type_name}      ${initiator_type_name}
    set test variable   ${test_service_tier_mask_2_initiator_actor_classification_name}      ${initiator_classification_name}
    set test variable   ${test_service_tier_mask_2_initiator_actor_postal_code}     ${empty}

    #    Payer condition
    set test variable   ${test_service_tier_mask_2_payer_actor}      Payer
    set test variable   ${test_service_tier_mask_2_payer_user_type}      ${payer_user_type}
    set test variable   ${test_service_tier_mask_2_payer_type_name}      ${payer_type_name}
    set test variable   ${test_service_tier_mask_2_payer_classification_name}      ${empty}
    set test variable   ${test_service_tier_mask_2_payer_postal_code}     ${empty}

    #    Paye condition
    set test variable   ${test_service_tier_mask_2_payere_actor}      Payee
    set test variable   ${test_service_tier_mask_2_payee_user_type}      ${payee_user_type}
    set test variable   ${test_service_tier_mask_2_payee_type_id}      ${empty}
    set test variable   ${test_service_tier_mask_2_payee_classification_name}      ${empty}
    set test variable   ${test_service_tier_mask_2_payee_postal_code}     ${empty}

[Payment][Test][200] - Create multiple tier masks for service with non condition
    ${tier_mask_name}     generate random string      10   [LETTERS]

    ${initiator_user_type_label}     set variable    Agent
    ${initiator_user_type}     set variable    agent
    ${initiator_type_id}     set variable    2
    ${initiator_classification_id}     generate random string  4    123456789

    ${payer_user_type_label}     set variable    Customer
    ${payer_user_type}     set variable    customer
    ${payer_type_id}     set variable       2
    ${payer_postal_code}     set variable       10000_2

    ${payee_user_type_label}     set variable    Agent
    ${payee_user_type}     set variable    agent

    [Common] - get current start date
    set test variable  ${from_effective_timestamp}      ${test_current_from_date}
    [Common] - get current end date
    set test variable  ${to_effective_timestamp}      ${test_current_end_date}

    ${arg_dic}  create dictionary
    ${arg_dic}  create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     service_id=${suite_service_id}
    ...     tier_mask_name=${tier_mask_name}
    ...     initiator_user_type=${initiator_user_type}
    ...     initiator_type_id=${initiator_type_id}
    ...     initiator_classification_id=${initiator_classification_id}
    ...     payer_user_type=${payer_user_type}
    ...     payer_type_id=${payer_type_id}
    ...     payer_postal_code=${payer_postal_code}
    ...     payee_user_type=${payee_user_type}
    ...     from_effective_timestamp=${from_effective_timestamp}
    ...     to_effective_timestamp=${to_effective_timestamp}
    ${dic}  [200] API create tier mask for service   ${arg_dic}
    set test variable   ${test_service_tier_mask_id}    ${dic.service_tier_mask_id}
    set test variable   ${test_service_tier_mask_name}    ${tier_mask_name}

    set test variable   ${test_service_tier_mask_priority}      1
    set test variable   ${test_service_tier_mask_display_name}      1. ${test_service_tier_mask_name}


[Payment][Test][200] - Create a tier mask for service
    ${tier_mask_name}=    generate random string      10   [LETTERS]

    ${requestor_user_type} =    set variable  agent
    ${requestor_type_id}=    set variable    ${suite_water_agent_type_id}
    ${requestor_type_name}=    set variable    ${suite_water_agent_type_name}
    ${requestor_classification_id}=    set variable     ${suite_agent_classification_id}
    ${requestor_classification_name}=    set variable     ${suite_agent_classification_name}
    ${requestor_postal_code}=    set variable   ${suite_water_agent_current_address_postal_code}

    ${initiator_user_type}=    set variable    agent
    ${initiator_type_id}=    set variable    ${suite_water_agent_type_id}
    ${initiator_type_name}=    set variable    ${suite_water_agent_type_name}
    ${initiator_classification_id}=    set variable    ${suite_agent_classification_id}
    ${initiator_classification_name}=    set variable     ${suite_agent_classification_name}
    ${initiator_postal_code}=    set variable   ${suite_water_agent_current_address_postal_code}

    ${payer_user_type}=    set variable    agent
    ${payer_type_id}=    set variable       ${suite_water_agent_type_id}
    ${payer_type_name}=    set variable    ${suite_water_agent_type_name}
    ${payer_classification_id}=    set variable     ${suite_agent_classification_id}
    ${payer_classification_name}=    set variable     ${suite_agent_classification_name}
    ${payer_postal_code}=    set variable   ${suite_water_agent_current_address_postal_code}

    ${payee_user_type}=    set variable    customer
    ${payee_user_type_name}=    set variable    customer
    ${payee_classification_id}=    set variable  ${suite_customer_classification_id}
    ${payee_classification_name}=    set variable  ${suite_customer_classification_name}
    ${payee_postal_code}=    set variable   ${suite_customer_current_address_postal_code}

    [Common] - get current start date
    set test variable  ${from_effective_timestamp}      ${test_current_from_date}
    [Common] - get current end date
    set test variable  ${to_effective_timestamp}      ${test_current_end_date}
    ${arg_dic}  create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     service_id=${suite_service_id}
    ...     tier_mask_name=${tier_mask_name}
    ...     requestor_user_type=${requestor_user_type}
    ...     requestor_type_id=${requestor_type_id}
    ...     requestor_classification_id=${requestor_classification_id}
    ...     requestor_postal_code=${requestor_postal_code}
    ...     initiator_user_type=${initiator_user_type}
    ...     initiator_type_id=${initiator_type_id}
    ...     initiator_classification_id=${initiator_classification_id}
    ...     initiator_postal_code=${initiator_postal_code}
    ...     payer_user_type=${payer_user_type}
    ...     payer_type_id=${payer_type_id}
    ...     payer_classification_id=${payer_classification_id}
    ...     payer_postal_code=${payer_postal_code}
    ...     payee_user_type=${payee_user_type}
    ...     payee_classification_id=${payee_classification_id}
    ...     payee_postal_code=${payee_postal_code}
    ...     from_effective_timestamp=${from_effective_timestamp}
    ...     to_effective_timestamp=${to_effective_timestamp}
    ${dic}  [200] API create tier mask for service   ${arg_dic}
    set test variable   ${test_service_tier_mask_id}    ${dic.service_tier_mask_id}
    set test variable   ${test_service_tier_mask_name}    ${tier_mask_name}

    set test variable   ${test_requestor_type_name}    ${requestor_type_name}
    set test variable   ${test_requestor_classification_name}    ${requestor_classification_name}

    set test variable   ${test_initiator_type_name}    ${initiator_type_name}
    set test variable   ${test_initiator_classification_name}    ${initiator_classification_name}

    set test variable   ${test_payer_type_name}    ${requestor_type_name}
    set test variable   ${test_payer_classification_name}    ${payer_classification_name}

    set test variable   ${test_payee_classification_name}    ${payee_classification_name}

[Payment][Test][200] - Create 2 tier masks for service
    ${tier_mask_name}=    generate random string      10   [LETTERS]

    ${requestor_user_type} =    set variable  agent
    ${requestor_type_id}=    set variable    ${suite_water_agent_type_id}
    ${requestor_type_name}=    set variable    ${suite_water_agent_type_name}
    ${requestor_classification_id}=    set variable     ${suite_agent_classification_id}
    ${requestor_classification_name}=    set variable     ${suite_agent_classification_name}
    ${requestor_postal_code}=    set variable   ${suite_water_agent_current_address_postal_code}

    ${initiator_user_type}=    set variable    agent
    ${initiator_type_id}=    set variable    ${suite_water_agent_type_id}
    ${initiator_type_name}=    set variable    ${suite_water_agent_type_name}
    ${initiator_classification_id}=    set variable    ${suite_agent_classification_id}
    ${initiator_classification_name}=    set variable     ${suite_agent_classification_name}
    ${initiator_postal_code}=    set variable   ${suite_water_agent_current_address_postal_code}

    ${payer_user_type}=    set variable    agent
    ${payer_type_id}=    set variable       ${suite_water_agent_type_id}
    ${payer_type_name}=    set variable    ${suite_water_agent_type_name}
    ${payer_classification_id}=    set variable     ${suite_agent_classification_id}
    ${payer_classification_name}=    set variable     ${suite_agent_classification_name}
    ${payer_postal_code}=    set variable   ${suite_water_agent_current_address_postal_code}

    ${payee_user_type}=    set variable    customer
    ${payee_user_type_name}=    set variable    customer
    ${payee_classification_id}=    set variable  ${suite_customer_classification_id}
    ${payee_classification_name}=    set variable  ${suite_customer_classification_name}
    ${payee_postal_code}=    set variable   ${suite_customer_current_address_postal_code}

    [Common] - get current start date
    set test variable  ${from_effective_timestamp}      ${test_current_from_date}
    [Common] - get current end date
    set test variable  ${to_effective_timestamp}      ${test_current_end_date}
    ${arg_dic}  create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     service_id=${suite_service_id}
    ...     tier_mask_name=${tier_mask_name}
    ...     requestor_user_type=${requestor_user_type}
    ...     requestor_type_id=${requestor_type_id}
    ...     requestor_classification_id=${requestor_classification_id}
    ...     requestor_postal_code=${requestor_postal_code}
    ...     initiator_user_type=${initiator_user_type}
    ...     initiator_type_id=${initiator_type_id}
    ...     initiator_classification_id=${initiator_classification_id}
    ...     initiator_postal_code=${initiator_postal_code}
    ...     payer_user_type=${payer_user_type}
    ...     payer_type_id=${payer_type_id}
    ...     payer_classification_id=${payer_classification_id}
    ...     payer_postal_code=${payer_postal_code}
    ...     payee_user_type=${payee_user_type}
    ...     payee_classification_id=${payee_classification_id}
    ...     payee_postal_code=${payee_postal_code}
    ...     from_effective_timestamp=${from_effective_timestamp}
    ...     to_effective_timestamp=${to_effective_timestamp}
    ${dic}  [200] API create tier mask for service   ${arg_dic}
    set test variable   ${test_service_tier_mask_id}    ${dic.service_tier_mask_id}
    set test variable   ${test_service_tier_mask_name}    ${tier_mask_name}

    set test variable   ${test_requestor_type_name}    ${requestor_type_name}
    set test variable   ${test_requestor_classification_name}    ${requestor_classification_name}

    set test variable   ${test_initiator_type_name}    ${initiator_type_name}
    set test variable   ${test_initiator_classification_name}    ${initiator_classification_name}

    set test variable   ${test_payer_type_name}    ${requestor_type_name}
    set test variable   ${test_payer_classification_name}    ${payer_classification_name}

    set test variable   ${test_payee_classification_name}    ${payee_classification_name}

    ${tier_mask_name_2}=    generate random string      10   [LETTERS]
    ${arg_dic}  create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     service_id=${suite_service_id}
    ...     tier_mask_name=${tier_mask_name_2}
    ...     requestor_user_type=${requestor_user_type}
    ...     requestor_type_id=${requestor_type_id}
    ...     requestor_classification_id=${requestor_classification_id}
    ...     requestor_postal_code=${requestor_postal_code}
    ...     initiator_user_type=${initiator_user_type}
    ...     initiator_type_id=${initiator_type_id}
    ...     initiator_classification_id=${initiator_classification_id}
    ...     initiator_postal_code=${initiator_postal_code}
    ...     payer_user_type=${payer_user_type}
    ...     payer_type_id=${payer_type_id}
    ...     payer_classification_id=${payer_classification_id}
    ...     payer_postal_code=${payer_postal_code}
    ...     payee_user_type=${payee_user_type}
    ...     payee_classification_id=${payee_classification_id}
    ...     payee_postal_code=${payee_postal_code}
    ...     from_effective_timestamp=${from_effective_timestamp}
    ...     to_effective_timestamp=${to_effective_timestamp}
    ${dic}  [200] API create tier mask for service   ${arg_dic}
    set test variable   ${test_service_tier_mask_2_id}    ${dic.service_tier_mask_id}
    set test variable   ${test_service_tier_mask_2_name}    ${tier_mask_name_2}

[Payment][Test][200] - Create multiple tier masks for specific service
    [Arguments]    ${service_id}

    ${tier_mask_name}     generate random string      10   [LETTERS]

    ${initiator_user_type_label}     set variable    Agent
    ${initiator_user_type}     set variable    agent
    ${initiator_type_id}     set variable    2
    ${initiator_classification_id}     generate random string  4    123456789

    ${payer_user_type_label}     set variable    Customer
    ${payer_user_type}     set variable    customer
    ${payer_type_id}     set variable       2
    ${payer_postal_code}     set variable       10000_2

    ${payee_user_type_label}     set variable    Agent
    ${payee_user_type}     set variable    agent

    [Common] - get current start date
    set test variable  ${from_effective_timestamp}      ${test_current_from_date}
    [Common] - get current end date
    set test variable  ${to_effective_timestamp}      ${test_current_end_date}

    ${arg_dic}  create dictionary
    ${arg_dic}  create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     service_id=${service_id}
    ...     tier_mask_name=${tier_mask_name}
    ...     initiator_user_type=${initiator_user_type}
    ...     initiator_type_id=${initiator_type_id}
    ...     initiator_classification_id=${initiator_classification_id}
    ...     payer_user_type=${payer_user_type}
    ...     payer_type_id=${payer_type_id}
    ...     payer_postal_code=${payer_postal_code}
    ...     payee_user_type=${payee_user_type}
    ...     from_effective_timestamp=${from_effective_timestamp}
    ...     to_effective_timestamp=${to_effective_timestamp}
    ${dic}  [200] API create tier mask for service   ${arg_dic}
    set test variable   ${test_service_tier_mask_id}    ${dic.service_tier_mask_id}
    set test variable   ${test_service_tier_mask_name}    ${tier_mask_name}

    set test variable   ${test_service_tier_mask_priority}      1
    set test variable   ${test_service_tier_mask_display_name}      1. ${test_service_tier_mask_name}

[Test][Prepare] - Prepare service transaction limitation for "${limitation_type}" with "${service_id}"
    ${1}  set variable    {"actor":"Payer","actor_user_type":"agent","actor_type_id":"${suite_agent_type_id}","actor_classification_id":"${suite_agent_classification_id}"}
    ${2}  set variable    {"actor":"Requestor","actor_user_type":"agent","actor_type_id":"${suite_agent_type_id}","actor_classification_id":"${suite_agent_classification_id}"}
    ${3}  set variable    {"actor":"Initiator","actor_user_type":"agent","actor_type_id":"${suite_agent_type_id}","actor_classification_id":"${suite_agent_classification_id}"}
    ${4}  set variable    {"actor":"Payee","actor_user_type":"customer","actor_classification_id":"${suite_customer_classification_id}"}
    @{list_condition}  create list     ${1}    ${2}     ${3}    ${4}

    @{list_block_unit}  create list     HOUR    DAY
    ${unit_value}=      Evaluate  random.choice($list_block_unit)  random

    ${limitation_value}=     Run Keyword If   '${limitation_type}'=='TRANSACTION'    generate random string  7    123456789
                             ...     ELSE              generate random string  1    123456789
    ${reset_time_block_value}     generate random string  1    123456789
    ${current_date}               Get Current Date	    UTC       result_format=%Y-%m-%dT%H:%M:%SZ
    ${tomorrow_date}=             add time to date    ${current_date}       1 days     result_format=%Y-%m-%dT00:00:00Z
    ${next_2_dates}=              add time to date    ${current_date}       2 days     result_format=%Y-%m-%dT00:00:00Z
    set test variable   ${test_limitation_value}    ${limitation_value}
    set test variable   ${test_reset_time_block_value}    ${reset_time_block_value}
    set test variable   ${test_start_date}    ${tomorrow_date}
    set test variable   ${test_end_date}    ${next_2_dates}
    set test variable   ${test_reset_time_block_unit}    ${unit_value}

    ${equation_id}=     Run Keyword If   '${limitation_type}'=='TRANSACTION'    set variable    1
                        ...     ELSE              set variable      ${param_not_used}

    ${create_service_limitation_dic}=    create dictionary      service_id=${service_id}   conditions=@{list_condition}     limitation_type=${limitation_type}     equation_id=${equation_id}
                                         ...                    limitation_value=${test_limitation_value}   reset_time_block_value=${test_reset_time_block_value}        reset_time_block_unit=${test_reset_time_block_unit}
                                         ...                    start_effective_timestamp=${test_start_date}   end_effective_timestamp=${test_end_date}
    log          ${create_service_limitation_dic}
    ${dic}    [200] API create service limitation     ${create_service_limitation_dic}
    ${response}=    get from dictionary    ${dic}   response
    ${service_limitation_id}     get from dictionary   ${dic}   service_limitation_id
    set test variable   ${test_service_limitation_id}    ${service_limitation_id}

[Payment][Test][200] - Create service fee tier mask with full label
    [Arguments]     &{arg_dic}
    ${gen_amount}   generate random string      3   123456789

    ${type_first}     set variable    Flat value
    ${from_first}     set variable    null
    ${amount_first}     set variable    ${gen_amount}
    ${type_second}     set variable    % rate
    ${from_second}     set variable    Amount
    ${amount_second}     set variable    5
    ${operator}     set variable    +

    ${label_catenate}   set variable    ${empty}
    ${label_list}   create list  a	b	c	d	e	f	g	h	i	j	k	l	m	n	o
    ${label_list_length}   get length      ${label_list}
    :FOR   ${index}    IN RANGE     0  ${label_list_length}
    \   ${label}    get from list   ${label_list}       ${index}
    \   ${label_config}    [Payment][Reuse][200] - Create label config     label=${label}  type_first=${type_first}     from_first=${from_first}   amount_first=${amount_first}    type_second=${type_second}  from_second=${from_second}  amount_second=${amount_second}      operator=${operator}
    \   ${label_catenate}         set variable     ${label_catenate},${label_config}
    set to dictionary   ${arg_dic}     label     ${label_catenate}
    ${dic}      [200] API create service fee tier mask      &{arg_dic}
    set test variable   ${test_fee_tier_mask_id}    ${dic.id}

[Payment][Suite][200] - create fee tier
   [Arguments]  &{arg_dic}
   ${text}=   generate random string    10      [LETTERS]
   ${number}=   generate random string    1      123
   ${number1}=   generate random string    7      123456789
   ${service_group_name}      set variable   	${text}
   ${service_name}      set variable   	${text}
   ${command_id}     set variable    1
   ${fee_tier_condition}     set variable    <
   ${condition_amount}     set variable    ${number1}
   ${fee_type}     set variable    % rate
   ${fee_amount}     set variable    10
   ${bonus_type}     set variable    NON
   ${bonus_amount}     set variable    null
   ${amount_type}     set variable    null
   ${settlement_type}     set variable    Amount

    set suite variable          ${suite_command_<_condition}     ${fee_tier_condition} ${condition_amount}
    set suite variable     ${suite_condition_amount}     ${condition_amount}
    set suite variable          ${suite_fee_tier_condition}     ${fee_tier_condition}

    set to dictionary     ${arg_dic}    fee_tier_condition      ${fee_tier_condition}
    set to dictionary     ${arg_dic}    condition_amount      ${condition_amount}
    set to dictionary     ${arg_dic}    fee_type      ${fee_type}
    set to dictionary     ${arg_dic}    fee_amount      ${fee_amount}
    set to dictionary     ${arg_dic}    bonus_type      ${bonus_type}
    set to dictionary     ${arg_dic}    bonus_amount      ${bonus_amount}
    set to dictionary     ${arg_dic}    amount_type      ${amount_type}
    set to dictionary     ${arg_dic}    settlement_type      ${settlement_type}
    ${return_dic}   [Payment][Reuse][200] - create fee tier    &{arg_dic}

[Payment][Test][200] - Create tier mask for service without requestor condition
    ${tier_mask_name}     generate random string      10   [LETTERS]

    ${requestor_user_type} =    set variable  agent
    ${requestor_type_id}=    set variable    ${suite_water_agent_type_id}
    ${requestor_type_name}=    set variable    ${suite_water_agent_type_name}
    ${requestor_classification_id}=    set variable     ${suite_agent_classification_id}
    ${requestor_classification_name}=    set variable     ${suite_agent_classification_name}
    ${requestor_postal_code}=    set variable   ${suite_water_agent_current_address_postal_code}

    ${initiator_user_type}=    set variable    agent
    ${initiator_user_type_label}=    set variable    Agent
    ${initiator_type_id}=    set variable    ${suite_water_agent_type_id}
    ${initiator_type_name}=    set variable    ${suite_water_agent_type_name}
    ${initiator_classification_id}=    set variable    ${suite_agent_classification_id}
    ${initiator_classification_name}=    set variable     ${suite_agent_classification_name}
    ${initiator_postal_code}=    set variable   ${suite_water_agent_current_address_postal_code}

    ${payer_user_type}=    set variable    agent
    ${payer_user_type_label}=    set variable    Agent
    ${payer_type_id}=    set variable       ${suite_water_agent_type_id}
    ${payer_type_name}=    set variable    ${suite_water_agent_type_name}
    ${payer_classification_id}=    set variable     ${suite_agent_classification_id}
    ${payer_classification_name}=    set variable     ${suite_agent_classification_name}
    ${payer_postal_code}=    set variable   ${suite_water_agent_current_address_postal_code}

    ${payee_user_type}=    set variable    customer
    ${payee_user_type_name}=    set variable    customer
    ${payee_user_type_name_label}=    set variable    Customer
    ${payee_classification_id}=    set variable  ${suite_customer_classification_id}
    ${payee_classification_name}=    set variable  ${suite_customer_classification_name}
    ${payee_postal_code}=    set variable   ${suite_customer_current_address_postal_code}

    [Common] - get current start date
    set test variable  ${from_effective_timestamp}      ${test_current_from_date}
    [Common] - get current end date
    set test variable  ${to_effective_timestamp}      ${test_current_end_date}

    ${arg_dic}  create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     service_id=${suite_service_id}
    ...     tier_mask_name=${tier_mask_name}
    ...     initiator_user_type=${initiator_user_type}
    ...     initiator_type_id=${initiator_type_id}
    ...     initiator_classification_id=${initiator_classification_id}
    ...     payer_user_type=${payer_user_type}
    ...     payer_type_id=${payer_type_id}
    ...     payee_user_type=${payee_user_type}
    ...     from_effective_timestamp=${from_effective_timestamp}
    ...     to_effective_timestamp=${to_effective_timestamp}
    ${dic}  [200] API create tier mask for service   ${arg_dic}

    set test variable   ${test_service_tier_mask_id}    ${dic.service_tier_mask_id}
    set test variable   ${test_service_tier_mask_name}    ${tier_mask_name}

    set test variable   ${test_service_tier_mask_priority}      2
    set test variable   ${test_service_tier_mask_display_name}      2. ${test_service_tier_mask_name}

    #    Requestor condition
    set test variable   ${test_service_tier_mask_requestor_actor}      Requestor
    set test variable   ${test_service_tier_mask_requestor_actor_user_type}      ${requestor_user_type}
    set test variable   ${test_service_tier_mask_requestor_actor_type_name}      ${requestor_type_name}
    set test variable   ${test_service_tier_mask_requestor_actor_classification_name}      ${requestor_classification_name}
    set test variable   ${test_service_tier_mask_requestor_actor_postal_code}      ${requestor_postal_code}

    #    Initiator condition
    set test variable   ${test_service_tier_mask_initiator_actor}      Initiator
    set test variable   ${test_service_tier_mask_initiator_actor_user_type}      ${initiator_user_type}
    set test variable   ${test_service_tier_mask_initiator_actor_user_type_label}      ${initiator_user_type_label}
    set test variable   ${test_service_tier_mask_initiator_actor_type_name}      ${initiator_type_name}
    set test variable   ${test_service_tier_mask_initiator_actor_classification_name}      ${initiator_classification_name}
    set test variable   ${test_service_tier_mask_initiator_actor_postal_code}     ${empty}

    #    Payer condition
    set test variable   ${test_service_tier_mask_payer_actor}      Payer
    set test variable   ${test_service_tier_mask_payer_user_type}      ${payer_user_type}
    set test variable   ${test_service_tier_mask_payer_user_type_label}      ${payer_user_type_label}
    set test variable   ${test_service_tier_mask_payer_classification_name}      ${empty}
    set test variable   ${test_service_tier_mask_payer_postal_code}     ${empty}

    #    Paye condition
    set test variable   ${test_service_tier_mask_payere_actor}      Payee
    set test variable   ${test_service_tier_mask_payee_user_type}      ${payee_user_type}
    set test variable   ${test_service_tier_mask_payee_user_type_label}      ${payee_user_type_name_label}
    set test variable   ${test_service_tier_mask_payee_type_id}      ${empty}
    set test variable   ${test_service_tier_mask_payee_classification_name}      ${empty}
    set test variable   ${test_service_tier_mask_payee_postal_code}     ${empty}

[Payment][Settlement][Test] - Create list of unsettled_transactions
    [Arguments]         &{arg_dic}
    @{list_condition}    create list
        :FOR     ${index}   IN RANGE    0   ${arg_dic.number}
    \   ${transaction_id}             generate random string    5    [LETTERS]
    \   ${amount}                     generate random string    5     123456789
    \	${amount_USD}				  generate random string    2     123456789
    \   ${current_date}               Get Current Date	    UTC       result_format=%Y-%m-%dT%H:%M:%SZ
    \   set test variable    ${test_internal_transaction_id_${index}}                 ${arg_dic.internal_order_id_${index}}
    \   set test variable    ${test_external_transaction_id_${index}}                 ${test_case_id}_external_${transaction_id}
    \	run keyword if      '${arg_dic.currency}'=='USD'		set test variable     ${test_unsettled_amount_per_transaction_${index}}        ${amount}.${amount_USD}
    \   						...		ELSE					set test variable     ${test_unsettled_amount_per_transaction_${index}}        ${amount}000
    \   set test variable    ${test_date_of_transaction_${index}}                     ${current_date}
    \   ${index}        set variable    {"internal_transaction_id":"${test_internal_transaction_id_${index}}","external_transaction_id":"${test_external_transaction_id_${index}}","unsettled_amount_per_transaction":"${test_unsettled_amount_per_transaction_${index}}","date_of_transaction":"${test_date_of_transaction_${index}}"}
    \   append to list      ${list_condition}      ${index}
    \   log                 ${list_condition}
    set test variable   ${test_list_condition}       ${list_condition}
    set test variable   ${test_size_of_list_condition}       ${arg_dic.number}
    [Return]    ${list_condition}

[Payment][Settlement][Suite] - Create list of unsettled_transactions
    [Arguments]         &{arg_dic}
    @{list_condition}    create list
        :FOR     ${index}   IN RANGE    0   ${arg_dic.number}
    \   ${transaction_id}             generate random string    5    [LETTERS]
    \   ${amount}                     generate random string    3     123456789
    \   ${current_date}               Get Current Date	    UTC       result_format=%Y-%m-%dT%H:%M:%SZ
    \   ${web_current_date}           Convert Date          ${current_date}     result_format=${CONFIG_NEW_DATETIME_PARTERN}
    \   ${current_start_date}=        Get Current Date	    UTC       result_format=%Y-%m-%dT00:00:00Z
    \   ${web_current_start_date}     Convert Date          ${current_start_date}     result_format=${CONFIG_NEW_DATETIME_PARTERN}
    \   ${tomorrow_date}=             add time to date      ${current_date}       1 days     result_format=%Y-%m-%dT00:00:00Z
    \   ${web_tomorrow_date}          Convert Date          ${tomorrow_date}     result_format=${CONFIG_NEW_DATETIME_PARTERN}
    \   set suite variable    ${suite_internal_transaction_id_${index}}                 ${arg_dic.internal_order_id_${index}}
    \   set suite variable    ${suite_external_transaction_id_${index}}                 test_external_${transaction_id}
    \   set suite variable    ${suite_unsettled_amount_per_transaction_${index}}        ${amount}000
    \   set suite variable    ${suite_date_of_transaction_${index}}                     ${current_date}
    \   set suite variable    ${suite_web_date_of_transaction_${index}}                 ${web_current_date}
    \   ${index}        set variable    {"internal_transaction_id":"${suite_internal_transaction_id_${index}}","external_transaction_id":"${suite_external_transaction_id_${index}}","unsettled_amount_per_transaction":"${suite_unsettled_amount_per_transaction_${index}}","date_of_transaction":"${suite_date_of_transaction_${index}}"}
    \   append to list      ${list_condition}      ${index}
    \   log                 ${list_condition}
    set suite variable   ${suite_list_condition}       ${list_condition}
    set suite variable   ${suite_size_of_list_condition}       ${arg_dic.number}
    [Return]    ${list_condition}

[Payment][Test][200] - Create pending settlement with all fields
    [Arguments]    &{arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   payer_sof_type_id    2
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   payee_sof_type_id    2
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   access_token    ${suite_setup_admin_access_token}
    ${random_string}    generate random string    5    [LETTERS]
    ${amount}           generate random string    3     123456789
    ${amount_USD}       generate random string    2     123456789
    ${reconciliation_id}          generate random string    9     123456789
    ${time_zone}    run keyword if     '${env}'=='local'    set variable   local
    ...     ELSE       set variable     UTC

    ${current_date}                Get Current Date	    ${time_zone}      result_format=%Y-%m-%dT%H:%M:%SZ
    ${current_date_start_date}     Get Current Date	    ${time_zone}      result_format=%Y-%m-%dT00:00:00Z
    ${current_date_end_date}       Get Current Date	    ${time_zone}      result_format=%Y-%m-%dT23:59:59Z
    ${tomorrow_date}=              add time to date     ${current_date}       1 days     result_format=%Y-%m-%dT00:00:00Z
    ${web_tomorrow_date}=          Convert Date         ${tomorrow_date}     result_format=${CONFIG_NEW_DATETIME_PARTERN}

    set test variable    ${test_settlement_description}                 ${test_case_id}_description_settlement_${random_string}
    set test variable    ${test_created_by}                             LOCAL
    set test variable    ${test_due_date}                               ${tomorrow_date}
    set test variable    ${test_web_due_date}                           ${web_tomorrow_date}
    set test variable    ${test_reconciliation_id}                      ${reconciliation_id}
    set test variable    ${test_reconciliation_from_date}               ${current_date_start_date}
    set test variable    ${test_reconciliation_to_date}                 ${current_date_end_date}
    run keyword if      '${arg_dic.currency}'=='USD'		set test variable    ${test_settlement_amount}        ${amount}.${amount_USD}
    						...		ELSE					set test variable    ${test_settlement_amount}        ${amount}000

    ${dic}    [200] API create pending settlement    access_token=${arg_dic.access_token}     description=${test_settlement_description}  amount=${test_settlement_amount}    payer_user_id=${arg_dic.payer_user_id}  payer_user_type=${arg_dic.payer_user_type}    payer_sof_id=${arg_dic.payer_sof_id}    payer_sof_type_id=${arg_dic.payer_sof_type_id}     payee_user_id=${arg_dic.payee_user_id}  payee_user_type=${arg_dic.payee_user_type}    payee_sof_id=${arg_dic.payee_sof_id}    payee_sof_type_id=${arg_dic.payee_sof_type_id}     created_by=${test_created_by}    currency=${arg_dic.currency}  due_date=${test_due_date}    reconciliation_id=${test_reconciliation_id}    reconciliation_from_date=${test_reconciliation_from_date}  reconciliation_to_date=${test_reconciliation_to_date}     unsettled_transactions=${arg_dic.unsettled_transactions}
    set test variable    ${test_settlement_id}    ${dic.id}

[Payment][Test][200] - Create pending settlement over duedate
    [Arguments]    &{arg_dic}
	${random_string}    generate random string    5    [LETTERS]
	${amount}           generate random string    3     123456789
    ${reconciliation_id}          generate random string    9     123456789
    ${time_zone}    run keyword if     '${env}'=='local'    set variable   local
    ...     ELSE       set variable     UTC
    ${current_date}                Get Current Date	    ${time_zone}          result_format=%Y-%m-%dT%H:%M:%SZ
	${due_date}=                   add time to date     ${current_date}       5 s     result_format=%Y-%m-%dT%H:%M:%SZ
	${web_due_date}                Convert Date         ${due_date}           result_format=${CONFIG_NEW_DATETIME_PARTERN}
    ${current_date_start_date}     Get Current Date	    ${time_zone}          result_format=%Y-%m-%dT00:00:00Z
    ${current_date_end_date}       Get Current Date	    ${time_zone}          result_format=%Y-%m-%dT23:59:59Z

	set test variable    ${test_settlement_description}                 suite_description_settlement ${random_string}
	set test variable    ${test_created_by}                             LOCAL
	set test variable    ${test_settlement_amount}                      ${amount}000
	set test variable    ${test_due_date}                               ${due_date}
	set test variable    ${test_web_due_date}                           ${web_due_date}
    set test variable    ${test_reconciliation_id}                      ${reconciliation_id}
    set test variable    ${test_reconciliation_from_date}               ${current_date_start_date}
    set test variable    ${test_reconciliation_to_date}                 ${current_date_end_date}

	${dic}    [200] API create pending settlement    access_token=${suite_setup_admin_access_token}     description=${test_settlement_description}  amount=${test_settlement_amount}    payer_user_id=${arg_dic.payer_user_id}  payer_user_type=${arg_dic.payer_user_type}    payer_sof_id=${arg_dic.payer_sof_id}    payer_sof_type_id=2     payee_user_id=${arg_dic.payee_user_id}  payee_user_type=${arg_dic.payee_user_type}    payee_sof_id=${arg_dic.payee_sof_id}    payee_sof_type_id=2     created_by=${test_created_by}    currency=${arg_dic.currency}  due_date=${test_due_date}   reconciliation_id=${test_reconciliation_id}    reconciliation_from_date=${test_reconciliation_from_date}  reconciliation_to_date=${test_reconciliation_to_date}     unsettled_transactions=${arg_dic.unsettled_transactions}

	#Wait until pending_settlement is over due date
	Sleep    7s
	set test variable    ${test_settlement_id}    ${dic.id}

[Payment][Suite][200] - Create pending settlement with all fields
    [Arguments]    &{arg_dic}
    ${random_string}    generate random string    5    [LETTERS]
    ${amount}           generate random string    3     123456789
    ${amount_USD}       generate random string    2     123456789
    ${reconciliation_id}          generate random string    9     123456789
    ${time_zone}    run keyword if     '${env}'=='local'    set variable   local
    ...     ELSE       set variable     UTC
    ${current_date}               Get Current Date	    ${time_zone}       result_format=%Y-%m-%dT%H:%M:%SZ
    ${current_date_start_date}     Get Current Date	    ${time_zone}      result_format=%Y-%m-%dT00:00:00Z
    ${current_date_end_date}       Get Current Date	    ${time_zone}      result_format=%Y-%m-%dT23:59:59Z
    ${tomorrow_date}=             add time to date      ${current_date}       1 days     result_format=%Y-%m-%dT00:00:00Z
    ${web_tomorrow_date}          Convert Date          ${tomorrow_date}     result_format=${CONFIG_NEW_DATETIME_PARTERN}

    set suite variable    ${suite_reconciliation_id}                      ${reconciliation_id}
    set suite variable    ${suite_settlement_description}                 suite_description_settlement ${random_string}
    set suite variable    ${suite_created_by}                             LOCAL
    set suite variable    ${suite_settlement_amount}                      ${amount}000
    set suite variable    ${suite_due_date}                               ${tomorrow_date}
    set suite variable    ${suite_web_due_date}                           ${web_tomorrow_date}
    set suite variable    ${suite_reconciliation_from_date}               ${current_date_start_date}
    set suite variable    ${suite_reconciliation_to_date}                 ${current_date_end_date}
    ${dic}    [200] API create pending settlement    access_token=${suite_setup_admin_access_token}     description=${suite_settlement_description}  amount=${suite_settlement_amount}    payer_user_id=${arg_dic.payer_user_id}  payer_user_type=${arg_dic.payer_user_type}    payer_sof_id=${arg_dic.payer_sof_id}    payer_sof_type_id=2     payee_user_id=${arg_dic.payee_user_id}  payee_user_type=${arg_dic.payee_user_type}    payee_sof_id=${arg_dic.payee_sof_id}    payee_sof_type_id=2     created_by=${suite_created_by}    currency=${arg_dic.currency}  due_date=${suite_due_date}   reconciliation_id=${suite_reconciliation_id}    reconciliation_from_date=${suite_reconciliation_from_date}  reconciliation_to_date=${suite_reconciliation_to_date}       unsettled_transactions=${arg_dic.unsettled_transactions}

    set suite variable    ${suite_settlement_id}    ${dic.id}

[Payment][Suite][Prepare] - Create pending settlement over duedate
    [Arguments]    &{arg_dic}
    ${random_string}    generate random string    5    [LETTERS]
    ${amount}           generate random string    3     123456789
    ${reconciliation_id}          generate random string    9     123456789
	${time_zone}        run keyword if     '${env}'=='local'    set variable   local
    ...     ELSE        set variable     UTC
    ${current_date}                Get Current Date	    ${time_zone}       result_format=%Y-%m-%dT%H:%M:%SZ
    ${due_date}=                   add time to date     ${current_date}       5 s     result_format=%Y-%m-%dT%H:%M:%SZ
    ${web_due_date}                Convert Date         ${due_date}     result_format=${CONFIG_NEW_DATETIME_PARTERN}
    ${current_date_start_date}     Get Current Date	    ${time_zone}      result_format=%Y-%m-%dT00:00:00Z
    ${current_date_end_date}       Get Current Date	    ${time_zone}      result_format=%Y-%m-%dT23:59:59Z

    set suite variable    ${suite_settlement_description}                 suite_description_settlement ${random_string}
    set suite variable    ${suite_created_by}                             LOCAL
    set suite variable    ${suite_settlement_amount}                      ${amount}000
    set suite variable    ${suite_due_date}                               ${due_date}
    set suite variable    ${suite_web_due_date}                           ${web_due_date}
    set suite variable    ${suite_reconciliation_id}                      ${reconciliation_id}
    set suite variable    ${suite_reconciliation_from_date}               ${current_date_start_date}
    set suite variable    ${suite_reconciliation_to_date}                 ${current_date_end_date}

    ${dic}    [200] API create pending settlement    access_token=${suite_setup_admin_access_token}     description=${suite_settlement_description}  amount=${suite_settlement_amount}    payer_user_id=${arg_dic.payer_user_id}  payer_user_type=${arg_dic.payer_user_type}    payer_sof_id=${arg_dic.payer_sof_id}    payer_sof_type_id=2     payee_user_id=${arg_dic.payee_user_id}  payee_user_type=${arg_dic.payee_user_type}    payee_sof_id=${arg_dic.payee_sof_id}    payee_sof_type_id=2     created_by=${suite_created_by}    currency=${arg_dic.currency}  due_date=${suite_due_date}   reconciliation_id=${suite_reconciliation_id}    reconciliation_from_date=${suite_reconciliation_from_date}  reconciliation_to_date=${suite_reconciliation_to_date}     unsettled_transactions=${arg_dic.unsettled_transactions}

    #Wait until pending_settlement is over due date
    Sleep    7s
    set suite variable    ${suite_over_due_date_settlement_id}    ${dic.id}
    
[Payment][Test][200] - Create pending settlement with only required fields
    [Arguments]    &{arg_dic}
    ${random_string}    generate random string    5    [LETTERS]
    ${amount}           generate random string    3     123456789
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   due_date_in_time    1 days
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   payer_sof_type_id    2
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   payee_sof_type_id    2
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   access_token    ${suite_setup_admin_access_token}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   amount    ${amount}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   reconciliation_id    ${param_not_used}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   reconciliation_from_date    ${param_not_used}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   reconciliation_to_date    ${param_not_used}
    ${random_string}    generate random string    5    [LETTERS]
    ${amount}           generate random string    3     123456789
    ${amount_USD}       generate random string    2     123456789
    ${time_zone}    run keyword if     '${env}'=='local'    set variable   local
    ...     ELSE       set variable     UTC

    ${current_date}                Get Current Date	    ${time_zone}         result_format=%Y-%m-%dT%H:%M:%SZ
    ${current_date_start_date}     Get Current Date	    ${time_zone}         result_format=%Y-%m-%dT00:00:00Z
    ${current_date_end_date}       Get Current Date	    ${time_zone}         result_format=%Y-%m-%dT23:59:59Z
    ${tomorrow_date}=              add time to date     ${current_date}      ${arg_dic.due_date_in_time}    result_format=%Y-%m-%dT%H:%M:%SZ

    set test variable    ${test_settlement_description}                 ${test_case_id}_description_settlement_${random_string}
    set test variable    ${test_created_by}                             LOCAL
    set test variable    ${test_settlement_amount}                      ${arg_dic.amount}
    set test variable    ${test_due_date}                               ${tomorrow_date}

    ${dic}    [200] API create pending settlement    access_token=${arg_dic.access_token}     description=${test_settlement_description}  amount=${test_settlement_amount}    payer_user_id=${arg_dic.payer_user_id}  payer_user_type=${arg_dic.payer_user_type}    payer_sof_id=${arg_dic.payer_sof_id}    payer_sof_type_id=${arg_dic.payer_sof_type_id}     payee_user_id=${arg_dic.payee_user_id}  payee_user_type=${arg_dic.payee_user_type}    payee_sof_id=${arg_dic.payee_sof_id}    payee_sof_type_id=${arg_dic.payee_sof_type_id}     created_by=${test_created_by}    currency=${arg_dic.currency}   due_date=${test_due_date}

    set test variable    ${test_settlement_id}    ${dic.id}

[Payment][Suite][200] - Create pending settlement with only required fields
    [Arguments]    &{arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   payer_sof_type_id    2
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   payee_sof_type_id    2
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   reconciliation_id    ${param_not_used}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   reconciliation_from_date    ${param_not_used}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   reconciliation_to_date    ${param_not_used}
    ${random_string}    generate random string    5    [LETTERS]
    ${amount}           generate random string    3     123456789
    ${time_zone}        run keyword if     '${env}'=='local'    set variable   local
    ...     ELSE        set variable     UTC
    ${current_date}                Get Current Date	    ${time_zone}         result_format=%Y-%m-%dT%H:%M:%SZ
    ${current_date_start_date}     Get Current Date	    ${time_zone}         result_format=%Y-%m-%dT00:00:00Z
    ${current_date_end_date}       Get Current Date	    ${time_zone}         result_format=%Y-%m-%dT23:59:59Z
    ${tomorrow_date}=              add time to date     ${current_date}      1 days     result_format=%Y-%m-%dT00:00:00Z

    set suite variable    ${suite_settlement_description}                 Prepare_description_settlement_${random_string}
    set suite variable    ${suite_created_by}                             LOCAL
    set suite variable    ${suite_settlement_amount}                      ${amount}000

    ${dic}    [200] API create pending settlement    access_token=${suite_setup_admin_access_token}     description=${suite_settlement_description}  amount=${suite_settlement_amount}    payer_user_id=${arg_dic.payer_user_id}  payer_user_type=${arg_dic.payer_user_type}    payer_sof_id=${arg_dic.payer_sof_id}    payer_sof_type_id=${arg_dic.payer_sof_type_id}     payee_user_id=${arg_dic.payee_user_id}  payee_user_type=${arg_dic.payee_user_type}    payee_sof_id=${arg_dic.payee_sof_id}    payee_sof_type_id=${arg_dic.payee_sof_type_id}    created_by=${suite_created_by}    currency=${arg_dic.currency}

    set suite variable    ${suite_settlement_id}    ${dic.id}

[Payment][Test][200] - Update pending settlement with only due date
    [Arguments]    ${access_token}
    ${current_date}               Get Current Date	    UTC       result_format=%Y-%m-%dT%H:%M:%SZ
    ${after_tomorrow_date}=             add time to date    ${current_date}       2 days     result_format=%Y-%m-%dT00:00:00Z
    set test variable    ${test_due_date}                               ${after_tomorrow_date}
    ${dic}    [200] API update pending settlement with only due date    access_token=${access_token}         settlement_id=${test_settlement_id}       due_date=${test_due_date}

[Agent][Suite][200] - Create Setup agent with currency
    [Arguments]     &{arg_dic}
    ${text}=   generate random string    10      [LETTERS]
    ${current_address_postal_code}   generate random string    10      123456789
    ${agent_type_name}      set variable   	${text}
    ${agent_first_name}      set variable   	agent_first_name_${text}
    ${agent_last_name}      set variable   	agent_last_name_${text}
    ${agent_type_id}      set variable   	${setup_company_agent_type_id}
    ${agent_unique_reference}      set variable   	${text}
    ${agent_username}     set variable    ${text}
    ${agent_password}      set variable   	${setup_agent_password_encrypted}
    ${agent_password_login}     set variable    ${setup_agent_password_encrypted_utf8}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      agent_type_name=${agent_type_name}
    ...     unique_reference=${agent_unique_reference}      identity_identity_type_id=1
    ...     first_name=${agent_first_name}    last_name=${agent_last_name}     agent_username=${agent_username}      agent_password=${agent_password}
    ...     agent_password_login=${agent_password_login}      currency=${arg_dic.currency}
    ...     user_type_id=2      current_address_postal_code=${current_address_postal_code}

    ${dic}  [Agent][Reuse][200] - create agent      ${arg_dic}
    set suite variable      ${suite_setup_agent_id}       ${dic.agent_id}
    set suite variable      ${suite_setup_agent_type}       agent
    set suite variable      ${suite_setup_agent_type_name}       ${agent_type_name}
    set suite variable      ${suite_setup_agent_first_name}       ${agent_first_name}
    set suite variable      ${suite_setup_agent_last_name}       ${agent_last_name}
#    set suite variable      ${suite_setup_sof_cash_id}       ${dic.sof_cash_id_in_${currency}}
    set suite variable      ${suite_setup_agent_unique_reference}       ${agent_unique_reference}
    set suite variable      ${suite_setup_agent_type_id}       ${dic.agent_type_id}
    set suite variable      ${suite_setup_agent_current_address_postal_code}       ${current_address_postal_code}
    set suite variable      ${suite_setup_agent_access_token}     ${dic.agent_access_token}

    ${currency_list}     split string    ${arg_dic.currency}     ,
    ${currency_list_length}   get length      ${currency_list}
    :FOR     ${index}   IN RANGE    0   ${currency_list_length}
    \   ${ccy}     get from list     ${currency_list}      ${index}
    \   set suite variable      ${suite_setup_sof_cash_id_in_${ccy}}       ${dic.sof_cash_id_in_${ccy}}


[Payment][Suite][200] - System user create settlement configuration
    [Arguments]     &{arg_dic}
    ${return_dic}  [200] API system user create settlement configuration     &{arg_dic}
    set suite variable   ${suite_settlement_configuration_id}   ${return_dic.id}

[Payment][Test][200] - System user create settlement configuration
    [Arguments]     &{arg_dic}
    ${dic}  [200] API system user create settlement configuration     &{arg_dic}
    set test variable   ${test_settlement_configuration_id}   ${dic.id}

[Payment][Test][200] - Create normal order and excute
    [Arguments]     ${output_order_id}=test_order_id      &{arg_dic}
    ${product_name}         generate random string      20   prepare_product_name_[LETTERS]
    ${product_ref_1}        generate random string      20   prepare_service_ref_1_[LETTERS]
    ${product_ref_2}        generate random string      20   prepare_service_ref_2_[LETTERS]
    ${product_ref_3}        generate random string      20   prepare_service_ref_3_[LETTERS]
    ${product_ref_4}        generate random string      20   prepare_service_ref_4_[LETTERS]
    ${product_ref_5}        generate random string      20   prepare_service_ref_5_[LETTERS]

    ${ext_transaction_id}      generate random string      12   [NUMBERS]
    ${product_service_id}      generate random string      12   [NUMBERS]
    ${dic}     [200] API payment create normal order
    ...     ${arg_dic.access_token}
    ...     ${ext_transaction_id}
    ...     ${arg_dic.product_service_id}
    ...     ${arg_dic.product_service_name}
    ...     ${product_name}
    ...     ${product_ref_1}
    ...     ${product_ref_2}
    ...     ${product_ref_3}
    ...     ${product_ref_4}
    ...     ${product_ref_5}
    ...     ${arg_dic.payer_user_ref_type}
    ...     ${arg_dic.payer_user_ref_value}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${arg_dic.payee_user_ref_type}
    ...     ${arg_dic.payee_user_ref_value}
    ...     ${arg_dic.payee_user_id}
    ...     ${arg_dic.payee_user_type}
    ...     ${arg_dic.amount}
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    ${order_id}=  get from dictionary   ${dic}   order_id
    [Common] - Set variable       name=${output_order_id}       value=${order_id}

     ${execute_order_dic}     create dictionary
        ...     access_token=${arg_dic.payer_access_token_id}
        ...     order_id=${order_id}
    [200] API payment execute order     ${execute_order_dic}

[Payment][Suite][200] - Create normal order and excute
    [Arguments]     &{arg_dic}
    ${product_name}         generate random string      20   prepare_product_name_[LETTERS]
    ${product_ref_1}        generate random string      20   prepare_service_ref_1_[LETTERS]
    ${product_ref_2}        generate random string      20   prepare_service_ref_2_[LETTERS]
    ${product_ref_3}        generate random string      20   prepare_service_ref_3_[LETTERS]
    ${product_ref_4}        generate random string      20   prepare_service_ref_4_[LETTERS]
    ${product_ref_5}        generate random string      20   prepare_service_ref_5_[LETTERS]

    ${ext_transaction_id}      generate random string      12   [NUMBERS]
    ${product_service_id}      generate random string      12   [NUMBERS]
    ${dic}     [200] API payment create normal order
    ...     ${arg_dic.access_token}
    ...     ${ext_transaction_id}
    ...     ${arg_dic.product_service_id}
    ...     ${arg_dic.product_service_name}
    ...     ${product_name}
    ...     ${product_ref_1}
    ...     ${product_ref_2}
    ...     ${product_ref_3}
    ...     ${product_ref_4}
    ...     ${product_ref_5}
    ...     ${arg_dic.payer_user_ref_type}
    ...     ${arg_dic.payer_user_ref_value}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${arg_dic.payee_user_ref_type}
    ...     ${arg_dic.payee_user_ref_value}
    ...     ${arg_dic.payee_user_id}
    ...     ${arg_dic.payee_user_type}
    ...     ${arg_dic.amount}
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}
    ${response}=    get from dictionary    ${dic}   response
    ${order_id}=  get from dictionary   ${dic}   order_id
    set suite variable       ${suite_order_id}     ${order_id}

     ${execute_order_dic}     create dictionary
        ...     access_token=${arg_dic.payer_access_token_id}
        ...     order_id=${suite_order_id}
    [200] API payment execute order     ${execute_order_dic}

[Payment][200] Company agent fund in for agent
    [Arguments]   ${output_order_id}=test_order_id    &{arg_dic}
    ${dic}   create dictionary      username=${setup_company_agent_account_id}      password=${setup_company_agent_password_encrypted}
    ${dic}    [200] API agent authentication        ${dic}
    ${company_agent_access_token}     get from dictionary      ${dic}     access_token

    API create user sof cash
        ...     ${arg_dic.currency}
        ...     ${company_agent_access_token}
        ...     ${setup_admin_client_id}
        ...     ${setup_admin_client_secret}

    [200] API add company balance    ${arg_dic.currency}         ${arg_dic.amount}        ${suite_admin_access_token}

    [Payment][Test][200] - Create normal order and excute
        ...     output_order_id=${output_order_id}
        ...     access_token=${company_agent_access_token}
        ...     payer_access_token_id=${company_agent_access_token}
        ...     product_service_id=${arg_dic.product_service_id}
        ...     product_service_name=${arg_dic.product_service_name}
        ...     payer_user_ref_type=access token
        ...     payer_user_ref_value=${company_agent_access_token}
        ...     payee_user_id=${arg_dic.agent_id}
        ...     payee_user_type=agent
        ...     payee_user_ref_type=${param_not_used}
        ...     payee_user_ref_value=${param_not_used}
        ...     amount=${arg_dic.amount}

[Payment][Suite][200] Company agent fund in for agent
    [Arguments]   ${output_order_id}=test_order_id    &{arg_dic}
    ${dic}   create dictionary      username=${setup_company_agent_account_id}      password=${setup_company_agent_password_encrypted}
    ${dic}    [200] API agent authentication        ${dic}
    ${company_agent_access_token}     get from dictionary      ${dic}     access_token

    API create user sof cash
        ...     ${arg_dic.currency}
        ...     ${company_agent_access_token}
        ...     ${setup_admin_client_id}
        ...     ${setup_admin_client_secret}

    [200] API add company balance    ${arg_dic.currency}         ${arg_dic.amount}        ${suite_admin_access_token}

    [Payment][Suite][200] - Create normal order and excute
        ...     output_order_id=${output_order_id}
        ...     access_token=${company_agent_access_token}
        ...     payer_access_token_id=${company_agent_access_token}
        ...     product_service_id=${arg_dic.product_service_id}
        ...     product_service_name=${arg_dic.product_service_name}
        ...     payer_user_ref_type=access token
        ...     payer_user_ref_value=${company_agent_access_token}
        ...     payee_user_id=${arg_dic.agent_id}
        ...     payee_user_type=agent
        ...     payee_user_ref_type=${param_not_used}
        ...     payee_user_ref_value=${param_not_used}
        ...     amount=${arg_dic.amount}

[Payment][Suite][200] - System user resolve pending settlement
    [Arguments]     &{arg_dic}
    ${return_dic}  [200] API system user resolve pending settlement     &{arg_dic}
    set suite variable   ${suite_settlement_resolving_history_id}   ${return_dic.id}

[Payment][200] Create service group
    [Arguments]     ${headers}=${SUITE_ADMIN_HEADERS}      ${output_id}=test_service_group_id
    ${service_group_name}      generate random string      20    prepare_[LETTERS]
    ${service_group_description}      generate random string      30        prepare_[LETTERS]
    ${service_group_display_name}       generate random string      20      prepare_[LETTERS]
    ${service_group_display_name_local}     generate random string   20      prepare_[LETTERS]
    ${image_url}    generate random string  20      prepare_[LETTERS]
    [Payment][Prepare] - Create Service Group - body
        ...     service_group_name=${service_group_name}
        ...     description=${service_group_description}
        ...     image_url=${image_url}
        ...     display_name=${service_group_display_name}
        ...     display_name_local=${service_group_display_name_local}
    [Payment][200] - Add service group
        ...     headers=${headers}
        ...     body=${body}
    [Payment][Extract] Create service group - service group id
        ...     output=${output_id}

[Payment][Test][200] - System user resolve pending settlement
    [Arguments]     &{arg_dic}
    ${return_dic}  [200] API system user resolve pending settlement     &{arg_dic}
    set suite variable   ${test_settlement_resolving_history_id}   ${return_dic.id}