*** Settings ***
Resource         ../../2_api/3_reuse_function/imports.robot

*** Keywords ***
[Test][200] API create agent type
    [200] API create agent type     ${setup_admin_client_id}       ${admin_client_secret}     ${admin_access_token}   ${agent_type_name}     ${agent_type_description}

[Suite][200] Create Agent Classification
    ${classification_name}=   generate random string    10      [LETTERS]
    ${classification_desc}=   generate random string    10      [LETTERS]
    ${arg_dic_classification}      create dictionary    name=${classification_name}    description=${classification_desc}    access_token=${suite_admin_access_token}
    ${dic_classification}    [200] API create agent classification    ${arg_dic_classification}
    ${agent_classification_id}    get from dictionary    ${dic_classification}    id
    set suite variable    ${suite_agent_classification_id}    ${agent_classification_id}
    set suite variable    ${suite_agent_classification_name}    ${classification_name}
    set suite variable    ${suite_agent_classification_desc}    ${classification_desc}

[Test][200] Create Agent Classification
    ${classification_name}=   generate random string    10      [LETTERS]
    ${classification_desc}=   generate random string    10      [LETTERS]
    ${arg_dic_classification}      create dictionary    name=${classification_name}    description=${classification_desc}    access_token=${suite_admin_access_token}
    ${dic_classification}    [200] API create agent classification    ${arg_dic_classification}
    ${agent_classification_id}    get from dictionary    ${dic_classification}    id
    set test variable    ${test_agent_classification_id}    ${agent_classification_id}
    set test variable    ${test_agent_classification_name}    ${classification_name}
    set test variable    ${test_agent_classification_desc}    ${classification_desc}

[Test][200] Prepare an agent with only sof_card
    ${agent_type_name}   generate random string  10    [LETTERS]
    set test variable       ${test_agent_type_name}     ${agent_type_name}
    set test variable       ${test_admin_access_token}  ${suite_admin_access_token}
    ${dic}      [200] API create agent type     ${test_admin_access_token}     ${test_agent_type_name}
    ${agent_type_id}     get from dictionary      ${dic}     id
    set test variable    ${test_agent_type_id}       ${agent_type_id}
    ${unique_reference}   generate random string  10    [NUMBERS]
    set test variable    ${test_unique_reference}       ${unique_reference}
    ${username}   generate random string  10    [NUMBERS]
    set test variable    ${test_username}       ${username}
    ${arg_dic}     create dictionary
    ...         access_token=${test_admin_access_token}
    ...         agent_type_id=${test_agent_type_id}
    ...         unique_reference=${test_unique_reference}
    ...         username=${test_username}
    ...         password=${setup_agent_password_encrypted}
    ...         identity_type_id=1

    ${dic}      [200] API create agent      ${arg_dic}
    ${agent_id}     get from dictionary      ${dic}     id
    set test variable    ${test_agent_id}       ${agent_id}
    set test variable    ${test_agent_username}    ${test_username}
    ${arg_dic}   create dictionary      username=${test_username}      password=${setup_agent_password_encrypted_encoded}
    ${dic}    [200] API agent authentication        ${arg_dic}
    ${access_token}     get from dictionary      ${dic}     access_token
    set test variable    ${test_agent_access_token}       ${access_token}


    ${provider_name}    generate random string  10    [LETTERS]
    set test variable   ${test_provider_name}       ${provider_name}
    ${dic}   [200] API add provider
    ...     ${test_provider_name}
    ...     ${test_admin_access_token}
    ${provider_id}     get from dictionary      ${dic}      id
    set test variable   ${test_provider_id}    ${provider_id}
    ${card_design_name}  generate random string  10    [LETTERS]
    set test variable    ${test_card_design_name}    ${card_design_name}

    ${card_account_name}   generate random string  20    [LETTERS]
    set test variable    ${test_card_account_name}       ${card_account_name}
    ${pattern}          generate random string  6   [NUMBERS]
    set test variable    ${test_pattern}       ${pattern}

    ${dic}  [200] API add card design
    ...    ${test_provider_id}
    ...    ${test_card_account_name}
	...    1
	...    true
	...    VND
	...    ${test_pattern}
	...    ${pre_link_url}
	...    10000
	...    ${link_url}
	...    10000
	...    ${un_link_url}
	...    10000
	...    ${debit_url}
	...    10000
	...    ${credit_url}
	...    10000
	...    ${check_status_url}
	...    10000
	...    ${cancel_url}
	...    10000
	...    ${pre_sof_order_url}
	...    10000
    ...    ${test_admin_access_token}
    ${citizen_id}   generate random string  6   [NUMBERS]
    set test variable    ${test_citizen_id}         ${citizen_id}
    ${cvv_cvc}    generate random string  3   [NUMBERS]
    set test variable   ${test_cvv_cvc}  ${cvv_cvc}
    ${pin}  generate random string  15   [NUMBERS]
    set test variable  ${test_pin}   ${pin}
    ${mobile_number}  generate random string  15   [NUMBERS]
    set test variable  ${test_mobile_number}  ${mobile_number}
    ${billing_address}  generate random string  15   [LETTERS]
    set test variable    ${test_billing_address}   ${billing_address}
    ${card number last 8}   generate random string  8   [NUMBERS]
    ${card number}     catenate     ${test_pattern}     ${card number last 8}
    ${dic}         [200] API verify (pre-link) card information
    ...     ${test_agent_access_token}
    ...     ${test_card_account_name}
    ...     ${card number}
    ...     ${test_citizen_id}
    ...     ${test_cvv_cvc}
    ...     ${test_pin}
    ...     12/08
    ...     11/08
    ...     ${test_mobile_number}
    ...     ${test_billing_address}
    ...     ATM
    ...     VND
    ${security_ref}     get from dictionary      ${dic}     security_ref
    set test variable    ${test_security_ref}     ${security_ref}
    ${dic}      [200] API user link card source of fund
    ...     ${test_agent_access_token}
    ...     ${test_card_account_name}
    ...     ${card number}
    ...     ${test_citizen_id}
    ...     ${test_cvv_cvc}
    ...     ${test_pin}
    ...     12/08
    ...     11/08
    ...     ${test_mobile_number}
    ...     ${test_billing_address}
    ...     VND
    ...     123456
    ...     ${test_security_ref}
    ${token}        get from dictionary      ${dic}     token
    set test variable    ${test_agent_card_token}     ${token}
    ${shop_name}         generate random string  20  [LETTERS]
    ${address}=     generate random string    70      [LETTERS]
    set test variable   ${test_agent_shop_name}      ${shop_name}
    [200] API create shop
    ...     ${suite_admin_access_token}
    ...     ${test_agent_id}
    ...     ${param_not_used}
    ...     ${shop_name}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${address}

[Suite][200] Prepare an agent with only sof_card
    ${agent_type_name}   generate random string  10    [LETTERS]
    set test variable       ${test_agent_type_name}     ${agent_type_name}
    set test variable       ${test_admin_access_token}  ${suite_admin_access_token}
    ${dic}      [200] API create agent type     ${test_admin_access_token}     ${test_agent_type_name}
    ${agent_type_id}     get from dictionary      ${dic}     id
    set test variable    ${test_agent_type_id}       ${agent_type_id}
    ${unique_reference}   generate random string  10    [NUMBERS]
    set test variable    ${test_unique_reference}       ${unique_reference}
    ${username}   generate random string  10    [NUMBERS]
    set test variable    ${test_username}       ${username}
    ${arg_dic}     create dictionary
    ...         access_token=${test_admin_access_token}
    ...         agent_type_id=${test_agent_type_id}
    ...         unique_reference=${test_unique_reference}
    ...         username=${test_username}
    ...         password=${setup_agent_password_encrypted}
    ...         identity_type_id=1

    ${dic}      [200] API create agent      ${arg_dic}
    ${agent_id}     get from dictionary      ${dic}     id
    set test variable    ${test_agent_id}       ${agent_id}
    set test variable    ${test_agent_username}    ${test_username}
    ${arg_dic}   create dictionary      username=${test_username}      password=${setup_agent_password_encrypted_encoded}
    ${dic}    [200] API agent authentication        ${arg_dic}
    ${access_token}     get from dictionary      ${dic}     access_token
    set test variable    ${test_agent_access_token}       ${access_token}


    ${provider_name}    generate random string  10    [LETTERS]
    set test variable   ${test_provider_name}       ${provider_name}
    ${dic}   [200] API add provider
    ...     ${test_provider_name}
    ...     ${test_admin_access_token}
    ${provider_id}     get from dictionary      ${dic}      id
    set test variable   ${test_provider_id}    ${provider_id}
    ${card_design_name}  generate random string  10    [LETTERS]
    set test variable    ${test_card_design_name}    ${card_design_name}

    ${card_account_name}   generate random string  20    [LETTERS]
    set test variable    ${test_card_account_name}       ${card_account_name}
    ${pattern}          generate random string  6   [NUMBERS]
    set test variable    ${test_pattern}       ${pattern}

    ${dic}  [200] API add card design
    ...    ${test_provider_id}
    ...    ${test_card_account_name}
	...    1
	...    true
	...    VND
	...    ${test_pattern}
	...    ${pre_link_url}
	...    10000
	...    ${link_url}
	...    10000
	...    ${un_link_url}
	...    10000
	...    ${debit_url}
	...    10000
	...    ${credit_url}
	...    10000
	...    ${check_status_url}
	...    10000
	...    ${cancel_url}
	...    10000
	...    ${pre_sof_order_url}
	...    10000
    ...    ${test_admin_access_token}
    ${citizen_id}   generate random string  6   [NUMBERS]
    set test variable    ${test_citizen_id}         ${citizen_id}
    ${cvv_cvc}    generate random string  3   [NUMBERS]
    set test variable   ${test_cvv_cvc}  ${cvv_cvc}
    ${pin}  generate random string  15   [NUMBERS]
    set test variable  ${test_pin}   ${pin}
    ${mobile_number}  generate random string  15   [NUMBERS]
    set test variable  ${test_mobile_number}  ${mobile_number}
    ${billing_address}  generate random string  15   [LETTERS]
    set test variable    ${test_billing_address}   ${billing_address}
    ${card number last 8}   generate random string  8   [NUMBERS]
    ${card number}     catenate     ${test_pattern}     ${card number last 8}
    ${dic}         [200] API verify (pre-link) card information
    ...     ${test_agent_access_token}
    ...     ${test_card_account_name}
    ...     ${card number}
    ...     ${test_citizen_id}
    ...     ${test_cvv_cvc}
    ...     ${test_pin}
    ...     12/08
    ...     11/08
    ...     ${test_mobile_number}
    ...     ${test_billing_address}
    ...     ATM
    ...     VND
    ${security_ref}     get from dictionary      ${dic}     security_ref
    set test variable    ${test_security_ref}     ${security_ref}
    ${dic}      [200] API user link card source of fund
    ...     ${test_agent_access_token}
    ...     ${test_card_account_name}
    ...     ${card number}
    ...     ${test_citizen_id}
    ...     ${test_cvv_cvc}
    ...     ${test_pin}
    ...     12/08
    ...     11/08
    ...     ${test_mobile_number}
    ...     ${test_billing_address}
    ...     VND
    ...     123456
    ...     ${test_security_ref}
    ${token}        get from dictionary      ${dic}     token
    set test variable    ${test_agent_card_token}     ${token}
    ${shop_name}         generate random string  20  [LETTERS]
    ${address}=     generate random string    70      [LETTERS]
    set test variable   ${test_agent_shop_name}      ${shop_name}
    [200] API create shop
    ...     ${suite_admin_access_token}
    ...     ${test_agent_id}
    ...     ${param_not_used}
    ...     ${shop_name}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${address}

[Test][200] As agent, Create Order
    [Arguments]
    ...     ${payee_agent_id}
    ...     ${agent_access_token}
    ...     ${agent_card_token}
    ${test_ext_transaction_id}           generate random string  20   [NUMBERS]
    set test variable       ${test_ext_transaction_id}      ${test_ext_transaction_id}
    ${dic}  [200] API payment create artifact order
    ...     ${agent_access_token}
    ...     ${test_ext_transaction_id}
    ...     ${test_service_id}
    ...     ${test_service_name}
    ...     Tên sản phầm
    ...     Sản phẩm liên quan
    ...     Reference 2
    ...     Reference 3
    ...     Reference 4
    ...     Reference 5
    ...     card sof token
    ...     ${agent_card_token}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${payee_agent_id}
    ...     agent
    ...     500000
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}
    ...     @{EMPTY}

    ${order_id}=  get from dictionary   ${dic}   order_id
    set test variable       ${test_order_id}     ${order_id}

[Test][200] As agent, Create Order with requestor
    [Arguments]
    ...     ${payee_agent_id}
    ...     ${agent_access_token}
    ...     ${agent_card_token}
    ${test_ext_transaction_id}           generate random string  20   [NUMBERS]
    set test variable       ${test_ext_transaction_id}      ${test_ext_transaction_id}

    ${arg_dic}    create dictionary
    ...     access_token=${agent_access_token}
    ...     ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${test_service_id}
    ...     product_service_name=${test_service_name}
    ...     product_name=Tên sản phầm
    ...     product_ref_1=Sản phẩm liên quan
    ...     product_ref_2=Reference 2
    ...     product_ref_3=Reference 3
    ...     product_ref_4=Reference 4
    ...     product_ref_5=Reference 5
    ...     payer_user_ref_type=card sof token
    ...     payer_user_ref_value=${agent_card_token}
    ...     payee_user_ref_type=${param_not_used}
    ...     payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${payee_agent_id}
    ...     payee_user_type=agent
    ...     requestor_user_user_id=${test_requestor_id}
    ...     requestor_user_user_type=agent
    ...     requestor_user_sof_id=${test_requestor_sof_id}
    ...     requestor_user_sof_type_id=2
    ...     amount=500000
    ...     setup_admin_client_id=${setup_admin_client_id}
    ...     setup_admin_client_secret=${setup_admin_client_secret}

    ${dic}  [200] API create artifact order    ${arg_dic}
    ${order_id}   Get from dictionary   ${dic}  order_id
    set test variable   ${test_order_id}    ${order_id}

[Agent][Test][200] - Create agent with required fields and identity
    ${text}=   generate random string    10      [NUMBERS]
    ${first_name}=   generate random string    10      [LETTERS]
    ${middle_name}=   generate random string    10      [LETTERS]
    ${last_name}=   generate random string    10      [LETTERS]
    ${primary_mobile_number}=   generate random string    11      [NUMBERS]
    ${email}=    set variable      ${first_name}@email.com
    set test variable   ${test_agent_type_id}      ${setup_company_agent_type_id}
    set test variable   ${test_agent_unique_reference}      ${text}
    set test variable   ${test_agent_username}      ${text}
    set test variable   ${test_agent_password}      ${setup_agent_password_encrypted}
    set test variable   ${test_agent_password_login}      ${setup_agent_password_encrypted_utf8}
    set test variable   ${test_first_name}      ${first_name}
    set test variable   ${test_middle_name}      ${middle_name}
    set test variable   ${test_last_name}      ${last_name}

    ${arg_dic}     create dictionary
    ...         access_token=${suite_admin_access_token}
    ...         agent_type_id=${test_agent_type_id}
    ...         unique_reference=${test_agent_unique_reference}
    ...         username=${test_agent_username}
    ...         password=${test_agent_password}
    ...         identity_type_id=1
    ...         first_name=${first_name}
    ...         middle_name=${middle_name}
    ...         last_name=${last_name}
    ...         primary_mobile_number=${primary_mobile_number}
    ...         email=${email}

    ${dic}      [200] API create agent         ${arg_dic}

    ${id}   get from dictionary       ${dic}    id
    set test variable        ${test_agent_id}     ${id}

    ${shop_name}         generate random string  20  [LETTERS]
    ${address}=     generate random string    70      [LETTERS]
    set test variable   ${test_agent_shop_name}      ${shop_name}
    [200] API create shop
    ...     ${suite_admin_access_token}
    ...     ${test_agent_id}
    ...     ${param_not_used}
    ...     ${shop_name}


#    ...     ${param_not_used}
#    ...     ${address}

[Agent][Test][200] - Create agent with required fields and PIN identity
    ${text}=   generate random string    12      [NUMBERS]
    ${pin_identity}=     catenate   SEPARATOR=    pin_    ${text}
    ${first_name}=   generate random string    10      [LETTERS]
    ${middle_name}=   generate random string    10      [LETTERS]
    ${last_name}=   generate random string    10      [LETTERS]
    set test variable   ${test_agent_type_id}      ${setup_company_agent_type_id}
    set test variable   ${test_agent_username}      ${text}
    set test variable   ${test_agent_unique_reference}      ${text}
    set test variable   ${test_agent_pin_username}      ${pin_identity}
    set test variable   ${test_agent_pin_password}      ${setup_agent_pin_encrypted}
    set test variable   ${test_agent_password_login}      ${setup_agent_pin_encrypted_utf8}
    set test variable   ${test_first_name}      ${first_name}
    set test variable   ${test_middle_name}      ${middle_name}
    set test variable   ${test_last_name}      ${last_name}
    ${arg_dic}      create dictionary
    ...         access_token=${suite_admin_access_token}
    ...         agent_type_id=${test_agent_type_id}
    ...         unique_reference=${test_agent_unique_reference}
    ...         username=${test_agent_pin_username}
    ...         password=${test_agent_pin_password}
    ...         identity_type_id=7
    ...         first_name=${first_name}
    ...         middle_name=${middle_name}
    ...         last_name=${last_name}

    ${dic}      [200] API create agent      ${arg_dic}
    ${id}   get from dictionary       ${dic}    id
    set test variable        ${test_agent_id}     ${id}

    ${shop_name}         generate random string  20  [LETTERS]
    ${address}=     generate random string    70      [LETTERS]
    set test variable   ${test_agent_shop_name}      ${shop_name}
    [200] API create shop
    ...     ${suite_admin_access_token}
    ...     ${test_agent_id}
    ...     ${param_not_used}
    ...     ${shop_name}


#    ...     ${param_not_used}
#    ...     ${address}

[Test][200] create agent and identity with agent type
    ${test_agent_type_name}=   generate random string    10      [LETTERS]
    ${dic}      [200] API create agent type     ${suite_admin_access_token}     ${test_agent_type_name}
    ${agent_type_id}     get from dictionary      ${dic}     id
    set test variable    ${test_agent_type_id}       ${agent_type_id}
    set test variable    ${test_agent_type_name}    ${test_agent_type_name}

    ${text}=   generate random string    12      [NUMBERS]
    ${pin_identity}=     catenate   SEPARATOR=    pin_    ${text}
    ${first_name}=   generate random string    10      [LETTERS]
    ${middle_name}=   generate random string    10      [LETTERS]
    ${last_name}=   generate random string    10      [LETTERS]
    set test variable   ${test_agent_username}      ${text}
    set test variable   ${test_agent_unique_reference}      ${text}
    set test variable   ${test_agent_pin_username}      ${pin_identity}
    set test variable   ${test_agent_pin_password}      ${setup_agent_pin_encrypted}
    set test variable   ${test_agent_password_login}      ${setup_agent_pin_encrypted_utf8}
    set test variable   ${test_first_name}      ${first_name}
    set test variable   ${test_middle_name}      ${middle_name}
    set test variable   ${test_last_name}      ${last_name}
    ${arg_dic}      create dictionary
    ...         access_token=${suite_admin_access_token}
    ...         agent_type_id=${test_agent_type_id}
    ...         unique_reference=${test_agent_unique_reference}
    ...         username=${test_agent_pin_username}
    ...         password=${test_agent_pin_password}
    ...         identity_type_id=7
    ...         first_name=${first_name}
    ...         middle_name=${middle_name}
    ...         last_name=${last_name}

    ${dic}      [200] API create agent      ${arg_dic}
    ${id}   get from dictionary       ${dic}    id
    set test variable        ${test_agent_id}     ${id}

[Test][200] create sale and identity with agent type
    ${test_agent_type_name}=   generate random string    10      [LETTERS]
    ${dic}      [200] API create agent type     ${suite_admin_access_token}     ${test_agent_type_name}
    ${agent_type_id}     get from dictionary      ${dic}     id
    set test variable    ${test_agent_type_id}       ${agent_type_id}
    set test variable    ${test_agent_type_name}    ${test_agent_type_name}

    ${text}=   generate random string    12      [NUMBERS]
    ${pin_identity}=     catenate   SEPARATOR=    pin_    ${text}
    ${first_name}=   generate random string    10      [LETTERS]
    ${middle_name}=   generate random string    10      [LETTERS]
    ${last_name}=   generate random string    10      [LETTERS]
    set test variable   ${test_agent_username}      ${text}
    set test variable   ${test_agent_unique_reference}      ${text}
    set test variable   ${test_agent_pin_username}      ${pin_identity}
    set test variable   ${test_agent_pin_password}      ${setup_agent_pin_encrypted}
    set test variable   ${test_agent_password_login}      ${setup_agent_pin_encrypted_utf8}
    set test variable   ${test_first_name}      ${first_name}
    set test variable   ${test_middle_name}      ${middle_name}
    set test variable   ${test_last_name}      ${last_name}
    ${arg_dic}      create dictionary
    ...         access_token=${suite_admin_access_token}
    ...         agent_type_id=${test_agent_type_id}
    ...         unique_reference=${test_agent_unique_reference}
    ...         username=${test_agent_pin_username}
    ...         password=${test_agent_pin_password}
    ...         identity_type_id=7
    ...         first_name=${first_name}
    ...         middle_name=${middle_name}
    ...         last_name=${last_name}
    ...         is_sale=true

    ${dic}      [200] API create agent      ${arg_dic}
    ${id}   get from dictionary       ${dic}    id
    set test variable        ${test_agent_id}     ${id}

[Agent][Test][200] - Create agent with required fields both PIN identity and PASSWORD identity
    ${text}=   generate random string    12      [NUMBERS]
    ${pin_identity}=     catenate   SEPARATOR=    pin_    ${text}
    ${first_name}=   generate random string    10      [LETTERS]
    ${middle_name}=   generate random string    10      [LETTERS]
    ${last_name}=   generate random string    10      [LETTERS]
    ${national_id}=  generate random string    10      [NUMBERS]
    set test variable   ${test_agent_type_id}      ${setup_company_agent_type_id}
    set test variable   ${test_agent_unique_reference}      ${text}
    set test variable   ${test_agent_username}      ${text}
    set test variable   ${test_agent_password}      ${setup_agent_password_encrypted}
    set test variable   ${test_agent_password_login}      ${setup_agent_password_encrypted_utf8}
    set test variable   ${test_first_name}      ${first_name}
    set test variable   ${test_middle_name}      ${middle_name}
    set test variable   ${test_last_name}      ${last_name}
    set test variable   ${test_national_id}      ${national_id}
    set test variable   ${test_agent_pin_username}      ${pin_identity}
    set test variable   ${test_agent_pin_password}      ${setup_agent_pin_encrypted}
    set test variable   ${test_agent_password_login}      ${setup_agent_pin_encrypted_utf8}
    ${arg_dic}      create dictionary
    ...         access_token=${suite_admin_access_token}
    ...         agent_type_id=${test_agent_type_id}
    ...         unique_reference=${test_agent_unique_reference}
    ...         identity_type_id=1
    ...         username=${test_agent_username}
    ...         password=${test_agent_password}
    ...         first_name=${first_name}
    ...         middle_name=${middle_name}
    ...         last_name=${last_name}
    ...         national_id_number=${national_id}
    ${dic}      [200] API create agent      ${arg_dic}

    ${id}   get from dictionary       ${dic}    id
    set test variable        ${test_agent_id}     ${id}
    set test variable        ${suite_agent_id}     ${id}

    # add identity with identity_type = 7 ( PIN) for this user
    [200] API add agent identity
    ...     ${test_agent_id}
    ...     7
    ...     ${test_agent_pin_username}
    ...     ${test_agent_pin_password}
    ...     ${suite_admin_access_token}

    #add shop
    ${shop_name}         generate random string  20  [LETTERS]
    ${address}=     generate random string    70      [LETTERS]
    set test variable   ${test_agent_shop_name}      ${shop_name}
    [200] API create shop
    ...     ${suite_admin_access_token}
    ...     ${test_agent_id}
    ...     ${param_not_used}
    ...     ${shop_name}


#    ...     ${param_not_used}
#    ...     ${address}


[Test][200] create agent with required fields and NO identity
    ${gen_agent_type}=   generate random string    10      [LETTERS]
    ${gen_description}=   generate random string    20      [LETTERS]
    ${gen_username}=   generate random string    10      [LETTERS]
    ${set_password}=   set variable    123456
    ${get_created_date}=    Get Current Date    result_format=${setup_datetime_format}
    # for phantomjs
    ${get_created_date_for_search}=    Get Current Date    result_format=%Y-%m-%d
     # for Chrome only
#    ${get_created_date_for_search}=    Get Current Date    result_format=%m-%d-%Y
    set test variable  ${test_agent_type}   ${gen_agent_type}
    set test variable  ${test_description}   ${gen_description}
    set test variable	${test_username}	${gen_username}
    set test variable  ${test_password}     ${setup_agent_password_encrypted}
    set test variable  ${test_password_utf8}     ${setup_password_customer_encrypted_utf8}
    set test variable  ${test_date}     ${get_created_date}
    set test variable  ${test_date_for_search}   ${get_created_date_for_search}

    ${response}                         call system user login api
    ${get_admin_access_token}=          set variable                        ${response.json()['access_token']}
    set test variable      ${test_admin_access_token}       ${get_admin_access_token}

    ${dic}  [200] API create agent type     ${suite_admin_access_token}     ${test_agent_type}  ${test_description}
    ${id}   get from dictionary     ${dic}  id
    set test variable     ${test_agent_type_id}     ${id}

    ${correlation_id}       generate random string  20  [UPPER]
    ${firstname}=   generate random string    10      [LETTERS]
    ${gen_firstname}=   set variable  Hỏi_${firstname}
    ${lastname}=   generate random string    10      [LETTERS]
    ${gen_lastname}=    set variable  Ngã_${lastname}
    ${gen_primary_mobile_number}=   generate random string    11      [NUMBERS]
    ${gen_card_id}=   generate random string    11      [NUMBERS]
    ${gen_national_id}=   generate random string    11      [NUMBERS]
    ${gen_email}=    set variable      ${gen_firstname}@email.com
    ${address}=   generate random string    10      [LETTERS]
    ${gen_address}=     set variable  Nặng_${address}
    ${unique_reference}=   generate random string    10      [LETTERS]
    ${gen_unique_reference}=    set variable  ประเทศไทย_${unique_reference}
    ${gen_kyc_status}=  set variable     false
    set test variable   ${test_firstname}     ${gen_firstname}
    set test variable  ${test_lastname}       ${gen_lastname}
    set test variable  ${test_primary_mobile_number}      ${gen_primary_mobile_number}
    set test variable  ${test_card_id}        ${gen_card_id}
    set test variable  ${test_national_id}        ${gen_national_id}
    set test variable  ${test_email}          ${gen_email}
    set test variable  ${test_address}        ${gen_address}
    set test variable  ${test_unique_reference}           ${test_primary_mobile_number}
    set test variable  ${test_kyc_status}         ${gen_kyc_status}

    ${dic}      [200] API create agent profiles
    ...     ${suite_admin_access_token}
    ...     ${test_agent_type_id}
    ...     ${test_unique_reference}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${test_firstname}
    ...     ${param_not_used}
    ...     ${test_lastname}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${test_national_id} # national_id
    ...     ${param_not_used}
    ...     ${test_email}
    ...     ${test_primary_mobile_number}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${test_address}

    ${id}   get from dictionary       ${dic}    id
    set test variable     ${test_agent_id}     ${id}
    #add shop
    ${shop_name}         generate random string  20  [LETTERS]
    ${address}=     generate random string    70      [LETTERS]
    set test variable   ${test_agent_shop_name}      ${shop_name}
    [200] API create shop
    ...     ${suite_admin_access_token}
    ...     ${test_agent_id}
    ...     ${param_not_used}
    ...     ${shop_name}


#    ...     ${param_not_used}
#    ...     ${address}


[Test][200] create agent with required fields both PIN identity and PASSWORD identity and extra identity
    ${text}=   generate random string    12      [NUMBERS]
    ${extra_text}=   generate random string    12      [NUMBERS]

    ${pin_identity}=     catenate   SEPARATOR=    pin_    ${text}
    ${first_name}=   generate random string    10      [LETTERS]
    ${middle_name}=   generate random string    10      [LETTERS]
    ${last_name}=   generate random string    10      [LETTERS]
    ${national_id}=  generate random string    10      [NUMBERS]
    set test variable   ${test_agent_type_id}      ${setup_company_agent_type_id}
    set test variable   ${test_agent_unique_reference}      ${text}
    set test variable   ${test_agent_username}      ${text}
    #test_agent_extra_username
    set test variable   ${test_agent_extra_username}      ${extra_text}
    set test variable   ${test_agent_password}      ${setup_agent_password_encrypted}
    set test variable   ${test_agent_password_login}      ${setup_agent_password_encrypted_utf8}
    set test variable   ${test_first_name}      ${first_name}
    set test variable   ${test_middle_name}      ${middle_name}
    set test variable   ${test_last_name}      ${last_name}
    set test variable   ${test_national_id}      ${national_id}
    set test variable   ${test_agent_pin_username}      ${pin_identity}
    set test variable   ${test_agent_pin_password}      ${setup_agent_pin_encrypted}
    set test variable   ${test_agent_password_login}      ${setup_agent_pin_encrypted_utf8}
    ${arg_dic}      create dictionary
    ...         access_token=${suite_admin_access_token}
    ...         agent_type_id=${test_agent_type_id}
    ...         unique_reference=${test_agent_unique_reference}
    ...         identity_type_id=1
    ...         username=${test_agent_username}
    ...         password=${test_agent_password}
    ...         first_name=${first_name}
    ...         middle_name=${middle_name}
    ...         last_name=${last_name}
    ...         national_id_number=${national_id}
    ${dic}      [200] API create agent      ${arg_dic}
    ${id}   get from dictionary       ${dic}    id
    set test variable        ${test_agent_id}     ${id}
    set test variable        ${suite_agent_id}     ${id}

    # add identity with identity_type = 7 ( PIN) for this user
    [200] API add agent identity
    ...     ${test_agent_id}
    ...     7
    ...     ${test_agent_pin_username}
    ...     ${test_agent_pin_password}
    ...     ${suite_admin_access_token}

    # add identity with identity_type = 2 ( PIN) for this user
    [200] API add agent identity
    ...     ${test_agent_id}
    ...     2
    ...     ${test_agent_extra_username}
    ...     ${test_agent_password}
    ...     ${suite_admin_access_token}

    #add shop
    ${shop_name}         generate random string  20  [LETTERS]
    ${address}=     generate random string    70      [LETTERS]
    set test variable   ${test_agent_shop_name}      ${shop_name}
    [200] API create shop
    ...     ${suite_admin_access_token}
    ...     ${test_agent_id}
    ...     ${param_not_used}
    ...     ${shop_name}
#    ...     ${param_not_used}
#    ...     ${address}


[Test][200] API create agent shop
    [Arguments]
    ...     ${agent_id}
    ${shop_name}=  generate random string    10      [LETTERS]
    ${address}=     generate random string    70      [LETTERS]
    set test variable   ${test_agent_shop_name}      ${shop_name}
    ${dic}      [200] API create shop
    ...     ${test_admin_access_token}
    ...     ${agent_id}
    ...     ${param_not_used}
    ...     ${shop_name}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${address}
    ${id}   get from dictionary       ${dic}    id
    set test variable        ${test_shop_id}     ${id}
    set test variable        ${test_shop_address}     ${address}

[Suite][200] Prepare an agent with sof_cash
    ${agent_type_name}   generate random string  2    [LETTERS]
    ${dic}      [200] API create agent type     ${suite_admin_access_token}     ${agent_type_name}
    ${agent_type_id}     get from dictionary      ${dic}     id
    ${unique_reference}   generate random string  10    [NUMBERS]
    ${username}   generate random string  10    [LETTERS]
    set suite variable    ${suite_username}       ${username}

     ${arg_dic}      create dictionary
    ...         access_token=${suite_admin_access_token}
    ...         agent_type_id=${agent_type_id}
    ...         unique_reference=${unique_reference}
    ...         identity_type_id=1
    ...         username=${suite_username}
    ...         password=${setup_agent_password_encrypted}

    ${dic}      [200] API create agent      ${arg_dic}

    ${agent_id}     get from dictionary      ${dic}     id
    set suite variable    ${suite_agent_id}       ${agent_id}

    ${arg_dic}   create dictionary      username=${suite_username}      password=${setup_agent_password_encrypted_encoded}
    ${dic}    [200] API agent authentication        ${arg_dic}
    ${access_token}     get from dictionary      ${dic}     access_token
    set suite variable    ${suite_agent_access_token}       ${access_token}

    ${dic}  [200] API create user sof cash          VND       ${suite_agent_access_token}
    ${sof_id}       get from dictionary     ${dic}      sof_id
    set suite variable    ${suite_agent_sof_id_VN}      ${sof_id}

    [200] Company Agent fund in "10000000" money for "agent": "${suite_agent_id}" with "${suite_fund_in_service_id}" and "${suite_fund_in_service_name}"


[Suite][200] Prepare an agent with specific agent_type and agent_classification
    ${agent_type_name}   generate random string  10    [LETTERS]
    set suite variable    ${suite_agent_type_name}       service_limit_${agent_type_name}
    ${dic}      [200] API create agent type     ${suite_admin_access_token}     ${suite_agent_type_name}
    ${agent_type_id}     get from dictionary      ${dic}     id
    set suite variable    ${suite_agent_type_id}       ${agent_type_id}

    ${agent_classification_name}   generate random string  10    [LETTERS]
    set suite variable    ${suite_agent_classification_name}       service_limit_${agent_classification_name}
    ${arg_dic}      create dictionary       access_token=${suite_admin_access_token}    name=${suite_agent_classification_name}
    ${dic}          [200] API create agent classification       ${arg_dic}
    ${agent_classification_id}     get from dictionary      ${dic}     id
    set suite variable    ${suite_agent_classification_id}       ${agent_classification_id}

    ${unique_reference}   generate random string  10    [NUMBERS]
    ${username}   generate random string  10    [LETTERS]
    set suite variable    ${suite_username}       ${username}
     ${arg_dic}      create dictionary
    ...         access_token=${suite_admin_access_token}
    ...         agent_type_id=${suite_agent_type_id}
    ...         agent_classification_id=${suite_agent_classification_id}
    ...         unique_reference=${unique_reference}
    ...         identity_type_id=1
    ...         username=${suite_username}
    ...         password=${setup_agent_password_encrypted}

    ${dic}      [200] API create agent      ${arg_dic}

    ${agent_id}     get from dictionary      ${dic}     id
    set suite variable    ${suite_agent_id}       ${agent_id}
    ${arg_dic}   create dictionary      username=${suite_username}      password=${setup_agent_password_encrypted_encoded}
    ${dic}    [200] API agent authentication        ${arg_dic}
    ${access_token}     get from dictionary      ${dic}     access_token
    set suite variable    ${suite_agent_access_token}       ${access_token}

    ${dic}  [200] API create user sof cash          VND       ${suite_agent_access_token}
    ${sof_id}       get from dictionary     ${dic}      sof_id
    set suite variable    ${suite_agent_sof_id_VN}      ${sof_id}

    [Suite][200] Company Agent fund in "10000000" money for "agent": "${suite_agent_id}"

[Suite][200] Prepare an agent with sof_cash without prepare agent fund in
    ${agent_type_name}   generate random string  5    [LETTERS]
    ${dic}      [200] API create agent type     ${suite_admin_access_token}     ${agent_type_name}
    ${agent_type_id}     get from dictionary      ${dic}     id
    ${unique_reference}   generate random string  10    [NUMBERS]
    ${username}   generate random string  10    [LETTERS]
    set suite variable    ${suite_username}       ${username}

     ${arg_dic}      create dictionary
    ...         access_token=${suite_admin_access_token}
    ...         agent_type_id=${agent_type_id}
    ...         unique_reference=${unique_reference}
    ...         identity_type_id=1
    ...         username=${suite_username}
    ...         password=${setup_agent_password_encrypted}
    set suite variable    ${suite_agent_type_id}       ${agent_type_id}
    set suite variable    ${suite_agent_type_name}       ${agent_type_name}

    ${dic}      [200] API create agent      ${arg_dic}

    ${agent_id}     get from dictionary      ${dic}     id
    set suite variable    ${suite_agent_id}       ${agent_id}
    ${arg_dic}   create dictionary      username=${suite_username}      password=${setup_agent_password_encrypted_encoded}
    ${dic}    [200] API agent authentication        ${arg_dic}
    ${access_token}     get from dictionary      ${dic}     access_token
    set suite variable    ${suite_agent_access_token}       ${access_token}

    ${dic}  [200] API create user sof cash          VND       ${suite_agent_access_token}
    ${sof_id}       get from dictionary     ${dic}      sof_id
    set suite variable    ${suite_agent_sof_id_VN}      ${sof_id}

[Suite] Save setup agent detail
    set suite variable    ${suite_setup_agent_id}      ${suite_agent_id}
    set suite variable    ${suite_setup_agent_access_token}      ${suite_agent_access_token}
    set suite variable    ${suite_setup_agent_sof_id_VN}      ${suite_agent_sof_id_VN}

[Suite][200] Prepare an agent with sof_cash for currency "${currency}"
    ${agent_type_name}   generate random string  10    [LETTERS]
    ${dic}      [200] API create agent type     ${suite_admin_access_token}     ${agent_type_name}
#    ${agent_type_id}     get from dictionary      ${dic}     id
#    ${unique_reference}   generate random string  10    [NUMBERS]
#    ${username}   generate random string  10    [LETTERS]
#    set suite variable    ${suite_username}       ${username}
#
#     ${arg_dic}      create dictionary
#    ...         access_token=${suite_admin_access_token}
#    ...         agent_type_id=${agent_type_id}
#    ...         unique_reference=${unique_reference}
#    ...         identity_type_id=1
#    ...         username=${suite_username}
#    ...         password=${setup_agent_password_encrypted}
#
#    ${dic}      [200] API create agent      ${arg_dic}
#
#    ${agent_id}     get from dictionary      ${dic}     id
#    set suite variable    ${suite_agent_id}       ${agent_id}
#    ${arg_dic}   create dictionary      username=${suite_username}      password=${setup_agent_password_encrypted_encoded}
#    ${dic}    [200] API agent authentication        ${arg_dic}
#    ${access_token}     get from dictionary      ${dic}     access_token
#    set suite variable    ${suite_agent_access_token}       ${access_token}
#
#    ${dic}  [200] API create user sof cash          ${currency}       ${suite_agent_access_token}
#    ${sof_id}       get from dictionary     ${dic}      sof_id
#    set suite variable    ${suite_agent_sof_id}      ${sof_id}
#    set suite variable    ${suite_agent_sof_type_id}      2

[Test][200] Create a sof_cash and fund in
    ${agent_type_name}   generate random string  10    [LETTERS]
    set test variable       ${test_agent_type_name}     ${agent_type_name}
    ${dic}      [200] API create agent type     ${suite_admin_access_token}     ${test_agent_type_name}
    ${agent_type_id}     get from dictionary      ${dic}     id
    set test variable    ${test_agent_type_id}       ${agent_type_id}
    ${unique_reference}   generate random string  10    [NUMBERS]
    set test variable    ${test_unique_reference}       ${unique_reference}
    ${username}   generate random string  10    [NUMBERS]
    set test variable    ${test_username}       ${username}
    ${arg_dic}     create dictionary
    ...         access_token=${suite_admin_access_token}
    ...         agent_type_id=${test_agent_type_id}
    ...         unique_reference=${test_unique_reference}
    ...         username=${test_username}
    ...         password=${setup_agent_password_encrypted}
    ...         identity_type_id=1

    ${dic}      [200] API create agent      ${arg_dic}
    ${agent_id}     get from dictionary      ${dic}     id
    set test variable    ${test_agent_id}       ${agent_id}
    set test variable    ${test_agent_username}    ${test_username}
    ${arg_dic}   create dictionary      username=${test_username}      password=${setup_agent_password_encrypted_encoded}
    ${dic}    [200] API agent authentication        ${arg_dic}
    ${access_token}     get from dictionary      ${dic}     access_token
    set test variable    ${test_agent_access_token}       ${access_token}

    ${dic}  [200] API create user sof cash          VND       ${test_agent_access_token}
    ${sof_id}       get from dictionary     ${dic}      sof_id
    set suite variable    ${test_agent_sof_id_VN}      ${sof_id}
    set suite variable    ${suite_agent_sof_id_VN}      ${sof_id}
#    [200] API system user create a company agent cash source of fund with currency      VND     ${suite_admin_access_token}
    [Test][200] Company Agent fund in "10000000" money for "agent": "${test_agent_id}"

[Test][200] Create a sof_cash and fund in for sale
    ${agent_type_name}   generate random string  10    [LETTERS]
    set test variable       ${test_agent_type_name}     ${agent_type_name}
    ${dic}      [200] API create agent type     ${suite_admin_access_token}     ${test_agent_type_name}
    ${agent_type_id}     get from dictionary      ${dic}     id
    set test variable    ${test_agent_type_id}       ${agent_type_id}
    ${unique_reference}   generate random string  10    [NUMBERS]
    set test variable    ${test_unique_reference}       ${unique_reference}
    ${username}   generate random string  10    [NUMBERS]
    set test variable    ${test_username}       ${username}
    ${arg_dic}     create dictionary
    ...         access_token=${suite_admin_access_token}
    ...         agent_type_id=${test_agent_type_id}
    ...         unique_reference=${test_unique_reference}
    ...         username=${test_username}
    ...         password=${setup_agent_password_encrypted}
    ...         identity_type_id=1
    ...         is_sale=true

    ${dic}      [200] API create agent      ${arg_dic}
    ${agent_id}     get from dictionary      ${dic}     id
    set test variable    ${test_agent_id}       ${agent_id}
    set test variable    ${test_agent_username}    ${test_username}
    ${arg_dic}   create dictionary      username=${test_username}      password=${setup_agent_password_encrypted_encoded}
    ${dic}    [200] API agent authentication        ${arg_dic}
    ${access_token}     get from dictionary      ${dic}     access_token
    set test variable    ${test_agent_access_token}       ${access_token}

    ${dic}  [200] API create user sof cash          VND       ${test_agent_access_token}
    ${sof_id}       get from dictionary     ${dic}      sof_id
    set suite variable    ${test_agent_sof_id_VN}      ${sof_id}
    set suite variable    ${suite_agent_sof_id_VN}      ${sof_id}
#    [200] API system user create a company agent cash source of fund with currency      VND     ${suite_admin_access_token}
    [Test][200] Company Agent fund in "10000000" money for "agent": "${test_agent_id}"

[Suite][200] Create a sof_cash and fund in
    ${agent_type_name}   generate random string  10    [LETTERS]
    set suite variable       ${suite_agent_type_name}     ${agent_type_name}
    ${dic}      [200] API create agent type     ${suite_admin_access_token}     ${suite_agent_type_name}
    ${agent_type_id}     get from dictionary      ${dic}     id
    set suite variable    ${suite_agent_type_id}       ${agent_type_id}
    ${unique_reference}   generate random string  10    [NUMBERS]
    set suite variable    ${suite_unique_reference}       ${unique_reference}
    ${username}   generate random string  10    [NUMBERS]
    set suite variable    ${suite_username}       ${username}
    ${arg_dic}     create dictionary
    ...         access_token=${suite_admin_access_token}
    ...         agent_type_id=${suite_agent_type_id}
    ...         unique_reference=${suite_unique_reference}
    ...         username=${suite_username}
    ...         password=${setup_agent_password_encrypted}
    ...         identity_type_id=1

    ${dic}      [200] API create agent      ${arg_dic}
    ${agent_id}     get from dictionary      ${dic}     id
    set suite variable    ${suite_agent_id}       ${agent_id}
    set suite variable    ${suite_agent_username}    ${suite_username}
    ${arg_dic}   create dictionary      username=${suite_username}      password=${setup_agent_password_encrypted_encoded}
    ${dic}    [200] API agent authentication        ${arg_dic}
    ${access_token}     get from dictionary      ${dic}     access_token
    set suite variable    ${suite_agent_access_token}       ${access_token}

    ${dic}  [200] API create user sof cash          VND       ${suite_agent_access_token}
    ${sof_id}       get from dictionary     ${dic}      sof_id
    set suite variable    ${suite_agent_sof_id_VN}      ${sof_id}
    set suite variable    ${suite_agent_sof_id_VN}      ${sof_id}
    [Suite][200] Company Agent fund in "10000000" money for "agent": "${suite_agent_id}"

[Test][200] Create an agent with sof_cash without fund in
    ${agent_type_name}   generate random string  10    [LETTERS]
    set test variable       ${test_agent_type_name}     ${agent_type_name}
    ${dic}      [200] API create agent type     ${suite_admin_access_token}     ${test_agent_type_name}
    ${agent_type_id}     get from dictionary      ${dic}     id
    set test variable    ${test_agent_type_id}       ${agent_type_id}
    ${unique_reference}   generate random string  10    [NUMBERS]
    set test variable    ${test_unique_reference}       ${unique_reference}
    ${username}   generate random string  10    [NUMBERS]
    set test variable    ${test_username}       ${username}
    ${arg_dic}     create dictionary
    ...         access_token=${suite_admin_access_token}
    ...         agent_type_id=${test_agent_type_id}
    ...         unique_reference=${test_unique_reference}
    ...         username=${test_username}
    ...         password=${setup_agent_password_encrypted}
    ...         identity_type_id=1

    ${dic}      [200] API create agent      ${arg_dic}
    ${agent_id}     get from dictionary      ${dic}     id
    set test variable    ${test_agent_id}       ${agent_id}
    set test variable    ${test_agent_username}    ${test_username}
    ${arg_dic}   create dictionary      username=${test_username}      password=${setup_agent_password_encrypted_encoded}
    ${dic}    [200] API agent authentication        ${arg_dic}
    ${access_token}     get from dictionary      ${dic}     access_token
    set test variable    ${test_agent_access_token}       ${access_token}

    ${dic}  [200] API create user sof cash          VND       ${test_agent_access_token}
    ${sof_id}       get from dictionary     ${dic}      sof_id
    set test variable    ${test_agent_sof_id_VN}      ${sof_id}
    
[Suite][200] Create a agent link with company and fund in sof cash
    ${agent_type_name}   generate random string  10    [LETTERS]
    set suite variable       ${suite_agent_type_name}     ${agent_type_name}
    ${dic}      [200] API create agent type     ${suite_admin_access_token}     ${suite_agent_type_name}
    ${agent_type_id}     get from dictionary      ${dic}     id
    set suite variable    ${suite_agent_type_id}       ${agent_type_id}
    ${unique_reference}   generate random string  10    [NUMBERS]
    set suite variable    ${suite_unique_reference}       ${unique_reference}
    ${username}   generate random string  10    [NUMBERS]
    set suite variable    ${suite_username}       ${username}
    ${arg_dic}     create dictionary
    ...         access_token=${suite_admin_access_token}
    ...         agent_type_id=${suite_agent_type_id}
    ...         unique_reference=${suite_unique_reference}
    ...         username=${suite_username}
    ...         password=${setup_agent_password_encrypted}
    ...         identity_type_id=1
    ...         company_id=${suite_company_id}

    ${dic}      [200] API create agent      ${arg_dic}
    ${agent_id}     get from dictionary      ${dic}     id
    set suite variable    ${suite_agent_id}       ${agent_id}
    set suite variable    ${suite_agent_username}    ${suite_username}
    ${arg_dic}   create dictionary      username=${suite_username}      password=${setup_agent_password_encrypted_encoded}
    ${dic}    [200] API agent authentication        ${arg_dic}
    ${access_token}     get from dictionary      ${dic}     access_token
    set suite variable    ${suite_agent_access_token}       ${access_token}

    ${dic}  [200] API create user sof cash          VND       ${suite_agent_access_token}
    ${sof_id}       get from dictionary     ${dic}      sof_id
    set suite variable    ${suite_agent_sof_id_VN}      ${sof_id}
    set suite variable    ${suite_agent_sof_id_VN}      ${sof_id}
    [Suite][200] Company Agent fund in "10000000" money for "agent": "${suite_agent_id}"

[Test][200] Authenticate agent
    ${arg_dic}      create dictionary       username=${test_agent_username}      password=${test_agent_password}
    ${dic}          [200] API authenticate agent        ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    ${agent_access_token}       set variable    ${response.json()['access_token']}
    set test variable      ${test_agent_access_token}      ${agent_access_token}
    [Return]    ${dic}

[Agent][Test][200] - Create agent shop
    ${unique_reference}     generate random string  12    [NUMBERS]

    ${arg_dic}      create dictionary
    ...         access_token=${test_admin_access_token}
    ...         agent_type_id=1
    ...         unique_reference=${unique_reference}
    ...         identity_type_id=1
    ...         username=${unique_reference}
    ...         password=${setup_agent_password_encrypted}

    ${dic}      [200] API create agent      ${arg_dic}
#    ...     ${test_admin_access_token}
#    ...     1
#    ...     ${unique_reference}
#    ...     1
#    ...     ${unique_reference}
#    ...     ${setup_agent_password_encrypted}
    ${agent_id}     get from dictionary   ${dic}   id
    set test variable   ${test_agent_id}        ${agent_id}
    set test variable   ${test_agent_username}       ${unique_reference}
    set test variable   ${test_agent_password_login}        ${setup_agent_password_encrypted_utf8}

    [Test][200] API create agent shop
    ...     ${test_agent_id}

[Prepare] - Create a basic agent
    ${first_name}     generate random string  7    [LETTERS]
    ${last_name}     generate random string  5    [LETTERS]
    ${unique_reference}     generate random string  12    [NUMBERS]
    ${national_id_number}=      generate random string    10      [NUMBERS]
    ${arg_dic}      create dictionary
    ...         access_token=${test_admin_access_token}
    ...         agent_type_id=1
    ...         unique_reference=${unique_reference}
    ...         identity_type_id=1
    ...         username=${unique_reference}
    ...         password=${setup_agent_password_encrypted}
    ...         first_name=${first_name}
    ...         last_name=${last_name}
    ...         national_id_number=${national_id_number}

    ${dic}      [200] API create agent      ${arg_dic}
    ${agent_id}     get from dictionary   ${dic}   id
    set test variable   ${test_first_name}        ${first_name}
    set test variable   ${test_last_name}        ${last_name}
    set test variable   ${test_agent_id}        ${agent_id}
    set test variable   ${test_agent_username}       ${unique_reference}
    set test variable   ${test_agent_password_login}        ${setup_agent_password_encrypted_utf8}
    set test variable   ${test_national_id_number}   ${national_id_number}

[Agent][Test][200] - Authenticate agent
    ${arg_dic}      create dictionary       username=${test_agent_username}      password=${test_agent_password_login}
    ${dic}          [200] API authenticate agent        ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    ${agent_access_token}       set variable    ${response.json()['access_token']}
    set test variable      ${test_agent_access_token}      ${agent_access_token}
    [Return]    ${dic}

[Agent][Test][200] - Authenticate agent with arguments
    [Arguments]  &{arg_dic}
    ${dic}          [200] API authenticate agent        ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    ${agent_access_token}       set variable    ${response.json()['access_token']}
    set test variable      ${test_agent_access_token}      ${agent_access_token}
    [Return]    ${dic}

[Prepare] - [200] Authenticate agent with input device_unique_reference and channel_id
    [Arguments]
    ...     ${device_unique_reference}
    ...     ${channel_id}
    ${arg_dic}      create dictionary
            ...     username=${test_agent_username}
            ...     password=${test_agent_password_login}
            ...     device_unique_reference=${device_unique_reference}
            ...     channel_id=${channel_id}
            ...     grant_type=password
            ...     param_client_id=CHANNELADAPTER123456789012345678
            ...     header_client_id=CHANNELADAPTER123456789012345678
            ...     header_client_secret=CHANNEL123456789012345678901234567890123456789012345678901234567
    ${dic}          [200] API authenticate agent        ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    ${agent_access_token}       set variable    ${response.json()['access_token']}
    set test variable      ${test_agent_access_token}      ${agent_access_token}
    [Return]    ${dic}

[Agent][Test][200] - Create an agent with no shop
    ${device_unique_reference}     generate random string  12    [LETTERS]
    set test variable      ${login_device_id}   ${device_unique_reference}
    [System_User][Test][200] - login as setup system user
    [Prepare] - Create a basic agent

[Prepare] - Create an agent with a shop
    ${device_unique_reference}     generate random string  12    [LETTERS]
    [System_User][Test][200] - login as setup system user
    [Prepare] - Create a basic agent
    [Test][200] API create agent shop
    ...     ${test_agent_id}
    [Agent][Test][200] - Authenticate agent

[Agent][Test][200] - Create an agent with multiple shops
    ${device_unique_reference}     generate random string  12    [LETTERS]
    set test variable      ${login_device_id}   ${device_unique_reference}
    [System_User][Test][200] - login as setup system user
    [Prepare] - Create a basic agent
    [Test][200] API create agent shop
    ...     ${test_agent_id}
    set test variable      ${test_shop_id_1}      ${test_shop_id}
    set test variable      ${test_agent_shop_name_1}      ${test_agent_shop_name}
    set test variable      ${test_shop_address_1}      ${test_shop_address}
    [Test][200] API create agent shop
    ...     ${test_agent_id}
    set test variable      ${test_shop_id_2}      ${test_shop_id}
    set test variable      ${test_agent_shop_name_2}      ${test_agent_shop_name}
    set test variable      ${test_shop_address_2}      ${test_shop_address}

[Prepare] - Create an agent with 1 shop
    ${device_unique_reference}     generate random string  12    [LETTERS]
    [System_User][Test][200] - login as setup system user
    [Prepare] - Create a basic agent
    [Test][200] API create agent shop
    ...     ${test_agent_id}
    set test variable      ${test_shop_id_1}      ${test_shop_id}
    set test variable      ${test_agent_shop_name_1}      ${test_agent_shop_name}
    set test variable      ${test_shop_address_1}      ${test_shop_address}

[Prepare] - Create an agent with multiple shops and max devices
    [Agent][Test][200] - Create an agent with multiple shops
    ${device_unique_reference_1}     generate random string  12    [LETTERS]
    ${device_unique_reference_2}     generate random string  12    [LETTERS]
    ${device_unique_reference_3}     generate random string  12    [LETTERS]
    ${device_model_1}     generate random string  12    [LETTERS]
    ${device_model_2}     generate random string  12    [LETTERS]
    ${device_model_3}     generate random string  12    [LETTERS]
    [Agent][Test][200] - Authenticate agent
    [200] agent create device
    ...     ${test_agent_access_token}
    ...     ${device_unique_reference_1}
    ...     ${device_model_1}
    ...     ${test_shop_id}
    ...     1
    ...     2
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    [200] agent create device
    ...     ${test_agent_access_token}
    ...     ${device_unique_reference_2}
    ...     ${device_model_2}
    ...     ${test_shop_id}
    ...     1
    ...     2
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    [200] agent create device
    ...     ${test_agent_access_token}
    ...     ${device_unique_reference_3}
    ...     ${device_model_3}
    ...     ${test_shop_id}
    ...     1
    ...     2
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}

[Prepare] - Create an agent and with shop
    ${device_unique_reference}     generate random string  12    [LETTERS]
    set test variable      ${test_device_unique_reference}      ${device_unique_reference}
    [System_User][Test][200] - login as setup system user
    [Agent][Test][200] - Create agent shop

[Prepare] - Create an agent and bind a device
    ${device_unique_reference}     generate random string  12    [LETTERS]
    set test variable      ${test_device_unique_reference}      ${device_unique_reference}
    [System_User][Test][200] - login as setup system user
    [Agent][Test][200] - Create agent shop
    [Agent][Test][200] - Authenticate agent
    [200] agent create device
    ...     ${test_agent_access_token}
    ...     ${device_unique_reference}
    ...     ${param_not_used}
    ...     ${test_shop_id}
    ...     1
    ...     2
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    [Prepare] - [200] Authenticate agent with input device_unique_reference and channel_id     ${device_unique_reference}      2

[Prepare] - Create an agent bind a device with EDC Serial Number
    ${device_unique_reference}     generate random string   12    [LETTERS]
    ${edc_serial_number}           generate random string   20    [LETTERS]
    set test variable      ${test_edc_serial_number}      ${edc_serial_number}
    set test variable      ${test_device_unique_reference}      ${device_unique_reference}
    [System_User][Test][200] - login as setup system user
    [Agent][Test][200] - Create agent shop
    [Agent][Test][200] - Authenticate agent
    ${dic}  [200] agent create device
    ...     ${test_agent_access_token}
    ...     ${device_unique_reference}
    ...     ${param_not_used}
    ...     ${test_shop_id}
    ...     3
    ...     1
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${edc_serial_number}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}

[Prepare] - Create an agent bind a device with Mobile Device Unique Reference
    ${device_unique_reference}     generate random string   12    [LETTERS]
    ${unique_number}           generate random string   12    [LETTERS]
    ${serial_number}           generate random string   20    [LETTERS]
    ${device_name}           generate random string   20    [LETTERS]
    set test variable      ${test_device_unique_reference}      ${device_unique_reference}
    set test variable      ${test_device_model}      ${device_unique_reference}
    set test variable      ${test_unique_number}      ${unique_number}
    set test variable      ${test_serial_number}      ${serial_number}
    set test variable      ${test_device_name}      ${device_name}
    [System_User][Test][200] - login as setup system user
    [Agent][Test][200] - Create agent shop
    [Agent][Test][200] - Authenticate agent
    ${dic}  [200] agent create device
    ...     ${test_agent_access_token}
    ...     ${test_device_unique_reference}
    ...     ${test_device_model}
    ...     ${test_shop_id}
    ...     1
    ...     2
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${test_device_name}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${test_unique_number}
    ...     ${test_serial_number}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}

[Prepare] - Create an agent and bind 3 devices
    ${device_unique_reference_1}     generate random string  12    [LETTERS]
    ${device_unique_reference_2}     generate random string  12    [LETTERS]
    ${device_unique_reference_3}     generate random string  12    [LETTERS]
    ${device_model_1}     generate random string  12    [LETTERS]
    ${device_model_2}     generate random string  12    [LETTERS]
    ${device_model_3}     generate random string  12    [LETTERS]
    [System_User][Test][200] - login as setup system user
    [Agent][Test][200] - Create agent shop
    [Agent][Test][200] - Authenticate agent
    [200] agent create device
    ...     ${test_agent_access_token}
    ...     ${device_unique_reference_1}
    ...     ${device_model_1}
    ...     ${test_shop_id}
    ...     1
    ...     2
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    [200] agent create device
    ...     ${test_agent_access_token}
    ...     ${device_unique_reference_2}
    ...     ${device_model_2}
    ...     ${test_shop_id}
    ...     1
    ...     2
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    [200] agent create device
    ...     ${test_agent_access_token}
    ...     ${device_unique_reference_3}
    ...     ${device_model_3}
    ...     ${test_shop_id}
    ...     1
    ...     2
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}


    set test variable   ${test_device_unique_reference_1}        ${device_unique_reference_1}
    set test variable   ${test_device_unique_reference_2}        ${device_unique_reference_2}
    set test variable   ${test_device_unique_reference_3}        ${device_unique_reference_3}
    set test variable   ${test_device_model_1}        ${device_model_1}
    set test variable   ${test_device_model_2}        ${device_model_2}
    set test variable   ${test_device_model_3}        ${device_model_3}

[Prepare] - Create an agent and bind 3 devices with login
    ${device_unique_reference_1}     generate random string  12    [LETTERS]
    ${device_unique_reference_2}     generate random string  12    [LETTERS]
    ${device_unique_reference_3}     generate random string  12    [LETTERS]
    ${device_model_1}     generate random string  12    [LETTERS]
    ${device_model_2}     generate random string  12    [LETTERS]
    ${device_model_3}     generate random string  12    [LETTERS]
    [System_User][Test][200] - login as setup system user
    [Agent][Test][200] - Create agent shop
    [Agent][Test][200] - Authenticate agent
    [200] agent create device
    ...     ${test_agent_access_token}
    ...     ${device_unique_reference_1}
    ...     ${device_model_1}
    ...     ${test_shop_id}
    ...     1
    ...     2
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    [200] agent create device
    ...     ${test_agent_access_token}
    ...     ${device_unique_reference_2}
    ...     ${device_model_2}
    ...     ${test_shop_id}
    ...     1
    ...     2
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    [200] agent create device
    ...     ${test_agent_access_token}
    ...     ${device_unique_reference_3}
    ...     ${device_model_3}
    ...     ${test_shop_id}
    ...     1
    ...     2
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}


    set test variable   ${test_device_unique_reference_1}        ${device_unique_reference_1}
    set test variable   ${test_device_unique_reference_2}        ${device_unique_reference_2}
    set test variable   ${test_device_unique_reference_3}        ${device_unique_reference_3}
    set test variable   ${test_device_model_1}        ${device_model_1}
    set test variable   ${test_device_model_2}        ${device_model_2}
    set test variable   ${test_device_model_3}        ${device_model_3}

    [Prepare] - [200] Authenticate agent with input device_unique_reference and channel_id     ${device_unique_reference_1}      2

[Data][200] create agent with required fields and identity
    [Arguments]     ${admin_access_token}
#    ${text}=   generate random string    10      [LETTERS]
#    ${agent_type_id}      set variable   	${setup_company_agent_type_id}
#    ${agent_unique_reference}      set variable   	${text}
#    ${agent_username}     set variable    ${text}
#    ${agent_password}      set variable   	${setup_agent_password_encrypted}
#    ${agent_password_login}     set variable    ${setup_agent_password_encrypted_utf8}
    ${arg_dic}      create dictionary
    ...         access_token=${admin_access_token}
    ...         agent_type_id=${agent_type_id}
    ...         unique_reference=${agent_unique_reference}
    ...         identity_type_id=1
    ...         username=${agent_username}
    ...         password=${agent_password}

    ${dic}      [200] API create agent  ${arg_dic}
    ${id}   get from dictionary       ${dic}    id
    set to dictionary   ${dic}      agent_id       ${id}
    set to dictionary   ${dic}      agent_type_id       ${agent_type_id}
    set to dictionary   ${dic}      agent_unique_reference      ${agent_unique_reference}
    set to dictionary   ${dic}      agent_username      ${agent_username}
    set to dictionary   ${dic}      agent_password      ${agent_password}
    set to dictionary   ${dic}      agent_password_login      ${agent_password_login}
    [Return]     ${dic}

[Agent][Test][Fail] - Create agent identity
    ${otp_reference_id}=    generate random string    1      [LETTERS]
    ${test_edc_serial_number}=    generate random string    10      [LETTERS]
    ${arg_dic}      create dictionary
    ...     otp_reference_id=${otp_reference_id}
    ...     username=${test_agent_username}
    ...     password=${test_agent_password_login}
    ...     agent_id=${test_agent_id}
    ...     identity_type=2
    ...     header_client_id=${setup_admin_client_id}
    ...     header_client_secret=${setup_admin_client_secret}
    ...     header_channel_id=2
    ...     header_device_unique_reference=${test_edc_serial_number}

    ${dic}      [Fail] API create agent identity        ${arg_dic}
    set test variable       ${test_result_dic}       ${dic}
    [Return]     ${dic}

[Agent][Suite][200] - System user create company representative profile
    [Arguments]     &{arg_dic}
    ${random_string}=    generate random string    5      [LETTERS]
    ${random_number}=    generate random string    5      [NUMBERS]
    ${arg_dic}      create dictionary
    ...     access_token=${arg_dic.access_token}
    ...     type_id=${arg_dic.type_id}
    ...     company_id=${arg_dic.company_id}
    ...     title=${random_string}_title
    ...     first_name=${random_string}_first_name
    ...     middle_name=${random_string}_middle_name
    ...     last_name=${random_string}_last_name
    ...     suffix=${random_string}_suffix
    ...     date_of_birth=1986-01-01T00:00:00Z
    ...     place_of_birth=${random_string}_place_of_birth
    ...     occupation=${random_string}_occupation
    ...     occupation_title=${random_string}_occupation_title
    ...     nationality=${random_string}_nationality
    ...     national_id_number=${random_string}_national_id_number
    ...     email=${random_string}_email@mail.com
    ...     source_of_funds=${random_string}_source_of_funds
    ...     mobile_number=${random_number}
    ...     business_phone_number=${random_number}
    ...     citizen_association=Ha Noi
    ...     neighbourhood_association=Ha Loi
    ...     address=165 Thai Ha
    ...     commune=Trung Tu
    ...     district=Dong Da
    ...     city=Ha Noi
    ...     province=Ha Noi
    ...     postal_code=100000
    ...     country=Viet Nam
    ...     landmark=Song Hong
    ...     longitude=123
    ...     latitude=123

    ${return_dic}  [200] API create company representative profiles     ${arg_dic}
    set suite variable   ${suite_company_representative_id}   ${return_dic.id}
    set suite variable   ${suite_company_representative_random_string}   ${random_string}
    set suite variable   ${suite_company_representative_random_number}   ${random_number}

[Suite][200] System user create company representatives profile all types
    [Agent][Suite][200] - System user create company representative profile     company_id=${suite_company_id}  access_token=${suite_admin_access_token}   type_id=1
    set suite variable   ${suite_company_billing_contact_representative_id}   ${suite_company_representative_id}
    set suite variable   ${suite_company_billing_contact_representative_random_string}   ${suite_company_representative_random_string}
    set suite variable   ${suite_company_billing_contact_representative_random_number}   ${suite_company_representative_random_number}
    [Agent][Suite][200] - System user create company representative profile     company_id=${suite_company_id}  access_token=${suite_admin_access_token}   type_id=2
    set suite variable   ${suite_company_legal_representative_id}   ${suite_company_representative_id}
    set suite variable   ${suite_company_legal_representative_random_string}   ${suite_company_representative_random_string}
    set suite variable   ${suite_company_legal_representative_random_number}   ${suite_company_representative_random_number}
    [Agent][Suite][200] - System user create company representative profile     company_id=${suite_company_id}  access_token=${suite_admin_access_token}   type_id=3
    set suite variable   ${suite_company_authorize_signatory_representative_id}   ${suite_company_representative_id}
    set suite variable   ${suite_company_authorize_signatory_representative_random_string}   ${suite_company_representative_random_string}
    set suite variable   ${suite_company_authorize_signatory_representative_random_number}   ${suite_company_representative_random_number}

[Test] Call API Delete agent by ID
    [200] API delete agent      ${suite_admin_access_token}     ${suite_agent_id}

[Test][200] create agent with full data
    ${test_agent_type_name}=   generate random string    10      [LETTERS]
    ${dic}      [200] API create agent type     ${suite_admin_access_token}     ${test_agent_type_name}
    ${agent_type_id}     get from dictionary      ${dic}     id
    set test variable    ${test_agent_type_id}       ${agent_type_id}

    ${unique_reference}   generate random string  10    [NUMBERS]
    set test variable    ${test_unique_reference}       ${unique_reference}

    ${acquisition_source}   generate random string  10    [LETTERS]
    set test variable    ${test_acquisition_source}       ${acquisition_source}

    ${mm_factory_card_number}		   generate random string  10    [NUMBERS]
    set test variable    ${test_mm_factory_card_number}       ${mm_factory_card_number}

    ${model_type}			   generate random string  10    [LETTERS]
    set test variable    ${test_model_type}       ${model_type}

    ${tin_number}		   generate random string  10    [NUMBERS]
    set test variable    ${test_tin_number}       ${tin_number}

    ${title}		   generate random string  10    [LETTERS]
    set test variable    ${test_title}       ${title}

    ${first_name}		   generate random string  10    [LETTERS]
    set test variable    ${test_first_name}       ${first_name}

    ${middle_name}		   generate random string  10    [LETTERS]
    set test variable    ${test_middle_name}       ${middle_name}

    ${last_name}		   generate random string  10    [LETTERS]
    set test variable    ${test_last_name}       ${last_name}

    ${suffix}		   generate random string  10    [LETTERS]
    set test variable    ${test_suffix}       ${suffix}

    set test variable    ${test_date_of_birth}       1988-01-01
    set test variable    ${date_of_birth}       ${test_date_of_birth}T00:00:00Z

    ${place_of_birth}	   generate random string  10    [LETTERS]
    set test variable    ${test_place_of_birth}       ${place_of_birth}

    set test variable    ${test_gender}       male

    ${ethnicity}		   generate random string  10    [LETTERS]
    set test variable    ${test_ethnicity}       ${ethnicity}

    ${nationality}		   generate random string  10    [LETTERS]
    set test variable    ${test_nationality}       ${nationality}

    ${occupation}		   generate random string  10    [LETTERS]
    set test variable    ${test_occupation}       ${occupation}

    ${occupation_title}		   generate random string  10    [LETTERS]
    set test variable    ${test_occupation_title}       ${occupation_title}

    ${township_code}		   generate random string  10    [LETTERS]
    set test variable    ${test_township_code}       ${township_code}

    ${township_name}		   generate random string  10    [LETTERS]
    set test variable    ${test_township_name}       ${township_name}

    ${nationality_id_number}		   generate random string  10    [NUMBERS]
    set test variable    ${test_nationality_id_number}       ${nationality_id_number}

    ${mother_name}		   generate random string  10    [LETTERS]
    set test variable    ${test_mother_name}       ${mother_name}

    ${email}		   generate random string  10    [LETTERS]
    ${gen_email}=    set variable    ${email}@email.com
    set test variable    ${test_email}       ${gen_email}

    ${primary_mobile_number}		   generate random string  10    [NUMBERS]
    set test variable    ${test_primary_mobile_number}       ${primary_mobile_number}

    ${secondary_mobile_number}		   generate random string  10    [NUMBERS]
    set test variable    ${test_secondary_mobile_number}       ${secondary_mobile_number}

    ${tertiary_mobile_number}		   generate random string  10    [NUMBERS]
    set test variable    ${test_tertiary_mobile_number}       ${tertiary_mobile_number}

    ${current_address_citizen_association}	generate random string  10    [LETTERS]
    set test variable    ${test_current_address_citizen_association}       ${current_address_citizen_association}

    ${current_address_neighbourhood_association}		   generate random string  10    [LETTERS]
    set test variable    ${test_current_address_neighbourhood_association}       ${current_address_neighbourhood_association}

    ${current_address_address}		   generate random string  10    [LETTERS]
    set test variable    ${test_current_address_address}       ${current_address_address}

    ${current_address_commune}		   generate random string  10    [LETTERS]
    set test variable    ${test_current_address_commune}       ${current_address_commune}

    ${current_address_district}		   generate random string  10    [LETTERS]
    set test variable    ${test_current_address_district}       ${current_address_district}

    ${current_address_city}		   generate random string  10    [LETTERS]
    set test variable    ${test_current_address_city}       ${current_address_city}

    ${current_address_province}		   generate random string  10    [LETTERS]
    set test variable    ${test_current_address_province}       ${current_address_province}

    ${current_address_postal_code}		   generate random string  10    [LETTERS]
    set test variable    ${test_current_address_postal_code}       ${current_address_postal_code}

    ${current_address_country}		   generate random string  10    [LETTERS]
    set test variable    ${test_current_address_country}       ${current_address_country}

    ${current_address_landmark}		   generate random string  10    [LETTERS]
    set test variable    ${test_current_address_landmark}       ${current_address_landmark}

    ${current_address_longitude}		   generate random string  10    [LETTERS]
    set test variable    ${test_current_address_longitude}       ${current_address_longitude}

    ${current_address_latitude}		   generate random string  10    [LETTERS]
    set test variable    ${test_current_address_latitude}       ${current_address_latitude}

    ${permanent_address_citizen_association}		   generate random string  10    [LETTERS]
    set test variable    ${test_permanent_address_citizen_association}       ${permanent_address_citizen_association}

    ${permanent_address_neighbourhood_association}		   generate random string  10    [LETTERS]
    set test variable    ${test_permanent_address_neighbourhood_association}       ${permanent_address_neighbourhood_association}

    ${permanent_address_address}		   generate random string  10    [LETTERS]
    set test variable    ${test_permanent_address_address}       ${permanent_address_address}

    ${permanent_address_commune}		   generate random string  10    [LETTERS]
    set test variable    ${test_permanent_address_commune}       ${permanent_address_commune}

    ${permanent_address_district}		   generate random string  10    [LETTERS]
    set test variable    ${test_permanent_address_district}       ${permanent_address_district}

    ${permanent_address_city}		   generate random string  10    [LETTERS]
    set test variable    ${test_permanent_address_city}       ${permanent_address_city}

    ${permanent_address_province}		   generate random string  10    [LETTERS]
    set test variable    ${test_permanent_address_province}       ${permanent_address_province}

    ${permanent_address_postal_code}		   generate random string  10    [LETTERS]
    set test variable    ${test_permanent_address_postal_code}       ${permanent_address_postal_code}

    ${permanent_address_country}		   generate random string  10    [LETTERS]
    set test variable    ${test_permanent_address_country}       ${permanent_address_country}

    ${permanent_address_landmark}		   generate random string  10    [LETTERS]
    set test variable    ${test_permanent_address_landmark}       ${permanent_address_landmark}

    ${permanent_address_longitude}		   generate random string  10    [LETTERS]
    set test variable    ${test_permanent_address_longitude}       ${permanent_address_longitude}

    ${permanent_address_latitude}		   generate random string  10    [LETTERS]
    set test variable    ${test_permanent_address_latitude}       ${permanent_address_latitude}

    ${bank_name}		   generate random string  10    [LETTERS]
    set test variable    ${test_bank_name}       ${bank_name}

    ${bank_account_name}		   generate random string  10    [LETTERS]
    set test variable    ${test_bank_account_name}       ${bank_account_name}

    ${bank_account_number}		   generate random string  10    [NUMBERS]
    set test variable    ${test_bank_account_number}       ${bank_account_number}

    ${bank_branch_area}		   generate random string  10    [LETTERS]
    set test variable    ${test_bank_branch_area}       ${bank_branch_area}

    ${bank_branch_city}		   generate random string  10    [LETTERS]
    set test variable    ${test_bank_branch_city}       ${bank_branch_city}

    ${bank_register_source}		   generate random string  10    [LETTERS]
    set test variable    ${test_bank_register_source}       ${bank_register_source}

    ${contract_type}		   generate random string  10    [LETTERS]
    set test variable    ${test_contract_type}       ${contract_type}

    ${contract_number}		   generate random string  10    [NUMBERS]
    set test variable    ${test_contract_number}       ${contract_number}

    ${contract_extension_type}		   generate random string  10    [LETTERS]
    set test variable    ${test_contract_extension_type}       ${contract_extension_type}

    ${contract_notification_alert}		   generate random string  10    [LETTERS]
    set test variable    ${test_contract_notification_alert}       ${contract_notification_alert}

    ${contract_day_of_period_reconciliation}		   generate random string  5    [NUMBERS]
    set test variable    ${test_contract_day_of_period_reconciliation}       ${contract_day_of_period_reconciliation}

    ${contract_release}		   generate random string  10    [LETTERS]
    set test variable    ${test_contract_release}       ${contract_release}

    ${contract_file_url}		   generate random string  10    [LETTERS]
    set test variable    ${test_contract_file_url}       ${contract_file_url}

    ${contract_assessment_information_url}		   generate random string  10    [LETTERS]
    set test variable    ${test_contract_assessment_information_url}       ${contract_assessment_information_url}

    ${primary_identity_type}		   generate random string  10    [LETTERS]
    set test variable    ${test_primary_identity_type}       ${primary_identity_type}

    ${primary_identity_status}		   generate random string  10    [LETTERS]
    set test variable    ${test_primary_identity_status}       ${primary_identity_status}

    ${primary_identity_identity_id}		   generate random string  10    [LETTERS]
    set test variable    ${test_primary_identity_identity_id}       ${primary_identity_identity_id}

    ${primary_identity_place_of_issue}		   generate random string  10    [LETTERS]
    set test variable    ${test_primary_identity_place_of_issue}       ${primary_identity_place_of_issue}

    set test variable    ${test_primary_identity_issue_date}       2000-01-01
    set test variable    ${primary_identity_issue_date}       ${test_primary_identity_issue_date}T00:00:00Z

    set test variable    ${test_primary_identity_expired_date}       2220-01-01
    set test variable    ${primary_identity_expired_date}       ${test_primary_identity_expired_date}T00:00:00Z

    ${primary_identity_front_identity_url}		   generate random string  10    [LETTERS]
    set test variable    ${test_primary_identity_front_identity_url}       ${primary_identity_front_identity_url}

    ${primary_identity_back_identity_url}		   generate random string  10    [LETTERS]
    set test variable    ${test_primary_identity_back_identity_url}       ${primary_identity_back_identity_url}

    ${secondary_identity_type}		   generate random string  10    [LETTERS]
    set test variable    ${test_secondary_identity_type}       ${secondary_identity_type}

    ${secondary_identity_status}		   generate random string  10    [LETTERS]
    set test variable    ${test_secondary_identity_status}       ${secondary_identity_status}

    ${secondary_identity_identity_id}		   generate random string  10    [LETTERS]
    set test variable    ${test_secondary_identity_identity_id}       ${secondary_identity_identity_id}

    ${secondary_identity_place_of_issue}		   generate random string  10    [LETTERS]
    set test variable    ${test_secondary_identity_place_of_issue}       ${secondary_identity_place_of_issue}

    ${secondary_identity_front_identity_url}		   generate random string  10    [LETTERS]
    set test variable    ${test_secondary_identity_front_identity_url}       ${secondary_identity_front_identity_url}

    ${secondary_identity_back_identity_url}		   generate random string  10    [LETTERS]
    set test variable    ${test_secondary_identity_back_identity_url}       ${secondary_identity_back_identity_url}

    ${remark}		   generate random string  10    [LETTERS]
    set test variable    ${test_remark}       ${remark}

    ${verify_by}		   generate random string  10    [LETTERS]
    set test variable    ${test_verify_by}       ${verify_by}

    ${risk_level}		   generate random string  10    [LETTERS]
    set test variable    ${test_risk_level}       ${risk_level}

    ${additional_acquiring_sales_executive_name}		   generate random string  10    [LETTERS]
    set test variable    ${test_additional_acquiring_sales_executive_name}       ${additional_acquiring_sales_executive_name}

    ${additional_relationship_manager_name}		   generate random string  10    [LETTERS]
    set test variable    ${test_additional_relationship_manager_name}       ${additional_relationship_manager_name}

    ${additional_sale_region}		   generate random string  10    [LETTERS]
    set test variable    ${test_additional_sale_region}       ${additional_sale_region}

    ${additional_commercial_account_manager}		   generate random string  10    [LETTERS]
    set test variable    ${test_additional_commercial_account_manager}       ${additional_commercial_account_manager}

    ${additional_profile_picture_url}		   generate random string  10    [LETTERS]
    set test variable    ${test_additional_profile_picture_url}       ${additional_profile_picture_url}

    ${additional_national_id_photo_url}		   generate random string  10    [LETTERS]
    set test variable    ${test_additional_national_id_photo_url}       ${additional_national_id_photo_url}

    ${additional_tax_id_card_photo_url}		   generate random string  10    [LETTERS]
    set test variable    ${test_additional_tax_id_card_photo_url}       ${additional_tax_id_card_photo_url}

    ${additional_field_1_name}		   generate random string  10    [LETTERS]
    set test variable    ${test_additional_field_1_name}       ${additional_field_1_name}

    ${additional_field_1_value}		   generate random string  10    [LETTERS]
    set test variable    ${test_additional_field_1_value}       ${additional_field_1_value}

    ${additional_field_2_name}		   generate random string  10    [LETTERS]
    set test variable    ${test_additional_field_2_name}       ${additional_field_2_name}

    ${additional_field_2_value}		   generate random string  10    [LETTERS]
    set test variable    ${test_additional_field_2_value}       ${additional_field_2_value}

    ${additional_field_3_name}		   generate random string  10    [LETTERS]
    set test variable    ${test_additional_field_3_name}       ${additional_field_3_name}

    ${additional_field_3_value}		   generate random string  10    [LETTERS]
    set test variable    ${test_additional_field_3_value}       ${additional_field_3_value}

    ${additional_field_4_name}		   generate random string  10    [LETTERS]
    set test variable    ${test_additional_field_4_name}       ${additional_field_4_name}

    ${additional_field_4_value}		   generate random string  10    [LETTERS]
    set test variable    ${test_additional_field_4_value}       ${additional_field_4_value}

    ${additional_field_5_name}		   generate random string  10    [LETTERS]
    set test variable    ${test_additional_field_5_name}       ${additional_field_5_name}

    ${additional_field_5_value}		   generate random string  10    [LETTERS]
    set test variable    ${test_additional_field_5_value}       ${additional_field_5_value}

    ${additional_supporting_file_1_url}		   generate random string  10    [LETTERS]
    set test variable    ${test_additional_supporting_file_1_url}       ${additional_supporting_file_1_url}

    ${additional_supporting_file_2_url}		   generate random string  10    [LETTERS]
    set test variable    ${test_additional_supporting_file_2_url}       ${additional_supporting_file_2_url}

    ${additional_supporting_file_3_url}		   generate random string  10    [LETTERS]
    set test variable    ${test_additional_supporting_file_3_url}       ${additional_supporting_file_3_url}

    ${additional_supporting_file_4_url}		   generate random string  10    [LETTERS]
    set test variable    ${test_additional_supporting_file_4_url}       ${additional_supporting_file_4_url}

    ${additional_supporting_file_5_url}		   generate random string  10    [LETTERS]
    set test variable    ${test_additional_supporting_file_5_url}       ${additional_supporting_file_5_url}

    ${dic}      [200] API create agent profiles
    ...     ${suite_admin_access_token}
    ...     ${test_agent_type_id}
    ...     ${test_unique_reference}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     3
    ...     system-user
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${mm_factory_card_number}
    ...     ${model_type}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${tin_number}
    ...     ${title}
    ...     ${first_name}
    ...     ${middle_name}
    ...     ${last_name}
    ...     ${suffix}
    ...     ${date_of_birth}
    ...     ${place_of_birth}
    ...     ${test_gender}
    ...     ${ethnicity}
    ...     ${nationality}
    ...     ${occupation}
    ...     ${occupation_title}
    ...     ${township_code}
    ...     ${township_name}
    ...     ${nationality_id_number}
    ...     ${mother_name}
    ...     ${gen_email}
    ...     ${primary_mobile_number}
    ...     ${secondary_mobile_number}
    ...     ${tertiary_mobile_number}
    ...     ${current_address_citizen_association}
    ...     ${current_address_neighbourhood_association}
    ...     ${current_address_address}
    ...     ${current_address_commune}
    ...     ${current_address_district}
    ...     ${current_address_city}
    ...     ${current_address_province}
    ...     ${current_address_postal_code}
    ...     ${current_address_country}
    ...     ${current_address_landmark}
    ...     ${current_address_longitude}
    ...     ${current_address_latitude}
    ...     ${permanent_address_citizen_association}
    ...     ${permanent_address_neighbourhood_association}
    ...     ${permanent_address_address}
    ...     ${permanent_address_commune}
    ...     ${permanent_address_district}
    ...     ${permanent_address_city}
    ...     ${permanent_address_province}
    ...     ${permanent_address_postal_code}
    ...     ${permanent_address_country}
    ...     ${permanent_address_landmark}
    ...     ${permanent_address_longitude}
    ...     ${permanent_address_latitude}
    ...     ${bank_name}
    ...     ${param_not_used}
    ...     ${bank_account_name}
    ...     ${bank_account_number}
    ...     ${bank_branch_area}
    ...     ${bank_branch_city}
    ...     ${param_not_used}
    ...     ${bank_register_source}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${contract_type}
    ...     ${contract_number}
    ...     ${contract_extension_type}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${contract_notification_alert}
    ...     ${contract_day_of_period_reconciliation}
    ...     ${contract_release}
    ...     ${contract_file_url}
    ...     ${contract_assessment_information_url}
    ...     ${primary_identity_type}
    ...     1
    ...     ${primary_identity_identity_id}
    ...     ${primary_identity_place_of_issue}
    ...     ${primary_identity_issue_date}
    ...     ${primary_identity_expired_date}
    ...     ${primary_identity_front_identity_url}
    ...     ${primary_identity_back_identity_url}
    ...     ${secondary_identity_type}
    ...     1
    ...     ${secondary_identity_identity_id}
    ...     ${secondary_identity_place_of_issue}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${secondary_identity_front_identity_url}
    ...     ${secondary_identity_back_identity_url}
    ...     ${param_not_used}
    ...     ${remark}
    ...     ${verify_by}
    ...     ${param_not_used}
    ...     ${risk_level}
    ...     ${param_not_used}
    ...     ${additional_acquiring_sales_executive_name}
    ...     ${param_not_used}
    ...     ${additional_relationship_manager_name}
    ...     ${additional_sale_region}
    ...     ${additional_commercial_account_manager}
    ...     ${additional_profile_picture_url}
    ...     ${additional_national_id_photo_url}
    ...     ${additional_tax_id_card_photo_url}
    ...     ${additional_field_1_name}
    ...     ${additional_field_1_value}
    ...     ${additional_field_2_name}
    ...     ${additional_field_2_value}
    ...     ${additional_field_3_name}
    ...     ${additional_field_3_value}
    ...     ${additional_field_4_name}
    ...     ${additional_field_4_value}
    ...     ${additional_field_5_name}
    ...     ${additional_field_5_value}
    ...     ${additional_supporting_file_1_url}
    ...     ${additional_supporting_file_2_url}
    ...     ${additional_supporting_file_3_url}
    ...     ${additional_supporting_file_4_url}
    ...     ${additional_supporting_file_5_url}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}

    ${id}   get from dictionary       ${dic}    id
    set test variable        ${test_agent_id}     ${id}

[Suite][200] create company profiles with required fields
  ${name}       generate random string    20  [LETTERS]
  ${arg_dic}            create dictionary    access_token=${suite_admin_access_token}  name=${name}
  ${return_dic}         [200] API create company profiles   ${arg_dic}
  set suite variable    ${suite_company_id}     ${return_dic.id}
  set suite variable    ${suite_company_name}       ${name}

[Test][200] create company profiles with required fields
  ${name}       generate random string    20  [LETTERS]
  ${arg_dic}            create dictionary    access_token=${suite_admin_access_token}  name=${name}
  ${return_dic}         [200] API create company profiles   ${arg_dic}
  set test variable    ${test_company_id}     ${return_dic.id}
  set test variable    ${test_company_name}       ${name}

[Test][200] delete company profile via api
    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     company_id=${test_company_id}
    [200] API delete company profiles  ${arg_dic}

[Suite][200] create sales with full data
    [Suite][200] create company profiles with required fields
    ${suite_agent_type_name}=   generate random string    10      [LETTERS]
    set suite variable      ${suite_agent_type_name}        ${suite_agent_type_name}
    ${dic}      [200] API create agent type     ${suite_admin_access_token}     ${suite_agent_type_name}
    ${type_id}     get from dictionary      ${dic}     id
    set suite variable    ${suite_sale_type_id}       ${type_id}
    set suite variable    ${suite_agent_id}      ${type_id}
    set suite variable    ${suite_agent_type_id}       ${type_id}

    ${unique_reference}   generate random string  10    [NUMBERS]
    set suite variable    ${suite_unique_reference}       ${unique_reference}

    ${acquisition_source}   generate random string  10    [LETTERS]
    set suite variable    ${suite_acquisition_source}       ${acquisition_source}

    ${mm_factory_card_number}		   generate random string  10    [NUMBERS]
    set suite variable    ${suite_mm_factory_card_number}       ${mm_factory_card_number}

    ${model_type}			   generate random string  10    [LETTERS]
    set suite variable    ${suite_model_type}       ${model_type}

    ${tin_number}		   generate random string  10    [NUMBERS]
    set suite variable    ${suite_tin_number}       ${tin_number}

    ${title}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_title}       ${title}

    ${first_name}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_first_name}       ${first_name}

    ${middle_name}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_middle_name}       ${middle_name}

    ${last_name}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_last_name}       ${last_name}

    ${suffix}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_suffix}       ${suffix}

    set suite variable    ${suite_date_of_birth}       1988-01-01
    set suite variable    ${date_of_birth}       ${suite_date_of_birth}T00:00:00Z

    ${place_of_birth}	   generate random string  10    [LETTERS]
    set suite variable    ${suite_place_of_birth}       ${place_of_birth}

    set suite variable    ${suite_gender}       male

    ${ethnicity}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_ethnicity}       ${ethnicity}

    ${nationality}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_nationality}       ${nationality}

    ${occupation}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_occupation}       ${occupation}

    ${occupation_title}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_occupation_title}       ${occupation_title}

    ${township_code}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_township_code}       ${township_code}

    ${township_name}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_township_name}       ${township_name}

    ${nationality_id_number}		   generate random string  10    [NUMBERS]
    set suite variable    ${suite_nationality_id_number}       ${nationality_id_number}

    ${mother_name}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_mother_name}       ${mother_name}

    ${email}		   generate random string  10    [LETTERS]
    ${gen_email}=    set variable    ${email}@email.com
    set suite variable    ${suite_email}       ${gen_email}

    ${primary_mobile_number}		   generate random string  10    [NUMBERS]
    set suite variable    ${suite_primary_mobile_number}       ${primary_mobile_number}

    ${secondary_mobile_number}		   generate random string  10    [NUMBERS]
    set suite variable    ${suite_secondary_mobile_number}       ${secondary_mobile_number}

    ${tertiary_mobile_number}		   generate random string  10    [NUMBERS]
    set suite variable    ${suite_tertiary_mobile_number}       ${tertiary_mobile_number}

    ${current_address_citizen_association}	generate random string  10    [LETTERS]
    set suite variable    ${suite_current_address_citizen_association}       ${current_address_citizen_association}

    ${current_address_neighbourhood_association}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_current_address_neighbourhood_association}       ${current_address_neighbourhood_association}

    ${current_address_address}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_current_address_address}       ${current_address_address}

    ${current_address_commune}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_current_address_commune}       ${current_address_commune}

    ${current_address_district}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_current_address_district}       ${current_address_district}

    ${current_address_city}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_current_address_city}       ${current_address_city}

    ${current_address_province}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_current_address_province}       ${current_address_province}

    ${current_address_postal_code}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_current_address_postal_code}       ${current_address_postal_code}

    ${current_address_country}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_current_address_country}       ${current_address_country}

    ${current_address_landmark}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_current_address_landmark}       ${current_address_landmark}

    ${current_address_longitude}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_current_address_longitude}       ${current_address_longitude}

    ${current_address_latitude}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_current_address_latitude}       ${current_address_latitude}

    ${permanent_address_citizen_association}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_permanent_address_citizen_association}       ${permanent_address_citizen_association}

    ${permanent_address_neighbourhood_association}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_permanent_address_neighbourhood_association}       ${permanent_address_neighbourhood_association}

    ${permanent_address_address}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_permanent_address_address}       ${permanent_address_address}

    ${permanent_address_commune}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_permanent_address_commune}       ${permanent_address_commune}

    ${permanent_address_district}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_permanent_address_district}       ${permanent_address_district}

    ${permanent_address_city}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_permanent_address_city}       ${permanent_address_city}

    ${permanent_address_province}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_permanent_address_province}       ${permanent_address_province}

    ${permanent_address_postal_code}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_permanent_address_postal_code}       ${permanent_address_postal_code}

    ${permanent_address_country}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_permanent_address_country}       ${permanent_address_country}

    ${permanent_address_landmark}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_permanent_address_landmark}       ${permanent_address_landmark}

    ${permanent_address_longitude}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_permanent_address_longitude}       ${permanent_address_longitude}

    ${permanent_address_latitude}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_permanent_address_latitude}       ${permanent_address_latitude}

    ${bank_name}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_bank_name}       ${bank_name}

    ${bank_account_name}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_bank_account_name}       ${bank_account_name}

    ${bank_account_number}		   generate random string  10    [NUMBERS]
    set suite variable    ${suite_bank_account_number}       ${bank_account_number}

    ${bank_branch_area}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_bank_branch_area}       ${bank_branch_area}

    ${bank_branch_city}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_bank_branch_city}       ${bank_branch_city}

    ${bank_register_source}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_bank_register_source}       ${bank_register_source}

    ${contract_type}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_contract_type}       ${contract_type}

    ${contract_number}		   generate random string  10    [NUMBERS]
    set suite variable    ${suite_contract_number}       ${contract_number}

    ${contract_extension_type}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_contract_extension_type}       ${contract_extension_type}

    ${contract_notification_alert}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_contract_notification_alert}       ${contract_notification_alert}

    ${contract_day_of_period_reconciliation}		   generate random string  5    [NUMBERS]
    set suite variable    ${suite_contract_day_of_period_reconciliation}       ${contract_day_of_period_reconciliation}

    ${contract_release}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_contract_release}       ${contract_release}

    ${contract_file_url}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_contract_file_url}       ${contract_file_url}

    ${contract_assessment_information_url}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_contract_assessment_information_url}       ${contract_assessment_information_url}

    ${primary_identity_type}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_primary_identity_type}       ${primary_identity_type}

    ${primary_identity_status}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_primary_identity_status}       ${primary_identity_status}

    ${primary_identity_identity_id}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_primary_identity_identity_id}       ${primary_identity_identity_id}

    ${primary_identity_place_of_issue}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_primary_identity_place_of_issue}       ${primary_identity_place_of_issue}

    set suite variable    ${suite_primary_identity_issue_date}       2000-01-01
    set suite variable    ${primary_identity_issue_date}       ${suite_primary_identity_issue_date}T00:00:00Z

    set suite variable    ${suite_primary_identity_expired_date}       2220-01-01
    set suite variable    ${primary_identity_expired_date}       ${suite_primary_identity_expired_date}T00:00:00Z

    ${primary_identity_front_identity_url}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_primary_identity_front_identity_url}       ${primary_identity_front_identity_url}

    ${primary_identity_back_identity_url}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_primary_identity_back_identity_url}       ${primary_identity_back_identity_url}

    ${secondary_identity_type}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_secondary_identity_type}       ${secondary_identity_type}

    ${secondary_identity_status}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_secondary_identity_status}       ${secondary_identity_status}

    ${secondary_identity_identity_id}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_secondary_identity_identity_id}       ${secondary_identity_identity_id}

    ${secondary_identity_place_of_issue}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_secondary_identity_place_of_issue}       ${secondary_identity_place_of_issue}

    ${secondary_identity_front_identity_url}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_secondary_identity_front_identity_url}       ${secondary_identity_front_identity_url}

    ${secondary_identity_back_identity_url}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_secondary_identity_back_identity_url}       ${secondary_identity_back_identity_url}

    ${remark}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_remark}       ${remark}

    ${verify_by}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_verify_by}       ${verify_by}

    ${risk_level}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_risk_level}       ${risk_level}

    ${additional_acquiring_sales_executive_name}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_additional_acquiring_sales_executive_name}       ${additional_acquiring_sales_executive_name}

    ${additional_relationship_manager_name}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_additional_relationship_manager_name}       ${additional_relationship_manager_name}

    ${additional_sale_region}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_additional_sale_region}       ${additional_sale_region}

    ${additional_commercial_account_manager}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_additional_commercial_account_manager}       ${additional_commercial_account_manager}

    ${additional_profile_picture_url}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_additional_profile_picture_url}       ${additional_profile_picture_url}

    ${additional_national_id_photo_url}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_additional_national_id_photo_url}       ${additional_national_id_photo_url}

    ${additional_tax_id_card_photo_url}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_additional_tax_id_card_photo_url}       ${additional_tax_id_card_photo_url}

    ${additional_field_1_name}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_additional_field_1_name}       ${additional_field_1_name}

    ${additional_field_1_value}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_additional_field_1_value}       ${additional_field_1_value}

    ${additional_field_2_name}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_additional_field_2_name}       ${additional_field_2_name}

    ${additional_field_2_value}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_additional_field_2_value}       ${additional_field_2_value}

    ${additional_field_3_name}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_additional_field_3_name}       ${additional_field_3_name}

    ${additional_field_3_value}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_additional_field_3_value}       ${additional_field_3_value}

    ${additional_field_4_name}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_additional_field_4_name}       ${additional_field_4_name}

    ${additional_field_4_value}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_additional_field_4_value}       ${additional_field_4_value}

    ${additional_field_5_name}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_additional_field_5_name}       ${additional_field_5_name}

    ${additional_field_5_value}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_additional_field_5_value}       ${additional_field_5_value}

    ${additional_supporting_file_1_url}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_additional_supporting_file_1_url}       ${additional_supporting_file_1_url}

    ${additional_supporting_file_2_url}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_additional_supporting_file_2_url}       ${additional_supporting_file_2_url}

    ${additional_supporting_file_3_url}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_additional_supporting_file_3_url}       ${additional_supporting_file_3_url}

    ${additional_supporting_file_4_url}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_additional_supporting_file_4_url}       ${additional_supporting_file_4_url}

    ${additional_supporting_file_5_url}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_additional_supporting_file_5_url}       ${additional_supporting_file_5_url}

     set suite variable    ${suite_is_testing_acc}       1

     set suite variable    ${suite_is_system_acc}       0

    ${employee_id}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_employee_id}       ${employee_id}

    ${calendar_id}		   generate random string  10    [LETTERS]
    set suite variable    ${suite_calendar_id}       ${calendar_id}

    ${dic}      [200] API create agent profiles
    ...     ${suite_admin_access_token}
    ...     ${suite_sale_type_id}
    ...     ${suite_unique_reference}
    ...     ${suite_is_testing_acc}
    ...     ${suite_is_system_acc}
    ...     ${param_not_used}
    ...     3
    ...     system-user
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${mm_factory_card_number}
    ...     ${model_type}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${tin_number}
    ...     ${title}
    ...     ${first_name}
    ...     ${middle_name}
    ...     ${last_name}
    ...     ${suffix}
    ...     ${date_of_birth}
    ...     ${place_of_birth}
    ...     ${suite_gender}
    ...     ${ethnicity}
    ...     ${nationality}
    ...     ${occupation}
    ...     ${occupation_title}
    ...     ${township_code}
    ...     ${township_name}
    ...     ${nationality_id_number}
    ...     ${mother_name}
    ...     ${gen_email}
    ...     ${primary_mobile_number}
    ...     ${secondary_mobile_number}
    ...     ${tertiary_mobile_number}
    ...     ${current_address_citizen_association}
    ...     ${current_address_neighbourhood_association}
    ...     ${current_address_address}
    ...     ${current_address_commune}
    ...     ${current_address_district}
    ...     ${current_address_city}
    ...     ${current_address_province}
    ...     ${current_address_postal_code}
    ...     ${current_address_country}
    ...     ${current_address_landmark}
    ...     ${current_address_longitude}
    ...     ${current_address_latitude}
    ...     ${permanent_address_citizen_association}
    ...     ${permanent_address_neighbourhood_association}
    ...     ${permanent_address_address}
    ...     ${permanent_address_commune}
    ...     ${permanent_address_district}
    ...     ${permanent_address_city}
    ...     ${permanent_address_province}
    ...     ${permanent_address_postal_code}
    ...     ${permanent_address_country}
    ...     ${permanent_address_landmark}
    ...     ${permanent_address_longitude}
    ...     ${permanent_address_latitude}
    ...     ${bank_name}
    ...     ${param_not_used}
    ...     ${bank_account_name}
    ...     ${bank_account_number}
    ...     ${bank_branch_area}
    ...     ${bank_branch_city}
    ...     ${param_not_used}
    ...     ${bank_register_source}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${contract_type}
    ...     ${contract_number}
    ...     ${contract_extension_type}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${contract_notification_alert}
    ...     ${contract_day_of_period_reconciliation}
    ...     ${contract_release}
    ...     ${contract_file_url}
    ...     ${contract_assessment_information_url}
    ...     ${primary_identity_type}
    ...     1
    ...     ${primary_identity_identity_id}
    ...     ${primary_identity_place_of_issue}
    ...     ${primary_identity_issue_date}
    ...     ${primary_identity_expired_date}
    ...     ${primary_identity_front_identity_url}
    ...     ${primary_identity_back_identity_url}
    ...     ${secondary_identity_type}
    ...     1
    ...     ${secondary_identity_identity_id}
    ...     ${secondary_identity_place_of_issue}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${secondary_identity_front_identity_url}
    ...     ${secondary_identity_back_identity_url}
    ...     ${param_not_used}
    ...     ${remark}
    ...     ${verify_by}
    ...     ${param_not_used}
    ...     ${risk_level}
    ...     ${param_not_used}
    ...     ${additional_acquiring_sales_executive_name}
    ...     ${param_not_used}
    ...     ${additional_relationship_manager_name}
    ...     ${additional_sale_region}
    ...     ${additional_commercial_account_manager}
    ...     ${additional_profile_picture_url}
    ...     ${additional_national_id_photo_url}
    ...     ${additional_tax_id_card_photo_url}
    ...     ${additional_field_1_name}
    ...     ${additional_field_1_value}
    ...     ${additional_field_2_name}
    ...     ${additional_field_2_value}
    ...     ${additional_field_3_name}
    ...     ${additional_field_3_value}
    ...     ${additional_field_4_name}
    ...     ${additional_field_4_value}
    ...     ${additional_field_5_name}
    ...     ${additional_field_5_value}
    ...     ${additional_supporting_file_1_url}
    ...     ${additional_supporting_file_2_url}
    ...     ${additional_supporting_file_3_url}
    ...     ${additional_supporting_file_4_url}
    ...     ${additional_supporting_file_5_url}
    ...     1
    ...     ${employee_id}
    ...     ${calendar_id}
    ...     ${setup_admin_client_id}
    ...     ${setup_admin_client_secret}
    ...     ${suite_company_id}

    ${id}   get from dictionary       ${dic}    id
    set suite variable        ${suite_sale_id}     ${id}
    [Return]    ${dic}

[Suite][200] Set suite_sale_id to be suite_sale_id_1
    set suite variable        ${suite_sale_id_1}     ${suite_sale_id}
[Suite][200] Set suite_sale_id to be suite_sale_id_2
    set suite variable        ${suite_sale_id_2}     ${suite_sale_id}

[Suite][200] create agent type
    ${gen_agent_type}=   generate random string    10      [LETTERS]
    ${gen_description}=   generate random string    20      [LETTERS]
    set suite variable  ${suite_agent_type}   ${gen_agent_type}
    set suite variable  ${suite_description}   ${gen_description}
    ${arg_dic}      create dictionary   username=${setup_admin_username}       password=${setup_password_admin_encrypted_utf8}
    ${dic}     [200] API system user authentication     ${arg_dic}
    ${get_admin_access_token}=          set variable                        ${dic.access_token}
    set suite variable      ${suite_admin_access_token}       ${get_admin_access_token}

    ${dic}  [200] API create agent type     ${suite_admin_access_token}     ${suite_agent_type}  ${suite_description}
    ${id}   get from dictionary     ${dic}  id
    set suite variable     ${suite_agent_type_id}     ${id}
    set suite variable     ${suite_agent_type_name}     ${suite_agent_type}

[Test][200] create agent type
    ${gen_agent_type}=   generate random string    10      [LETTERS]
    ${gen_description}=   generate random string    20      [LETTERS]
    set test variable  ${test_agent_type}   ${gen_agent_type}
    set test variable  ${test_description}   ${gen_description}

    ${dic}  [200] API create agent type     ${suite_admin_access_token}     ${test_agent_type}  ${test_description}
    ${id}   get from dictionary     ${dic}  id
    set test variable     ${test_agent_type_id}     ${id}
    set test variable     ${test_agent_type_name}     ${test_agent_type}

[Test][200] create agent sale type
    ${gen_agent_type}=   generate random string    10      [LETTERS]
    ${gen_description}=   generate random string    20      [LETTERS]
    set test variable  ${test_agent_type}   ${gen_agent_type}
    set test variable  ${test_description}   ${gen_description}
    set test variable  ${test_is_sale}   true
    ${dic}  [200] API create agent type     ${suite_admin_access_token}     ${test_agent_type}  ${test_description}     ${test_is_sale}
    ${id}   get from dictionary     ${dic}  id
    set test variable     ${test_agent_type_id}     ${id}
    set test variable     ${test_agent_type_name}     ${test_agent_type}

[Test][200] create agent with required fields
    ${text}=   generate random string    10      [NUMBERS]
    ${first_name}=   generate random string    10      [LETTERS]
    ${middle_name}=   generate random string    10      [LETTERS]
    ${last_name}=   generate random string    10      [LETTERS]
    set test variable   ${test_agent_type_id}      ${test_agent_type_id}
    set test variable   ${test_agent_unique_reference}      ${text}
    set test variable   ${test_agent_username}      ${text}
    set test variable   ${test_agent_password}      ${setup_agent_password_encrypted}
    set test variable   ${test_agent_password_login}      ${setup_agent_password_encrypted_utf8}
    set test variable   ${test_first_name}      ${first_name}
    set test variable   ${test_middle_name}      ${middle_name}
    set test variable   ${test_last_name}      ${last_name}

    ${arg_dic}     create dictionary
    ...         access_token=${suite_admin_access_token}
    ...         agent_type_id=${test_agent_type_id}
    ...         unique_reference=${test_agent_unique_reference}
    ...         username=${test_agent_username}
    ...         password=${test_agent_password}
    ...         identity_type_id=1
    ...         first_name=${first_name}
    ...         middle_name=${middle_name}
    ...         last_name=${last_name}

    ${dic}      [200] API create agent         ${arg_dic}
    set test variable        ${test_agent_id}     ${dic.id}

[Test][200] create agent
    ${agent_type_name}   generate random string  10    [LETTERS]
    set test variable       ${test_agent_type_name}     ${agent_type_name}
    ${dic}      [200] API create agent type     ${suite_admin_access_token}     ${test_agent_type_name}
    ${agent_type_id}     get from dictionary      ${dic}     id
    set test variable    ${test_agent_type_id}       ${agent_type_id}
    ${unique_reference}   generate random string  10    [LETTERS]
    set test variable    ${test_unique_reference}       ${unique_reference}
    ${username}   generate random string  10    [LETTERS]
    set test variable    ${test_username}       ${username}
    ${first_name}   generate random string  15    [LETTERS]
    set test variable    ${test_first_name}       ${first_name}
    ${last_name}   generate random string  15    [LETTERS]
    set test variable    ${test_last_name}       ${last_name}
    ${email}   generate random string  15    [LETTERS]
    set test variable    ${test_email}       ${email}
    ${primary_mobile_number}   generate random string  15    [NUMBERS]
    set test variable    ${test_mobile_number}       ${primary_mobile_number}
    set test variable    ${test_is_testing_account}     true
    set test variable    ${test_is_system_account}      true

    ${arg_dic}      create dictionary
    ...         access_token=${suite_admin_access_token}
    ...         agent_type_id=${test_agent_type_id}
    ...         unique_reference=${test_unique_reference}
    ...         identity_type_id=1
    ...         username=${test_username}
    ...         password=${setup_agent_password_encrypted}
    ...         is_testing_account=true
    ...         is_system_account=true
    ...         first_name=${first_name}
    ...         last_name=${last_name}
    ...         email=${email}
    ...         primary_mobile_number=${primary_mobile_number}
    ${dic}      [200] API create agent      ${arg_dic}

    ${agent_id}     get from dictionary      ${dic}     id
    set test variable    ${test_agent_id}       ${agent_id}
    ${shop_name}         generate random string  20  [LETTERS]
    ${address}=     generate random string    70      [LETTERS]
    set test variable   ${test_agent_shop_name}      ${shop_name}
    [200] API create shop
    ...     ${suite_admin_access_token}
    ...     ${test_agent_id}
    ...     ${param_not_used}
    ...     ${shop_name}
#    ...     ${param_not_used}
#    ...     ${address}


[Suite] prepare a new relationship via api for agents
    [Arguments]     ${agent_id_1}       ${agent_id_2}       ${relationship_type_id}
    ${dic}      create dictionary
    ...     relationship_type_id=${relationship_type_id}
    ...     main_user_id=${agent_id_1}
    ...     sub_user_id=${agent_id_2}
    ${dic_relationship}     [200] API create relationship   ${dic}
#    ${response}=    call create agent relationship type Admin-Agent via api      ${agent_id_1}       ${agent_id_2}
    ${relationship_id}     get from dictionary      ${dic_relationship}             id
    set suite variable     ${suite_relationship_id}         ${relationship_id}


[Suite][200] Add relationship between "Company Agent" and "Marketing Wallet" Agent
    [Suite] prepare a new relationship via api for agents        1           ${suite_marketing_wallet_agent_id}          2

[Agent][Suite][200] - create agent A in "${currency}" currency
    ${text}=   generate random string    10      [LETTERS]
    ${agent_type_name}      set variable   	${text}
    ${agent_type_id}      set variable   	${setup_company_agent_type_id}
    ${agent_unique_reference}      set variable   	${text}
    ${agent_username}     set variable    ${text}
    ${agent_password}      set variable   	${setup_agent_password_encrypted}
    ${agent_password_login}     set variable    ${setup_agent_password_encrypted_utf8}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      agent_type_name=${agent_type_name}
    ...     unique_reference=${agent_unique_reference}      identity_identity_type_id=1
    ...     agent_username=${agent_username}      agent_password=${agent_password}
    ...     agent_password_login=${agent_password_login}      currency=${currency}
    ...     user_type_id=2

    ${dic}  [Agent][Reuse][200] - create agent      ${arg_dic}
    set suite variable      ${suite_agent_a_id}       ${dic.agent_id}
    set suite variable      ${suite_agent_a_type}       agent
    set suite variable      ${suite_marketing_wallet_sof_cash_id}       ${dic.sof_cash_id_in_${currency}}
    ${arg_dic}      create dictionary       username=${agent_username}      password=${agent_password_login}
    ${dic}          [200] API authenticate agent        ${arg_dic}
    set suite variable      ${suite_agent_a_access_token}     ${dic.access_token}


[Suite][200] Create 2 random agents and create connection between them
    [Suite][200] Prepare an agent with sof_cash without prepare agent fund in
    set suite variable    ${suite_master_agent_type_name}       ${suite_agent_type_name}
    set suite variable    ${suite_master_agent_type_id}         ${suite_agent_type_id}
    set suite variable    ${suite_master_agent_id}      ${suite_agent_id}
    set suite variable    ${suite_master_agent_access_token}      ${suite_agent_access_token}
    [Suite][200] Prepare an agent with sof_cash without prepare agent fund in
    set suite variable    ${suite_sub_agent_type_name}       ${suite_agent_type_name}
    set suite variable    ${suite_sub_agent_type_id}         ${suite_agent_type_id}
    set suite variable    ${suite_sub_agent_id}      ${suite_agent_id}
    set suite variable    ${suite_sub_agent_access_token}      ${suite_agent_access_token}

    ${name}=   generate random string    10      [LETTERS]
    ${description}=   generate random string    15      [LETTERS]
    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      name=${name}          root_id=${suite_sub_agent_id}     description=${description}
    ${return_dic}=           [200] API create sale hierachy         ${arg_dic}
    ${sale_hierarchy_id}=    get from dictionary    ${return_dic}   hierarchy_id


    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      sale_hierarchy_id=${sale_hierarchy_id}          parent_id=${suite_master_agent_id}        child_id=${suite_sub_agent_id}
    [200] API create connection         ${arg_dic}

[Suite][200] Create company type
    ${name}    generate random string  10    [LETTERS]
    ${description}    generate random string  20    [LETTERS]

    ${arg_dic}      create dictionary   username=${setup_admin_username}       password=${setup_password_admin_encrypted_utf8}
    ${dic}     [200] API system user authentication     ${arg_dic}
    ${get_admin_access_token}=          set variable                        ${dic.access_token}
    set suite variable      ${suite_admin_access_token}       ${get_admin_access_token}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      name=${name}      description=${description}
    ${dic}    [200] API create company type    ${arg_dic}
    ${id}     get from dictionary    ${dic}   id
    set suite variable       ${suite_company_types_name_variable}       ${name}
    set suite variable       ${suite_company_types_id_variable}       ${id}

[Test][200] Create company type
    ${name}    generate random string  10    [LETTERS]
    ${description}    generate random string  20    [LETTERS]

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      name=${name}      description=${description}
    ${dic}    [200] API create company type    ${arg_dic}
    ${id}     get from dictionary    ${dic}   id
    set test variable       ${test_company_types_name_variable}       ${name}
    set test variable       ${test_company_types_id_variable}       ${id}

#    ${text}=   generate random string    10      [LETTERS]
#    ${agent_type_name}      set variable   	${text}
#    ${agent_type_id}      set variable   	${setup_company_agent_type_id}
#    ${agent_unique_reference}      set variable   	${text}
#    ${agent_username}     set variable    ${text}
#    ${agent_password}      set variable   	${setup_agent_password_encrypted}
#    ${agent_password_login}     set variable    ${setup_agent_password_encrypted_utf8}
#
#    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      agent_type_name=${agent_type_name}

[Suite][200] create sale hierarchy
    [Suite][200] create agent with required fields
    ${name}     generate random string      8       [LETTERS]
    ${description}     generate random string   100     [LETTERS]
    ${arg_dic}      create dictionary  access_token=${suite_admin_access_token}
                    ...     name=${name}    description=${description}      root_id=${suite_agent_id}
    ${return_dic}   [200] API Create Sale Hierachy     ${arg_dic}
    set suite variable  ${suite_sale_hierarchy_id}    ${return_dic.hierarchy_id}
    set suite variable  ${suite_sale_hierarchy_name}    ${name}
    set suite variable  ${suite_sale_hierarchy_root_id}     ${suite_agent_id}
    set suite variable  ${suite_sale_hierarchy_description}     ${description}

[Test][200] create sale hierarchy
    [Suite][200] create agent with required fields
    ${name}     generate random string      8       [LETTERS]
    ${description}     generate random string   100     [LETTERS]
    ${arg_dic}      create dictionary  access_token=${suite_admin_access_token}
                    ...     name=${name}    description=${description}      root_id=${suite_agent_id}
    ${return_dic}   [200] API Create Sale Hierachy     ${arg_dic}
    set test variable  ${test_sale_hierarchy_id}    ${return_dic.hierarchy_id}
    set test variable  ${test_sale_hierarchy_name}    ${name}
    set test variable  ${test_sale_hierarchy_root_id}     ${suite_agent_id}
    set test variable  ${test_sale_hierarchy_description}     ${description}

[Test][200] Create Sale Hierarchy with arguments
    [Arguments]  &{arg_dic}
    ${name}     generate random string      8       [LETTERS]
    ${description}     generate random string   100     [LETTERS]
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   name    ${name}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}   description    ${description}
    ${return_dic}   [200] API Create Sale Hierachy     ${arg_dic}
    set test variable  ${test_sale_hierarchy_id}    ${return_dic.hierarchy_id}
    set test variable  ${test_sale_hierarchy_name}    ${name}
    set test variable  ${test_sale_hierarchy_description}     ${description}

[Suite][200] create agent with required fields
    ${agent_type_name}   generate random string  6    [LETTERS]
    set suite variable       ${suite_agent_type_name}     ${agent_type_name}
    ${dic}      [200] API create agent type     ${suite_admin_access_token}     ${suite_agent_type_name}
    ${agent_type_id}     get from dictionary      ${dic}     id
    set suite variable    ${suite_agent_type_id}       ${agent_type_id}
    ${unique_reference}   generate random string  10    [NUMBERS]
    set suite variable    ${suite_unique_reference}       ${unique_reference}
    ${username}   generate random string  10    [LETTERS]
    set suite variable    ${suite_username}       ${username}
    ${arg_dic}      create dictionary
    ...         access_token=${suite_admin_access_token}
    ...         agent_type_id=${suite_agent_type_id}
    ...         unique_reference=${suite_unique_reference}
    ...         identity_type_id=5
    ...         username=${suite_username}
    ...         password=${setup_agent_password_encrypted}
    ${dic}      [200] API create agent  ${arg_dic}
    ${agent_id}     get from dictionary      ${dic}     id
    set suite variable    ${suite_agent_id}       ${agent_id}
    ${dic}  create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     agent_id=${agent_id}
    ${response}     [200] API get all agent identities   ${dic}
    set suite variable   ${suite_identity_id}     ${response['data'][0]['id']}
    set suite variable   ${suite_identity_type_name}     ${response['data'][0]['identity_type']['name']}


[Suite][200] Create two sales, create sale hierarchy and create connection for hierarchy
    [Suite][200] create sales with full data
    set suite variable    ${suite_master_agent_id}      ${suite_sale_id}
    set suite variable    ${suite_master_agent_first_name}      ${suite_first_name}
    set suite variable    ${suite_master_agent_type_id}      ${suite_agent_type_id}
    set suite variable    ${suite_master_agent_middle_name}      ${suite_middle_name}
    set suite variable    ${suite_master_agent_last_name}      ${suite_last_name}
    set suite variable    ${suite_master_agent_type_name}      ${suite_agent_type_name}
    set suite variable    ${suite_master_agent_type_id}      ${suite_sale_type_id}
    [Suite][200] create sales with full data
    set suite variable    ${suite_sub_agent_id}      ${suite_sale_id}
    set suite variable    ${suite_sub_agent_first_name}      ${suite_first_name}
    set suite variable    ${suite_sub_agent_last_name}      ${suite_last_name}
    set suite variable    ${suite_sub_agent_middle_name}      ${suite_middle_name}
    set suite variable    ${suite_sub_agent_type_id}      ${suite_agent_type_id}
    set suite variable    ${suite_sub_agent_unique_reference}      ${suite_unique_reference}
    [Suite][200] create sale hierarchy
    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      sale_hierarchy_id=${suite_sale_hierarchy_id}          parent_id=${suite_master_agent_id}        child_id=${suite_sub_agent_id}
    [200] API create connection         ${arg_dic}

[Suite] Setup suite variable for wallet agent
    set suite variable   ${suite_wallet_agent_id}    ${suite_agent_id}
    set suite variable   ${suite_wallet_agent_access_token}    ${suite_agent_access_token}
    set suite variable   ${suite_wallet_agent_sof_id}    ${suite_agent_sof_id_VN}

[Suite] Setup suite variable for connected agent
    set suite variable   ${suite_connected_agent_id}    ${suite_agent_id}
    set suite variable   ${suite_connected_agent_access_token}    ${suite_agent_access_token}
    set suite variable   ${suite_connected_agent_sof_id}    ${suite_agent_sof_id_VN}

[Suite] Setup suite variable for normal agent
    set suite variable   ${suite_normal_agent_id}    ${suite_agent_id}
    set suite variable   ${suite_normal_agent_access_token}    ${suite_agent_access_token}
    set suite variable   ${suite_normal_agent_sof_id}    ${suite_agent_sof_id_VN}
    set suite variable   ${suite_agent_type_id_payee}    ${suite_agent_type_id}

[Test][200] create smart card via api
    ${card_number}=   generate random string    10      [LETTERS]
    set test variable  ${test_card_number}     ${card_number}
    set test variable  ${test_serial_number}     ${card_number}
    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     agent_id=${suite_agent_id}
    ...     card_number=${test_card_number}
    ${dic}  [200] API create smart card    ${arg_dic}
    ${id}   get from dictionary       ${dic}    id
    set test variable     ${test_smart_card_id}     ${id}

[Agent][Test][200] - Create agent smart card via api
    ${card_number}=   generate random string     8       [NUMBERS]
    set test variable  ${test_card_number}     1${card_number}
    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     agent_id=${test_agent_id}
    ...     card_number=${test_card_number}
    ${dic}  [200] API create smart card    ${arg_dic}
    ${id}   get from dictionary       ${dic}    id
    set test variable     ${test_smart_card_id}     ${id}

[Test][200] create agent A in "${currency}" currency
    ${text}=   generate random string    10      [LETTERS]
    ${agent_type_name}      set variable   	${text}
    ${agent_type_id}      set variable   	${setup_company_agent_type_id}
    ${agent_unique_reference}      set variable   	${text}
    ${agent_username}     set variable    ${text}
    ${agent_password}      set variable   	${setup_agent_password_encrypted}
    ${agent_password_login}     set variable    ${setup_agent_password_encrypted_utf8}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      agent_type_name=${agent_type_name}
    ...     unique_reference=${agent_unique_reference}      identity_identity_type_id=1
    ...     agent_username=${agent_username}      agent_password=${agent_password}
    ...     agent_password_login=${agent_password_login}      currency=${currency}
    ...     user_type_id=2

    ${dic}  [Agent][Reuse][200] - create agent      ${arg_dic}
    set test variable      ${test_agent_a_id}       ${dic.agent_id}
    set test variable      ${test_agent_a_type}       agent
    set test variable      ${test_agent_a_sof_cash_id}       ${dic.sof_cash_id_in_${currency}}
    ${arg_dic}      create dictionary       username=${agent_username}      password=${agent_password_login}
    ${dic}          [200] API authenticate agent        ${arg_dic}
    set test variable      ${test_agent_a_access_token}     ${dic.access_token}

[Test][200] create agent B in "${currency}" currency
    ${text}=   generate random string    10      [LETTERS]
    ${agent_type_name}      set variable   	${text}
    ${agent_type_id}      set variable   	${setup_company_agent_type_id}
    ${agent_unique_reference}      set variable   	${text}
    ${agent_username}     set variable    ${text}
    ${agent_password}      set variable   	${setup_agent_password_encrypted}
    ${agent_password_login}     set variable    ${setup_agent_password_encrypted_utf8}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      agent_type_name=${agent_type_name}
    ...     unique_reference=${agent_unique_reference}      identity_identity_type_id=1
    ...     agent_username=${agent_username}      agent_password=${agent_password}
    ...     agent_password_login=${agent_password_login}      currency=${currency}
    ...     user_type_id=2

    ${dic}  [Agent][Reuse][200] - create agent      ${arg_dic}
    set test variable      ${test_agent_b_id}       ${dic.agent_id}
    set test variable      ${test_agent_b_type}       agent
    set test variable      ${test_agent_b_sof_cash_id}       ${dic.sof_cash_id_in_${currency}}
    ${arg_dic}      create dictionary       username=${agent_username}      password=${agent_password_login}
    ${dic}          [200] API authenticate agent        ${arg_dic}
    set test variable      ${test_agent_b_access_token}     ${dic.access_token}

[Test][200] Create agent C in "${currency}" currency
    ${text}=   generate random string    10      [LETTERS]
    ${agent_type_name}      set variable   	${text}
    ${agent_type_id}      set variable   	${setup_company_agent_type_id}
    ${agent_unique_reference}      set variable   	${text}
    ${agent_username}     set variable    ${text}
    ${agent_password}      set variable   	${setup_agent_password_encrypted}
    ${agent_password_login}     set variable    ${setup_agent_password_encrypted_utf8}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      agent_type_name=${agent_type_name}
    ...     unique_reference=${agent_unique_reference}      identity_identity_type_id=1
    ...     agent_username=${agent_username}      agent_password=${agent_password}
    ...     agent_password_login=${agent_password_login}      currency=${currency}
    ...     user_type_id=2

    ${dic}  [Agent][Reuse][200] - create agent      ${arg_dic}
    set test variable      ${test_agent_c_id}       ${dic.agent_id}
    set test variable      ${test_agent_c_type}       agent
    set test variable      ${test_agent_c_sof_cash_id}       ${dic.sof_cash_id_in_${currency}}
    ${arg_dic}      create dictionary       username=${agent_username}      password=${agent_password_login}
    ${dic}          [200] API authenticate agent        ${arg_dic}
    set test variable      ${test_agent_c_access_token}     ${dic.access_token}

[Suite][200] create agent remittance in "${currency}" currency
    ${text}=   generate random string    10      [LETTERS]
    ${agent_type_name}      set variable   	${text}
    ${agent_type_id}      set variable   	${setup_company_agent_type_id}
    ${agent_unique_reference}      set variable   	${text}
    ${agent_username}     set variable    ${text}
    ${agent_password}      set variable   	${setup_agent_password_encrypted}
    ${agent_password_login}     set variable    ${setup_agent_password_encrypted_utf8}
    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      agent_type_name=${agent_type_name}
    ...     unique_reference=${agent_unique_reference}      identity_identity_type_id=1
    ...     agent_username=${agent_username}      agent_password=${agent_password}
    ...     agent_password_login=${agent_password_login}      currency=${currency}
    ...     user_type_id=2

    ${dic}  [Agent][Reuse][200] - create agent      ${arg_dic}
    set suite variable      ${suite_agent_remittance_id}       ${dic.agent_id}
    set suite variable      ${suite_agent_remittance_type}       agent
    set suite variable      ${suite_agent_remittance_sof_cash_id}       ${dic.sof_cash_id_in_${currency}}
    ${arg_dic}      create dictionary       username=${agent_username}      password=${agent_password_login}
    ${dic}          [200] API authenticate agent        ${arg_dic}
    set suite variable      ${suite_agent_remittance_access_token}     ${dic.access_token}

[Suite][200] create smart card via api
    ${suite_card_number}=   generate random string    10      [LETTERS]
    ${get_created_date}=    Get Current Date    result_format=%Y-%m-%d
    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     agent_id=${suite_agent_id}
    ...     card_number=${suite_username}
    ${dic}  [200] API create smart card    ${arg_dic}
    ${id}   get from dictionary       ${dic}    id
    set suite variable     ${suite_smart_card_id}     ${id}
    set suite variable     ${suite_card_number}     ${suite_username}
    set suite variable  ${suite_created_date}     ${get_created_date}


[Suite][200] update smart card identity via api
    ${get_modified_date}=    Get Current Date    result_format=%Y-%m-%d
    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     smart_card_id=${suite_smart_card_id}
    ...     contain_password=false
    ...     identity_id=${suite_identity_id}
    ${dic}  [200] API update smart card    ${arg_dic}
    set suite variable     ${suite_contain_password}     USERNAME
    set suite variable     ${suite_state}     Only Username
    set suite variable  ${suite_modified_date}     ${get_modified_date}

[Suite][200] create shop with required fields
    ${shop_name}         generate random string  20  [LETTERS]
    ${dic}  [200] API create shop
    ...     ${suite_admin_access_token}
    ...     ${suite_agent_id}
    ...     ${param_not_used}
    ...     ${shop_name}
    ${id}   get from dictionary       ${dic}    id
    set suite variable     ${suite_shop_id}     ${id}

[Test][200] create shop with required fields
    ${shop_name}         generate random string  20  [LETTERS]
    ${dic}  [200] API create shop
    ...     ${suite_admin_access_token}
    ...     ${test_agent_id}
    ...     ${param_not_used}
    ...     ${shop_name}
    ${id}   get from dictionary       ${dic}    id
    set suite variable     ${test_shop_id}     ${id}

[Suite][200] create EDC agent device
    ${edc_serial_number}=                   generate random string              10        [NUMBERS]
    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     channel_type_id=3
    ...     channel_id=1
    ...     shop_id=${suite_shop_id}
    ...     edc_serial_number=${edc_serial_number}
    ${dic}  [200] API system user creates agent device    ${arg_dic}
    ${id}   get from dictionary       ${dic}    id
    set suite variable      ${suite_edc_device_id}     ${id}

[Suite][200] create Mobile sale device
    ${channel_type_id}      set variable    1
    set suite variable      ${suite_sale_mobile_channel_type_id}     ${channel_type_id}
    ${channel_id}           set variable    4
    set suite variable      ${suite_sale_mobile_channel_id}     ${channel_id}
    ${mac_address}=         generate random string              10        [LETTERS]
    set suite variable      ${suite_sale_mobile_mac_address}     ${mac_address}
    ${network_provider_name}=     generate random string              10        [LETTERS]
    set suite variable      ${suite_sale_mobile_network_provider_name}     ${network_provider_name}
    ${public_ip_address}=     generate random string              10        [NUMBER]
    set suite variable      ${suite_sale_mobile_public_ip_address}     ${public_ip_address}
    ${supporting_file_1}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_mobile_supporting_file_1}     ${supporting_file_1}
    ${supporting_file_2}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_mobile_supporting_file_2}     ${supporting_file_2}
    ${device_name}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_mobile_device_name}     ${device_name}
    ${device_model}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_mobile_device_model}     ${device_model}
    ${device_unique_reference}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_mobile_device_unique_reference}     ${device_unique_reference}
    ${os}=     generate random string              4        [LETTERS]
    set suite variable      ${suite_sale_mobile_os}     ${os}
    ${os_version}=     generate random string              10        [LETTERS]
    set suite variable      ${suite_sale_mobile_os_version}     ${os_version}
    ${display_size_in_inches}=     generate random string              3        [NUMBERS]
    set suite variable      ${suite_sale_mobile_display_size_in_inches}     ${display_size_in_inches}
    ${pixel_counts}=     generate random string              8        [NUMBERS]
    set suite variable      ${suite_sale_mobile_pixel_counts}     ${pixel_counts}
    ${unique_number}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_mobile_unique_number}     ${unique_number}
    ${serial_number}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_mobile_serial_number}     ${serial_number}
    ${app_version}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_mobile_app_version}     ${app_version}
    ${ssid}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_mobile_ssid}     ${ssid}

    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     channel_type_id=${channel_type_id}
    ...     channel_id=${channel_id}
    ...     owner_id=${suite_sale_id}
    ...     mac_address=${mac_address}
    ...     network_provider_name=${network_provider_name}
    ...     public_ip_address=${public_ip_address}
    ...     supporting_file_1=${supporting_file_1}
    ...     supporting_file_2=${supporting_file_2}
    ...     device_name=${device_name}
    ...     device_model=${device_model}
    ...     device_unique_reference=${device_unique_reference}
    ...     os=${os}
    ...     os_version=${os_version}
    ...     display_size_in_inches=${display_size_in_inches}
    ...     pixel_counts=${pixel_counts}
    ...     unique_number=${unique_number}
    ...     serial_number=${serial_number}
    ...     app_version=${app_version}
    ...     ssid=${ssid}
    ${dic}  [200] API system user creates agent device    ${arg_dic}
    ${id}   get from dictionary       ${dic}    id
    set suite variable      ${suite_sale_mobile_device_id}     ${id}

[Suite][200] create Web portal sale device
    ${channel_type_id}      set variable    2
    set suite variable      ${suite_sale_web_channel_type_id}     ${channel_type_id}
    ${channel_id}           set variable    5
    set suite variable      ${suite_sale_web_channel_id}     ${channel_id}
    ${mac_address}=         generate random string              10        [LETTERS]
    set suite variable      ${suite_sale_web_mac_address}     ${mac_address}
    ${network_provider_name}=     generate random string              10        [LETTERS]
    set suite variable      ${suite_sale_web_network_provider_name}     ${network_provider_name}
    ${public_ip_address}=     generate random string              10        [NUMBER]
    set suite variable      ${suite_sale_web_public_ip_address}     ${public_ip_address}
    ${supporting_file_1}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_web_supporting_file_1}     ${supporting_file_1}
    ${supporting_file_2}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_web_supporting_file_2}     ${supporting_file_2}

    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     channel_type_id=${channel_type_id}
    ...     channel_id=${channel_id}
    ...     owner_id=${suite_sale_id}
    ...     mac_address=${mac_address}
    ...     network_provider_name=${network_provider_name}
    ...     public_ip_address=${public_ip_address}
    ...     supporting_file_1=${supporting_file_1}
    ...     supporting_file_2=${supporting_file_2}
    ${dic}  [200] API system user creates agent device    ${arg_dic}
    ${id}   get from dictionary       ${dic}    id
    set suite variable      ${suite_sale_web_device_id}     ${id}

[Suite][200] create EDC sale device
    ${channel_type_id}=      set variable   3
    set suite variable      ${suite_sale_edc_channel_type_id}     ${channel_type_id}
    ${channel_id}=           set variable   1
    set suite variable      ${suite_sale_edc_channel_id}     ${channel_id}
    ${mac_address}=         generate random string              10        [LETTERS]
    set suite variable      ${suite_sale_edc_mac_address}     ${mac_address}
    ${network_provider_name}=     generate random string              10        [LETTERS]
    set suite variable      ${suite_sale_edc_network_provider_name}     ${network_provider_name}
    ${public_ip_address}=     generate random string              10        [NUMBER]
    set suite variable      ${suite_sale_edc_public_ip_address}     ${public_ip_address}
    ${supporting_file_1}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_edc_supporting_file_1}     ${supporting_file_1}
    ${supporting_file_2}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_edc_supporting_file_2}     ${supporting_file_2}
    ${edc_serial_number}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_edc_serial_number}     ${edc_serial_number}
    ${edc_model}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_edc_model}     ${edc_model}
    ${edc_firmware_version}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_edc_firmware_version}     ${edc_firmware_version}
    ${edc_software_version}=     generate random string              4        [LETTERS]
    set suite variable      ${suite_sale_edc_software_version}     ${edc_software_version}
    ${edc_sim_card_number}=     generate random string              10        [LETTERS]
    set suite variable      ${suite_sale_edc_sim_card_number}     ${edc_sim_card_number}
    ${edc_battery_serial_number}=     generate random string              10        [LETTERS]
    set suite variable      ${suite_sale_edc_battery_serial_number}     ${edc_battery_serial_number}
    ${edc_adapter_serial_number}=     generate random string              8        [NUMBERS]
    set suite variable      ${suite_sale_edc_adapter_serial_number}     ${edc_adapter_serial_number}
    ${edc_smartcard_1_number}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_edc_smartcard_1_number}     ${edc_smartcard_1_number}
    ${edc_smartcard_2_number}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_edc_smartcard_2_number}     ${edc_smartcard_2_number}
    ${edc_smartcard_3_number}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_edc_smartcard_3_number}     ${edc_smartcard_3_number}
    ${edc_smartcard_4_number}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_edc_smartcard_4_number}     ${edc_smartcard_4_number}
    ${edc_smartcard_5_number}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_edc_smartcard_5_number}     ${edc_smartcard_5_number}
    ${ssid}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_edc_ssid}     ${ssid}

    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     channel_type_id=${channel_type_id}
    ...     channel_id=${channel_id}
    ...     owner_id=${suite_sale_id}
    ...     mac_address=${mac_address}
    ...     network_provider_name=${network_provider_name}
    ...     public_ip_address=${public_ip_address}
    ...     supporting_file_1=${supporting_file_1}
    ...     supporting_file_2=${supporting_file_2}
    ...     edc_serial_number=${edc_serial_number}
    ...     edc_model=${edc_model}
    ...     edc_firmware_version=${edc_firmware_version}
    ...     edc_software_version=${edc_software_version}
    ...     edc_sim_card_number=${edc_sim_card_number}
    ...     edc_battery_serial_number=${edc_battery_serial_number}
    ...     edc_adapter_serial_number=${edc_adapter_serial_number}
    ...     edc_smartcard_1_number=${edc_smartcard_1_number}
    ...     edc_smartcard_2_number=${edc_smartcard_2_number}
    ...     edc_smartcard_3_number=${edc_smartcard_3_number}
    ...     edc_smartcard_4_number=${edc_smartcard_4_number}
    ...     edc_smartcard_5_number=${edc_smartcard_5_number}
    ...     ssid=${ssid}
    ${dic}  [200] API system user creates agent device    ${arg_dic}
    ${id}   get from dictionary       ${dic}    id
    set suite variable      ${suite_sale_edc_device_id}     ${id}

[Suite][200] create POS sale device
    ${channel_type_id}      set variable    4
    set suite variable      ${suite_sale_pos_channel_type_id}     ${channel_type_id}
    ${channel_id}           set variable    7
    set suite variable      ${suite_sale_pos_channel_id}     ${channel_id}
    ${mac_address}=         generate random string              10        [LETTERS]
    set suite variable      ${suite_sale_pos_mac_address}     ${mac_address}
    ${network_provider_name}=     generate random string              10        [LETTERS]
    set suite variable      ${suite_sale_pos_network_provider_name}     ${network_provider_name}
    ${public_ip_address}=     generate random string              10        [NUMBER]
    set suite variable      ${suite_sale_pos_public_ip_address}     ${public_ip_address}
    ${supporting_file_1}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_pos_supporting_file_1}     ${supporting_file_1}
    ${supporting_file_2}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_pos_supporting_file_2}     ${supporting_file_2}
    ${pos_serial_number}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_pos_serial_number}     ${pos_serial_number}
    ${pos_model}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_pos_model}     ${pos_model}
    ${pos_firmware_version}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_pos_firmware_version}     ${pos_firmware_version}
    ${pos_software_version}=     generate random string              4        [LETTERS]
    set suite variable      ${suite_sale_pos_software_version}     ${pos_software_version}
    ${pos_smartcard_1_number}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_pos_smartcard_1_number}     ${pos_smartcard_1_number}
    ${pos_smartcard_2_number}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_pos_smartcard_2_number}     ${pos_smartcard_2_number}
    ${pos_smartcard_3_number}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_pos_smartcard_3_number}     ${pos_smartcard_3_number}
    ${pos_smartcard_4_number}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_pos_smartcard_4_number}     ${pos_smartcard_4_number}
    ${pos_smartcard_5_number}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_pos_smartcard_5_number}     ${pos_smartcard_5_number}
    ${ssid}=     generate random string              20        [LETTERS]
    set suite variable      ${suite_sale_pos_ssid}     ${ssid}

    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     channel_type_id=${channel_type_id}
    ...     channel_id=${channel_id}
    ...     owner_id=${suite_sale_id}
    ...     mac_address=${mac_address}
    ...     network_provider_name=${network_provider_name}
    ...     public_ip_address=${public_ip_address}
    ...     supporting_file_1=${supporting_file_1}
    ...     supporting_file_2=${supporting_file_2}
    ...     pos_serial_number=${pos_serial_number}
    ...     pos_model=${pos_model}
    ...     pos_firmware_version=${pos_firmware_version}
    ...     pos_software_version=${pos_software_version}
    ...     pos_smartcard_1_number=${pos_smartcard_1_number}
    ...     pos_smartcard_2_number=${pos_smartcard_2_number}
    ...     pos_smartcard_3_number=${pos_smartcard_3_number}
    ...     pos_smartcard_4_number=${pos_smartcard_4_number}
    ...     pos_smartcard_5_number=${pos_smartcard_5_number}
    ...     ssid=${ssid}
    ${dic}  [200] API system user creates agent device    ${arg_dic}
    ${id}   get from dictionary       ${dic}    id
    set suite variable      ${suite_sale_pos_device_id}     ${id}

[Suite][200] Create devices for setup sale
    [Suite][200] create Mobile sale device
    [Suite][200] create Web portal sale device
    [Suite][200] create EDC sale device
    [Suite][200] create POS sale device

[Test][200] create mobile device for setup sales
    ${device_unique_reference}=                   generate random string              10        [NUMBERS]
    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     channel_type_id=1
    ...     channel_id=4
    ...     owner_id=${suite_sale_id}
    ...     device_unique_reference=${device_unique_reference}
    ${dic}  [200] API system user creates agent device    ${arg_dic}
    ${id}   get from dictionary       ${dic}    id
    set test variable       ${test_sales_mobile_device_id}     ${id}
    set test variable       ${test_sales_mobile_device_unique_reference}     ${device_unique_reference}

[Test][200] create EDC device for setup sales
    ${edc_serial_number}=                   generate random string              10        [NUMBERS]
    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     channel_type_id=3
    ...     channel_id=1
    ...     owner_id=${suite_sale_id}
    ...     edc_serial_number=${edc_serial_number}
    ${dic}  [200] API system user creates agent device    ${arg_dic}
    ${id}   get from dictionary       ${dic}    id
    set test variable       ${test_sales_edc_device_id}     ${id}
    set test variable       ${test_sales_edc_serial_number}     ${edc_serial_number}

[Agent][Test][200] - Create EDC agent device
    ${edc_serial_number}=                   generate random string              10        [NUMBERS]
    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     channel_type_id=3
    ...     channel_id=1
    ...     shop_id=${test_shop_id}
    ...     edc_serial_number=${edc_serial_number}
    ${dic}  [200] API system user creates agent device    ${arg_dic}
    ${id}   get from dictionary       ${dic}    id
    set test variable       ${test_edc_device_id}     ${id}
    set test variable       ${test_edc_serial_number}     ${edc_serial_number}

[Suite][200] create many EDC agent devices
    [Suite][200] create EDC agent device
    set suite variable      ${suite_edc_device_id_1}      ${suite_edc_device_id}
    [Suite][200] create EDC agent device
    set suite variable      ${suite_edc_device_id_2}      ${suite_edc_device_id}
    [Suite][200] create EDC agent device
    set suite variable      ${suite_edc_device_id_3}      ${suite_edc_device_id}
    [Suite][200] create EDC agent device

[Suite][200] create POS agent device
    ${pos_serial_number}=                   generate random string              10        [NUMBERS]
    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     channel_type_id=4
    ...     channel_id=7
    ...     shop_id=${suite_shop_id}
    ...     pos_serial_number=${pos_serial_number}
    ${dic}  [200] API system user creates agent device    ${arg_dic}
    ${id}   get from dictionary       ${dic}    id
    set suite variable     ${suite_pos_device_id}     ${id}

[Suite][200] link smart card to EDC device
    ${edc_serial_number}=                   generate random string              10        [NUMBERS]
    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     smart_card_id=${suite_smart_card_id}
    ...     device_id=${suite_edc_device_id}
    ${dic}  [200] API system user links smart card to device    ${arg_dic}
    ${id}   get from dictionary       ${dic}    id
    set suite variable      ${suite_link_smart_card_edc_device_id}     ${id}

[Agent][Test][200] - Link smart card to EDC device
    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     smart_card_id=${test_smart_card_id}
    ...     device_id=${test_edc_device_id}
    ${dic}  [200] API system user links smart card to device    ${arg_dic}
    ${id}   get from dictionary       ${dic}    id
    set test variable       ${test_link_smart_card_edc_device_id}     ${id}

[Suite][200] Create Sale
    ${current_date}    Get Current Date
    ${current_date}    Convert Date    ${current_date}     result_format=${datetime_format}
    set suite variable     ${suite_date_of_birth}     ${current_date}
    set suite variable     ${suite_register_date}     ${current_date}
    set suite variable     ${suite_end_date}     ${current_date}
    set suite variable     ${suite_contract_sign_date}     ${current_date}
    set suite variable     ${suite_contract_issue_date}     ${current_date}
    set suite variable     ${suite_contract_expiry_date}     ${current_date}
    set suite variable     ${suite_primary_issue_date}     ${current_date}
    set suite variable     ${suite_primary_expiry_date}     ${current_date}
    set suite variable     ${suite_secondary_issue_date}     ${current_date}
    set suite variable     ${suite_secondary_expiry_date}     ${current_date}
    # Set company id
    ${company_type_name}    generate random string  10  company_type_[LETTERS]
    ${arg_dic_company_type}      create dictionary   access_token=${suite_admin_access_token}      name=${company_type_name}
    ${dic_company_type}    [200] API create company type    ${arg_dic_company_type}
    ${company_type_id}     get from dictionary    ${dic_company_type}   id
    set suite variable       ${suite_company_type_name_variable}       ${company_type_name}
    set suite variable       ${suite_company_type_id_variable}       ${company_type_id}
    [Suite][200] create company profiles

    # Set Sale Type
    ${agent_type_name}   generate random string  6    [LETTERS]
    set suite variable       ${suite_agent_type_name}     ${agent_type_name}
    ${dic_sale_type}      [200] API create agent type     ${suite_admin_access_token}     ${suite_agent_type_name}
    ${sale_type_id}     get from dictionary      ${dic_sale_type}     id
    set suite variable    ${suite_sale_type_id}       ${sale_type_id}
    set suite variable    ${suite_sale_type_name}       ${agent_type_name}

    # Set classification
    ${classification_name}=   generate random string    10      [LETTERS]
    ${classification_desc}=   generate random string    10      [LETTERS]
    ${arg_dic_classification}      create dictionary    name=${classification_name}    description=${classification_desc}    access_token=${suite_admin_access_token}
    ${dic_classification}    [200] API create agent classification    ${arg_dic_classification}
    ${agent_classification_id}    get from dictionary    ${dic_classification}    id
    set suite variable    ${suite_agent_classification_id}    ${agent_classification_id}
    set suite variable    ${suite_agent_classification_name}    ${classification_name}

    # Set Profile accreditation status
    ${dic_config}      [200] API get configuration detail        access_token=${suite_admin_access_token}        scopeName=global        key=country
    set suite variable      ${country_code}     ${dic_config.value}
    ${dic_accreditation}   create dictionary      access_token=${suite_admin_access_token}    country_code=${country_code}
    ${dic_acceditation_status}      [200] API search agent accreditation status     ${dic_accreditation}
    set suite variable   ${suite_acceditation_status_id}     ${dic_acceditation_status.first_creditation_status_id}
    set suite variable   ${suite_acceditation_status_status}     ${dic_acceditation_status.first_creditation_status_status}

    # Set status
    set suite variable    ${suite_status}    Active

    # Set user name type
    ${user_name_type}=    set variable    Free Text Username Alphanumeric
    set suite variable    ${suite_user_name_type}    ${user_name_type}
    set suite variable    ${suite_user_name_type_id}    1

    # Set referrer user type
    ${referrer_user_type_id}    set variable    1
    set suite variable    ${suite_referrer_user_type_id}    ${referrer_user_type_id}
    set suite variable    ${suite_referrer_user_type_name}    Customer

    # Set currency
    set suite variable    ${suite_currency}    VND

    # Set MM card type
    set suite variable    ${suite_mm_card_type_id}    1
    set suite variable    ${suite_mm_card_type_name}    Salesperson

    # Set MM card level
    set suite variable    ${suite_mm_card_level_id}    1
    set suite variable    ${suite_mm_card_level}    Level0

    # Set Model type
    set suite variable    ${suite_model_type}    1

    # Set bank status
    set suite variable    ${suite_bank_account_status}    1
    set suite variable    ${suite_bank_account_status_name}    Active

    # Set primary identity status
    set suite variable    ${suite_primary_identity_status}    1
    set suite variable    ${suite_primary_identity_status_name}    Active
    # Set secondary identity status
    set suite variable    ${suite_secondary_identity_status}    1
    set suite variable    ${suite_secondary_identity_status_name}    Active

    # Set risk level
    set suite variable    ${suite_risk_level}    High

    ${mm_factory_card_number}=   generate random string    5      123456789
    set suite variable    ${suite_mm_factory_card_number}    ${mm_factory_card_number}

    ${tin_number}=   generate random string    5      123456789
    set suite variable    ${suite_tin_number}    ${tin_number}

    ${title}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_title}    ${title}

    ${title_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_title_local}    ${title_local}

    ${first_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_first_name}    ${first_name}

    ${middle_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_middle_name}    ${middle_name}

    ${last_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_last_name}    ${last_name}

    ${suffix}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_suffix}    ${suffix}

    ${place_of_birth}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_place_of_birth}    ${place_of_birth}

    ${gender}=   generate random string    1      MF
    set suite variable    ${suite_gender}    ${gender}

    ${ethnicity}    set variable    Asian
    set suite variable    ${suite_ethnicity}    ${ethnicity}

    ${nationality}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_nationality}    ${nationality}

    ${occupation}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_occupation}    ${occupation}

    ${occupation_title}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_occupation_title}    ${occupation_title}

    ${township_code}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_township_code}    ${township_code}

    ${township_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_township_name}    ${township_name}

    ${national_id_number}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_national_id_number}    ${national_id_number}

    ${mother_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_mother_name}    ${mother_name}

    ${email}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_email}    ${email}@gmail.com

    ${primary_mobile_number}=   generate random string    10      123456789
    set suite variable    ${suite_primary_mobile_number}    ${primary_mobile_number}

    ${secondary_mobile_number}=   generate random string    10      123456789
    set suite variable    ${suite_secondary_mobile_number}    ${secondary_mobile_number}

    ${tertiary_mobile_number}=   generate random string    10      123456789
    set suite variable    ${suite_tertiary_mobile_number}    ${tertiary_mobile_number}

    ${current_address_citizen_association}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_citizen_association}    ${current_address_citizen_association}

    ${current_address_neighbourhood_association}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_neighbourhood_association}    ${current_address_neighbourhood_association}

    ${current_address_address}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_address}    ${current_address_address}

    ${current_address_commune}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_commune}    ${current_address_commune}

    ${current_address_district}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_district}    ${current_address_district}

    ${current_address_city}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_city}    ${current_address_city}

    ${current_address_province}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_province}    ${current_address_province}

    ${current_address_postal_code}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_postal_code}    ${current_address_postal_code}

    ${current_address_country}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_country}    ${current_address_country}

    ${current_address_landmark}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_landmark}    ${current_address_landmark}

    ${current_address_longitude}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_longitude}    ${current_address_longitude}

    ${current_address_latitude}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_latitude}    ${current_address_latitude}

    ${permanent_address_citizen_association}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_citizen_association}    ${permanent_address_citizen_association}

    ${permanent_address_neighbourhood_association}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_neighbourhood_association}    ${permanent_address_neighbourhood_association}

    ${permanent_address_address}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_address}    ${permanent_address_address}

    ${permanent_address_commune}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_commune}    ${permanent_address_commune}

    ${permanent_address_district}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_district}    ${permanent_address_district}

    ${permanent_address_province}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_province}    ${permanent_address_province}

    ${permanent_address_postal_code}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_postal_code}    ${permanent_address_postal_code}

    ${permanent_address_country}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_country}    ${permanent_address_country}

    ${permanent_address_landmark}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_landmark}    ${permanent_address_landmark}

    ${permanent_address_longitude}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_longitude}    ${permanent_address_longitude}

    ${permanent_address_latitude}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_latitude}    ${permanent_address_latitude}

    ${bank_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_bank_name}    ${bank_name}

    ${bank_account_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_bank_account_name}    ${bank_account_name}

    ${bank_account_name_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_bank_account_name_local}    ${bank_account_name_local}

    ${bank_account_number}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_bank_account_number}    ${bank_account_number}

    ${bank_account_number_local}=   generate random string    10      [NUMBERS]
    set suite variable    ${suite_bank_account_number_local}    ${bank_account_number_local}

    ${bank_branch_area}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_bank_branch_area}    ${bank_branch_area}

    ${bank_branch_area_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_bank_branch_area_local}    ${bank_branch_area_local}

    ${bank_branch_city}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_bank_branch_city}    ${bank_branch_city}

    ${bank_branch_city_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_bank_branch_city_local}    ${bank_branch_city_local}

    ${bank_register_source}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_bank_register_source}    ${bank_register_source}

    ${contract_type}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_contract_type}    ${contract_type}

    ${contract_number}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_contract_number}    ${contract_number}

    ${contract_extension_type}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_contract_extension_type}    ${contract_extension_type}

    ${contract_notification_alert}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_contract_notification_alert}    ${contract_notification_alert}

    ${contract_day_of_period_reconciliation}=   generate random string    2      123456789
    set suite variable    ${suite_contract_day_of_period_reconciliation}    ${contract_day_of_period_reconciliation}

    ${contract_release}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_contract_release}    ${contract_release}

    ${contract_file_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_contract_file_url}    ${contract_file_url}

    ${contract_assessment_information_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_contract_assessment_information_url}    ${contract_assessment_information_url}

    ${primary_identity_type}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_primary_identity_type}    ${primary_identity_type}

    ${primary_identity_identity_id}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_primary_identity_identity_id}    ${primary_identity_identity_id}

    ${primary_identity_place_of_issue}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_primary_identity_place_of_issue}    ${primary_identity_place_of_issue}

    ${primary_identity_front_identity_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_primary_identity_front_identity_url}    ${primary_identity_front_identity_url}

    ${primary_identity_back_identity_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_primary_identity_back_identity_url}    ${primary_identity_back_identity_url}

    ${primary_identity_identity_id_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_primary_identity_identity_id_local}    ${primary_identity_identity_id_local}

    ${secondary_identity_type}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_secondary_identity_type}    ${secondary_identity_type}

    ${secondary_identity_identity_id}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_secondary_identity_identity_id}    ${secondary_identity_identity_id}

    ${secondary_identity_place_of_issue}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_secondary_identity_place_of_issue}    ${secondary_identity_place_of_issue}

    ${secondary_identity_front_identity_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_secondary_identity_front_identity_url}    ${secondary_identity_front_identity_url}

    ${secondary_identity_back_identity_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_secondary_identity_back_identity_url}    ${secondary_identity_back_identity_url}

    ${secondary_identity_identity_id_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_secondary_identity_identity_id_local}    ${secondary_identity_identity_id_local}

    ${remark}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_remark}    ${remark}

    ${verify_by}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_verify_by}    ${verify_by}

    ${acquiring_sale_executive_id}=   generate random string    10      123456789
    set suite variable    ${suite_acquiring_sale_executive_id}    ${acquiring_sale_executive_id}

    ${acquiring_sale_executive_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_acquiring_sale_executive_name}    ${acquiring_sale_executive_name}

    ${relationship_manager_id}=   generate random string    10      123456789
    set suite variable    ${suite_relationship_manager_id}    ${relationship_manager_id}

    ${relationship_manager_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_relationship_manager_name}    ${relationship_manager_name}

    ${sale_region}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_sale_region}    ${sale_region}

    ${commercial_account_manager}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_commercial_account_manager}    ${commercial_account_manager}

    ${profile_picture_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_profile_picture_url}    ${profile_picture_url}

    ${national_id_photo_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_national_id_photo_url}    ${national_id_photo_url}

    ${tax_id_card_photo_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_tax_id_card_photo_url}    ${tax_id_card_photo_url}

    ${field_1_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_field_1_name}    ${field_1_name}

    ${field_1_value}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_field_1_value}    ${field_1_value}

    ${field_2_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_field_2_name}    ${field_2_name}

    ${field_2_value}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_field_2_value}    ${field_2_value}

    ${field_3_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_field_3_name}    ${field_3_name}

    ${field_3_value}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_field_3_value}    ${field_3_value}

    ${field_4_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_field_4_name}    ${field_4_name}

    ${field_4_value}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_field_4_value}    ${field_4_value}

    ${field_5_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_field_5_name}    ${field_5_name}

    ${field_5_value}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_field_5_value}    ${field_5_value}

    ${supporting_file_1_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_supporting_file_1_url}    ${supporting_file_1_url}

    ${supporting_file_2_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_supporting_file_2_url}    ${supporting_file_2_url}

    ${supporting_file_3_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_supporting_file_3_url}    ${supporting_file_3_url}

    ${supporting_file_4_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_supporting_file_4_url}    ${supporting_file_4_url}

    ${supporting_file_5_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_supporting_file_5_url}    ${supporting_file_5_url}

    ${tin_number_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_tin_number_local}    ${tin_number_local}

    ${sale_username}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_sale_username}    ${sale_username}

    ${sale_password}      set variable   	${setup_agent_password_encrypted}
    ${sale_password_login}     set variable    ${setup_agent_password_encrypted_utf8}

    ${tin_number_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_tin_number_local}    ${tin_number_local}

    ${first_name_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_first_name_local}    ${first_name_local}

    ${middle_name_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_middle_name_local}    ${middle_name_local}

    ${last_name_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_last_name_local}    ${last_name_local}

    ${suffix_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_suffix_local}    ${suffix_local}

    ${place_of_birth_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_place_of_birth_local}    ${place_of_birth_local}

    ${gender_local}=   generate random string    1      MF
    set suite variable    ${suite_gender_local}    ${gender_local}

    ${occupation_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_occupation_local}    ${occupation_local}

    ${occupation_title_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_occupation_title_local}    ${occupation_title_local}

    ${township_name_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_township_name_local}    ${township_name_local}

    ${national_id_number_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_national_id_number_local}    ${national_id_number_local}

    ${mother_name_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_mother_name_local}    ${mother_name_local}

    ${current_address_address_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_address_local}    ${current_address_address_local}

    ${current_address_commune_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_commune_local}    ${current_address_commune_local}

    ${current_address_district_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_district_local}    ${current_address_district_local}

    ${current_address_city_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_city_local}    ${current_address_city_local}

    ${current_address_province_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_province_local}    ${current_address_province_local}

    ${current_address_postal_code_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_postal_code_local}    ${current_address_postal_code_local}

    ${current_address_country_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_country_local}    ${current_address_country_local}

    ${permanent_address_address_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_address_local}    ${permanent_address_address_local}

    ${permanent_address_commune_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_commune_local}    ${permanent_address_commune_local}

    ${permanent_address_district_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_district_local}    ${permanent_address_district_local}

    ${permanent_address_city}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_city}    ${permanent_address_city}

    ${permanent_address_city_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_city_local}    ${permanent_address_city_local}

    ${permanent_address_province_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_province_local}    ${permanent_address_province_local}

    ${permanent_address_postal_code_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_postal_code_local}    ${permanent_address_postal_code_local}

    ${permanent_address_country_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_country_local}    ${permanent_address_country_local}

    ${bank_name_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_bank_name_local}    ${bank_name_local}

    ${acquiring_sale_executive_name_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_acquiring_sale_executive_name_local}    ${acquiring_sale_executive_name_local}

    ${relationship_manager_name_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_relationship_manager_name_local}    ${relationship_manager_name_local}

    ${sale_region_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_sale_region_local}    ${sale_region_local}

    ${commercial_account_manager_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_commercial_account_manager_local}    ${commercial_account_manager_local}

    ${star_rating}=   generate random string    1      12345
    set suite variable    ${suite_star_rating}    ${star_rating}

    ${warning_count}=   generate random string    3      123456789
    set suite variable    ${suite_warning_count}    ${warning_count}

    ${employee_id}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_employee_id}    ${employee_id}

    ${calendar_id}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_calendar_id}    ${calendar_id}

    ${unique_reference}=    generate random string    10      [LETTERS]
    set suite variable    ${suite_unique_reference}    ${unique_reference}

    ${referrer_user_id}=    generate random string    8    123456789
    set suite variable    ${suite_referrer_user_id}    ${referrer_user_id}

    ${acquisition_source}   generate random string  10    [LETTERS]
    set suite variable    ${suite_acquisition_source}       ${acquisition_source}

    ${current_date}=     Get Current Date    result_format=%Y-%m-%dT00:00:00Z

    ${arg_dic}      create dictionary    access_token=${suite_admin_access_token}
                    # Identity
                    ...     identity_type_id=${suite_user_name_type_id}
                    ...     username=${suite_sale_username}
                    ...     password=${setup_agent_password_encrypted}
                    ...     auto_generate_password=true
                    # Profile
                    ...     is_testing_account=true
                    ...     is_system_account=true
                    ...     acquisition_source=${suite_acquisition_source}
                    ...     referrer_user_id=${suite_referrer_user_id}
                    ...     referrer_user_type_id=${suite_referrer_user_type_id}
                    ...     referrer_user_type_name=${suite_referrer_user_type_name}
                    ...     agent_type_id=${suite_sale_type_id}
                    ...     currency=${suite_currency}
                    ...     unique_reference=${suite_unique_reference}
                    ...     mm_card_type_id=${suite_mm_card_type_id}
                    ...     mm_card_level_id=${suite_mm_card_level_id}
                    ...     mm_factory_card_number=${suite_mm_factory_card_number}
                    ...     model_type=${suite_model_type}
                    ...     is_require_otp=true
                    ...     agent_classification_id=${suite_agent_classification_id}
                    ...     tin_number=${suite_tin_number}
                    ...     tin_number_local=${suite_tin_number_local}
                    ...     title=${suite_title}
                    ...     title_local=${suite_title_local}
                    ...     first_name=${suite_first_name}
                    ...     first_name_local=${suite_first_name_local}
                    ...     middle_name=${suite_middle_name}
                    ...     middle_name_local=${suite_middle_name_local}
                    ...     last_name=${suite_last_name}
                    ...     last_name_local=${suite_last_name_local}
                    ...     suffix=${suite_suffix}
                    ...     suffix_local=${suite_suffix_local}
                    ...     date_of_birth=${suite_date_of_birth}
                    ...     place_of_birth=${suite_place_of_birth}
                    ...     place_of_birth_local=${suite_place_of_birth_local}
                    ...     gender=${suite_gender}
                    ...     gender_local=${suite_gender_local}
                    ...     ethnicity=${suite_ethnicity}
                    ...     nationality=${suite_nationality}
                    ...     occupation=${suite_occupation}
                    ...     occupation_local=${suite_acquisition_source}
                    ...     occupation_title=${suite_occupation_title}
                    ...     occupation_title_local=${suite_occupation_title_local}
                    ...     township_code=${suite_township_code}
                    ...     township_name=${suite_township_name}
                    ...     township_name_local=${suite_township_name_local}
                    ...     national_id_number=${suite_national_id_number}
                    ...     national_id_number_local=${suite_national_id_number_local}
                    ...     mother_name=${suite_mother_name}
                    ...     mother_name_local=${suite_mother_name_local}
                    ...     email=${suite_email}
                    ...     primary_mobile_number=${suite_primary_mobile_number}
                    ...     secondary_mobile_number=${suite_secondary_mobile_number}
                    ...     tertiary_mobile_number=${suite_tertiary_mobile_number}
                    ...     star_rating=${suite_star_rating}
                    ...     warning_count=${suite_warning_count}
                    ...     is_sale=true
                    # Address
                    ...     current_address_citizen_association=${suite_current_address_citizen_association}
                    ...     current_address_neighbourhood_association=${suite_current_address_neighbourhood_association}
                    ...     current_address_address=${suite_current_address_address}
                    ...     current_address_address_local=${suite_current_address_address_local}
                    ...     current_address_commune=${suite_current_address_commune}
                    ...     current_address_commune_local=${suite_current_address_commune_local}
                    ...     current_address_district=${suite_current_address_district}
                    ...     current_address_district_local=${suite_current_address_district_local}
                    ...     current_address_city=${suite_current_address_city}
                    ...     current_address_city_local=${suite_current_address_city_local}
                    ...     current_address_province=${suite_current_address_province}
                    ...     current_address_province_local=${suite_current_address_province_local}
                    ...     current_address_postal_code=${suite_current_address_postal_code}
                    ...     current_address_postal_code_local=${suite_current_address_postal_code_local}
                    ...     current_address_country=${suite_current_address_country}
                    ...     current_address_country_local=${suite_current_address_country_local}
                    ...     current_address_landmark=${suite_current_address_landmark}
                    ...     current_address_longitude=${suite_current_address_longitude}
                    ...     current_address_latitude=${suite_current_address_latitude}
                    ...     permanent_address_citizen_association=${suite_permanent_address_citizen_association}
                    ...     permanent_address_neighbourhood_association=${suite_permanent_address_neighbourhood_association}
                    ...     permanent_address_address=${suite_permanent_address_address}
                    ...     permanent_address_address_local=${suite_permanent_address_address_local}
                    ...     permanent_address_commune=${suite_permanent_address_commune}
                    ...     permanent_address_commune_local=${suite_permanent_address_commune_local}
                    ...     permanent_address_district=${suite_permanent_address_district}
                    ...     permanent_address_district_local=${suite_permanent_address_district_local}
                    ...     permanent_address_city=${suite_permanent_address_city}
                    ...     permanent_address_city_local=${suite_permanent_address_city_local}
                    ...     permanent_address_province=${suite_permanent_address_province}
                    ...     permanent_address_province_local=${suite_permanent_address_province_local}
                    ...     permanent_address_postal_code=${suite_permanent_address_postal_code}
                    ...     permanent_address_postal_code_local=${suite_permanent_address_postal_code_local}
                    ...     permanent_address_country=${suite_permanent_address_country}
                    ...     permanent_address_country_local=${suite_permanent_address_country_local}
                    ...     permanent_address_landmark=${suite_permanent_address_landmark}
                    ...     permanent_address_longitude=${suite_permanent_address_longitude}
                    ...     permanent_address_latitude=${suite_permanent_address_latitude}
                    # Company
                    ...     company_id=${suite_company_id}
                    # Bank
                    ...     bank_name=${suite_bank_name}
                    ...     bank_name_local=${suite_bank_name_local}
                    ...     bank_account_status=${suite_bank_account_status}
                    ...     bank_account_name=${suite_bank_account_name}
                    ...     bank_account_name_local=${suite_bank_account_name_local}
                    ...     bank_account_number=${suite_bank_account_number}
                    ...     bank_account_number_local=${suite_bank_account_number_local}
                    ...     bank_branch_area=${suite_bank_branch_area}
                    ...     bank_branch_area_local=${suite_bank_branch_area_local}
                    ...     bank_branch_city=${suite_bank_branch_city}
                    ...     bank_branch_city_local=${suite_bank_branch_city_local}
                    ...     bank_register_date=${suite_register_date}
                    ...     bank_register_source=${suite_bank_register_source}
                    ...     is_verified=true
                    ...     bank_end_date=${suite_end_date}
                    # Contract
                    ...     contract_release=${suite_contract_release}
                    ...     contract_type=${suite_contract_type}
                    ...     contract_number=${suite_contract_number}
                    ...     contract_extension_type=${suite_contract_extension_type}
                    ...     contract_sign_date=${suite_contract_sign_date}
                    ...     contract_issue_date=${suite_contract_issue_date}
                    ...     contract_expired_date=${suite_contract_expiry_date}
                    ...     contract_notification_alert=${suite_contract_notification_alert}
                    ...     contract_day_of_period_reconciliation=${suite_contract_day_of_period_reconciliation}
                    ...     contract_file_url=${suite_contract_file_url}
                    ...     contract_assessment_information_url=${suite_contract_assessment_information_url}
                    # Accreditation
                    ...     primary_identity_type=${suite_primary_identity_type}
                    ...     primary_identity_status=${suite_primary_identity_status}
                    ...     primary_identity_identity_id=${suite_primary_identity_identity_id}
                    ...     primary_identity_identity_id_local=${suite_primary_identity_identity_id_local}
                    ...     primary_identity_place_of_issue=${suite_primary_identity_place_of_issue}
                    ...     primary_identity_issue_date=${suite_primary_issue_date}
                    ...     primary_identity_expired_date=${suite_primary_expiry_date}
                    ...     primary_identity_front_identity_url=${suite_primary_identity_front_identity_url}
                    ...     primary_identity_back_identity_url=${suite_primary_identity_back_identity_url}
                    ...     secondary_identity_type=${suite_secondary_identity_type}
                    ...     secondary_identity_status=${suite_secondary_identity_status}
                    ...     secondary_identity_identity_id=${suite_secondary_identity_identity_id}
                    ...     secondary_identity_identity_id_local=${suite_secondary_identity_identity_id_local}
                    ...     secondary_identity_place_of_issue=${suite_secondary_identity_place_of_issue}
                    ...     secondary_identity_issue_date=${suite_secondary_issue_date}
                    ...     secondary_identity_expired_date=${suite_secondary_expiry_date}
                    ...     secondary_identity_front_identity_url=${suite_secondary_identity_front_identity_url}
                    ...     secondary_identity_back_identity_url=${suite_secondary_identity_back_identity_url}
                    ...     status_id=${suite_acceditation_status_id}
                    ...     verify_by=${suite_verify_by}
                    ...     remark=${suite_remark}
                    ...     risk_level=${suite_risk_level}
                    # Sale
                    ...     employee_id=${suite_employee_id}
                    ...     calendar_id=${suite_calendar_id}
                    # Additional
                    ...     acquiring_sale_executive_name=${suite_acquiring_sale_executive_name}
                    ...     acquiring_sale_executive_name_local=${suite_acquiring_sale_executive_name_local}
                    ...     acquiring_sale_executive_id=${suite_acquiring_sale_executive_id}
                    ...     relationship_manager_name=${suite_relationship_manager_name}
                    ...     relationship_manager_name_local=${suite_relationship_manager_name_local}
                    ...     relationship_manager_id=${suite_relationship_manager_id}
                    ...     sale_region=${suite_sale_region}
                    ...     sale_region_local=${suite_sale_region_local}
                    ...     commercial_account_manager=${suite_commercial_account_manager}
                    ...     commercial_account_manager_local=${suite_commercial_account_manager_local}
                    ...     profile_picture_url=${suite_profile_picture_url}
                    ...     national_id_photo_url=${suite_national_id_photo_url}
                    ...     tax_id_card_photo_url=${suite_tax_id_card_photo_url}
                    ...     field_1_name=${suite_field_1_name}
                    ...     field_1_value=${suite_field_1_value}
                    ...     field_2_name=${suite_field_2_name}
                    ...     field_2_value=${suite_field_2_value}
                    ...     field_3_name=${suite_field_3_name}
                    ...     field_3_value=${suite_field_3_value}
                    ...     field_4_name=${suite_field_4_name}
                    ...     field_4_value=${suite_field_4_value}
                    ...     field_5_name=${suite_field_5_name}
                    ...     field_5_value=${suite_field_5_value}
                    ...     supporting_file_1_url=${suite_supporting_file_1_url}
                    ...     supporting_file_2_url=${suite_supporting_file_2_url}
                    ...     supporting_file_3_url=${suite_supporting_file_3_url}
                    ...     supporting_file_4_url=${suite_supporting_file_4_url}
                    ...     supporting_file_5_url=${suite_supporting_file_5_url}
    ${dic}    [200] API create sale      ${arg_dic}
    set suite variable      ${suite_sale_id}       ${dic.id}

    # Create sofs cash link with sale
    ${dic}      [200] API system user create user sof cash
    ...     ${suite_sale_id}
    ...     2
    ...     ${suite_currency}
    ...     ${suite_admin_access_token}

[Agent][Suite][200] - create agent B in "${currency}" currency
    ${text}=   generate random string    10      [LETTERS]
    ${agent_type_name}      set variable   	${text}
    ${agent_type_id}      set variable   	${setup_company_agent_type_id}
    ${agent_unique_reference}      set variable   	${text}
    ${agent_username}     set variable    ${text}
    ${agent_password}      set variable   	${setup_agent_password_encrypted}
    ${agent_password_login}     set variable    ${setup_agent_password_encrypted_utf8}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      agent_type_name=${agent_type_name}
    ...     unique_reference=${agent_unique_reference}      identity_identity_type_id=1
    ...     agent_username=${agent_username}      agent_password=${agent_password}
    ...     agent_password_login=${agent_password_login}      currency=${currency}
    ...     user_type_id=2

    ${dic}  [Agent][Reuse][200] - create agent      ${arg_dic}
    set suite variable      ${suite_agent_b_id}       ${dic.agent_id}
    set suite variable      ${suite_agent_b_type}       agent
    set suite variable      ${suite_agent_b_sof_cash_id}       ${dic.sof_cash_id_in_${currency}}
    ${arg_dic}      create dictionary       username=${agent_username}      password=${agent_password_login}
    ${dic}          [200] API authenticate agent        ${arg_dic}
    set suite variable      ${suite_agent_b_access_token}     ${dic.access_token}

[Suite][200] company agent fund in for agent B
    ${text}=     generate random string      15      [LETTERS]
    ${arg_dic}      create dictionary   access_token=${suite_company_agent_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_service_id}      product_service_name=${suite_service_name}      product_name=${text}
    ...     product_ref_1=${text}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}     product_ref_3=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_company_agent_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_agent_B_id}       payee_user_type=${suite_agent_B_type}
    ...     amount=500000
    [Payment][Reuse][200] - create and execute normal order    ${arg_dic}
[Agent][Test][200] - System user delete device via api
    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     device_id=${test_edc_device_id}
    ${dic}    [200] System user delete device via api     ${arg_dic}

[Test][200] System user get agent device
    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     device_id=${test_edc_device_id}
    ${dic}   [200] API system user gets agent device     ${arg_dic}

    set test variable       ${test_edc_device_management}       ${dic.id}

[Agent][Test][200] - Inactive created device
    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     device_id=${test_edc_device_id}
    ${dic}   [200] API Inactive created device     ${arg_dic}

[Test][200] Create funded agent which has PASSWORD identity, one shop
    [Prepare] - Prepare service structure from service name
    [Test] Set test service group id
    [Channel_Adapter][Test][200] - Create an order configuration for transaction detail
    [Channel_Adapter][Test][200] - Create order config detail
    [Test][200] Create a sof_cash and fund in
    set test variable   ${test_agent_username}   ${test_username}
    [Test][200] API create agent shop
    ...     ${test_agent_id}

[Test][Fail] get smart card by unexisted serial number via api
    ${test_card_number}=   generate random string    10      [LETTERS]
    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     serial_number=${test_card_number}
    ...     code=400
    ${dic}  [Fail] API get smart card by serial number    ${arg_dic}
    set test variable       ${test_result_dic}       ${dic}

[Test][Fail] get smart card by deleted serial number via api
    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     serial_number=${test_serial_number}
    ...     code=400
    ${dic}  [Fail] API get smart card by serial number    ${arg_dic}
    set test variable       ${test_result_dic}       ${dic}

[Test][200] get smart card by serial number via api
    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     serial_number=${test_serial_number}
    ${dic}  [200] API get smart card by serial number    ${arg_dic}
    set test variable       ${test_result_dic}       ${dic}

[Test][200] delete smart card via api
    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     agent_id=${suite_agent_id}
    ...     smart_card_id=${test_smart_card_id}
    [200] API delete smart card  ${arg_dic}


[Test][200] delete smart card by id via api
    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     smart_card_id=${test_smart_card_id}
    [200] API delete smart card by id  ${arg_dic}

[Prepare] - [200] Create agent is sale
    ${unique_reference}     generate random string  12    [NUMBERS]

    ${arg_dic}      create dictionary
    ...         access_token=${test_admin_access_token}
    ...         agent_type_id=1
    ...         unique_reference=${unique_reference}
    ...         identity_type_id=1
    ...         username=${unique_reference}
    ...         is_sale=true
    ...         password=${setup_agent_password_encrypted}

    ${dic}      [200] API create agent      ${arg_dic}
    ${agent_id}     get from dictionary   ${dic}   id
    set test variable   ${test_agent_id}        ${agent_id}
    set test variable   ${test_agent_username}       ${unique_reference}
    set test variable   ${test_agent_password_login}        ${setup_agent_password_encrypted_utf8}

[Test][200][Prepare] - create shop type
    ${shop_type_name}=              generate random string    10      [LETTERS]
    ${shop_type_description}=       generate random string    20      [LETTERS]
    set test variable               ${test_shop_type_name}               robot_${shop_type_name}
    set test variable               ${test_shop_type_description}        ${shop_type_description}
    ${dic}      [200] API create shop type   ${suite_admin_access_token}    ${test_shop_type_name}     ${test_shop_type_description}
    ${get_shop_type_id}     get from dictionary     ${dic}     id
    set test variable       ${test_shop_type_id}         ${get_shop_type_id}

[Test][200] API create agent shop with dicionary params
    ${shop_name}=  generate random string    10      [LETTERS]
    ${address}=     generate random string    70      [LETTERS]

    ${arg_dic}  create dictionary
    ...     access_token=${test_admin_access_token}
    ...     shop_type_id=${test_shop_type_id}
    ...     agent_id=${test_agent_id}
    ...     name=${shop_name}
    ...     address=${address}
    ${dic}  [200] API create shop with dictionary params   ${arg_dic}
    ${id}   get from dictionary       ${dic}    id
    set test variable        ${test_shop_id}     ${id}

[Test][200] call suspend agent api by id
    [Arguments]  ${agent_id_param}
    [200] API active-suspend agent   ${suite_admin_access_token}    true    ${agent_id_param}

[Test][200] call active agent api by id
    [Arguments]  ${agent_id_param}
    [200] API active-suspend agent   ${suite_admin_access_token}    false    ${agent_id_param}

[Agent][Suite][200] - Create agent classification custom
    [Arguments]    ${access_token}
    [Agent][Suite][200] - Create agent classification       ${access_token}
    set suite variable     ${suite_agent_classification_name_custom}     ${suite_agent_classification_name}

[Agent][Suite][200] - Create agent classification
    [Arguments]    ${access_token}
    ${agent_classification_name}=   generate random string    10      [LETTERS]
    ${arg_dic}    create dictionary
    ...     access_token=${access_token}
    ...     name=${agent_classification_name}
    ${dic}  [200] API create agent classification    ${arg_dic}
    set suite variable     ${suite_agent_classification_id}     ${dic.id}
    set suite variable     ${suite_agent_classification_name}     ${agent_classification_name}

[Agent][Suite][200] - Admin updates agent profiles
    [Arguments]     ${access_token}     ${agent_id}     ${agent_type_id}    ${agent_classification_id}    ${unique_reference}
    ${dic}  create dictionary   access_token=${access_token}    agent_id=${agent_id}  agent_type_id=${agent_type_id}  agent_classification_id=${agent_classification_id}    unique_reference=${unique_reference}
    [200] API admin updates agent profiles      ${dic}

[Test][200] Create funded agent which has PASSWORD identity, 2 shop, 3 devices, 3 currencies, mobile profile
    [Prepare] - Prepare service structure from service name
    [Test] Set test service group id
    [Channel_Adapter][Test][200] - Create an order configuration for transaction detail
    [Channel_Adapter][Test][200] - Create order config detail
    ${agent_type_name}   generate random string  10    [LETTERS]
    set test variable       ${test_agent_type_name}     ${agent_type_name}
    ${dic}      [200] API create agent type     ${suite_admin_access_token}     ${test_agent_type_name}
    ${agent_type_id}     get from dictionary      ${dic}     id
    set test variable    ${test_agent_type_id}       ${agent_type_id}
    ${unique_reference}   generate random string  10    [NUMBERS]
    set test variable    ${test_unique_reference}       ${unique_reference}
    ${username}   generate random string  10    [NUMBERS]
    set test variable    ${test_username}       ${username}
    ${first_name}=   generate random string    10      [LETTERS]
    ${middle_name}=   generate random string    10      [LETTERS]
    ${last_name}=   generate random string    10      [LETTERS]
    ${primary_mobile_number}=   generate random string    11      [NUMBERS]
    ${email}=    set variable      ${first_name}@email.com
    ${national_id}=  generate random string    10      [NUMBERS]

    ${arg_dic}     create dictionary
    ...         access_token=${suite_admin_access_token}
    ...         agent_type_id=${test_agent_type_id}
    ...         unique_reference=${test_unique_reference}
    ...         username=${test_username}
    ...         password=${setup_agent_password_encrypted}
    ...         identity_type_id=1
    ...         first_name=${first_name}
    ...         middle_name=${middle_name}
    ...         last_name=${last_name}
    ...         primary_mobile_number=${primary_mobile_number}
    ...         email=${email}
    ...         national_id_number=${national_id}
    ${dic}      [200] API create agent      ${arg_dic}
    ${agent_id}     get from dictionary      ${dic}     id
    set test variable    ${test_agent_id}       ${agent_id}
    set test variable    ${test_agent_username}    ${test_username}
    ${arg_dic}   create dictionary      username=${test_username}      password=${setup_agent_password_encrypted_encoded}
    ${dic}    [200] API agent authentication        ${arg_dic}
    ${access_token}     get from dictionary      ${dic}     access_token
    set test variable    ${test_agent_access_token}       ${access_token}

    ${dic}  [200] API create user sof cash          VND       ${test_agent_access_token}
    ${dic}  [200] API create user sof cash          THB       ${test_agent_access_token}
    ${dic}  [200] API create user sof cash          USD       ${test_agent_access_token}
    ${sof_id}       get from dictionary     ${dic}      sof_id
    set suite variable    ${test_agent_sof_id_VN}      ${sof_id}
    set suite variable    ${suite_agent_sof_id_VN}      ${sof_id}
    [Test][200] Company Agent fund in "10000000" money for "agent": "${test_agent_id}"
    set test variable   ${test_agent_username}   ${test_username}
    [Test][200] API create agent shop
    ...     ${test_agent_id}
    [Test][200] API create agent shop
    ...     ${test_agent_id}
    ${device_unique_reference_1}     generate random string  12    [LETTERS]
    ${device_unique_reference_2}     generate random string  12    [LETTERS]
    ${device_unique_reference_3}     generate random string  12    [LETTERS]
    ${device_model_1}     generate random string  12    [LETTERS]
    ${device_model_2}     generate random string  12    [LETTERS]
    ${device_model_3}     generate random string  12    [LETTERS]
    set suite variable   ${test_agent_password_login}      ${setup_agent_password_encrypted_utf8}
    [Agent][Test][200] - Authenticate agent
    [200] agent create device
    ...     ${test_agent_access_token}
    ...     ${device_unique_reference_1}
    ...     ${device_model_1}
    ...     ${test_shop_id}
    ...     1
    ...     2
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    [200] agent create device
    ...     ${test_agent_access_token}
    ...     ${device_unique_reference_2}
    ...     ${device_model_2}
    ...     ${test_shop_id}
    ...     1
    ...     2
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    [200] agent create device
    ...     ${test_agent_access_token}
    ...     ${device_unique_reference_3}
    ...     ${device_model_3}
    ...     ${test_shop_id}
    ...     1
    ...     2
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}

[Suite][200] Create full data for Agent
    ${current_date}    Get Current Date
    ${current_date}    Convert Date    ${current_date}     result_format=${datetime_format}
    set suite variable     ${suite_date_of_birth}     ${current_date}
    set suite variable     ${suite_register_date}     ${current_date}
    set suite variable     ${suite_end_date}     ${current_date}
    set suite variable     ${suite_contract_sign_date}     ${current_date}
    set suite variable     ${suite_contract_issue_date}     ${current_date}
    set suite variable     ${suite_contract_expiry_date}     ${current_date}
    set suite variable     ${suite_primary_issue_date}     ${current_date}
    set suite variable     ${suite_primary_expiry_date}     ${current_date}
    set suite variable     ${suite_secondary_issue_date}     ${current_date}
    set suite variable     ${suite_secondary_expiry_date}     ${current_date}
    # Set company id
    ${company_type_name}    generate random string  10  company_type_[LETTERS]
    ${arg_dic_company_type}      create dictionary   access_token=${suite_admin_access_token}      name=${company_type_name}
    ${dic_company_type}    [200] API create company type    ${arg_dic_company_type}
    ${company_type_id}     get from dictionary    ${dic_company_type}   id
    set suite variable       ${suite_company_type_name_variable}       ${company_type_name}
    set suite variable       ${suite_company_type_id_variable}       ${company_type_id}
    [Suite][200] create company profiles

    # Set agent Type
    ${agent_type_name}   generate random string  6    [LETTERS]
    set suite variable       ${suite_agent_type_name}     ${agent_type_name}
    ${dic_sale_type}      [200] API create agent type     ${suite_admin_access_token}     ${suite_agent_type_name}
    ${sale_type_id}     get from dictionary      ${dic_sale_type}     id
    set suite variable    ${suite_sale_type_id}       ${sale_type_id}
    set suite variable    ${suite_sale_type_name}       ${agent_type_name}

    # Set classification
    ${classification_name}=   generate random string    10      [LETTERS]
    ${classification_desc}=   generate random string    10      [LETTERS]
    ${arg_dic_classification}      create dictionary    name=${classification_name}    description=${classification_desc}    access_token=${suite_admin_access_token}
    ${dic_classification}    [200] API create agent classification    ${arg_dic_classification}
    ${agent_classification_id}    get from dictionary    ${dic_classification}    id
    set suite variable    ${suite_agent_classification_id}    ${agent_classification_id}
    set suite variable    ${suite_agent_classification_name}    ${classification_name}

    # Set Profile accreditation status
    ${dic_config}      [200] API get configuration detail        access_token=${suite_admin_access_token}        scopeName=global        key=country
    set suite variable      ${country_code}     ${dic_config.value}
    ${dic_accreditation}   create dictionary      access_token=${suite_admin_access_token}    country_code=${country_code}
    ${dic_acceditation_status}      [200] API search agent accreditation status     ${dic_accreditation}
    set suite variable   ${suite_acceditation_status_id}     ${dic_acceditation_status.first_creditation_status_id}
    set suite variable   ${suite_acceditation_status_status}     ${dic_acceditation_status.first_creditation_status_status}

    # Set status
    set suite variable    ${suite_status}    Active

    # Set user name type
    ${user_name_type}=    set variable    Free Text Username Alphanumeric
    set suite variable    ${suite_user_name_type}    ${user_name_type}
    set suite variable    ${suite_user_name_type_id}    1

    # Set referrer user type
    ${referrer_user_type_id}    set variable    1
    set suite variable    ${suite_referrer_user_type_id}    ${referrer_user_type_id}
    set suite variable    ${suite_referrer_user_type_name}    Customer

    # Set currency
    set suite variable    ${suite_currency}    VND

    # Set MM card type
    set suite variable    ${suite_mm_card_type_id}    1
    set suite variable    ${suite_mm_card_type_name}    Salesperson

    # Set MM card level
    set suite variable    ${suite_mm_card_level_id}    1
    set suite variable    ${suite_mm_card_level}    Level0

    # Set Model type
    set suite variable    ${suite_model_type}    1

    # Set bank status
    set suite variable    ${suite_bank_account_status}    1
    set suite variable    ${suite_bank_account_status_name}    Active

    # Set primary identity status
    set suite variable    ${suite_primary_identity_status}    1
    set suite variable    ${suite_primary_identity_status_name}    Active
    # Set secondary identity status
    set suite variable    ${suite_secondary_identity_status}    1
    set suite variable    ${suite_secondary_identity_status_name}    Active

    # Set risk level
    set suite variable    ${suite_risk_level}    High

    ${mm_factory_card_number}=   generate random string    5      123456789
    set suite variable    ${suite_mm_factory_card_number}    ${mm_factory_card_number}

    ${tin_number}=   generate random string    5      123456789
    set suite variable    ${suite_tin_number}    ${tin_number}

    ${title}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_title}    ${title}

    ${title_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_title_local}    ${title_local}

    ${first_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_first_name}    ${first_name}

    ${middle_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_middle_name}    ${middle_name}

    ${last_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_last_name}    ${last_name}

    ${suffix}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_suffix}    ${suffix}

    ${place_of_birth}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_place_of_birth}    ${place_of_birth}

    ${gender}=   generate random string    1      MF
    set suite variable    ${suite_gender}    ${gender}

    ${ethnicity}    set variable    Asian
    set suite variable    ${suite_ethnicity}    ${ethnicity}

    ${nationality}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_nationality}    ${nationality}

    ${occupation}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_occupation}    ${occupation}

    ${occupation_title}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_occupation_title}    ${occupation_title}

    ${township_code}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_township_code}    ${township_code}

    ${township_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_township_name}    ${township_name}

    ${national_id_number}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_national_id_number}    ${national_id_number}

    ${mother_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_mother_name}    ${mother_name}

    ${email}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_email}    ${email}@gmail.com

    ${primary_mobile_number}=   generate random string    10      123456789
    set suite variable    ${suite_primary_mobile_number}    ${primary_mobile_number}

    ${secondary_mobile_number}=   generate random string    10      123456789
    set suite variable    ${suite_secondary_mobile_number}    ${secondary_mobile_number}

    ${tertiary_mobile_number}=   generate random string    10      123456789
    set suite variable    ${suite_tertiary_mobile_number}    ${tertiary_mobile_number}

    ${current_address_citizen_association}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_citizen_association}    ${current_address_citizen_association}

    ${current_address_neighbourhood_association}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_neighbourhood_association}    ${current_address_neighbourhood_association}

    ${current_address_address}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_address}    ${current_address_address}

    ${current_address_commune}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_commune}    ${current_address_commune}

    ${current_address_district}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_district}    ${current_address_district}

    ${current_address_city}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_city}    ${current_address_city}

    ${current_address_province}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_province}    ${current_address_province}

    ${current_address_postal_code}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_postal_code}    ${current_address_postal_code}

    ${current_address_country}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_country}    ${current_address_country}

    ${current_address_landmark}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_landmark}    ${current_address_landmark}

    ${current_address_longitude}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_longitude}    ${current_address_longitude}

    ${current_address_latitude}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_latitude}    ${current_address_latitude}

    ${permanent_address_citizen_association}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_citizen_association}    ${permanent_address_citizen_association}

    ${permanent_address_neighbourhood_association}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_neighbourhood_association}    ${permanent_address_neighbourhood_association}

    ${permanent_address_address}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_address}    ${permanent_address_address}

    ${permanent_address_commune}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_commune}    ${permanent_address_commune}

    ${permanent_address_district}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_district}    ${permanent_address_district}

    ${permanent_address_province}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_province}    ${permanent_address_province}

    ${permanent_address_postal_code}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_postal_code}    ${permanent_address_postal_code}

    ${permanent_address_country}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_country}    ${permanent_address_country}

    ${permanent_address_landmark}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_landmark}    ${permanent_address_landmark}

    ${permanent_address_longitude}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_longitude}    ${permanent_address_longitude}

    ${permanent_address_latitude}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_latitude}    ${permanent_address_latitude}

    ${bank_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_bank_name}    ${bank_name}

    ${bank_account_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_bank_account_name}    ${bank_account_name}

    ${bank_account_name_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_bank_account_name_local}    ${bank_account_name_local}

    ${bank_account_number}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_bank_account_number}    ${bank_account_number}

    ${bank_account_number_local}=   generate random string    10      [NUMBERS]
    set suite variable    ${suite_bank_account_number_local}    ${bank_account_number_local}

    ${bank_branch_area}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_bank_branch_area}    ${bank_branch_area}

    ${bank_branch_area_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_bank_branch_area_local}    ${bank_branch_area_local}

    ${bank_branch_city}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_bank_branch_city}    ${bank_branch_city}

    ${bank_branch_city_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_bank_branch_city_local}    ${bank_branch_city_local}

    ${bank_register_source}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_bank_register_source}    ${bank_register_source}

    ${contract_type}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_contract_type}    ${contract_type}

    ${contract_number}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_contract_number}    ${contract_number}

    ${contract_extension_type}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_contract_extension_type}    ${contract_extension_type}

    ${contract_notification_alert}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_contract_notification_alert}    ${contract_notification_alert}

    ${contract_day_of_period_reconciliation}=   generate random string    2      123456789
    set suite variable    ${suite_contract_day_of_period_reconciliation}    ${contract_day_of_period_reconciliation}

    ${contract_release}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_contract_release}    ${contract_release}

    ${contract_file_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_contract_file_url}    ${contract_file_url}

    ${contract_assessment_information_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_contract_assessment_information_url}    ${contract_assessment_information_url}

    ${primary_identity_type}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_primary_identity_type}    ${primary_identity_type}

    ${primary_identity_identity_id}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_primary_identity_identity_id}    ${primary_identity_identity_id}

    ${primary_identity_place_of_issue}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_primary_identity_place_of_issue}    ${primary_identity_place_of_issue}

    ${primary_identity_front_identity_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_primary_identity_front_identity_url}    ${primary_identity_front_identity_url}

    ${primary_identity_back_identity_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_primary_identity_back_identity_url}    ${primary_identity_back_identity_url}

    ${primary_identity_identity_id_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_primary_identity_identity_id_local}    ${primary_identity_identity_id_local}

    ${secondary_identity_type}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_secondary_identity_type}    ${secondary_identity_type}

    ${secondary_identity_identity_id}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_secondary_identity_identity_id}    ${secondary_identity_identity_id}

    ${secondary_identity_place_of_issue}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_secondary_identity_place_of_issue}    ${secondary_identity_place_of_issue}

    ${secondary_identity_front_identity_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_secondary_identity_front_identity_url}    ${secondary_identity_front_identity_url}

    ${secondary_identity_back_identity_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_secondary_identity_back_identity_url}    ${secondary_identity_back_identity_url}

    ${secondary_identity_identity_id_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_secondary_identity_identity_id_local}    ${secondary_identity_identity_id_local}

    ${remark}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_remark}    ${remark}

    ${verify_by}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_verify_by}    ${verify_by}

    ${acquiring_sale_executive_id}=   generate random string    10      123456789
    set suite variable    ${suite_acquiring_sale_executive_id}    ${acquiring_sale_executive_id}

    ${acquiring_sale_executive_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_acquiring_sale_executive_name}    ${acquiring_sale_executive_name}

    ${relationship_manager_id}=   generate random string    10      123456789
    set suite variable    ${suite_relationship_manager_id}    ${relationship_manager_id}

    ${relationship_manager_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_relationship_manager_name}    ${relationship_manager_name}

    ${sale_region}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_sale_region}    ${sale_region}

    ${commercial_account_manager}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_commercial_account_manager}    ${commercial_account_manager}

    ${profile_picture_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_profile_picture_url}    ${profile_picture_url}

    ${national_id_photo_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_national_id_photo_url}    ${national_id_photo_url}

    ${tax_id_card_photo_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_tax_id_card_photo_url}    ${tax_id_card_photo_url}

    ${field_1_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_field_1_name}    ${field_1_name}

    ${field_1_value}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_field_1_value}    ${field_1_value}

    ${field_2_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_field_2_name}    ${field_2_name}

    ${field_2_value}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_field_2_value}    ${field_2_value}

    ${field_3_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_field_3_name}    ${field_3_name}

    ${field_3_value}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_field_3_value}    ${field_3_value}

    ${field_4_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_field_4_name}    ${field_4_name}

    ${field_4_value}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_field_4_value}    ${field_4_value}

    ${field_5_name}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_field_5_name}    ${field_5_name}

    ${field_5_value}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_field_5_value}    ${field_5_value}

    ${supporting_file_1_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_supporting_file_1_url}    ${supporting_file_1_url}

    ${supporting_file_2_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_supporting_file_2_url}    ${supporting_file_2_url}

    ${supporting_file_3_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_supporting_file_3_url}    ${supporting_file_3_url}

    ${supporting_file_4_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_supporting_file_4_url}    ${supporting_file_4_url}

    ${supporting_file_5_url}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_supporting_file_5_url}    ${supporting_file_5_url}

    ${tin_number_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_tin_number_local}    ${tin_number_local}

    ${sale_username}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_sale_username}    ${sale_username}

    ${sale_password}      set variable   	${setup_agent_password_encrypted}
    ${sale_password_login}     set variable    ${setup_agent_password_encrypted_utf8}

    ${tin_number_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_tin_number_local}    ${tin_number_local}

    ${first_name_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_first_name_local}    ${first_name_local}

    ${middle_name_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_middle_name_local}    ${middle_name_local}

    ${last_name_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_last_name_local}    ${last_name_local}

    ${suffix_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_suffix_local}    ${suffix_local}

    ${place_of_birth_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_place_of_birth_local}    ${place_of_birth_local}

    ${gender_local}=   generate random string    1      MF
    set suite variable    ${suite_gender_local}    ${gender_local}

    ${occupation_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_occupation_local}    ${occupation_local}

    ${occupation_title_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_occupation_title_local}    ${occupation_title_local}

    ${township_name_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_township_name_local}    ${township_name_local}

    ${national_id_number_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_national_id_number_local}    ${national_id_number_local}

    ${mother_name_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_mother_name_local}    ${mother_name_local}

    ${current_address_address_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_address_local}    ${current_address_address_local}

    ${current_address_commune_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_commune_local}    ${current_address_commune_local}

    ${current_address_district_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_district_local}    ${current_address_district_local}

    ${current_address_city_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_city_local}    ${current_address_city_local}

    ${current_address_province_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_province_local}    ${current_address_province_local}

    ${current_address_postal_code_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_postal_code_local}    ${current_address_postal_code_local}

    ${current_address_country_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_current_address_country_local}    ${current_address_country_local}

    ${permanent_address_address_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_address_local}    ${permanent_address_address_local}

    ${permanent_address_commune_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_commune_local}    ${permanent_address_commune_local}

    ${permanent_address_district_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_district_local}    ${permanent_address_district_local}

    ${permanent_address_city}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_city}    ${permanent_address_city}

    ${permanent_address_city_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_city_local}    ${permanent_address_city_local}

    ${permanent_address_province_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_province_local}    ${permanent_address_province_local}

    ${permanent_address_postal_code_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_postal_code_local}    ${permanent_address_postal_code_local}

    ${permanent_address_country_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_permanent_address_country_local}    ${permanent_address_country_local}

    ${bank_name_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_bank_name_local}    ${bank_name_local}

    ${acquiring_sale_executive_name_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_acquiring_sale_executive_name_local}    ${acquiring_sale_executive_name_local}

    ${relationship_manager_name_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_relationship_manager_name_local}    ${relationship_manager_name_local}

    ${sale_region_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_sale_region_local}    ${sale_region_local}

    ${commercial_account_manager_local}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_commercial_account_manager_local}    ${commercial_account_manager_local}

    ${star_rating}=   generate random string    1      12345
    set suite variable    ${suite_star_rating}    ${star_rating}

    ${warning_count}=   generate random string    3      123456789
    set suite variable    ${suite_warning_count}    ${warning_count}

    ${employee_id}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_employee_id}    ${employee_id}

    ${calendar_id}=   generate random string    10      [LETTERS]
    set suite variable    ${suite_calendar_id}    ${calendar_id}

    ${unique_reference}=    generate random string    10      [LETTERS]
    set suite variable    ${suite_unique_reference}    ${unique_reference}

    ${referrer_user_id}=    generate random string    8    123456789
    set suite variable    ${suite_referrer_user_id}    ${referrer_user_id}

    ${acquisition_source}   generate random string  10    [LETTERS]
    set suite variable    ${suite_acquisition_source}       ${acquisition_source}

    ${active_suspend_reason}    generate random string  10    [LETTERS]
    set suite variable      ${suite_active_suspend_reason}      ${active_suspend_reason}

    ${current_date}=     Get Current Date    result_format=%Y-%m-%dT00:00:00Z

    ${arg_dic}      create dictionary    access_token=${suite_admin_access_token}
                    # Identity
                    ...     identity_type_id=${suite_user_name_type_id}
                    ...     username=${suite_sale_username}
                    ...     password=${setup_agent_password_encrypted}
                    ...     auto_generate_password=true
                    # Profile
                    ...     is_testing_account=true
                    ...     is_system_account=true
                    ...     acquisition_source=${suite_acquisition_source}
                    ...     referrer_user_id=${suite_referrer_user_id}
                    ...     referrer_user_type_id=${suite_referrer_user_type_id}
                    ...     referrer_user_type_name=${suite_referrer_user_type_name}
                    ...     active_suspend_reason=${suite_active_suspend_reason}
                    ...     agent_type_id=${suite_sale_type_id}
                    ...     currency=${suite_currency}
                    ...     unique_reference=${suite_unique_reference}
                    ...     mm_card_type_id=${suite_mm_card_type_id}
                    ...     mm_card_level_id=${suite_mm_card_level_id}
                    ...     mm_factory_card_number=${suite_mm_factory_card_number}
                    ...     model_type=${suite_model_type}
                    ...     is_require_otp=true
                    ...     agent_classification_id=${suite_agent_classification_id}
                    ...     tin_number=${suite_tin_number}
                    ...     tin_number_local=${suite_tin_number_local}
                    ...     title=${suite_title}
                    ...     title_local=${suite_title_local}
                    ...     first_name=${suite_first_name}
                    ...     first_name_local=${suite_first_name_local}
                    ...     middle_name=${suite_middle_name}
                    ...     middle_name_local=${suite_middle_name_local}
                    ...     last_name=${suite_last_name}
                    ...     last_name_local=${suite_last_name_local}
                    ...     suffix=${suite_suffix}
                    ...     suffix_local=${suite_suffix_local}
                    ...     date_of_birth=${suite_date_of_birth}
                    ...     place_of_birth=${suite_place_of_birth}
                    ...     place_of_birth_local=${suite_place_of_birth_local}
                    ...     gender=${suite_gender}
                    ...     gender_local=${suite_gender_local}
                    ...     ethnicity=${suite_ethnicity}
                    ...     nationality=${suite_nationality}
                    ...     occupation=${suite_occupation}
                    ...     occupation_local=${suite_occupation_local}
                    ...     occupation_title=${suite_occupation_title}
                    ...     occupation_title_local=${suite_occupation_title_local}
                    ...     township_code=${suite_township_code}
                    ...     township_name=${suite_township_name}
                    ...     township_name_local=${suite_township_name_local}
                    ...     national_id_number=${suite_national_id_number}
                    ...     national_id_number_local=${suite_national_id_number_local}
                    ...     mother_name=${suite_mother_name}
                    ...     mother_name_local=${suite_mother_name_local}
                    ...     email=${suite_email}
                    ...     primary_mobile_number=${suite_primary_mobile_number}
                    ...     secondary_mobile_number=${suite_secondary_mobile_number}
                    ...     tertiary_mobile_number=${suite_tertiary_mobile_number}
                    ...     star_rating=${suite_star_rating}
                    ...     warning_count=${suite_warning_count}
                    ...     is_sale=false
                    # Address
                    ...     current_address_citizen_association=${suite_current_address_citizen_association}
                    ...     current_address_neighbourhood_association=${suite_current_address_neighbourhood_association}
                    ...     current_address_address=${suite_current_address_address}
                    ...     current_address_address_local=${suite_current_address_address_local}
                    ...     current_address_commune=${suite_current_address_commune}
                    ...     current_address_commune_local=${suite_current_address_commune_local}
                    ...     current_address_district=${suite_current_address_district}
                    ...     current_address_district_local=${suite_current_address_district_local}
                    ...     current_address_city=${suite_current_address_city}
                    ...     current_address_city_local=${suite_current_address_city_local}
                    ...     current_address_province=${suite_current_address_province}
                    ...     current_address_province_local=${suite_current_address_province_local}
                    ...     current_address_postal_code=${suite_current_address_postal_code}
                    ...     current_address_postal_code_local=${suite_current_address_postal_code_local}
                    ...     current_address_country=${suite_current_address_country}
                    ...     current_address_country_local=${suite_current_address_country_local}
                    ...     current_address_landmark=${suite_current_address_landmark}
                    ...     current_address_longitude=${suite_current_address_longitude}
                    ...     current_address_latitude=${suite_current_address_latitude}
                    ...     permanent_address_citizen_association=${suite_permanent_address_citizen_association}
                    ...     permanent_address_neighbourhood_association=${suite_permanent_address_neighbourhood_association}
                    ...     permanent_address_address=${suite_permanent_address_address}
                    ...     permanent_address_address_local=${suite_permanent_address_address_local}
                    ...     permanent_address_commune=${suite_permanent_address_commune}
                    ...     permanent_address_commune_local=${suite_permanent_address_commune_local}
                    ...     permanent_address_district=${suite_permanent_address_district}
                    ...     permanent_address_district_local=${suite_permanent_address_district_local}
                    ...     permanent_address_city=${suite_permanent_address_city}
                    ...     permanent_address_city_local=${suite_permanent_address_city_local}
                    ...     permanent_address_province=${suite_permanent_address_province}
                    ...     permanent_address_province_local=${suite_permanent_address_province_local}
                    ...     permanent_address_postal_code=${suite_permanent_address_postal_code}
                    ...     permanent_address_postal_code_local=${suite_permanent_address_postal_code_local}
                    ...     permanent_address_country=${suite_permanent_address_country}
                    ...     permanent_address_country_local=${suite_permanent_address_country_local}
                    ...     permanent_address_landmark=${suite_permanent_address_landmark}
                    ...     permanent_address_longitude=${suite_permanent_address_longitude}
                    ...     permanent_address_latitude=${suite_permanent_address_latitude}
                    # Company
                    ...     company_id=${suite_company_id}
                    # Bank
                    ...     bank_name=${suite_bank_name}
                    ...     bank_name_local=${suite_bank_name_local}
                    ...     bank_account_status=${suite_bank_account_status}
                    ...     bank_account_name=${suite_bank_account_name}
                    ...     bank_account_name_local=${suite_bank_account_name_local}
                    ...     bank_account_number=${suite_bank_account_number}
                    ...     bank_account_number_local=${suite_bank_account_number_local}
                    ...     bank_branch_area=${suite_bank_branch_area}
                    ...     bank_branch_area_local=${suite_bank_branch_area_local}
                    ...     bank_branch_city=${suite_bank_branch_city}
                    ...     bank_branch_city_local=${suite_bank_branch_city_local}
                    ...     bank_register_date=${suite_register_date}
                    ...     bank_register_source=${suite_bank_register_source}
                    ...     is_verified=true
                    ...     bank_end_date=${suite_end_date}
                    # Contract
                    ...     contract_release=${suite_contract_release}
                    ...     contract_type=${suite_contract_type}
                    ...     contract_number=${suite_contract_number}
                    ...     contract_extension_type=${suite_contract_extension_type}
                    ...     contract_sign_date=${suite_contract_sign_date}
                    ...     contract_issue_date=${suite_contract_issue_date}
                    ...     contract_expired_date=${suite_contract_expiry_date}
                    ...     contract_notification_alert=${suite_contract_notification_alert}
                    ...     contract_day_of_period_reconciliation=${suite_contract_day_of_period_reconciliation}
                    ...     contract_file_url=${suite_contract_file_url}
                    ...     contract_assessment_information_url=${suite_contract_assessment_information_url}
                    # Accreditation
                    ...     primary_identity_type=${suite_primary_identity_type}
                    ...     primary_identity_status=${suite_primary_identity_status}
                    ...     primary_identity_identity_id=${suite_primary_identity_identity_id}
                    ...     primary_identity_identity_id_local=${suite_primary_identity_identity_id_local}
                    ...     primary_identity_place_of_issue=${suite_primary_identity_place_of_issue}
                    ...     primary_identity_issue_date=${suite_primary_issue_date}
                    ...     primary_identity_expired_date=${suite_primary_expiry_date}
                    ...     primary_identity_front_identity_url=${suite_primary_identity_front_identity_url}
                    ...     primary_identity_back_identity_url=${suite_primary_identity_back_identity_url}
                    ...     secondary_identity_type=${suite_secondary_identity_type}
                    ...     secondary_identity_status=${suite_secondary_identity_status}
                    ...     secondary_identity_identity_id=${suite_secondary_identity_identity_id}
                    ...     secondary_identity_identity_id_local=${suite_secondary_identity_identity_id_local}
                    ...     secondary_identity_place_of_issue=${suite_secondary_identity_place_of_issue}
                    ...     secondary_identity_issue_date=${suite_secondary_issue_date}
                    ...     secondary_identity_expired_date=${suite_secondary_expiry_date}
                    ...     secondary_identity_front_identity_url=${suite_secondary_identity_front_identity_url}
                    ...     secondary_identity_back_identity_url=${suite_secondary_identity_back_identity_url}
                    ...     status_id=${suite_acceditation_status_id}
                    ...     verify_by=${suite_verify_by}
                    ...     remark=${suite_remark}
                    ...     risk_level=${suite_risk_level}
                    # Sale
                    ...     employee_id=${suite_employee_id}
                    ...     calendar_id=${suite_calendar_id}
                    # Additional
                    ...     acquiring_sale_executive_name=${suite_acquiring_sale_executive_name}
                    ...     acquiring_sale_executive_name_local=${suite_acquiring_sale_executive_name_local}
                    ...     acquiring_sale_executive_id=${suite_acquiring_sale_executive_id}
                    ...     relationship_manager_name=${suite_relationship_manager_name}
                    ...     relationship_manager_name_local=${suite_relationship_manager_name_local}
                    ...     relationship_manager_id=${suite_relationship_manager_id}
                    ...     sale_region=${suite_sale_region}
                    ...     sale_region_local=${suite_sale_region_local}
                    ...     commercial_account_manager=${suite_commercial_account_manager}
                    ...     commercial_account_manager_local=${suite_commercial_account_manager_local}
                    ...     profile_picture_url=${suite_profile_picture_url}
                    ...     national_id_photo_url=${suite_national_id_photo_url}
                    ...     tax_id_card_photo_url=${suite_tax_id_card_photo_url}
                    ...     field_1_name=${suite_field_1_name}
                    ...     field_1_value=${suite_field_1_value}
                    ...     field_2_name=${suite_field_2_name}
                    ...     field_2_value=${suite_field_2_value}
                    ...     field_3_name=${suite_field_3_name}
                    ...     field_3_value=${suite_field_3_value}
                    ...     field_4_name=${suite_field_4_name}
                    ...     field_4_value=${suite_field_4_value}
                    ...     field_5_name=${suite_field_5_name}
                    ...     field_5_value=${suite_field_5_value}
                    ...     supporting_file_1_url=${suite_supporting_file_1_url}
                    ...     supporting_file_2_url=${suite_supporting_file_2_url}
                    ...     supporting_file_3_url=${suite_supporting_file_3_url}
                    ...     supporting_file_4_url=${suite_supporting_file_4_url}
                    ...     supporting_file_5_url=${suite_supporting_file_5_url}
    ${dic}    [200] API create sale      ${arg_dic}
    set suite variable      ${suite_agent_id}       ${dic.id}

        # Create sofs cash link with sale
    ${dic}      [200] API system user create user sof cash
    ...     ${suite_agent_id}
    ...     2
    ...     ${suite_currency}
    ...     ${suite_admin_access_token}

[Agent][200] Prepare an agent
    [Arguments]
        ...     ${headers}
        ...     ${output_agent_type_id}=test_agent_type_id
        ...     ${output_agent_id}=test_agent_id
        ...     ${output_agent_username}=test_agent_username
        ...     ${output_agent_password}=test_agent_password
    ${agent_type_name}   generate random string  10    [LETTERS]
    [Agent][Prepare] - create agent type - body
        ...     name=${agent_type_name}
        ...     description=description_${agent_type_name}
        ...     is_sale=true
    [Agent][200] - create agent type
        ...     headers=${headers}
        ...     body=${body}
    [Common][Extract] - ID
        ...     output=${output_agent_type_id}
    ${unique_reference}   generate random string  10    [NUMBERS]
    ${username}   generate random string  10    [NUMBERS]
    [Common] - Set variable
        ...     name=${output_agent_username}
        ...     value=${username}
    [Common] - Set variable
        ...     name=${output_agent_password}
        ...     value=${setup_agent_password}
    ${encrypted_password}     rsa encrypt     ${setup_agent_password}     ${agent_rsa_public_key}     ${False}
    [Agent][Prepare] - create agent - body
        ...         agent_type_id=${${output_agent_type_id}}
        ...         unique_reference=${unique_reference}
        ...         username=${username}
        ...         password=${encrypted_password}
        ...         identity_type_id=1
    [Agent][200] - create agent
        ...     headers=${headers}
        ...     body=${body}
    [Common][Extract] - ID
        ...     output=${output_agent_id}

[Agent][200] Prepare an agent with sof_card
    [Arguments]
        ...     ${headers}
        ...     ${currency}=USD
        ...     ${output_agent_type_id}=test_agent_type_id
        ...     ${output_agent_id}=test_agent_id
        ...     ${output_sof_card_token}=test_sof_card_token
    [Agent][200] Prepare an agent
        ...     headers=${headers}
        ...     output_agent_type_id=${output_agent_type_id}
        ...     output_agent_id=${output_agent_id}
        ...     output_agent_username=agent_username
        ...     output_agent_password=agent_password
    [Api Gateway][200] agent authentication with plain password
        ...     username=${agent_username}
        ...     password=${agent_password}
        ...     client_id=${headers["client_id"]}
        ...     client_secret=${headers["client_secret"]}
        ...     output_token=agent_access_token
    [Common][Prepare] - Access token headers
        ...     access_token=${agent_access_token}
        ...     output=agent_headers
    ${provider_name}    generate random string  10    [LETTERS]
    [Sof_Card][Prepare] - add provider - body
        ...     $.name=${provider_name}
    [Sof_Card][200] - add provider
        ...     headers=${headers}
        ...     body=${body}
    [Common][Extract] - ID
        ...     output=provider_id
    ${card_design_name}  generate random string     20    [LETTERS]
    ${pattern}          generate random string  6   [NUMBERS]
    [Sof_Card][Prepare] - add card design - body
        ...     $.name=${card_design_name}
        ...     $.card_type_id=1
        ...     $.is_active=true
        ...     $.currency=${currency}
        ...     $.pan_pattern=${pattern}
        ...     $.pre_link_url=${pre_link_url}
        ...     $.pre_link_read_timeout=10000
        ...     $.link_url=${link_url}
        ...     $.link_read_timeout=10000
        ...     $.un_link_url=${un_link_url}
        ...     $.un_link_read_timeout=10000
        ...     $.debit_url=${debit_url}
        ...     $.debit_read_timeout=10000
        ...     $.credit_url=${credit_url}
        ...     $.credit_read_timeout=10000
        ...     $.check_status_url=${check_status_url}
        ...     $.check_status_read_timeout=10000
        ...     $.cancel_url=${cancel_url}
        ...     $.cancel_read_timeout=10000
        ...     $.pre_sof_order_url=${pre_sof_order_url}
        ...     $.pre_sof_order_read_timeout=10000
    [Sof_Card][200] - add card design
        ...     headers=${headers}
        ...     body=${body}
        ...     provider_id=${provider_id}
    [Common][Extract] - ID
        ...     output=card_design_id
    ${citizen_id}   generate random string  6   [NUMBERS]
    ${cvv_cvc}    generate random string  3   [NUMBERS]
    ${pin}  generate random string  15   [NUMBERS]
    ${mobile_number}  generate random string  15   [NUMBERS]
    ${billing_address}  generate random string  15   [LETTERS]
    ${card_number_last_8}   generate random string  8   [NUMBERS]
    ${card_number}     catenate     ${pattern}     ${card_number_last_8}
    [Payment][Prepare] - verify (pre-link) card information - body
        ...     card_account_name=${card_design_name}
        ...     card_account_number=${card_number}
        ...     citizen_id=${citizen_id}
        ...     cvv_cvc=${cvv_cvc}
        ...     pin=${pin}
        ...     expiry_date=12/08
        ...     issue_date=11/08
        ...     mobile_number=${mobile_number}
        ...     billing_address=${billing_address}
        ...     card_type=ATM
        ...     currency=${currency}
    [Payment][200] - verify (pre-link) card information
        ...     headers=${agent_headers}
        ...     body=${body}
    [Payment][Extract] Verify (pre-link) card information - security ref
        ...     output=security_ref
    [Payment][Prepare] - user link card source of fund - body
        ...     card_account_name=${card_design_name}
        ...     card_account_number=${card_number}
        ...     citizen_id=${citizen_id}
        ...     cvv_cvc=${cvv_cvc}
        ...     pin=${pin}
        ...     expiry_date=12/08
        ...     issue_date=11/08
        ...     mobile_number=${mobile_number}
        ...     billing_address=${billing_address}
        ...     currency=${currency}
        ...     security_code=123456
        ...     security_ref=${security_ref}
    [Payment][200] - user link card source of fund
        ...     headers=${agent_headers}
        ...     body=${body}
    [Payment][Extract] user link card source of fund - token
        ...     output=${output_sof_card_token}

[Agent][200] Prepare an agent with sof_cash
    [Arguments]
        ...     ${headers}
        ...     ${currency}=USD
        ...     ${output_agent_type_id}=test_agent_type_id
        ...     ${output_agent_id}=test_agent_id
        ...     ${output_sof_cash_id}=test_sof_cash_id

    [Agent][200] Prepare an agent
        ...     headers=${headers}
        ...     output_agent_type_id=${output_agent_type_id}
        ...     output_agent_id=${output_agent_id}
        ...     output_agent_username=agent_username
        ...     output_agent_password=agent_password
    [Api Gateway][200] agent authentication with plain password
        ...     username=${agent_username}
        ...     password=${agent_password}
        ...     client_id=${headers["client_id"]}
        ...     client_secret=${headers["client_secret"]}
        ...     output_token=agent_access_token
    [Common][Prepare] - Access token headers
        ...     access_token=${agent_access_token}
        ...     output=agent_headers
    [Payment][Prepare] - create user sof cash - body
        ...     currency=${currency}
    [Payment][200] - create user sof cash
        ...     headers=${agent_headers}
        ...     body=${body}
    [Payment][Extract] Create user sof cash - sof id
        ...     output=${output_sof_cash_id}

[Agent][200] - Create agent relationship
    [Arguments]     &{arg_dic}
    [Common][Prepare] - Access token headers
        ...     access_token=${arg_dic.access_token}
        ...     output=headers
    [Agent][Prepare] - create relationship - body   output=body  &{arg_dic}
    [Agent][200] - create relationship
        ...     headers=${headers}
        ...     body=${body}

[200][Prepare] - Set permission is true with fund in function and actor is sale type 1 and target is sale type 2
    [Arguments]     &{arg_dic}
    [Common][Prepare] - Access token headers
        ...     access_token=${arg_dic.access_token}
        ...     output=headers
    [Agent][Prepare] - Create sale permissions configurations - body        output=body  &{arg_dic}
    [Agent][200] - create sale permissions configurations
        ...     headers=${headers}
        ...     body=${body}

[Agent][Test][200] Check sale fund in permission on actor and target agent
    [Arguments]     &{arg_dic}
    [Common][Prepare] - Access token headers
        ...     access_token=${arg_dic.access_token}
        ...     output=headers
    ${subordinate_id}   get from dictionary     ${arg_dic}   subordinate_id
    ${subordinate_id}  convert to string   ${subordinate_id}
    [Agent][Prepare] API check sale fund in perrmission - body    output=body  &{arg_dic}
    ${dic}  [Agent][200] API check sale fund in perrmission
        ...     headers=${headers}
        ...     body=${body}
        ...     subordinate_id=${subordinate_id}
    set test variable   ${test_result_dic}      ${dic}

[Agent][Test][Fail] Call validate permission with un-exist actor
    [Arguments]     &{arg_dic}
    [Common][Prepare] - Access token headers
        ...     access_token=${arg_dic.access_token}
        ...     output=headers
    ${subordinate_id}   get from dictionary     ${arg_dic}   subordinate_id
    ${subordinate_id}  convert to string   ${subordinate_id}
    [Agent][Prepare] API check sale fund in perrmission - body    output=body  &{arg_dic}
    ${dic}  [Agent][Fail] API check sale fund in perrmission
        ...     ${headers}
        ...     ${body}
        ...     ${subordinate_id}
    set test variable   ${test_result_dic}      ${dic}

[Agent][Test][200] - Create connection for sale 1 and sale 2
    [Arguments]     &{arg_dic}
    [200] API create connection         ${arg_dic}

[Agent][Test] Call validate permission with un-exist permission code
    [Arguments]     &{arg_dic}
    [Common][Prepare] - Access token headers
        ...     access_token=${arg_dic.access_token}
        ...     output=headers
    ${subordinate_id}   get from dictionary     ${arg_dic}   subordinate_id
    ${subordinate_id}  convert to string   ${subordinate_id}
    [Agent][Prepare] API check sale fund in perrmission - body    output=body  &{arg_dic}
    ${dic}  [Agent][Fail] API check sale fund in perrmission
        ...     ${headers}
        ...     ${body}
        ...     ${subordinate_id}
    set test variable   ${test_result_dic}      ${dic}