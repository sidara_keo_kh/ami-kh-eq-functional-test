*** Settings ***
Resource         ../../2_api/3_reuse_function/imports.robot

*** Keywords ***
[Test][200] API get formula by report type id
    [Arguments]     ${report_type_id}       ${suite_admin_access_token}=${suite_admin_access_token}
    ${dic}=     [200] API report - get formula by report type id     ${report_type_id}       ${suite_admin_access_token}

    ${actual_tpv}=                      get from dictionary      ${dic}      actual_tpv
    ${actual_fee}=                      get from dictionary      ${dic}      actual_fee
    ${actual_commission}=               get from dictionary      ${dic}      actual_commission
    ${actual_effective_timestamp}=      get from dictionary      ${dic}      actual_effective_timestamp

    set test variable    ${test_tpv}                      ${actual_tpv}
    set test variable    ${test_fee}                      ${actual_fee}
    set test variable    ${test_commission}               ${actual_commission}
    set test variable    ${test_effective_timestamp}      ${actual_effective_timestamp}
