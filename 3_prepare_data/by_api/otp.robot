*** Settings ***
Resource         ../../2_api/3_reuse_function/imports.robot

*** Keywords ***
[OTP][200] - activation Otp generation
    ${arg_dic}  create dictionary   access_token=${test_admin_access_token}     activation_field=${test_national_id_number}    agent_id=${test_agent_id}
    ${dic}      [200] API activation Otp generation    ${arg_dic}
    ${otp_reference_id}    get from dictionary    ${dic}   otp_reference_id
    set test variable       ${test_otp_reference_id}        ${otp_reference_id}

[OTP][200] - get OTP detail on mock
    ${dic}     [200] API get OTP detail on mock        ${test_otp_reference_id}
    ${otp_code}    get from dictionary    ${dic}   otp_code
    ${user_reference_code}    get from dictionary    ${dic}   user_reference_code
    set test variable       ${test_otp_code}        ${otp_code}
    set test variable       ${test_user_reference_code}      ${user_reference_code}

[OTP][200] - activation Otp verification
    ${arg_dic}  create dictionary   access_token=${test_admin_access_token}     otp_code=${test_otp_code}  otp_ref=${test_otp_reference_id}   user_ref_code=${test_user_reference_code}   agent_id=${test_agent_id}
    [200] API activation Otp verification    ${arg_dic}
