*** Settings ***
Resource         ../../2_api/3_reuse_function/imports.robot

*** Keywords ***
[Centralize_Configuration][200] - Get centralize configuration currency codes
    [Arguments]     &{arg_dic}
    ${currency_code_list}=   create list
    ${dic}   [200] API get configuration detail  access_token=${arg_dic.access_token}    scopeName=${arg_dic.scopeName}    key=${arg_dic.key}
    ${get_configuration_currency}=   get from dictionary       ${dic}      value
    ${currencies}=     split string    ${get_configuration_currency}   ,
    :FOR        ${currency}    IN     @{currencies}
    \   ${currency_split}=     split string    ${currency}   |
    \   ${currency_code}=     Get From List    ${currency_split}    0
    \   append to list      ${currency_code_list}   ${currency_code}
    [Return]  ${currency_code_list}