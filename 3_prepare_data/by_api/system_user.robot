*** Settings ***
Resource         ../../2_api/3_reuse_function/imports.robot

*** Keywords ***
[System_User][Test][200] - login as setup system user
    ${arg_dic}      create dictionary   username=${setup_admin_username}       password=${setup_password_admin_encrypted_utf8}
    ${dic}     [200] API system user authentication     ${arg_dic}
    ${response}=        get from dictionary     ${dic}          response

    ${access_token}=        get from dictionary     ${dic}          access_token
    set test variable       ${test_admin_access_token}     ${access_token}

    ${user_id}=        get from dictionary     ${dic}          user_id
    set test variable       ${test_admin_user_id}     ${user_id}

[System_User][Test][200] - create system user
    [Arguments]
        ...     ${output_user_id}=test_new_system_user_id
        ...     ${output_user_name}=test_new_system_user_username
        ...     ${output_password}=test_new_system_user_password
        ...     &{arg_dic}
    ${username}     generate random string  10  [LETTERS]
    ${password}     rsa encrypt     ${setup_password_robot}     ${system_user_rsa_public_key}
    ${firstname}    generate random string  10  [LETTERS]
    ${lastname}     generate random string  10  [LETTERS]
    ${email}        generate random string  10  [LETTERS]
    ${email}       set variable    ${email}@gmail.com
    ${arg_dic_1}      create dictionary   access_token=${arg_dic.access_token}
                                  ...   username=${username}
                                  ...  	password=${password}
                                  ...   firstname=${firstname}
                                  ... 	lastname=${lastname}
                                  ...   email=${email}

    ${dic}  [200] API create system user    ${arg_dic_1}
    ${id}     get from dictionary    ${dic}   id
    set test variable       \${${output_user_id}}       ${id}
    set test variable       \${${output_user_name}}       ${username}
    set test variable       \${${output_password}}       ${setup_password_robot}
    @{list_system_user_id}          create list     ${id}
    set test variable           @{list_system_user_id}          @{list_system_user_id}

[System_User][Test][200] - create system user with role Admin
    [Arguments]     &{arg_dic}
    [System_User][Test][200] - create system user       access_token=${arg_dic.access_token}
    [200] API add user to role      access_token=${arg_dic.access_token}     user_id=${test_new_system_user_id}     role_id=1
    [Test][200] API system user authentication      ${test_new_system_user_username}     ${setup_admin_password_encrypted_utf8}

[System_User][Test][Fail] - Create system user
    [Arguments]   &{arg_dic}
    ${username}     generate random string  10  [LETTERS]
    ${password}     set variable    ${setup_admin_password_encrypted}
    ${firstname}    generate random string  10  [LETTERS]
    ${lastname}     generate random string  10  [LETTERS]
    ${email}     generate random string  10  [LETTERS]
    ${email}    set variable    ${email}@gmail.com
    ${arg_dic_1}      create dictionary   access_token=${arg_dic.access_token}
                                  ...   username=${username}
                                  ...  	password=${password}
                                  ...   firstname=${firstname}
                                  ... 	lastname=${lastname}
                                  ...   email=${email}
                                  ...   code=${arg_dic.code}
                                  ...   status_code=${arg_dic.status_code}
                                  ...   status_message=${arg_dic.status_message}
    ${dic}  [Fail] API create system user   ${arg_dic_1}

[System_User][Test][200] - login as company agent
    ${arg_dic}   create dictionary      username=${setup_company_agent_account_id}      password=${setup_company_agent_password_encrypted}
    ${dic}    [200] API agent authentication        ${arg_dic}
    ${response}=        get from dictionary     ${dic}          response
    ${access_token}      get from dictionary    ${dic}   access_token
    set test variable   ${test_company_agent_access_token}      ${access_token}

[System_User][Suite][200] - login as setup system user
    ${arg_dic}      create dictionary
    ...     username  ${setup_admin_username}
    ...     password  ${setup_password_admin_encrypted_utf8}
    ...     client_id  ${setup_admin_client_id}
    ...     client_secret  ${setup_admin_client_secret}
    ${dic}     [200] API system user authentication     ${arg_dic}
######    Continue here
#    ${response}=        get from dictionary     ${dic}          response
#    ${correlation_id}      get from dictionary    ${dic}   correlation_id
#    set suite variable   ${suite_correlation_id}      ${correlation_id}
#    ${access_token}=        get from dictionary     ${dic}          access_token
#    set suite variable       ${suite_admin_access_token}     ${access_token}
#    set suite variable      ${suite_setup_admin_access_token}   ${access_token}
#    set suite variable      ${suite_system_user_no_link_access_token}   ${access_token}

[System_User][Suite][200] - create account login via api
    ${gen_username}=    generate random string  10     [LETTERS]
    ${arg_dic}      create dictionary   username=${setup_admin_username}       password=${setup_password_admin_encrypted_utf8}
    ${dic}     [200] API system user authentication     ${arg_dic}
    ${admin_access_token}=          set variable                    ${dic.access_token}

    ${username}      set variable       robot_${gen_username}
    ${arg_dic}      create dictionary   access_token=${admin_access_token}
                                  ...   username=${username}
                                  ...  	password=${setup_admin_password_encrypted_old}
                                  ...   firstname=${username} firstName
                                  ... 	lastname=${username} lastName
                                  ...   email=${username}@email.com
                                  ...   middle_name=${username}_middleNname
                                  ...   is_external=true
    ${dic}      [200] API create system user		 ${arg_dic}
    ${system_user_id}   set variable   ${dic.id}

    [200] API add user to role      access_token=${admin_access_token}     user_id=${system_user_id}     role_id=1
    ${arg_dic}      create dictionary   username=${username}       password=${setup_admin_password_encrypted_utf8_old}
    ${dic}     [200] API system user authentication     ${arg_dic}
    ${admin_access_token}=          set variable                    ${dic.access_token}

    ${arg_dic}     create dictionary     access_token=${admin_access_token}     old_password=${setup_admin_password_encrypted_old}     new_password=${setup_admin_password_encrypted}
    [200] API Change system-user password       ${arg_dic}
    ${arg_dic}      create dictionary   username=${username}       password=${setup_admin_password_encrypted_utf8}
    ${dic}     [200] API system user authentication     ${arg_dic}

    set suite variable      ${suite_admin_username}       ${username}
    set suite variable      ${suite_admin_access_token}     ${dic.access_token}
    set suite variable      ${suite_admin_user_id}       ${system_user_id}
    set suite variable      ${suite_admin_password}       ${setup_password_robot}
    set suite variable      ${suite_admin_password_encrypted}       ${setup_admin_password_encrypted}
    set suite variable      ${suite_admin_user_type}       system-user
    set suite variable      ${suite_setup_admin_access_token}   ${dic.access_token}
    [Common][Prepare] - Access token headers
        ...     access_token=${suite_admin_access_token}
        ...     output=SUITE_ADMIN_HEADERS


[System_User][Test][200] - create account login via api without any role
    ${gen_username}=    generate random string  10     [LETTERS]
    ${arg_dic}      create dictionary   username=${setup_admin_username}       password=${setup_password_admin_encrypted_utf8}
    ${dic}     [200] API system user authentication     ${arg_dic}
    ${admin_access_token}=          set variable                    ${dic.access_token}

    ${username}      set variable       robot_${gen_username}
    ${arg_dic}      create dictionary   access_token=${admin_access_token}
                                  ...   username=${username}
                                  ...  	password=${setup_admin_password_encrypted_old}
                                  ...   firstname=${username} firstName
                                  ... 	lastname=${username} lastName
                                  ...   email=${username}@email.com
    ${dic}      [200] API create system user        ${arg_dic}
    ${system_user_id}   set variable   ${dic.id}
    ${arg_dic}      create dictionary   username=${username}       password=${setup_admin_password_encrypted_utf8_old}
    ${dic}      [200] API system user authentication     ${arg_dic}
    ${admin_access_token}=          set variable                    ${dic.access_token}

    ${arg_dic}     create dictionary     access_token=${admin_access_token}     old_password=${setup_admin_password_encrypted_old}     new_password=${setup_admin_password_encrypted}
    [200] API Change system-user password       ${arg_dic}
    ${arg_dic}      create dictionary   username=${username}       password=${setup_admin_password_encrypted_utf8}
    ${dic}     [200] API system user authentication     ${arg_dic}

    set test variable      ${test_admin_username}       ${username}
    set test variable      ${test_admin_access_token}     ${dic.access_token}
    set test variable      ${test_admin_user_id}       ${system_user_id}
    set test variable      ${test_admin_password}       ${setup_password_robot}
    set test variable      ${test_admin_password_encrypted}       ${setup_admin_password_encrypted}

[System_User][Suite][200] - create account login without changing password
    ${gen_username}=    generate random string  10     [LETTERS]
    ${arg_dic}      create dictionary   username=${setup_admin_username}       password=${setup_password_admin_encrypted_utf8}
    ${dic}     [200] API system user authentication     ${arg_dic}
    ${admin_access_token}=          set variable                    ${dic.access_token}

    ${username}      set variable       robot_${gen_username}
    ${arg_dic}      create dictionary   access_token=${admin_access_token}
                                  ...   username=${username}
                                  ...  	password=${setup_admin_password_encrypted}
                                  ...   firstname=${username} firstName
                                  ... 	lastname=${username} lastName
                                  ...   email=${username}@email.com
    ${dic}      [200] API create system user		${arg_dic}
    ${system_user_id}   set variable   ${dic.id}
    [200] API add user to role       access_token=${admin_access_token}     user_id=${system_user_id}     role_id=1
    ${arg_dic}      create dictionary   username=${username}       password=${setup_admin_password_encrypted_utf8}
    ${dic}     [200] API system user authentication     ${arg_dic}
    ${admin_access_token}=          set variable                    ${dic.access_token}

    set suite variable      ${suite_admin_username}       ${username}
    set suite variable      ${suite_admin_access_token}     ${dic.access_token}
    set suite variable      ${suite_admin_user_id}       ${system_user_id}
    set suite variable      ${suite_admin_password}       ${setup_password_robot}
    set suite variable      ${suite_admin_password_encrypted}       ${setup_admin_password_encrypted}
    set suite variable      ${suite_setup_admin_access_token}   ${dic.access_token}

[System_User][Test][200] - Update system user
    [Arguments]   &{arg_dic}
    ${username}     generate random string  10  [LETTERS]
    ${password}     set variable    ${setup_admin_password_encrypted}
    ${firstname}    generate random string  10  [LETTERS]
    ${lastname}     generate random string  10  [LETTERS]
    ${email}     generate random string  10  [LETTERS]
    ${email}    set variable    ${email}@gmail.com
    ${dic}  [200] API update system user   access_token=${arg_dic.access_token}
                                  ...   username=${username}
                                  ...  	password=${password}
                                  ...   firstname=${firstname}
                                  ... 	lastname=${lastname}
                                  ...   email=${email}
                                  ...   user_id=${arg_dic.user_id}

[System_User][Test][Fail] - Update system user
    [Arguments]   &{arg_dic}
    ${username}     generate random string  10  [LETTERS]
    ${password}     set variable    ${setup_admin_password_encrypted}
    ${firstname}    generate random string  10  [LETTERS]
    ${lastname}     generate random string  10  [LETTERS]
    ${email}     generate random string  10  [LETTERS]
    ${email}    set variable    ${email}@gmail.com
    ${dic}  [Fail] API update system user   access_token=${arg_dic.access_token}
                                  ...   username=${username}
                                  ...  	password=${password}
                                  ...   firstname=${firstname}
                                  ... 	lastname=${lastname}
                                  ...   email=${email}
                                  ...   user_id=${arg_dic.user_id}
                                  ...   code=${arg_dic.code}
                                  ...   status_code=${arg_dic.status_code}
                                  ...   status_message=${arg_dic.status_message}

[System_User][Test][200] - Create permission
    [Arguments]     &{arg_dic}
    ${description}      generate random string      20      [LETTERS]
    ${name}             generate random string      10      [LETTERS]
    set test variable    ${is_page_level}    true
    ${dic}    [200] API create permission  access_token=${arg_dic.access_token}
                                        ...    description=${description}
                                        ...    name=${name}
                                        ...    is_page_level=${is_page_level}
    ${response}     get from dictionary   ${dic}     response
    set test variable   ${test_permission_id}    ${response.json()['data']['id']}
    set test variable   ${test_description}    ${description}
    set test variable   ${test_name}    ${name}
    set test variable   ${test_is_page_level}    ${is_page_level}

[System_User][Test][Fail] - Create permission
    [Arguments]     &{arg_dic}
    ${description}      generate random string      20      [LETTERS]
    ${name}             generate random string      10      [LETTERS]
    set test variable    ${is_page_level}    true
    ${dic}    [Fail] API create permission    access_token=${arg_dic.access_token}
                                        ...   description=${description}
                                        ...   name=${name}
                                        ...   is_page_level=${is_page_level}
                                        ...   code=${arg_dic.code}
                                        ...   status_code=${arg_dic.status_code}
                                        ...  status_message=${arg_dic.status_message}
[System_User][Test][200] - Update permission
    [Arguments]     &{arg_dic}
    ${description}      generate random string      20      [LETTERS]
    ${name}             generate random string      10      [LETTERS]
    ${dic}          [200] API update permission    access_token=${arg_dic.access_token}
                                       ...   permission_id=${arg_dic.permission_id}
                                        ...   description=${description}
                                        ...   name=${name}
                                        ...   is_page_level=${is_page_level}

[System_User][Test][Fail] - Update permission
    [Arguments]     &{arg_dic}
    ${description}      generate random string      20      [LETTERS]
    ${name}             generate random string      10      [LETTERS]
    ${dic}          [Fail] API update permission    access_token=${arg_dic.access_token}
                                        ...   permission_id=${arg_dic.permission_id}
                                        ...   description=${description}
                                        ...   name=${name}
                                        ...   is_page_level=${is_page_level}
                                        ...   code=${arg_dic.code}
                                        ...   status_code=${arg_dic.status_code}
                                        ...   status_message=${arg_dic.status_message}

[System_User][Test][200] - Create role
    [Arguments]     &{arg_dic}
    ${name}     generate random string   10     [LETTERS]
    set test variable   ${test_name}        ${name}
    ${dic}      [200] API create role    access_token=${arg_dic.access_token}
                                        ...   name=${name}
    ${response}     get from dictionary   ${dic}        response
    set test variable   ${test_role_id}     ${response.json()['data']['id']}

[System_User][Test][Fail] - Create role
    [Arguments]     &{arg_dic}
    ${name}         generate random string      10      [LETTERS]
    ${dic}          [Fail] API create role    access_token=${arg_dic.access_token}
                                        ...   name=${name}
                                        ...   code=${arg_dic.code}
                                        ...   status_code=${arg_dic.status_code}
                                        ...  status_message=${arg_dic.status_message}

[System User][200] Add permission by name to user
    [Arguments]     ${permission_name}      ${user_id}      ${headers}=${SUITE_ADMIN_HEADERS}
    [Report][200] Search permission by name
        ...     permission_name=${permission_name}
        ...     output_permission_id=permission_id
        ...     headers=${headers}
    [System_User][Prepare] - Create role - body
    [System_User][200] - Create role
        ...     headers=${headers}
        ...     body=${body}
    [Common][Extract] - ID
        ...     output=role_id
    [System_User][Prepare] - Add permissions to role - body
        ...     $.permissions=[ ${permission_id} ]
    [System_User][200] - Add permission to role
        ...     headers=${headers}
        ...     body=${body}
        ...     role_id=${role_id}
    [System_User][Prepare] - Add user to role - body
        ...     $.user_id=${user_id}
    [System_User][200] - Add user to role
        ...     headers=${headers}
        ...     body=${body}
        ...     role_id=${role_id}