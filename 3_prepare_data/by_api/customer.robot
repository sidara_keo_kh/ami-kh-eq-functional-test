*** Settings ***
Resource         ../../2_api/3_reuse_function/imports.robot

*** Keywords ***
[Test][200] Search order_detail_id by order_id
    [Arguments]       ${admin_access_token}          ${order_id}
    ${arg_dic}       create dictionary       access_token=${admin_access_token}      order_id=${order_id}
    ${dic}=     [200] API search card transactions    ${arg_dic}

     ${first_order_detail_id}=      get from dictionary     ${dic}      first_order_detail_id
     set test variable          ${search_order_detail_id}           ${first_order_detail_id}

[Test][200] Update report formula
    [Common] - Get current start date
    ${arg_dic}       create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     report_type_id=1
    ...     effective_timestamp=${test_current_from_date}
    ...     tpv=amount
    ...     fee=a
    ...     commission=b
    [200] API update report formula     ${arg_dic}

[Test][200] Update payment report formula
    [Arguments]       &{arg_dic}
    ${request}       create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     report_type_id=${arg_dic.report_type_id}
    ...     effective_timestamp=${arg_dic.effective_timestamp}
    ...     tpv=${arg_dic.tpv}
    ...     fee=${arg_dic.fee}
    ...     commission=${arg_dic.commission}
    [200] API update report formula     ${request}

[Suite][200] Create Customers Classification
    ${classification_name}=   generate random string    10      [LETTERS]
    ${classification_desc}=   generate random string    20      [LETTERS]
    ${dic_classification}    [200] API system user create customer classification   ${suite_admin_access_token}    ${classification_name}       ${classification_desc}
    ${customer_classification_id}    get from dictionary    ${dic_classification}    id
    set suite variable    ${suite_customer_classification_id}    ${customer_classification_id}
    set suite variable    ${suite_customer_classification_name}    ${classification_name}
    set suite variable    ${suite_customer_classification_desc}    ${classification_desc}

[Test][200] Create Customers Classification
    ${classification_name}=   generate random string    10      [LETTERS]
    ${classification_desc}=   generate random string    20      [LETTERS]
    ${dic_classification}    [200] API system user create customer classification   ${suite_admin_access_token}    ${classification_name}       ${classification_desc}
    ${customer_classification_id}    get from dictionary    ${dic_classification}    id
    set test variable    ${test_customer_classification_id}    ${customer_classification_id}
    set test variable    ${test_customer_classification_name}    ${classification_name}
    set test variable    ${test_customer_classification_desc}    ${classification_desc}


[Suite][200] System user create customer
    set suite variable   ${identity_identity_type_id}        1
    ${suite_customer_username}=      generate random string      10      [LETTERS]
    ${primary_mobile_number}=       generate random string      10      [NUMBERS]
    ${unique_reference}=            generate random string      10      [LETTERS]

    set suite variable   ${suite_customer_username}       ${suite_customer_username}
    ${dic}      [200] API system user create customer
    ...     ${suite_customer_username}
    ...     ${setup_password_customer_encrypted}
    ...     ${identity_identity_type_id}
    ...     ${suite_admin_access_token}
    ...     ${primary_mobile_number}
    ...     ${unique_reference}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
   ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}

    ${get_customer_id}  get from dictionary     ${dic}      id
    set suite variable  ${suite_customer_id}    ${get_customer_id}

[Suite][200] System user create customer with specific classification
    ${customer_classification_name}=      generate random string      10      [LETTERS]
    set suite variable   ${suite_customer_classification_name}       service_limit_${customer_classification_name}
    ${dic}      [200] API system user create customer classification
    ...     ${suite_admin_access_token}
    ...     ${suite_customer_classification_name}

    ${customer_classification_id}  get from dictionary     ${dic}      id
    set suite variable   ${suite_customer_classification_id}     ${customer_classification_id}

    set suite variable   ${identity_identity_type_id}        1
    ${customer_username}=      generate random string      10      [LETTERS]
    ${primary_mobile_number}=       generate random string      10      [NUMBERS]
    ${unique_reference}=            generate random string      10      [LETTERS]

    set suite variable   ${suite_customer_username}       ${customer_username}
    ${dic}      [200] API system user create customer
    ...     ${suite_customer_username}
    ...     ${setup_password_customer_encrypted}
    ...     ${identity_identity_type_id}
    ...     ${suite_admin_access_token}
    ...     ${primary_mobile_number}
    ...     ${unique_reference}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${suite_customer_classification_id}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}

    ${get_customer_id}  get from dictionary     ${dic}      id
    set suite variable   ${suite_customer_id}     ${get_customer_id}

    ${arg_dic}      create dictionary     username=${suite_customer_username}      password=${setup_password_customer_encrypted_utf8}     grant_type=password     param_client_id=${setup_admin_client_id}
    ${login_customer_dic}      [200] API customer authentication    ${arg_dic}

    ${get_customer_access_token}  get from dictionary     ${login_customer_dic}      access_token
    set suite variable   ${suite_customer_access_token}     ${get_customer_access_token}

    ${created_sof_cash_dic}     [200] API create user sof cash          VND       ${suite_customer_access_token}


[Customer][Suite][200] - Create customer classification custom
    [Arguments]     ${access_token}
    [Customer][Suite][200] - Create customer classification     ${access_token}
    set suite variable      ${suite_customer_classification_name_custom}     ${suite_customer_classification_name}

[Customer][Suite][200] - Create customer classification
    [Arguments]     ${access_token}
    ${name}     generate random string  12    [LETTERS]
    ${dic}  [200] API system user create customer classification    ${access_token}     ${name}
    set suite variable      ${suite_customer_classification_id}     ${dic.id}
    set suite variable      ${suite_customer_classification_name}     ${name}

[Customer][Suite][200] - System user updates customer profile
    [Arguments]     ${access_token}     ${customer_id}     ${customer_classification_id}    ${unique_reference}
    ${dic}  create dictionary   access_token=${access_token}    customer_id=${customer_id}  customer_classification_id=${customer_classification_id}    unique_reference=${unique_reference}
    [200] API system user updates customer profile      ${dic}

[200] API system user create customer with KYC level
    [Arguments]     ${arg_dic}
    set test variable   ${test_customer_username}        ${arg_dic.username}
    set test variable   ${test_kyc_level}        ${arg_dic.kyc_level}
    set test variable   ${identity_identity_type_id}        1
    ${primary_mobile_number}=       generate random string      10      [NUMBERS]
    ${unique_reference}=            generate random string      10      [LETTERS]
    set test variable   ${test_customer_username}       ${test_customer_username}
    ${dic}      [200] API system user create customer
    ...     ${test_customer_username}
    ...     ${setup_password_customer_encrypted}
    ...     ${identity_identity_type_id}
    ...     ${suite_admin_access_token}
    ...     ${primary_mobile_number}
    ...     ${unique_reference}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${test_kyc_level}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}

    ${get_customer_id}  get from dictionary     ${dic}      id
    [Return]    ${get_customer_id}

[Test][200] System user create customer
    set test variable   ${identity_identity_type_id}        1
    ${test_customer_username}=      generate random string      10      [LETTERS]
    ${primary_mobile_number}=       generate random string      10      [NUMBERS]
    ${unique_reference}=            generate random string      10      [LETTERS]

    set test variable   ${test_customer_username}       ${test_customer_username}
    ${dic}      [200] API system user create customer
    ...     ${test_customer_username}
    ...     ${setup_password_customer_encrypted}
    ...     ${identity_identity_type_id}
    ...     ${suite_admin_access_token}
    ...     ${primary_mobile_number}
    ...     ${unique_reference}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}

    ${get_customer_id}  get from dictionary     ${dic}      id
    set test variable  ${test_customer_id}    ${get_customer_id}