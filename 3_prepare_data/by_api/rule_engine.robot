*** Settings ***
Resource         ../../2_api/3_reuse_function/imports.robot

*** Keywords ***
[Test][200] API create an active rule
    ${rule_name}      generate random string      20   prepare_[LETTERS]
    ${rule_description}      generate random string      30   prepare_[LETTERS]
    ${is_active}  set variable    true
    ${get_start_date}=              get current date                result_format=%Y-%m-%d
    ${start_date}=                  set variable      ${get_start_date}T00:00:00Z
    ${get_end_date}=                get current date    result_format=%Y-%m-%d
    ${get_end_date}=               add time to date    ${get_end_date}             30 days          result_format=%Y-%m-%d
    ${end_date}=                    set variable      ${get_end_date}T00:00:00Z
    ${dic}     [200] API create rule
    ...     ${rule_name}
    ...     ${rule_description}
    ...     ${is_active}
    ...     ${start_date}
    ...     ${end_date}
    ...     ${test_admin_access_token}
    ${response}     get from dictionary   ${dic}   response
    ${rule_id}     get from dictionary   ${dic}   rule_id
    set test variable   ${rule_id}       ${rule_id}

[Test][200] API update rule with status inactive
    ${rule_name}    generate random string      15      [LETTERS]
    ${rule_description}    generate random string      15      [LETTERS]
    [Arguments]  ${rule_id}
    [200] API update rule
    ...     ${rule_id}
    ...     ${rule_name}
    ...     ${rule_description}
    ...     false
    ...     ${test_admin_access_token}

[Test][200] API create mechanic by rule id "${rule_id}" with event name "create_order"
    ${get_start_date}=              get current date                result_format=%Y-%m-%d
    ${start_date}=                  set variable      ${get_start_date}T00:00:00Z
    ${get_end_date}=                get current date    result_format=%Y-%m-%d
    ${get_end_date}=               add time to date    ${get_end_date}             30 days      result_format=%Y-%m-%d
    ${end_date}=                    set variable      ${get_end_date}T00:00:00Z
    ${dic}     [200] API create mechanic
    ...     ${rule_id}
    ...     create_order
    ...     ${start_date}
    ...     ${end_date}
    ...     ${test_admin_access_token}
    ${response}     get from dictionary   ${dic}   response
    ${mechanic_id}     get from dictionary   ${dic}   mechanic_id
    set test variable   ${mechanic_id}       ${mechanic_id}

[Test][200] API create condition event detail by rule id "${rule_id}" and mechanic id "${mechanic_id}"
    ${dic}     [200] API create condition
    ...     ${rule_id}
    ...     ${mechanic_id}
    ...     event_detail
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${test_admin_access_token}
    ${response}     get from dictionary   ${dic}   response
    ${condition_id}     get from dictionary   ${dic}   condition_id
    set test variable   ${condition_id}       ${condition_id}

[Test][200] API create equal comparison by service name
    ${dic}     [200] API create comparison
    ...     ${rule_id}
    ...     ${mechanic_id}
    ...     ${condition_id}
    ...     service_name
    ...     text
    ...     ${test_service_name}
    ...     =
    ...     ${test_admin_access_token}
    ${response}     get from dictionary   ${dic}   response
    ${comparison_id}     get from dictionary   ${dic}   comparison_id
    set test variable   ${comparison_id}       ${comparison_id}

[Test][200] API create response action with service and PIN security
    ${action_data_1}=       set variable        {"key_name": "service_name", "key_value": "@@service_name@@", "key_value_type": "text"}
    ${action_data_2}=       set variable        {"key_name": "security_type", "key_value": "PIN", "key_value_type": "text"}
    ${action_data_3}=       set variable        {"key_name": "service_id", "key_value": "@@service_id@@", "key_value_type": "numeric"}
    @{action_data}  create list     ${action_data_1}    ${action_data_2}    ${action_data_3}


    ${arg_dic}  create dictionary      access_token=${test_admin_access_token}     rule_id=${rule_id}       mechanic_id=${mechanic_id}       action_type_id=3
    ...         data=@{action_data}
    ${dic}     [200] API create new action
    ...     ${arg_dic}
    ${response}     get from dictionary   ${dic}   response
    ${action_id}     get from dictionary   ${dic}    id
    set test variable   ${action_id}       ${action_id}

[Test][200] API create response action with service and EXTERNAL security
    ${action_data_1}=       set variable        {"key_name": "service_name", "key_value": "@@service_name@@", "key_value_type": "text"}
    ${action_data_2}=       set variable        {"key_name": "security_type", "key_value": "EXTERNAL", "key_value_type": "text"}
    ${action_data_3}=       set variable        {"key_name": "service_id", "key_value": "@@service_id@@", "key_value_type": "numeric"}
    @{action_data}  create list     ${action_data_1}    ${action_data_2}    ${action_data_3}

    ${arg_dic}  create dictionary      access_token=${test_admin_access_token}     rule_id=${rule_id}       mechanic_id=${mechanic_id}       action_type_id=3
    ...         data=@{action_data}
    ${dic}     [200] API create new action          ${arg_dic}
    ${response}     get from dictionary   ${dic}   response
    ${action_id}     get from dictionary   ${dic}   id
    set test variable   ${action_id}       ${action_id}