*** Settings ***
Resource         ../../2_api/3_reuse_function/imports.robot
Resource         ../../3_prepare_data/by_api/imports.robot

*** Keywords ***
[Test][200] Create an order configuration
    ${service_group_id}=      set variable    1
    ${template_id}=     set variable    2
    ${author}=   generate random string    15      robot_author_[LETTERS]
    ${status}=   generate random string    5      robot_status_[LETTERS]

    set test variable   ${test_service_group_id}       ${service_group_id}
    set test variable   ${test_template_id}       ${template_id}
    set test variable   ${test_author}       ${author}
    set test variable   ${test_status}       ${status}
    ${response}       API create order configuration api     ${test_service_group_id}        ${test_template_id}     ${test_author}      ${test_status}      ${test_agent_access_token}

    ${order_configuration_id}=       set variable        ${response.json()['data']['id']}
    set test variable    ${test_order_configuration_id}       ${order_configuration_id}

[Channel_Adapter][Test][200] - Create an order configuration for transaction detail
    ${author}=   generate random string    15      robot_author_[LETTERS]

    set test variable   ${test_template_id}       2
    set test variable   ${test_author}      ${author}
    set test variable   ${test_status}  published
    set test variable   ${test_agent_access_token}      ${suite_admin_access_token}

    ${arg_dic}      Create dictionary
    ...     service_group_id=${suite_service_group_id}
    ...     template_id=${test_template_id}
    ...     author=${test_author}
    ...     status=${test_status}
    ...     access_token=${suite_admin_access_token}
    ${response}     [200] API create order configuration    ${arg_dic}
    ${order_configuration_id}=       set variable        ${response.json()['data']['id']}
    set test variable    ${test_order_configuration_id}       ${order_configuration_id}

[Test][200] Create an order configuration for printing
    ${author}=   generate random string    15      robot_author_[LETTERS]

    set test variable   ${test_template_id}       3
    set test variable   ${test_author}      ${author}
    set test variable   ${test_status}  published
    set test variable   ${test_agent_access_token}      ${suite_admin_access_token}

    ${arg_dic}      Create dictionary
    ...     service_group_id=${suite_service_group_id}
    ...     template_id=${test_template_id}
    ...     author=${test_author}
    ...     status=${test_status}
    ...     access_token=${suite_admin_access_token}
    ${response}     [200] API create order configuration    ${arg_dic}
    ${order_configuration_id}=       set variable        ${response.json()['data']['id']}
    set test variable    ${test_order_configuration_id}       ${order_configuration_id}

[Channel_Adapter][Test][200] - Create order config detail
    ${a}  set variable    {"en_label":"","local_label":"","value":"a","other_ref_key":"","priority":1,"equation":"+","config_type_id":"2", "actor_id":"1"},
    ${b}  set variable    {"en_label":"","local_label":"","value":"a","other_ref_key":"","priority":1,"equation":"+","config_type_id":"2", "actor_id":"2"},
    ${c}  set variable    {"en_label":"","local_label":"","value":"a","other_ref_key":"","priority":1,"equation":"+","config_type_id":"2", "actor_id":"3"}
    ${list_configs_data}  create list     ${a}    ${b}    ${c}
    [200] API update order configuration api    ${test_order_configuration_id}      ${test_author}      ${test_status}      ${service_group_id}       ${test_agent_access_token}      ${list_configs_data}

[Channel_Adapter][Test][200] - Create order config detail with pre and post wallet balance
    ${a}  set variable    {"en_label":"","local_label":"","value":"pre_wallet_balance","other_ref_key":"","priority":1,"equation":"+","config_type_id":"2", "actor_id":"1"},
    ${b}  set variable    {"en_label":"","local_label":"","value":"post_wallet_balance","other_ref_key":"","priority":1,"equation":"+","config_type_id":"2", "actor_id":"2"},
    ${c}  set variable    {"en_label":"","local_label":"","value":"a","other_ref_key":"","priority":1,"equation":"+","config_type_id":"2", "actor_id":"3"}
    ${list_configs_data}  create list     ${a}    ${b}      ${c}
    [200] API update order configuration api with pre and post wallet balance    ${test_order_configuration_id}      ${test_author}      ${test_status}      ${service_group_id}       ${test_agent_access_token}      ${list_configs_data}

[Test] Set test access token from suite
    set test variable   ${test_admin_access_token}      ${suite_admin_access_token}

[Test] Set test service group id
    set test variable       ${suite_service_group_id}   ${service_group_id}

[Channel_Adapter][Test][200] - Search order configuration by service_group_id and template_id
    ${arg_dic}  create dictionary
    ...     service_group_id=${suite_service_group_id}
    ...     template_id=${test_template_id}
    ...     access_token=${test_agent_access_token}
    ${dic}  [200] API Search order configuration by service_group_id and template_id  ${arg_dic}
    set test variable   ${test_result_dic}      ${dic}

[Channel_Adapter][Test][Fail] - Search order configuration without template_id
    set test variable   ${test_agent_access_token}      ${suite_admin_access_token}
    ${arg_dic}  create dictionary
    ...     service_group_id=${suite_service_group_id}
    ...     template_id=2
    ...     access_token=${test_agent_access_token}
    ${dic}  [Fail] API Search order configuration by service_group_id and template_id  ${arg_dic}
    set test variable   ${test_result_dic}      ${dic}

[Test][200] Create and update order configuration with field A - O for payer
    [Arguments]     ${service_group_id}
    ${template_id}=     set variable    2
    ${author}=   generate random string    15      [LETTERS]
    ${status}=   set variable    published

    @{items}=    Create List   a   b   c   d   e   f   g   h   i   j   k   l   m   n   o
    ${list_configs_data}       set variable         {"en_label":"En label amount","local_label":"Local label amount","value":"amount","other_ref_key":"","priority":1,"equation":"+","config_type_id":2, "actor_id":1},
    ${list_configs_data}         catenate        ${list_configs_data}           {"en_label":"agent payer id","local_label":"Local agent id","value":"payer_user.id","other_ref_key":"","priority":1,"equation":"","config_type_id":1, "actor_id":1},
    ${list_configs_data}         catenate        ${list_configs_data}           {"en_label":"agent payee id","local_label":"Local agent id","value":"payee_user.id","other_ref_key":"","priority":2,"equation":"","config_type_id":1, "actor_id":1},

    ${count_item}=     set variable        2
    : FOR    ${item}    IN    @{items}
    \     ${en_label_field}=   set variable     robot_enlabel_${item}
    \     ${local_label_field}=    set variable     robot_enlabel_${item}
    \     ${count_item}=       Evaluate    ${count_item}+1
    \     ${field_item}  set variable    {"en_label":"${en_label_field}","local_label":"${local_label_field}","value":"${item}","other_ref_key":"","priority":${count_item},"equation":"+","config_type_id":2, "actor_id":1},
    \     ${list_configs_data}         catenate        ${list_configs_data}       ${field_item}
    ${list_configs_data}         catenate        ${list_configs_data}           {"en_label":"En label total amount","local_label":"Local label total amount","value":"","other_ref_key":"","priority":${count_item+1},"equation":"+","config_type_id":2, "actor_id":1}
    set test variable   ${test_service_group_id}       ${service_group_id}
    set test variable   ${test_template_id}       ${template_id}
    set test variable   ${test_author}       ${author}
    set test variable   ${test_status}       ${status}

    ${dic}       [200] API create order configuration api     ${test_service_group_id}        ${test_template_id}     ${test_author}      ${test_status}      ${test_admin_access_token}
    ${response}      get from dictionary      ${dic}        response
    ${order_configuration_id}=       set variable        ${response.json()['data']['id']}
    set test variable    ${test_order_configuration_id}       ${order_configuration_id}

    ${dic}       [200] API update order configuration api
    ...     ${test_order_configuration_id}
    ...     ${test_author}
    ...     ${test_status}
    ...     ${test_service_group_id}
    ...     ${test_admin_access_token}
    ...     ${list_configs_data}
    set test variable  ${test_result_dic}   ${dic}

[200] Agent bind a device without specifying shop id in request
    ${device_unique_reference}      generate random string  12    [LETTERS]
    ${dic}  [200] API agent bind his/her device via channel adpater
    ...     ${test_agent_access_token}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${device_unique_reference}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    set test variable  ${test_device_unique_reference}   ${device_unique_reference}
    set test variable  ${test_result_dic}   ${dic}

    ${data}       get from dictionary     ${test_result_dic}      data
    ${test_device_id}       get from dictionary     ${data}      id
    set test variable            ${test_device_id}       ${test_device_id}

[Channel_Adapter][Test][Fail] - Agent bind a device without specifying shop id in request
    ${dic}  [Fail] API agent bind his/her device via channel adpater
    ...     ${test_agent_access_token}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${login_device_id}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${test_otp_reference_id}
    set test variable  ${test_result_dic}   ${dic}

[Channel_Adapter][Test][Fail] - Agent bind a device with specifying a shop that is not exist
    ${dic}  [Fail] API agent bind his/her device via channel adpater
    ...     ${test_agent_access_token}
    ...     -1
    ...     ${param_not_used}
    ...     ${login_device_id}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${test_otp_reference_id}
    set test variable  ${test_result_dic}   ${dic}

[Channel_Adapter][Test][200] - Agent bind a device with specifying created shop
    ${dic}  [200] API agent bind his/her device via channel adpater
    ...     ${test_agent_access_token}
    ...     ${test_shop_id}
    ...     ${param_not_used}
    ...     ${login_device_id}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${test_otp_reference_id}

    set test variable  ${test_result_dic}   ${dic}

    ${data}       get from dictionary     ${test_result_dic}      data
    ${test_device_id}       get from dictionary     ${data}      id
    set test variable            ${test_device_id}       ${test_device_id}

[Fail] Agent bind a device with specifying created shop
    ${device_unique_reference}      generate random string  12    [LETTERS]
    ${dic}  [Fail] API agent bind his/her device via channel adpater
    ...     ${test_agent_access_token}
    ...     ${test_shop_id}
    ...     ${param_not_used}
    ...     ${device_unique_reference}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${test_otp_reference_id}

    set test variable  ${test_result_dic}   ${dic}

[200] Agent bind a device that already bound before
    ${dic}  [200] API agent bind his/her device via channel adpater
    ...     ${test_agent_access_token}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${test_device_unique_reference}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    set test variable  ${test_result_dic}   ${dic}

[Channel_Adapter][Test][200] - Agent get list binded devices
    ${dic}  [200] Agent get list binded devices via channel adapter         ${test_agent_access_token}
    set test variable  ${test_result_dic}   ${dic}

[Channel_Adapter][Test][200] - Agent unbind device via channel adpater
    ${dic}  [200] API agent unbind device via channel adpater
    ...     ${test_agent_access_token}
    ...     ${binded_device_id}
    ...     ${login_device_id}
    ...     ${test_otp_reference_id}
    set test variable  ${test_result_dic}   ${dic}

[Channel_Adapter][Test][Fail] - Agent unbind device via channel adpater
    ${dic}  [Fail] API agent unbind device via channel adpater
    ...     ${test_agent_access_token}
    ...     ${binded_device_id}
    ...     ${login_device_id}
    ...     ${test_otp_reference_id}
    set test variable  ${test_result_dic}   ${dic}

[Channel_Adapter][Test][200] - Agent get basic profile
    ${arg_dic}		create dictionary 		access_token=${test_agent_access_token}		username=${test_agent_username}
    ${dic}  [200] API channel gateway - agent get basic profile     ${arg_dic}
    set test variable  ${test_result_dic}   ${dic}

[Channel_Adapter][Test][200] - Agent login with pin
    ${device_id}      generate random string  4    [LETTERS]
    set test variable   ${grant_type}       password
    set test variable   ${login_device_id}       ${device_id}
    set test variable   ${channel_id}       2
    ${dic}  [200] API agent login via channel adpater
    ...     ${test_agent_username}
    ...     ${test_agent_pin_password}
    ...     pin
    ...     ${grant_type}
    ...     ${device_id}
    ...     ${channel_id}
    ...     ${param_not_used}
    ${response}    get from dictionary     ${dic}    response
    ${agent_access_token}       set variable    ${response.json()['data']['access_token']}
    set test variable      ${test_agent_access_token}      ${agent_access_token}
    set test variable  ${test_result_dic}   ${dic}

[Channel_Adapter][Test][200] - Agent login with pin on N3 device
    ${device_id}      generate random string  4    [LETTERS]
    set test variable   ${grant_type}       password
    set test variable   ${login_device_id}       ${device_id}
    set test variable   ${channel_id}       ${setup_edc_channel_id}
    ${dic}  [200] API agent login via channel adpater
    ...     ${test_agent_username}
    ...     ${test_agent_pin_password}
    ...     pin
    ...     ${grant_type}
    ...     ${device_id}
    ...     ${channel_id}
    ...     n3
    ${response}    get from dictionary     ${dic}    response
    ${agent_access_token}       set variable    ${response.json()['data']['access_token']}
    set test variable      ${test_agent_access_token}      ${agent_access_token}
    set test variable  ${test_result_dic}   ${dic}

[Channel_Adapter][Test][200] - Agent login with username
    ${device_id}      generate random string  4    [LETTERS]
    set test variable   ${grant_type}       password
    set test variable   ${login_device_id}       ${device_id}
    set test variable   ${channel_id}       2
    ${dic}  [200] API agent login via channel adpater
    ...     ${test_agent_username}
    ...     ${test_agent_password}
    ...     ${param_is_null}
    ...     ${grant_type}
    ...     ${device_id}
    ...     ${channel_id}
    ...     ${param_not_used}
    ${response}    get from dictionary     ${dic}    response
    ${agent_access_token}       set variable    ${response.json()['data']['access_token']}
    set test variable      ${test_agent_access_token}      ${agent_access_token}
    set test variable  ${test_result_dic}   ${dic}

[Channel_Adapter][Test][200] - Revoke access token when login new device
    ${dic}  [200] Revoke access token when login new device
    ...     ${test_agent_access_token}

[Channel_Adapter][Test][200] - Get mobile configuration
    ${dic}  [200] API get mobile configuration
    set test variable  ${test_result_dic}   ${dic}

[Channel_Adapter][Test][200] - Agent logout
    ${dic}  [200] API agent logout via channel adapter
    ...     ${test_agent_access_token}
    ...     ${login_device_id}
    set test variable  ${test_result_dic}   ${dic}

[Channel_Adapter][Test][Fail] - Agent logout
    ${dic}  [Fail] API agent logout via channel adapter
    ...     ${test_agent_access_token}
    ...     ${login_device_id}
    set test variable  ${test_result_dic}   ${dic}


[Channel_Adapter][Test][200] - Validate agent identity
    ${arg_dic}      create dictionary
    ...     test_national_id=${test_national_id}
    ...     test_agent_id=${test_agent_id}
    ${dic}      [200] API Validate agent identity     ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    set test variable   ${test_result_dic}      ${dic}
    set test variable   ${test_otp_reference_id}    ${response.json()['data']['otp_reference_id']}

[Channel_Adapter][Test][Fail] - Validate agent identity
    ${arg_dic}      create dictionary
    ...     test_national_id=0
    ...     test_agent_id=12345
    ${dic}      [Fail] API Validate agent identity     ${arg_dic}
    set test variable   ${test_result_dic}      ${dic}

[Channel_Adapter][Test][200] - Set new password
    ${arg_dic}      create dictionary
    ...     user_name=${test_agent_username}
    ...     new_password=${setup_agent_password_rsa_encrypted}
    ...     agent_id=${test_agent_id}
    ...     national_id=${test_national_id}
    ...     test_otp_ref_id=${test_otp_ref}
    ${dic}      [200] Set new password     ${arg_dic}
    set test variable   ${test_result_dic}      ${dic}

[Channel_Adapter][Test][Fail] - Set new password with deleted identity
    ${arg_dic}      create dictionary
    ...     user_name=${test_agent_username}
    ...     new_password=${set_new_password_encrypted}
    ...     agent_id=${test_agent_id}
    ...     national_id=${test_national_id}
    ...     test_otp_ref_id=${test_otp_ref}
    ${dic}      [Fail] Set new password with deleted identity    ${arg_dic}
    set test variable   ${test_result_dic}      ${dic}

[Channel_Adapter][Test][Fail] - Set new password with invalid credential
    ${arg_dic}      create dictionary
    ...     user_name=${test_agent_username}
    ...     new_password=${set_invalid_password_encrypted}
    ...     agent_id=${test_agent_id}
    ...     national_id=${test_national_id}
    ...     test_otp_ref_id=${test_otp_ref}
    ${dic}      [Fail] Set new password with invalid credential    ${arg_dic}
    set test variable   ${test_result_dic}      ${dic}

[Channel_Adapter][Test][200] - Change agent identity password
    ${arg_dic}  create dictionary
    ...     username=${test_agent_username}
    ...     old_password=${test_agent_password}
    ...     new_password=${setup_agent_password_rsa_encrypted}
    ...     access_token=${test_agent_access_token}
    [200] API agent change password of identity   ${arg_dic}

[Channel_Adapter][Test][Fail] - Agent change identity password with invalid credential
    ${arg_dic}  create dictionary
    ...     username=${test_agent_username}
    ...     old_password=${setup_agent_wrong_password_rsa_encrypted}
    ...     new_password=${setup_agent_password_rsa_encrypted}
    ...     access_token=${test_agent_access_token}
    ${dic}  [Fail] API agent change identity password with invalid credential   ${arg_dic}
    set test variable   ${test_result_dic}      ${dic}

[Channel_Adapter][Test][200] - Change agent identity PIN
    ${text}=   generate random string    12      [NUMBERS]
    ${new_pin}=     catenate   SEPARATOR=    pin_    ${text}
    ${arg_dic}  create dictionary
    ...     username=${test_agent_username}
    ...     password=${test_agent_password}
    ...     pin=${setup_agent_pin_rsa_encrypted}
    ...     access_token=${test_agent_access_token}
    ${test_result_dic}   [200] API agent change pin of identity   ${arg_dic}
    set test variable   ${test_result_dic}      ${test_result_dic}

[Test][Fail] Change agent identity PIN with invalid credential
    ${arg_dic}  create dictionary
    ...     username=${test_agent_username}
    ...     password=${setup_agent_wrong_pin_rsa_encrypted}
    ...     pin=${setup_agent_pin_rsa_encrypted}
    ...     access_token=${test_agent_access_token}
    ${test_result_dic}  [Fail] API agent change pin of identity   ${arg_dic}
    set test variable   ${test_result_dic}      ${test_result_dic}

[Channel_Adapter][Test][Fail] - Change agent identity PIN with deleted credential
    ${arg_dic}  create dictionary
    ...     username=${test_agent_username}
    ...     password=${test_agent_password}
    ...     pin=${setup_agent_pin_rsa_encrypted}
    ...     access_token=${test_agent_access_token}
    ${test_result_dic}  [Fail] API agent change pin of identity   ${arg_dic}
    set test variable   ${test_result_dic}      ${test_result_dic}

[Fail] Agent change identity password with invalid credential
    ${incorrect_password}=   generate random string    12      [LETTERS]
    ${arg_dic}  create dictionary
    ...     username=${test_agent_username}
    ...     old_password=${incorrect_password}
    ...     new_password=${setup_agent_password_rsa_encrypted}
    ...     access_token=${test_agent_access_token}
    [Fail] API agent change identity password with invalid credential   ${arg_dic}

[Channel_Adapter][Test][200] - Check if identity is logged in or not
    ${arg_dic}  create dictionary
    ...     agent_id=${suite_agent_id}
    ...     channel_id=2
    ...     username=${test_agent_username}
    ...     access_token=${test_agent_access_token}
    ${dic}  [200] API Check if identiy is logged in or not  ${arg_dic}
    set test variable   ${test_result_dic}      ${dic}

[Channel_Adapter][Test][200] - Call generate OTP API with access token
    ${arg_dic}  create dictionary
    ...     agent_id=${test_agent_id}
    ${dic}  [200] Call generate OTP API with access token   ${arg_dic}
    ${response}    get from dictionary     ${dic}    response
    set test variable   ${test_result_dic}      ${dic}
    set test variable   ${test_otp_reference_id}    ${response.json()['data']['otp_reference_id']}
    sleep   7s

[Channel_Adapter][Test][200] - Agent bind device
    ${dic}  [200] API agent bind his/her device via channel adpater
    ...     ${test_agent_access_token}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${login_device_id}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${test_otp_reference_id}

    ${response}    get from dictionary     ${dic}    response
    set test variable   ${test_result_dic}      ${dic}
    set test variable   ${binded_device_id}     ${response.json()['data']['id']}

[Channel_Adapter][Test][Fail] - Agent bind device
    ${dic}  [Fail] API agent bind his/her device via channel adpater
    ...     ${test_agent_access_token}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${login_device_id}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${param_not_used}
    ...     ${test_otp_reference_id}
    set test variable   ${test_result_dic}      ${dic}

[Prepare] - Create agent orders
    ${order_service_name_1}     generate random string  12    [LETTERS]
    ${order_display_service_name_1}     generate random string  12    [LETTERS]
    ${order_display_service_name_local_1}     generate random string  12    [LETTERS]

    [Prepare] - Create an order and execute order with service name          ${order_service_name_1}          ${order_display_service_name_1}         ${order_display_service_name_local_1}
    ${order_id_1}       set variable                   ${test_order_id}

[Channel_Adapter][Test][200] - Search agent orders
    ${arg_dic}  create dictionary
    ...     page_index=1
    ...     paging=true
    ...     username=${test_agent_username}
    ...     access_token=${test_agent_access_token}
    ...     service_group_id_list=${test_service_group_id}
    ${dic}  [200] API Search agent orders  ${arg_dic}

[Channel_Adapter][Test][200] - Get agent transactions summary
    ${arg_dic}  create dictionary
    ...     start_date=2018-01-23T03:06:12Z
    ...     end_date=2020-02-23T03:06:12Z
    ...     access_token=${test_agent_access_token}
    ${dic}  [200] API Get agent transactions summary  ${arg_dic}
    set test variable   ${test_result_dic}  ${dic}

[Channel_Adapter][Test][Fail] - Get agent transactions summary without date
    ${currency}     generate random string  3    [LETTERS]
    ${arg_dic}  create dictionary
    ...     start_date=null
    ...     end_date=null
    ...     access_token=${test_agent_access_token}
    ${dic}  [Fail] API Get agent transactions summary   ${arg_dic}
    set test variable   ${test_result_dic}  ${dic}

[Channel_Adapter][Test][Fail] - Get agent transactions summary with invalid date format
    ${start_date}     generate random string  12    [LETTERS]
    ${end_date}     generate random string  12    [LETTERS]
    ${arg_dic}  create dictionary
    ...     start_date=${start_date}
    ...     end_date=${end_date}
    ...     access_token=${test_agent_access_token}
    ${dic}  [Fail] API Get agent transactions summary   ${arg_dic}
    set test variable   ${test_result_dic}  ${dic}

[Channel_Adapter][Test][Fail] - Login agent with wrong password
    ${device_id}      generate random string  4    [LETTERS]
    set test variable   ${grant_type}       password
    set test variable   ${channel_id}       2
    ${dic}  [Fail] API agent login via channel adpater
    ...     ${test_agent_username}
    ...     ${setup_agent_wrong_password_encrypted}
    ...     password
    ...     ${grant_type}
    ...     ${device_id}
    ...     ${channel_id}
    ...     ${param_not_used}
    ${response}    get from dictionary     ${dic}    response
    log      ${dic}
    set test variable  ${test_result_dic}   ${dic}

[Channel_Adapter][Test][Fail] - Login agent with wrong password 6 times
    :FOR    ${index}    IN RANGE    1  6
     \        [Channel_Adapter][Test][Fail] - Login agent with wrong password

[Channel_Adapter][Test][200] - Get OTP code from mock server
    ${dic}    [200] Get OTP code from mock server
    ${response}    get from dictionary    ${dic}   response
    set test variable    ${test_otp_code}     ${response.json()['data']['otp_code']}
    set test variable    ${test_user_reference_code}   ${response.json()['data']['user_reference_code']}
    set test variable    ${test_otp_ref}   ${response.json()['data']['otp_reference_id']}

[Channel_Adapter][Test][200] - Generate new device id
    ${device_id}      generate random string  4    [LETTERS]
    set test variable   ${login_device_id}       ${device_id}

[Channel_Adapter][Test][200] - Call Get order detail api
    ${arg_dic}  create dictionary
    ...     order_id=${test_order_id}
    ...     access_token=${test_agent_access_token}
    ${dic}      [200] API get order detail via channel adapter      ${arg_dic}
    set test variable   ${test_result_dic}      ${dic}

[Channel_Adapter][Test][200] - Call Get order detail api with pre and post wallet balance
    ${arg_dic}  create dictionary
    ...     order_id=${test_order_id}
    ...     access_token=${test_payer_access_token}
    ${dic}      [200] API get order detail via channel adapter      ${arg_dic}
    set test variable   ${test_result_dic}      ${dic}

[Channel_Adapter][Test][Fail] - Call get order detail api via channel adapter with invalid order id
    ${order_id}     generate random string  12    [LETTERS]
    ${arg_dic}  create dictionary
    ...     order_id=${order_id}
    ...     access_token=${test_agent_access_token}
    ${dic}      [Fail] API get order detail via channel adapter      ${arg_dic}
    set test variable   ${test_result_dic}      ${dic}

[Channel_Adapter][Test][200] - Get order detail by printing configuration
    ${arg_dic}  create dictionary
    ...     order_id=${test_order_id}
    ...     access_token=${test_agent_access_token}
    ${dic}      [200] API get order detail for printing    ${arg_dic}
    set test variable   ${test_result_dic}      ${dic}

[Channel_Adapter][Test][Fail] - Get order detail by printing configuration with unexist order id
    ${order_id}     generate random string  12    [LETTERS]
    ${arg_dic}  create dictionary
    ...     order_id=${order_id}
    ...     access_token=${test_agent_access_token}
    ${dic}      [Fail] API get order detail for printing      ${arg_dic}
    set test variable   ${test_result_dic}      ${dic}

[Channel_Adapter][Test][200] - Get transaction history of agent
    ${arg_dic}  create dictionary
    ...     access_token=${test_agent_access_token}
    ${dic}  [200] API get order list    ${arg_dic}
    set test variable   ${test_result_dic}      ${dic}

[Channel_Adapter][Test][Fail] - Get order list of agent
    ${arg_dic}  create dictionary
    ...     access_token=${test_agent_access_token}
    ${dic}  [Fail] API get order list    ${arg_dic}
    set test variable   ${test_result_dic}      ${dic}

[Channel_Adapter][Test][200] - Call verify smart card api
    ${arg_dic}      create dictionary
    ...     card_serial_number=${test_card_number}
    ...     edc_serial_number=${test_edc_serial_number}
    ${dic}      [200] API Verify smartcard with edc device     ${arg_dic}

[Channel_Adapter][Test][200] - Call get service group list API
    ${arg_dic}  create dictionary
    ...     access_token=${test_admin_access_token}
    ${dic}  [200] API Get Service Group List Via Channel Adapter    ${arg_dic}
    set test variable   ${test_result_dic}      ${dic}

[Channel_Adapter][Test][200] - Call login with agent identity
    ${dic}     [200] Agent login with smart card
    ${access_token}    get from dictionary      ${dic}        access_token
    set test variable   ${agent_access_token}      ${access_token}
    set test variable   ${test_result_dic}      ${dic}

[Channel_Adapter][Test][Fail] - Call login with un-exist serial number
    ${serial_number}=   generate random string    8       [NUMBERS]
    ${arg_dic}  create dictionary
    ...     serial_number=${serial_number}
    ...     device_id=${test_edc_serial_number}
    ${dic}     [Fail] Agent login with smart card  ${arg_dic}
    set test variable   ${test_result_dic}      ${dic}

[Channel_Adapter][Test][Fail] - Call login api with deleted device
    ${arg_dic}  create dictionary
    ...     serial_number=${test_card_number}
    ...     device_id=${test_edc_serial_number}
    ${dic}     [Fail] Agent login with smart card  ${arg_dic}
    set test variable   ${test_result_dic}      ${dic}


[Channel_Adapter][Test][200] - Create agent identity with username is smart card serial number
    ${dic}  [200] API add agent identity
    ...     ${test_agent_id}
    ...     11
    ...     ${test_card_number}
    ...     ${setup_agent_pin_encrypted}
    ...     ${suite_admin_access_token}
    ${response}    get from dictionary     ${dic}    response
    log  ${response.text}
    should be equal as integers     ${response.status_code}    200
    [Return]   ${dic}

[Channel_Adapter][Test][Fail] - Call verify smart card api with channel type is mobile
    ${arg_dic}      create dictionary
    ...     card_serial_number=${test_card_number}
    ...     edc_serial_number=${test_edc_serial_number}
    ...     channel_id=${setup_mobile_channel_id}
    ${dic}      [Fail] API Verify smartcard with Invalid Request       ${arg_dic}
    set test variable   ${test_result_dic}      ${dic}

[Channel_Adapter][Test][Fail] - Call verify smart card api with un-exist serial card number
    ${invalid_card_number}      generate random string  8    [NUMBERS]
    ${test_invalid_card_number}=     set variable      1${invalid_card_number}
    ${arg_dic}      create dictionary
    ...     card_serial_number=${test_invalid_card_number}
    ...     edc_serial_number=${test_edc_serial_number}
    ...     channel_id=${setup_edc_channel_id}
    ${dic}      [Fail] API Verify smartcard with Invalid Request     ${arg_dic}
     set test variable   ${test_result_dic}      ${dic}

[Channel_Adapter][Test][Fail] - Call verify smart card api
    ${arg_dic}      create dictionary
    ...     card_serial_number=${test_card_number}
    ...     edc_serial_number=${test_edc_serial_number}
    ...     channel_id=${setup_edc_channel_id}
    ${dic}      [Fail] API Verify smartcard with Invalid Request     ${arg_dic}
    set test variable     ${test_result_dic}            ${dic}

[Channel_Adapter][Test][200] - Delete smart card via api
    ${arg_dic}    create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     agent_id=${test_agent_id}
    ...     smart_card_id=${test_smart_card_id}
    [200] API delete smart card  ${arg_dic}
    
[Channel_Adapter][Test][200] - Call create agent identity api with channel type is EDC
    ${arg_dic}      create dictionary
    ...     header_channel_id=1
    ...     header_device_unique_reference=${test_edc_serial_number}
    ...     username=${test_card_number}
    ...     password=${setup_agent_password_encrypted}
    ...     agent_id=${test_agent_id}
    ...     otp_reference_id=${test_otp_reference_id}
    ...     identity_type=EDC
    ${dic}      [200] API Create agent identity via channel adapter     ${arg_dic}

[Channel_Adapter][Test][Fail] - Call create agent identity api with channel type is EDC
    ${arg_dic}      create dictionary
    ...     header_channel_id=1
    ...     header_device_unique_reference=${test_edc_serial_number}
    ...     username=${test_card_number}
    ...     password=${setup_agent_password_encrypted}
    ...     agent_id=${test_agent_id}
    ...     otp_reference_id=${test_otp_reference_id}
    ...     identity_type=EDC
    ${dic}      [Fail] API Create agent identity via channel adapter     ${arg_dic}
    set test variable     ${test_result_dic}            ${dic}

[Test] Set login device from setup device
    set test variable   ${login_device_id}       ${setup_device_id}

[Channel_Adapter][Test][200] - Call get list contact us api for sale
    ${arg_dic}      create dictionary
    ...     lang=en
    ...     user_type=sale
    ...     access_token=${test_admin_access_token}
    ${dic}      [200] API get list contact us     ${arg_dic}

[Channel_Adapter][Test][200] - Call get list FAQ api for sale
    ${arg_dic}  create dictionary
    ...     access_token=${test_admin_access_token}
    ...     lang=en
    ...     user_type=sale
    ${dic}  [200] API get list faqs   ${arg_dic}
    set test variable   ${test_result_dic}            ${dic}

[Channel_Adapter][Test][200] - Call get list contact us api for agent
    ${arg_dic}      create dictionary
    ...     lang=en
    ...     user_type=agent
    ...     access_token=${test_admin_access_token}
    ${dic}      [200] API get list contact us     ${arg_dic}
    set test variable   ${test_result_dic}            ${dic}

[Channel_Adapter][Test][200] - Call get list FAQ api for agent
    ${arg_dic}  create dictionary
    ...     access_token=${test_admin_access_token}
    ...     lang=en
    ...     user_type=agent
    ${dic}  [200] API get list faqs   ${arg_dic}
    set test variable   ${test_result_dic}            ${dic}

[Channel_Adapter][Test][200] - Call get list contact us api for agent with un-exist language
    ${lang}=   generate random string    5      [LETTERS]
    ${arg_dic}      create dictionary
    ...     lang=${lang}
    ...     user_type=agent
    ...     access_token=${test_admin_access_token}
    ${dic}      [Fail] API get list contact us     ${arg_dic}
    set test variable   ${test_result_dic}            ${dic}

[Channel_Adapter][Test][200] - Call get list faq api for agent with un-exist language
    ${lang}=   generate random string    5      [LETTERS]
    ${arg_dic}      create dictionary
    ...     lang=${lang}
    ...     user_type=agent
    ...     access_token=${test_admin_access_token}
    ${dic}      [Fail] API get list faqs     ${arg_dic}
    set test variable   ${test_result_dic}            ${dic}

[Channel_Adapter][Test][200] - Generate QR code
    ${arg_dic}  create dictionary
    ...     access_token=${test_admin_access_token}
    ${dic}  [200] API Generate QR code     ${arg_dic}
    set test variable   ${test_result_dic}            ${dic}

[Channel_Adapter][Test][200] - Verify QR code
    ${arg_dic}  create dictionary
    ...     access_token=${test_admin_access_token}
    ...     qr_code_id=${qr_code_id}
    ...     qr_code_created_timestamp=${qr_code_created_timestamp}
    ${dic}  [200] API Verify QR code     ${arg_dic}
    set test variable   ${test_result_dic}            ${dic}

[Channel_Adapter][Test][200] - Create normal order via channel adapter
    ${payer_user_ref_type}=    set variable    access token
    ${payer_user_ref_value}=    set variable   ${test_payer_access_token}
    ${payer_user_id}=    set variable    ${test_payer_id}
    ${payer_user_type}=    set variable    agent
    ${payee_user_ref_type}=    set variable    ${param_not_used}
    ${payee_user_ref_value}=    set variable    ${param_not_used}
    ${payee_user_id}=    set variable    ${test_payee_id}
    ${payee_user_type}=    set variable    agent
    ${note}         generate random string      20      prepare_product_name_[LETTERS]

    ${arg_dic}  create dictionary
    ...     payer_user_ref_type=${payer_user_ref_type}
    ...     payer_user_ref_value=${payer_user_ref_value}
    ...     payer_user_id=${payer_user_id}
    ...     payer_user_type=${payer_user_type}
    ...     payee_user_id=${payee_user_id}
    ...     payee_user_type=${payee_user_type}
    ...     payee_user_ref_type=${payee_user_ref_type}
    ...     payee_user_ref_value=${payee_user_ref_value}
    ...     amount=10000
    ...     note=${note}
    ...     currency=VND

    ${dic}    [200] API Create normal order via channel adapter    ${arg_dic}
    ${order_id}=  get from dictionary   ${dic}   order_id
    set test variable   ${test_order_id}      ${order_id}

[Channel_Adapter][Test][200] - Create normal order via channel adapter with arguments
    [arguments]     &{arg_dic}
    ${payer_user_ref_type}=    set variable    access token
    ${payer_user_ref_value}=    set variable   ${arg_dic.test_payer_access_token}
    ${payer_user_id}=    set variable    ${arg_dic.test_payer_id}
    ${payer_user_type}=    set variable    agent
    ${payee_user_ref_type}=    set variable    ${param_not_used}
    ${payee_user_ref_value}=    set variable    ${param_not_used}
    ${payee_user_id}=    set variable    ${arg_dic.test_payee_id}
    ${payee_user_type}=    set variable    agent
    ${note}         generate random string      20      prepare_product_name_[LETTERS]

    ${arg_dic}  create dictionary
    ...     payer_user_ref_type=${payer_user_ref_type}
    ...     payer_user_ref_value=${payer_user_ref_value}
    ...     payer_user_id=${payer_user_id}
    ...     payer_user_type=${payer_user_type}
    ...     payee_user_id=${payee_user_id}
    ...     payee_user_type=${payee_user_type}
    ...     payee_user_ref_type=${payee_user_ref_type}
    ...     payee_user_ref_value=${payee_user_ref_value}
    ...     amount=10000
    ...     note=${note}
    ...     currency=VND

    ${dic}    [200] API Create normal order via channel adapter    ${arg_dic}
    ${order_id}=  get from dictionary   ${dic}   order_id
    set test variable   ${test_order_id}      ${order_id}

[Channel_Adapter][Test][Fail] - Create normal order with un-exist currency via channel adapter
    ${payer_user_ref_type}=    set variable    access token
    ${payer_user_ref_value}=    set variable   ${test_payer_access_token}
    ${payer_user_id}=    set variable    ${test_payer_id}
    ${payer_user_type}=    set variable    agent
    ${payee_user_ref_type}=    set variable    ${param_not_used}
    ${payee_user_ref_value}=    set variable    ${param_not_used}
    ${payee_user_id}=    set variable    ${test_payee_id}
    ${payee_user_type}=    set variable    agent
    ${note}         generate random string      20      prepare_product_name_[LETTERS]
    ${currency}         generate random string      3      [LETTERS]

    ${arg_dic}  create dictionary
    ...     payer_user_ref_type=${payer_user_ref_type}
    ...     payer_user_ref_value=${payer_user_ref_value}
    ...     payer_user_id=${payer_user_id}
    ...     payer_user_type=${payer_user_type}
    ...     payee_user_id=${payee_user_id}
    ...     payee_user_type=${payee_user_type}
    ...     payee_user_ref_type=${payee_user_ref_type}
    ...     payee_user_ref_value=${payee_user_ref_value}
    ...     amount=10000
    ...     note=${note}
    ...     currency=${currency}

    ${dic}    [Fail] API Create normal order via channel adapter    ${arg_dic}
    set test variable   ${test_result_dic}      ${dic}

[Channel_Adapter][Test][200] - Execute order via channel adapter
    ${arg_dic}  create dictionary
    ...     security=${setup_payment_pin_encrypted}
    ...     order_id=${test_order_id}
    ...     access_token=${test_payer_access_token}
    ${dic}     [200] API execute order via channel adapter      ${arg_dic}

[Channel_Adapter][Test][Fail] - Execute order via channel adapter
    ${arg_dic}  create dictionary
    ...     security=${setup_payment_pin_encrypted}
    ...     order_id=${test_order_id}
    ...     access_token=${test_admin_access_token}
    ${dic}     [Fail] API execute order via channel adapter      ${arg_dic}
    set test variable   ${test_result_dic}      ${dic}

[Channel_Adapter][Test][Fail] - Verify QR code
    ${arg_dic}  create dictionary
    ...     access_token=${test_admin_access_token}
    ...     qr_code_id=${qr_code_id}
    ...     qr_code_created_timestamp=${qr_code_created_timestamp}
    ${dic}  [Fail] API Verify QR code     ${arg_dic}
    set test variable   ${test_result_dic}            ${dic}

[Channel_Adapter][Test][Fail] - Verify QR code with unexisted id
    ${random_num}           generate random string      8      [NUMBERS]
    ${arg_dic}  create dictionary
    ...     access_token=${test_admin_access_token}
    ...     qr_code_id=${random_num}
    ...     qr_code_created_timestamp=${qr_code_created_timestamp}
    ${dic}  [Fail] API Verify QR code     ${arg_dic}
    set test variable   ${test_result_dic}            ${dic}

[Channel_Adapter][Test][200] - Set payment pin for payer
    [Test][200] API create payment pin
    ...     ${setup_payment_pin_encrypted}
    ...     ${test_payer_access_token}

[Channel_Adapter][Test][200] - Get extension order fields via channel adapter
    [Arguments]     ${headers}=${SUITE_ADMIN_HEADERS}
    [Channel_Adapter][200] - Get extension order fields via channel adapter
    ...     ${headers}
