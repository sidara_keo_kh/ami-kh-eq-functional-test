*** Settings ***
Resource         ../../2_api/3_reuse_function/imports.robot

*** Keywords ***
[Test][200] API suspend sof cash
    [200] API suspend sof cash
    set test variable  ${test_result_dic}    ${dic}

[Test][Fail] API suspend sof cash with unexist sofid
    ${dic}  [Fail] API suspend sof cash with unexist sofid
    set test variable  ${test_result_dic}    ${dic}

[Test][200] API update sof cash prefill amount
    ${prefill_amount}=    set variable    1000
    ${dic}    Create Dictionary
        ...     sof_cash_id=${suite_agent_sof_id_VN}
        ...     access_token=${suite_admin_access_token}
        ...     header_client_id=${setup_admin_client_id}
        ...     header_client_secret=${setup_admin_client_secret}
        ...     prefill_amount=${prefill_amount}
    ${dic_result}    [200] API update sof cash prefill amount    ${dic}
    set test variable  ${test_result_dic}    ${dic_result}
    set test variable  ${suite_prefill_amount}    ${prefill_amount}
    [Return]    ${dic_result}

[Test][Fail] API update sof cash prefill amount with unexist sofid
    ${sof_cash_id}    generate random string    8
    ${dic}    Create Dictionary
        ...     sof_cash_id=${sof_cash_id}
        ...     access_token=${suite_admin_access_token}
        ...     header_client_id=${setup_admin_client_id}
        ...     header_client_secret=${setup_admin_client_secret}
        ...     prefill_amount=1000
    ${dic_result}    [Fail] API update sof cash prefill amount    ${dic}
    set test variable  ${test_result_dic}    ${dic_result}
    [Return]    ${dic_result}

[Test][Fail] API update sof cash prefill amount with neagetive number
    ${dic}    Create Dictionary
        ...     sof_cash_id=${suite_agent_sof_id_VN}
        ...     access_token=${suite_admin_access_token}
        ...     header_client_id=${setup_admin_client_id}
        ...     header_client_secret=${setup_admin_client_secret}
        ...     prefill_amount=-1000
    ${dic_result}    [Fail] API update sof cash prefill amount    ${dic}
    set test variable  ${test_result_dic}    ${dic_result}
    [Return]    ${dic_result}

[Test][Fail] API update sof cash prefill amount with zero value
    ${dic}    Create Dictionary
        ...     sof_cash_id=${suite_agent_sof_id_VN}
        ...     access_token=${suite_admin_access_token}
        ...     header_client_id=${setup_admin_client_id}
        ...     header_client_secret=${setup_admin_client_secret}
        ...     prefill_amount=0
    ${dic_result}    [Fail] API update sof cash prefill amount    ${dic}
    set test variable  ${test_result_dic}    ${dic_result}
    [Return]    ${dic_result}

[Test][Fail] API update sof cash prefill amount with text value
    ${prefill_amount}    generate random string    10
    ${dic}    Create Dictionary
        ...     sof_cash_id=${suite_agent_sof_id_VN}
        ...     access_token=${suite_admin_access_token}
        ...     header_client_id=${setup_admin_client_id}
        ...     header_client_secret=${setup_admin_client_secret}
        ...     prefill_amount=${prefill_amount}
    ${dic_result}    [Fail] API update sof cash prefill amount    ${dic}
    set test variable  ${test_result_dic}    ${dic_result}
    [Return]    ${dic_result}

[Test][Fail] API update sof cash prefill amount with more decimal than config to specific sof cash
    ${dic}    Create Dictionary
        ...     sof_cash_id=${suite_agent_sof_id_VN}
        ...     access_token=${suite_admin_access_token}
        ...     header_client_id=${setup_admin_client_id}
        ...     header_client_secret=${setup_admin_client_secret}
        ...     prefill_amount=1000.111111
    ${dic_result}    [Fail] API update sof cash prefill amount    ${dic}
    set test variable  ${test_result_dic}    ${dic_result}
    [Return]    ${dic_result}

[Test][200] Save prefill amount is null to specific sof cash
    ${dic}    Create Dictionary
        ...     sof_cash_id=${suite_agent_sof_id_VN}
        ...     access_token=${suite_admin_access_token}
        ...     header_client_id=${setup_admin_client_id}
        ...     header_client_secret=${setup_admin_client_secret}
        ...     prefill_amount=null
    ${dic_result}    [200] API update sof cash prefill amount    ${dic}
    set test variable  ${test_result_dic}    ${dic_result}
    [Return]    ${dic_result}

[Test][200] Save prefill amount is empty to specific sof cash
    ${dic}    Create Dictionary
        ...     sof_cash_id=${suite_agent_sof_id_VN}
        ...     access_token=${suite_admin_access_token}
        ...     header_client_id=${setup_admin_client_id}
        ...     header_client_secret=${setup_admin_client_secret}
        ...     prefill_amount=\"\"
    ${dic_result}    [200] API update sof cash prefill amount    ${dic}
    set test variable  ${test_result_dic}    ${dic_result}
    [Return]    ${dic_result}

[Test][200] Admin create user sof cash
    [Arguments]   ${headers}=${SUITE_ADMIN_HEADERS}        &{arg_dic}
    [Payment][Prepare] - system user create user sof cash - body
        ...     user_id=${arg_dic.user_id}
        ...     user_type_id=${arg_dic.user_type_id}
        ...     currency=${arg_dic.currency}
    [Payment][200] - system user create user sof cash
        ...     headers=${SUITE_ADMIN_HEADERS}
        ...     body=${body}