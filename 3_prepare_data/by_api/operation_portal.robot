*** Settings ***
Resource         ../../2_api/3_reuse_function/imports.robot

*** Keywords ***
[Suite][200] create company profiles
  ${name}       generate random string    20  [LETTERS]
  ${first_name}       generate random string    20  [LETTERS]
  ${middle_name}       generate random string    20  [LETTERS]
  ${last_name}       generate random string    20  [LETTERS]
  ${business_type}       generate random string    20  [LETTERS]
  ${address}       generate random string    20  [LETTERS]
  ${city}       generate random string    20  [LETTERS]
  ${country}       generate random string    20  [LETTERS]
  ${postal_code}       generate random string    20  [LETTERS]
  ${mobile_numbers}       generate random string    10  [NUMBERS]

  ${current_date}    Get Current Date
  ${current_date}    Convert Date    ${current_date}     result_format=${datetime_format}
  set suite variable     ${register_date}     ${current_date}
  set suite variable     ${end_date}     ${current_date}
  set suite variable     ${sign_date}     ${current_date}
  set suite variable     ${issue_date}     ${current_date}
  set suite variable     ${expired_date}     ${current_date}
  set suite variable     ${date_of_birth}     ${current_date}

  ${arg_dic}            create dictionary
        ...     access_token=${suite_admin_access_token}
        ...     name=${name}
        ...     first_name=${first_name}
        ...     middle_name=${middle_name}
        ...     last_name=${last_name}
        ...     business_type=${business_type}
        ...     company_type_id=${suite_company_type_id_variable}
        ...     address=${address}
        ...     city=${city}
        ...     country=${country}
        ...     postal_code=${postal_code}
        ...     mobile_number=${mobile_numbers}
        ...     register_date=${register_date}
        ...     end_date=${end_date}
        ...     sign_date=${sign_date}
        ...     issue_date=${issue_date}
        ...     expired_date=${expired_date}
        ...     date_of_birth=${date_of_birth}

  ${return_dic}         [200] API create company profiles   ${arg_dic}
  set suite variable    ${suite_company_id}   ${return_dic.id}
  set suite variable    ${suite_company_name}   ${name}
  set suite variable    ${suite_company_owner_first_name}   ${first_name}
  set suite variable    ${suite_company_owner_middle_name}   ${middle_name}
  set suite variable    ${suite_company_owner_last_name}   ${last_name}
  set suite variable    ${suite_company_business_type}   ${business_type}
  set suite variable    ${suite_company_address}   ${address}
  set suite variable    ${suite_company_city}   ${city}
  set suite variable    ${suite_company_country}   ${country}
  set suite variable    ${suite_company_postal_code}   ${postal_code}
  set suite variable    ${suite_company_mobile_numbers}   ${mobile_numbers}

[Suite][200] link agent profile with company profile
  ${arg_dic}            create dictionary    access_token=${suite_admin_access_token}  company_id=${suite_company_id}   agent_id=${suite_agent_id}
  [200] API link agent profile with company profile     ${arg_dic}

[Test][200] create company profiles
  ${name}       generate random string    20  [LETTERS]
  ${arg_dic}            create dictionary    access_token=${suite_admin_access_token}  name=${name}
  ${return_dic}         [200] API create company profiles   ${arg_dic}
  set test variable    ${test_company_id}   ${return_dic.id}
  set test variable    ${test_company_name}   ${name}

[Test][200] link agent profile with company profile
  ${arg_dic}            create dictionary    access_token=${suite_admin_access_token}  company_id=${test_company_id}   agent_id=${test_agent_id}
  [200] API link agent profile with company profile     ${arg_dic}

[Test][200] search agent belong to company
  ${arg_dic}            create dictionary    access_token=${suite_admin_access_token}  company_id=${test_company_id}
  [200] API search agent belong to company    ${arg_dic}

[Suite][200] create system user account with full permissions for operation portal
  [System_User][Suite][200] - create account login via api

[Suite][200] Get date time format configuration
    ${dic}          [200] API get all configurations        ${suite_admin_access_token}     global
    ${date_format_value}        get from dictionary     ${dic.data}     date-format
    ${time_format_value}        get from dictionary     ${dic.data}     time-format
    set suite variable       ${suite_date_format_variable}      ${date_format_value}
    set suite variable       ${suite_time_format_variable}      ${time_format_value}
    set suite variable       ${suite_date_time_format_variable}      ${date_format_value} ${time_format_value}