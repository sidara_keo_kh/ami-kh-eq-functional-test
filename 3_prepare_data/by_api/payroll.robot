*** Settings ***
Resource         ../../2_api/3_reuse_function/imports.robot

*** Keywords ***
[Test][200] create and execute "Cashout payment" order
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set test variable      ${test_ext_transaction_id}     ${text}
    set test variable      ${test_product_name}     ${text}
    set test variable      ${test_product_ref_1}     ${text}
    set test variable      ${test_payer_user_id}     ${num}
    ${arg_dic}      create dictionary   access_token=${suite_water_agent_access_token}    ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${suite_service_id}      product_service_name=${suite_service_name}
    ...     payer_user_ref_type=payroll card number       payer_user_ref_value=${suite_payroll_card_number}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}        payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_water_agent_id}       payee_user_type=${suite_water_agent_type}
    ...     amount=10
    ${dic}      [200] API bank agent create and execute payroll order        ${arg_dic}
    set test variable     ${test_payroll_order_id}       ${dic.order_id}

[Test][200] create cashout payment order then execute
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set test variable      ${test_ext_transaction_id}     ${text}
    set test variable      ${test_product_name}     ${text}
    set test variable      ${test_product_ref_1}     ${text}
    set test variable      ${test_payer_user_id}     ${num}
    ${arg_dic}      create dictionary   access_token=${test_water_agent_access_token}    ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${test_service_id}      product_service_name=${test_service_name}
    ...     payer_user_ref_type=payroll card number       payer_user_ref_value=${test_payroll_card_number}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}        payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${test_water_agent_id}       payee_user_type=${test_water_agent_type}
    ...     amount=10
    ${dic}      [200] API bank agent create and execute payroll order        ${arg_dic}
    set test variable     ${test_payroll_order_id}       ${dic.order_id}

[Test][200] create cashout payment order with requestor then execute
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set test variable      ${test_ext_transaction_id}     ${text}
    set test variable      ${test_product_name}     ${text}
    set test variable      ${test_product_ref_1}     ${text}
    set test variable      ${test_payer_user_id}     ${num}
    ${arg_dic}      create dictionary   access_token=${test_water_agent_access_token}    ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${test_service_id}      product_service_name=${test_service_name}
    ...     payer_user_ref_type=payroll card number       payer_user_ref_value=${test_payroll_card_number}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}        payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${test_water_agent_id}       payee_user_type=${test_water_agent_type}
    ...     amount=10
    ...     requestor_user_user_id=${test_water_agent_id}
    ...     requestor_user_user_type=agent
    ...     requestor_user_sof_id=${test_water_sof_cash_id}
    ...     requestor_user_sof_type_id=2

    ${dic}      [200] API bank agent create and execute payroll order        ${arg_dic}
    set test variable     ${test_payroll_order_id}       ${dic.order_id}
    set test variable     ${test_order_id}       ${dic.order_id}