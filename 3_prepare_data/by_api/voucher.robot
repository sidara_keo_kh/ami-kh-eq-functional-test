*** Settings ***
Resource         ../../2_api/3_reuse_function/imports.robot

*** Keywords ***
[Agent][Suite][200] - create "Marketing Wallet" agent in "${currency}" currency
    ${text}=   generate random string    10      [LETTERS]
    ${agent_type_name}      set variable   	${text}
    ${agent_type_id}      set variable   	${setup_company_agent_type_id}
    ${agent_unique_reference}      set variable   	${text}
    ${agent_username}     set variable    ${text}
    ${agent_password}      set variable   	${setup_agent_password_encrypted}
    ${agent_password_login}     set variable    ${setup_agent_password_encrypted_utf8}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      agent_type_name=${agent_type_name}
    ...     unique_reference=${agent_unique_reference}      identity_identity_type_id=1
    ...     agent_username=${agent_username}      agent_password=${agent_password}
    ...     agent_password_login=${agent_password_login}      currency=${currency}
    ...     user_type_id=2

    ${dic}  [Agent][Reuse][200] - create agent      ${arg_dic}
    set suite variable      ${suite_marketing_wallet_agent_id}       ${dic.agent_id}
    set suite variable      ${suite_marketing_wallet_agent_type}       agent
    set suite variable      ${suite_marketing_wallet_sof_cash_id}       ${dic.sof_cash_id_in_${currency}}
    set suite variable      ${suite_marketing_wallet_agent_access_token}     ${dic.agent_access_token}

[Agent][Suite][200] - create "Cashback Voucher Wallet" agent in "${currency}" currency
   ${text}=   generate random string    10      [LETTERS]
   ${agent_type_name}      set variable   	${text}
   ${agent_type_id}      set variable   	${setup_company_agent_type_id}
   ${agent_unique_reference}      set variable   	${text}
   ${agent_username}     set variable    ${text}
   ${agent_password}      set variable   	${setup_agent_password_encrypted}
   ${agent_password_login}     set variable    ${setup_agent_password_encrypted_utf8}

   ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      agent_type_name=${agent_type_name}
   ...     unique_reference=${agent_unique_reference}      identity_identity_type_id=1
   ...     agent_username=${agent_username}      agent_password=${agent_password}
   ...     agent_password_login=${agent_password_login}      currency=${currency}
   ...     user_type_id=2

   ${dic}  [Agent][Reuse][200] - create agent      ${arg_dic}
    set suite variable      ${suite_cashback_voucher_wallet_agent_id}       ${dic.agent_id}
    set suite variable      ${suite_cashback_voucher_wallet_agent_type}     agent

[Payment][Suite][200] - create cashback service structure in "${currency}" currency
   ${text}=   generate random string    10      [LETTERS]
   ${number}=   generate random string    1      123
   ${number1}=   generate random string    3      123456789
   ${service_group_name}      set variable   	${text}
   ${service_name}      set variable   	${text}
   ${currency}      set variable   	${currency}
   ${command_id}     set variable    1
   ${fee_tier_condition}     set variable    unlimit
   ${condition_amount}     set variable    null
   ${fee_type}     set variable    % rate
   ${fee_amount}     set variable    10
   ${bonus_type}     set variable    NON
   ${bonus_amount}     set variable    null
   ${amount_type}     set variable    null
   ${settlement_type}     set variable    Amount

   ${post_payment_spi_url_type}     set variable     Post-Payment
   ${post_payment_url}     set variable      ${internal_voucher_cashback_generate_url}
   ${post_payment_spi_url_call_method}     set variable      synchronous
   ${post_payment_expire_in_minute}     set variable     ${number}
   ${post_payment_max_retry}     set variable        ${number}
   ${post_payment_retry_delay_millisecond}     set variable      ${number1}
   ${post_payment_read_timeout}     set variable      120000

    ${debit}  set variable    {"action_type":"debit","actor_type":"Payer","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    ${credit}  set variable    {"action_type":"credit","actor_type":"Payee","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${debit}  ${credit}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}    service_group_name=${service_group_name}
    ...     service_name=${service_name}    currency=${currency}    command_id=${command_id}    fee_tier_condition=${fee_tier_condition}
    ...     condition_amount=${condition_amount}        fee_type=${fee_type}    fee_amount=${fee_amount}
    ...     bonus_type=${bonus_type}        bonus_amount=${bonus_amount}    amount_type=${amount_type}      settlement_type=${settlement_type}
    ...     post_payment_spi_url_type=${post_payment_spi_url_type}    post_payment_url=${post_payment_url}      post_payment_spi_url_call_method=${post_payment_spi_url_call_method}      post_payment_expire_in_minute=${post_payment_expire_in_minute}
    ...     post_payment_read_timeout=${post_payment_read_timeout}    post_payment_max_retry=${post_payment_max_retry}      post_payment_retry_delay_millisecond=${post_payment_retry_delay_millisecond}
    ...     balance_distributions=@{balance_distributions}

   ${return_dic}   [Payment][Reuse][200] - create service structure        ${arg_dic}

    set suite variable      ${suite_cashback_cash_in_service_id}        ${return_dic.service_id}
    set suite variable      ${suite_cashback_cash_in_service_name}        ${return_dic.service_name}

[Payment][Suite][200] - create normal service structure in "${currency}" currency
   ${text}=   generate random string    10      [LETTERS]
   ${number}=   generate random string    1      123
   ${number1}=   generate random string    3      123456789
   ${service_group_name}      set variable   	${text}
   ${service_name}      set variable   	${text}
   ${currency}      set variable   	${currency}
   ${command_id}     set variable    1
   ${fee_tier_condition}     set variable    unlimit
   ${condition_amount}     set variable    null
   ${fee_type}     set variable    % rate
   ${fee_amount}     set variable    10
   ${bonus_type}     set variable    NON
   ${bonus_amount}     set variable    null
   ${amount_type}     set variable    null
   ${settlement_type}     set variable    Amount

    ${debit}  set variable    {"action_type":"debit","actor_type":"Payer","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    ${credit}  set variable   {"action_type":"credit","actor_type":"Payee","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${debit}  ${credit}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}    service_group_name=${service_group_name}
    ...     service_name=${service_name}    currency=${currency}    command_id=${command_id}    fee_tier_condition=${fee_tier_condition}
    ...     condition_amount=${condition_amount}        fee_type=${fee_type}    fee_amount=${fee_amount}
    ...     bonus_type=${bonus_type}        bonus_amount=${bonus_amount}    amount_type=${amount_type}      settlement_type=${settlement_type}
    ...     balance_distributions=${balance_distributions}

   ${return_dic}   [Payment][Reuse][200] - create service structure       ${arg_dic}

    set suite variable      ${suite_service_id}        ${return_dic.service_id}
    set suite variable      ${suite_service_id_${currency}}        ${return_dic.service_id}
    set suite variable      ${suite_service_name}        ${return_dic.service_name}
    set suite variable      ${suite_service_name_${currency}}        ${return_dic.service_name}
    set suite variable      ${suite_service_group_id}        ${return_dic.service_group_id}
    set suite variable      ${suite_service_group_name}        ${return_dic.service_group_name}
    set suite variable      ${suite_service_payment_fee_tier_id}        ${return_dic.fee_tier_id}
    set suite variable      ${suite_service_payment_command_id}        ${return_dic.service_command_id}

[Payment][Test][200] - create normal service structure with 2 fee tiers for payment command
    [Arguments]     ${currency}
   ${text}=   generate random string    10      [LETTERS]
   ${number}=   generate random string    1      123
   ${number1}=   generate random string    3      123456789
   ${service_group_name}      set variable   	${text}
   ${service_name}      set variable   	${text}
   ${currency}      set variable   	${currency}
   ${command_id}     set variable    1
   ${fee_tier_condition_1}     set variable    unlimit
   ${condition_amount_1}     set variable    null
   ${fee_type_1}     set variable    % rate
   ${fee_amount_1}     set variable    10
   ${bonus_type_1}     set variable    NON
   ${bonus_amount_1}     set variable    null
   ${amount_type_1}     set variable    null
   ${settlement_type_1}     set variable    Amount

   ${fee_tier_condition_2}     set variable    >
   ${condition_amount_2}     set variable    100000
   ${fee_type_2}     set variable    % rate
   ${fee_amount_2}     set variable    10
   ${bonus_type_2}     set variable    NON
   ${bonus_amount_2}     set variable    null
   ${amount_type_2}     set variable    null
   ${settlement_type_2}     set variable    Amount

    ${debit}  set variable    {"action_type":"debit","actor_type":"Payer","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    ${credit}  set variable   {"action_type":"credit","actor_type":"Payee","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${debit}  ${credit}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}    service_group_name=${service_group_name}
    ...     service_name=${service_name}    currency=${currency}    command_id=${command_id}    fee_tier_condition_1=${fee_tier_condition_1}
    ...     condition_amount_1=${condition_amount_1}        fee_type_1=${fee_type_1}    fee_amount_1=${fee_amount_1}
    ...     bonus_type_1=${bonus_type_1}        bonus_amount_1=${bonus_amount_1}    amount_type_1=${amount_type_1}      settlement_type_1=${settlement_type_1}
    ...     fee_tier_condition_2=${fee_tier_condition_2}    condition_amount_2=${condition_amount_2}        fee_type_2=${fee_type_2}    fee_amount_2=${fee_amount_2}
    ...     bonus_type_2=${bonus_type_2}        bonus_amount_2=${bonus_amount_2}    amount_type_2=${amount_type_2}      settlement_type_2=${settlement_type_2}
    ...     balance_distributions=${balance_distributions}

   ${return_dic}   [Payment][Reuse][200] - create service structure with 2 fee tiers        ${arg_dic}

    set test variable      ${test_service_id}        ${return_dic.service_id}
    set test variable      ${test_service_name}        ${return_dic.service_name}
    set test variable      ${test_service_group_id}        ${return_dic.service_group_id}
    set test variable      ${test_service_group_name}        ${return_dic.service_group_name}

[Payment][Suite][200] - company agent fund in for "Marketing Wallet" agent
    ${text}=     generate random string      15      [LETTERS]
    ${arg_dic}      create dictionary   access_token=${suite_company_agent_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_service_id}      product_service_name=${suite_service_name}      product_name=${text}
    ...     product_ref_1=${text}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}     product_ref_3=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_company_agent_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_marketing_wallet_agent_id}       payee_user_type=${suite_marketing_wallet_agent_type}
    ...     amount=500000
    ${dic}      [Payment][Reuse][200] - create and execute normal order        ${arg_dic}
    set suite variable     ${suite_order_id_for_agent_relationship}       ${dic.order_id}

[Payment][Suite][200] - company agent fund in for "Master" agent
    ${text}=     generate random string      15      [LETTERS]
    ${arg_dic}      create dictionary   access_token=${suite_company_agent_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_service_id}      product_service_name=${suite_service_name}      product_name=${text}
    ...     product_ref_1=${text}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}     product_ref_3=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_company_agent_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_master_agent_id}       payee_user_type=agent
    ...     amount=500000
    ${dic}      [Payment][Reuse][200] - create and execute normal order        ${arg_dic}
    set suite variable     ${suite_order_id_fund_master_agent}       ${dic.order_id}

[Payment][Suite][200] - "Master" agent fund in for "Sub" agent
    ${text}=     generate random string      15      [LETTERS]
    ${arg_dic}      create dictionary   access_token=${suite_master_agent_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_service_id}      product_service_name=${suite_service_name}      product_name=${text}
    ...     product_ref_1=${text}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}     product_ref_3=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_master_agent_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_sub_agent_id}       payee_user_type=agent
    ...     amount=100000
    ${dic}      [Payment][Reuse][200] - create and execute normal order        ${arg_dic}
    set suite variable     ${suite_order_id_master_fund_sub}       ${dic.order_id}



[Payment][Suite][200] - add money for company agent in "${currency}" currency
    [None] API system user create a company agent cash source of fund with currency        ${currency}       ${suite_admin_access_token}
    [200] API system user update company agent cash balance     ${currency}     5000000     ${suite_admin_access_token}

[Payment][Test][200] - add money for company agent in "${currency}" currency
    [None] API system user create a company agent cash source of fund with currency        ${currency}       ${suite_admin_access_token}
    [200] API system user update company agent cash balance     ${currency}     5000000     ${suite_admin_access_token}

[Agent][Suite][200] - log in as company agent
    ${arg_dic}      create dictionary       username=${setup_company_agent_account_id}      password=${setup_agent_password_encrypted_utf8}
    ${dic}          [200] API authenticate agent        ${arg_dic}
    set suite variable      ${suite_company_agent_access_token}     ${dic.access_token}

[Payment][Suite][200] - create and execute cashback cash in order
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set suite variable      ${suite_ext_transaction_id}     ${text}
    set suite variable      ${suite_product_name}     ${text}
    set suite variable      ${suite_product_ref_1}     ${text}
    set suite variable      ${suite_payer_user_id}     ${num}
    ${arg_dic}      create dictionary   access_token=${suite_marketing_wallet_agent_access_token}    ext_transaction_id=${suite_ext_transaction_id}
    ...     product_service_id=${suite_cashback_cash_in_service_id}      product_service_name=${suite_cashback_cash_in_service_name}
    ...     product_name=${suite_product_name}       product_ref_1=${suite_product_ref_1}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}     product_ref_3=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_marketing_wallet_agent_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_cashback_voucher_wallet_agent_id}       payee_user_type=${suite_cashback_voucher_wallet_agent_type}
    ...     amount=10
    ${dic}      [Payment][Reuse][200] - create and execute normal order        ${arg_dic}
    set suite variable     ${suite_cashback_order_id}       ${dic.order_id}

[Payment][Suite][200] - get order detail of cashback cash in order
    ${dic}      [200] API get order detail       ${suite_marketing_wallet_agent_access_token}       ${suite_cashback_order_id}
    set suite variable      ${suite_cashback_product_name}      ${dic.product_name}
    set suite variable      ${suite_cashback_voucher_id}        ${dic.product_ref1}
    set suite variable      ${suite_cashback_order_id}         ${dic.order_id}
    set suite variable      ${suite_cashback_user_id}        ${dic.user_id}
    set suite variable      ${suite_cashback_ref_order_id}        ${dic.ref_order_id}
    set suite variable      ${suite_cashback_created_unique_device}        ${dic.created_unique_device}
    set suite variable      ${suite_cashback_created_client_id}        ${dic.created_client_id}
    set suite variable      ${suite_cashback_executed_client_id}        ${dic.executed_client_id}
    set suite variable      ${suite_cashback_service_group}        ${dic.service_group}
    set suite variable      ${suite_cashback_ext_transaction_id}        ${dic.ext_transaction_id}
    set suite variable      ${suite_cashback_short_order_id}        ${dic.short_order_id}
    set suite variable      ${suite_cashback_payer_id}        ${dic.payer_id}
    set suite variable      ${suite_cashback_payee_id}        ${dic.payee_id}
    set suite variable      ${suite_cashback_service_name}    ${dic.service_name}

[Payment][Test][200] - create and execute cashback cash in order
    ${text}=     generate random string      15      [LETTERS]
    ${arg_dic}      create dictionary   access_token=${suite_marketing_wallet_agent_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_cashback_cash_in_service_id}      product_service_name=${suite_cashback_cash_in_service_name}
    ...     product_name=${text}       product_ref_1=${text}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}     product_ref_3=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_marketing_wallet_agent_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_cashback_voucher_wallet_agent_id}       payee_user_type=${suite_cashback_voucher_wallet_agent_type}
    ...     amount=10
    ${dic}      [Payment][Reuse][200] - create and execute normal order        ${arg_dic}
    set test variable     ${test_cashback_order_id}       ${dic.order_id}

[Payment][Test][200] - get order detail of cashback cash in order
    ${dic}      [200] API get order detail       ${suite_marketing_wallet_agent_access_token}       ${suite_cashback_order_id}
    set test variable      ${test_cashback_voucher_id}        ${dic.product_ref1}

[Report][Suite][200] - search voucher cashback by voucher id
    ${arg_dic}      create dictionary       access_token=${suite_admin_access_token}    voucher_id=${suite_cashback_voucher_id}
    ${dic}  [200] API search voucher    ${arg_dic}
    set suite variable      ${suite_id_of_cashback_voucher}        ${dic.id}

[Report][Test][200] - search voucher by voucher id
    ${arg_dic}      create dictionary       access_token=${suite_admin_access_token}    voucher_id=${test_cashback_voucher_id}
    ${dic}  [200] API search voucher    ${arg_dic}
    set suite variable      ${test_id_of_cashback_voucher}        ${dic.id}

[Report][Test][200] - search order by order id on report
    ${arg_dic}     create dictionary   access_token=${suite_admin_access_token}      order_id=${test_order_id}
    ${dic}    [200] API get report order detail     ${arg_dic}
    set test variable    ${test_wait_delay_timestamp}    ${dic.wait_delay_timestamp}

[Payment][Test][200] - create timeout-waiting service structure in "${currency}" currency
    ${text}=   generate random string    10      [LETTERS]
    ${number}=   generate random string    1      123
    ${number1}=   generate random string    3      123456789
    ${service_group_name}      set variable   	${text}
    ${service_name}      set variable   	${text}
    ${currency}      set variable   	${currency}
    ${command_id}     set variable    1
    ${fee_tier_condition}     set variable    unlimit
    ${condition_amount}     set variable    null
    ${fee_type}     set variable    % rate
    ${fee_amount}     set variable    10
    ${bonus_type}     set variable    NON
    ${bonus_amount}     set variable    null
    ${amount_type}     set variable    null
    ${settlement_type}     set variable    Amount

    ${post_payment_spi_url_type}     set variable     Post-Payment
    ${post_payment_url}     set variable      ${SPI_Timeout_for_delay_order}
    ${post_payment_spi_url_call_method}     set variable      synchronous
    ${post_payment_expire_in_minute}     set variable     ${number}
    ${post_payment_max_retry}     set variable        ${number}
    ${post_payment_retry_delay_millisecond}     set variable      ${number1}
    ${post_payment_read_timeout}     set variable      120000

     ${debit}  set variable    {"action_type":"debit","actor_type":"Payer","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
     ${credit}  set variable    {"action_type":"credit","actor_type":"Payee","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
     @{balance_distributions}  create list     ${debit}  ${credit}

     ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}    service_group_name=${service_group_name}
     ...     service_name=${service_name}    currency=${currency}    command_id=${command_id}    fee_tier_condition=${fee_tier_condition}
     ...     condition_amount=${condition_amount}        fee_type=${fee_type}    fee_amount=${fee_amount}
     ...     bonus_type=${bonus_type}        bonus_amount=${bonus_amount}    amount_type=${amount_type}      settlement_type=${settlement_type}
     ...     post_payment_spi_url_type=${post_payment_spi_url_type}    post_payment_url=${post_payment_url}      post_payment_spi_url_call_method=${post_payment_spi_url_call_method}      post_payment_expire_in_minute=${post_payment_expire_in_minute}
     ...     post_payment_read_timeout=${post_payment_read_timeout}    post_payment_max_retry=${post_payment_max_retry}      post_payment_retry_delay_millisecond=${post_payment_retry_delay_millisecond}
     ...     balance_distributions=@{balance_distributions}

    ${return_dic}   [Payment][Reuse][200] - create service structure        ${arg_dic}

     set test variable      ${test_service_id}        ${return_dic.service_id}
     set test variable      ${test_service_name}        ${return_dic.service_name}

[Payment][Test][Fail] - create and execute waiting order
    ${text}=     generate random string      15      [LETTERS]
    ${arg_dic}      create dictionary   access_token=${suite_marketing_wallet_agent_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${test_service_id}      product_service_name=${test_service_name}
    ...     product_name=${text}       product_ref_1=${text}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}     product_ref_3=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_marketing_wallet_agent_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_cashback_voucher_wallet_agent_id}       payee_user_type=${suite_cashback_voucher_wallet_agent_type}
    ...     amount=10   code=500
    ${dic}      [Payment][Reuse][Fail] - execute order        ${arg_dic}
    set test variable     ${test_order_id}       ${dic.order_id}

[Agent][Suite][200] - Create "Water" agent with currency
    [Arguments]     &{arg_dic}
    ${text}=   generate random string    10      [LETTERS]
    ${current_address_postal_code}   generate random string    10      123456789
    ${agent_type_name}      set variable   	${text}
    ${agent_type_id}      set variable   	${setup_company_agent_type_id}
    ${agent_unique_reference}      set variable   	${text}
    ${agent_username}     set variable    ${text}
    ${agent_password}      set variable   	${setup_agent_password_encrypted}
    ${agent_password_login}     set variable    ${setup_agent_password_encrypted_utf8}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      agent_type_name=${agent_type_name}
    ...     unique_reference=${agent_unique_reference}      identity_identity_type_id=1
    ...     agent_username=${agent_username}      agent_password=${agent_password}
    ...     agent_password_login=${agent_password_login}      currency=${arg_dic.currency}
    ...     user_type_id=2      current_address_postal_code=${current_address_postal_code}

    ${dic}  [Agent][Reuse][200] - create agent      ${arg_dic}
    set suite variable      ${suite_water_agent_id}       ${dic.agent_id}
    set suite variable      ${suite_water_agent_type}       agent
    set suite variable      ${suite_water_agent_type_name}       ${agent_type_name}

    ${currency_list}     split string    ${arg_dic.currency}     ,
    ${currency_list_length}   get length      ${currency_list}
    :FOR     ${index}   IN RANGE    0   ${currency_list_length}
    \   ${currency}     get from list     ${currency_list}      ${index}
    \   set suite variable      ${suite_water_sof_cash_id_${currency}}       ${dic.sof_cash_id_in_${currency}}

    set suite variable      ${suite_water_agent_unique_reference}       ${agent_unique_reference}
    set suite variable      ${suite_water_agent_type_id}       ${dic.agent_type_id}
    set suite variable      ${suite_water_agent_current_address_postal_code}       ${current_address_postal_code}
    set suite variable      ${suite_water_agent_access_token}     ${dic.agent_access_token}

[Agent][Test][200] - create "Water" agent in "${currency}" currency
    ${text}=   generate random string    10      [LETTERS]
    ${agent_type_name}      set variable   	${text}
    ${agent_type_id}      set variable   	${setup_company_agent_type_id}
    ${agent_unique_reference}      set variable   	${text}
    ${agent_username}     set variable    ${text}
    ${agent_password}      set variable   	${setup_agent_password_encrypted}
    ${agent_password_login}     set variable    ${setup_agent_password_encrypted_utf8}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      agent_type_name=${agent_type_name}
    ...     unique_reference=${agent_unique_reference}      identity_identity_type_id=1
    ...     agent_username=${agent_username}      agent_password=${agent_password}
    ...     agent_password_login=${agent_password_login}      currency=${currency}
    ...     user_type_id=2

    ${dic}  [Agent][Reuse][200] - create agent      ${arg_dic}
    set test variable      ${test_water_agent_id}       ${dic.agent_id}
    set test variable      ${test_water_agent_type}       agent
    set test variable      ${test_water_sof_cash_id}       ${dic.sof_cash_id_in_${currency}}
    set test variable      ${test_water_agent_access_token}       ${dic.agent_access_token}

[Voucher][Suite][200] - create discount voucher in "${currency}" currency
    ${prefix}     generate random string      10      [LETTERS]
    ${number_of_digit}     generate random string      1      3456789
    ${discount_amount}     generate random string      2      123456789
    ${voucher_group}     generate random string      10      [LETTERS]
    set suite variable      ${suite_discount_prefix}     ${prefix}
    set suite variable      ${suite_discount_number_of_digit}     ${number_of_digit}
    set suite variable      ${suite_discount_type}     % rate
    set suite variable      ${suite_discount_amount}    ${discount_amount}
    set suite variable      ${suite_discount_currency}    ${currency}
    set suite variable      ${suite_discount_payer_user_id}    ${suite_marketing_wallet_agent_id}
    set suite variable      ${suite_discount_payer_user_type}    agent
    set suite variable      ${suite_discount_payer_user_sof_id}    ${suite_marketing_wallet_sof_cash_id}
    set suite variable      ${suite_discount_payer_user_sof_type_id}    2
    set suite variable      ${suite_discount_applied_to}    Amount
    set suite variable      ${suite_discount_min_amount}    1
    set suite variable      ${suite_discount_max_discount}    100
    set suite variable      ${suite_discount_permitted_service_group_id}    ${suite_service_group_id}
    set suite variable      ${suite_discount_permitted_service_id}    ${suite_service_id}
    set suite variable      ${suite_discount_voucher_group}    ${voucher_group}
    set suite variable      ${suite_discount_number_of_needed_voucher}    1

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}    prefix=${suite_discount_prefix}
    ...     number_of_digit=${suite_discount_number_of_digit}   discount_type=${suite_discount_type}
    ...     discount_amount=${suite_discount_amount}       currency=${suite_discount_currency}
    ...     payer_user_id=${suite_discount_payer_user_id}      payer_user_type=${suite_discount_payer_user_type}
    ...     payer_user_sof_id=${suite_discount_payer_user_sof_id}       payer_user_sof_type_id=${suite_discount_payer_user_sof_type_id}
    ...     applied_to=${suite_discount_applied_to}     min_amount=${suite_discount_min_amount}     max_discount=${suite_discount_max_discount}
    ...     permitted_service_group_id=${suite_discount_permitted_service_group_id}     permitted_service_id=${suite_discount_permitted_service_id}
    ...     voucher_group=${suite_discount_voucher_group}       number_of_needed_voucher=${suite_discount_number_of_needed_voucher}
    ${dic}     [200] API create multiple discount vouchers     ${arg_dic}
    ${ids}      convert to list    ${dic.ids}
    ${id_of_voucher}       get from list       ${ids}      0
    set suite variable        ${suite_id_of_voucher}        ${id_of_voucher}

[Voucher][Test][200] - create discount voucher in "${currency}" currency
    ${prefix}     generate random string      10      [LETTERS]
    ${number_of_digit}     generate random string      1      3456789
    ${discount_amount}     generate random string      2      123456789
    ${voucher_group}     generate random string      10      [LETTERS]
    set test variable      ${test_discount_prefix}     ${prefix}
    set test variable      ${test_discount_number_of_digit}     ${number_of_digit}
    set test variable      ${test_discount_type}     % rate
    set test variable      ${test_discount_amount}    ${discount_amount}
    set test variable      ${test_discount_currency}    ${currency}
    set test variable      ${test_discount_payer_user_id}    ${suite_marketing_wallet_agent_id}
    set test variable      ${test_discount_payer_user_type}    agent
    set test variable      ${test_discount_payer_user_sof_id}    ${suite_marketing_wallet_sof_cash_id}
    set test variable      ${test_discount_payer_user_sof_type_id}    2
    set test variable      ${test_discount_applied_to}    Amount
    set test variable      ${test_discount_min_amount}    1
    set test variable      ${test_discount_max_discount}    100
    set test variable      ${test_discount_permitted_service_group_id}    ${empty}
    set test variable      ${test_discount_permitted_service_id}    ${empty}
    set test variable      ${test_discount_voucher_group}    ${voucher_group}
    set test variable      ${test_discount_number_of_needed_voucher}    1

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}    prefix=${test_discount_prefix}
    ...     number_of_digit=${test_discount_number_of_digit}   discount_type=${test_discount_type}
    ...     discount_amount=${test_discount_amount}       currency=${test_discount_currency}
    ...     payer_user_id=${test_discount_payer_user_id}      payer_user_type=${test_discount_payer_user_type}
    ...     payer_user_sof_id=${test_discount_payer_user_sof_id}       payer_user_sof_type_id=${test_discount_payer_user_sof_type_id}
    ...     applied_to=${test_discount_applied_to}     min_amount=${test_discount_min_amount}     max_discount=${test_discount_max_discount}
    ...     permitted_service_group_id=${test_discount_permitted_service_group_id}     permitted_service_id=${test_discount_permitted_service_id}
    ...     voucher_group=${test_discount_voucher_group}       number_of_needed_voucher=${test_discount_number_of_needed_voucher}
    ${dic}     [200] API create multiple discount vouchers     ${arg_dic}
    ${ids}      convert to list    ${dic.ids}
    ${id_of_voucher}       get from list       ${ids}      0
    set test variable        ${test_id_of_voucher}        ${id_of_voucher}

[Report][Suite][200] - search discount voucher by id
    ${arg_dic}      create dictionary       access_token=${suite_admin_access_token}    id=${suite_id_of_voucher}
    ${dic}  [200] API search voucher    ${arg_dic}
    set suite variable      ${suite_discount_code}        ${dic.voucher_id}

[Payment][Test][200] - create normal order in "${currency}" currency
    ${text}=     generate random string      15      [LETTERS]
    ${arg_dic}      create dictionary   access_token=${suite_marketing_wallet_agent_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_service_id}      product_service_name=${suite_service_name}
    ...     product_name=${text}       product_ref_1=${text}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}     product_ref_3=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_marketing_wallet_agent_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_water_agent_id}       payee_user_type=${suite_water_agent_type}
    ...     amount=10
    ${dic}  [Payment][Reuse][200] - create normal order   ${arg_dic}
    set test variable      ${test_order_id}     ${dic.order_id}

[Payment][Test][200] - apply discount code
    ${arg_dic}  create dictionary   access_token=${suite_admin_access_token}       order_id=${test_order_id}    voucher_id=${test_discount_code}
    [200] API apply discount code       ${arg_dic}

[Report][Test][200] - search discount voucher by id
    ${arg_dic}      create dictionary       access_token=${suite_admin_access_token}    id=${test_id_of_voucher}
    ${dic}  [200] API search voucher    ${arg_dic}
    set test variable      ${test_discount_code}        ${dic.voucher_id}

[Payment][Suite][200] - company agent fund in for "Water" agent
    ${text}=     generate random string      15      [LETTERS]
    ${arg_dic}      create dictionary   access_token=${suite_company_agent_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_service_id}      product_service_name=${suite_service_name}      product_name=${text}
    ...     product_ref_1=${text}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}     product_ref_3=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_company_agent_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_water_agent_id}       payee_user_type=${suite_water_agent_type}
    ...     amount=500000
    ${return_dic}   [Payment][Reuse][200] - create and execute normal order    ${arg_dic}
    set suite variable      ${suite_fund_in_order_id}     ${return_dic.order_id}

[Payment][Suite][200] - company agent fund in for prepared customer
    ${text}=     generate random string      15      [LETTERS]
    ${arg_dic}      create dictionary   access_token=${suite_company_agent_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_service_id}      product_service_name=${suite_service_name}      product_name=${text}
    ...     product_ref_1=${text}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}     product_ref_3=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_company_agent_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_customer_id}       payee_user_type=${suite_customer_type}
    ...     amount=500000
    [Payment][Reuse][200] - create and execute normal order    ${arg_dic}

[Payment][Test][200] company agent fund in for prepared customer
    ${text}=     generate random string      15      [LETTERS]
    ${arg_dic}      create dictionary   access_token=${suite_company_agent_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${test_service_id}      product_service_name=${test_service_name}      product_name=${text}
    ...     product_ref_1=${text}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}     product_ref_3=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_company_agent_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${test_customer_id}       payee_user_type=${test_customer_type}
    ...     amount=500000
    [Payment][Reuse][200] - create and execute normal order    ${arg_dic}

[Payment][Suite][200] - create structure for "P2P Cash-In" service in "${currency}" currency
   ${text}=   generate random string    10      [LETTERS]
   ${number}=   generate random string    1      123
   ${number1}=   generate random string    3      123456789
   ${service_group_name}      set variable   	${text}
   ${service_name}      set variable   	${text}
   ${currency}      set variable   	${currency}
   ${command_id}     set variable    1
   ${fee_tier_condition}     set variable    unlimit
   ${condition_amount}     set variable    null
   ${fee_type}     set variable    % rate
   ${fee_amount}     set variable    10
   ${bonus_type}     set variable    NON
   ${bonus_amount}     set variable    null
   ${amount_type}     set variable    null
   ${settlement_type}     set variable    Amount

   ${post_payment_spi_url_type}     set variable     Post-Payment
   ${post_payment_url}     set variable      ${internal_voucher_p2p_generate_url}
   ${post_payment_spi_url_call_method}     set variable      synchronous
   ${post_payment_expire_in_minute}     set variable     ${number}
   ${post_payment_max_retry}     set variable        ${number}
   ${post_payment_retry_delay_millisecond}     set variable      ${number1}
   ${post_payment_read_timeout}     set variable      120000

    ${debit}  set variable    {"action_type":"debit","actor_type":"Payer","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    ${credit}  set variable    {"action_type":"credit","actor_type":"Payee","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${debit}  ${credit}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}    service_group_name=${service_group_name}
    ...     service_name=${service_name}    currency=${currency}    command_id=${command_id}    fee_tier_condition=${fee_tier_condition}
    ...     condition_amount=${condition_amount}        fee_type=${fee_type}    fee_amount=${fee_amount}
    ...     bonus_type=${bonus_type}        bonus_amount=${bonus_amount}    amount_type=${amount_type}      settlement_type=${settlement_type}
    ...     post_payment_spi_url_type=${post_payment_spi_url_type}    post_payment_url=${post_payment_url}      post_payment_spi_url_call_method=${post_payment_spi_url_call_method}      post_payment_expire_in_minute=${post_payment_expire_in_minute}
    ...     post_payment_read_timeout=${post_payment_read_timeout}    post_payment_max_retry=${post_payment_max_retry}      post_payment_retry_delay_millisecond=${post_payment_retry_delay_millisecond}
    ...     balance_distributions=@{balance_distributions}

   ${return_dic}   [Payment][Reuse][200] - create service structure        ${arg_dic}

    set suite variable      ${suite_p2p_cash_in_service_id_in_${currency}}        ${return_dic.service_id}
    set suite variable      ${suite_p2p_cash_in_service_name_in_${currency}}        ${return_dic.service_name}

[Payment][Suite][200] - create structure for "P2P Cash-Out" service in "${currency}" currency
   ${text}=   generate random string    10      [LETTERS]
   ${number}=   generate random string    1      123
   ${number1}=   generate random string    3      123456789
   ${service_group_name}      set variable   	${text}
   ${service_name}      set variable   	${text}
   ${currency}      set variable   	${currency}
   ${command_id}     set variable    1
   ${fee_tier_condition}     set variable    unlimit
   ${condition_amount}     set variable    null
   ${fee_type}     set variable    % rate
   ${fee_amount}     set variable    10
   ${bonus_type}     set variable    NON
   ${bonus_amount}     set variable    null
   ${amount_type}     set variable    null
   ${settlement_type}     set variable    Amount

   ${pre_payment_spi_url_type}     set variable     pre-Payment
   ${pre_payment_url}     set variable      ${internal_voucher_verification_url}
   ${pre_payment_spi_url_call_method}     set variable      synchronous
   ${pre_payment_expire_in_minute}     set variable     ${number}
   ${pre_payment_max_retry}     set variable        ${number}
   ${pre_payment_retry_delay_millisecond}     set variable      ${number1}
   ${pre_payment_read_timeout}     set variable      120000

   ${post_payment_spi_url_type}     set variable     Post-Payment
   ${post_payment_url}     set variable      ${internal_voucher_claim_url}
   ${post_payment_spi_url_call_method}     set variable      synchronous
   ${post_payment_expire_in_minute}     set variable     ${number}
   ${post_payment_max_retry}     set variable        ${number}
   ${post_payment_retry_delay_millisecond}     set variable      ${number1}
   ${post_payment_read_timeout}     set variable      120000

    ${debit}  set variable    {"action_type":"debit","actor_type":"Specific ID","specific_actor_id":${suite_remittance_escrow_agent_id},"sof_type_id":2,"specific_sof":"${suite_remittance_escrow_sof_cash_id}","amount_type":"Amount","rate":null}
    ${credit}  set variable    {"action_type":"credit","actor_type":"Payee","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${debit}  ${credit}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}    service_group_name=${service_group_name}
    ...     service_name=${service_name}    currency=${currency}    command_id=${command_id}    fee_tier_condition=${fee_tier_condition}
    ...     condition_amount=${condition_amount}        fee_type=${fee_type}    fee_amount=${fee_amount}
    ...     bonus_type=${bonus_type}        bonus_amount=${bonus_amount}    amount_type=${amount_type}      settlement_type=${settlement_type}
    ...     pre_payment_spi_url_type=${pre_payment_spi_url_type}    pre_payment_url=${pre_payment_url}      pre_payment_spi_url_call_method=${pre_payment_spi_url_call_method}      pre_payment_expire_in_minute=${pre_payment_expire_in_minute}
    ...     pre_payment_read_timeout=${pre_payment_read_timeout}    pre_payment_max_retry=${pre_payment_max_retry}      pre_payment_retry_delay_millisecond=${pre_payment_retry_delay_millisecond}
    ...     post_payment_spi_url_type=${post_payment_spi_url_type}    post_payment_url=${post_payment_url}      post_payment_spi_url_call_method=${post_payment_spi_url_call_method}      post_payment_expire_in_minute=${post_payment_expire_in_minute}
    ...     post_payment_read_timeout=${post_payment_read_timeout}    post_payment_max_retry=${post_payment_max_retry}      post_payment_retry_delay_millisecond=${post_payment_retry_delay_millisecond}
    ...     balance_distributions=@{balance_distributions}

   ${return_dic}   [Payment][Reuse][200] - create service structure        ${arg_dic}

    set suite variable      ${suite_p2p_cash_out_service_id_in_${currency}}        ${return_dic.service_id}
    set suite variable      ${suite_p2p_cash_out_service_name_in_${currency}}        ${return_dic.service_name}

[Payment][Suite][200] - create and execute "P2P Cash-In" order in "${currency}" currency
    ${text}=     generate random string      15      [LETTERS]
    ${recipient_mobile}=     generate random string      11      123456789
    ${sender_mobile_number}=     generate random string      11      123456789
    ${sender_name}=     generate random string      20      [LETTERS]
    ${recipient_name}=     generate random string      20      [LETTERS]
    ${amount}=     generate random string      2      123456789
    ${arg_dic}      create dictionary   access_token=${suite_customer_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_p2p_cash_in_service_id_in_${currency}}      product_service_name=${suite_p2p_cash_in_service_name_in_${currency}}
    ...     product_name=${text}       product_ref_1=${empty}     product_ref_2=${recipient_mobile}     product_ref_3=${sender_mobile_number}
    ...     product_ref_4=${sender_name}     product_ref_5=${recipient_name}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_customer_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_remittance_escrow_agent_id}       payee_user_type=${suite_remittance_escrow_agent_type}
    ...     amount=${amount}
    ${dic}      [Payment][Reuse][200] - create and execute normal order        ${arg_dic}

    set suite variable     ${suite_p2p_cash_in_order_amount}       ${amount}
    set suite variable     ${suite_p2p_cash_in_product_ref_2}       ${recipient_mobile}
    set suite variable     ${suite_p2p_cash_in_product_ref_3}       ${sender_mobile_number}
    set suite variable     ${suite_p2p_cash_in_product_ref_4}       ${sender_name}
    set suite variable     ${suite_p2p_cash_in_product_ref_5}       ${recipient_name}
    set suite variable     ${suite_p2p_cash_in_order_id}       ${dic.order_id}

[Payment][Suite][200] - create and execute "P2P Cash-Out" order in "${currency}" currency
    ${text}=     generate random string      15      [LETTERS]
    ${arg_dic}      create dictionary   access_token=${suite_water_agent_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_p2p_cash_out_service_id_in_${currency}}      product_service_name=${suite_p2p_cash_out_service_name_in_${currency}}
    ...     product_name=${text}       product_ref_1=${suite_p2p_voucher_id}     product_ref_2=${suite_p2p_cash_in_product_ref_2}     product_ref_3=${suite_p2p_cash_in_product_ref_3}
    ...     product_ref_4=${suite_p2p_cash_in_product_ref_4}     product_ref_5=${suite_p2p_cash_in_product_ref_5}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_water_agent_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_water_agent_id}       payee_user_type=${suite_water_agent_type}
    ...     amount=${suite_p2p_cash_in_order_amount}
    ${dic}      [Payment][Reuse][200] - create and execute normal order        ${arg_dic}

    set suite variable     ${suite_p2p_cash_out_order_id}       ${dic.order_id}

[Payment][Suite][200] - create "Remittance Escrow" agent in "${currency}" currency
    ${text}=   generate random string    10      [LETTERS]
    ${agent_type_name}      set variable   	${text}
    ${agent_type_id}      set variable   	${setup_company_agent_type_id}
    ${agent_unique_reference}      set variable   	${text}
    ${agent_username}     set variable    ${text}
    ${agent_password}      set variable   	${setup_agent_password_encrypted}
    ${agent_password_login}     set variable    ${setup_agent_password_encrypted_utf8}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      agent_type_name=${agent_type_name}
    ...     unique_reference=${agent_unique_reference}      identity_identity_type_id=1
    ...     agent_username=${agent_username}      agent_password=${agent_password}
    ...     agent_password_login=${agent_password_login}      currency=${currency}
    ...     user_type_id=2

    ${dic}  [Agent][Reuse][200] - create agent      ${arg_dic}
    set suite variable      ${suite_remittance_escrow_agent_id}       ${dic.agent_id}
    set suite variable      ${suite_remittance_escrow_agent_name}       ${agent_username}
    set suite variable      ${suite_remittance_escrow_agent_type}       agent
    set suite variable      ${suite_remittance_escrow_sof_cash_id}       ${dic.sof_cash_id_in_${currency}}
    set suite variable      ${suite_remittance_escrow_agent_access_token}     ${dic.agent_access_token}

[Payment][Suite][200] - get order detail of "P2P Cash-In" order
    ${dic}      [200] API get order detail       ${suite_customer_access_token}       ${suite_p2p_cash_in_order_id}
    set suite variable      ${suite_p2p_voucher_id}        ${dic.product_ref1}

[Agent][Suite][200] - create "Reciever" agent in "${currency}" currency
    ${text}=   generate random string    10      [LETTERS]
    ${agent_type_name}      set variable   	${text}
    ${agent_type_id}      set variable   	${setup_company_agent_type_id}
    ${agent_unique_reference}      set variable   	${text}
    ${agent_username}     set variable    ${text}
    ${agent_password}      set variable   	${setup_agent_password_encrypted}
    ${agent_password_login}     set variable    ${setup_agent_password_encrypted_utf8}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      agent_type_name=${agent_type_name}
    ...     unique_reference=${agent_unique_reference}      identity_identity_type_id=1
    ...     agent_username=${agent_username}      agent_password=${agent_password}
    ...     agent_password_login=${agent_password_login}      currency=${currency}
    ...     user_type_id=2

    ${dic}  [Agent][Reuse][200] - create agent      ${arg_dic}
    set suite variable      ${suite_remittance_escrow_agent_id}       ${dic.agent_id}
    set suite variable      ${suite_remittance_escrow_agent_type}       agent
    set suite variable      ${suite_remittance_escrow_sof_cash_id}       ${dic.sof_cash_id_in_${currency}}
    set suite variable      ${suite_remittance_escrow_agent_access_token}     ${dic.agent_access_token}


[Customer][Suite][200] - Create customer with currency
    [Arguments]     &{arg_dic}
    ${current_address_postal_code}   generate random string    10      123456789
    ${password_encrypted}=          set variable        ${setup_password_customer_encrypted}
    ${password_encrypted_utf8}=          set variable        ${setup_password_customer_encrypted_utf8}
    ${unique_reference}    generate random string      15          [LOWER][NUMBERS]
    ${username}            generate random string      15        [LETTERS]
    ${first_name}    generate random string      10        [LETTERS]
    ${last_name}     generate random string      10        [LETTERS]

    ${imputted_customer_arg_dic}     create dictionary       unique_reference=${unique_reference}       identity_type_id=1      username=${username}    password=${password_encrypted}    password_encrypted_utf8=${password_encrypted_utf8}      currency=${arg_dic.currency}     current_address_postal_code=${current_address_postal_code}
    ...     first_name=${first_name}    last_name=${last_name}
    ${dic}      [Customer][Reuse][200] - create customer       ${imputted_customer_arg_dic}
    set suite variable      ${suite_customer_id}    ${dic.customer_id}
    set suite variable      ${suite_customer_type}    ${dic.customer_type}
    set suite variable      ${suite_customer_access_token}    ${dic.customer_access_token}

    ${currency_list}     split string    ${arg_dic.currency}     ,
    ${currency_list_length}   get length      ${currency_list}
    :FOR     ${index}   IN RANGE    0   ${currency_list_length}
    \   ${currency}     get from list     ${currency_list}      ${index}
    \   set suite variable      ${suite_customer_sof_cash_id_in_${currency}}    ${dic.sof_cash_id_in_${currency}}

    set suite variable      ${suite_customer_first_name}    ${first_name}
    set suite variable      ${suite_customer_last_name}    ${last_name}
    set suite variable      ${suite_customer_unique_reference}    ${unique_reference}
    set suite variable      ${suite_customer_current_address_postal_code}    ${current_address_postal_code}

[Customer][Test][200] - create customer in "${currency}" currency
    ${password_encrypted}=          set variable        ${setup_password_customer_encrypted}
    ${password_encrypted_utf8}=          set variable        ${setup_password_customer_encrypted_utf8}
    ${unique_reference}    generate random string      15          [LOWER][NUMBERS]
    ${username}    generate random string      15        [LETTERS]
    ${first_name}    generate random string      10        [LETTERS]
    ${last_name}    generate random string      10        [LETTERS]

    ${imputted_customer_arg_dic}     create dictionary       unique_reference=${unique_reference}       identity_type_id=1      username=${username}    password=${password_encrypted}    password_encrypted_utf8=${password_encrypted_utf8}      currency=${currency}
    ...     first_name=${first_name}    last_name=${last_name}
    ${dic}      [Customer][Reuse][200] - create customer       ${imputted_customer_arg_dic}
    set test variable      ${test_customer_id}    ${dic.customer_id}
    set test variable      ${test_customer_type}    ${dic.customer_type}
    set test variable      ${test_customer_access_token}    ${dic.customer_access_token}
    set test variable      ${test_customer_sof_cash_id_in_${currency}}    ${dic.sof_cash_id_in_${currency}}
    set test variable     ${test_customer_first_name}    ${first_name}
    set test variable     ${test_customer_last_name}    ${last_name}
    set test variable     ${test_customer_unique_reference}    ${unique_reference}

[Customer][Suite][200] - create "Payroll Escrow" agent in "${currency}" currency
    ${text}=   generate random string    10      [LETTERS]
    ${agent_type_name}      set variable   	${text}
    ${agent_type_id}      set variable   	${setup_company_agent_type_id}
    ${agent_unique_reference}      set variable   	${text}
    ${agent_username}     set variable    ${text}
    ${agent_password}      set variable   	${setup_agent_password_encrypted}
    ${agent_password_login}     set variable    ${setup_agent_password_encrypted_utf8}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      agent_type_name=${agent_type_name}
    ...     unique_reference=${agent_unique_reference}      identity_identity_type_id=1
    ...     agent_username=${agent_username}      agent_password=${agent_password}
    ...     agent_password_login=${agent_password_login}      currency=${currency}
    ...     user_type_id=2

    ${dic}  [Agent][Reuse][200] - create agent      ${arg_dic}
    set suite variable      ${suite_payroll_escrow_agent_id}       ${dic.agent_id}
    set suite variable      ${suite_payroll_escrow_agent_name}       ${agent_username}
    set suite variable      ${suite_payroll_escrow_agent_type}       agent
    set suite variable      ${suite_payroll_escrow_sof_cash_id}       ${dic.sof_cash_id_in_${currency}}
    set suite variable      ${suite_payroll_escrow_agent_access_token}     ${dic.agent_access_token}

[Payment][Suite][200] - create structure for "Payout Cash-In" service in "${currency}" currency
   ${text}=   generate random string    10      [LETTERS]
   ${number}=   generate random string    1      123
   ${number1}=   generate random string    3      123456789
   ${service_group_name}      set variable   	${text}
   ${service_name}      set variable   	${text}
   ${currency}      set variable   	${currency}
   ${command_id}     set variable    1
   ${fee_tier_condition}     set variable    unlimit
   ${condition_amount}     set variable    null
   ${fee_type}     set variable    % rate
   ${fee_amount}     set variable    10
   ${bonus_type}     set variable    NON
   ${bonus_amount}     set variable    null
   ${amount_type}     set variable    null
   ${settlement_type}     set variable    Amount

   ${post_payment_spi_url_type}     set variable     Post-Payment
   ${post_payment_url}     set variable      ${internal_voucher_payout_generate_url}
   ${post_payment_spi_url_call_method}     set variable      synchronous
   ${post_payment_expire_in_minute}     set variable     ${number}
   ${post_payment_max_retry}     set variable        ${number}
   ${post_payment_retry_delay_millisecond}     set variable      ${number1}
   ${post_payment_read_timeout}     set variable      120000

    ${debit}  set variable    {"action_type":"debit","actor_type":"Payer","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    ${credit}  set variable    {"action_type":"credit","actor_type":"Payee","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${debit}  ${credit}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}    service_group_name=${service_group_name}
    ...     service_name=${service_name}    currency=${currency}    command_id=${command_id}    fee_tier_condition=${fee_tier_condition}
    ...     condition_amount=${condition_amount}        fee_type=${fee_type}    fee_amount=${fee_amount}
    ...     bonus_type=${bonus_type}        bonus_amount=${bonus_amount}    amount_type=${amount_type}      settlement_type=${settlement_type}
    ...     post_payment_spi_url_type=${post_payment_spi_url_type}    post_payment_url=${post_payment_url}      post_payment_spi_url_call_method=${post_payment_spi_url_call_method}      post_payment_expire_in_minute=${post_payment_expire_in_minute}
    ...     post_payment_read_timeout=${post_payment_read_timeout}    post_payment_max_retry=${post_payment_max_retry}      post_payment_retry_delay_millisecond=${post_payment_retry_delay_millisecond}
    ...     balance_distributions=@{balance_distributions}

   ${return_dic}   [Payment][Reuse][200] - create service structure        ${arg_dic}

    set suite variable      ${suite_payout_cash_in_service_id_in_${currency}}        ${return_dic.service_id}
    set suite variable      ${suite_payout_cash_in_service_name_in_${currency}}        ${return_dic.service_name}

[Payment][Suite][200] - create structure for "Payout Cash-Out" service in "${currency}" currency
   ${text}=   generate random string    10      [LETTERS]
   ${number}=   generate random string    1      123
   ${number1}=   generate random string    3      123456789
   ${service_group_name}      set variable   	${text}
   ${service_name}      set variable   	${text}
   ${currency}      set variable   	${currency}
   ${command_id}     set variable    1
   ${fee_tier_condition}     set variable    unlimit
   ${condition_amount}     set variable    null
   ${fee_type}     set variable    % rate
   ${fee_amount}     set variable    10
   ${bonus_type}     set variable    NON
   ${bonus_amount}     set variable    null
   ${amount_type}     set variable    null
   ${settlement_type}     set variable    Amount

   ${pre_payment_spi_url_type}     set variable     pre-Payment
   ${pre_payment_url}     set variable      ${internal_voucher_verification_url}
   ${pre_payment_spi_url_call_method}     set variable      synchronous
   ${pre_payment_expire_in_minute}     set variable     ${number}
   ${pre_payment_max_retry}     set variable        ${number}
   ${pre_payment_retry_delay_millisecond}     set variable      ${number1}
   ${pre_payment_read_timeout}     set variable      120000

   ${post_payment_spi_url_type}     set variable     Post-Payment
   ${post_payment_url}     set variable      ${internal_voucher_claim_url}
   ${post_payment_spi_url_call_method}     set variable      synchronous
   ${post_payment_expire_in_minute}     set variable     ${number}
   ${post_payment_max_retry}     set variable        ${number}
   ${post_payment_retry_delay_millisecond}     set variable      ${number1}
   ${post_payment_read_timeout}     set variable      120000

    ${debit}  set variable    {"action_type":"debit","actor_type":"Specific ID","specific_actor_id":${suite_payroll_escrow_agent_id},"sof_type_id":2,"specific_sof":"${suite_payroll_escrow_sof_cash_id}","amount_type":"Amount","rate":null}
    ${credit}  set variable    {"action_type":"credit","actor_type":"Payee","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${debit}  ${credit}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}    service_group_name=${service_group_name}
    ...     service_name=${service_name}    currency=${currency}    command_id=${command_id}    fee_tier_condition=${fee_tier_condition}
    ...     condition_amount=${condition_amount}        fee_type=${fee_type}    fee_amount=${fee_amount}
    ...     bonus_type=${bonus_type}        bonus_amount=${bonus_amount}    amount_type=${amount_type}      settlement_type=${settlement_type}
    ...     pre_payment_spi_url_type=${pre_payment_spi_url_type}    pre_payment_url=${pre_payment_url}      pre_payment_spi_url_call_method=${pre_payment_spi_url_call_method}      pre_payment_expire_in_minute=${pre_payment_expire_in_minute}
    ...     pre_payment_read_timeout=${pre_payment_read_timeout}    pre_payment_max_retry=${pre_payment_max_retry}      pre_payment_retry_delay_millisecond=${pre_payment_retry_delay_millisecond}
    ...     post_payment_spi_url_type=${post_payment_spi_url_type}    post_payment_url=${post_payment_url}      post_payment_spi_url_call_method=${post_payment_spi_url_call_method}      post_payment_expire_in_minute=${post_payment_expire_in_minute}
    ...     post_payment_read_timeout=${post_payment_read_timeout}    post_payment_max_retry=${post_payment_max_retry}      post_payment_retry_delay_millisecond=${post_payment_retry_delay_millisecond}
    ...     balance_distributions=@{balance_distributions}

   ${return_dic}   [Payment][Reuse][200] - create service structure        ${arg_dic}

    set suite variable      ${suite_payout_cash_out_service_id_in_${currency}}        ${return_dic.service_id}
    set suite variable      ${suite_payout_cash_out_service_name_in_${currency}}        ${return_dic.service_name}

[Payment][Suite][200] - create and execute "Payout Cash-In" order in "${currency}" currency
    ${text}=     generate random string      15      [LETTERS]
    ${primary_number}=     generate random string      11      123456789
    ${primary_number2}=     generate random string      11      123456789
    ${secondary_number}=     generate random string      11      [LETTERS]
    ${employee_id}=     generate random string      1      123456789
    ${amount}=     generate random string      2      123456789
    ${arg_dic}      create dictionary   access_token=${suite_customer_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_payout_cash_in_service_id_in_${currency}}      product_service_name=${suite_payout_cash_in_service_name_in_${currency}}
    ...     product_name=${text}       product_ref_1=${empty}     product_ref_2=${primary_number}     product_ref_3=${primary_number2}
    ...     product_ref_4=${secondary_number}     product_ref_5=${employee_id}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_customer_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_payroll_escrow_agent_id}       payee_user_type=${suite_payroll_escrow_agent_type}
    ...     amount=${amount}
    ${dic}      [Payment][Reuse][200] - create and execute normal order        ${arg_dic}

    set suite variable     ${suite_payout_cash_in_order_amount}       ${amount}
    set suite variable     ${suite_payout_cash_in_product_ref_2}       ${primary_number}
    set suite variable     ${suite_payout_cash_in_product_ref_3}       ${primary_number2}
    set suite variable     ${suite_payout_cash_in_product_ref_4}       ${secondary_number}
    set suite variable     ${suite_payout_cash_in_product_ref_5}       ${employee_id}
    set suite variable     ${suite_payout_cash_in_order_id}       ${dic.order_id}

[Payment][Suite][200] - create and execute "Payout Cash-Out" order in "${currency}" currency
    ${text}=     generate random string      15      [LETTERS]
    ${arg_dic}      create dictionary   access_token=${suite_water_agent_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_payout_cash_out_service_id_in_${currency}}      product_service_name=${suite_payout_cash_out_service_name_in_${currency}}
    ...     product_name=${text}       product_ref_1=${suite_payout_voucher_id}     product_ref_2=${suite_payout_cash_in_product_ref_2}     product_ref_3=${suite_payout_cash_in_product_ref_3}
    ...     product_ref_4=${suite_payout_cash_in_product_ref_4}     product_ref_5=${suite_payout_cash_in_product_ref_5}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_water_agent_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_water_agent_id}       payee_user_type=${suite_water_agent_type}
    ...     amount=${suite_payout_cash_in_order_amount}
    ${dic}      [Payment][Reuse][200] - create and execute normal order        ${arg_dic}

    set suite variable     ${suite_payout_cash_out_order_id}       ${dic.order_id}

[Payment][Suite][200] - get order detail of "Payout Cash-In" order
    ${dic}      [200] API get order detail       ${suite_customer_access_token}       ${suite_payout_cash_in_order_id}
    set suite variable      ${suite_payout_voucher_id}        ${dic.product_ref1}

[Payment][Test][200] - create and execute "P2P Cash-In" order in "${currency}" currency
    ${text}=     generate random string      15      [LETTERS]
    ${recipient_mobile}=     generate random string      11      123456789
    ${sender_mobile_number}=     generate random string      11      123456789
    ${sender_name}=     generate random string      20      [LETTERS]
    ${recipient_name}=     generate random string      20      [LETTERS]
    ${amount}=     generate random string      2      123456789
    ${arg_dic}      create dictionary   access_token=${suite_customer_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_p2p_cash_in_service_id_in_${currency}}      product_service_name=${suite_p2p_cash_in_service_name_in_${currency}}
    ...     product_name=${text}       product_ref_1=${empty}     product_ref_2=${recipient_mobile}     product_ref_3=${sender_mobile_number}
    ...     product_ref_4=${sender_name}     product_ref_5=${recipient_name}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_customer_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_remittance_escrow_agent_id}       payee_user_type=${suite_remittance_escrow_agent_type}
    ...     amount=${amount}
    ${dic}      [Payment][Reuse][200] - create and execute normal order        ${arg_dic}

    set test variable     ${test_p2p_cash_in_order_amount}       ${amount}
    set test variable     ${test_p2p_cash_in_product_ref_2}       ${recipient_mobile}
    set test variable     ${test_p2p_cash_in_product_ref_3}       ${sender_mobile_number}
    set test variable     ${test_p2p_cash_in_product_ref_4}       ${sender_name}
    set test variable     ${test_p2p_cash_in_product_ref_5}       ${recipient_name}
    set test variable     ${test_p2p_cash_in_order_id}       ${dic.order_id}

[Payment][Test][200] - get order detail of "P2P Cash-In" order
    ${dic}      [200] API get order detail       ${suite_customer_access_token}       ${test_p2p_cash_in_order_id}
    set suite variable      ${test_p2p_voucher_id}        ${dic.product_ref1}

[Payment][Test][200] - create and execute "Payout Cash-In" order in "${currency}" currency
    ${text}=     generate random string      15      [LETTERS]
    ${primary_number}=     generate random string      11      123456789
    ${primary_number2}=     generate random string      11      123456789
    ${secondary_number}=     generate random string      11      [LETTERS]
    ${employee_id}=     generate random string      1      123456789
    ${amount}=     generate random string      2      123456789
    ${arg_dic}      create dictionary   access_token=${suite_customer_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_payout_cash_in_service_id_in_${currency}}      product_service_name=${suite_payout_cash_in_service_name_in_${currency}}
    ...     product_name=${text}       product_ref_1=${empty}     product_ref_2=${primary_number}     product_ref_3=${primary_number2}
    ...     product_ref_4=${secondary_number}     product_ref_5=${employee_id}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_customer_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_payroll_escrow_agent_id}       payee_user_type=${suite_payroll_escrow_agent_type}
    ...     amount=${amount}
    ${dic}      [Payment][Reuse][200] - create and execute normal order        ${arg_dic}

    set test variable     ${test_payout_cash_in_order_amount}       ${amount}
    set test variable     ${test_payout_cash_in_product_ref_2}       ${primary_number}
    set test variable     ${test_payout_cash_in_product_ref_3}       ${primary_number2}
    set test variable     ${test_payout_cash_in_product_ref_4}       ${secondary_number}
    set test variable     ${test_payout_cash_in_product_ref_5}       ${employee_id}
    set test variable     ${test_payout_cash_in_order_id}       ${dic.order_id}

[Payment][Test][200] - get order detail of "Payout Cash-In" order
    ${dic}      [200] API get order detail       ${suite_customer_access_token}       ${test_payout_cash_in_order_id}
    set test variable      ${test_payout_voucher_id}        ${dic.product_ref1}

[Payment][Test][200] - get payout voucher details via api by voucher_id and product_ref_2
    set test variable     ${test_product_ref_2}       ${test_payout_cash_in_product_ref_2}
    set test variable     ${test_voucher_id}       ${test_payout_voucher_id}
    ${dic}=        [Report][Test][200] - search voucher by voucher id and product ref 2
    set test variable      ${test_id_of_payout_voucher}     ${dic.id}

[Payment][Test][200] - get p2p voucher details via api by voucher_id and product_ref_2
    set test variable     ${test_product_ref_2}       ${test_p2p_cash_in_product_ref_2}
    set test variable     ${test_voucher_id}       ${test_p2p_voucher_id}
    ${dic}=        [Report][Test][200] - search voucher by voucher id and product ref 2
    set test variable      ${test_id_of_p2p_voucher}     ${dic.id}

[Report][Test][200] - search voucher by voucher id and product ref 2
    ${arg_dic}      create dictionary       access_token=${suite_admin_access_token}    voucher_id=${test_voucher_id}   product_ref2=${test_product_ref_2}
    ${dic}  [200] API search voucher    ${arg_dic}
    [Return]    ${dic}

[Payment][Suite][200] - add cancel command for service in "${currency}" currency
   ${text}=   generate random string    10      [LETTERS]
   ${number}=   generate random string    1      123
   ${number1}=   generate random string    3      123456789
   ${currency}      set variable   	${currency}
   ${command_id}     set variable    2
   ${fee_tier_condition}     set variable    unlimit
   ${condition_amount}     set variable    null
   ${fee_type}     set variable    % rate
   ${fee_amount}     set variable    10
   ${bonus_type}     set variable    NON
   ${bonus_amount}     set variable    null
   ${amount_type}     set variable    null
   ${settlement_type}     set variable    Amount

    ${debit}  set variable    {"action_type":"debit","actor_type":"Payer","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    ${credit}  set variable   {"action_type":"credit","actor_type":"Payee","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${debit}  ${credit}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}
    ...     service_id=${suite_service_id}    currency=${currency}    command_id=${command_id}    fee_tier_condition=${fee_tier_condition}
    ...     condition_amount=${condition_amount}        fee_type=${fee_type}    fee_amount=${fee_amount}
    ...     bonus_type=${bonus_type}        bonus_amount=${bonus_amount}    amount_type=${amount_type}      settlement_type=${settlement_type}
    ...     balance_distributions=${balance_distributions}

    ${return_dic}   [Payment][Reuse][200] - add "Cancel" command info for service      ${arg_dic}
    set suite variable      ${suite_service_cancel_fee_tier_id}        ${return_dic.fee_tier_id}

[Payment][Test][200] - create and execute normal order
    [Arguments]   &{arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}  access_token    ${suite_water_agent_access_token}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}  product_service_id    ${suite_service_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}  product_service_name    ${suite_service_name}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}  amount    10
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    ${header_device_unique_reference}=      generate random string      32      [LETTERS]
    set test variable      ${test_ext_transaction_id}     ${text}
    set test variable      ${test_product_name}     ${text}
    set test variable      ${test_product_ref_1}     ${text}
    set test variable      ${test_payer_user_id}     ${num}
    set test variable      ${test_channel_id}    1
    ${arg_dic}      create dictionary   access_token=${arg_dic.access_token}    ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${arg_dic.product_service_id}      product_service_name=${arg_dic.product_service_name}
    ...     product_name=${test_product_name}       product_ref_1=${test_product_ref_1}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}     product_ref_3=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_water_agent_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_customer_id}       payee_user_type=${suite_customer_type}
    ...     amount=${arg_dic.amount}    header_device_unique_reference=${header_device_unique_reference}
    ...     header_channel_id=${test_channel_id}
    ${dic}      [Payment][Reuse][200] - create and execute normal order        ${arg_dic}
    set test variable     ${test_normal_order_id}       ${dic.order_id}
    set test variable     ${test_normal_order_device_unique_reference}       ${header_device_unique_reference}

[Payment][Test][200] - create and execute normal order with service doesn't have PAYMENT command
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set test variable      ${test_ext_transaction_id}     ${text}
    set test variable      ${test_product_name}     ${text}
    set test variable      ${test_product_ref_1}     ${text}
    set test variable      ${test_payer_user_id}     ${num}
    ${arg_dic}      create dictionary   access_token=${suite_water_agent_access_token}    ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${test_service_id}      product_service_name=${test_service_name}
    ...     product_name=${test_product_name}       product_ref_1=${test_product_ref_1}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}     product_ref_3=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_water_agent_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_customer_id}       payee_user_type=${suite_customer_type}
    ...     amount=10
    ${dic}      [Payment][Reuse][200] - create and execute normal order        ${arg_dic}
    set test variable     ${test_normal_order_id}       ${dic.order_id}

[Payment][Test][200] - create and execute artifact order
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}      access_token     ${suite_water_agent_access_token}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}      product_service_id     ${suite_service_id}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}      product_service_name     ${suite_service_name}
    [Common] - Set default value for keyword in dictionary      ${arg_dic}      amount     10
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set test variable      ${test_ext_transaction_id}     ${text}
    set test variable      ${test_product_name}     ${text}
    set test variable      ${test_product_ref_1}     ${text}
    set test variable      ${test_payer_user_id}     ${num}
    ${arg_dic}      create dictionary   access_token=${arg_dic.access_token}    ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${arg_dic.product_service_id}      product_service_name=${arg_dic.product_service_name}
    ...     product_name=${test_product_name}       product_ref_1=${test_product_ref_1}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}     product_ref_3=${text}
    ...     payer_user_ref_type=bank token       payer_user_ref_value=${suite_bank_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}      payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_customer_id}       payee_user_type=${suite_customer_type}
    ...     amount=${arg_dic.amount}
    ${dic}      [Payment][Reuse][200] - create and execute artifact order      ${arg_dic}
    set test variable     ${test_artifact_order_id}       ${dic.order_id}

[Payment][Test][200] - create and execute normal order with payee ref is bank token
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set test variable      ${test_ext_transaction_id}     ${text}
    set test variable      ${test_product_name}     ${text}
    set test variable      ${test_product_ref_1}     ${text}
    set test variable      ${test_payer_user_id}     ${num}
    ${arg_dic}      create dictionary   access_token=${suite_water_agent_access_token}    ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${suite_service_id}      product_service_name=${suite_service_name}
    ...     product_name=${test_product_name}       product_ref_1=${test_product_ref_1}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}     product_ref_3=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_water_agent_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=bank token       payee_user_ref_value=${suite_bank_token}
    ...     payee_user_id=${param_not_used}       payee_user_type=${param_not_used}
    ...     amount=10
    ${dic}      [Payment][Reuse][200] - create and execute normal order        ${arg_dic}
    set test variable     ${test_normal_order_id}       ${dic.order_id}

[Payment][Test][200] - create and execute normal order with payee ref is sof card token
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set test variable      ${test_ext_transaction_id}     ${text}
    set test variable      ${test_product_name}     ${text}
    set test variable      ${test_product_ref_1}     ${text}
    set test variable      ${test_payer_user_id}     ${num}
    ${arg_dic}      create dictionary   access_token=${suite_water_agent_access_token}    ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${suite_service_id}      product_service_name=${suite_service_name}
    ...     product_name=${test_product_name}       product_ref_1=${test_product_ref_1}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}     product_ref_3=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_water_agent_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=card sof token       payee_user_ref_value=${suite_card_token}
    ...     payee_user_id=${param_not_used}       payee_user_type=${param_not_used}
    ...     amount=10
    ${dic}      [Payment][Reuse][200] - create and execute normal order        ${arg_dic}
    set test variable     ${test_normal_order_id}       ${dic.order_id}

[Payment][Test][200] - create normal service structure
   [Arguments]     ${access_token}      ${currency}
   ${text}=   generate random string    10      [LETTERS]
   ${number}=   generate random string    1      123
   ${number1}=   generate random string    3      123456789
   ${service_group_name}      set variable      ${text}
   ${service_name}      set variable    ${text}
   ${currency}      set variable    ${currency}
   ${command_id}     set variable    1
   ${fee_tier_condition}     set variable    unlimit
   ${condition_amount}     set variable    null
   ${fee_type}     set variable    % rate
   ${fee_amount}     set variable    10
   ${bonus_type}     set variable    NON
   ${bonus_amount}     set variable    null
   ${amount_type}     set variable    null
   ${settlement_type}     set variable    Amount

    ${debit}  set variable    {"action_type":"debit","actor_type":"Payer","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    ${credit}  set variable   {"action_type":"credit","actor_type":"Payee","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${debit}  ${credit}

    ${arg_dic}      create dictionary   access_token=${access_token}    service_group_name=${service_group_name}
    ...     service_name=${service_name}    currency=${currency}    command_id=${command_id}    fee_tier_condition=${fee_tier_condition}
    ...     condition_amount=${condition_amount}        fee_type=${fee_type}    fee_amount=${fee_amount}
    ...     bonus_type=${bonus_type}        bonus_amount=${bonus_amount}    amount_type=${amount_type}      settlement_type=${settlement_type}
    ...     balance_distributions=${balance_distributions}

   ${return_dic}   [Payment][Reuse][200] - create service structure       ${arg_dic}

    set test variable      ${test_service_id}        ${return_dic.service_id}
    set test variable      ${test_service_name}        ${return_dic.service_name}
    set test variable      ${test_service_group_id}        ${return_dic.service_group_id}
    set test variable      ${test_service_group_name}        ${return_dic.service_group_name}

[Payment][Test][200] - create normal service structure in "${currency}" currency
   [Payment][Test][200] - create normal service structure      ${suite_admin_access_token}      ${currency}

[Payment][Test][200] - create and execute adjustment order
    ${reference_order_id}=     generate random string      10      123456789
    set test variable      ${test_reference_order_id}     ${reference_order_id}
    ${arg_dic}   create dictionary   access_token=${suite_admin_access_token}     product_service_id=${suite_service_id}
    ...     reference_order_id=${test_reference_order_id}      initiator_user_id=${suite_water_agent_id}
    ...     initiator_user_type_id=2    initiator_user_sof_id=${suite_water_sof_cash_id}
    ...     initiator_sof_type_id=2     payer_user_id=${suite_water_agent_id}     payer_user_type_id=2
    ...     payer_user_sof_id=${suite_water_sof_cash_id}     payer_user_sof_type_id=2
    ...     payee_user_id=${suite_customer_id}     payee_user_type_id=1        payee_user_sof_id=${suite_customer_sof_cash_id_in_VND}
    ...     payee_user_sof_type_id=2      amount=10

    ${dic}      [Workflow][Reuse][200] - create and execute adjustment order      ${arg_dic}
    set test variable     ${test_adjustment_order_id}       ${dic.order_id}

[Trust_Management][Test][200] - create and execute trust order
    ${ext_transaction_id}=     generate random string      10      123456789
    ${arg_dic}   create dictionary   access_token=${suite_trusted_agent_access_token}	ext_transaction_id=${ext_transaction_id}
    ...     trust_token=${suite_trust_token} 	product_service_id=${suite_service_id} 	product_service_name=${suite_service_name}
    ...     payer_user_id=${suite_trusted_agent_id}	payer_user_type_name=${suite_trusted_agent_type}	payer_sof_id=${suite_trusted_sof_cash_id}
    ...     payer_sof_type_id=2     payee_user_id=${suite_water_agent_id}	payee_user_type_name=${suite_water_agent_type}
    ...     payee_sof_id=${suite_water_sof_cash_id}	    payee_sof_type_id=2
    ...     amount=10
    ${dic}      [Trust_Management][Reuse][200] - create and execute trust order      ${arg_dic}
    set test variable     ${test_trust_order_id}       ${dic.order_id}

[Agent][Suite][200] - create trusted agent in "${currency}" currency
    ${text}=   generate random string    10      [LETTERS]
    ${agent_type_name}      set variable   	${text}
    ${agent_type_id}      set variable   	${setup_company_agent_type_id}
    ${agent_unique_reference}      set variable   	${text}
    ${agent_username}     set variable    ${text}
    ${agent_password}      set variable   	${setup_agent_password_encrypted}
    ${agent_password_login}     set variable    ${setup_agent_password_encrypted_utf8}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      agent_type_name=${agent_type_name}
    ...     unique_reference=${agent_unique_reference}      identity_identity_type_id=1
    ...     agent_username=${agent_username}      agent_password=${agent_password}
    ...     agent_password_login=${agent_password_login}      currency=${currency}
    ...     user_type_id=2

    ${dic}  [Agent][Reuse][200] - create agent      ${arg_dic}
    set suite variable      ${suite_trusted_agent_id}       ${dic.agent_id}
    set suite variable      ${suite_trusted_agent_type}       agent
    set suite variable      ${suite_trusted_sof_cash_id}       ${dic.sof_cash_id_in_${currency}}
    set suite variable      ${suite_trusted_agent_access_token}     ${dic.agent_access_token}

[Agent][Suite][200] - create truster agent in "${currency}" currency
    ${text}=   generate random string    10      [LETTERS]
    ${agent_type_name}      set variable   	${text}
    ${agent_type_id}      set variable   	${setup_company_agent_type_id}
    ${agent_unique_reference}      set variable   	${text}
    ${agent_username}     set variable    ${text}
    ${agent_password}      set variable   	${setup_agent_password_encrypted}
    ${agent_password_login}     set variable    ${setup_agent_password_encrypted_utf8}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      agent_type_name=${agent_type_name}
    ...     unique_reference=${agent_unique_reference}      identity_identity_type_id=1
    ...     agent_username=${agent_username}      agent_password=${agent_password}
    ...     agent_password_login=${agent_password_login}      currency=${currency}
    ...     user_type_id=2

    ${dic}  [Agent][Reuse][200] - create agent      ${arg_dic}
    set suite variable      ${suite_truster_agent_id}       ${dic.agent_id}
    set suite variable      ${suite_truster_agent_type}       agent
    set suite variable      ${suite_truster_sof_cash_id}       ${dic.sof_cash_id_in_${currency}}
    set suite variable      ${suite_truster_agent_access_token}     ${dic.agent_access_token}

[Payment][Suite][200] - company agent fund in for trusted agent
    ${text}=     generate random string      15      [LETTERS]
    ${arg_dic}      create dictionary   access_token=${suite_company_agent_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_service_id}      product_service_name=${suite_service_name}      product_name=${text}
    ...     product_ref_1=${text}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}     product_ref_3=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_company_agent_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_trusted_agent_id}       payee_user_type=${suite_trusted_agent_type}
    ...     amount=500000
    [Payment][Reuse][200] - create and execute normal order    ${arg_dic}


[Payroll][Test][200] - create and execute payroll order
    ${ext_transaction_id}=     generate random string      10      123456789
    ${arg_dic}   create dictionary   access_token=${suite_admin_access_token}      ext_transaction_id=${ext_transaction_id}
    ...     product_service_id=${suite_service_id}  	product_service_name=${suite_service_name}
    ...     payer_user_id=${suite_agent_A_id}	payer_user_type=${suite_agent_A_type}
    ...     payee_user_id=${suite_agent_B_id}	payee_user_type=${suite_agent_B_type}	amount=10
    ${dic}      [200] API system create and execute payroll order      ${arg_dic}
    set test variable     ${test_payroll_order_id}       ${dic.order_id}

[Agent][Suite][200] - create agent A in "${currency}" currency under company
    ${text}=   generate random string    10      [LETTERS]
    ${agent_type_name}      set variable   	${text}
    ${agent_type_id}      set variable   	${setup_company_agent_type_id}
    ${agent_unique_reference}      set variable   	${text}
    ${agent_username}     set variable    ${text}
    ${agent_password}      set variable   	${setup_agent_password_encrypted}
    ${agent_password_login}     set variable    ${setup_agent_password_encrypted_utf8}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      agent_type_name=${agent_type_name}
    ...     unique_reference=${agent_unique_reference}      identity_identity_type_id=1
    ...     agent_username=${agent_username}      agent_password=${agent_password}
    ...     agent_password_login=${agent_password_login}      currency=${currency}
    ...     user_type_id=2      company_id=${suite_company_id}

    ${dic}  [Agent][Reuse][200] - create agent      ${arg_dic}
    set suite variable      ${suite_agent_A_id}       ${dic.agent_id}
    set suite variable      ${suite_agent_A_type}       agent
    set suite variable      ${suite_agent_A_sof_cash_id}       ${dic.sof_cash_id_in_${currency}}
    set suite variable      ${suite_agent_A_access_token}     ${dic.agent_access_token}

[System_User][Suite][200] link system user with company
    ${arg_dic}      create dictionary
                    ...  company_profile_id=${suite_company_id}
                    ...  user_id=${suite_admin_user_id}
                    ...  access_token=${suite_admin_access_token}
    [200] API system user link system user top company profile     ${arg_dic}

[Agent][Suite][200] - system user update company wallet user for agent A
    ${arg_dic}  create dictionary   access_token=${suite_admin_access_token}   company_id=${suite_company_id}    wallet_user_id=${suite_agent_A_id}
    ...     wallet_user_type_id=2   wallet_user_type_name=${suite_agent_A_type}
    [200] API system user update company wallet user    ${arg_dic}

[Agent][Suite][200] - system user update company wallet user for agent
    ${arg_dic}  create dictionary
    ...     access_token=${suite_wallet_agent_access_token}
    ...     company_id=${suite_company_id}
    ...     wallet_user_id=${suite_wallet_agent_id}
    ...     wallet_user_type_id=2
    ...     wallet_user_type_name=agent
    [200] API system user update company wallet user    ${arg_dic}

[Agent][Suite][200] - create agent B in "${currency}" currency under company
    ${text}=   generate random string    10      [LETTERS]
    ${agent_type_name}      set variable   	${text}
    ${agent_type_id}      set variable   	${setup_company_agent_type_id}
    ${agent_unique_reference}      set variable   	${text}
    ${agent_username}     set variable    ${text}
    ${agent_password}      set variable   	${setup_agent_password_encrypted}
    ${agent_password_login}     set variable    ${setup_agent_password_encrypted_utf8}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}      agent_type_name=${agent_type_name}
    ...     unique_reference=${agent_unique_reference}      identity_identity_type_id=1
    ...     agent_username=${agent_username}      agent_password=${agent_password}
    ...     agent_password_login=${agent_password_login}      currency=${currency}
    ...     user_type_id=2      company_id=${suite_company_id}

    ${dic}  [Agent][Reuse][200] - create agent      ${arg_dic}
    set suite variable      ${suite_agent_B_id}       ${dic.agent_id}
    set suite variable      ${suite_agent_B_type}       agent
    set suite variable      ${suite_agent_B_sof_cash_id}       ${dic.sof_cash_id_in_${currency}}
    set suite variable      ${suite_agent_B_access_token}     ${dic.agent_access_token}

[Payment][Suite][200] - company agent fund in for agent A
    ${text}=     generate random string      15      [LETTERS]
    ${arg_dic}      create dictionary   access_token=${suite_company_agent_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_service_id}      product_service_name=${suite_service_name}      product_name=${text}
    ...     product_ref_1=${text}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}     product_ref_3=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_company_agent_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_agent_A_id}       payee_user_type=${suite_agent_A_type}
    ...     amount=500000
    [Payment][Reuse][200] - create and execute normal order    ${arg_dic}

[Payment][Test][200] - add payment command for service in "${currency}" currency
   ${text}=   generate random string    10      [LETTERS]
   ${number}=   generate random string    1      123
   ${number1}=   generate random string    3      123456789
   ${currency}      set variable   	${currency}
   ${command_id}     set variable    1
   ${fee_tier_condition}     set variable    unlimit
   ${condition_amount}     set variable    null
   ${fee_type}     set variable    % rate
   ${fee_amount}     set variable    10
   ${bonus_type}     set variable    NON
   ${bonus_amount}     set variable    null
   ${amount_type}     set variable    null
   ${settlement_type}     set variable    Amount

    ${debit}  set variable    {"action_type":"debit","actor_type":"Payer","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    ${credit}  set variable   {"action_type":"credit","actor_type":"Payee","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${debit}  ${credit}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}
    ...     service_id=${test_service_id}    currency=${currency}    command_id=${command_id}    fee_tier_condition=${fee_tier_condition}
    ...     condition_amount=${condition_amount}        fee_type=${fee_type}    fee_amount=${fee_amount}
    ...     bonus_type=${bonus_type}        bonus_amount=${bonus_amount}    amount_type=${amount_type}      settlement_type=${settlement_type}
    ...     balance_distributions=${balance_distributions}

    [Payment][Reuse][200] - add "Payment" command info for service      ${arg_dic}

[Voucher][Suite][200] - create multiple-use vouchers in "${currency}" currency
    ${prefix}     generate random string      10      [LETTERS]
    ${number_of_digit}     generate random string      1      3456789
    ${discount_amount}     generate random string      2      123456789
    ${discount_code}     generate random string      10      [LETTERS]
    ${voucher_group}     generate random string      10      [LETTERS]
    set suite variable      ${suite_multiple_use_voucher_code}     ${discount_code}
    set suite variable      ${suite_multiple_use_voucher_type}     % rate
    set suite variable      ${suite_multiple_use_voucher_amount}    ${discount_amount}
    set suite variable      ${suite_multiple_use_voucher_currency}    ${currency}
    set suite variable      ${suite_multiple_use_voucher_payer_user_id}    ${suite_water_agent_id}
    set suite variable      ${suite_multiple_use_voucher_payer_user_type}    agent
    set suite variable      ${suite_multiple_use_voucher_payer_user_sof_id}    ${suite_water_sof_cash_id}
    set suite variable      ${suite_multiple_use_voucher_payer_user_sof_type_id}    2
    set suite variable      ${suite_multiple_use_voucher_applied_to}    Amount
    set suite variable      ${suite_multiple_use_voucher_min_amount}    1
    set suite variable      ${suite_multiple_use_voucher_max_discount}    100
    set suite variable      ${suite_multiple_use_voucher_permitted_service_group_id}    ${suite_service_group_id}
    set suite variable      ${suite_multiple_use_voucher_permitted_service_id}    ${suite_service_id}
    set suite variable      ${suite_multiple_use_voucher_max_per_user}    100

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}
    ...     voucher_code=${suite_multiple_use_voucher_code}     discount_type=${suite_multiple_use_voucher_type}
    ...     discount_amount=${suite_multiple_use_voucher_amount}       currency=${suite_multiple_use_voucher_currency}
    ...     payer_user_id=${suite_multiple_use_voucher_payer_user_id}      payer_user_type=${suite_multiple_use_voucher_payer_user_type}
    ...     payer_user_sof_id=${suite_multiple_use_voucher_payer_user_sof_id}       payer_user_sof_type_id=${suite_multiple_use_voucher_payer_user_sof_type_id}
    ...     applied_to=${suite_multiple_use_voucher_applied_to}     min_amount=${suite_multiple_use_voucher_min_amount}     max_discount=${suite_multiple_use_voucher_max_discount}
    ...     permitted_service_group_id=${suite_multiple_use_voucher_permitted_service_group_id}     permitted_service_id=${suite_multiple_use_voucher_permitted_service_id}
    ...     max_per_user=${suite_multiple_use_voucher_max_per_user}
    ${dic}     [200] API create multiple-use vouchers     ${arg_dic}
    set suite variable        ${suite_id_of_mulitple_use_voucher}        ${dic.id}

[Voucher][Test][200] - create multiple-use vouchers in "${currency}" currency
    ${prefix}     generate random string      10      [LETTERS]
    ${number_of_digit}     generate random string      1      3456789
    ${discount_amount}     generate random string      2      123456789
    ${discount_code}     generate random string      10      [LETTERS]
    ${voucher_group}     generate random string      10      [LETTERS]
    set test variable      ${test_multiple_use_voucher_code}     ${discount_code}
    set test variable      ${test_multiple_use_voucher_type}     % rate
    set test variable      ${test_multiple_use_voucher_amount}    ${discount_amount}
    set test variable      ${test_multiple_use_voucher_currency}    ${currency}
    set test variable      ${test_multiple_use_voucher_payer_user_id}    ${suite_water_agent_id}
    set test variable      ${test_multiple_use_voucher_payer_user_type}    agent
    set test variable      ${test_multiple_use_voucher_payer_user_sof_id}    ${suite_water_sof_cash_id}
    set test variable      ${test_multiple_use_voucher_payer_user_sof_type_id}    2
    set test variable      ${test_multiple_use_voucher_applied_to}    Amount
    set test variable      ${test_multiple_use_voucher_min_amount}    1
    set test variable      ${test_multiple_use_voucher_max_discount}    100
    set test variable      ${test_multiple_use_voucher_permitted_service_group_id}    ${empty}
    set test variable      ${test_multiple_use_voucher_permitted_service_id}    ${empty}
    set test variable      ${test_multiple_use_voucher_max_per_user}    100

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}
    ...     voucher_code=${test_multiple_use_voucher_code}     discount_type=${test_multiple_use_voucher_type}
    ...     discount_amount=${test_multiple_use_voucher_amount}       currency=${test_multiple_use_voucher_currency}
    ...     payer_user_id=${test_multiple_use_voucher_payer_user_id}      payer_user_type=${test_multiple_use_voucher_payer_user_type}
    ...     payer_user_sof_id=${test_multiple_use_voucher_payer_user_sof_id}       payer_user_sof_type_id=${test_multiple_use_voucher_payer_user_sof_type_id}
    ...     applied_to=${test_multiple_use_voucher_applied_to}     min_amount=${test_multiple_use_voucher_min_amount}     max_discount=${test_multiple_use_voucher_max_discount}
    ...     permitted_service_group_id=${test_multiple_use_voucher_permitted_service_group_id}     permitted_service_id=${test_multiple_use_voucher_permitted_service_id}
    ...     max_per_user=${test_multiple_use_voucher_max_per_user}
    ${dic}     [200] API create multiple-use vouchers     ${arg_dic}
    set test variable        ${test_id_of_mulitple_use_voucher}        ${dic.id}

[Report][Test][200] - search voucher by the id of voucher "${test_id_of_voucher}"
    ${arg_dic}      create dictionary       access_token=${suite_admin_access_token}    id=${test_id_of_voucher}
    ${dic}  [200] API search voucher    ${arg_dic}
    [Return]    ${dic}

[Payment][Suite][200] - create A-O service structure in "${currency}" currency
   ${text}=   generate random string    10      [LETTERS]
   ${number}=   generate random string    1      123
   ${number1}=   generate random string    3      123456789
   ${service_group_name}      set variable   	${text}
   ${service_name}      set variable   	${text}
   ${currency}      set variable   	${currency}
   ${command_id}     set variable    1
   ${fee_tier_condition}     set variable    unlimit
   ${condition_amount}     set variable    null
   ${fee_type}     set variable    % rate
   ${fee_amount}     set variable    10
   ${bonus_type}     set variable    NON
   ${bonus_amount}     set variable    null
   ${amount_type}     set variable    null
   ${settlement_type}     set variable    Amount

    ${debit}  set variable    {"action_type":"debit","actor_type":"Payer","sof_type_id":null,"specific_sof":null,"amount_type":"A","rate":null}
    ${credit}  set variable   {"action_type":"credit","actor_type":"Payee","sof_type_id":null,"specific_sof":null,"amount_type":"A","rate":null}
    @{balance_distributions}  create list     ${debit}  ${credit}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}    service_group_name=${service_group_name}
    ...     service_name=${service_name}    currency=${currency}    command_id=${command_id}    fee_tier_condition=${fee_tier_condition}
    ...     condition_amount=${condition_amount}        fee_type=${fee_type}    fee_amount=${fee_amount}
    ...     bonus_type=${bonus_type}        bonus_amount=${bonus_amount}    amount_type=${amount_type}      settlement_type=${settlement_type}
    ...     balance_distributions=${balance_distributions}

   ${return_dic}   [Payment][Reuse][200] - create service structure       ${arg_dic}

    set suite variable      ${suite_service_a_o_id}        ${return_dic.service_id}
    set suite variable      ${suite_service_a_o_name}        ${return_dic.service_name}
    set suite variable      ${suite_service_a_o_group_id}        ${return_dic.service_group_id}
    set suite variable      ${suite_service_a_o_group_name}        ${return_dic.service_group_name}

[Payment][Test][200] - create normal order
    [Arguments]    ${arg_dic}
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set test variable      ${test_ext_transaction_id}     ${text}
    set test variable      ${test_product_name}     ${text}
    set test variable      ${test_product_ref_1}     ${text}
    set test variable      ${test_payer_user_id}     ${num}
    ${arg_dic}      create dictionary   access_token=${arg_dic.access_token}    ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${arg_dic.product_service_id}      product_service_name=${arg_dic.product_service_name}
    ...     product_name=${test_product_name}       product_ref_1=${test_product_ref_1}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}     product_ref_3=${text}
    ...     payer_user_ref_type=${arg_dic.payer_user_ref_type}       payer_user_ref_value=${arg_dic.payer_user_ref_value}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${arg_dic.payee_user_id}       payee_user_type=${arg_dic.payee_user_type}
    ...     amount=10
    ${dic}      [Payment][Reuse][200] - create normal order        ${arg_dic}
    set test variable     ${test_order_id}       ${dic.order_id}

[Payment][Test][200] - apply tier level mask to an order
    [Arguments]    ${arg_dic}
    set test variable   ${test_a_tier_level_name}    A
    set test variable   ${test_a_tier_level_value}    10
    set test variable   ${test_b_tier_level_name}    B
    set test variable   ${test_b_tier_level_value}    20
    ${tier_level_masks}    catenate
        ...     		{
        ...     			"tier_level_name": "${test_a_tier_level_name}",
        ...     			"value": ${test_a_tier_level_value}
        ...     		},
        ...     		{
        ...     			"tier_level_name": "${test_b_tier_level_name}",
        ...     			"value": ${test_b_tier_level_value}
        ...     		}
    ${arg_dic}  create dictionary  access_token=${arg_dic.access_token}     tier_level_masks=${tier_level_masks}    order_id=${arg_dic.order_id}
    [200] API apply tier level mask to an order     ${arg_dic}

[Payment][Test][200] - execute normal order
    [Arguments]   ${access_token}   ${order_id}
    ${arg_dic}  create dictionary   access_token=${access_token}    order_id=${order_id}
    [200] API payment execute order     ${arg_dic}

[Voucher][Test][200] - create multiple-use vouchers in "${currency}" currency with soon expriry date
    ${prefix}     generate random string      10      [LETTERS]
    ${number_of_digit}     generate random string      1      3456789
    ${discount_amount}     generate random string      2      123456789
    ${discount_code}     generate random string      10      [LETTERS]
    ${voucher_group}     generate random string      10      [LETTERS]
    ${expiry_date}      Get Current Date    UTC     result_format=%Y-%m-%dT%H:%M:%SZ
    ${expiry_date}      Add Time To Date    ${expiry_date}      3s    result_format=%Y-%m-%dT%H:%M:%SZ
    set test variable      ${test_multiple_use_voucher_code}     ${discount_code}
    set test variable      ${test_multiple_use_voucher_type}     % rate
    set test variable      ${test_multiple_use_voucher_amount}    ${discount_amount}
    set test variable      ${test_multiple_use_voucher_currency}    ${currency}
    set test variable      ${test_multiple_use_voucher_payer_user_id}    ${suite_water_agent_id}
    set test variable      ${test_multiple_use_voucher_payer_user_type}    agent
    set test variable      ${test_multiple_use_voucher_payer_user_sof_id}    ${suite_water_sof_cash_id}
    set test variable      ${test_multiple_use_voucher_payer_user_sof_type_id}    2
    set test variable      ${test_multiple_use_voucher_applied_to}    Amount
    set test variable      ${test_multiple_use_voucher_min_amount}    1
    set test variable      ${test_multiple_use_voucher_max_discount}    100
    set test variable      ${test_multiple_use_voucher_permitted_service_group_id}    ${empty}
    set test variable      ${test_multiple_use_voucher_permitted_service_id}    ${empty}
    set test variable      ${test_multiple_use_voucher_max_per_user}    100
    set test variable      ${test_multiple_use_voucher_expiry_date}    ${expiry_date}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}
    ...     voucher_code=${test_multiple_use_voucher_code}     discount_type=${test_multiple_use_voucher_type}
    ...     discount_amount=${test_multiple_use_voucher_amount}       currency=${test_multiple_use_voucher_currency}
    ...     payer_user_id=${test_multiple_use_voucher_payer_user_id}      payer_user_type=${test_multiple_use_voucher_payer_user_type}
    ...     payer_user_sof_id=${test_multiple_use_voucher_payer_user_sof_id}       payer_user_sof_type_id=${test_multiple_use_voucher_payer_user_sof_type_id}
    ...     applied_to=${test_multiple_use_voucher_applied_to}     min_amount=${test_multiple_use_voucher_min_amount}     max_discount=${test_multiple_use_voucher_max_discount}
    ...     permitted_service_group_id=${test_multiple_use_voucher_permitted_service_group_id}     permitted_service_id=${test_multiple_use_voucher_permitted_service_id}
    ...     max_per_user=${test_multiple_use_voucher_max_per_user}
    ...     max_per_user=${test_multiple_use_voucher_max_per_user}      expiration_date=${test_multiple_use_voucher_expiry_date}
    ${dic}     [200] API create multiple-use vouchers     ${arg_dic}
    set test variable        ${test_id_of_mulitple_use_voucher}        ${dic.id}

[Payment][Suite][200] - Create "Remittance Cash-In" service structure belongs to service group A
   ${text}=   generate random string    10      [LETTERS]
   ${number}=   generate random string    1      123
   ${number1}=   generate random string    3      123456789
   ${service_group_name}    set variable     ${text}
   ${service_name}          set variable    CashIn_${text}
   ${currency}              set variable    VND
   ${command_id}            set variable    1
   ${fee_tier_condition}    set variable    unlimit
   ${condition_amount}      set variable    null
   ${fee_type}              set variable    % rate
   ${fee_amount}            set variable    10
   ${bonus_type}            set variable    NON
   ${bonus_amount}          set variable    null
   ${amount_type}           set variable    null
   ${settlement_type}       set variable    Amount

   ${post_payment_spi_url_type}             set variable    Post-Payment
   ${post_payment_url}                      set variable    ${internal_voucher_generate_url}
   ${post_payment_spi_url_call_method}      set variable    synchronous
   ${post_payment_expire_in_minute}         set variable    ${number}
   ${post_payment_max_retry}                set variable    ${number}
   ${post_payment_retry_delay_millisecond}  set variable    ${number1}
   ${post_payment_read_timeout}             set variable    120000

    ${debit}  set variable    {"action_type":"debit","actor_type":"Payer","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    ${credit}  set variable    {"action_type":"credit","actor_type":"Payee","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${debit}  ${credit}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}    service_group_name=${service_group_name}
    ...     service_name=${service_name}    currency=${currency}    command_id=${command_id}    fee_tier_condition=${fee_tier_condition}
    ...     condition_amount=${condition_amount}        fee_type=${fee_type}    fee_amount=${fee_amount}
    ...     bonus_type=${bonus_type}        bonus_amount=${bonus_amount}    amount_type=${amount_type}      settlement_type=${settlement_type}
    ...     post_payment_spi_url_type=${post_payment_spi_url_type}    post_payment_url=${post_payment_url}      post_payment_spi_url_call_method=${post_payment_spi_url_call_method}      post_payment_expire_in_minute=${post_payment_expire_in_minute}
    ...     post_payment_read_timeout=${post_payment_read_timeout}    post_payment_max_retry=${post_payment_max_retry}      post_payment_retry_delay_millisecond=${post_payment_retry_delay_millisecond}
    ...     balance_distributions=@{balance_distributions}

   ${return_dic}   [Payment][Reuse][200] - create service structure        ${arg_dic}

    set suite variable  ${suite_remittance_cash_in_service_id}      ${return_dic.service_id}
    set suite variable  ${suite_remittance_cash_in_service_name}    ${return_dic.service_name}
    set suite variable  ${suite_service_group_a_id}                 ${return_dic.service_group_id}

[Payment][Suite][200] - Create "P2P Cash-In" service structure belongs to service group A
    ${text}=         generate random string  10  [LETTERS]
    ${number}=       generate random string  1   123
    ${number1}=      generate random string  3   123456789
    ${service_group_name}    set variable     ${text}
    ${service_name}          set variable    CashIn_${text}
    ${currency}              set variable    VND
    ${command_id}            set variable    1
    ${fee_tier_condition}    set variable    unlimit
    ${condition_amount}      set variable    null
    ${fee_type}              set variable    % rate
    ${fee_amount}            set variable    10
    ${bonus_type}            set variable    NON
    ${bonus_amount}          set variable    null
    ${amount_type}           set variable    null
    ${settlement_type}       set variable    Amount

    ${post_payment_spi_url_type}             set variable    Post-Payment
    ${post_payment_url}                      set variable    ${internal_voucher_p2p_generate_url}
    ${post_payment_spi_url_call_method}      set variable    synchronous
    ${post_payment_expire_in_minute}         set variable    ${number}
    ${post_payment_max_retry}                set variable    ${number}
    ${post_payment_retry_delay_millisecond}  set variable    ${number1}
    ${post_payment_read_timeout}             set variable    120000

    ${debit}  set variable    {"action_type":"debit","actor_type":"Payer","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    ${credit}  set variable    {"action_type":"credit","actor_type":"Payee","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${debit}  ${credit}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}    service_group_name=${service_group_name}
    ...     service_name=${service_name}    currency=${currency}    command_id=${command_id}    fee_tier_condition=${fee_tier_condition}
    ...     condition_amount=${condition_amount}        fee_type=${fee_type}    fee_amount=${fee_amount}
    ...     bonus_type=${bonus_type}        bonus_amount=${bonus_amount}    amount_type=${amount_type}      settlement_type=${settlement_type}
    ...     post_payment_spi_url_type=${post_payment_spi_url_type}    post_payment_url=${post_payment_url}      post_payment_spi_url_call_method=${post_payment_spi_url_call_method}      post_payment_expire_in_minute=${post_payment_expire_in_minute}
    ...     post_payment_read_timeout=${post_payment_read_timeout}    post_payment_max_retry=${post_payment_max_retry}      post_payment_retry_delay_millisecond=${post_payment_retry_delay_millisecond}
    ...     balance_distributions=@{balance_distributions}

    ${return_dic}   [Payment][Reuse] - create service under service group id    ${suite_service_group_a_id}    ${arg_dic}

    set suite variable  ${suite_p2p_cash_in_service_id}     ${return_dic.service_id}
    set suite variable  ${suite_p2p_cash_in_service_name}   ${return_dic.service_name}

[Payment][Suite][200] - Create "Payout Cash-In" service structure belongs to service group A
    ${text}=         generate random string  10  [LETTERS]
    ${number}=       generate random string  1   123
    ${number1}=      generate random string  3   123456789
    ${service_group_name}    set variable     ${text}
    ${service_name}          set variable    CashIn_${text}
    ${currency}              set variable    VND
    ${command_id}            set variable    1
    ${fee_tier_condition}    set variable    unlimit
    ${condition_amount}      set variable    null
    ${fee_type}              set variable    % rate
    ${fee_amount}            set variable    10
    ${bonus_type}            set variable    NON
    ${bonus_amount}          set variable    null
    ${amount_type}           set variable    null
    ${settlement_type}       set variable    Amount

    ${post_payment_spi_url_type}             set variable    Post-Payment
    ${post_payment_url}                      set variable    ${internal_voucher_payout_generate_url}
    ${post_payment_spi_url_call_method}      set variable    synchronous
    ${post_payment_expire_in_minute}         set variable    ${number}
    ${post_payment_max_retry}                set variable    ${number}
    ${post_payment_retry_delay_millisecond}  set variable    ${number1}
    ${post_payment_read_timeout}             set variable    120000

    ${debit}  set variable    {"action_type":"debit","actor_type":"Payer","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    ${credit}  set variable    {"action_type":"credit","actor_type":"Payee","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${debit}  ${credit}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}    service_group_name=${service_group_name}
    ...     service_name=${service_name}    currency=${currency}    command_id=${command_id}    fee_tier_condition=${fee_tier_condition}
    ...     condition_amount=${condition_amount}        fee_type=${fee_type}    fee_amount=${fee_amount}
    ...     bonus_type=${bonus_type}        bonus_amount=${bonus_amount}    amount_type=${amount_type}      settlement_type=${settlement_type}
    ...     post_payment_spi_url_type=${post_payment_spi_url_type}    post_payment_url=${post_payment_url}      post_payment_spi_url_call_method=${post_payment_spi_url_call_method}      post_payment_expire_in_minute=${post_payment_expire_in_minute}
    ...     post_payment_read_timeout=${post_payment_read_timeout}    post_payment_max_retry=${post_payment_max_retry}      post_payment_retry_delay_millisecond=${post_payment_retry_delay_millisecond}
    ...     balance_distributions=@{balance_distributions}

    ${return_dic}   [Payment][Reuse] - create service under service group id    ${suite_service_group_a_id}    ${arg_dic}

    set suite variable  ${suite_payout_cash_in_service_id}     ${return_dic.service_id}
    set suite variable  ${suite_payout_cash_in_service_name}   ${return_dic.service_name}

[Payment][Suite][200] - Create "Remittance Cash-Out" service structure belongs to service group B
    ${text}=   generate random string    10      [LETTERS]
    ${number}=   generate random string    1      123
    ${number1}=   generate random string    3      123456789
    ${service_group_name}      set variable     ${text}
    ${service_name}      set variable       CashOut_${text}
    ${currency}      set variable       VND
    ${command_id}     set variable    1
    ${fee_tier_condition}     set variable    unlimit
    ${condition_amount}     set variable    null
    ${fee_type}     set variable    % rate
    ${fee_amount}     set variable    10
    ${bonus_type}     set variable    NON
    ${bonus_amount}     set variable    null
    ${amount_type}     set variable    null
    ${settlement_type}     set variable    Amount

    ${pre_payment_spi_url_type}     set variable     Pre-Payment
    ${pre_payment_url}     set variable      ${internal_voucher_verification_url}
    ${pre_payment_spi_url_call_method}     set variable      synchronous
    ${pre_payment_expire_in_minute}     set variable     ${number}
    ${pre_payment_max_retry}     set variable        ${number}
    ${pre_payment_retry_delay_millisecond}     set variable      ${number1}
    ${pre_payment_read_timeout}     set variable      120000

    ${post_payment_spi_url_type}     set variable     Post-Payment
    ${post_payment_url}     set variable      ${internal_voucher_claim_url}
    ${post_payment_spi_url_call_method}     set variable      synchronous
    ${post_payment_expire_in_minute}     set variable     ${number}
    ${post_payment_max_retry}     set variable        ${number}
    ${post_payment_retry_delay_millisecond}     set variable      ${number1}
    ${post_payment_read_timeout}     set variable      120000
    ${debit}  set variable    {"action_type":"Debit","actor_type":"Specific ID","specific_actor_id":${suite_agent_remittance_id},"sof_type_id":2,"specific_sof":${suite_agent_remittance_sof_cash_id},"amount_type":"Amount","rate":null}
    ${credit}  set variable    {"action_type":"credit","actor_type":"Payee","sof_type_id":2,"specific_sof":null,"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${debit}  ${credit}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}    service_group_name=${service_group_name}
    ...     service_name=${service_name}    currency=${currency}    command_id=${command_id}    fee_tier_condition=${fee_tier_condition}
    ...     condition_amount=${condition_amount}        fee_type=${fee_type}    fee_amount=${fee_amount}
    ...     bonus_type=${bonus_type}        bonus_amount=${bonus_amount}    amount_type=${amount_type}      settlement_type=${settlement_type}
    ...     pre_payment_spi_url_type=${pre_payment_spi_url_type}    pre_payment_url=${pre_payment_url}      pre_payment_spi_url_call_method=${pre_payment_spi_url_call_method}      pre_payment_expire_in_minute=${pre_payment_expire_in_minute}
    ...     pre_payment_read_timeout=${pre_payment_read_timeout}    pre_payment_max_retry=${pre_payment_max_retry}      pre_payment_retry_delay_millisecond=${pre_payment_retry_delay_millisecond}
    ...     post_payment_spi_url_type=${post_payment_spi_url_type}    post_payment_url=${post_payment_url}      post_payment_spi_url_call_method=${pre_payment_spi_url_call_method}      post_payment_expire_in_minute=${post_payment_expire_in_minute}
    ...     post_payment_read_timeout=${post_payment_read_timeout}    post_payment_max_retry=${post_payment_max_retry}      post_payment_retry_delay_millisecond=${post_payment_retry_delay_millisecond}
    ...     balance_distributions=@{balance_distributions}

    ${return_dic}   [Payment][Reuse][200] - create service structure        ${arg_dic}

    set suite variable  ${suite_remittance_cash_out_service_id}     ${return_dic.service_id}
    set suite variable  ${suite_remittance_cash_out_service_name}   ${return_dic.service_name}
    set suite variable  ${suite_service_group_b_id}                 ${return_dic.service_group_id}

[Payment][Suite][200] - Create "P2P Cash-Out" service structure belongs to service group B
    ${text}=   generate random string    10      [LETTERS]
    ${number}=   generate random string    1      123
    ${number1}=   generate random string    3      123456789
    ${service_group_name}      set variable     ${text}
    ${service_name}      set variable       CashOut_${text}
    ${currency}      set variable       VND
    ${command_id}     set variable    1
    ${fee_tier_condition}     set variable    unlimit
    ${condition_amount}     set variable    null
    ${fee_type}     set variable    % rate
    ${fee_amount}     set variable    10
    ${bonus_type}     set variable    NON
    ${bonus_amount}     set variable    null
    ${amount_type}     set variable    null
    ${settlement_type}     set variable    Amount

    ${pre_payment_spi_url_type}     set variable     Pre-Payment
    ${pre_payment_url}     set variable      ${internal_voucher_verification_url}
    ${pre_payment_spi_url_call_method}     set variable      synchronous
    ${pre_payment_expire_in_minute}     set variable     ${number}
    ${pre_payment_max_retry}     set variable        ${number}
    ${pre_payment_retry_delay_millisecond}     set variable      ${number1}
    ${pre_payment_read_timeout}     set variable      120000

    ${post_payment_spi_url_type}     set variable     Post-Payment
    ${post_payment_url}     set variable      ${internal_voucher_claim_url}
    ${post_payment_spi_url_call_method}     set variable      synchronous
    ${post_payment_expire_in_minute}     set variable     ${number}
    ${post_payment_max_retry}     set variable        ${number}
    ${post_payment_retry_delay_millisecond}     set variable      ${number1}
    ${post_payment_read_timeout}     set variable      120000
    ${debit}  set variable    {"action_type":"Debit","actor_type":"Specific ID","specific_actor_id":${suite_agent_remittance_id},"sof_type_id":2,"specific_sof":${suite_agent_remittance_sof_cash_id},"amount_type":"Amount","rate":null}
    ${credit}  set variable    {"action_type":"credit","actor_type":"Payee","sof_type_id":2,"specific_sof":null,"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${debit}  ${credit}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}    service_group_name=${service_group_name}
    ...     service_name=${service_name}    currency=${currency}    command_id=${command_id}    fee_tier_condition=${fee_tier_condition}
    ...     condition_amount=${condition_amount}    fee_type=${fee_type}    fee_amount=${fee_amount}
    ...     bonus_type=${bonus_type}        bonus_amount=${bonus_amount}    amount_type=${amount_type}              settlement_type=${settlement_type}
    ...     pre_payment_spi_url_type=${pre_payment_spi_url_type}    pre_payment_url=${pre_payment_url}              pre_payment_spi_url_call_method=${pre_payment_spi_url_call_method}      pre_payment_expire_in_minute=${pre_payment_expire_in_minute}
    ...     pre_payment_read_timeout=${pre_payment_read_timeout}    pre_payment_max_retry=${pre_payment_max_retry}  pre_payment_retry_delay_millisecond=${pre_payment_retry_delay_millisecond}
    ...     post_payment_spi_url_type=${post_payment_spi_url_type}  post_payment_url=${post_payment_url}            post_payment_spi_url_call_method=${pre_payment_spi_url_call_method}      post_payment_expire_in_minute=${post_payment_expire_in_minute}
    ...     post_payment_read_timeout=${post_payment_read_timeout}  post_payment_max_retry=${post_payment_max_retry}    post_payment_retry_delay_millisecond=${post_payment_retry_delay_millisecond}
    ...     balance_distributions=@{balance_distributions}

    ${return_dic}   [Payment][Reuse] - create service under service group id    ${suite_service_group_b_id}     ${arg_dic}

    set suite variable  ${suite_p2p_cash_out_service_id}     ${return_dic.service_id}
    set suite variable  ${suite_p2p_cash_out_service_name}   ${return_dic.service_name}

[Payment][Suite][200] - Create "Payout Cash-Out" service structure belongs to service group B
    ${text}=   generate random string    10      [LETTERS]
    ${number}=   generate random string    1      123
    ${number1}=   generate random string    3      123456789
    ${service_group_name}      set variable     ${text}
    ${service_name}      set variable       CashOut_${text}
    ${currency}      set variable       VND
    ${command_id}     set variable    1
    ${fee_tier_condition}     set variable    unlimit
    ${condition_amount}     set variable    null
    ${fee_type}     set variable    % rate
    ${fee_amount}     set variable    10
    ${bonus_type}     set variable    NON
    ${bonus_amount}     set variable    null
    ${amount_type}     set variable    null
    ${settlement_type}     set variable    Amount

    ${pre_payment_spi_url_type}     set variable     Pre-Payment
    ${pre_payment_url}     set variable      ${internal_voucher_verification_url}
    ${pre_payment_spi_url_call_method}     set variable      synchronous
    ${pre_payment_expire_in_minute}     set variable     ${number}
    ${pre_payment_max_retry}     set variable        ${number}
    ${pre_payment_retry_delay_millisecond}     set variable      ${number1}
    ${pre_payment_read_timeout}     set variable      120000

    ${post_payment_spi_url_type}     set variable     Post-Payment
    ${post_payment_url}     set variable      ${internal_voucher_claim_url}
    ${post_payment_spi_url_call_method}     set variable      synchronous
    ${post_payment_expire_in_minute}     set variable     ${number}
    ${post_payment_max_retry}     set variable        ${number}
    ${post_payment_retry_delay_millisecond}     set variable      ${number1}
    ${post_payment_read_timeout}     set variable      120000
    ${debit}  set variable    {"action_type":"Debit","actor_type":"Specific ID","specific_actor_id":${suite_agent_remittance_id},"sof_type_id":2,"specific_sof":${suite_agent_remittance_sof_cash_id},"amount_type":"Amount","rate":null}
    ${credit}  set variable    {"action_type":"credit","actor_type":"Payee","sof_type_id":2,"specific_sof":null,"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${debit}  ${credit}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}    service_group_name=${service_group_name}
    ...     service_name=${service_name}    currency=${currency}    command_id=${command_id}    fee_tier_condition=${fee_tier_condition}
    ...     condition_amount=${condition_amount}        fee_type=${fee_type}    fee_amount=${fee_amount}
    ...     bonus_type=${bonus_type}        bonus_amount=${bonus_amount}    amount_type=${amount_type}      settlement_type=${settlement_type}
    ...     pre_payment_spi_url_type=${pre_payment_spi_url_type}    pre_payment_url=${pre_payment_url}      pre_payment_spi_url_call_method=${pre_payment_spi_url_call_method}      pre_payment_expire_in_minute=${pre_payment_expire_in_minute}
    ...     pre_payment_read_timeout=${pre_payment_read_timeout}    pre_payment_max_retry=${pre_payment_max_retry}      pre_payment_retry_delay_millisecond=${pre_payment_retry_delay_millisecond}
    ...     post_payment_spi_url_type=${post_payment_spi_url_type}    post_payment_url=${post_payment_url}      post_payment_spi_url_call_method=${pre_payment_spi_url_call_method}      post_payment_expire_in_minute=${post_payment_expire_in_minute}
    ...     post_payment_read_timeout=${post_payment_read_timeout}    post_payment_max_retry=${post_payment_max_retry}      post_payment_retry_delay_millisecond=${post_payment_retry_delay_millisecond}
    ...     balance_distributions=@{balance_distributions}

    ${return_dic}   [Payment][Reuse] - create service under service group id    ${suite_service_group_b_id}     ${arg_dic}

    set suite variable  ${suite_payout_cash_out_service_id}     ${return_dic.service_id}
    set suite variable  ${suite_payout_cash_out_service_name}   ${return_dic.service_name}

[Payment][Suite][200] - Create "Remittance Cash-In" service structure belongs to service group B
    ${text}=   generate random string    10      [LETTERS]
    ${number}=   generate random string    1      123
    ${number1}=   generate random string    3      123456789
    ${service_group_name}    set variable     ${text}
    ${service_name}          set variable    CashIn_${text}
    ${currency}              set variable    VND
    ${command_id}            set variable    1
    ${fee_tier_condition}    set variable    unlimit
    ${condition_amount}      set variable    null
    ${fee_type}              set variable    % rate
    ${fee_amount}            set variable    10
    ${bonus_type}            set variable    NON
    ${bonus_amount}          set variable    null
    ${amount_type}           set variable    null
    ${settlement_type}       set variable    Amount

    ${post_payment_spi_url_type}             set variable    Post-Payment
    ${post_payment_url}                      set variable    ${internal_voucher_generate_url}
    ${post_payment_spi_url_call_method}      set variable    synchronous
    ${post_payment_expire_in_minute}         set variable    ${number}
    ${post_payment_max_retry}                set variable    ${number}
    ${post_payment_retry_delay_millisecond}  set variable    ${number1}
    ${post_payment_read_timeout}             set variable    120000

    ${debit}  set variable    {"action_type":"debit","actor_type":"Payer","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    ${credit}  set variable    {"action_type":"credit","actor_type":"Payee","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${debit}  ${credit}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}    service_group_name=${service_group_name}
    ...     service_name=${service_name}    currency=${currency}    command_id=${command_id}    fee_tier_condition=${fee_tier_condition}
    ...     condition_amount=${condition_amount}        fee_type=${fee_type}    fee_amount=${fee_amount}
    ...     bonus_type=${bonus_type}        bonus_amount=${bonus_amount}    amount_type=${amount_type}      settlement_type=${settlement_type}
    ...     post_payment_spi_url_type=${post_payment_spi_url_type}    post_payment_url=${post_payment_url}      post_payment_spi_url_call_method=${post_payment_spi_url_call_method}      post_payment_expire_in_minute=${post_payment_expire_in_minute}
    ...     post_payment_read_timeout=${post_payment_read_timeout}    post_payment_max_retry=${post_payment_max_retry}      post_payment_retry_delay_millisecond=${post_payment_retry_delay_millisecond}
    ...     balance_distributions=@{balance_distributions}

    ${return_dic}   [Payment][Reuse] - create service under service group id    ${suite_service_group_b_id}     ${arg_dic}
    set suite variable  ${suite_remittance_cash_in_b_service_id}      ${return_dic.service_id}
    set suite variable  ${suite_remittance_cash_in_b_service_name}    ${return_dic.service_name}
    [Return]    ${return_dic}

[Payment][Suite][200] - Create normal service structure belongs to service group B
    ${text}=   generate random string    10      [LETTERS]
    ${number}=   generate random string    1      123
    ${number1}=   generate random string    3      123456789
    ${service_group_name}   set variable   	${text}
    ${service_name}         set variable   	${text}
    ${currency}             set variable    VND
    ${command_id}           set variable    1
    ${fee_tier_condition}   set variable    unlimit
    ${condition_amount}     set variable    null
    ${fee_type}             set variable    % rate
    ${fee_amount}           set variable    10
    ${bonus_type}           set variable    NON
    ${bonus_amount}         set variable    null
    ${amount_type}          set variable    null
    ${settlement_type}      set variable    Amount

    ${debit}  set variable    {"action_type":"debit","actor_type":"Payer","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    ${credit}  set variable   {"action_type":"credit","actor_type":"Payee","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${debit}  ${credit}

    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}    service_group_name=${service_group_name}
    ...     service_name=${service_name}    currency=${currency}    command_id=${command_id}    fee_tier_condition=${fee_tier_condition}
    ...     condition_amount=${condition_amount}        fee_type=${fee_type}    fee_amount=${fee_amount}
    ...     bonus_type=${bonus_type}        bonus_amount=${bonus_amount}    amount_type=${amount_type}      settlement_type=${settlement_type}
    ...     balance_distributions=${balance_distributions}

    ${return_dic}   [Payment][Reuse] - create service under service group id    ${suite_service_group_b_id}     ${arg_dic}
    set suite variable  ${suite_normal_b_service_id}    ${return_dic.service_id}
    set suite variable  ${suite_normal_b_service_name}  ${return_dic.service_name}
    [Return]    ${return_dic}

[Payment][Test][200] - Create and execute remittance cash in order by agent A
    ${text}=     generate random string      15      [LETTERS]
    ${phone_number}=     generate random string      15      [NUMBERS]
    ${arg_dic}      create dictionary   access_token=${test_agent_a_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_remittance_cash_in_service_id}      product_service_name=${suite_remittance_cash_in_service_name}
    ...     product_name=${text}       product_ref_1=${text}     product_ref_2=${text}     product_ref_3=${phone_number}
    ...     product_ref_4=${text}     product_ref_5=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${test_agent_a_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_agent_remittance_id}       payee_user_type=${suite_agent_remittance_type}
    ...     amount=10
    ${dic}      [Payment][Reuse][200] - create and execute normal order        ${arg_dic}
    set test variable     ${test_cash_in_order_id}       ${dic.order_id}
    set test variable  ${test_order_text}     ${text}
    set test variable  ${test_phone_number}     ${phone_number}

[Payment][Test][200] - Create and execute p2p cash in order by agent A
    ${text}=     generate random string      15      [LETTERS]
    ${phone_number}=     generate random string      15      [NUMBERS]
    ${arg_dic}      create dictionary   access_token=${test_agent_a_access_token}   ext_transaction_id=${text}
    ...     product_service_id=${suite_p2p_cash_in_service_id}  product_service_name=${suite_p2p_cash_in_service_name}
    ...     product_name=${text}    product_ref_1=${text}   product_ref_2=${text}   product_ref_3=${phone_number}
    ...     product_ref_4=${text}   product_ref_5=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${test_agent_a_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_agent_remittance_id}       payee_user_type=${suite_agent_remittance_type}
    ...     amount=10
    ${dic}      [Payment][Reuse][200] - create and execute normal order        ${arg_dic}
    set test variable   ${test_p2p_cash_in_order_id}    ${dic.order_id}
    set test variable   ${test_p2p_cash_in_passcode}    ${text}
    set test variable   ${test_p2p_phone_number}        ${phone_number}

[Payment][Test][200] - Create and execute payout cash in order by agent A
    ${text}=     generate random string      15      [LETTERS]
    ${phone_number}=     generate random string      15      [NUMBERS]
    ${arg_dic}      create dictionary   access_token=${test_agent_a_access_token}   ext_transaction_id=${text}
    ...     product_service_id=${suite_payout_cash_in_service_id}   product_service_name=${suite_payout_cash_in_service_name}
    ...     product_name=${text}    product_ref_1=${text}   product_ref_2=${text}   product_ref_3=${phone_number}
    ...     product_ref_4=${text}   product_ref_5=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${test_agent_a_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_agent_remittance_id}       payee_user_type=${suite_agent_remittance_type}
    ...     amount=10
    ${dic}      [Payment][Reuse][200] - create and execute normal order        ${arg_dic}
    set test variable   ${test_payout_cash_in_order_id}     ${dic.order_id}
    set test variable   ${test_payout_cash_in_passcode}     ${text}
    set test variable   ${test_payout_phone_number}         ${phone_number}

[Payment][Test][200] - Agent B create cash in order with service belongs to service group A
    ${text}=     generate random string      15      [LETTERS]
    ${phone_number}=     generate random string      15      [NUMBERS]
    ${arg_dic}      create dictionary   access_token=${test_agent_b_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_remittance_cash_in_service_id}      product_service_name=${suite_remittance_cash_in_service_name}
    ...     product_name=${text}       product_ref_1=${text}     product_ref_2=${text}     product_ref_3=${phone_number}
    ...     product_ref_4=${text}     product_ref_5=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${test_agent_b_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_agent_remittance_id}       payee_user_type=${suite_agent_remittance_type}
    ...     amount=10
    ${dic}      [Payment][Reuse][200] - create and execute normal order        ${arg_dic}
    set test variable     ${test_cash_in_order_id}       ${dic.order_id}
    set test variable  ${test_order_text}     ${text}

[Payment][Test][Fail] - Agent B create cash in order with service belongs to service group B
    ${text}=     generate random string      15      [LETTERS]
    ${phone_number}=     generate random string      15      [NUMBERS]
    ${arg_dic}      create dictionary   access_token=${test_agent_b_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_remittance_cash_in_b_service_id}      product_service_name=${suite_remittance_cash_in_b_service_name}
    ...     product_name=${text}    product_ref_1=${text}   product_ref_2=${text}   product_ref_3=${phone_number}
    ...     product_ref_4=${text}   product_ref_5=${text}
    ...     payer_user_ref_type=access token        payer_user_ref_value=${test_agent_b_access_token}
    ...     payer_user_id=${param_not_used}         payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}   payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_agent_remittance_id}  payee_user_type=${suite_agent_remittance_type}
    ...     amount=10
    ${dic}  [Payment][Reuse][Fail] - create normal order with service_id under blocked service group    ${arg_dic}

[Payment][Test][Fail] - Agent B create normal order with service belongs to service group B
    ${text}=     generate random string      15      [LETTERS]
    ${phone_number}=     generate random string      15      [NUMBERS]
    ${arg_dic}      create dictionary   access_token=${test_agent_b_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_normal_b_service_id}      product_service_name=${suite_normal_b_service_name}
    ...     product_name=${text}    product_ref_1=${text}   product_ref_2=${text}   product_ref_3=${phone_number}
    ...     product_ref_4=${text}   product_ref_5=${text}
    ...     payer_user_ref_type=access token        payer_user_ref_value=${test_agent_b_access_token}
    ...     payer_user_id=${param_not_used}         payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}   payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_agent_remittance_id}  payee_user_type=${suite_agent_remittance_type}
    ...     amount=10
    ${dic}  [Payment][Reuse][Fail] - create normal order with service_id under blocked service group    ${arg_dic}

[Payment][Test][Fail] - Agent B create cash out order with service belongs to service group B
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set test variable      ${test_ext_transaction_id}     ${text}
    set test variable      ${test_product_name}     ${text}
    set test variable      ${test_product_ref_1}    ${text}
    set test variable      ${test_payer_user_id}    ${num}
    ${arg_dic}      create dictionary   access_token=${test_agent_b_access_token}   ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${suite_remittance_cash_out_service_id}       product_service_name=${suite_remittance_cash_out_service_name}
    ...     payer_user_ref_type=access token        payer_user_ref_value=${test_agent_b_access_token}
    ...     payer_user_id=${param_not_used}         payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}   payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${test_agent_c_id}        payee_user_type=${test_agent_c_type}
    ...     amount=10   product_name=${test_order_text}
    ...     product_ref_1=${test_cash_in_voucher_id}    product_ref_2=${test_order_text}    product_ref_3=${test_phone_number}
    ...     product_ref_4=${test_order_text}    product_ref_5=${test_order_text}
    ${dic}  [Payment][Reuse][Fail] - create normal order with service_id under blocked service group    ${arg_dic}

[Payment][Test][200] - Agent B create and execute remittance cash out order using service belongs to service group B
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set test variable      ${test_ext_transaction_id}     ${text}
    set test variable      ${test_product_name}     ${text}
    set test variable      ${test_product_ref_1}     ${text}
    set test variable      ${test_payer_user_id}     ${num}
    ${arg_dic}      create dictionary   access_token=${test_agent_b_access_token}    ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${suite_remittance_cash_out_service_id}       product_service_name=${suite_remittance_cash_out_service_name}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${test_agent_b_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}        payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${test_agent_c_id}       payee_user_type=${test_agent_c_type}
    ...     amount=10   product_name=${test_order_text}
    ...     product_ref_1=${test_cash_in_voucher_id}    product_ref_2=${test_order_text}    product_ref_3=${test_phone_number}
    ...     product_ref_4=${test_order_text}    product_ref_5=${test_order_text}
    ${dic}  [Payment][Reuse][200] - create and execute normal order    ${arg_dic}

[Payment][Test][Fail] - Customer create and execute remittance cash out order with invalid phone number and valid voucher_id in x times
    [Arguments]     ${number}
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set test variable      ${test_ext_transaction_id}     ${text}
    set test variable      ${test_product_name}     ${text}
    set test variable      ${test_product_ref_1}     ${text}
    set test variable      ${test_payer_user_id}     ${num}
    ${arg_dic}      create dictionary   access_token=${test_customer_access_token}    ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${suite_remittance_cash_out_service_id}       product_service_name=${suite_remittance_cash_out_service_name}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${test_customer_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}        payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${test_customer_id}       payee_user_type=${test_customer_type}
    ...     amount=10   product_name=${test_order_text}
    ...     product_ref_1=${test_cash_in_voucher_id}    product_ref_2=${test_order_text}    product_ref_3=invalid
    ...     product_ref_4=${test_order_text}    product_ref_5=${test_order_text}
    :FOR    ${index}    IN RANGE    0   ${number}
    \   ${dic}  [Payment][Reuse][Fail] - create and execute normal order with invalid phone number and valid voucher_id  ${arg_dic}

[Payment][Test][Fail] - Customer create and execute p2p cash out order with invalid phone number and valid voucher_id in x times
    [Arguments]     ${number}
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set test variable   ${test_ext_transaction_id}  ${text}
    set test variable   ${test_product_name}        ${text}
    set test variable   ${test_product_ref_1}       ${text}
    set test variable   ${test_payer_user_id}       ${num}
    ${arg_dic}      create dictionary   access_token=${test_customer_access_token}    ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${suite_p2p_cash_out_service_id}     product_service_name=${suite_p2p_cash_out_service_name}
    ...     payer_user_ref_type=access token    payer_user_ref_value=${test_customer_access_token}
    ...     payer_user_id=${param_not_used}     payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}   payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${test_customer_id}       payee_user_type=${test_customer_type}
    ...     amount=10   product_name=${test_product_name}
    ...     product_ref_1=${test_cash_in_voucher_id}    product_ref_2=${test_p2p_cash_in_passcode}  product_ref_3=invalid
    ...     product_ref_4=${test_p2p_cash_in_passcode}  product_ref_5=${test_p2p_cash_in_passcode}
    :FOR    ${index}    IN RANGE    0   ${number}
    \   ${dic}  [Payment][Reuse][Fail] - create and execute normal order with invalid phone number and valid voucher_id  ${arg_dic}

[Payment][Test][Fail] - Customer create and execute payout cash out order with invalid phone number and valid voucher_id in x times
    [Arguments]     ${number}
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set test variable   ${test_ext_transaction_id}  ${text}
    set test variable   ${test_product_name}        ${text}
    set test variable   ${test_product_ref_1}       ${text}
    set test variable   ${test_payer_user_id}       ${num}
    ${arg_dic}      create dictionary   access_token=${test_customer_access_token}    ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${suite_payout_cash_out_service_id}     product_service_name=${suite_payout_cash_out_service_name}
    ...     payer_user_ref_type=access token    payer_user_ref_value=${test_customer_access_token}
    ...     payer_user_id=${param_not_used}     payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}   payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${test_customer_id}       payee_user_type=${test_customer_type}
    ...     amount=10   product_name=${test_product_name}
    ...     product_ref_1=${test_cash_in_voucher_id}    product_ref_2=${test_payout_cash_in_passcode}  product_ref_3=invalid
    ...     product_ref_4=${test_payout_cash_in_passcode}   product_ref_5=${test_payout_cash_in_passcode}
    :FOR    ${index}    IN RANGE    0   ${number}
    \   ${dic}  [Payment][Reuse][Fail] - create and execute normal order with invalid phone number and valid voucher_id  ${arg_dic}

[Payment][Test][Fail] - Customer create and execute remittance cash out order with invalid voucher_id in x times
    [Arguments]     ${number}
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set test variable      ${test_ext_transaction_id}     ${text}
    set test variable      ${test_product_name}     ${text}
    set test variable      ${test_product_ref_1}     ${text}
    set test variable      ${test_payer_user_id}     ${num}
    ${arg_dic}      create dictionary   access_token=${test_customer_access_token}    ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${suite_remittance_cash_out_service_id}       product_service_name=${suite_remittance_cash_out_service_name}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${test_customer_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}        payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${test_customer_id}       payee_user_type=${test_customer_type}
    ...     amount=10   product_name=${test_order_text}
    ...     product_ref_1=invalid    product_ref_2=${test_order_text}    product_ref_3=${test_phone_number}
    ...     product_ref_4=${test_order_text}    product_ref_5=${test_order_text}
    :FOR    ${index}    IN RANGE    0   ${number}
    \   ${dic}  [Payment][Reuse][Fail] - create and execute normal order with valid phone number and invalid voucher_id  ${arg_dic}

[Payment][Test][Fail] - Customer create and execute p2p cash out order with invalid voucher_id in x times
    [Arguments]     ${number}
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set test variable      ${test_ext_transaction_id}   ${text}
    set test variable      ${test_product_name}         ${text}
    set test variable      ${test_product_ref_1}        ${text}
    set test variable      ${test_payer_user_id}        ${num}
    ${arg_dic}      create dictionary   access_token=${test_customer_access_token}    ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${suite_p2p_cash_out_service_id}     product_service_name=${suite_p2p_cash_out_service_name}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${test_customer_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}        payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${test_customer_id}       payee_user_type=${test_customer_type}
    ...     amount=10   product_name=${test_p2p_cash_in_passcode}
    ...     product_ref_1=invalid    product_ref_2=${test_p2p_cash_in_passcode}    product_ref_3=${test_p2p_phone_number}
    ...     product_ref_4=${test_p2p_cash_in_passcode}    product_ref_5=${test_p2p_cash_in_passcode}
    :FOR    ${index}    IN RANGE    0   ${number}
    \   ${dic}  [Payment][Reuse][Fail] - create and execute normal order with valid phone number and invalid voucher_id  ${arg_dic}

[Payment][Test][Fail] - Customer create and execute remittance cash out order with valid phone number and blocked voucher_id
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set test variable      ${test_ext_transaction_id}     ${text}
    set test variable      ${test_product_name}     ${text}
    set test variable      ${test_product_ref_1}     ${text}
    set test variable      ${test_payer_user_id}     ${num}
    ${arg_dic}      create dictionary   access_token=${test_customer_access_token}    ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${suite_remittance_cash_out_service_id}       product_service_name=${suite_remittance_cash_out_service_name}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${test_customer_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}        payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${test_customer_id}       payee_user_type=${test_customer_type}
    ...     amount=10   product_name=${test_order_text}
    ...     product_ref_1=${test_cash_in_voucher_id}    product_ref_2=${test_order_text}    product_ref_3=${test_phone_number}
    ...     product_ref_4=${test_order_text}    product_ref_5=${test_order_text}
    ${dic}      [Payment][Reuse][Fail] - create and execute normal order with valid phone number and blocked voucher_id   ${arg_dic}

[Payment][Test][Fail] - Customer create and execute p2p cash out order with valid phone number and blocked voucher code
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set test variable   ${test_ext_transaction_id}  ${text}
    set test variable   ${test_product_name}        ${text}
    set test variable   ${test_product_ref_1}       ${text}
    set test variable   ${test_payer_user_id}       ${num}
    ${arg_dic}      create dictionary   access_token=${test_customer_access_token}    ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${suite_p2p_cash_out_service_id}     product_service_name=${suite_p2p_cash_out_service_name}
    ...     payer_user_ref_type=access token            payer_user_ref_value=${test_customer_access_token}
    ...     payer_user_id=${param_not_used}             payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${test_customer_id}           payee_user_type=${test_customer_type}
    ...     amount=10   product_name=${test_p2p_cash_in_passcode}
    ...     product_ref_1=${test_cash_in_voucher_id}    product_ref_2=${test_p2p_cash_in_passcode}    product_ref_3=${test_p2p_phone_number}
    ...     product_ref_4=${test_p2p_cash_in_passcode}  product_ref_5=${test_p2p_cash_in_passcode}
    ${dic}      [Payment][Reuse][Fail] - create and execute normal order with valid phone number and blocked voucher_id   ${arg_dic}

[Payment][Test][200] - Customer create and execute payout cash out order with valid both phone number and voucher code
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set test variable   ${test_ext_transaction_id}  ${text}
    set test variable   ${test_product_name}        ${text}
    set test variable   ${test_product_ref_1}       ${text}
    set test variable   ${test_payer_user_id}       ${num}
    ${arg_dic}      create dictionary   access_token=${test_customer_access_token}    ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${suite_payout_cash_out_service_id}  product_service_name=${suite_payout_cash_out_service_id}
    ...     payer_user_ref_type=access token            payer_user_ref_value=${test_customer_access_token}
    ...     payer_user_id=${param_not_used}             payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${test_customer_id}           payee_user_type=${test_customer_type}
    ...     amount=10   product_name=${test_payout_cash_in_passcode}
    ...     product_ref_1=${test_cash_in_voucher_id}    product_ref_2=${test_payout_cash_in_passcode}    product_ref_3=${test_payout_phone_number}
    ...     product_ref_4=${test_payout_cash_in_passcode}  product_ref_5=${test_payout_cash_in_passcode}
    ${dic}      [Payment][Reuse][200] - create and execute normal order  ${arg_dic}

[Payment][Test][200] - Get order detail of order id
    [Arguments]     ${order_id}
    ${dic}      [200] API get order detail  ${test_agent_a_access_token}    ${order_id}
    set test variable      ${test_cash_in_voucher_id}        ${dic.product_ref1}

[Report][Test][200] - Search voucher by voucher code and passcode
    [Arguments]     ${voucher_code}     ${product_ref2}
    ${arg_dic}  create dictionary   access_token=${suite_company_agent_access_token}    voucher_id=${voucher_code}  product_ref2=${product_ref2}
    ${dic}  [200] API search voucher    ${arg_dic}
    set test variable  ${voucher_detail_dic}    ${dic}

[Payment][Test][200] - company agent fund in for agent A
    ${text}=     generate random string      15      [LETTERS]
    ${arg_dic}      create dictionary   access_token=${suite_company_agent_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_service_id}      product_service_name=${suite_service_name}      product_name=${text}
    ...     product_ref_1=${text}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_company_agent_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${test_agent_A_id}       payee_user_type=${test_agent_A_type}
    ...     amount=500000
    [Payment][Reuse][200] - create and execute normal order    ${arg_dic}

[Payment][Test][200] - company agent fund in for agent B
    ${text}=     generate random string      15      [LETTERS]
    ${arg_dic}      create dictionary   access_token=${suite_company_agent_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_service_id}      product_service_name=${suite_service_name}      product_name=${text}
    ...     product_ref_1=${text}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_company_agent_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${test_agent_B_id}       payee_user_type=${test_agent_B_type}
    ...     amount=500000
    [Payment][Reuse][200] - create and execute normal order    ${arg_dic}

[Payment][Test][200] - Company agent fund in for agent C
    ${text}=     generate random string      15      [LETTERS]
    ${arg_dic}      create dictionary   access_token=${suite_company_agent_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_service_id}      product_service_name=${suite_service_name}      product_name=${text}
    ...     product_ref_1=${text}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_company_agent_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${test_agent_c_id}       payee_user_type=${test_agent_c_type}
    ...     amount=500000
    [Payment][Reuse][200] - create and execute normal order    ${arg_dic}

[Payment][Test][Fail] - Create and execute remittance cash out order with phone number is null and valid voucher_id
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set test variable   ${test_ext_transaction_id}  ${text}
    set test variable   ${test_product_name}        ${text}
    set test variable   ${test_product_ref_1}       ${text}
    set test variable   ${test_payer_user_id}       ${num}
    ${arg_dic}      create dictionary   access_token=${test_agent_b_access_token}    ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${suite_remittance_cash_out_service_id}       product_service_name=${suite_remittance_cash_out_service_name}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${test_agent_b_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}        payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${test_agent_c_id}       payee_user_type=${test_agent_c_type}
    ...     amount=10   product_name=${test_order_text}
    ...     product_ref_1=${test_cash_in_voucher_id}    product_ref_2=${test_order_text}    product_ref_3=${null}
    ...     product_ref_4=${test_order_text}    product_ref_5=${test_order_text}
    ${dic}      [Payment][Reuse][Fail] - create and execute normal order with phone number is null and valid voucher_id  ${arg_dic}

[Payment][Test][Fail] - Create and execute remittance cash out order with valid phone number and invalid voucher_id
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    ${voucher_id}=      generate random string      5      [NUMBERS]
    set test variable   ${test_ext_transaction_id}  ${text}
    set test variable   ${test_product_name}    ${text}
    set test variable   ${test_product_ref_1}   ${text}
    set test variable   ${test_payer_user_id}   ${num}
    ${arg_dic}      create dictionary   access_token=${test_agent_b_access_token}   ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${suite_remittance_cash_out_service_id}       product_service_name=${suite_remittance_cash_out_service_name}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${test_agent_b_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}        payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${test_agent_c_id}       payee_user_type=${test_agent_c_type}
    ...     amount=10   product_name=${test_order_text}
    ...     product_ref_1=${voucher_id}    product_ref_2=${test_order_text}    product_ref_3=${test_phone_number}
    ...     product_ref_4=${test_order_text}    product_ref_5=${test_order_text}
    ${dic}      [Payment][Reuse][Fail] - create and execute normal order with valid phone number and invalid voucher_id  ${arg_dic}

[Payment][Test][Fail] - Create and execute remittance cash out order with invalid both phone number and voucher_id
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    ${voucher_id}=      generate random string      5      [NUMBERS]
    set test variable   ${test_ext_transaction_id}     ${text}
    set test variable   ${test_product_name}    ${text}
    set test variable   ${test_product_ref_1}   ${text}
    set test variable   ${test_payer_user_id}   ${num}
    ${arg_dic}      create dictionary   access_token=${test_agent_b_access_token}   ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${suite_remittance_cash_out_service_id}       product_service_name=${suite_remittance_cash_out_service_name}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${test_agent_b_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}        payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${test_agent_c_id}       payee_user_type=${test_agent_c_type}
    ...     amount=10   product_name=${test_order_text}
    ...     product_ref_1=${voucher_id}    product_ref_2=${test_order_text}    product_ref_3=${text}
    ...     product_ref_4=${test_order_text}    product_ref_5=${test_order_text}
    ${dic}      [Payment][Reuse][Fail] - create and execute normal order with both invalid phone number and voucher_id  ${arg_dic}

[Payment][Test][Fail] - Create and execute remittance cash out order with valid phone number and invalid voucher_id by customer
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set test variable      ${test_ext_transaction_id}     ${text}
    set test variable      ${test_product_name}     ${text}
    set test variable      ${test_product_ref_1}     ${text}
    set test variable      ${test_payer_user_id}     ${num}
    ${arg_dic}      create dictionary   access_token=${test_customer_access_token}    ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${suite_remittance_cash_out_service_id}       product_service_name=${suite_remittance_cash_out_service_name}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${test_customer_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}        payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${test_customer_id}       payee_user_type=${test_customer_type}
    ...     amount=10   product_name=${test_order_text}
    ...     product_ref_1=${text}    product_ref_2=${test_order_text}    product_ref_3=${test_phone_number}
    ...     product_ref_4=${test_order_text}    product_ref_5=${test_order_text}
    ${dic}      [Payment][Reuse][Fail] - create and execute normal order with valid phone number and invalid voucher_id  ${arg_dic}

[Payment][Test][Fail] - Customer create and execute p2p cash out order with valid phone number and invalid voucher_id
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set test variable      ${test_ext_transaction_id}     ${text}
    set test variable      ${test_product_name}     ${text}
    set test variable      ${test_product_ref_1}     ${text}
    set test variable      ${test_payer_user_id}     ${num}
    ${arg_dic}      create dictionary   access_token=${test_customer_access_token}    ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${suite_p2p_cash_out_service_id}       product_service_name=${suite_p2p_cash_out_service_name}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${test_customer_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}        payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${test_customer_id}       payee_user_type=${test_customer_type}
    ...     amount=10   product_name=${test_p2p_cash_in_passcode}
    ...     product_ref_1=${text}    product_ref_2=${test_p2p_cash_in_passcode}    product_ref_3=${test_p2p_phone_number}
    ...     product_ref_4=${test_p2p_cash_in_passcode}    product_ref_5=${test_p2p_cash_in_passcode}
    ${dic}      [Payment][Reuse][Fail] - create and execute normal order with valid phone number and invalid voucher_id  ${arg_dic}

[Payment][Test][200] - Create and execute remittance cash out order with valid both phone number and voucher_id by customer
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set test variable      ${test_ext_transaction_id}     ${text}
    set test variable      ${test_product_name}     ${text}
    set test variable      ${test_product_ref_1}     ${text}
    set test variable      ${test_payer_user_id}     ${num}
    ${arg_dic}      create dictionary   access_token=${test_customer_access_token}    ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${suite_remittance_cash_out_service_id}       product_service_name=${suite_remittance_cash_out_service_name}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${test_customer_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}        payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${test_customer_id}       payee_user_type=${test_customer_type}
    ...     amount=10   product_name=${test_order_text}
    ...     product_ref_1=${test_cash_in_voucher_id}    product_ref_2=${test_order_text}    product_ref_3=${test_phone_number}
    ...     product_ref_4=${test_order_text}    product_ref_5=${test_order_text}
    ${dic}      [Payment][Reuse][200] - create and execute normal order  ${arg_dic}

[Payment][Test][200] - Customer create and execute remittance cash out order with valid both phone number and voucher_id
    ${text}=     generate random string      15      [LETTERS]
    ${num}=      generate random string      10      [NUMBERS]
    set test variable      ${test_ext_transaction_id}     ${text}
    set test variable      ${test_product_name}     ${text}
    set test variable      ${test_product_ref_1}     ${text}
    set test variable      ${test_payer_user_id}     ${num}
    ${arg_dic}      create dictionary   access_token=${test_customer_access_token}    ext_transaction_id=${test_ext_transaction_id}
    ...     product_service_id=${suite_p2p_cash_out_service_id}       product_service_name=${suite_p2p_cash_out_service_name}
    ...     payer_user_ref_type=access token        payer_user_ref_value=${test_customer_access_token}
    ...     payer_user_id=${param_not_used}         payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}   payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${test_customer_id}       payee_user_type=${test_customer_type}
    ...     amount=10   product_name=${test_p2p_cash_in_passcode}
    ...     product_ref_1=${test_cash_in_voucher_id}    product_ref_2=${test_p2p_cash_in_passcode}    product_ref_3=${test_p2p_phone_number}
    ...     product_ref_4=${test_p2p_cash_in_passcode}  product_ref_5=${test_p2p_cash_in_passcode}
    ${dic}      [Payment][Reuse][200] - create and execute normal order  ${arg_dic}

[Payment][Suite][200] - Create and execute remittance cash in order by agent A
    ${text}=     generate random string      15      [LETTERS]
    ${phone_number}=     generate random string      15      [NUMBERS]
    ${arg_dic}      create dictionary   access_token=${suite_agent_a_access_token}    ext_transaction_id=${text}
    ...     product_service_id=${suite_remittance_cash_in_service_id}      product_service_name=${suite_remittance_cash_in_service_name}
    ...     product_name=${text}       product_ref_1=${text}     product_ref_2=${text}     product_ref_3=${phone_number}
    ...     product_ref_4=${text}     product_ref_5=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${suite_agent_a_access_token}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${suite_agent_remittance_id}       payee_user_type=${suite_agent_remittance_type}
    ...     amount=10
    ${dic}      [Payment][Reuse][200] - create and execute normal order        ${arg_dic}
    set suite variable  ${suite_cash_in_order_id}       ${dic.order_id}
    set suite variable  ${suite_order_text}     ${text}
    set suite variable  ${suite_phone_number}     ${phone_number}

[Payment][Suite][200] - Get order detail of remittance cash in order
    ${dic}      [200] API get order detail       ${suite_agent_a_access_token}       ${suite_cash_in_order_id}
    set suite variable      ${suite_cash_in_voucher_id}        ${dic.product_ref1}

[Report][Suite][200] - Search voucher by voucher code and passcode
    ${arg_dic}  create dictionary   access_token=${suite_company_agent_access_token}    voucher_id=${suite_cash_in_voucher_id}   product_ref2=${suite_order_text}
    ${dic}  [200] API search voucher    ${arg_dic}
    set suite variable  ${voucher_detail_dic}    ${dic}

[Test][200] - Get order detail
    [Arguments]     &{arg_dic}
    ${dic}         [200] API get order detail   ${arg_dic.access_token}     ${arg_dic.order_id}
    ${product_name}=    get from dictionary    ${dic}    product_name
    ${product_ref1}=    get from dictionary    ${dic}    product_ref1
    ${product_ref2}=    get from dictionary    ${dic}    product_ref2
    ${product_ref3}=    get from dictionary    ${dic}    product_ref3
    ${product_ref4}=    get from dictionary    ${dic}    product_ref4
    ${product_ref5}=    get from dictionary    ${dic}    product_ref5
    set test variable      ${test_product_ref2_encrypted}       ${product_ref2}
    set test variable      ${test_voucher_id}         ${product_ref1}

[Payment][Test][200] - Create beneficiary service structure
   [Arguments]   &{arg_dic}
   ${text}=   generate random string    10      [LETTERS]
   ${number}=   generate random string    1      123
   ${number1}=   generate random string    3      123456789
   ${service_group_name}      set variable   	${text}
   ${service_name}      set variable   	${text}
   ${currency}      set variable   	${arg_dic.currency}
   ${command_id}     set variable    1
   ${fee_tier_condition}     set variable    unlimit
   ${condition_amount}     set variable    null
   ${fee_type}     set variable    % rate
   ${fee_amount}     set variable    10
   ${bonus_type}     set variable    NON
   ${bonus_amount}     set variable    null
   ${amount_type}     set variable    null
   ${settlement_type}     set variable    Amount

    ${debit}  set variable    {"action_type":"debit","actor_type":"Payer's Beneficiary","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    ${credit}  set variable    {"action_type":"debit","actor_type":"Payee's Beneficiary","sof_type_id":null,"specific_sof":null,"amount_type":"Amount","rate":null}
    @{balance_distributions}  create list     ${debit}  ${credit}

    ${arg_dic}      create dictionary   access_token=${arg_dic.access_token}    service_group_name=${service_group_name}
    ...     service_name=${service_name}    currency=${currency}    command_id=${command_id}    fee_tier_condition=${fee_tier_condition}
    ...     condition_amount=${condition_amount}        fee_type=${fee_type}    fee_amount=${fee_amount}
    ...     bonus_type=${bonus_type}        bonus_amount=${bonus_amount}    amount_type=${amount_type}      settlement_type=${settlement_type}
    ...     balance_distributions=${balance_distributions}

   ${return_dic}   [Payment][Reuse][200] - create service structure       ${arg_dic}

    set test variable      ${test_service_beneficiary_id}        ${return_dic.service_id}
    set test variable      ${test_service_beneficiary_name}        ${return_dic.service_name}
    set test variable      ${test_service_beneficiary_group_id}        ${return_dic.service_group_id}
    set test variable      ${test_service_beneficiary_group_name}        ${return_dic.service_group_name}
    set test variable      ${test_service_beneficiary_payment_fee_tier_id}        ${return_dic.fee_tier_id}
    set test variable      ${test_service_beneficiary_payment_command_id}        ${return_dic.service_command_id}

[Agent][200] - Create agent with currency
    [Arguments]     &{arg_dic}
    ${text}=   generate random string    10      [LETTERS]
    ${current_address_postal_code}   generate random string    10      123456789
    ${agent_type_name}      set variable   	${text}
    ${agent_type_id}      set variable   	${setup_company_agent_type_id}
    ${agent_unique_reference}      set variable   	${text}
    ${agent_username}     set variable    ${text}
    ${agent_password}      set variable   	${setup_agent_password_encrypted}
    ${agent_password_login}     set variable    ${setup_agent_password_encrypted_utf8}

    ${created_dic}      create dictionary   access_token=${arg_dic.access_token}      agent_type_name=${agent_type_name}
    ...     unique_reference=${agent_unique_reference}      identity_identity_type_id=1
    ...     agent_username=${agent_username}      agent_password=${agent_password}
    ...     agent_password_login=${agent_password_login}      currency=${arg_dic.currency}
    ...     user_type_id=2      current_address_postal_code=${current_address_postal_code}

    ${return_dic}  [Agent][Reuse][200] - create agent      ${created_dic}

    ${currency_list}     split string    ${arg_dic.currency}     ,
    ${currency_list_length}   get length      ${currency_list}
    :FOR     ${index}   IN RANGE    0   ${currency_list_length}
    \   ${currency}     get from list     ${currency_list}      ${index}
    \   [Common] - Set variable     name=${arg_dic.output_sof_cash_id}_${currency}     value=${return_dic.sof_cash_id_in_${currency}}

    [Common] - Set variable     name=${arg_dic.output_agent_id}     value=${return_dic.agent_id}
    [Common] - Set variable     name=${arg_dic.output_agent_type}     value=agent

[Payment][200] - Company agent fund in
    [Arguments]     &{arg_dic}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      product_service_id      ${suite_service_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      product_service_name      ${suite_service_name}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      payer_user_ref_value    ${suite_company_agent_access_token}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      payee_user_id    ${suite_water_agent_id}
    [Common] - Set default value for keyword in dictionary     ${arg_dic}      payee_user_type    ${suite_water_agent_type}
    ${text}=     generate random string      15      [LETTERS]
    ${created_dic}      create dictionary   access_token=${arg_dic.access_token}    ext_transaction_id=${text}
    ...     product_service_id=${arg_dic.product_service_id}      product_service_name=${arg_dic.product_service_name}      product_name=${text}
    ...     product_ref_1=${text}     product_ref_2=${text}     product_ref_3=${text}
    ...     product_ref_4=${text}     product_ref_5=${text}     product_ref_3=${text}
    ...     payer_user_ref_type=access token       payer_user_ref_value=${arg_dic.payer_user_ref_value}
    ...     payer_user_id=${param_not_used}       payer_user_type=${param_not_used}
    ...     payee_user_ref_type=${param_not_used}       payee_user_ref_value=${param_not_used}
    ...     payee_user_id=${arg_dic.payee_user_id}       payee_user_type=${arg_dic.payee_user_type}
    ...     amount=${arg_dic.amount}
    [Payment][Reuse][200] - create and execute normal order    ${created_dic}