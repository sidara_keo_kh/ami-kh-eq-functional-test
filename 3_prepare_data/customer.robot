*** Settings ***
Resource         ../2_api/3_reuse_function/imports.robot
Resource         ../3_prepare_data/imports.robot

*** Keywords ***
[Customer][200][Prepare] - Delete customer classification
    [Arguments]     &{arg_dic}
    [Common][Prepare] - Access token headers
        ...     access_token=${arg_dic.access_token}
        ...     output=headers
    [Customer][200] - delete customer classification
        ...     headers=${headers}
        ...     customer_classification_id=${arg_dic.customer_classification_id}

[Customer][200][Prepare] - Get all customer identities
    [Arguments]     &{arg_dic}
    [Common][Prepare] - Access token headers
        ...     access_token=${arg_dic.access_token}
        ...     output=headers
    [Customer][200] - get all customer identities
        ...     headers=${headers}
    ${data}    REST.Output    $.data
    [Common] - Set variable     name=${arg_dic.ouput_data}      value=${data}