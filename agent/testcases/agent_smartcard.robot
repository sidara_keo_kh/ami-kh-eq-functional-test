*** Settings ***
Resource          ../../agent/keywords/agent_smartcard.robot

Suite Setup       run keywords
...     [System_User][Suite][200] - login as setup system user
Default Tags      robot     API     agent_smartcard

*** Test Cases ***
TC_EQP_11595 - [API][Fail] Get smart card profile with un-exist serial number
    [Tags]    openshift      API    success      2018-26-VN-SAVIORS     smoke_test
    [Agent][Test][200] - Create agent with required fields both PIN identity and PASSWORD identity
    [Test][200] create smart card via api
    [Test][Fail] get smart card by unexisted serial number via api
    [Fail] [API] [Get smart card] Verify api return smart card not found

TC_EQP_11596 - [API][Fail] Get smart card profile with deleted profile
    [Tags]    openshift      API    success      2018-26-VN-SAVIORS
    [Agent][Test][200] - Create agent with required fields both PIN identity and PASSWORD identity
    [Test][200] create smart card via api
    [Test][200] delete smart card by id via api
    [Test][Fail] get smart card by deleted serial number via api
    [Fail] [API] [Get smart card] Verify api return smart card not found

TC_EQP_11594 - [API][200] Get smart card profile
    [Tags]    openshift      API    success      2018-26-VN-SAVIORS
    [Agent][Test][200] - Create agent with required fields both PIN identity and PASSWORD identity
    [Test][200] create smart card via api
    [Test][200] get smart card by serial number via api
    [200][API] [Get smart card] Verify that all attributes are returned in response