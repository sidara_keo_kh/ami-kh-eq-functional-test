*** Settings ***
Documentation   Test cases for Agent Type
Resource        ../../3_prepare_data/by_api/imports.robot
Resource        ../../3_prepare_data/imports.robot
Resource        ../../agent/keywords/agent_type.robot

Suite Setup       run keywords
...     [System_User][Suite][200] - create account login via api
...     [Agent][Suite][200] - log in as company agent
Default Tags      robot     API     agent_type

*** Test Cases ***
TC_EQP_12462 - Update details sale type - additional fields
    [Tags]  regression  2019-05-TH-Zeus     EQTR-17247      smoke_test
    [Agent][200][Prepare] - Create agent type   access_token=${suite_admin_access_token}    is_sale=True        is_manager=true
    [Agent][200][Prepare] - Update agent type   access_token=${suite_admin_access_token}    is_manager=True     agent_type_id=${test_agent_type_id}
    [Agent][200][Prepare] - Search all agent types  access_token=${suite_admin_access_token}    $.id=${test_agent_type_id}
    [200][API][Agent Type] Verify agent type info   ${test_agent_types}

TC_EQP_12459 - [API][Success] Create sale type with additional fields
    [Tags]  2019-05-TH-Zeus     EQTR-17245
    [Agent][200][Prepare] - Create agent type   access_token=${suite_admin_access_token}    is_sale=true        is_manager=true
    [Agent][200][Prepare] - Search all agent types  access_token=${suite_admin_access_token}    $.id=${test_agent_type_id}
    [200][API][Agent Type] Verify new agent type info has created   ${test_agent_types}
