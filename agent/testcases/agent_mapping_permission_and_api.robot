*** Settings ***
Resource          ../../2_api/3_reuse_function/imports.robot

Suite Setup     [System_User][200][Prepare] - Create system user

Default Tags      robot     API     sof-card


*** Test Cases ***
TC_EQP_12930 - Agent - Mapping api with permission CAN_SEARCH_PAYMENT_ORDER
    [Tags]   2019-06-TH-Minions     EQTR-13420     not_ready
    [System_User][Reuse] - Verify permission - api link
        ...     permission_name=CAN_SEARCH_PAYMENT_ORDER
        ...     keyword=[Agent][200] - get all relationship type
        ...     body={}
