*** Settings ***
Resource         ../../3_prepare_data/by_api/imports.robot
Resource          ../../agent/keywords/agent.robot

Suite Setup       run keywords
...     [System_User][Suite][200] - login as setup system user

*** Test Cases ***
TC_EQP_12637 - [API][Success] Validate permission between 2 connected sales that set set permission of fund in
    [Tags]    openshift      API     success     regression      2019-05-VN-SAVIORS     smoke_test
    [Test][200] create sale and identity with agent type
    [Agent][Test][200] - Set actor agent id
    [Test][200] Create Sale Hierarchy with arguments    access_token=${suite_admin_access_token}    root_id=${test_agent_id}
    [Test][200] create sale and identity with agent type
    [Agent][Test][200] - Set target agent id
    [Agent][Test][200] - Create connection for sale 1 and sale 2    access_token=${suite_admin_access_token}    sale_hierarchy_id=${test_sale_hierarchy_id}     parent_id=${agent_actor_id}     child_id=${agent_target_id}
    [200][Prepare] - Set permission is true with fund in function and actor is sale type 1 and target is sale type 2    access_token=${suite_admin_access_token}    hierarchy_id=${test_sale_hierarchy_id}  permission_id=7     apply_to_all_agent=false    actor_type_id=${sale_type_1_id}     target_type_id=${sale_type_2_id}
    [Agent][Test][200] - Authenticate agent with arguments      username=${test_agent_actor_username}      password=${test_agent_password_login}
    [Agent][Test][200] Check sale fund in permission on actor and target agent  access_token=${test_agent_access_token}    subordinate_id=${agent_target_id}    permission_code=FUND_IN
    [Agent][Test][200] Verify validate role sale permission return true     ${test_result_dic}

TC_EQP_12638 - [API][Success] Validate permission of sales between 2 different hierarchy with different permission for fund in
    [Tags]    openshift      API     success     regression      2019-05-VN-SAVIORS
    [Test][200] create sale and identity with agent type
    [Agent][Test][200] - Set actor agent id
    [Test][200] Create Sale Hierarchy with arguments    access_token=${suite_admin_access_token}    root_id=${test_agent_id}
    [Test][200] create sale and identity with agent type
    [Agent][Test][200] - Set target agent id
    [Agent][Test][200] - Create connection for sale 1 and sale 2    access_token=${suite_admin_access_token}    sale_hierarchy_id=${test_sale_hierarchy_id}     parent_id=${agent_actor_id}     child_id=${agent_target_id}
    [200][Prepare] - Set permission is true with fund in function and actor is sale type 1 and target is sale type 2    access_token=${suite_admin_access_token}    hierarchy_id=${test_sale_hierarchy_id}  permission_id=7     apply_to_all_agent=false    actor_type_id=${sale_type_1_id}     target_type_id=${sale_type_2_id}
    [Agent][Test][200] - Authenticate agent with arguments      username=${test_agent_actor_username}      password=${test_agent_password_login}
    [Test][200] Create Sale Hierarchy with arguments    access_token=${suite_admin_access_token}    root_id=${test_agent_id}
    [Agent][Test][200] - Create connection for sale 1 and sale 2    access_token=${suite_admin_access_token}    sale_hierarchy_id=${test_sale_hierarchy_id}     parent_id=${agent_actor_id}     child_id=${agent_target_id}
    [Agent][Test][200] Check sale fund in permission on actor and target agent  access_token=${test_agent_access_token}    subordinate_id=${agent_target_id}     permission_code=FUND_IN
    [Agent][Test][200] Verify validate role sale permission return true     ${test_result_dic}

TC_EQP_12639 - [API][Fail] Validate permission of sales without connection
    [Tags]    openshift      API     success     regression      2019-05-VN-SAVIORS
    [Test][200] create sale and identity with agent type
    [Agent][Test][200] - Set actor agent id
    [Test][200] Create Sale Hierarchy with arguments    access_token=${suite_admin_access_token}    root_id=${test_agent_id}
    [Test][200] create sale and identity with agent type
    [Agent][Test][200] - Set target agent id
    [Agent][Test][200] - Authenticate agent with arguments      username=${test_agent_actor_username}      password=${test_agent_password_login}
    [200][Prepare] - Set permission is true with fund in function and actor is sale type 1 and target is sale type 2    access_token=${suite_admin_access_token}    hierarchy_id=${test_sale_hierarchy_id}  permission_id=7     apply_to_all_agent=false    actor_type_id=${sale_type_1_id}     target_type_id=${sale_type_2_id}
    [Agent][Test][200] Check sale fund in permission on actor and target agent  access_token=${test_agent_access_token}    subordinate_id=${agent_target_id}     permission_code=FUND_IN
    [Agent][Test][200] Verify validate role sale permission return false     ${test_result_dic}

TC_EQP_12640 - [API][Fail] Validate permission of sales without un-exist actor
    [Tags]    openshift      API     success     regression      2019-05-VN-SAVIORS
    [Agent][Test][Fail] Call validate permission with un-exist actor    access_token=${suite_admin_access_token}    subordinate_id=-1    permission_code=FUND_IN
    [Agent][Test][Fail] Verify validate role sale permission return bad request     ${test_result_dic}

TC_EQP_12641 - [API][Fail] Validate permission of sales without un-exist permission code
    [Tags]    openshift      API     success     regression      2019-05-VN-SAVIORS
    [Test][200] create sale and identity with agent type
    [Agent][Test][200] - Set actor agent id
    [Test][200] create sale and identity with agent type
    [Agent][Test][200] - Set target agent id
    [Agent][Test][200] - Authenticate agent with arguments      username=${test_agent_actor_username}      password=${test_agent_password_login}
    [Agent][Test] Call validate permission with un-exist permission code    access_token=${test_agent_access_token}    subordinate_id=${agent_target_id}     permission_code=Just_For_Fun
    [Agent][Test][Fail] Verify validate role sale permission return bad request     ${test_result_dic}

TC_EQP_12681 - [API][Success] Validate permission between 2 connected sales that not set permission of fund in
    [Test][200] create sale and identity with agent type
    [Agent][Test][200] - Set actor agent id
    [Test][200] Create Sale Hierarchy with arguments    access_token=${suite_admin_access_token}    root_id=${test_agent_id}
    [Test][200] create sale and identity with agent type
    [Agent][Test][200] - Set target agent id
    [Agent][Test][200] - Authenticate agent with arguments      username=${test_agent_actor_username}      password=${test_agent_password_login}
    [Agent][Test][200] - Create connection for sale 1 and sale 2    access_token=${suite_admin_access_token}    sale_hierarchy_id=${test_sale_hierarchy_id}     parent_id=${agent_actor_id}     child_id=${agent_target_id}
    [Agent][Test][200] Check sale fund in permission on actor and target agent  access_token=${test_agent_access_token}    subordinate_id=${agent_target_id}    permission_code=FUND_IN
    [Agent][Test][200] Verify validate role sale permission return false     ${test_result_dic}

TC_EQP_12682 - [API][Success] Validate permission of sales that not in any hierarchy
    [Test][200] create sale and identity with agent type
    [Agent][Test][200] - Set actor agent id
    [Test][200] create sale and identity with agent type
    [Agent][Test][200] - Set target agent id
    [Agent][Test][200] - Authenticate agent with arguments      username=${test_agent_actor_username}      password=${test_agent_password_login}
    [Agent][Test][200] Check sale fund in permission on actor and target agent  access_token=${test_agent_access_token}    subordinate_id=${agent_target_id}     permission_code=FUND_IN