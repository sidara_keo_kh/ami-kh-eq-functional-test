*** Settings ***
Resource          ../../agent/keywords/company_representative_profile.robot

Suite Setup       run keywords
...     [System_User][Suite][200] - login as setup system user     AND
...     [Suite][200] create company profiles with required fields   AND
...     [Suite] Set company representative type mapping

Default Tags      robot     API     company_profiles

*** Test Cases ***
TC_EQP_12049 - [API][200] System user creates company representative profile with all fields
    [Tags]      API    success      2019-03-TH-AVENGERS     smoke_test
    [Test][200] System user create company representative profile     type_id=1
    [Test][200] Search all representatives company profile by id
    [Test][200] Verify API get company representative by id return data correctly for create all fields

TC_EQP_12048 - [API][200] System user creates company representative profile with required fields
    [Tags]      API    success      2019-03-TH-AVENGERS
    [Test][200] System user create company representative profile required fields   type_id=1
    [Test][200] Search all representatives company profile by id
    [Test] Verify API get company representative by id return data correctly for create required fields

TC_EQP_12050 - [API][200] System user updates company representative profile with all fields
    [Tags]      API    success      2019-03-TH-AVENGERS
    [Test][200] System user create company representative profile     type_id=1
    [Test][200] System user update company representative profile     type_id=2
    [Test][200] Search all representatives company profile by id
    [Test][200] Verify API get company representative by id return data correctly for update all fields

TC_EQP_12051 - [API][200] System user updates company representative profile with required fields
    [Tags]      API    success      2019-03-TH-AVENGERS
    [Test][200] System user create company representative profile     type_id=1
    [Test][200] System user update company representative profile required fields     type_id=2
    [Test][200] Search all representatives company profile by id
    [Test] Verify API get company representative by id return data correctly for update required fields

TC_EQP_12058 - [API][Fail] System user creates company representative profile - representative type null
    [Tags]      API    success      2019-03-TH-AVENGERS
    [Test][Fail] System user create company representative without type
    [Test] Verify fail response correct     400     invalid_request     Representative type is required

TC_EQP_12059 - [API][Fail] System user creates company representative profile with all fields
    [Tags]      API    success      2019-03-TH-AVENGERS
    [Test][Fail] System user create company representative with not supported type
    [Test] Verify fail response correct     400     invalid_request     Representative type is not supported

TC_EQP_12060 - [API][Fail] System user creates company representative profile - company not existed
    [Tags]      API    success      2019-03-TH-AVENGERS
    [Test][Fail] System user create company representative with not existed company
    [Test] Verify fail response correct     400     invalid_request     Company does not exist

TC_EQP_12061 - [API][Fail] System user creates company representative profile - company deleted
    [Tags]      API    success      2019-03-TH-AVENGERS
    [Test][200] create company profiles with required fields
    [Test][200] delete company profile via api
    [Test][Fail] System user create company representative with not deleted company
    [Test] Verify fail response correct     400     invalid_request     Company already deleted

TC_EQP_12052 - [API][200] System user delete company representative profile
    [Tags]      API    success      2019-03-TH-AVENGERS
    [Test][200] System user create company representative profile       type_id=1
    [Test][200] System user delete company representative profile       representative_id=${test_company_representative_id}
    [Test][200] Search all representatives company profile by id
    [Test] Verify company representative is deleted

TC_EQP_12201 - [API][Fail] System user update company representative profile - representative type null
    [Tags]      API    fail         2019-03-TH-AVENGERS
    [Test][200] System user create company representative profile       type_id=1
    [Test][Fail] System user update company representative without type
    [Test] Verify fail response correct     400     invalid_request     Representative type is required

TC_EQP_12203 - [API][Fail] System user update company representative profile - representative type not supported
    [Tags]      API    fail         2019-03-TH-AVENGERS
    [Test][200] System user create company representative profile       type_id=1
    [Test][Fail] System user update company representative with not supported type
    [Test] Verify fail response correct     400     invalid_request     Representative type is not supported

TC_EQP_12204 - [API][Fail] System user update company representative profile - representative does not exist
    [Test][Fail] System user update company representative with not existed representative
    [Test] Verify fail response correct     400     invalid_request     Company representative does not exist

TC_EQP_12205 - [API][Fail] System user update company representative profile - representative is deleted
    [Test][200] System user create company representative profile       type_id=1
    [Test][200] System user delete company representative profile       representative_id=${test_company_representative_id}
    [Test][Fail] System user update company representative with not deleted representative
    [Test] Verify fail response correct     400     invalid_request     Company representative already deleted

TC_EQP_12207 - [API][Fail] System user delete company representative profile - representative does not exist
    [Test][Fail] System user delete company representative with not existed representative
    [Test] Verify fail response correct     400     invalid_request     Company representative does not exist

TC_EQP_12206 - [API][Fail] System user delete company representative profile - representative is deleted
    [Test][200] System user create company representative profile       type_id=1
    [Test][200] System user delete company representative profile       representative_id=${test_company_representative_id}
    [Test][Fail] System user delete company representative with not deleted representative
    [Test] Verify fail response correct     400     invalid_request     Company representative already deleted