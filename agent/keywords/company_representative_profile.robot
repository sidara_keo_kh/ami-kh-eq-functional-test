*** Settings ***
Resource          ../../3_prepare_data/by_api/imports.robot

*** Keywords ***
[Suite] Set company representative type mapping
    ${mapping_dic}      create dictionary
    ...     1=Billing Contact
    ...     2=Legal Representative
    ...     3=Authorized Signatory
    ...     4=Owner
    set suite variable   ${suite_company_representative_type_mapping}   ${mapping_dic}

[Test][200] System user create company representative profile
    [Arguments]     &{arg_dic}
    ${random_string}=    generate random string    5      [LETTERS]
    ${random_number}=    generate random string    5      [NUMBERS]
    ${arg_dic}      create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     type_id=${arg_dic.type_id}
    ...     company_id=${suite_company_id}
    ...     title=${random_string}_title
    ...     first_name=${random_string}_first_name
    ...     middle_name=${random_string}_middle_name
    ...     last_name=${random_string}_last_name
    ...     suffix=${random_string}_suffix
    ...     date_of_birth=1986-01-01T00:00:00Z
    ...     place_of_birth=${random_string}_place_of_birth
    ...     occupation=${random_string}_occupation
    ...     occupation_title=${random_string}_occupation_title
    ...     nationality=${random_string}_nationality
    ...     national_id_number=${random_string}_national_id_number
    ...     email=${random_string}_email@mail.com
    ...     source_of_funds=${random_string}_source_of_funds
    ...     mobile_number=${random_number}
    ...     business_phone_number=${random_number}
    ...     citizen_association=${random_string}_citizen_association
    ...     neighbourhood_association=${random_string}_neighbourhood_association
    ...     address=${random_string}_address
    ...     commune=${random_string}_commune
    ...     district=${random_string}_district
    ...     city=${random_string}_city
    ...     province=${random_string}_province
    ...     postal_code=${random_number}
    ...     country=${random_string}_country
    ...     landmark=${random_string}_landmark
    ...     longitude=${random_number}
    ...     latitude=${random_number}

    ${return_dic}  [200] API create company representative profiles     ${arg_dic}
    set test variable   ${test_company_representative_id}   ${return_dic.id}
    set test variable   ${test_company_representative_random_string}   ${random_string}
    set test variable   ${test_company_representative_random_number}   ${random_number}

[Test][Fail] System user create company representative profile
    [Arguments]     &{arg_dic}
    ${random_string}=    generate random string    5      [LETTERS]
    ${random_number}=    generate random string    5      [NUMBERS]
    ${arg_dic}      create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     type_id=${arg_dic.type_id}
    ...     company_id=${arg_dic.company_id}
    ...     first_name=${random_string}_first_name
    ...     middle_name=${random_string}_middle_name
    ...     last_name=${random_string}_last_name
    ...     suffix=${random_string}_suffix
    ...     mobile_number=${random_number}

    ${return_dic}  [Fail] API create company representative profiles     ${arg_dic}
    set test variable   ${api_fail_response_dict}   ${return_dic}

[Test][Fail] System user create company representative with not supported type
    [Test][Fail] System user create company representative profile      type_id=5       company_id=${suite_company_id}

[Test][Fail] System user create company representative without type
    [Test][Fail] System user create company representative profile       type_id=${EMPTY}      company_id=${suite_company_id}

[Test][Fail] System user create company representative with not existed company
    [Test][Fail] System user create company representative profile       type_id=3      company_id=-1

[Test][Fail] System user create company representative with not deleted company
    [Test][Fail] System user create company representative profile       type_id=1      company_id=${test_company_id}

[Test] Verify fail response correct
    [Arguments]     ${fail_status}          ${fail_code}        ${fail_message}
    ${fail_status_response}         get from dictionary     ${api_fail_response_dict}    fail_status
    ${fail_code_response}           get from dictionary     ${api_fail_response_dict}    fail_code
    ${fail_message_response}        get from dictionary     ${api_fail_response_dict}    fail_message
    should be equal as strings   ${fail_status_response}                            ${fail_status}
    should be equal as strings   ${fail_code_response}                              ${fail_code}
    should be equal as strings   ${fail_message_response}                           ${fail_message}

[Test][200] System user create company representative profile required fields
    [Arguments]     &{arg_dic}
    ${arg_dic}      create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     type_id=${arg_dic.type_id}
    ...     company_id=${suite_company_id}

    ${return_dic}  [200] API create company representative profiles     ${arg_dic}
    set test variable   ${test_company_representative_id}   ${return_dic.id}

[Test][200] System user update company representative profile
    [Arguments]     &{arg_dic}
    ${random_string}=    generate random string    5      [LETTERS]
    ${random_number}=    generate random string    5      [NUMBERS]
    ${arg_dic}      create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     type_id=${arg_dic.type_id}
    ...     representative_id=${test_company_representative_id}
    ...     title=${random_string}_title
    ...     first_name=${random_string}_first_name
    ...     middle_name=${random_string}_middle_name
    ...     last_name=${random_string}_last_name
    ...     suffix=${random_string}_suffix
    ...     date_of_birth=1986-01-01T00:00:00Z
    ...     place_of_birth=${random_string}_place_of_birth
    ...     occupation=${random_string}_occupation
    ...     occupation_title=${random_string}_occupation_title
    ...     nationality=${random_string}_nationality
    ...     national_id_number=${random_string}_national_id_number
    ...     email=${random_string}_email@mail.com
    ...     source_of_funds=${random_string}_source_of_funds
    ...     mobile_number=${random_number}
    ...     business_phone_number=${random_number}
    ...     citizen_association=${random_string}_citizen_association
    ...     neighbourhood_association=${random_string}_neighbourhood_association
    ...     address=${random_string}_address
    ...     commune=${random_string}_commune
    ...     district=${random_string}_district
    ...     city=${random_string}_city
    ...     province=${random_string}_province
    ...     postal_code=${random_number}
    ...     country=${random_string}_country
    ...     landmark=${random_string}_landmark
    ...     longitude=${random_number}
    ...     latitude=${random_number}

    [200] API update company representative profiles     ${arg_dic}
    set test variable   ${test_updated_company_representative_random_string}   ${random_string}
    set test variable   ${test_updated_company_representative_random_number}   ${random_number}

[Test][Fail] System user update company representative profile
    [Arguments]     &{arg_dic}
    ${random_string}=    generate random string    5      [LETTERS]
    ${random_number}=    generate random string    5      [NUMBERS]
    ${arg_dic}      create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     type_id=${arg_dic.type_id}
    ...     representative_id=${arg_dic.representative_id}
    ...     title=${random_string}_title
    ...     first_name=${random_string}_first_name
    ...     middle_name=${random_string}_middle_name
    ...     last_name=${random_string}_last_name
    ...     suffix=${random_string}_suffix
    ...     email=${random_string}r@email.com
    ...     mobile_number=0345678901

    ${return_dic}  [Fail] API update company representative profiles     ${arg_dic}
    set test variable   ${api_fail_response_dict}   ${return_dic}

[Test][Fail] System user update company representative without type
    [Test][Fail] System user update company representative profile       type_id=${EMPTY}      representative_id=${test_company_representative_id}

[Test][Fail] System user update company representative with not supported type
    [Test][Fail] System user update company representative profile       type_id=5             representative_id=${test_company_representative_id}

[Test][Fail] System user update company representative with not existed representative
    [Test][Fail] System user update company representative profile       type_id=1             representative_id=-1

[Test][Fail] System user update company representative with not deleted representative
    [Test][Fail] System user update company representative profile       type_id=1             representative_id=${test_company_representative_id}

[Test][200] System user delete company representative profile
    [Arguments]     &{arg_dic}

    ${arg_dic}      create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     representative_id=${arg_dic.representative_id}
    [200] API delete company representative profile     ${arg_dic}

[Test][Fail] System user delete company representative profile
    [Arguments]     &{arg_dic}
    ${random_string}=    generate random string    5      [LETTERS]
    ${random_number}=    generate random string    5      [NUMBERS]
    ${arg_dic}      create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     representative_id=${arg_dic.representative_id}

    ${return_dic}  [Fail] API delete company representative profiles     ${arg_dic}
    set test variable   ${api_fail_response_dict}   ${return_dic}

[Test][Fail] System user delete company representative with not existed representative
    [Test][Fail] System user delete company representative profile       representative_id=-1

[Test][Fail] System user delete company representative with not deleted representative
    [Test][Fail] System user delete company representative profile       representative_id=${test_company_representative_id}

[Test][200] System user update company representative profile required fields
    [Arguments]     &{arg_dic}
    ${arg_dic}      create dictionary
    ...     access_token=${suite_admin_access_token}
    ...     type_id=${arg_dic.type_id}
    ...     representative_id=${test_company_representative_id}

    [200] API update company representative profiles     ${arg_dic}

[Test][200] Search all representatives company profile by id
    ${arg_dic}      create dictionary   access_token=${suite_admin_access_token}    id=${test_company_representative_id}
    ${result}   [200] API search company representatives profile on report  ${arg_dic}
    set test variable   ${test_result_data}     ${result}

[Test][200] Verify API get company representative by id return data correctly for create all fields
    [Test] Verify API get company representative by id return data correctly all fields     ${test_company_representative_random_string}        ${test_company_representative_random_number}    1

[Test][200] Verify API get company representative by id return data correctly for update all fields
    [Test] Verify API get company representative by id return data correctly all fields     ${test_updated_company_representative_random_string}        ${test_updated_company_representative_random_number}    2

[Test] Verify API get company representative by id return data correctly all fields
    [Arguments]     ${test_string}          ${test_number}        ${type_id}
    ${representatives}        get from dictionary     ${test_result_data}    representatives
    ${representatives_size}      get length       ${representatives}
    Should Be True      ${representatives_size} == 1
    should be equal as integers  ${representatives[0]['id']}                        ${test_company_representative_id}
    should be equal as strings   ${representatives[0]['first_name']}                ${test_string}_first_name
    should be equal as strings   ${representatives[0]['last_name']}                 ${test_string}_last_name
    should be equal as strings   ${representatives[0]['middle_name']}               ${test_string}_middle_name
    should be equal as strings   ${representatives[0]['occupation']}                ${test_string}_occupation
    should be equal as strings   ${representatives[0]['date_of_birth']}             1986-01-01T00:00:00Z
    should be equal as strings   ${representatives[0]['place_of_birth']}            ${test_string}_place_of_birth
    should be equal as strings   ${representatives[0]['occupation_title']}          ${test_string}_occupation_title
    should be equal as strings   ${representatives[0]['nationality']}               ${test_string}_nationality
    should be equal as strings   ${representatives[0]['national_id_number']}        ${test_string}_national_id_number
    should be equal as strings   ${representatives[0]['email']}                     ${test_string}_email@mail.com
    should be equal as strings   ${representatives[0]['source_of_funds']}           ${test_string}_source_of_funds
    should be equal as strings   ${representatives[0]['mobile_number']}             ${test_number}
    should be equal as strings   ${representatives[0]['business_phone_number']}     ${test_number}
    should be equal as strings   ${representatives[0]['current_address']['citizen_association']}                ${test_string}_citizen_association
    should be equal as strings   ${representatives[0]['current_address']['neighbourhood_association']}          ${test_string}_neighbourhood_association
    should be equal as strings   ${representatives[0]['current_address']['address']}                            ${test_string}_address
    should be equal as strings   ${representatives[0]['current_address']['commune']}                            ${test_string}_commune
    should be equal as strings   ${representatives[0]['current_address']['district']}                           ${test_string}_district
    should be equal as strings   ${representatives[0]['current_address']['city']}                               ${test_string}_city
    should be equal as strings   ${representatives[0]['current_address']['province']}                           ${test_string}_province
    should be equal as strings   ${representatives[0]['current_address']['postal_code']}                        ${test_number}
    should be equal as strings   ${representatives[0]['current_address']['country']}                            ${test_string}_country
    should be equal as strings   ${representatives[0]['current_address']['landmark']}                           ${test_string}_landmark
    should be equal as strings   ${representatives[0]['current_address']['longitude']}                          ${test_number}
    should be equal as strings   ${representatives[0]['current_address']['latitude']}                           ${test_number}
    should be equal as strings   ${representatives[0]['permanent_address']['citizen_association']}              ${test_string}_citizen_association
    should be equal as strings   ${representatives[0]['permanent_address']['neighbourhood_association']}        ${test_string}_neighbourhood_association
    should be equal as strings   ${representatives[0]['permanent_address']['address']}                          ${test_string}_address
    should be equal as strings   ${representatives[0]['permanent_address']['commune']}                          ${test_string}_commune
    should be equal as strings   ${representatives[0]['permanent_address']['district']}                         ${test_string}_district
    should be equal as strings   ${representatives[0]['permanent_address']['city']}                             ${test_string}_city
    should be equal as strings   ${representatives[0]['permanent_address']['province']}                         ${test_string}_province
    should be equal as strings   ${representatives[0]['permanent_address']['postal_code']}                      ${test_number}
    should be equal as strings   ${representatives[0]['permanent_address']['country']}                          ${test_string}_country
    should be equal as strings   ${representatives[0]['permanent_address']['landmark']}                         ${test_string}_landmark
    should be equal as strings   ${representatives[0]['permanent_address']['longitude']}                        ${test_number}
    should be equal as strings   ${representatives[0]['permanent_address']['latitude']}                         ${test_number}
    should be equal as strings   ${representatives[0]['type']['id']}                                            ${type_id}
    ${type_name}=    get from dictionary        ${suite_company_representative_type_mapping}   ${type_id}
    should be equal as strings   ${representatives[0]['type']['name']}                                          ${type_name}

[Test] Verify API get company representative by id return data correctly for create required fields
    [Test][200] Verify API get company representative by id return data correctly required fields     1

[Test] Verify API get company representative by id return data correctly for update required fields
    [Test][200] Verify API get company representative by id return data correctly required fields     2

[Test] Verify company representative is deleted
    ${representatives}        get from dictionary     ${test_result_data}    representatives
    ${representatives_size}      get length       ${representatives}
    Should Be True      ${representatives_size} == 1
    Should Be True      ${representatives[0]['is_deleted']}


[Test][200] Verify API get company representative by id return data correctly required fields
    [Arguments]     ${type_id}
    ${representatives}        get from dictionary     ${test_result_data}    representatives
    ${representatives_size}      get length       ${representatives}
    Should Be True      ${representatives_size} == 1
    should be equal              ${representatives[0]['id']}                        ${test_company_representative_id}
    should be equal as strings   ${representatives[0]['first_name']}                None
    should be equal as strings   ${representatives[0]['last_name']}                 None
    should be equal as strings   ${representatives[0]['middle_name']}               None
    should be equal as strings   ${representatives[0]['occupation']}                None
    should be equal as strings   ${representatives[0]['date_of_birth']}             None
    should be equal as strings   ${representatives[0]['place_of_birth']}            None
    should be equal as strings   ${representatives[0]['occupation_title']}          None
    should be equal as strings   ${representatives[0]['nationality']}               None
    should be equal as strings   ${representatives[0]['national_id_number']}        None
    should be equal as strings   ${representatives[0]['email']}                     None
    should be equal as strings   ${representatives[0]['source_of_funds']}           None
    should be equal as strings   ${representatives[0]['mobile_number']}             None
    should be equal as strings   ${representatives[0]['business_phone_number']}     None
    should be equal as strings   ${representatives[0]['current_address']['citizen_association']}                None
    should be equal as strings   ${representatives[0]['current_address']['neighbourhood_association']}          None
    should be equal as strings   ${representatives[0]['current_address']['address']}                            None
    should be equal as strings   ${representatives[0]['current_address']['commune']}                            None
    should be equal as strings   ${representatives[0]['current_address']['district']}                           None
    should be equal as strings   ${representatives[0]['current_address']['city']}                               None
    should be equal as strings   ${representatives[0]['current_address']['province']}                           None
    should be equal as strings   ${representatives[0]['current_address']['postal_code']}                        None
    should be equal as strings   ${representatives[0]['current_address']['country']}                            None
    should be equal as strings   ${representatives[0]['current_address']['landmark']}                           None
    should be equal as strings   ${representatives[0]['current_address']['longitude']}                          None
    should be equal as strings   ${representatives[0]['current_address']['latitude']}                           None
    should be equal as strings   ${representatives[0]['permanent_address']['citizen_association']}              None
    should be equal as strings   ${representatives[0]['permanent_address']['neighbourhood_association']}        None
    should be equal as strings   ${representatives[0]['permanent_address']['address']}                          None
    should be equal as strings   ${representatives[0]['permanent_address']['commune']}                          None
    should be equal as strings   ${representatives[0]['permanent_address']['district']}                         None
    should be equal as strings   ${representatives[0]['permanent_address']['city']}                             None
    should be equal as strings   ${representatives[0]['permanent_address']['province']}                         None
    should be equal as strings   ${representatives[0]['permanent_address']['postal_code']}                      None
    should be equal as strings   ${representatives[0]['permanent_address']['country']}                          None
    should be equal as strings   ${representatives[0]['permanent_address']['landmark']}                         None
    should be equal as strings   ${representatives[0]['permanent_address']['longitude']}                        None
    should be equal as strings   ${representatives[0]['permanent_address']['latitude']}                         None
    should be equal as strings   ${representatives[0]['type']['id']}                                            ${type_id}
    ${type_name}=    get from dictionary        ${suite_company_representative_type_mapping}   ${type_id}
    should be equal as strings   ${representatives[0]['type']['name']}                                          ${type_name}

