*** Settings ***
Resource    ../../3_prepare_data/by_api/imports.robot

*** Keywords ***
[200][API][Agent Type] Verify agent type info
    [Arguments]     ${agent_types}
    :FOR    ${agent_type}   IN  @{agent_types}
    \   ${status}   Run keyword and return status   should be equal as integers     ${test_agent_type_id}   ${agent_type['id']}
    \   run keyword if  ${status}   should be equal as strings      ${agent_type['name']}           ${test_updated_agent_type_name}
    \   run keyword if  ${status}   should be equal as strings      ${agent_type['description']}    ${test_updated_agent_type_desc}
    \   run keyword if  ${status}   should be equal as strings      ${agent_type['is_manager']}     ${test_updated_agent_type_manager}
    \   run keyword if  ${status}   should be equal as strings      ${agent_type['reference_1']}    ${test_updated_agent_type_ref}
    \   run keyword if  ${status}   should be equal as strings      ${agent_type['reference_2']}    ${test_updated_agent_type_ref}
    \   run keyword if  ${status}   should be equal as strings      ${agent_type['reference_3']}    ${test_updated_agent_type_ref}
    \   run keyword if  ${status}   should be equal as strings      ${agent_type['reference_4']}    ${test_updated_agent_type_ref}
    \   run keyword if  ${status}   should be equal as strings      ${agent_type['reference_5']}    ${test_updated_agent_type_ref}
    \   Should be true   ${status} == True

[200][API][Agent Type] Verify new agent type info has created
    [Arguments]     ${agent_types}
    :FOR    ${agent_type}   IN  @{agent_types}
    \   ${status}   Run keyword and return status   should be equal as integers             ${test_agent_type_id}       ${agent_type['id']}
    \   run keyword if  ${status}   should be equal as strings  ${agent_type['name']}          ${test_created_agent_type_name}
    \   run keyword if  ${status}   should be equal as strings  ${agent_type['description']}   ${test_created_agent_type_desc}
    \   run keyword if  ${status}   should be equal as strings  ${agent_type['is_manager']}    True
    \   run keyword if  ${status}   should be equal as strings  ${agent_type['reference_1']}   ${test_created_agent_type_ref}
    \   run keyword if  ${status}   should be equal as strings  ${agent_type['reference_2']}   ${test_created_agent_type_ref}
    \   run keyword if  ${status}   should be equal as strings  ${agent_type['reference_3']}   ${test_created_agent_type_ref}
    \   run keyword if  ${status}   should be equal as strings  ${agent_type['reference_4']}   ${test_created_agent_type_ref}
    \   run keyword if  ${status}   should be equal as strings  ${agent_type['reference_5']}   ${test_created_agent_type_ref}
    \   Should be true   ${status} == True
