*** Settings ***
Documentation    Suite description

*** Keywords ***
[Agent][Test][200] - Set actor agent id
    set test variable   ${agent_actor_id}   ${test_agent_id}
    set test variable   ${sale_type_1_id}   ${test_agent_type_id}
    set test variable   ${test_agent_actor_username}    ${test_agent_pin_username}

[Agent][Test][200] - Set target agent id
    set test variable   ${agent_target_id}   ${test_agent_id}
    set test variable   ${sale_type_2_id}   ${test_agent_type_id}

[Agent][Test][200] Verify validate role sale permission return true
    [Arguments]  ${test_result_dic}
    ${is_valid}    get from dictionary     ${test_result_dic}     is_valid
    Should Be Equal  ${is_valid}    ${TRUE}

[Agent][Test][200] Verify validate role sale permission return false
    [Arguments]  ${test_result_dic}
    ${is_valid}    get from dictionary     ${test_result_dic}     is_valid
    Should Be Equal  ${is_valid}    ${FALSE}

[Agent][Test][Fail] Verify validate role sale permission return bad request
    [Arguments]  ${test_result_dic}
    ${code}    get from dictionary     ${test_result_dic}     code
    ${message}  get from dictionary     ${test_result_dic}      message
    Should be equal as strings  ${code}     invalid_request