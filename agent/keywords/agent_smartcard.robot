*** Settings ***
Resource          ../../3_prepare_data/by_api/imports.robot

*** Keywords ***
[200][API] [Get smart card] Verify that all attributes are returned in response
    ${data}        get from dictionary     ${test_result_dic}     data
    Dictionary Should Contain Key    ${data}    id
    Dictionary Should Contain Key    ${data}    serial_number
    Dictionary Should Contain Key    ${data}    is_deleted
    Dictionary Should Contain Key    ${data}    owner
    Dictionary Should Contain Key    ${data}    created_timestamp
    Dictionary Should Contain Key    ${data}    last_updated_timestamp

    ${serial_number}    get from dictionary     ${data}     serial_number
    should be equal as strings    ${serial_number}    ${test_serial_number}

[Fail] [API] [Get smart card] Verify api return smart card not found
    ${status}    get from dictionary     ${test_result_dic}     status
    ${code}    get from dictionary     ${status}     code
    ${message}    get from dictionary     ${status}     message
    should be equal as strings    ${code}    invalid_request
    should be equal as strings    ${message}    Smart card not found